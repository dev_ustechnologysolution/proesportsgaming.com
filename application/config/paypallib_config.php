<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------
// Paypal IPN Class
// ------------------------------------------------------------------------

// Use PayPal on Sandbox or Live
// FALSE for live environment
$host_split = explode('.',$_SERVER['HTTP_HOST']);


$config['sandbox'] = $host_split[0] == 'localhost' || $host_split[0] == 'esportstesting' ? TRUE : FALSE;

if ($host_split[0] == 'esportstesting') {
	$config['pemfile'] = '/etc/apache2/ssl/esportstesting.key';
} else if ($host_split[0] == 'proesportsgaming' || $host_split[0] == 'proesports') {
	$config['pemfile'] = '/var/www/html/proesportsgaming.com/ssl-cert/new/proesportsgaming_private_latest.key';
} else {
	$config['pemfile'] = '';
}

/* 
 * PayPal API Version
 * ------------------
 * The library is currently using PayPal API version 123.0.
 * You may adjust this value here and then pass it into the PayPal object when you create it within your scripts to override if necessary.
 */
$config['apiversion'] = '123.0';

// If (and where) to log ipn to file
$config['paypal_lib_ipn_log_file'] = BASEPATH . 'logs/paypal_ipn.log';
$config['paypal_lib_ipn_log'] = TRUE;

// Where are the buttons located at 
$config['paypal_lib_button_path'] = 'buttons';

// What is the default currency?
$config['paypal_lib_currency_code'] = 'USD';

?>
