<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 include BASEPATH.'../application/controllers/My_Controller.php';
class Authorize extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library('authorize_net');
		$this->authorize_net->authorizeAndCapture();
		
	}
	function index() {
		
	    $data['returnURL'] 	= $_REQUEST['return']; //payment success url
		$data['cancelURL'] 	= $_REQUEST['cancel_return'];  //payment cancel url
		$data['notifyURL'] 	= $_REQUEST['notify_url']; //ipn url
		$data['item_name'] 	= $_REQUEST['invoice'];
		$data['amount']    	= $_REQUEST['amount'];
		$data['custom']   	= $_REQUEST['custom'];
		$data['userID'] = $this->session->userdata('user_id'); 
		
       $this->load->view('authorize/authorize.tpl.php',$data);
	}

	function authorize_payment()
	{
		$auth_net = array(
		'x_card_num'			=> $this->input->post('card_num'),
		// 'x_exp_date'			=> '12/22',
		'x_exp_date'			=>  $this->input->post('exp_month').'/'. $this->input->post('exp_year'),
		'x_card_code'			=> $this->input->post('cvv'),
		'x_amount'				=> $this->input->post('amount'),
		// 'x_receipt_link_url'	=> $this->input->post('x_receipt_link_url'),
		// 'x_relay_response'		=> $this->input->post('x_relay_response'),
		// 'x_relay_url'			=> $this->input->post('x_relay_url'),
		'x_customer_ip'			=> $this->input->ip_address(),
		);
		// echo "<pre>";
		// print_r($auth_net);
		// exit();
		$this->authorize_net->setData($auth_net);

		// Try to AUTH_CAPTURE
		// echo "<pre>";
		// print_r($this->authorize_net->authorizeAndCapture());
		// exit();
		$data['card_num'] = $this->input->post('card_num');
		$data['cvv'] = $this->input->post('cvv');
		$data['exp_month'] = $this->input->post('exp_month');
		$data['exp_year'] = $this->input->post('exp_year');
       
		$paydata = array(
			
			'user_id'		=>	$this->session->userdata('user_id'),
			'game_id'		=> 	'',
			'subgame_id'	=>	'',
			'order_id'		=>	$this->input->post('invoice'),
			'payment_date'	=>	date('Y-m-d H:i:s'),
			'payment_status'=>	'due',
			'payment_type'=>	2,
			'card_data' => json_encode($data),
		);
		$this->General_model->insert_data('payment',$paydata);

		$paydata['amount'] = $amount;


		if( $this->authorize_net->authorizeAndCapture() )
		{
				$custom = json_decode($_REQUEST['custom']);
				$user_id = $custom->user_id;
				
				$userData = $this->General_model->view_data('user_detail',array('user_id'=>$user_id));
				if(property_exists($custom,'amount_tobe_added')){
					// echo 'Individual purchase';
					$amount_to_add = $custom->amount_tobe_added;
				} else {
					// echo 'Package _REQUESThase';
					$amount_to_add = $custom->amount-$custom->collected_fees;
				}
				// print_r(((13 * $custom->percentage) / 100));
				// echo $custom->percentage;
				$totPoints = $userData[0]['total_points'] + $custom->points;
				$totBalance = $userData[0]['total_balance'] + $amount_to_add;
				// echo 'actual amount : '.$_REQUEST['mc_gross'];
				// echo 'amount to add : ';print_r($amount_to_add);
				// exit;
				$updatedPoints = array(
					'updated_time' => date('Y-m-d H:i:s'),
					'total_balance' => $totBalance,
					'total_points' => $totPoints
				);
				$this->General_model->update_data('user_detail', $updatedPoints, array('user_id'=>$user_id));
				$this->session->set_userdata('total_balance',$totBalance);
				// print_r($totPoints);exit(0);
				//$item_no          = 	$_REQUEST['item_number'];
				$item_transaction   = 	$this->authorize_net->getTransactionId(); // transaction ID
				$item_price         = 	$custom->amount; // Paypal received amount
				$item_currency      = 	$this->input->post('currency_code');
				// $payer_email      	= 	$_REQUEST['payer_email'];
				// $payment_date      	= 	$_REQUEST['payment_date'];
				$points           	= 	$custom->points;
				$package_id         = 	$custom->package_id;
				//update data when challenger pay payment
				$dat = array(
					'transaction_id'	=>	$item_transaction,
					'amount'			=>	$item_price,
					'currency'			=>	$item_currency,
					'payment_status'	=>	'completed',
					'fees_percentage' 	=> 	$custom->fees_percentage,
					'collected_fees'    => 	$custom->collected_fees,
					'points' 			=> 	$points,
					'package_id' 		=> 	$package_id
					// 'payment_date'		=>	date('Y-m-d H:i:s',strtotime($payment_date)),
					// 'payer_email'			=>	$payer_email,
					
				);
				$this->General_model->update_data('payment',$dat,array('order_id' => $this->input->post('invoice',TRUE)));
				$data['dt']='Payment Successful';
				$data['banner_data'] = $this->General_model->tab_banner();
				$content=$this->load->view('payment/payment_success.tpl.php',$data,true);
				$this->render($content);
			// echo '<h2>Success!</h2>';
			// echo '<p>Transaction ID: ' . $this->authorize_net->getTransactionId() . '</p>';
			// echo '<p>Approval Code: ' . $this->authorize_net->getApprovalCode() . '</p>';	
		}
		else
		{
			echo '<h2>Fail!</h2>';
			// Get error
			echo '<p>' . $this->authorize_net->getError() . '</p>';
			// Show debug data
			$this->authorize_net->debug();
			// $data['dt']='Payment Fail';
	  //       $data['banner_data'] = $this->General_model->tab_banner();
	  //       $content=$this->load->view('payment/payment_failure.tpl.php',$data,true);
	  //       $this->render($content);
			
		}
	}
	
	
}

