<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Authorizesecond extends My_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library("MyAuthorize");
		$this->load->library('user_agent');
	}
	public function index() {			
		$this->check_user_page_access();
		$data['returnURL'] 	= $_REQUEST['return']; //payment success url
		$data['cancelURL'] 	= $_REQUEST['cancel_return'];  //payment cancel url
		$data['notifyURL'] 	= $_REQUEST['notify_url']; //ipn url
		$data['item_name'] 	= $_REQUEST['invoice'];
		$data['amount']    	= $_REQUEST['amount'];
		$data['custom']   	= $_REQUEST['custom'];
		$data['userID'] = $this->session->userdata('user_id');
		$content = $this->load->view('authorize/authorizesecond.tpl.php',$data,true);
    	$this->render($content);
	}
	public function pushPayment(){
		$expdata=$this->input->post('exp_month').$this->input->post('exp_year');
		// echo "<pre>";
		// print_r($this->input->post('amount'));
		// exit();
		$dataCustomers=array(
			"cnumber"=>$this->input->post('card_num'),
			"cexpdate"=>$expdata,
			"ccode"=>$this->input->post('cvv'),
			// "cdesc"=>$this->input->post('cdesc'),
			"amount"=>$this->input->post('amount')
		);	
		$data['card_num'] = $this->input->post('card_num');
		$data['cvv'] = $this->input->post('cvv');
		$data['exp_month'] = $this->input->post('exp_month');
		$data['exp_year'] = $this->input->post('exp_year');
		$paydata = array(
			
			'user_id'		=>	$this->session->userdata('user_id'),
			'game_id'		=> 	'',
			'subgame_id'	=>	'',
			'order_id'		=>	$this->input->post('invoice'),
			'payment_date'	=>	date('Y-m-d H:i:s'),
			'payer_email'	=>	$this->input->post('email'),
			'payment_status'=>	'due',
			'payment_type'=>	2,
			'card_data' => json_encode($data),
		);
		$this->General_model->insert_data('payment',$paydata);

		$paydata['amount'] = $amount;
		$this->myauthorize->authorize_fallback_data($this->agent->browser(), 'User Browser = ');
		$this->myauthorize->authorize_fallback_data(json_encode($paydata), 'Purchase Package card Checkout');

		$result = $this->myauthorize->chargerCreditCard($dataCustomers);
	
		if(!empty($result))
		{
			$this->payment_success($result);
		}
	}
	public function payment_success($transaction_id)
	{
		// echo "<pre>";
		// print_r($transaction_id);
		// exit();
		
		$custom = json_decode($_REQUEST['custom']);

				$user_id = $custom->user_id;
				$this->myauthorize->authorize_fallback_data(json_encode($_REQUEST), 'Successful Payment');
				
				$userData = $this->General_model->view_data('user_detail',array('user_id'=>$user_id));
				if(property_exists($custom,'amount_tobe_added')){
					// echo 'Individual purchase';
					$amount_to_add = $custom->amount_tobe_added;
				} else {
					// echo 'Package _REQUESThase';
					$amount_to_add = $custom->amount-$custom->collected_fees;
				}
				// print_r(((13 * $custom->percentage) / 100));
				// echo $custom->percentage;
				$totPoints = $userData[0]['total_points'] + $custom->points;
				$totBalance = $userData[0]['total_balance'] + $amount_to_add;
				// echo 'actual amount : '.$_REQUEST['mc_gross'];
				// echo 'amount to add : ';print_r($amount_to_add);
				// exit;
				$updatedPoints = array(
					'updated_time' => date('Y-m-d H:i:s'),
					'total_balance' => $totBalance,
					'total_points' => $totPoints
				);
				$this->General_model->update_data('user_detail', $updatedPoints, array('user_id'=>$user_id));
				$this->session->set_userdata('total_balance',$totBalance);
				// print_r($totPoints);exit(0);
				//$item_no          = 	$_REQUEST['item_number'];
				$item_transaction   = 	$transaction_id; // transaction ID
				$item_price         = 	$custom->amount; // Paypal received amount
				$item_currency      = 	$this->input->post('currency_code');
				// $payer_email      	= 	$_REQUEST['payer_email'];
				// $payment_date      	= 	$_REQUEST['payment_date'];
				$points           	= 	$custom->points;
				$package_id         = 	$custom->package_id;
				//update data when challenger pay payment
				$dat = array(
					'transaction_id'	=>	$item_transaction,
					'amount'			=>	$item_price,
					'currency'			=>	$item_currency,
					'payment_status'	=>	'completed',
					'fees_percentage' 	=> 	$custom->fees_percentage,
					'collected_fees'    => 	$custom->collected_fees,
					'points' 			=> 	$points,
					'package_id' 		=> 	$package_id
					// 'payment_date'		=>	date('Y-m-d H:i:s',strtotime($payment_date)),
					// 'payer_email'			=>	$payer_email,
					
				);
				$this->General_model->update_data('payment',$dat,array('order_id' => $this->input->post('invoice',TRUE)));
				$data['dt']='Payment Successful';
				$data['banner_data'] = $this->General_model->tab_banner();
				$content=$this->load->view('payment/payment_success.tpl.php',$data,true);
				 $this->render($content);
	}
	public function failure() {
         $this->myauthorize->authorize_fallback_data(json_encode($_REQUEST), 'Payment Fail');

        $data['dt']='Payment Fail';
        $data['banner_data'] = $this->General_model->tab_banner();
        $content=$this->load->view('payment/payment_failure.tpl.php',$data,true);
        $this->render($content);
    }

}
