<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Cart extends My_Controller {
	public function __construct() {
      parent::__construct();
      $this->load->model('My_model');
      $this->load->model('User_model');
      $this->load->library('pagination');
      $this->pageLimit = 20;
      $this->countItem = 10;
      $this->product_image_url = BASEPATH.'../upload/products/';
      $this->check_cover_page_access();
  }
  // Cart page
  function index() {
    $this->session->unset_userdata('shippaddress');
    $this->session->unset_userdata('billaddress');
    $data['category_list'] = $this->Product_model->getCategories();
    $data['cart_item'] = $this->Cart_model->cart_items();
    $data['admin_shipping_fee_arr'] = $this->Cart_model->calc_shipping_mdl('');
    $data['user_detail'] = $this->User_model->user_detail()[0];
    $content = $this->load->view('cart/cart.tpl.php',$data,true);
    $this->render($content);
  }
  function base64_to_jpeg($base64_string) {
    $img = time().'.jpg';
    $ifp = fopen($this->product_image_url.$img, 'wb'); 
    $data = explode(',', $base64_string);
    fwrite($ifp, base64_decode($data[1]));
    fclose($ifp); 
    return $img; 
  }
  function add_product_cart() {
    if (!empty($_REQUEST)) {
      if ($_POST['qty'] < 1) {
        $this->session->set_flashdata('message_err', 'Please select quantity more than '.$_POST['qty']);
        redirect($_SERVER['HTTP_REFERER']);
      }
      $cart_items = $this->Cart_model->cart_items();
      $product_image = (!empty($_POST['product_image_base64'])) ? $this->base64_to_jpeg($_POST['product_image_base64']) : $_POST['product_image'];
      $add_product = array(
        'product_id' => $_POST['product_id'],
        'product_image' => $product_image,
        'product_name' => $_POST['product_name'],
        'qty' => $_POST['qty'],
        'amount' => $_POST['amount'],
        'product_fee' => $_POST['product_fee'],
        'ref_seller_id' => $_POST['ref_seller_id'],
      );
      if (!empty($_POST['sel_variation'])) {
        $sel_variation = $_POST['sel_variation'];
        $sel_variation_extra_amnt = $_POST['extra_amount'];
        
        // Get Attribute list with option
        $arr = array('product_id' => $_POST['product_id']);
        $product_attribute_data = $this->Product_model->attributes_list($arr);
        $option_arr = array();
        if (!empty($product_attribute_data)) {
          $title_arr = array_column($product_attribute_data, 'title');
          $attr_arr = array_column($product_attribute_data, 'attr_id');
          $attribute_type_list = array_combine($attr_arr, $title_arr);
          foreach($product_attribute_data as $attribute) {
            $option_arr[$attribute->attr_id][$attribute->option_id] = $attribute->option_label; 
          }
          $sel_variation_arr = $sel_variation_amont_arr = array();
          foreach ($sel_variation as $key => $sv) {
            foreach ($sv as $k => $sel_attr) {
              $sel_variation_arr[] = 'atrlbl_'.$attribute_type_list[$key].'&optlbl_'.$option_arr[$key][$sel_attr];
              $sel_variation_amont_arr[] = 'atr_'.$key.'&opt_'.$sel_attr.'&variation_amount_'.
              $sel_variation_extra_amnt[$key][$sel_attr];
              
            }
          }
        }
        if (!empty($_POST['Custom_Tag'])) {
          $sel_variation_arr[] = 'atrlbl_Custom Tag&optlbl_'.trim($_POST['Custom_Tag']);
        }
        $add_product['sel_variation'] = $sel_variation_arr;
        $add_product['sel_variation_amount'] = $sel_variation_amont_arr;
       
      }
      // 'sel_variation_amount' => (!empty($_POST['extra_amount'])) ? $_POST['extra_amount'] : '',
      // 'sel_variation' => $_POST['sel_variation'],

      if (isset($_POST['add_to_cart'])) {
        $insert_product = $this->Cart_model->add_item_cart($add_product);
        if ($insert_product['added'] == true) {
          $msg = 'Item added into Cart';
          $this->session->set_flashdata('message_succ', $msg);
          redirect(base_url().'cart');
        } else if ($insert_product['errmsg'] !='') {
        	$this->session->set_flashdata('message_err', $insert_product['errmsg']);
        	redirect(base_url().'store/view_product/'.$insert_product['product_id']);
        } else {
          $this->session->set_flashdata('message_err', 'Unable to add item');
          redirect(base_url().'store/view_product/'.$_POST['product_id']);
        }
      } else if (isset($_POST['add_to_wishlist']) || (isset($_SESSION['request_data']) && $_SESSION['request_data']['action'] == 'add_to_wishlist')) {

        if (empty($this->session->userdata('user_id'))) {
          $_SESSION['redirect_url'] = base_url().$this->router->fetch_class().'/'.$this->router->fetch_method();
          $_SESSION['request_data'] = $add_product;
          $_SESSION['request_data']['action'] = 'add_to_wishlist';
          redirect(base_url().'login');
        }
        (empty($_POST)) ? $add_product = $_SESSION['request_data'] : '';

        $insert_product = $this->Cart_model->add_item_wishlist($add_product);
        unset($_SESSION['redirect_url']);
        unset($_SESSION['request_data']);

        if ($insert_product) {
          $this->session->set_flashdata('message_succ', 'Item added into Wishlist');
          redirect(base_url().'store/wishlist');
        } else {
          $this->session->set_flashdata('message_err', 'Unable to add item into Wishlist');
          redirect(base_url().'store/view_product/'.$_POST['product_id']);
        }
      }
    }
  }
  function add_to_cart_from_whislist() {
    if (!empty($_GET)) {
      $getitem = $this->db->get_where('wishlist_item',array('id' => $_GET['id']))->row();
      $insert_data = array(
        'action' => 'from_wishlist',
        'product_id' => $getitem->product_id,
        'product_image' => $getitem->product_image,
        'product_name' => $getitem->product_name,
        'qty' => $getitem->qty,
        'amount' => $getitem->amount,
        'product_fee' => $getitem->product_fee,
        'sel_variation' => $getitem->sel_variation,
        'sel_variation_amount' => $getitem->sel_variation_amount,
        'ref_seller_id' => $getitem->ref_seller_id
      );
      $insert_product = $this->Cart_model->add_item_cart($insert_data);
      if ($insert_product['added'] == true) {
        $this->General_model->delete_data('wishlist_item',array('id'=>$_GET['id']));
        $this->session->set_flashdata('message_succ', 'Item added into Cart');
        redirect(base_url().'cart');
      } else if ($insert_product['errmsg'] !='') {
        $this->session->set_flashdata('message_err', $insert_product['errmsg']);
        redirect(base_url().'store/wishlist/');
      } else {
        $this->session->set_flashdata('message_succ', 'Unable to add item');
        redirect(base_url().'store/wishlist/');
      }
    }
  }
  function remove_product_cart($id) {
  	$delete = $this->Product_model->remove_item($id,'single_product');
    ($delete) ? $this->session->set_flashdata('message_succ', 'Removed Item from cart') : $this->session->set_flashdata('message_err', 'Unable to remove');
    redirect(base_url().'cart');
  }
  function clear_cart($user_id) {
    $delete = $this->Product_model->remove_item($user_id,'all_product');
    ($delete) ? $this->session->set_flashdata('message_succ', 'Cart Cleared') : $this->session->set_flashdata('message_err', 'Unable to Clear');
    redirect(base_url().'cart');
  }
  // Cart Items Update
  function cart_items_update() {
    if (!empty($_POST)) {
      $cart_item = $this->Cart_model->cart_items();
      $getqty_col = array_column($cart_item, 'qty', 'id');
      $getprdct_col = array_column($cart_item, 'product_id', 'id');
      $product_qty = 0;
      foreach ($getprdct_col as $key => $gc) {
       ($gc == $_POST['product_id'] && $key != $_POST['cart_item_id']) ? $product_qty += $getqty_col[$key] : '';
      }
      $arr = array( 'product_id' => $_POST['product_id'], 'get' => 'row');
      $product_detail = $this->Product_model->productDetail($arr);
      $data['item'] = $_POST;
      if (($_POST['prdct_qty']+$product_qty) <= $product_detail->qty) {
        if ($this->session->userdata('user_id') !='') {
          $cart_update = $this->General_model->update_data('cart_item', array('qty' => $_POST['prdct_qty']), array('id' => $_POST['cart_item_id']));
        } else {
          $_SESSION['cart_item_list'][$_POST['cart_item_id']]['qty'] = $_POST['prdct_qty'];
        }
        $data['data_update'] = 'Item Update successfully';
        $prdcts_total_price = ($product_detail->fee + $product_detail->price + $_POST['extra_amount']) * $_POST['prdct_qty'];
        $data['prdcts_total_price'] = number_format($prdcts_total_price,2,".","");
      } else {
        $data['err'] = $exceed_prdct = "You can't add more items";
      }
      echo json_encode($data);
      exit();
    }
  }
}