<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Category extends My_Controller {
  public function __construct() {
    parent::__construct();
    $this->target_dir = BASEPATH.'../upload/products/';
    $this->check_cover_page_access();
    $this->check_user_page_access();
  }
  //page loading
  function index() {
  	$arr['user_id'] = $this->session->userdata('user_id');
 	$data['list'] = $this->Product_model->category_list($arr);
	$content = $this->load->view('store/categorylist.tpl.php',$data,true);
    $this->render($content);
  }
  function category_add($type) {
  	$data['type'] = $type;
  	$arr = array(
  		'user_id' => $this->session->userdata('user_id')
  	);
  	$data['list'] = $this->Product_model->category_list($arr);
    $content = $this->load->view('store/categoryadd.tpl.php',$data,true);
    $this->render($content);
  }
 	public function category_edit($type,$id) {
		$data['type'] = $type;
		$data['category_data'] = $this->General_model->view_data('product_categories_tbl',array('id'=>$id));
		$arr['user_id'] = $this->session->userdata('user_id');
	 	$data['list'] = $this->Product_model->category_list($arr);
		$content = $this->load->view('store/categoryadd.tpl.php',$data,true);
		$this->render($content);
	}
	
	public function category_insert() {
		$query = $this->db->query("SELECT MAX(sort_num) as sort_num FROM product_categories_tbl");
		$row = $query->row();
		$data = array(
			'parent_id' => (isset($_POST['parent_id'])) ? $_POST['parent_id'] : 0,
			'user_id' => $this->session->userdata('user_id'),
			'name' => trim($_REQUEST['name']),
			'status' => 1,
			'sort_num' => $row->sort_num +1,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		);
		
		$r = $this->General_model->insert_data('product_categories_tbl',$data);
		($r) ? $this->session->set_flashdata('message_succ', 'Category Insert successfull') : $this->session->set_flashdata('message_err', 'Failure to update');
		redirect(base_url().'category');
	}
	public function category_update() {
		$id = $this->input->post('id', TRUE);
		$data = array(
			'parent_id' => $_POST['parent_id'],
			'name' => trim($_POST['name']),
			'updated_at' => date('Y-m-d H:i:s')
		);
		$r = $this->General_model->update_data('product_categories_tbl',$data,array('id'=>$id));
		if ($r) {
			$this->session->set_flashdata('message_succ', 'Update successfull');
			redirect(base_url().'category');
		} else {
			$this->session->set_flashdata('message_err', 'Failure to update');	
			redirect(base_url().'category/category_edit/'.$this->input->post('type', TRUE).'/'.$this->input->post('id', TRUE));
		}
	}
	public function delete($id) {
		$r = $this->General_model->delete_data('product_categories_tbl',array('id'=> $id));
		($r) ? $this->session->set_flashdata('message_succ', 'Delete Successfull') : $this->session->set_flashdata('message_err', 'Failure to delete');
		header('location:'.base_url().'category');
	}
	public function update_is_status($id,$type){
		$data['status'] = $type;
		$this->General_model->update_data('product_categories_tbl',$data,array('id'=>$id));		
	}
}