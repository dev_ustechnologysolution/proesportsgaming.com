<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Checkout extends My_Controller {
    public function __construct() {
    	parent::__construct();
    	$this->load->model('My_model');
        $this->load->model('Email_model');
        $this->load->model('Paypal_payment_model');
        $this->load->library("MyAuthorize");
        $this->load->library('user_agent');
        $this->check_cover_page_access();
    }
    // Checkout page

    function _remap($method,$args) {
        if (method_exists($this, $method)) {
            if (count($args) < 2) {
                $this->$method($args[0]);
            } else {
                redirect(base_url());
            }
        } else {
            $this->index($method,$args);
        }
    }

    public function index($type) {
        (empty($type)) ? redirect(base_url().'cart') : '';
        $less_qty = array();
        $data['category_list'] = $this->General_model->view_all_data('product_categories_tbl','id','asc');
        $data['user_detail'] = $this->User_model->user_detail()[0];
        $data['country_list'] = $this->User_model->country_list();
        $cart_item = $this->Cart_model->cart_items();
        ($cart_item[0]['seller_id'] == 0) ? $cart_item[0]['country'] = 231 : '';
        $county_id = ($type == 'inside' && !empty($cart_item[0]['country'])) ? $cart_item[0]['country'] : $data['user_detail']['country'];
        $data['state_list'] = $this->User_model->state_list($county_id);
        $data['cart_item'] = $cart_item;
        foreach ($data['cart_item'] as $crt) {
            if($crt['qty'] < 1) {
                $less_qty[] = $crt['id'];
            }
        }
        if (count($less_qty) > 0){
            $this->session->set_flashdata('message_err', 'Please select quantity more than 1');
            redirect(base_url().'cart');
        }
        $data['admin_shipping_fee_arr'] = $this->Cart_model->calc_shipping_mdl('');
        if (in_array("", array_column($data['cart_item'], 'product_exist_id'))) {
            $this->session->set_flashdata('message_err', 'Remove the item which is not available');
            redirect(base_url().'cart');
        }
        (count($data['cart_item']) == 0) ? redirect(base_url().'cart') : '';

        $country = ($this->session->userdata('shippaddress')['country'] !='') ? $this->session->userdata('shippaddress')['country'] : '';
        $data['shipping_fee'] = $this->Cart_model->calc_shipping_mdl(array('country' => $country));

        $shipping_fee = explode('_', $data['shipping_fee']);
        $data['shipping_cal_type'] = $shipping_fee[0];
        $data['shipping_val'] = $shipping_fee[1];
        $_SESSION['shipping_location_type'] = $data['shipping_location_type'] = $type; 
        $content = $this->load->view('store/checkout.tpl.php',$data,true);
        $this->render($content);
    }
    public function login() {
    	$email = $this->input->post('email', TRUE);
		$password = md5($this->input->post('password', TRUE));
		$data_arr = $this->User_model->authenticate($email,$password);
		if ($this->input->post('check_as_guest') == 'on') {
			$randomNum = substr(str_shuffle("0123456789abcdefghijklmnopqrstvwxyz"), 0, 11);
			$this->session->set_userdata('guest_user',$randomNum);
			redirect(base_url().'checkout/'.$_SESSION['shipping_location_type']);
		} else {
			$changeactive_flag = $this->User_model->changeactive_flag($data_arr);
			if (isset($data_arr) && is_array($data_arr) && $data_arr->is_active == '0') {
				$this->User_model->log_this_login($data_arr);
				$this->session->unset_userdata('login_att');
				redirect(base_url().'checkout/'.$_SESSION['shipping_location_type']);
				exit();
			} else if (isset($data_arr) && is_array($data_arr) && $data_arr->is_active == '1') {
				$data['err']="";
				if($email!=''){	
					$checkbanned = $this->User_model->checkbanned($data_arr);
					$data['err'] ='<h5 style="margin-left: 255px;color: red;">'.$checkbanned.'</h5>';
				}
	        	$content=$this->load->view('login/login.php',$data,true);
	        	$this->render($content);
			} else { 
	        	$data['err']="";
	        	if ($email!='') {
	        		$data['err'] ='<h5 style="margin-left: 255px;color: red;">Invalid username or password</h5>';
	        	}
				if ($this->session->userdata('login_att') == '') {
					$this->session->set_userdata('login_att','1');
				} else {
					$user_att = $this->session->userdata('login_att') + 1;
					$this->session->set_userdata('login_att',$user_att);
				}
				if ($this->session->userdata('login_att') > 7) {
					$data['err'] ='<h5 style="margin-left: 255px;color: red;">Your Account Has Been Blocked </h5>';
					$user_data	=	array(
						'is_active'	=>	'1',
					);
					$this->General_model->update_data('user',$user_data,array('email'=>$email));
				}
				$content=$this->load->view('login/login.php',$data,true);
				$this->render($content);
	        }
	    }
    }
    public function contact_bill_info() {
    	if ($_POST['email'] !='' && $_POST['name'] !='' && $_POST['phone'] !='') {
    		$order_usrinfo = array(
    			'email' => $_POST['email'],
    			'name' => $_POST['name'],
    			'phone' => $_POST['phone']
    		);
    		$this->session->set_userdata('order_usrinfo',$order_usrinfo);
    	}
    	if ($_POST['address'] !='' && $_POST['country'] !='' && $_POST['state'] !='' && $_POST['zip_code'] !='') {
    		$billaddress = $shippaddress = array(
    			'address' => $_POST['address'],
    			'country' => $_POST['country'],
    			'state' => $_POST['state'],
                'city' => $_POST['city'],
    			'countrysortname' => $_POST['bill_countrysortname'],
    			'zip_code' => $_POST['zip_code']
    		);
    		$this->session->set_userdata('billaddress',$billaddress);
    		if ($_POST['ship_to_diffadd_check'] == 'on') {
	    		$shippaddress = array(
	    			'address' => $_POST['shipping_address'],
	    			'country' => $_POST['shipping_country'],
	    			'state' => $_POST['shipping_state'],
                    'city' => $_POST['shipping_city'],
	    			'countrysortname' => $_POST['shipp_countrysortname'],
	    			'zip_code' => $_POST['shipping_zip_code'],
	    		);
    		}
            $this->session->set_userdata('shippaddress',$shippaddress);
            // $country = ($this->session->userdata('shippaddress')['country'] !='') ? $this->session->userdata('shippaddress')['country'] : '';
            // $calc_shipping_mdl = $this->Cart_model->calc_shipping_mdl(array('country'=>$country));
            // if ($_POST['shipping_location_type'] == 'inside') {
            //     $shipping_fee = array_map(function ($value) {
            //         if ($value['id'] == 5)
            //             return $value['option_value'];
            //     }, $calc_shipping_mdl)[0];
            // } else {
            //     $shipping_fee = array_map(function ($value) { 
            //         if ($value['id'] == 4)
            //             return $value['option_value'];
            //     }, $calc_shipping_mdl)[0];
            // }
            // $shipping_fee = explode('_', $shipping_fee);

            // $ship_amnt = ($shipping_fee[0] == 1) ? '$ '.$shipping_fee[1] : $shipping_fee[1].' %'; 
            $ship_amnt = '$ '.number_format((float) $_SESSION['current_shipping_charge'], 2, '.', '');
            $this->session->set_flashdata('message_succ', 'A '.$ship_amnt.' Shipping Fee will be added');
            unset($_SESSION['current_shipping_charge']);
    	}
    	redirect(base_url().'checkout/'.$_SESSION['shipping_location_type']);
    }
    public function checkoutpay() {
    	if (!empty($_POST)) {
    		$user_id = ($this->session->userdata('user_id') !='') ? $this->session->userdata('user_id') : 0;
            $country = ($this->session->userdata('shippaddress')['country'] !='') ? $this->session->userdata('shippaddress')['country'] : '';
            $data['shipping_fee'] = $this->Cart_model->calc_shipping_mdl(array('country' => $country));
            $shipping_fee = explode('_', $data['shipping_fee']);
            $cart_item = $this->Cart_model->cart_items();
            $prdct_ref_sel = array_column($cart_item,'ref_seller_id');
	    	$prdct_prc = array_column($cart_item,'amount');
            $prdct_fee = array_column($cart_item,'product_fee');
            $prdct_qty = array_column($cart_item,'qty');
            $prdct_vrtn_arr = array_column($cart_item,'sel_variation_amount');
            $prdct_vrtn_amt = [];
            foreach ($prdct_vrtn_arr as $key => $pva) {
                $sv = explode('&', $pva);
                $prdct_vrtn_amt[] = end(explode('_', $sv[2]));
            }
            $SUBTOTAL = array_sum(array_map(function($w, $x, $y, $z) { return $w * ($x + $y + $z); }, $prdct_qty, $prdct_prc, $prdct_fee, $prdct_vrtn_amt));
            // $SHIPPING = $shipping_fee[0] == 1 ? $shipping_fee[1] : $SUBTOTAL*$shipping_fee[1]/100;
            (empty($_SESSION['current_shipping_charge'])) ? redirect(base_url().'checkout/'.$_SESSION['shipping_location_type']) : '';

            $SHIPPING = $_SESSION['current_shipping_charge'];
	    	$total = number_format((float)($SHIPPING)+($SUBTOTAL), 2, '.', '');
	    	$order_calc = array(
				'shipping_fee' => $SHIPPING,
				'subtotal' => $SUBTOTAL,
				'total' => $total
			);
			$this->session->set_userdata('order_calc',$order_calc);
			// Amount calculation
			$pymnt_opt = array(
				'user_id' => $user_id,
				'shipping_fee' => $SHIPPING,
				'cart_item' => $cart_item,
				'int' => $int,
				'prdct_prc' => $prdct_prc,
				'SUBTOTAL' => $SUBTOTAL,
				'total' => $total,
                'ref_id' => $prdct_ref_sel[0],
                'seller_id' => $cart_item[0]['seller_id'],
			);
			if (in_array('from_wallet', $_POST['check_paypal']) && !in_array('pay_with_paypal', $_POST['check_paypal'])  && !in_array('pay_with_card', $_POST['check_paypal'])) {
               
				if ($total > $this->session->userdata('total_balance')) {
					$this->session->set_flashdata('message_err', 'Insufficient Wallet balance to checkout');
					redirect(base_url().'checkout/'.$_SESSION['shipping_location_type']);
					exit();
				}
                $pymnt_opt['paypal_deducted'] = 0;
				$pymnt_opt['payment_method'] = 'wallet';
				$this->wallet_checkout($pymnt_opt);
			} else if (in_array('from_wallet', $_POST['check_paypal'])  && in_array('pay_with_paypal', $_POST['check_paypal']) && !in_array('pay_with_card', $_POST['check_paypal'])) {
                
   				$rvtotal = $total - $this->session->userdata('total_balance');
   				if ($rvtotal > 0) {
   					$pymnt_opt['payment_method'] = ($total == $rvtotal) ? 'paypal' : 'paypal&wallet';
   					$total = $rvtotal;
   					$pymnt_opt['total'] = $total;
   					$pymnt_opt['fromwallet'] = $this->session->userdata('total_balance');
		   			$this->paypal_checkout($pymnt_opt);
   				} else {
                    $pymnt_opt['paypal_deducted'] = 0;
   					$pymnt_opt['payment_method'] = 'wallet';
					$this->wallet_checkout($pymnt_opt);
   				}
            } else if (in_array('from_wallet', $_POST['check_paypal']) && in_array('pay_with_card', $_POST['check_paypal']) && !in_array('pay_with_paypal', $_POST['check_paypal'])) {
                     
                   
                $rvtotal = $total - $this->session->userdata('total_balance');

                if ($rvtotal > 0) {
                    $pymnt_opt['payment_method'] = ($total == $rvtotal) ? 'card' : 'card&wallet';
                    $total = $rvtotal;
                    $pymnt_opt['total'] = $total;
                    $pymnt_opt['fromwallet'] = $this->session->userdata('total_balance');

                    $this->payment_form($pymnt_opt);
                } else {
                    $pymnt_opt['card_deducted'] = 0;
                    $pymnt_opt['payment_method'] = 'wallet';
                    $this->wallet_checkout($pymnt_opt);
                }
   			} else if (!in_array('from_wallet', $_POST['check_paypal']) && !in_array('pay_with_card', $_POST['check_paypal']) && in_array('pay_with_paypal', $_POST['check_paypal'])) {
             
   				$pymnt_opt['fromwallet'] = 0;
   				$pymnt_opt['payment_method'] = 'paypal';
	   			$this->paypal_checkout($pymnt_opt);
   			}
            else if (!in_array('from_wallet', $_POST['check_paypal']) && !in_array('pay_with_paypal', $_POST['check_paypal']) && in_array('pay_with_card', $_POST['check_paypal'])) {
                
                $pymnt_opt['fromwallet'] = 0;
                $pymnt_opt['payment_method'] = 'card';
                 $this->payment_form($pymnt_opt);
                
            }
    	}
    }
    function wallet_checkout($wallet_opt) {
    	$user_id = $wallet_opt['user_id'];
    	$grand_total = $wallet_opt['total'];
    	$payment_method = $wallet_opt['payment_method'];
        $paypal_deducted = $wallet_opt['paypal_deducted'];
        $transactionid = (isset($wallet_opt['transactionid']) && $wallet_opt['transactionid'] != '') ? $wallet_opt['transactionid'] : '';
        $payerid = (isset($wallet_opt['payerid']) && $wallet_opt['payerid'] != '') ? $wallet_opt['payerid'] : '';
    	if ($user_id != 0) {
            $rem_bal = $this->session->userdata('total_balance') - $grand_total;
            $update_data['total_balance'] = number_format((float) $rem_bal, 2, '.', '');
            $this->General_model->update_data('user_detail', $update_data, array('user_id' => $user_id));
        }
        $paymentmethod = ($payment_method == 'wallet') ? '0' : (($payment_method == 'paypal') ? '1' : (($payment_method == 'paypal&wallet') ? '2' : ''));
    	$plc_ckordr = array(
			'user_id' => $user_id,
            'wallet_deducted' => $grand_total,
            'paypal_deducted' => $paypal_deducted,
            'payerid' => $payerid,
            'transactionid' => $transactionid,
			'payment_method' => $paymentmethod,
            'ref_id' => $wallet_opt['ref_id'],
            'seller_id' => $wallet_opt['seller_id']
		);
    	$this->place_ckout_order($plc_ckordr);
    }
    function paypal_checkout($paypl_opt) {
		$fromwallet = $paypl_opt['fromwallet'];
		$payment_method = $paypl_opt['payment_method'];
		$user_id = $paypl_opt['user_id'];
    	$cart_item = $paypl_opt['cart_item'];
    	$shipping_fee = $paypl_opt['shipping_fee'];
    	$int = $paypl_opt['int'];
    	$prdct_prc = $paypl_opt['prdct_prc'];
		$SUBTOTAL = $paypl_opt['SUBTOTAL'];
		$total = $paypl_opt['total'];
    	if (isset($_SESSION['order_usrinfo']) && !empty($_SESSION['order_usrinfo'])) {
			$data['shiptoname'] = $_SESSION['order_usrinfo']['name'];
			$data['shiptophonenum'] = $_SESSION['order_usrinfo']['phone'];
		}
		if (isset($_SESSION['shippaddress']) && !empty($_SESSION['shippaddress'])) {
            $data['shiptostreet'] = $_SESSION['shippaddress']['address'];
            $data['shiptozip'] = $_SESSION['shippaddress']['zip_code'];
            $data['shiptostate'] = $_SESSION['shippaddress']['state'];
            $data['shiptocountrycode'] = $_SESSION['shippaddress']['countrysortname'];
            $data['shiptocity'] = $_SESSION['shippaddress']['city'];
		}
		foreach ($cart_item as $key => $ci) {
			$set_cartitem[] = array(
                'name' => $ci['product_name'],
                'amt' => $ci['amount'],
                'number' => $ci['product_id'],
                'qty' => $ci['qty'],
                'itemurl' => base_url().'Store/view_product/'.$ci['product_id']
			);
		}
		$data['cartitem'] = $set_cartitem;
        $data['subscribe_amount'] = $total;        
        $payer_detail = $this->User_model->user_detail();
        $data['payer_email'] = $payer_detail[0]['email'];
       	$data['returnurl'] = $data['cancelurl'] = base_url().'checkout/get_payment_data/';
       	
        $custom_arr = array(
            'userid' => $user_id,
            'fromwallet' => $fromwallet,
            'payment_method' => $payment_method,
            'ref_id' => $paypl_opt['ref_id'],
            'seller_id' => $paypl_opt['seller_id'],
            'return_url' => base_url().'checkout/'.$_SESSION['shipping_location_type']
        );

        $data['custom'] = json_encode($custom_arr);
       
       	$data['title'] = 'Pay Amount.';
       	$data['notifyurl'] = '';
        $data['billingtype'] = 'MerchantInitiatedBilling';
        // echo "<pre>";
        // print_r($data);
        // exit();
        // $data['ref_id'] = $paypl_opt['ref_id'];
       	$response = $this->Paypal_payment_model->Set_express_checkout($data);
       	if($response['redirect'] != ''){
       		redirect($response['redirect']);
       	} else {
       		$this->error($response['errors']);
       	}
    }
    function get_payment_data() {
    	if ($_GET['token']) {
            $token = $_GET['token'];
            $result = $this->Paypal_payment_model->Get_express_checkout_details($token);
            $PayPalResult = $result['data'];
            $doexpresschk_detail =  $this->Paypal_payment_model->do_express_checkout_payment($result['data']);
            $custom_data = json_decode($PayPalResult['CUSTOM']);
            if (strtolower($doexpresschk_detail['data']['ACK']) == 'success') {
                // $response['data']['return_url'] = $custom_data->return_url;
       
        		// $custom_data = explode(',', $doexpresschk_detail['data']['REQUESTDATA']['PAYMENTREQUEST_0_CUSTOM']);

	    		$user_id = $custom_data->userid;
	    		$wallet_amount = $custom_data->fromwallet;
	    		$payment_method = $custom_data->payment_method;
                $ref_id = $custom_data->ref_id;
                $seller_id = $custom_data->seller_id;
                $payerid = $doexpresschk_detail['data']['REQUESTDATA']['PAYERID'];
                $transactionid = $doexpresschk_detail['data']['PAYMENTINFO_0_TRANSACTIONID'];
	    		$plc_ckordr = array(
                    'paypal_deducted' => $doexpresschk_detail['data']['PAYMENTINFO_0_AMT'],
	    			'payment_method' => $payment_method,
	    			'total' => $wallet_amount,
                    'transactionid' => $transactionid,
                    'payerid' => $payerid,
                    'ref_id' => $ref_id,
	    			'user_id' => $user_id,
                    'seller_id' => $seller_id
	    		);
            	$this->wallet_checkout($plc_ckordr);
	    	} else {
                echo "<pre>";
                print_r($result['data']);
                exit();
                // $response['data']['return_url'] = $custom_data->return_url;
                 // $this->error($result['data']);
            }
    	}
    }
  
    function place_ckout_order($plc_ckordr) {
       $card_data = $plc_ckordr['card_data'];
        $ref_seller_id = $plc_ckordr['ref_id'];
        $seller_id = $plc_ckordr['seller_id'];
        $wallet_deducted = number_format((float) $plc_ckordr['wallet_deducted'], 2, '.', '');
        $paypal_deducted = number_format((float) $plc_ckordr['paypal_deducted'], 2, '.', '');
        $card_deducted = number_format((float) $plc_ckordr['card_deducted'], 2, '.', '');
    	$payment_method = $plc_ckordr['payment_method'];

        $payerid = $plc_ckordr['payerid'];
        $paypal_transaction_id = $plc_ckordr['transactionid'];
        $card_transaction_id = $plc_ckordr['transaction_id'];
        $wbsite_transaction_id = $this->db->select_max('ot.transaction_id')->from('orders_tbl ot')->get()->row()->transaction_id+1;

    	$user_id = $plc_ckordr['user_id'];

        $shippaddress = $this->session->userdata('shippaddress');
        $billaddress = $this->session->userdata('billaddress');
        $cust_info = $this->session->userdata('order_usrinfo');

        if (!empty($this->session->userdata('order_calc'))) {
        	$subtotal = $this->session->userdata('order_calc')['subtotal'];
        	$shipping_fee = $this->session->userdata('order_calc')['shipping_fee'];
        	$grand_total = $this->session->userdata('order_calc')['total'];
        }
        $insert_order = array(
            'transaction_id' => $wbsite_transaction_id,
            'paypal_payer_id' => $payerid,
            'paypal_transaction_id' => $paypal_transaction_id,
            'card_transaction_id' => $card_transaction_id,
            'sold_by' => $seller_id,
            'ref_seller_id' => $ref_seller_id,
            'user_id' => $user_id,
            'name' => $cust_info['name'],
            'email' => $cust_info['email'],
            'phone' => $cust_info['phone'],
            'shipping_address' => $shippaddress['address'],
            'shipping_country' => $shippaddress['country'],
            'shipping_state' => $shippaddress['state'],
            'shipping_city' => $shippaddress['city'],
            'shipping_zip_code' => $shippaddress['zip_code'],
            'billing_address' => $billaddress['address'],
            'billing_country' => $billaddress['country'],
            'billing_state' => $billaddress['state'],
            'billing_city' => $billaddress['city'],
            'billing_zip_code' => $billaddress['zip_code'],
            'sub_total' => $subtotal,
            'shipping_charge' => $shipping_fee,
            'grand_total' => $grand_total,
            'payment_method' => $payment_method,
            'wallet_deducted' => $wallet_deducted,
            'paypal_deducted' => $paypal_deducted,
            'card_deducted' => $card_deducted,
            'order_status' => 'placed',
            'card_data' => $card_data,
            'created_at' => date('Y-m-d H:i:s', time()),
        );
        if($payment_method==3 || $payment_method==4){

            $this->myauthorize->authorize_fallback_data(json_encode($insert_order), 'Successful Payment');
        } 
        $order = $this->General_model->insert_data('orders_tbl', $insert_order);
        if ($order) {
            $cart_item = $this->Cart_model->cart_items();
            $is_single_download = [];
            $items_arr = [];
            foreach ($cart_item as $key => $itmlst) {
                $is_single_download[] = $itmlst['file_name'];
                $sel_variation = !empty($itmlst['sel_variation']) ? explode(', ', $itmlst['sel_variation']) : '';
                $variation = [];
	        	if (isset($sel_variation) && $sel_variation != '') {
	        		foreach ($sel_variation as $k => $sv) {
	        			$sv = explode('&', $sv);
	    				$var_title = end(explode('_', $sv[0]));
	        			$var_opt = end(explode('_', $sv[1]));
	        			$variation[] = $var_title.'_'.$var_opt;
	        		}
	        	}
				$product_attributes = implode(',', $variation);

                $insert_order_items = $items_arr[] = array(
                    'order_id' => $order,
                    'product_id' => $itmlst['product_id'],
                    'sold_by' => $itmlst['seller_id'],
                    'ref_seller_id' => $itmlst['ref_seller_id'],
                    'product_name' => $itmlst['product_name'],
                    'product_image' => $itmlst['product_image'],
                    'qty' => $itmlst['qty'],
                    'amount' => $itmlst['amount'],
                    'product_fee' => $itmlst['product_fee'],
                    'file_name' => $itmlst['file_name'],
                    'product_attributes' => $product_attributes,
                    'sel_variation_amount' => $itmlst['sel_variation_amount'],
                );
                $insertitems = $this->General_model->insert_data('orders_items_tbl', $insert_order_items);
                if ($insertitems) {
                    $darr['product_id'] = $itmlst['product_id'];
                    $darr['get'] = 'row';
                    $get_product_detail = $this->Product_model->productDetail($darr);
                    $update_data['qty'] = $get_product_detail->qty - $itmlst['qty'];
                    $this->General_model->update_data('products_tbl', $update_data, array('id' => $itmlst['product_id']));
                    $this->Product_model->remove_item($itmlst['id'],'single_product');
                }
            }
            if (sizeof($is_single_download) == 1 && $is_single_download[0] !='') {
                $user_data['order_status'] = 'completed';
                $this->General_model->update_data('orders_tbl',$user_data,array('id'=>$order));
            }
            if ($insertitems) {
                $emailarr = array(
                    'email_title' => 'Thank You for order',
                    'email_address' => $insert_order['email'],
                    // 'email_address' => 'amarjoshi581@gmail.com',
                    'name' => $insert_order['name'],
                    'umessage' => 'Your order has been received is now being processed.',
                    'wbsite_transaction_id' => $wbsite_transaction_id,
                    'order_info' => $insert_order,
                    'items_arr' => $items_arr,
                    'subject' => 'Your order has been placed',
                    'order_created_date' => $insert_order['created_at'],
                );
                $email_to_cust = $this->Email_model->placed_order_mail($emailarr);
                $emailarr['email_title'] = 'Hey! you have got a new order';
                $emailarr['subject'] = 'You have got new order';
                $emailarr['email_address'] = 'storeadmin';
                $emailarr['umessage'] = 'You have received an order from '.$cust_info['name'].'. The order is as follows:';
                $email_to_admin = $this->Email_model->placed_order_mail($emailarr); 

                $this->General_model->sendMail($email,$name,$subject,$body);

                $this->session->unset_userdata('shippaddress');
                $this->session->unset_userdata('billaddress');
                $this->session->unset_userdata('order_usrinfo');
                $this->session->set_flashdata('message_succ', 'Order placed successfull');
                redirect(base_url().'order');
            } else {
                $this->session->set_flashdata('message_err', 'Unable to Place order');
                redirect(base_url().'checkout/'.$_SESSION['shipping_location_type']);
            }
        } else {
            $this->session->set_flashdata('message_err', 'Unable to Place order');
            redirect(base_url().'checkout/'.$_SESSION['shipping_location_type']);
        }
    }
    function reset_shippingnfo() {
        $this->session->unset_userdata('shippaddress');
        $this->session->unset_userdata('billaddress');
        redirect(base_url().'checkout/'.$_SESSION['shipping_location_type']);
    }

    function payment_form($paypl_opt) {
        
        // $this->check_user_page_access();    
       $fromwallet = $paypl_opt['fromwallet'];
        $payment_method = $paypl_opt['payment_method'];
        $user_id = $paypl_opt['user_id'];
        $cart_item = $paypl_opt['cart_item'];
        $shipping_fee = $paypl_opt['shipping_fee'];
        $int = $paypl_opt['int'];
        $prdct_prc = $paypl_opt['prdct_prc'];
        $SUBTOTAL = $paypl_opt['SUBTOTAL'];
        $total = $paypl_opt['total'];
        if (isset($_SESSION['order_usrinfo']) && !empty($_SESSION['order_usrinfo'])) {
            $data['shiptoname'] = $_SESSION['order_usrinfo']['name'];
            $data['shiptophonenum'] = $_SESSION['order_usrinfo']['phone'];
        }
        if (isset($_SESSION['shippaddress']) && !empty($_SESSION['shippaddress'])) {
            $data['shiptostreet'] = $_SESSION['shippaddress']['address'];
            $data['shiptozip'] = $_SESSION['shippaddress']['zip_code'];
            $data['shiptostate'] = $_SESSION['shippaddress']['state'];
            $data['shiptocountrycode'] = $_SESSION['shippaddress']['countrysortname'];
            $data['shiptocity'] = $_SESSION['shippaddress']['city'];
        }
        foreach ($cart_item as $key => $ci) {
            $set_cartitem[] = array(
                'name' => $ci['product_name'],
                'amt' => $ci['amount'],
                'number' => $ci['product_id'],
                'qty' => $ci['qty'],
                'itemurl' => base_url().'Store/view_product/'.$ci['product_id']
            );
        }
        $data['fromwallet'] = $fromwallet;
        $data['cartitem'] = $set_cartitem;
        $data['subscribe_amount'] = $total;        
        $data['user_id'] = $user_id;        
        $data['payment_method'] = $payment_method;        
        $payer_detail = $this->User_model->user_detail();
        $data['payer_email'] = $payer_detail[0]['email'];
        $data['returnurl'] = $data['cancelurl'] = base_url().'checkout/get_payment_data/';
        $custom_arr = array(
            'userid' => $user_id,
            'fromwallet' => $fromwallet,
            'payment_method' => $payment_method,
            'ref_id' => $paypl_opt['ref_id'],
            'seller_id' => $paypl_opt['seller_id'],
            'return_url' => base_url().'checkout/'.$_SESSION['shipping_location_type'],
        );

        $data['custom'] = json_encode($custom_arr);

        $data['title'] = 'Pay Amount.';
        $data['notifyurl'] = '';
        $data['billingtype'] = 'MerchantInitiatedBilling';
        // echo "<pre>";
        // print_r($data);
        // exit();
        $content = $this->load->view('authorize/authorizecheckout.tpl.php',$data,true);
        $this->render($content);

    }
    function card_checkout()
    {
        
        $user_id=$this->input->post('user_id');
        $total=$this->input->post('total');
        $payment_method=$this->input->post('payment_method');
        $fromwallet=$this->input->post('fromwallet');

        $data['card_num'] = $this->input->post('card_num');
        $data['cvv'] = $this->input->post('cvv');
        $data['exp_month'] = $this->input->post('exp_month');
        $data['exp_year'] = $this->input->post('exp_year');
        $card_data =json_encode($data);

        $payload_data['user_id']=$user_id;
        $payload_data['total']=$total;
        $payload_data['payment_method']=$payment_method;
        $payload_data['fromwallet']=$fromwallet;
        $payload_data['card_data']=$card_data;
        $this->myauthorize->authorize_fallback_data($this->agent->browser(), 'User Browser = ');
        $this->myauthorize->authorize_fallback_data(json_encode($payload_data), 'Purchase product card Checkout');
      
        $expdata=$this->input->post('exp_month').$this->input->post('exp_year');
        
        $dataCustomers = array(
            "cnumber" => $this->input->post('card_num'),
            "cexpdate" => $expdata,
            "ccode" => $this->input->post('cvv'),
            "amount" => $this->input->post('total')
        );
        $result = $this->myauthorize->chargerCreditCard($dataCustomers);
        if(!empty($result)) {
            $this->payment_success($result,$user_id,$total,$payment_method,$fromwallet,$card_data);
        }
    }
    public function payment_success($transaction_id,$user_id,$total,$payment_method,$fromwallet,$card_data) {
        
       //update user data
        if ($user_id != 0) {
            $rem_bal = $this->session->userdata('total_balance') - $fromwallet;
            $update_data['total_balance'] = number_format((float) $rem_bal, 2, '.', '');
            $this->General_model->update_data('user_detail', $update_data, array('user_id' => $user_id));
        }

        
        $plc_ckordr = array(
            'user_id' => $user_id,
            'wallet_deducted' => $fromwallet,
            'card_deducted' => $total,
            'payerid' => '',
            'transactionid' => '',
            'transaction_id' => $transaction_id,
            'payment_method' => $payment_method=($payment_method == 'card') ? 3 : 4,
            'card_data'=>$card_data,
            
        );
        $this->place_ckout_order($plc_ckordr);
    }
}