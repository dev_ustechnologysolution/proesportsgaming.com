<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Cron extends My_Controller {
  function __construct() {
    parent::__construct();
		$this->load->model('User_model');
    $this->target_dir=BASEPATH.'../upload/game/';
  }
  function paypal_remaining_credits() {
		// date_default_timezone_set('UTC');  	
	  // 	$start = date('Y-m-d\TH:i:s\Z', strtotime('-2 day'));
	  // 	$end = date('Y-m-d\TH:i:s\Z');
	  // 	$get_playedgrp_data = $this->General_model->view_data('payment', array(
	  //     'payment_date > ' => $start,
	  //     'payment_date < ' => $end,
	  //     'payment_status' => 'due'
	  //   ));

  	date_default_timezone_set('GMT');
  	$arr['startfrom'] = date('Y-m-d\TH:i:s\Z', strtotime('-5 minutes'));
  	$arr['toend'] = date('Y-m-d\TH:i:s\Z');
  	$croncall_started = date('Y-m-d\TH:i:s\Z');
    // if (!empty($get_playedgrp_data)) {
		$searchdata = $this->Paypal_payment_model->get_transactions($arr);
		$response = $searchdata['data']['SEARCHRESULTS'];
		if (!empty($response)) {
			$email_data = $remaing_paypal_credited = [];
			foreach ($response as $key => $rp) {
				$arr['TRANSACTIONID'] = $rp['L_TRANSACTIONID'];
				$get_transaction_details = $this->Paypal_payment_model->get_transaction_details($arr);
				if (!empty($get_transaction_details['data']) && !empty(json_decode($get_transaction_details['data']['CUSTOM'])) && strtolower($get_transaction_details['data']['PAYMENTSTATUS']) == 'completed') {						
					$get_pckg_detail = json_decode($get_transaction_details['data']['CUSTOM']);
					$get_orderid = $get_transaction_details['data']['L_NAME0'];

					$amount_to_add = (property_exists($get_pckg_detail,'amount_tobe_added')) ? $get_pckg_detail->amount_tobe_added : $get_pckg_detail->amount-$get_pckg_detail->collected_fees;

					$this->db->select('p.*, ud.total_balance, ud.account_no, ud.name');
					$this->db->from('payment p');
					$this->db->join('user_detail ud','ud.user_id = p.user_id');
					$this->db->where('p.user_id', $get_pckg_detail->user_id);
					$this->db->where('p.order_id', $get_orderid);
					$this->db->where('p.payment_status', 'due');
					$our_paypal_data = $this->db->get()->row();
					
					if (!empty($our_paypal_data)) {
						$remaing_paypal_credited = $update_paypal_data = array(
							'transaction_id'	=>	$get_transaction_details['data']['TRANSACTIONID'],
							'payer_email'			=>	$get_transaction_details['data']['EMAIL'],
							'payment_status'	=>	'completed',
							'amount'					=> 	$get_pckg_detail->amount,
							'amount'					=>	$get_transaction_details['data']['AMT'],
							'currency'				=>	$get_transaction_details['data']['CURRENCYCODE'],
							'collected_fees'  => 	$get_pckg_detail->collected_fees,
							'points' 					=> 	$get_pckg_detail->points,
							'package_id' 			=> 	$get_pckg_detail->package_id
						);
						
						$updateBalance = number_format($our_paypal_data->total_balance + $amount_to_add,2,".","");
						$remaing_paypal_credited['name'] = $our_paypal_data->name;
						$remaing_paypal_credited['account_no'] = $our_paypal_data->account_no;

						$email_data['remaing_paypal_credited'][] = $remaing_paypal_credited;

						$update = $this->General_model->update_data('payment', $update_paypal_data, array('order_id' => $get_orderid, 'user_id' => $get_pckg_detail->user_id));

						if ($update) {
	    				$updatedPoints = array(
								'updated_time' => date('Y-m-d H:i:s'),
								'total_balance' => $updateBalance,
							);
							$this->General_model->update_data('user_detail', $updatedPoints, array('user_id'=>$get_pckg_detail->user_id));
						}
			    }
				}
			}
			$croncall_end = date('Y-m-d\TH:i:s\Z');
			if (!empty($email_data['remaing_paypal_credited'])) {
				$body = '<html>
					<body>
						<div style="width: 100%; text-align: center;">
							<div style="border: 1px solid #ff5000; padding: 0 15px;">
							    <div style="background-color: #ff5000; color: #fff;">
							    	<h2 style="text-align: center;">Remaining Paypal Credits</h2>
							    </div>
							    <div>
								    <table cellpadding="0" cellspacing="0" border="1" width="600" style="width: 100%;">
                      <tr>
                          <th style="width: 10%; text-align: left; padding: 10px;"> Account No </th>
                          <th style="width: 20%; text-align: left; padding: 10px;"> Name </th>
                          <th style="width: 30%; text-align: left; padding: 10px;"> Payer Email </th>
                          <th style="width: 10%; text-align: left; padding: 10px;"> Amount </th>
                          <th style="width: 30%; text-align: left; padding: 10px;"> Transaction ID </th>
                      </tr>';
                    	foreach ($email_data['remaing_paypal_credited'] as $k => $rpc) {
                    		$body .= '<tr>
                    			<td>'.$rpc['account_no'].'</td>
                    			<td>'.$rpc['name'].'</td>
                    			<td>'.$rpc['payer_email'].'</td>
                    			<td>'.$amount_to_add.'</td>
                    			<td>'.$rpc['transaction_id'].'</td>
                    		</tr>';
                    	}
                    	$body .=' </table>
                	</div>
                	<div>cron started : '.$croncall_started.' <br> cron end : '. $croncall_end.' </div>
								</div>
							</div>
						</div>
					</body>
				</html>';
				$host_split = explode('.',$_SERVER['HTTP_HOST']);
				$email = 'amit@aspireedge.com';
				$name = "Mehul vaghela";
				$subject = "Paypal Remaining Credits From ".$host_split[0]." Site";
				$this->General_model->sendMail($email,$name,$subject,$body);
  			echo "Paypal Crediteds";
  			exit();
  		}
		}
		$croncall_end = date('Y-m-d\TH:i:s\Z');
		$message = "Call for paypal remaining payment check \n================== \n";
		$message .= 'Call Started : '.$croncall_started."\n";
		if (!empty($get_transaction_details)) {
			$message .= 'Get Transaction Details :'.json_encode($get_transaction_details)."\n";
		}
		$message .= 'Call Ended :'.$croncall_end."\n\n\n\n\n";
		$file = BASEPATH.'../croncall.log';
		$method = (file_exists($file)) ? 'a' : 'w';
		$fh = fopen($file,$method);
		if (fwrite($fh, $message."\n")) {
			echo "success";
			exit();
		};	
		// }
  }
  public function index(){
		$where = array(
			'status' =>  '1',
		);

		$gameListDp = $this->General_model->view_data('game',$where);
		foreach($gameListDp as $gameList){
			if ($gameList['game_timer'] != '' && $gameList['game_timer']  != "0000-00-00 00:00:00" ) {
				if (strtotime($gameList['game_timer']) < time()) {
					echo $gameList['price'];
					$user_data_where = array(
						'user_id'	=> $gameList['user_id'],
					);
					$userListDp = $this->General_model->view_data('user_detail',$user_data_where);
					foreach($userListDp as $userList) {
						$user_where = array(
							'user_id'	=> $userList['user_id'],
						);
						$update_user = array(
							'total_balance'	=> $gameList['price'] + $userList['total_balance'],
						);
						$this->General_model->update_data('user_detail',$update_user,$user_where);
						echo $this->db->last_query();
					}
					$update_game = array(
						'status'	=> '0',
					);
					$update_where =	array(
						'id'		=> $gameList['id']
					);
					$this->General_model->update_data('game',$update_game,$update_where);
				} else {
					
				}
			}			
		}
  }
	function game_won_lose(){
		$list = $this->General_model->url_info();
		
		if(!empty($list)){
			foreach($list as $lis) {
				$listuser 	= explode(',',$lis['bet_user_id']);
				$liststatus = explode(',',$lis['v_status']);
				$listvideo 	= explode(',',$lis['v_video_id']);
				
				if($listuser[0] != '' && $listuser[1] != ''){
					
					if($liststatus[0] == '1' && $liststatus[1]  == '1'){
						echo 'Both Same';
					} else  if($liststatus[0] == '0' && $liststatus[1]  == '0'){
						echo 'Both Same';
					} else {
						
						if($liststatus[0] == '1'){
							$user_id_win	= $listuser[0];
							$video_id		= $listvideo[0];
							$user_id_lose 	= $listuser[1];
						} else {
							$user_id_win 	= $listuser[1];
							$video_id		= $listvideo[1];
							$user_id_lose	= $listuser[0];
						}
							
						$dt=array(
							'status'	=> 3,
							'video_id'	=> $video_id,
						);
						$this->General_model->update_data('game',$dt,array('id'=>$lis['game_tbl_id']));						
						$arr_bet_id = $this->General_model->view_data('game',array('id'=>$lis['game_tbl_id']));						
						$arr_bet_data = $this->General_model->view_data('bet',array('id'=>$arr_bet_id[0]['bet_id']));
						$data	=	array('winner_id'=>$user_id_win,'loser_id'=>$user_id_lose,'win_lose_deta'=>date('Y-m-d H:i:s',time()));
						$r		=	$this->General_model->update_data('bet',$data,array('id'=>$arr_bet_id[0]['bet_id']));

						$user_data_where = array(
							'user_id'	=> $user_id_win,
						);
						$userListDp = $this->General_model->view_data('user_detail',$user_data_where);
						foreach($userListDp as $userList){
							$user_where = array(
								'user_id'	=> $userList['user_id'],
							);
							$update_user = array(
								'total_balance'	=> $arr_bet_id[0]['price'] + $userList['total_balance'],
							);
							$this->General_model->update_data('user_detail',$update_user,$user_where);
						}
					}
				}
			}
		}	
	}
	public function winlist_cron(){
		$user_list = $this->User_model->user_view_admin();
		if(!empty($user_list)){
			foreach($user_list as $user_li){
				$won_sum[]	= '';
				$lost_sum[]	= '';
				$withdrawal_sum[]	= '';
				
				$win_amount = '';
				$lost_amount = '';
				$with_amount = '';
				
				$won_array = $this->User_model->get_all_won_amt($user_li['id']);
				if(!empty($won_array)){
					foreach($won_array as $won_arr){
						$won_sum[]	= $won_arr['price'];
					}
					$win_amount = array_sum($won_sum);
					unset($won_sum);
				}
				
				$lost_array = $this->User_model->get_all_lost_amt($user_li['id']);
				if(!empty($lost_array)){
					foreach($lost_array as $lost_arr){
						$lost_sum[]	= $lost_arr['price'];
					}
					$lost_amount =  array_sum($lost_sum);
					unset($lost_sum);
				}
				
				$withdrawal_array = $this->User_model->get_all_withdrawal_amt($user_li['id']);
				if(!empty($withdrawal_array)){
					foreach($withdrawal_array as $withdrawal_arr){
						$withdrawal_sum[]	= $withdrawal_arr['amount_paid'];
					}
					$with_amount = array_sum($withdrawal_sum);
					unset($withdrawal_sum);
				}
					
				$data =array(
					'won_amt' 			=> $win_amount,
					'lost_amt' 			=> $lost_amount,
					'withdrawal_amt' 	=> $with_amount,
				);
				
				$data_where = array(
					'user_id'	=> $user_li['id'],
				);
				
				$this->General_model->update_data('user_detail',$data,$data_where);
			}
		}
	}
	public function get_transaction_detail()	{
		if (!empty($_GET['transaction_id'])) {
			$arr['TRANSACTIONID'] = $_GET['transaction_id'];
			$get_transaction_details = $this->Paypal_payment_model->get_transaction_details($arr);
			echo "<pre>";
			print_r($get_transaction_details);
			exit();
		}
	}

	public function sync_stream()	{
		$this->db->select('u.id, u.ban_option, ud.common_settings, ss.default_stream_channel');
	    $this->db->from('user u');
	    $this->db->join('user_detail ud', 'u.id = ud.user_id');
	    $this->db->join('stream_settings ss', 'u.id = ss.user_id');
	    $this->db->where('ss.key_option', 'default_obs_stream');
	    $this->db->where('u.ban_option', 0);
	    $this->db->order_by('ud.account_no','asc');
        $query = $this->db->get();
        $data = array();
        if($query !== FALSE && $query->num_rows() > 0){
            foreach ($query->result_array() as $row) {
            	$common_settings = json_decode($row['common_settings']);
            	if($common_settings->auto_sync_stream == 'on'){
            		$this->db->select('lf.id, lf.obs_stream_channel, lf.lobby_id');
					$this->db->from('lobby_fans lf');
					$this->db->join('group_bet gb','gb.lobby_id = lf.lobby_id');
					$this->db->join('lobby l','l.id = lf.lobby_id AND l.user_id = lf.user_id');
					$this->db->where('NOT ((lf.obs_stream_channel IS NULL OR lf.obs_stream_channel = ""))');
					$this->db->where('gb.bet_status', 1);
					$this->db->where('l.status', 1);
					$this->db->where('l.is_archive', 0);
					$this->db->where('lf.stream_type', 2);
					$this->db->where('l.user_id', $row['id']);
					$this->db->order_by('l.created_at','asc');
					$query = $this->db->get();
					$lobbies = $query->result();
					if(count($lobbies) > 0){
						foreach ($lobbies as $value) {
							if($value->obs_stream_channel != $row['default_stream_channel']){
								$update_where = array(
									'id'	=> $value->id,
								);
								$update_data = array(
									'obs_stream_channel'	=> $row['default_stream_channel'],
								);
								$this->General_model->update_data('lobby_fans',$update_data,$update_where);
								$res = ['lobby_fan_id' => $value->id, 'lobby_id' => $value->lobby_id, 'old_channel' => $value->obs_stream_channel, 'updated_channel' => $row['default_stream_channel']];
								echo '<pre>';
								print_r($res);
							}
						}
					}
                }
            }
        }
	}
}