<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include BASEPATH.'../application/controllers/My_Controller.php';

class Dashboard extends My_Controller {

	function __construct(){
		parent::__construct();
		$this->check_user_page_access();
		$this->load->model('User_model');
		$this->target_dir=BASEPATH.'../upload/profile_img/';
	}
	public function index() {
		
		$data['dt']=1;
		if (MATCH_MODULE == 'on') {
			$data['list'] = $this->General_model->open_challenge_game_list();
			$data['list1'] = $this->General_model->playing_game_list();
			
			$data['friend_challange_list'] = $this->General_model->friend_challange_list();
			for($i=0; $i<count($data['list1']); $i++) {
				$data['video_status'][$i] = $this->General_model->get_num_rows('video', array('game_tbl_id' => $data['list1'][$i]['id'], 'game_id' => $data['list1'][$i]['game_id'], 'user_id' => $this->session->userdata('user_id')));
			}
		}
		
		
		$data['get_my_lobby_list'] = $this->General_model->get_my_lobby_list($this->session->userdata('user_id'),$fans_count='yes');
		
		$data['created_lobbys'] = $this->General_model->created_lobbys($this->session->userdata('user_id'));

		$data['list2']=$this->General_model->winning_game_list();
		$data['list3']=$this->General_model->played_game_list();
		
		$data['list11']=$this->General_model->view_data('game',array('user_id'=>$this->session->userdata('user_id'),'status'=>1,'payment_status'=>1));
		$data['list4']=$this->General_model->accepter_game_list();
		$data['list6']=$this->General_model->accepter_old_game_list();
		$data['list5']=$this->General_model->view_data('game',array('user_id'=>$this->session->userdata('user_id'),'status'=>3));
		$content=$this->load->view('dashboard/dashboard.tpl.php',$data,true);
		$this->render($content);	
	}

	public function cancelChallenge($gameId){
		$get_price_game = $this->General_model->view_data('game',array('id'=>$gameId));
		
		$points = $this->General_model->view_data('user_detail',array('user_id'=>$this->session->userdata('user_id')));
		$percentage = $this->General_model->view_all_data('admin_points_percentage','id','asc');                        
		$lessthan5_total 		= $percentage[0]['lessthan5'];	
		$morethan5_25_total 	= $percentage[0]['morethan5_25'];	
		$morethan25_50_total 	= $percentage[0]['morethan25_50'];	
		$morethan50_total		= $percentage[0]['morethan50'];	
		
		$price_total = $get_price_game[0]['price'];
		if($price_total < 5){
			$price_add = $price_total + $lessthan5_total;
		}else if($price_total >= 5 && $price_total < 25){
			$price_add = $price_total + $morethan5_25_total;
		} else if($price_total >= 25 && $price_total < 50){
			$price_add = $price_total + $morethan25_50_total;
		} else if($price_total >= 50){
			$price_add = $price_total + $morethan50_total;
		}
		$price_add_decimal = number_format((float)$price_add, 2, '.', '');
		
		$old_balance=$this->General_model->view_data('user_detail',array('user_id'=>$get_price_game[0]['user_id']));
		$update_balance	= $old_balance[0]['total_balance'] + $price_add_decimal;
		$data_Where = array(
			'total_balance'	=>	$update_balance,
		);
		$this->General_model->update_data('user_detail',$data_Where,array('user_id'=>$get_price_game[0]['user_id']));
	
		$data = array(
			'status'=> 0
		);
		$this->General_model->update_data('game',$data,array('id'=>$gameId));
		$this->session->set_flashdata('canceled', 'Challenge successfully cancelled.');
		redirect(base_url().'dashboard'); 
		exit;
	}

}

