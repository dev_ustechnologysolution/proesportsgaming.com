<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH . '../application/controllers/My_Controller.php';
class Giveaway extends My_Controller {
  public function __construct() {
    parent::__construct();
    $this->check_cover_page_access();
    $this->check_user_page_access();
    $this->target_allimg_dir = BASEPATH.'../upload/all_images/';
    $this->target_stream_settings_dir = BASEPATH.'../upload/stream_setting/';
  }
  function _remap($method,$args) {
    (method_exists($this, $method)) ? ((count($args) < 2) ? $this->$method($args[0]) : redirect(base_url())) : $this->index($method,$args);
  }
  public function index($lobby_id) {
    // $data['view_lby_det'] = $this->General_model->lobby_bet_detail($lobby_id)[0];
    $data['giveaway_data'] = $this->Giveaway_model->get_ga_data(array('id' => $lobby_id));

    $data['lobbycreator'] = $data['giveaway_data'][0]['crtr'];
    $data['fanscount'] = $this->General_model->fanscount($lobby_id);
    $get_my_fan_id = array_search($this->session->userdata('user_id'),array_column($data['fanscount'], 'user_id'));
    (!$get_my_fan_id && $data['lobbycreator'] != $this->session->userdata('user_id')) ? redirect(base_url().'livelobby/watchLobby/'.$lobby_id) : '';
    $data['get_my_fan_detail'] = $data['fanscount'][$get_my_fan_id];
    $data['getcrtr_lobby_detail'] = $this->General_model->get_my_fan_tag($lobby_id,$data['lobbycreator']);
    // $data['view_lobby_fan_profile'] = $this->User_model->get_user_detail($data['lobbycreator']);
    // $data['view_lobby_fan_profile']->id = $data['lobbycreator'];
    $follower_data = $this->User_model->getfollower_and_following($data['lobbycreator']);
    $data['followercount'] = $follower_data->followercount;
    $data['is_followed'] = (in_array($this->session->userdata('user_id'), explode(',', $follower_data->followers_arr))) ? 'following' : 'follow';
    $arr = array(
      'lobby_id' => $lobby_id,
    );
    $data['lobby_chat'] = $this->Giveaway_model->get_ga_chat($arr);
    // $data['default_stream'] = $this->General_model->get_stream_setting($data['lobbycreator'],'','default_stream')[0];
    if (empty($data['giveaway_data'])) {
      redirect(base_url().'livelobby/watchLobby/'.$lobby_id);
    }
    $data['giveaway_users'] = $this->Giveaway_model->giveaway_all_users(array('id' => $data['giveaway_data'][0]['gw_id']));

    // Global Giveaway Fee
    $global_gafees = $this->General_model->view_data('site_settings', array('option_title' => 'global_giveaway_fees'))[0]['option_value'];
    $data['global_giveawayfees'] = (!empty($global_gafees)) ? explode('_', $global_gafees) : '';

    $data['global_dst_hour'] = $this->General_model->view_data('site_settings', array('option_title' => 'global_dst_hour'))[0];
    // Get the Daylight option is on for user
    $data['global_dst_option'] = $this->General_model->view_data('site_settings', array('option_title' => 'global_dst_option'))[0];
     
    $content = $this->load->view('giveaway/giveaway.tpl.php', $data, true);
    $this->render($content);
  }
  public function giveaway_setting($lobby_id) {
    if (!empty($_POST)) {
      $data = array(
        'lobby_id' => $lobby_id,
        'title' => trim($_POST['ga_title']),
        'amount' => number_format((float) $_POST['amount'], 2, '.', ''),
        'user_id' => $this->session->userdata('user_id'),
        'entrieslimit' => trim($_POST['entrieslimit']),
        'description' => trim($_POST['description']),
        'is_multiple_entries' => ($_POST['is_multiple_entries'] == 'on') ? 1 : 0,
        'date_time_of_giveaway' => date('m/d/Y H:i:s', strtotime($_POST['datetime_giveaway'])),
        'fireworks_duration_time' => $_POST['fireworks_duration_seconds'],
        'date_time_registration_close' => date('m/d/Y H:i:s', strtotime($_POST['registration_close_time'])),
      );
      
      $data['use_custom_fireworks_audio'] = ($_POST['use_custom_fireworks_audio'] == 'on') ? 1 : 0;
      if (empty($_POST['gw_id'])) {
        $default_stream = $this->General_model->get_stream_setting($this->session->userdata('user_id'),'','default_stream')[0];
        if (!empty($default_stream)) {
          $data['default_stream'] = ($default_stream->default_stream_channel != '') ? $default_stream->default_stream_channel : '';
          $data['default_stream_showstatus'] = ($default_stream->default_stream_channel != '') ? 1 : 0;
        }
      }
      if (!empty($_FILES['giveaway_img']) && !empty($_FILES['giveaway_img']['name'])) {
          $img = time().basename($_FILES["giveaway_img"]["name"]);
          $ext = strtolower(end((explode(".", $img))));
          if (!in_array(strtolower($ext), array('gif','jpeg','jpg', 'png'))){
            $this->session->set_flashdata('message_err', 'Please select valid image');
            redirect($_SERVER['HTTP_REFERER']);
          }
          $data['img'] = $img;
          if (!move_uploaded_file($_FILES["giveaway_img"]["tmp_name"], $this->target_allimg_dir.$img)) {
            $this->session->set_flashdata('message_err', 'Something wrong');
            $res['updated'] = 0;
            echo json_encode($res);
            exit();
          }
      }
      if (!empty($_FILES['fireworks_audio']) && !empty($_FILES['fireworks_audio']['name'])) {
        $fa = preg_replace('/[^a-zA-Z0-9_.]/', '_', $_FILES["fireworks_audio"]["name"]);
        $fireworks_audio = time().basename($fa);
        $ext = strtolower(end((explode(".", $fireworks_audio))));
        if (!in_array(strtolower($ext), array('mp3'))){
          $this->session->set_flashdata('message_err', 'Please select valid audio file');
          $res['updated'] = 0;
          echo json_encode($res);
          exit();
        }
        $data['fireworks_audio'] = $fireworks_audio;
        if (!move_uploaded_file($_FILES["fireworks_audio"]["tmp_name"], $this->target_stream_settings_dir.$fireworks_audio)) {
          $this->session->set_flashdata('message_err', 'Something wrong');
          $res['updated'] = 0;
          echo json_encode($res);
          exit();
        }
      }

      $get_exist_giveaway_data = $this->Giveaway_model->get_ga_data(array('id' => $lobby_id));

      $r =  (!empty($_POST['gw_id']) || !empty($get_exist_giveaway_data)) ? $this->General_model->update_data('giveaway', $data, array('id' => $_POST['gw_id'])) : $this->General_model->insert_data('giveaway', $data);

      if ($r) {
        $res['updated'] = $data;
        $this->session->set_flashdata('message_succ', 'Successfully Update');
      } else {
        $res['updated'] = 0;
        $this->session->set_flashdata('message_err', 'Unable to Update');
      }
      echo json_encode($res);
      exit();
    }
  }
  public function resetgiveaway() {
    if (!empty($_POST['lobby_id'])) {
      $update_data['reset'] = 1;
      $result['updated_where'] = array('lobby_id' => $_POST['lobby_id']);
      $r = $this->General_model->update_data('giveaway', $update_data, $result['updated_where']);
      if ($r) {
        $this->session->set_flashdata('message_succ', 'Giveaway Reset successfully');
        $result['updated'] = true;
        echo json_encode($result);
        exit();
      }
    }
  }
  public function join_giveaway() {
    if (!empty($_POST['giveaway_amount']) && !empty($_POST['giveaway_id']) && !empty($_POST['lobby_id'])) {
      $result['giveaway_data'] = $this->General_model->view_data('giveaway', array('id' => $_POST['giveaway_id']));
      $result['lobby_data'] = $this->General_model->view_data('lobby', array('id' => $_POST['lobby_id']));
      
      $global_gafees = $this->General_model->view_data('site_settings', array('option_title' => 'global_giveaway_fees'))[0];
      $global_gafees = explode('_', $global_gafees['option_value']);
      
      $ga_feeclctr = $this->General_model->view_data('site_settings', array('option_title' => 'global_giveaway_fee_collector'))[0]['option_value'];
      $ga_feeclctr = (!empty($ga_feeclctr)) ? $ga_feeclctr : 0;
      $ga_fee_clctr_detail = $this->General_model->view_single_row('user_detail', array('account_no' => $ga_feeclctr),'*');

      $get_ga_userdata = $this->General_model->view_data('giveaway_users', array('giveaway_id' => $_POST['giveaway_id'], 'lobby_id' => $_POST['lobby_id']),'*');

      if (count($get_ga_userdata) >= $result['giveaway_data'][0]['entrieslimit']) {
        $this->session->set_flashdata('message_err', 'Giveaway Full.');
        $result['insert_data'] = false;
        echo json_encode($result);
        exit();
      }
      $get_my_id = array_search($this->session->userdata('user_id'),array_column($get_ga_userdata,'user_id'));

      $deduction = $result['giveaway_data'][0]['amount'];

      $total_deduction = (isset($_POST['another_entries']) && !empty($_POST['another_entries'])) ? number_format((float) $deduction * $_POST['another_entries'], 2, '.', '') : $deduction;

      if ($total_deduction > $this->session->userdata('total_balance')) {
        $this->session->set_flashdata('message_err', 'You have insufficient balance.');
        $result['insert_data'] = false;
        echo json_encode($result);
        exit();
      }      
      if (isset($_POST['another_entries']) && !empty($_POST['another_entries'])) {
        $result['another_entries'] = $_POST['another_entries'];
        if (is_numeric($result['another_entries'])) {
          $err = [];
          if (($result['another_entries']+count($get_ga_userdata)) > $result['giveaway_data'][0]['entrieslimit']) {
            $this->session->set_flashdata('message_err', "You can't enter more than ".($result['giveaway_data'][0]['entrieslimit']-count($get_ga_userdata))." Tickets");
            $result['insert_data'] = false;
            echo json_encode($result);
            exit();
          }
          // Giveaway Calculation
          $result['ga_fee_collector_cr_amnt'] = ($global_gafees[0] == 1) ? number_format((float) $result['another_entries'] * $global_gafees[1], 2, '.', '') : number_format((float) $result['another_entries'] * ($result['giveaway_data'][0]['amount']*$global_gafees[1]/100), 2, '.', '');
          $result['lbycrtr_cr_amnt'] = $total_deduction - $result['ga_fee_collector_cr_amnt'];

          for ($i = 1; $i <= $result['another_entries']; $i++) { 
            $insert_data = array(
              'lobby_id' => $get_ga_userdata[$get_my_id]['lobby_id'],
              'giveaway_id' => $get_ga_userdata[$get_my_id]['giveaway_id'],
              'user_id' => $this->session->userdata('user_id'),
              'deducted_amount' => $deduction,
              'giveaway_tag' => $get_ga_userdata[$get_my_id]['giveaway_tag'],
            );
            $r = $this->General_model->insert_data('giveaway_users', $insert_data);
            (!$r) ? $err[] = 'Something went wrong' : '';
          }
        }
      } else if (isset($_POST['giveaway_tag']) && !empty($_POST['giveaway_tag'])) {
        // Giveaway Calculation
        $result['ga_fee_collector_cr_amnt'] = ($global_gafees[0] == 1) ? number_format((float) $global_gafees[1], 2, '.', '') : number_format((float) $result['giveaway_data'][0]['amount']*$global_gafees[1]/100, 2, '.', '');
        $result['lbycrtr_cr_amnt'] = $total_deduction - $result['ga_fee_collector_cr_amnt'];

        $result['insert_data'] = array(
          'lobby_id' => $_POST['lobby_id'],
          'giveaway_id' => $_POST['giveaway_id'],
          'user_id' => $this->session->userdata('user_id'),
          'deducted_amount' => $deduction,
          'giveaway_tag' => preg_replace('/[^A-Za-z0-9. _-]/', '', $_POST['giveaway_tag']),
        );
        $r = $this->General_model->insert_data('giveaway_users', $result['insert_data']);
        (!$r) ? $err[] = 'Something went wrong' : '';
      }
      if (!empty($err)) {
        $this->session->set_flashdata('message_err', 'Something went wrong.');
        redirect($_SERVER['HTTP_REFERER']);
      }

      // Giveaway ticket deduction
      $this->db->set("total_balance", "total_balance - ".$total_deduction, FALSE)->where('user_id', $this->session->userdata('user_id'))->update('user_detail');
      $get_userdata = $this->General_model->view_single_row('user_detail', array('user_id' => $this->session->userdata('user_id')),'total_balance');
      $result['updated_balance'] = number_format((float) $get_userdata['total_balance'], 2, '.', '');
      $this->session->set_userdata('total_balance', $result['updated_balance']);

      // Giveaway ticket fee credit
      $this->db->set("total_balance", "total_balance + ".$result['ga_fee_collector_cr_amnt'], FALSE)->where('user_id', $ga_fee_clctr_detail['user_id'])->update('user_detail');
      $ga_fee_clctr = $this->General_model->view_single_row('user_detail', array('user_id' => $ga_fee_clctr_detail['user_id']),'total_balance');
      $result['ga_fee_clctor_arr']['user_id'] = $ga_fee_clctr_detail['user_id'];
      $result['ga_fee_clctor_arr']['total_balance'] = number_format((float) $ga_fee_clctr['total_balance'], 2, '.', '');

      // Giveaway ticket amnt credit
      $this->db->set("total_balance", "total_balance + ".$result['lbycrtr_cr_amnt'], FALSE)->where('user_id', $result['lobby_data'][0]['user_id'])->update('user_detail');
      $ga_amnt_clctr = $this->General_model->view_single_row('user_detail', array('user_id' => $result['lobby_data'][0]['user_id']),'total_balance');
      $result['ga_amnt_clctor_arr']['user_id'] = $result['lobby_data'][0]['user_id'];
      $result['ga_amnt_clctor_arr']['total_balance'] = number_format((float) $ga_amnt_clctr['total_balance'], 2, '.', '');

      $result['get_img'] = ($get_userdata['image'] != null && !empty($get_userdata['image']) && file_exists(BASEPATH.'../upload/profile_img/'.$get_userdata['image'])) ? $get_userdata['image'] : 'placeholder-image.png';;
      $result['inserted'] = $r;
      echo json_encode($result);
      exit();
    }
  } 
  public function edit_giveaway_tag() {
    if (!empty($_POST['giveaway_tag']) && !empty($_POST['giveaway_id']) && !empty($_POST['lobby_id'])) {
      $update_data['giveaway_tag'] = preg_replace('/[^A-Za-z0-9. _-]/', '', $_POST['giveaway_tag']);
      $result['updated_where'] = array(
        'lobby_id' => $_POST['lobby_id'],
        'giveaway_id' => $_POST['giveaway_id'],
        'user_id' => $this->session->userdata('user_id')
      );
      $r = $this->General_model->update_data('giveaway_users', $update_data, $result['updated_where']);
      if ($r) {
        $result['updated'] = $r;
        echo json_encode($result);
        exit();
      }
    }
  }
  public function giveaway_winner() {
    if (!empty($_POST['user_id']) && !empty($_POST['giveaway_id']) && !empty($_POST['lobby_id']) && !empty($_POST['giveaway_tag'])) {
      // $is_winner_exist = $this->General_model->view_single_row('giveaway_winners', array('user_id' => $_POST['user_id'], 'giveaway_id' => $_POST['giveaway_id'], 'lobby_id' => $_POST['lobby_id']),'id');
      // if (empty($is_winner_exist)) {
        $result['insert_data'] = array(
          'giveaway_id' => $_POST['giveaway_id'],
          'lobby_id' => $_POST['lobby_id'],
          'user_id' => $_POST['user_id'],
          'giveaway_tag' => $_POST['giveaway_tag'],
        );
        $r = $this->General_model->insert_data('giveaway_winners', $result['insert_data']);
      // } else {
      //   $result['is_exist'] = 1;
      // }
      echo json_encode($result);
      exit();
    }
  }
  public function change_ga_stream_status() {
    if (!empty($_POST['change_status']) && !empty($_POST['giveaway_id']) && !empty($_POST['lobby_id'])) {
      $result['update_data'] = array('default_stream_showstatus' => ($_POST['change_status'] == 'enable') ? 1 : 0 );

      (!empty($_POST['ga_default_stream'])) ? $result['update_data']['default_stream'] = $_POST['ga_default_stream'] : '';
      $result['updated_where'] = array(
        'id' => $_POST['giveaway_id'],
        'lobby_id' => $_POST['lobby_id']
      );
      $r = $this->General_model->update_data('giveaway', $result['update_data'], $result['updated_where']);
      if ($r) {
        $result['giveaway_data'] = $this->General_model->view_data('giveaway', array('id' => $_POST['giveaway_id']))[0];
        $result['updated'] = $r;
        echo json_encode($result);
        exit();
      }
    }
  }
  public function send_message_to_giveaway_grp() {
    $this->check_user_page_access();
    if (isset($_POST)) {
      $message = $this->General_model->replace_url_to_anchor_tag($_POST['message']);
      if (isset($_FILES['file']) && $_FILES['file'] != '' && $_POST['lobby_id'] != '' && $_POST['user_id'] != '') {
        for($i = 0; $i < count($_FILES["file"]["name"]); $i++) {
          if (isset($_FILES["file"]["name"][$i]) && $_FILES["file"]["name"][$i]!='') {
            $img = time().basename($_FILES["file"]["name"][$i]);
            $ext = end((explode(".", $img)));
            if(in_array(strtolower($ext),array('jpeg','png','jpg','gif'))) {
              $target_file = $this->target_allimg_dir.$img;
              move_uploaded_file($_FILES["file"]["tmp_name"][$i], $target_file);
              $files_name[] = $img;
            }
          }
        }
        $files_name = implode(',', $files_name);
        $insert_data = array(
          'attachment' => $files_name,
          'message_type' => 'file'
        );
        $message_id  = $this->General_model->insert_data('xwb_messages', $insert_data);
      } else {
        $insert_data = array(
          'message' => $message,
          'message_type' => 'message'
        );
        $message_id = $this->General_model->insert_data('xwb_messages', $insert_data);
      }
      if ($message_id) {
        $lobby_insert_data = array(
          'lobby_id' => $_POST['lobby_id'],
          'user_id' => $_POST['user_id'],
          'giveaway_id' => $_POST['giveaway_id'],
          'message_id' => $message_id,
          'status' => 1,
          'date' => date('Y-m-d H:i:s', time()),
        );
        $lobby_group_conversation_id = $this->General_model->insert_data('giveaway_group_conversation', $lobby_insert_data);
        if ($lobby_group_conversation_id) {
          $arr = array('id' => $lobby_group_conversation_id);
          $srcData['ga_get_chat'] = $this->Giveaway_model->get_ga_chat($arr)[0];
          $srcData['session_id'] = $this->session->userdata('user_id');
          $srcData['is_admin'] = $this->session->userdata('is_admin');
          echo json_encode($srcData);
          exit();
        }
      }
    }
  }
}