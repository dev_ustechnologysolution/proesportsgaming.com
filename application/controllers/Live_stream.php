<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Live_stream extends My_Controller {
	function __construct() {
		parent::__construct();
	}
	public function index() {
		if (!empty($_GET['channel'])) {
			$data['stream_key'] = $_GET['channel'];
			$new_stream_key = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(8/strlen($x)) )),1,8);
			// $data['base_url'] = (base_url() != 'https://proesportsgaming.com/') ? 'https://esportstesting.com/' : base_url();
			$data['base_url'] = base_url();
			$data['random_id'] = $new_stream_key;
			$data['user_id'] = $_GET['user_id'];
			$data['autoplay'] = (!empty($_GET['play']) && $_GET['play'] == 'false') ? '' : 'autoplay';
			if(!empty($_GET['lobby_id'])){$data['lobby_id'] = $_GET['lobby_id'];}
			if(!empty($_GET['group_bet_id'])){$data['group_bet_id'] = $_GET['group_bet_id'];}
			$this->load->view('streamsetting/get_live_stream.tpl.php',$data);
		}
	}
}