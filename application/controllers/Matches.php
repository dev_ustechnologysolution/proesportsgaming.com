<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Matches extends My_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->check_user_page_access();
        $this->load->model('My_model');
        $this->load->library('pagination');
        $this->pageLimit=20;
        $this->check_cover_page_access();
    }
    //page loading
    public function index($type='',$page=0) {
        if (MATCH_MODULE == 'on') {
            $data['dt']=1;
            // get all game under category xbox
            $start = 0;
            $offset = 5;
            /*
            $condition = "1 AND game_category_id='1' LIMIT ".$start.",".$offset;
            $data['gameList']=$this->General_model->admin_Matches_list($condition);
            */

            /****************Xbox game list*****************/
            $page_one=$page;
              
            if($type!='' && $type=='game')
            {
               $page=0;
            }
            else if($type!='' && $type=='challange')
            {
               $page_one=0;
            }
            $conditions='';
            $data['gameListDpPoser']='';
            if(isset($_POST['game_id']) && $_POST['game_id']!='')
            {
                $conditions=" AND ag.id='".$_POST['game_id']."'";
                $data['gameListDpPoser']=$_POST['game_id'];
            }

            $data['gameListDp']= $this->My_model->getMatchesTabGameChallange(-1,$page_one,$category_id=1);
            $data['gameList']= $this->My_model->getMatchesTabGameChallange($this->pageLimit,$page_one,$category_id=1,$conditions);
            if(!isset($data['gameList']['tot']))
            {
                if($page_one>=$data['gameList']['tot'] && $page_one!=0)
                {
                    $page_one=$data['gameList']['tot'] - $this->pageLimit;
                    if($page_one<0)
                        $page_one=0;
                    header('location:'.base_url().'Matches/index/game/'.$page_one);
                    exit;
                }
            }
            paggingInitialization($this,
                array('base_url'=>base_url().'Matches/index/game',
                    'total_row'=>$data['gameList']['tot'],
                    'per_page'=>$this->pageLimit,
                    'uri_segment'=>4,
                    'next_link'=>'<i class="fa fa-chevron-right"></i>',
                    'prev_link'=>'<i class="fa fa-chevron-left"></i>'
                )
            );

            $data['paging_one']= $this->pagination->create_links();

            /****************End Xbox game list*****************/

            /****************Xbox challange list****************/
            
            $conditions='';
            $data['challangeDpPoser']='';
            
            if(isset($_POST['game_name']) && $_POST['game_name']!='')
            {
                $conditions=" AND g.id='".$_POST['game_name']."'";
                $data['challangeDpPoser']=$_POST['game_name'];
            }

            if(isset($_POST['sub_game-name']) && $_POST['sub_game-name']!='')
            {
                $conditions=" AND asg.sub_game_name='".$_POST['sub_game-name']."'";
                $data['gameSubGame']=$_POST['sub_game-name'];
            }

            if(isset($_POST['game_type']) && $_POST['game_type']!='')
            {
                $conditions=" AND g.game_type='".$_POST['game_type']."'";
                $data['gameGameSize']=$_POST['game_type'];
            }

            if(isset($_POST['game_name']) && $_POST['game_name']!='' && isset($_POST['sub_game-name']) && $_POST['sub_game-name']!='')
            {
                $conditions=" AND g.id='".$_POST['game_name']."' AND asg.sub_game_name='".$_POST['sub_game-name']."'";
                $data['challangeDpPoser']=$_POST['game_name'];
                $data['gameSubGame']=$_POST['sub_game-name'];
            }

            if(isset($_POST['sub_game-name']) && $_POST['sub_game-name']!='' && isset($_POST['game_type']) && $_POST['game_type']!='')
            {
                $conditions=" AND g.game_type='".$_POST['game_type']."' AND asg.sub_game_name='".$_POST['sub_game-name']."'";
                $data['gameSubGame']=$_POST['sub_game-name'];
                $data['gameGameSize']=$_POST['game_type'];
            }        

            if(isset($_POST['game_name']) && $_POST['game_name']!=''  && isset($_POST['game_type']) && $_POST['game_type']!='')
            {
                $conditions=" AND g.id='".$_POST['game_name']."' AND g.game_type='".$_POST['game_type']."'";
                $data['challangeDpPoser']=$_POST['game_name'];
                $data['gameGameSize']=$_POST['game_type'];
            }

            if(isset($_POST['game_name']) && $_POST['game_name']!='' && isset($_POST['sub_game-name']) && $_POST['sub_game-name']!='' && isset($_POST['game_type']) && $_POST['game_type']!='')
            {
                $conditions=" AND g.id='".$_POST['game_name']."' AND asg.sub_game_name='".$_POST['sub_game-name']."' AND g.game_type='".$_POST['game_type']."'";
                $data['challangeDpPoser']=$_POST['game_name'];
                $data['gameSubGame']=$_POST['sub_game-name'];
                $data['gameGameSize']=$_POST['game_type'];
            }        
            if (isset($_POST['srch-term'])) {

                $conditions= "AND g.id LIKE '%".$_POST['srch-term']."%' OR g.payment_status='1' AND g.status='1' AND g.game_category_id='1' AND g.device_id LIKE '%".$_POST['srch-term']."%'"; 
                $data['gameSearchTerms']=$_POST['srch-term'];

                // $data['playstore_game']=$this->My_model->getPcTabGameChallange($main_conditions);           
            }
            $data['systemList']= $this->General_model->matches_system_list();
            $data['gamenameList']= $this->General_model->matches_game_name_list();
            $data['subgamenameList']= $this->General_model->matches_sub_game_name_list();
            $data['gameSize']= $this->General_model->matches_game_size();
            
            $data['challangetDp']= $this->My_model->getMatchesTabGameChallange(-1,$page,$category_id=1);
            
            $data['Matches']=$this->My_model->getMatchesTabGameChallange($this->pageLimit,$page,$category_id=1,$conditions);
            if(!isset($data['Matches']['tot']))
            {
                if($page>=$data['Matches']['tot'] && $page!=0)
                {
                    $page=$data['Matches']['tot']-$this->pageLimit;
                    if($page<0)
                        $page=0;
                    header('location:'.base_url().'Matches/index/challange/'.$page);
                    exit;
                }
            }
            paggingInitialization($this,
                array('base_url'=>base_url().'Matches/index/challange/',
                    'total_row'=>$data['Matches']['tot'],
                    'per_page'=>$this->pageLimit,
                    'uri_segment'=>4,
                    'next_link'=>'<i class="fa fa-chevron-right"></i>',
                    'prev_link'=>'<i class="fa fa-chevron-left"></i>'
                )
            );
            $data['paging_two']= $this->pagination->create_links();

            /****************End Xbox challange list****************/

            // echo '<pre>';
            // print_r($data['gameList']);exit;
            //$data['gameList']=$this->General_model->view_data('admin_game',$condition);
            // GET TOTAL NO OF ROWS
            $tot_game = $this->General_model->get_num_rows('game',array('game_category_id'=>1));
            //$tot_game = $this->General_model->get_num_rows('admin_game',array('game_category_id'=>1));
            // CALCULATE NO OF PAGES
            $tot_page = ceil($tot_game/$offset);
            $data['totGame'] = $tot_game;
            $data['totPage'] = $tot_page;
            // GET ALL CATEGORY LIST
            
            $data['banner_data'] = $this->General_model->tab_banner();
            $data['catList'] = $this->General_model->xbox_game_list('admin_game_category','id','ASC');
            $content=$this->load->view('matches/matches.tpl.php',$data,true);
            $this->render($content);
        }
    }
    // pagination function
    public function page($page_id) {
        $data['dt']=1;
        // get all game under category xbox
        $offset = 5;
        $start = ($page_id*$offset)-$offset;
        $condition = "1 AND game_category_id='1' LIMIT ".$start.",".$offset;
        $data['gameList']=$this->General_model->admin_Matches_list($condition);
        //$data['gameList']=$this->General_model->view_data('admin_game',$condition);
        // GET TOTAL NO OF ROWS

        $tot_game = $this->General_model->get_num_rows('game',array('game_category_id'=>1));
        // CALCULATE NO OF PAGES
        $tot_page = ceil($tot_game/$offset);
        $data['totGame'] = $tot_game;
        $data['totPage'] = $tot_page;
        // GET ALL CATEGORY LIST

        $data['catList'] = $this->General_model->xbox_game_list();
        $content=$this->load->view('matches/matches.tpl.php',$data,true);
        $this->render($content);
    }
}