<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';

class Membership extends My_Controller{
	public function __construct(){
        parent::__construct();       
        $this->load->model('Paypal_payment_model');
    }
    function getMembershipPlan(){
        $this->check_user_page_access();
        $options = array(
            'select' => 'pms.plan_id, pms.user_id, pms.payer_email, m.title, m.time_duration, pms.payment_date, pms.transaction_id',
            'table' => 'player_membership_subscriptions pms',
            'join' => array('membership m' => 'm.id = pms.plan_id'),
            'where' => array(
                'pms.user_id' => $this->session->userdata('user_id'),
                'pms.payment_status' => 'Active'
            ),
            'order_by' => array('pms.id' => 'desc'),
            'single' => true
        );
        $data['user_plan_data'] = $this->General_model->commonGet($options);
    	if ($data['user_plan_data']->plan_id != '') {
            $data['active_plan'] = $data['user_plan_data']->plan_id;
            $data['transaction_id'] = $data['user_plan_data']->transaction_id;
        } else {
            $data['active_plan'] = '0';
            $data['transaction_id'] = '0';
        }
        $data['plan'] = $this->General_model->view_all_data('membership','display_order','asc');
        $content = $this->load->view('membership/membershiplist.tpl.php',$data,true);
        $this->render($content);
    }
    function activeMembershipPlan(){
    	if ($_POST['membership_subscribe_plan'] !='' && $_POST['membership_subscribe_amount'] !='') {
            if (!empty($_POST['membership_payment_id']) && !empty($_POST['membership_subscribed_plan'])) {
                $_SESSION['membership_payment_id'] = $_POST['membership_payment_id'];
                $_SESSION['membership_subscribed_plan'] = $_POST['membership_subscribed_plan'];
            } else {
                unset($_SESSION['membership_payment_id']);
                unset($_SESSION['membership_subscribed_plan']);
            }

            $data['subscribe_amount'] = number_format(($_POST['membership_subscribe_amount'] + $_POST['fees']),2,".","");
            $data['subscribe_plan'] = $_POST['membership_subscribe_plan'];
            $payer_detail = $this->User_model->user_detail();
            $data['payer_email'] = $payer_detail[0]['email'];
           	$data['returnurl'] = $data['cancelurl'] = base_url().'membership/GetPaymentDetails';

            $custom_arr = array(
              'subscripltion_plan' => $_POST['membership_subscribe_plan'],
              'title' => $_POST['title'],
              'return_url' => base_url().'Membership/getMembershipPlan',
              'time_duration' => $_POST['time_duration']
            );
            $data['custom'] = json_encode($custom_arr);
            $data['title'] = $_POST['title'].' Membership Plan.';
           	$data['notifyurl'] = '';
            $data['billingtype'] = 'RecurringPayments';
            $data['noshipping'] = '0';
            $data['addroverride'] = '1';
           	$response = $this->Paypal_payment_model->Set_express_checkout($data);
            if($response['redirect'] != '') {
           		redirect($response['redirect']);
				exit();
           	} else {
           		$this->error($response['errors']);
           	}          
        }
    }
    function GetPaymentDetails($token = ''){
        if ($_GET['token']) {
            $token = $_GET['token'];
        }
    	$result = $this->Paypal_payment_model->Get_express_checkout_details($token);
    	$PayPalResult = $result['data'];
        $custom_data = json_decode($PayPalResult['CUSTOM']);
        if ($result['status'] == 1) {
            // $implode_custom_data = explode('&', $PayPalResult['CUSTOM'])
	        $plan_description = $custom_data->title.' Membership Plan.';
            $billing_duration = $custom_data->time_duration;
            date_default_timezone_set('UTC');
            $PayPalResult['TIMESTAMP'] = date("Y-m-d\Th:i:s\Z", strtotime('-10 minute', strtotime($PayPalResult['TIMESTAMP'])));
            $response = $this->Paypal_payment_model->CreateRecurringPaymentsProfile($PayPalResult,'Admin',$plan_description,$billing_duration);
            $response['data']['CUSTOM'] = $result['data']['CUSTOM'];
            $response['data']['return_url'] = $custom_data->return_url;
            if ($response['status'] == 0) {
                $this->General_model->paypal_fallback_data(json_encode($response['data']), 'Membership CreateRecurringPaymentsProfile Fail');
                $this->error($response['data']);
	        } else {
	            $this->savePlayerMembership($response['data']);
	        } 
    	} else {
            $response['data']['return_url'] = $custom_data->return_url;
    		$this->General_model->paypal_fallback_data(json_encode($result['data']), 'Get express checkout Fail');
            $this->error($result['data']);
    	}
    }    
    function savePlayerMembership($getdetails) {
        // $user_plan_data = $this->User_model->get_activated_membership($this->session->userdata('user_id'));

        $condition = array('user_id' => $this->session->userdata('user_id'),'payment_status' => 'Active');
    	$user_plan_data = $this->General_model->view_data('player_membership_subscriptions',$condition);
        if (!empty($user_plan_data)) {
            foreach ($user_plan_data as $key => $upd) {
                if (!empty($upd['transaction_id']) && $upd['transaction_id'] != '') {
                    $data['membership_payment_id'] = $upd['transaction_id'];
                    $data['membership_subscription_opt'] = 'Unbscribe';
                    $data['redirect'] = 'false';
                    $this->unsubscribeMembershipplan($data);
                }
            }
        }
        // $getdetails[] = $this->paypal_pro->GetExpressCheckoutDetails($_GET['token']); 
        $custom_data = json_decode($getdetails['CUSTOM']);
        // $implode_custom_data = explode('&', $getdetails[0]['CUSTOM']);
        $plan = $custom_data->subscripltion_plan;
        $player_subscriptions_data = array(
            'token_id' => $getdetails['REQUESTDATA']['TOKEN'],
            'transaction_id' => $getdetails['PROFILEID'],
            'user_id' => $this->session->userdata('user_id'),
            'plan_id' => $plan,            
            'validity' => '',
            'valid_from' => $getdetails['REQUESTDATA']['PROFILESTARTDATE'],
            'valid_to' => '',
            'amount' => $getdetails['REQUESTDATA']['AMT'],
            'currency_code' => $getdetails['REQUESTDATA']['CURRENCYCODE'],
            'payer_email' => $getdetails['REQUESTDATA']['EMAIL'],
            'create_at' => date('Y-m-d H:i:s', time()),
            'update_at' => date('Y-m-d H:i:s', time()),
            'payment_status' => 'Active',
            'payment_date' => $getdetails['TIMESTAMP'] 
        );        
        $r = $this->General_model->insert_data('player_membership_subscriptions', $player_subscriptions_data);
        if ($r) {
            $common_settings = json_decode($this->session->userdata('common_settings'), true);
            $common_settings['free_membership'] = 'off';
            $this->General_model->update_data('user_detail',array('common_settings' => json_encode($common_settings)),array('user_id'=>$this->session->userdata('user_id')));
            $data['getdetails'] = $getdetails;
            // $data['email'] = $getdetails['REQUESTDATA']['EMAIL'];
            $data['email'] = '';
            $data['plan'] = $custom_data->title;
            $data['return_url'] = $custom_data->return_url;
            $data['subscribe_amount'] = $getdetails['REQUESTDATA']['AMT'];
            $content=$this->load->view('membership/membership_success.tpl.php',$data,true);
            $this->render($content);
        }
    }
    function inactiveMembershipPlan(){
    	if (!empty($_POST)) {
    		$data = $_POST;
    		$this->unsubscribeMembershipplan($data);
        }
    }
    function unsubscribeMembershipplan($data){
    	$payment_id = $data['membership_payment_id'];            
        $PayPalResponseResult = $this->Paypal_payment_model->get_recurring_payments_profile_details($payment_id);
        
        $subscription_opt = $data['membership_subscription_opt'];
        if ($subscription_opt == 'Unbscribe') {
            $subscription_opt = 'Cancel';
        }
        if($PayPalResponseResult['status'] == 1){
        	$PayPalResult = $PayPalResponseResult['data'];
        	
        	if ($PayPalResult['STATUS'] != 'Cancelled') {
           	 	$this->Paypal_payment_model->Manage_recurring_payments_profile_status($payment_id,$subscription_opt);
            }
            $this->General_model->update_data('player_membership_subscriptions',array('payment_status' => 'Inactive','payment_date' => date('Y-m-d H:i:s', time())), array('transaction_id' => $payment_id));
            if(isset($data['redirect'])) {
            	return 1;
            } else {
            	redirect(base_url().'Membership/getMembershipPlan');
            }
        } else {
        	$this->error($PayPalResponseResult['data']);
        }

    }
    function error($errors){
        $data['return_url'] = $errors->return_url;
        $content = $this->load->view('membership/membership_error.tpl.php',$data, true);
        $this->render($content);
    }
    function getMembershipPlanById(){
        $id = $_POST['id'];
        $data = $this->General_model->view_single_row('membership','id',$id);
        echo json_encode($data);
        exit();
    }
    function get_mebership_for_ticket() { 
        if (!empty($_POST['get_my_membership'])) {
            $res['get_activated_membership'] = $this->User_model->get_activated_membership($this->session->userdata('user_id'));
            $arr['time_duration'] = 'Month';
            $res['month_plans'] = $this->User_model->get_ticket_disc_memberships($arr);
            $arr['time_duration'] = 'Year';
            $res['year_plans'] = $this->User_model->get_ticket_disc_memberships($arr);

            if (in_array($res['get_activated_membership']->plan_id, array_column($res['month_plans'], 'id'))) {
                $activated_membership_plan_key = array_search($res['get_activated_membership']->plan_id, array_column($res['month_plans'], 'id'));
                $res['get_activated_month_membership'] = $res['month_plans'][$activated_membership_plan_key];
                unset($res['month_plans'][$activated_membership_plan_key]);
                $res['month_plans'] = array_splice($res['month_plans'], 0, count($res['month_plans']));
            } else if (in_array($res['get_activated_membership']->plan_id, array_column($res['year_plans'], 'id'))) {
                $activated_membership_plan_key = array_search($res['get_activated_membership']->plan_id, array_column($res['year_plans'], 'id'));
                $res['get_activated_year_membership'] = $res['year_plans'][$activated_membership_plan_key];
                unset($res['year_plans'][$activated_membership_plan_key]);
                $res['year_plans'] = array_splice($res['year_plans'], 0, count($res['year_plans']));
            }
            
            $common_settings = json_decode($this->session->userdata('common_settings'));
            $res['is_free_membership'] = (!empty($common_settings) && $common_settings->free_membership == 'on') ? '1' : '0';
            $res['is_admin'] = $this->session->userdata('is_admin');
            echo json_encode($res);
            exit();
        }
    }
    function get_membership_by_transactionid($id){
        $result = $PayPalResponseResult = $this->Paypal_payment_model->get_recurring_payments_profile_details($id);
        echo "<pre>";
        print_r($result);
        exit();   
    }
}

