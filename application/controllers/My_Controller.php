<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class My_Controller extends CI_Controller {
	public $footer_data, $footer_logo, $footer_link, $header_menu, $footer_menu, $footer_cs, $current_class, $current_method, $myprofile_button_active, $dashboard_button_active; //decleare as a global variable
	private $get_userData;
	
	function __construct() {
		parent::__construct();
		$this->load->library('breadcrumbs');
	    $this->load->library('gameimage');
	    $this->load->library('pagination');
	    $this->load->helper('my_common_helper');
		$this->footer_content();
		$this->menu_mgmt();
		$this->redirectTohttps();
		// $this->cart_item_to_session();
		
		if ($this->session->userdata('user_id')) {
			$this->get_userData = $this->General_model->view_data('user_detail',array('user_id'=>$this->session->userdata('user_id')));
			$arra = $this->get_userData[0];
			// savetimezone_forever
			if (!empty($this->session->userdata('timezone')) && empty($arra['timezone'])) {
				unset($arra['timezone']);
			}
			$common_settings = json_decode($arra['common_settings']);
			if (empty($common_settings->daylight_option)) {
				$opt_where['option_title'] = 'global_dst_option';
				$global_dst_option = $this->General_model->view_data('site_settings', $opt_where)[0];
				if (!empty($global_dst_option) && $global_dst_option['option_value'] == 'on') {
					$common_settings->daylight_option = 'on';
				}
			}
			$arra['common_settings'] = json_encode($common_settings);
			$this->session->set_userdata($arra);
		}
		$this->timezone();
		// Get the Class of every page
		$this->current_class = $this->router->fetch_class();
		$this->current_method = $this->router->fetch_method();

		$this->myprofile_button_active = array('page', 'sendmoney', 'points', 'game', 'dashboard', 'live_lobby_list', 'trade', 'order', 'cart', 'store', 'products', 'user', 'category');
		$this->dashboard_button_active = array('chat', 'home', 'challenge_mail', 'mail', 'friend', 'Ranking', 'matches','challenge_mail', 'streamsetting', 'subscribeuser', 'membership', 'checkout');
	}

	function redirectTohttps() {
	    if ($_SERVER['HTTPS'] != "on" && $_SERVER['HTTP_HOST'] != 'localhost') {
	        $redirect= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	        redirect($redirect); 
	    } 
	}
	//load view
	public function render($content) {
		$view_data = array('content' =>$content);
		$this->load->view('layout',$view_data);
	}
	// login status check
	protected function check_user_page_access() {
		if ($this->session->userdata('user_id') == '') {
			header('location:'.base_url());
	    	exit;
	  	}
	}
	protected function check_cover_page_access(){
		$cover_data = $this->General_model->view_data('cover',array('server_name' => $_SERVER['SERVER_NAME']))[0];
	    if ($cover_data['status'] == '1') {
	      if (!isset($_SESSION['cover_page_password'])) {
	      	redirect(base_url().'Home/cover_page');
	      }
	    }
	}

	protected function check_membeship(){
		$user_id = $this->session->userdata('user_id');
		$data['get_my_plan'] = $this->General_model->view_data('player_membership_subscriptions', array('user_id' => $user_id,'payment_status' => 'Active'));
		$data['common_settings'] = json_decode($this->session->userdata('common_settings'), true);

		if (((!empty($data['common_settings']['free_membership']) && $data['common_settings']['free_membership'] == 'off') || empty($data['common_settings']['free_membership'])) && empty($data['get_my_plan'])) {
			$this->session->set_flashdata('message_err', 'Please buy Membership to add product in your Store !');
			redirect(base_url().'membership/getMembershipPlan');
	    } else {
	    	return $data;
	    }
	}

	public function footer_content() {
		$this->footer_data=$this->General_model->view_all_data('footer_content','id','asc');
		$this->footer_logo=$this->General_model->view_all_data('logo','id','asc');
		$this->footer_link=$this->General_model->view_all_data('footer_link','id','asc');
		$this->footer_menu=$this->General_model->view_data('menu',array('position'=>2,'status'=>'active'));
		$this->footer_cs=$this->General_model->view_all_data('footer_customer_service','id','asc');
	}

	public function menu_mgmt() {
		$this->header_menu=$this->General_model->view_data_by_order('menu',array('position'=>1,'status'=>'active'),'menu_order','desc');
	}
	public function timezone() {
	  	if (!empty($_COOKIE['time_zne']) && !$this->session->userdata('user_id')) {date_default_timezone_set(timezone_name_from_abbr('', $_COOKIE['time_zne'] * 60, false));
	  	} else if ($this->session->userdata('user_id')) {
	  		date_default_timezone_set($this->session->userdata('timezone'));
	  	}
	}

	// Make array of variations from combined string
	public function convertSelVariantToArray($string){
	    $variant = array();
	    $variants = explode(",",$string);
	    foreach ($variants as $var_value) {
	        $var_value = ltrim($var_value);
	        $pairs = explode("&",$var_value);
	        foreach ($pairs as $value) {
	            $pair = explode("_", $value);
	            if(count($pair)-1 > 1){
	                $part1 = substr("$value",0, strrpos($value,'_'));
	                $part2 = substr("$value", (strrpos($value,'_') + 1));
	                $variant[$part1][] = $part2;
	            }else{
	                $variant[$pair[0]][] = $pair[1];
	            }
	        }
	    }
	    return $variant;
	}
}

