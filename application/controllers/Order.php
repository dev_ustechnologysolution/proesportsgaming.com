<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Order extends My_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Paypal_payment_model');
        $this->load->model('Email_model');
        $this->check_cover_page_access();
        $this->check_user_page_access();
    }
    function index() {
        $data['banner_data'] = $this->General_model->tab_banner();
        $arr = array( 'user_id' => $this->session->userdata('user_id'), 'order_by' => 'true' );
        $data['order_list'] = $this->Product_model->get_my_product($arr);
        $content = $this->load->view('order/order.tpl.php',$data,true);
        $this->render($content);
    }

    function player_orders() {
        $data['banner_data'] = $this->General_model->tab_banner();
        $arr = array( 'ref_seller_id' => $this->session->userdata('user_id'), 'order_by' => 'true' );
        $data['order_list'] = $this->Product_model->get_my_product($arr);
        $content = $this->load->view('order/player_orders.tpl.php',$data,true);
        $this->render($content);
    }

    function order_items() {
        if (!empty($_POST)) {
            $id = $_POST['id'];
            $order_status = $_POST['order_status'];
            $user_id = (isset($_POST['user_id']) && $_POST['user_id'] != null && $_POST['user_id'] != '') ? $_POST['user_id'] : $this->session->userdata('user_id');
            $arr = array(
                'user_id' => $user_id,
                'id' => $id
            );
            $order_items = $this->Product_model->get_my_product($arr);
            $plist = '<div class="common_product_list col-sm-12"><ul class="c_product_list">';
            $plist .= '<li class="cart_list_heading clearfix">
                <div class="col-sm-1 text-left"><label>Image</label></div>
                <div class="col-sm-3 text-left"><label>Name</label></div>
                <div class="col-sm-2 text-left"><label>Option</label></div>
                <div class="col-sm-1 text-right"><label>Quantity</label></div>
                <div class="col-sm-2 text-right"><label>Amount</label></div>
                <div class="col-sm-1 text-right"><label>Processing</label></div>
                <div class="col-sm-2 text-right"><label>Gross Total</label></div>
            </li>';
            foreach ($order_items as $key => $oi) {
                $sv = explode('&', $oi->sel_variation_amount);
                $variation_amount = end(explode('_', $sv[2]));
                $amount = $variation_amount + $oi->amount;
                $attr_div = '';
                if ($oi->product_attributes !='') {
                    $att = '';
                    $product_attributes = explode(',', $oi->product_attributes);
                    foreach ($product_attributes as $key => $pa) {
                        $att = explode('_', $pa);
                        $attr_div .= '<div>
                            <h4>
                                <span class="var_title">'.$att[0].'</span> : <span> '.$att[1].'</span>
                            </h4>
                        </div>';
                    }
                }
                $gross_total = $oi->qty * ($amount + $oi->product_fee);

                $download_link = ($oi->file_name !='' && $order_status == 'completed') ? '<a href="'.base_url().'upload/products/'.$oi->file_name.'" class="click_down"><div> <i class="fa fa-download"></i>&nbsp;Click to download</div></a>' : '';
                $plist .= '<li class="c_product_list_li">
                    <div class="col-md-1 c_prdct_img text-left">
                        <img src="'.base_url().'upload/products/'.$oi->product_image.'" alt="'.$oi->product_image.'" class="img img-responsive">
                    </div>
                    <div class="col-md-3 c_prdct_name text-left">
                        <a href="'.base_url().'Store/view_product/'.$oi->product_id.'">
                            <div>'.$oi->product_name.'</div>
                        </a>
                        '.$download_link.'
                    </div>
                    <div class="col-md-2 c_slct_varation text-left">'.$attr_div.'</div>
                    <div class="col-md-1 c_prdct_price select_prdct_qnt text-right">
                        <div>'.$oi->qty.'</div>
                    </div>
                    <div class="col-md-2 c_prdct_price text-right">
                        <div>$'.number_format($amount, 2, '.', '').'</div>
                    </div>
                    <div class="col-md-1 c_prdct_price text-right">
                        <div>$'.number_format($oi->product_fee, 2, '.', '').'</div>
                    </div>
                    <div class="col-md-2 c_prdct_price text-right">
                        <div>$'.number_format($gross_total, 2, '.', '').'</div>
                    </div>
                </li>';
            }
            $plist .= '</ul></div>';
            $res['order_item'] = $plist;
            echo json_encode($res);
            exit();
        }
    }
    function cancel_order() {
        if (!empty($_POST)) {
            $arr['order_id'] = $_POST['order_id'];
            $detail = $this->Cart_model->cancelorder($arr);
            $this->Email_model->order_action_mail(array('order_id' => $arr['order_id'], 'mail_action' => 'canceled'));
            ($detail['order_status']) ? $this->session->set_flashdata('message_succ', $detail['message_succ']) : $this->session->set_flashdata('message_err', 'Unable to cancel Order');
            $res['redirect'] = base_url().'order';
            echo json_encode($res);
            exit();
        }
    }

    public function reward_calculation($total_sold_items, $user_id, $total_reward, $user_level){

        $level_detail = $this->General_model->view_single_row('store_levels','level',$user_level[0]['current_level']);

        if(!empty($level_detail)){
            if($total_sold_items >= $level_detail['item_sold']){
                if(($level_detail['item_sold'] - $user_level[0]['sold_items']) >= 0) {
                    if(($level_detail['item_sold'] - $user_level[0]['sold_items']) != 0){
                        $lvl_items = $this->General_model->view_data('user_level_items',array('level' => $user_level[0]['current_level'], 'user_level_id'=>$user_level[0]['id']));
                        $item_data = array(
                            'user_level_id' => $user_level[0]['id'],
                            'user_id' => $user_id,
                            'sold_items' => (count($lvl_items) > 0) ? ($level_detail['item_sold'] - $user_level[0]['sold_items'] <= $total_sold_items) ? ($level_detail['item_sold'] - $user_level[0]['sold_items']) : ($total_sold_items - $user_level[0]['sold_items']) : $level_detail['item_sold'],
                            'level' => $user_level[0]['current_level'],
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        );
                        $this->General_model->insert_data('user_level_items',$item_data);
                        for ($i = $user_level[0]['current_level']; $i > 0 ; $i--) {
                            $reward_level_detail = $this->General_model->view_single_row('store_levels','level',$i);

                            $total_reward = $total_reward + ($level_detail['item_sold'] * $reward_level_detail['reward']);
                            // print_r($level_detail['item_sold'] * $reward_level_detail['reward']);
                        }
                    } 
                    if($user_level[0]['sold_items'] >= $level_detail['item_sold']){
                        $this->General_model->update_data('user_level', array('sold_items' => $level_detail['item_sold'], 'current_level' => $user_level[0]['current_level'] + 1), array('user_id' => $user_id));
                    } else {
                        $this->General_model->update_data('user_level', array('sold_items' => 0,'current_level' => $user_level[0]['current_level'] + 1), array('user_id' => $user_id));
                    }
                }
                $user_level = $this->General_model->view_data('user_level',array('user_id' => $user_id));
                $this->reward_calculation($total_sold_items - $level_detail['item_sold'], $user_id, $total_reward, $user_level);
                
                $this->db->where('user_id', $user_id);
                $this->db->set('total_balance', 'total_balance + '.$total_reward, FALSE);
                $this->db->update('user_detail');
             }else{
                $item_data = array(
                    'user_level_id' => $user_level[0]['id'],
                    'user_id' => $user_id,
                    'sold_items' => ($user_level[0]['sold_items'] > $total_sold_items) ? $total_sold_items : $total_sold_items - $user_level[0]['sold_items'],
                    'level' => $user_level[0]['current_level'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );
                $this->General_model->insert_data('user_level_items',$item_data);

                $this->db->where('user_id', $user_id);
                $this->db->set('sold_items', $total_sold_items, FALSE);
                $this->db->update('user_level');

             }
        }
    }

    function edit_tracking_id() {
        if (!empty($_POST)) {           
            $qty = 0;
            if ($_POST['editid'] !='' && $_POST['edit_traking_id']) {
                $odata = $this->General_model->view_single_row('orders_tbl','id',$_POST['editid']);
                if(($odata['tracking_id'] == null || empty($odata['tracking_id'])) && ($_POST['item_sold_by'] !='' && $_POST['grand_total'] !='' && $_POST['ref_seller'] !='')){
                    if ($_POST['item_sold_by'] == '0' && $_POST['ref_seller'] != '0'){

                        //Users get reward based on sum of reward of level they reached like if users is on 2nd level then they will get reward of level one and level 2 both(level 1 0.01 + level 2 0.02 = 0.03)

                        $items = $this->General_model->view_data('orders_items_tbl',array('order_id' => $_POST['editid']));
                        
                        foreach ($items as $item) {
                            $qty += $item['qty'];
                        }
                        $user_level = $this->General_model->view_data('user_level',array('user_id' => $_POST['ref_seller']));
                        
                        if (count($user_level) > 0 && $user_level != null) {
                            $total_sold_items = $user_level[0]['sold_items'] + $qty;
                            $m = $this->reward_calculation($total_sold_items, $_POST['ref_seller'], $total_reward = 0, $user_level);

                        } else {
                            $data = array(
                                'user_id' => $_POST['ref_seller'],
                                'sold_items' => 0,
                                'current_level' => 1,
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s')
                            );
                            $this->General_model->insert_data('user_level',$data);
                            $user_level = $this->General_model->view_data('user_level',array('user_id' => $_POST['ref_seller']));
                            $m = $this->reward_calculation($qty, $_POST['ref_seller'], $total_reward = 0, $r);
                        }
                    } else {
                        $global_store_fee = $this->General_model->view_single_row('site_settings','option_title','global_store_fee');
                        $this->db->where('user_id', $_POST['item_sold_by']);
                        $this->db->set('total_balance', 'total_balance + '.($_POST['grand_total'] - $global_store_fee['option_value']), FALSE);
                        $this->db->update('user_detail');
                    }
                }
                $r = $this->General_model->update_data('orders_tbl', array('tracking_id' => $_POST['edit_traking_id']), array('id' => $_POST['editid']));
                ($r) ? $this->session->set_flashdata('message_succ', 'Tracking info updated') : $this->session->set_flashdata('message_err', 'Unable to updated Tracking info');
                redirect(base_url().'order/player_orders');
            }
        }
    }


    public function savedata() {
        if ($_GET['id'] != '' && $_GET['action'] != '') {
            $id = $_GET['id'];
            $fields['order_status'] = $_GET['action'];
            $fields['updated_at'] = date('Y-m-d H:i:s');
            $update_data_where['id'] = $id;
            $updatedata = $this->General_model->update_data('orders_tbl', $fields, $update_data_where);
            $this->Email_model->order_action_mail(array('order_id' => $_GET['id'], 'mail_action' => $_GET['action']));
            ($updatedata) ? $this->session->set_flashdata('message_succ', 'Order status has been '.$fields['order_status']) : $this->session->set_flashdata('message_err', 'Unable to updated Order status');
            redirect($_SERVER['HTTP_REFERER']);
            exit();
        }
    }

}