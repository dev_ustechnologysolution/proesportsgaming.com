<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Payment extends My_Controller
{
//page loading
    public function __construct() {
        parent::__construct();
        $this->load->library('user_agent');
        $this->load->helper('url');
        $this->load->helper('file');
    }

    public function index() {
        // $this->check_user_page_access();
        $data['dt']=1;
        $order="orderID".time();
        $data=array(
            'user_id'=>$this->session->userdata('user_id'),
            'game_id'=>$this->session->userdata('game_id'),
            'subgame_id'=>$this->session->userdata('sgame_id'),
            'order_id'=>$order,
            'payment_status'=>'due'
        );
        $this->General_model->insert_data('payment',$data);
        $dt['info']=$order;
        $content=$this->load->view('payment/payment.tpl.php',$dt,true);
        $this->render($content);
    }
    public function success() {
        //$item_no          = $_REQUEST['item_number'];
        $item_transaction   = $_REQUEST['txn_id']; // Paypal transaction ID
        $item_price         = $_REQUEST['mc_gross']; // Paypal received amount
        $item_currency      = $_REQUEST['mc_currency'];
        $payer_email      = $_REQUEST['payer_email'];
        //update data when challenger pay payment
        $dat=array(
            'transaction_id'=>$item_transaction,
            'amount'=>$item_price,
            'currency'=>$item_currency,
            'payment_date'=>date('Y-m-d H:i:s'),
            'payer_email'=>$payer_email,
            'payment_status'=>'completed'
        );
        $this->General_model->update_data('payment',$dat,array('order_id' => $this->input->post('invoice',TRUE)));
        //finding record inside the bit table if there dn update when accepter pay or insert when challenger pay
        if($this->General_model->get_num_rows('bet',array('game_id'=>$this->session->userdata('game_id'),'is_accept'=>0))) {
            //accepter part
            // get bet table id
            $arr_bet_id = $this->General_model->view_data('bet',array('game_id'=>$this->session->userdata('game_id'),'is_accept'=>0));
            $bt_data = array(
                'accepter_id' =>$this->session->userdata('user_id'),
                'bet_status' =>1,
                'is_accept'=>1
            );
            $this->General_model->update_data('bet',$bt_data,array('game_id'=>$this->session->userdata('game_id'),'id'=>$arr_bet_id[0]['id']));
            $bt_dat = array(
                'status' =>2
            );
             $this->General_model->update_data('game',$bt_dat,array('id'=>$this->session->userdata('game_tble_id')));
            // update `admin_sub_game` table to `status` 0 changed on 26.07.2016
            $this->General_model->update_data('admin_sub_game',array('status'=>0),array('game_id'=>$this->session->userdata('game_id')));
            // update `admin_game` table to `play_status` 0 changed on 26.07.2016
            $this->General_model->update_data('admin_game',array('play_status'=>0),array('id'=>$this->session->userdata('game_id')));
        } else {
            //challenger part.
            $bet_data = array(
                'game_id' =>$this->session->userdata('game_id'),
                'game_sub_id' =>$this->session->userdata('sgame_id'),
                'challenger_id' =>$this->session->userdata('user_id')
            );
            $bet_id = $this->General_model->insert_data('bet',$bet_data);
            // get `id` from table `game`
            $condition = "1 AND `game_id`='".$this->session->userdata('game_id')."' AND (`status`=1 OR `status`=0)";
            $arr_game_id = $this->General_model->view_data('game',$condition);
            $dt=array(
                'payment_status'=>1,
                'status'=>2,
                'bet_id'=>$bet_id
            );
            $this->General_model->update_data('game',$dt,array('game_id'=>$this->session->userdata('game_id'),'id'=>$arr_game_id[0]['id']));
            // $gm_status=array(
            //     'play_status'=>1
            // );
            // $this->General_model->update_data('admin_game',$gm_status,array('id'=>$this->session->userdata('game_id')));
            $gm_st=array(
                'status'=>1
            );
            $this->General_model->update_data('admin_sub_game',$gm_st,array('game_id'=>$this->session->userdata('game_id')));
        }
        $data['dt']='Payment Successful';
        $data['banner_data'] = $this->General_model->tab_banner();
        $content=$this->load->view('payment/payment_success.tpl.php',$data,true);
        $this->render($content);
    }
    public function failure() {
        $this->General_model->paypal_fallback_data(json_encode($_REQUEST), 'Payment Fail');

        $data['dt']='Payment Fail';
        $data['banner_data'] = $this->General_model->tab_banner();
        $content=$this->load->view('payment/payment_failure.tpl.php',$data,true);
        $this->render($content);
    }
    
    public function payment_success(){
        $data['dt']='Payment Successful';
        $data['banner_data'] = $this->General_model->tab_banner();
        $content=$this->load->view('payment/payment_success.tpl.php',$data,true);
        $this->render($content);
    }
    public function lobby_payment_success($lobby_id,$user_id){
        $data['display_name'] = $this->General_model->get_my_fan_tag($lobby_id,$user_id)->fan_tag;
        $data['dt']='Lobby Payment Successful';
        $data['lobby_id'] = $lobby_id;
        
        $data['banner_data'] = $this->General_model->tab_banner();
        $content=$this->load->view('payment/lobby_payment_success.tpl.php',$data,true);
        $this->render($content);
    }
    
}