<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include BASEPATH.'../application/controllers/My_Controller.php';

class Privacy extends My_Controller 

{

	

	public function __construct()

    {

	parent::__construct();

	// $this->check_user_page_access();

    }

//page loading

	public function index()

	{
		
		$data['dt']=$this->General_model->view_all_data('privacy','id','asc');

		$content=$this->load->view('privacy/privacy.tpl.php',$data,true);

		$this->render($content);

		

	}

}

