 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Products extends My_Controller {
  public function __construct() {
    parent::__construct();
		$this->target_dir = BASEPATH.'../upload/products/';
    $this->check_cover_page_access();
    $this->check_user_page_access();
  }
  //page loading
  function index() {
    $arr = array('seller_id' => $this->session->userdata('user_id'), 'order_by' => 'pt.product_order', 'group_by' => 'pt.id');
    $data['list'] = $this->Product_model->productDetail($arr);
    $content = $this->load->view('products/products.tpl.php',$data,true);
    $this->render($content);
  }
  function add_selling_product() { 
  	// get my membership 
  	$this->check_membeship();

  	$data['category_list'] = $this->Product_model->category_list(array('parent_id' => '0'));
		$options = array(
      'select' => 'at.title, at.type, at.image_upload, at.attr_order, paot.attr_id, paot.option_label, paot.option_id',
      'table' => 'attributes_tbl at',
      'join' => array(
      	'product_attributes_opt_tbl paot' => 'paot.attr_id = at.id',
      ),
      'order' => array('at.attr_order' => 'asc', 'paot.opt_order' => 'asc'),
      'where' => array('at.status' => 1)
    );
    $attributes_list = $this->General_model->commonGet($options);
    $result = array();
    foreach($attributes_list as $attribute) {
    	$result[$attribute->title.'_'.$attribute->type][] = $attribute; 
    }
    $data['attribute_list'] = $result;
    $data['type'] = $_GET['type'];
  	$content = $this->load->view('products/add_products.tpl.php',$data,true);
    $this->render($content);	
  }
  function selling_product_insert() {	
		$query = $this->db->query('SELECT max(product_order) as product_order FROM products_tbl');
		$current_product_order = $query->result();
		$product_order = $current_product_order[0]->product_order + 1;
		$data = array(
			'category_id' => $_POST['category_id'].','.$_POST['subcategory_id'],
			'name' => trim($_POST['product_name']),
			'description' => trim($_POST['description']),
			'price' => trim($_POST['price']),
			'final_price' => (isset($_POST['final_price']) && $_POST['final_price'] != '') ? trim($_POST['final_price']) : '0.00',
			'special_price' => ($_POST['special_price'] != '') ? trim($_POST['special_price']) : '0.00',
			'qty' => trim($_POST['qty']),
			'fee' => trim($_POST['fee']),
			'status' => 1,
			'seller_id' => $this->session->userdata('user_id'),
			'is_18' => $this->session->userdata('is_18'),
			'type' => $_POST['type'],
			'product_order' =>$product_order,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		);	
		$r = $this->General_model->insert_data('products_tbl',$data);
		if($r) {	
			if (!empty($_POST['attr_id'])) {
				foreach ($_POST['attr_id'] as $key => $value) {
					$arr_attr_data = explode('_', $key);
					$insert_attr_data[] = array(
						'product_id' => $r,
						'attribute_id' => $arr_attr_data[0],
						'option_id' => $arr_attr_data[1],
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s')
					);										
				}
				if (count($insert_attr_data) > 0) {
					$this->db->insert_batch('product_attributes_tbl',$insert_attr_data);
				}
			}
			if (isset($_FILES['game_file']["name"])) {
				$game_name = time().'_'.$r.basename($_FILES["game_file"]["name"]);
				$target_file = $this->target_dir.$game_name;
				move_uploaded_file($_FILES["game_file"]["tmp_name"], $target_file);
				$this->General_model->update_data('products_tbl',array('game_name' => $game_name),array('id' => $r));
			}
			$this->General_model->update_data('product_images',array('product_id' => $r),array('product_id' => 0));
			$this->General_model->update_data('product_attributes_tbl',array('product_id' => $r),array('product_id' => 0));

			$this->session->set_flashdata('message_succ', 'Insert successfull');
			redirect(base_url().'products');
		} else {
			$this->session->set_flashdata('message_err', 'Failure to Insert');	
			redirect(base_url().'products/add_selling_product?type=1');
		}
	}
	function product_edit($id) {
		$data['product_data'] = $this->General_model->view_data('products_tbl',array('id' => $id));
		$data['product_image_data'] = $this->General_model->view_data('product_images',array('product_id' => $id));
		$data['product_attribute_data'] = $this->General_model->view_data('product_attributes_tbl',array('product_id' => $id));
		$data['category_list'] = $this->General_model->view_all_data('product_categories_tbl','id','asc');
		$options = array(
      'select' => 'at.title, at.type, at.image_upload, at.attr_order, paot.attr_id, paot.option_label, paot.option_id',
      'table' => 'attributes_tbl at',
      'join' => array(
      	'product_attributes_opt_tbl paot' => 'paot.attr_id = at.id',            
      ),
      'order' => array('at.attr_order' => 'asc', 'paot.opt_order' => 'asc'),
      'where' => array('at.status' => 1)
    );
    $attributes_list = $this->General_model->commonGet($options);
    $result = array();
    foreach($attributes_list as $attribute){        
    	$result[$attribute->title.'_'.$attribute->type][]= $attribute; 
    }
    $data['attribute_list'] = $result;
		$content=$this->load->view('products/edit_products.tpl.php',$data,true);
		$this->render($content);
	}
	function selling_product_update() {
		$this->check_user_page_access();
		$id = $this->input->post('id', TRUE);
		$data = array(
			'category_id' => $_POST['category_id'].','.$_POST['subcategory_id'],
			'name' => trim($_POST['product_name']),
			'description' => trim($_POST['description']),
			'price' => trim($_POST['price']),
			'final_price' => ($_POST['final_price'] != '') ? trim($_POST['final_price']) : '0.00',
			'special_price' => ($_POST['special_price'] != '') ? trim($_POST['special_price']) : '0.00',
			'qty' => trim($_POST['qty']),
			'fee' => trim($_POST['fee']),
			'status' => 1,
			'is_18' => $_POST['is_18'],
			'updated_at' => date('Y-m-d H:i:s')
		);
		$r = $this->General_model->update_data('products_tbl', $data, array('id'=>$id));

		if($r) {
			if ($_FILES['game_file']["name"] != '') {
				$game_name = time().'_'.$r.basename($_FILES["game_file"]["name"]);
				$target_file = $this->target_dir.$game_name;
				move_uploaded_file($_FILES["game_file"]["tmp_name"], $target_file);
				$this->General_model->update_data('products_tbl', array('game_name' => $game_name), array('id' => $id));
			}

			$product_img_data = $this->General_model->view_data('product_images',array('product_id' => $id,'is_default' => '0'));

			$old_gallery_image = explode(',', $_POST['old_gallery_image']);
			$t = array_diff(array_column($product_img_data,'id'), $old_gallery_image);
			$product_attribute_data = $this->General_model->view_data('product_attributes_tbl',array('product_id'=>$id));
			if (count($_POST['attr_id']) > 0) {
				foreach ($product_attribute_data as $v) {
					if (!in_array($v['attribute_id'].'_'.$v['option_id'], array_keys($_POST['attr_id']))) {
						$this->General_model->delete_data('product_attributes_tbl',array('id' => $v['id']));
					} else {
						$update_attr_data = array(
							'attribute_id' => $v['attribute_id'],
							'option_id' => $v['option_id'],
							'updated_at' => date('Y-m-d H:i:s')
						);
						$this->General_model->update_data('product_attributes_tbl',$update_attr_data,array('product_id'=>$id,'option_id' => $arr_attr_data[1]));
					}
				}
				$arr_option = array_column($product_attribute_data,'option_id');
				foreach ($_POST['attr_id'] as $key => $value) {
					$arr_attr_data = explode('_', $key);
					if (!in_array($arr_attr_data[1], $arr_option)) {
						$insert_attr_data = array(
							'product_id' => $id,
							'attribute_id' => $arr_attr_data[0],
							'option_id' => $arr_attr_data[1],
							'updated_at' => date('Y-m-d H:i:s'),
							'created_at' => date('Y-m-d H:i:s')
						);
						$this->General_model->insert_data('product_attributes_tbl',$insert_attr_data);
					}
				}
			}
			$this->session->set_flashdata('message_succ', 'Update successfull');
			redirect(base_url().'products');
		} else {
			$this->session->set_flashdata('message_err', 'Failure to update');	
			redirect(base_url().'products/product_edit/'.$this->input->post('id', TRUE));
		}
	}
	function delete_file() {
		($_REQUEST['type'] == 1 ) ? $this->General_model->delete_data('product_attributes_tbl',array('id' => $_REQUEST['id'])) : $this->General_model->delete_data('product_images',array('id' => $_REQUEST['id']));
	}
  function upload_file() {	
  	if ($_FILES['file']["name"] != '') {
			$main_img = time().'_'.basename($_FILES["file"]["name"]);				
			$target_file = $this->target_dir.$main_img;
			move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);

			if($_POST['type'] == 1) {
				$attribute_data_arr = explode('_', $_POST['attr']);	
				$main_img_data = array(
					'product_id' => (isset($_POST['product_id'])) ? $_POST['product_id'] : 0,
					'user_id' => (isset($_POST['user_id'])) ? $_POST['user_id'] : 1,
					'attribute_id' => $attribute_data_arr[1],
					'option_id' => $attribute_data_arr[2],
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s'),
					'option_image' => $main_img
				);
				$this->db->insert('product_attributes_tbl', $main_img_data);
			} else {
				$main_img_data = array(
        	'product_id' => (isset($_POST['product_id'])) ? $_POST['product_id'] : 0,
        	'user_id' => (isset($_POST['user_id'])) ? $_POST['user_id'] : 1,
        	'image' => $main_img,
        	'is_default' => '0',
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        );
        $this->General_model->insert_data('product_images',$main_img_data);
      }
      $result['id'] = $this->db->insert_id();
      $result['name'] = base_url().'upload/products/'.$main_img;
		}
		echo json_encode($result);
		exit();
	}
	function delete_product($id) {
		if (isset($id) && $id != '') {
			$r = $this->General_model->delete_data('products_tbl', array('id' => $id));
			($r) ? $this->session->set_flashdata('message_succ', 'Item has been Removed from your store') : $this->session->set_flashdata('message_err', 'Failure to delete');
      redirect(base_url().'products');
    }
	}
	function update_product_fee() {
		if (!empty($_POST)) {
			($_POST['fee_type'] == 'Local') ? $update_data['local_product_fee'] = $_POST['type'].'_'.$_POST['fee_update'] : $update_data['international_product_fee'] = $_POST['type'].'_'.$_POST['fee_update'];
			$update = $this->General_model->update_data('user_detail', $update_data, array(
				'user_id' => $this->session->userdata('user_id')
			));
			($update) ? $this->session->set_flashdata('message_succ', 'Fee Updated') : $this->session->set_flashdata('message_err', 'Failure to Update');
			redirect(base_url().'products');
		}
	}
}