<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Ranking extends My_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->check_user_page_access();
        $this->load->model('My_model');
        $this->load->model('General_model');
        $this->load->library('pagination');
        $this->pageLimit=20;
    }
//page loading
    public function index()
    {
        $this->check_user_page_access();
        
        $data['page']=$this->General_model->view_data('menu',array('id'=>'4'));

        $data['banner_data'] = $this->General_model->tab_banner();

        $content=$this->load->view('ranking/ranking.tpl.php',$data,true);

        $this->render($content);
    }
}