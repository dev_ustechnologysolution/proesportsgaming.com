<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include BASEPATH.'../application/controllers/My_Controller.php';

class Signup extends My_Controller 

{

  function __construct()

    {

	parent::__construct();

	$this->load->model('User_model');

	$this->target_dir=BASEPATH.'../upload/profile_img/';

	}

	

public function index()

	{

		

		$data['msg']='';

		$user=array(

		'email'=>$this->input->post('email', TRUE),

		'password'=>md5($this->input->post('password', TRUE)),

		 );

		

		 if($this->input->post('email', TRUE)!='')

		 {

			//echo $this->User_model->check_mail('user',array('email'=>$this->input->post('email', TRUE)));exit;

			if($this->User_model->check_mail('user',array('email'=>$this->input->post('email', TRUE))))

			{

			$data['msg']='Your email already exist';

			}

			else

			{

				

				$r=$this->General_model->insert_data('user',$user);

				if($r)

				{	

					$img='';

					if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name']))

					{
						
						$img=time().basename($_FILES['image']['name']);

						$ext = end((explode(".", $img)));
						
						if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg')
						{

							$target_file=$this->target_dir.$img;

							move_uploaded_file($_FILES['image']['tmp_name'], $target_file);

							$this->General_model->insert_data('user_detail',array('image'=>$img));
						}

					}



					$date_of_birth = $this->input->post('date', TRUE) . "-" . $this->input->post('month', TRUE)."-". $this->input->post('year', TRUE);

					 $user_detail=array(

						'name'=>$this->input->post('name', TRUE),

						'number'=>$this->input->post('number', TRUE),

						'location'=>$this->input->post('location', TRUE),

						'dob'=>$date_of_birth,

						'user_id'=>$r

						 );	

					if($this->General_model->insert_data('user_detail',$user_detail))

					$data['msg']='Register Successfully';

					else

					$data['msg']='Register Unsuccessfull';

					redirect(base_url().'login');

				}

			}



			

		}

		$content=$this->load->view('login/registration.tpl.php',$data,true);

		$this->render($content);

		

	}

	

	

	

}

