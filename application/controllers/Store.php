<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Store extends My_Controller {
    public function __construct() {
      parent::__construct();
      $this->load->model('User_model');
      $this->load->library('pagination');
      $this->pageLimit = 20;
      $this->countItem = 50;
      $this->countItem_loader = 25;
      $this->check_cover_page_access();
      unset($_SESSION['redirect_url']);
      unset($_SESSION['request_data']);
      $this->target_dir = BASEPATH.'../upload/all_images/';
    }
    function _remap($method,$args) {
        if (method_exists($this, $method)) {
            if (count($args) < 2) {
                $this->$method($args[0]);
            } else {
                redirect(base_url());
            }
        } else {
            $this->index($method,$args);
        }
    }
    //page loading
    function index($customstore) {
        $data['user_id'] = '0';
        $data['cus_store'] = 0;
        if (!empty($customstore)) {
            $data['customstore'] = urldecode($customstore);
            $userdata = $this->General_model->view_single_row('user_detail',array('store_link' => $data['customstore']),'*');
            $data['user_id'] = $userdata['user_id'];
            $get_user_plan = $this->General_model->view_data('player_membership_subscriptions', array(
                'user_id' => $data['user_id'],
                'payment_status' => 'Active'
            ));
            $store_membership_status = $this->General_model->view_single_row('site_settings','option_title','store_membership_status');
            if(($data['user_id'] != '0' && $store_membership_status['option_value'] == 'off' && (count($get_user_plan) > 0 || json_decode($userdata['common_settings'], true)['free_membership'] == 'on')) || $store_membership_status['option_value'] == 'on' || $data['user_id'] == '0'){
                $data['user_data'] = $userdata;
                $data['cus_store'] = 1;
            } else {
                $this->session->set_flashdata('message_err', 'Store is unavailable!!!');
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
        $data['banner_data'] = $this->General_model->tab_banner();
        $data['itemperpage'] = $this->countItem;

        if (isset($_GET['search_product'])) {
            if (!empty($_POST)) {
                $main_cat = $data['selected_main_cat'] = $_POST['main_category'];
                $main_sub_cat = $data['selected_main_sub_cat'] = $_POST['main_sub_cat'];
                $product_srch_term = $data['inputed_product_srch_term'] = $_POST['product_srch-term'];
                if ($main_cat !='') {
                    $arr = array( 'main_cat' => $main_cat );
                    $all_sub_cat = $this->Product_model->getSubCarArr($arr);
                    $all_sub_cat_arr = explode(', ', $all_sub_cat->cate_id_arr);
                }
                $store_ads_list = $this->General_model->get_ads_detail('store_ads',100,'img','');
                $list_arr = array(
                    'all_sub_cat_arr' => $all_sub_cat_arr,
                    'main_cat' => $main_cat,
                    'main_sub_cat' => $main_sub_cat,
                    'product_srch_term' => $product_srch_term,
                    'order_by' => 'product_order',
                    'seller_id' => $data['user_id'],
                    'limit' => $data['itemperpage'],
                );
                $data['list'] = $this->Product_model->SearchProductArr($list_arr);
                $get_last_arr = array(
                    'all_sub_cat_arr' => $all_sub_cat_arr,
                    'main_cat' => $main_cat,
                    'main_sub_cat' => $main_sub_cat,
                    'product_srch_term' => $product_srch_term,
                    'get_last_id' => 'yes',
                    'seller_id' => $data['user_id'],
                    'limit' => 1,
                );
                $data['get_last_id'] = $this->Product_model->SearchProductArr($get_last_arr)[0];
                if($data['get_last_id']['count'] > 0){
                    $data['count_product'] = $data['get_last_id']['count'].' Result to display';    
                }else{
                    $data['count_product'] = '';
                }                
            }
        } else {
            $list_arr = array(
                'limit' => $data['itemperpage'],
                'order_by' => 'product_order',
                'seller_id' => '0',
            );
            $data['list'] = $this->Product_model->SearchProductArr($list_arr);
            $get_last_arr = array(
                'get_last_id' => 'yes',
                'limit' => 1,
                'seller_id' => $data['user_id'],
            );
            $data['get_last_id'] = $this->Product_model->SearchProductArr($get_last_arr)[0];
        }

        $data['category_list'] = $this->Product_model->getCategories();
        $data['store_ads_list'] = $this->General_model->get_ads_detail('store_ads',count($data['list']),'img','');
        $data['category_list_arr'] = array_column($data['category_list'], 'parent_id');
        $content = $this->load->view('store/store.tpl.php',$data,true);
        $this->render($content);
    }

    // product search
    function search_product() {
        if (!empty($_POST)) {
            $main_cat = $_POST['main_category'];
            $store_id = $_POST['store_id'];
            $main_sub_cat = $_POST['main_sub_cat'];
            $product_srch_term = $_POST['product_srch_term'];
            $last_item = (!empty($_POST['allcount']) && $_POST['allcount'] != 'NaN') ? $_POST['allcount'] : '';
            if ($main_cat !='') {
                $arr = array( 'main_cat' => $main_cat );
                $all_sub_cat = $this->Product_model->getSubCarArr($arr);
                $all_sub_cat_arr = explode(',', $all_sub_cat->cate_id_arr);
            }
            $store_ads_list = $this->General_model->get_ads_detail('store_ads',100,'img','');
            if($_POST['btn_id'] != null && $_POST['btn_id'] != ''){
                $last_item = '';
            }
            $arr1 = array(
                'all_sub_cat_arr' => $all_sub_cat_arr,
                'main_cat' => $main_cat,
                'main_sub_cat' => $main_sub_cat,
                'product_srch_term' => $product_srch_term,
                'last_id' => $last_item,
                'order_by' => 'product_order',
                'limit' => $this->countItem_loader,
                'seller_id' => $store_id
            );

            if (isset($_POST['num_display_prdct']) && !empty($_POST['num_display_prdct'])) {
                $arr1['limit'] = $_POST['num_display_prdct'];
            }
            $res_arr = $this->Product_model->SearchProductArr($arr1);

            $get_last_arr = array(
                'all_sub_cat_arr' => $all_sub_cat_arr,
                'main_cat' => $main_cat,
                'main_sub_cat' => $main_sub_cat,
                'product_srch_term' => $product_srch_term,
                'get_last_id' => 'yes',
                'seller_id' => $store_id,
                'limit' => 1,
            );
            $data['get_last_id'] = $this->Product_model->SearchProductArr($get_last_arr)[0];
            // if (!empty($res_arr)) {
            //     // $i = 0;
            //     $data['last_id'] = $last_item;
            //     foreach ($res_arr as $key => $res) {
            //         // echo"<pre>";
            //         // print_r($res);exit;
            //         $data['last_id'] = $product_id =  $res['id'];
            //         $data['last_product'] = $res['product_order'];
            //         $img = $this->General_model->view_single_row('product_images',array('product_id' => $product_id),'image')['image'];
            //         if ($key % 5 == 0) { 
            //             $result .='<li class="ads row">
            //                 <div class="add_listing_div">
            //                     <div class="col-md-3"></div>
            //                     <div class="col-md-6">
            //                         <a class="ads_container_desciption_div" href="'.$store_ads_list[$i]->sponsered_by_logo_link.'" target="_blank">
            //                            <div class="ads_img_div col-sm-4">
            //                             <img src="'.base_url().'upload/ads_master_upload/'.$store_ads_list[$i]->upload_sponsered_by_logo.'" alt="'.$store_ads_list[$i]->upload_sponsered_by_logo.'" class="img img-responsive">
            //                            </div>
            //                            <div class="ads_content_div col-sm-8">
            //                             <div class="ads_desciption">'.$store_ads_list[$i]->sponsered_by_logo_description.'</div>
            //                           </div>
            //                         </a>
            //                     </div>
            //                     <div class="col-md-3"></div>
            //                 </div>
            //             </li>'; 
            //             $i++;
            //         }

            //         $result .= '<li class="product_itm product_'.$product_id.'">
            //             <div class="main_product">
            //                 <div class="product_img_dv"><img src="'.base_url().'upload/products/'.$img.'" alt="'.$img.'" class="img img-responsive"></div>
            //                 <div class="product_title_dv">
            //                     <label class="title">'.$res['name'].'</label>
            //                 </div>
            //                 <div class="product_view_dv">
            //                     <a class="view_product" href="'.base_url().'Store/view_product/'.$product_id.'"> View Item </a>
            //                 </div>
            //             </div>
            //         </li>';
            //     }
            // }
            $data['ads'] = $store_ads_list;
            $data['result'] = $res_arr;
            echo json_encode($data);
            exit();
        }
    }
    function search_category() {
        if (!empty($_POST)) {
            $main_cat = $_POST['main_category'];
            $res['suboption'] = '<option value="">Please Select Sub-Category</option>';
            if ($main_cat !='') {
                $arr = array( 'main_cat' => $main_cat );
                $all_sub_cat = $this->Product_model->getSubCarArr($arr);
                $all_sub_cat_arr = explode(',', $all_sub_cat->cate_id_arr);
                $all_subcat = explode(',', $all_sub_cat->subcate_name_arr);
                foreach ($all_sub_cat_arr as $key => $asc) {
                    $res['suboption'] .= '<option value="'.trim($asc).'">'.$all_subcat[$key].'</option>';
                }
            }
            echo json_encode($res);
            exit();
        }
    }
    function view_product($id) {
        $data['ref_id'] = $_SESSION['ref_id'];
        $data['category_list'] = $this->Product_model->getCategories();
        $data['product_id'] = $id;
        $arr = array('product_id' => $id);
        $data['product_detail'] = $this->Product_model->productDetail($arr);
        (empty($data['product_detail']))? redirect(base_url().'store'):'';
        $data['prdct_img_arr'] = array_unique(array_column($data['product_detail'],'image'));
        $data['default_sel_img'] = $data['prdct_img_arr'][0];

        $arr = array('product_id' => $id);
        $product_attribute_data = $this->Product_model->attributes_list($arr);
        
        $result = array();
        if (!empty($product_attribute_data)) {
            $type_arr = array_column($product_attribute_data, 'type');
            $title_arr = array_column($product_attribute_data, 'title');
            $attribute_type_list = array_combine($title_arr, $type_arr);
            foreach($product_attribute_data as $attribute) {
                $result[$attribute->title][] = $attribute; 
            }
        }
        $data['attribute_type_list'] = $attribute_type_list;
        
        $data['attribute_list'] = $result;
        $data['product_ads'] = $this->General_model->get_ads_detail('product_ads',1,'',$data['product_id'])[0];

        $content = $this->load->view('store/single_product.tpl.php',$data,true);
        $this->render($content);
    }
    function get_img_selvar() {
        if (!empty($_POST)) {
            if (!empty($_POST['sel_variation'])) {
                $sel_variation = $_POST['sel_variation'];
                $sel_variation = array_filter($sel_variation);
                $product_id = $_POST['product_id'];
                foreach ($sel_variation as $key => $sv) {
                    $attribute_id = $key;
                    $option_id = $sv;
                    if ($attribute_id == undefined && $option_id == undefined) {
                        $arr = array( 'product_id' => $product_id );
                        $data['product_detail'] = $this->Product_model->productDetail($arr);
                        $get_main_imgs = array_unique(array_column($data['product_detail'],'image'));
                        $default_main_sel_img = $get_main_imgs[0];
                    } else {
                        $attr = array(
                           'attribute_id' => $attribute_id,
                           'option_id' => $option_id,
                           'product_id' => $product_id,
                           'fetch_data' => 'result',
                        );
                        $getdata = $this->Cart_model->get_attribute_img($attr);
                        $get_main_imgs = array_unique(array_column($getdata,'option_image'));
                        $default_main_sel_img = $get_main_imgs[0];
                    }
                }
                $var_imgs = (!empty($get_main_imgs)) ? $get_main_imgs : '';
                $data['img'] = '';
                $img_count = 0;
                foreach ($var_imgs as $key => $vi) {
                  if ($vi !='') {
                    if ($img_count == 0) {
                        if (!empty($get_main_imgs) && $default_main_sel_img !='') {
                            $data['selected_img'] = $default_main_sel_img;
                            $data['zoom_img'] = base_url().'upload/products/'.$default_main_sel_img;
                        } else {
                            $data['selected_img'] = $vi;
                            $data['zoom_img'] = base_url().'upload/products/'.$vi;
                        }
                        $data['img'] .= '<li class="prdctimg active" data-tg_img="'.$vi.'"><img class="prdct_thmbnl img img-responsive show-small-img" src="'.base_url().'upload/products/'.$vi.'" alt="now"></li>';
                    } else {
                        $data['img'] .= '<li class="prdctimg " data-tg_img="'.$vi.'"><img class="prdct_thmbnl img img-responsive show-small-img" src="'.base_url().'upload/products/'.$vi.'" alt=""></li>';
                    }
                    $img_count++;
                  }
                }
                $data['show_arrows'] = ($img_count > 4) ? 'yes': 'no';
                echo json_encode($data);
                exit();
            }
        }
    }
    public function wishlist() {
        $this->check_user_page_access();
        $data['wishlist_item'] = $this->Cart_model->wishlist_items();
        $data['category_list'] = $this->Product_model->getCategories();
        $data['user_detail'] = $this->User_model->user_detail()[0];
        $content = $this->load->view('store/wishlist.tpl.php',$data,true);
        $this->render($content);
    }
    public function removeProductWishlist(){
      $wishlist_item_id = $_REQUEST['id'];
      $r = $this->General_model->delete_data('wishlist_item',array('id'=>$wishlist_item_id));
      ($r) ? $this->session->set_userdata('message_succ', 'Item has been Removed from wishlist successfully') :  $this->session->set_userdata('message_err', 'Failure to delete');
      redirect(base_url().'store/wishlist');
    }
    function my_store() {
        $this->check_user_page_access();
        $levels_array = array();
        $user_level_data_array = array();
        $user_level_data_array_remain = array();
        $data['active_tab'] = 'store_detail';
        $data['order_detail'] = $this->Order_model->get_user_order_details($this->session->userdata('user_id'));
        $data['levels'] = $this->General_model->view_all_data('store_levels','order_id','asc');
        $data['user_level'] = $this->General_model->view_single_row('user_level',array('user_id' => $this->session->userdata('user_id')), '*');

        // $data['user_level_data'] = $this->General_model->view_data('user_level_items', array('user_level_id' => $data['user_level']['id']));
        $this->db->select('*');
        $this->db->select('SUM(sold_items) AS items', FALSE);
        $this->db->from('user_level_items'); 
        $this->db->where('user_level_id', $data['user_level']['id']);       
        $this->db->group_by('level');           
        $sql = $this->db->get();
        $data['user_level_data'] = $sql->result_array();

        foreach ($data['levels'] as $key => $lvl) {
            $levels_array[] = $lvl['level_title'];
            $user_level_data_array[] = ($data['user_level_data'][$key]['items']) ? $data['user_level_data'][$key]['items'] : 0;
            $user_level_data_array_remain[] = $lvl['item_sold'] - $data['user_level_data'][$key]['items'];
        }
        // foreach ($data['user_level_data'] as $itm) {
        //     $user_level_data_array[] = $itm['items'];
        // }

        $data['levels_array'] = $levels_array;
        $data['user_level_data_array'] = $user_level_data_array;
        $data['user_level_data_array_remain'] = $user_level_data_array_remain;

        $current_key = array_search($data['user_level']['current_level'],array_column($data['levels'], 'level'));
        $data['current_level'] = $data['levels'][$current_key];
        $content = $this->load->view('store/my_store.tpl.php',$data,true);
        $this->render($content);   
    }
    function store_link_update() {
        $this->check_user_page_access();
        if (!empty($_POST['store_link'])) {
            $store_link = trim($_POST['store_link']);
            $update_data['store_link'] = $store_link;
            $update_data['store_link']=str_replace(" ","-",$update_data['store_link']);
            $checklink = array(
                'store_link' => $store_link,
                'user_id !=' => $this->session->userdata('user_id')
            );
            
            $userdata = $this->General_model->view_single_row('user_detail',$checklink,'store_link');
            (!empty($userdata)) ? $res['error'] = 'Store is exist with this name. Please try another name..' : ((strpos($store_link, 'account') !== false) ? $res['error'] = "Can't use Account keyword. Please try another name.." : $res['error'] = '');
            if (empty($res['error'])) {
                $r = $this->General_model->update_data('user_detail', $update_data, array('user_id' => $this->session->userdata('user_id')));
                ($r) ? $this->session->set_flashdata('message_succ', 'Store link has been Updated.') :  $this->session->set_flashdata('message_err', 'Failure to update store link.');
            }
            echo json_encode($res);
            exit();
        }
    }
    function check_store_link() {
        $this->check_user_page_access();
        if (!empty($_POST['store_link'])) {
            $store_link = trim($_POST['store_link']);
            $userdata = $this->General_model->view_single_row('user_detail',array('store_link' => $store_link),'store_link');
            (!empty($userdata)) ? $res['error'] = 'Store is exist with this name. Please try another name..' : ((strpos($store_link, 'account') !== false) ? $res['error'] = "Can't use Account keyword. Please try another name.." : $res['error'] = '');
            echo json_encode($res);
            exit();
        }
    }

    public function store_banner_update(){
        $img=$this->input->post('old_image', TRUE);
        if(isset($_FILES["image"]["name"]) && $_FILES["image"]["name"]!='')
        {
            $img=time().basename($_FILES["image"]["name"]);
            $target_file = $this->target_dir.$img;
            if(move_uploaded_file($_FILES["image"]["tmp_name"], $target_file))
            {
                if(file_exists($this->target_dir.$this->input->post('old_image', TRUE)))
                {
                    unlink($this->target_dir.$this->input->post('old_image', TRUE));
                }
            }
        }
        $data=array(
            'store_banner'=>$img
            );
        $r = $this->General_model->update_data('user_detail',$data,array('user_id'=>$this->session->userdata('user_id')));
        ($r) ? $this->session->set_userdata('message_succ', 'Update successfull') : $this->session->set_userdata('message_err', 'Failure to update');
        
        redirect(base_url().'store/my_store');
    }

    public function delete_store_banner(){
        $data=array(
            'store_banner'=>''
            );
        $r = $this->General_model->update_data('user_detail',$data,array('user_id'=>$this->session->userdata('user_id')));
        ($r) ? $this->session->set_flashdata('message_succ', 'Banner removed successfully') : $this->session->set_flashdata('message_err', 'Fail to remove banner');
        header('location:'.base_url().'store/my_store');
    }

}