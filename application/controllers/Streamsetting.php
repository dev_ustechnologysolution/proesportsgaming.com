<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Streamsetting extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->check_user_page_access();
		$this->load->library(array('form_validation'));
		$this->target_dir=BASEPATH.'../upload/stream_setting/';
	}
	public function index() {
		$streamdata = $this->General_model->get_stream_setting($this->session->userdata('user_id'),'','');
		
		foreach ($streamdata as $key => $sd) {
			switch ($sd->key_option) {
				case 'tip_icon':
				$data['get_tipicon'][] = $sd;
				break;
				case 'tip_sound':
				$data['get_tipsound'][] = $sd;
				break;
				case 'sub_sound':
				$data['get_subsound'][] = $sd;
				break;
				case 'default_stream':
				$data['get_defaultstream'][] = $sd;
				break;
				case 'default_obs_stream':
				$data['get_defaultobsstream'][] = $sd;
				break;
				case 'force_start_stream':
				$data['force_start_stream'] = $sd->default_value;
				break;
				default:
				break;
			}
		}
		$data['get_user_detail'] = $this->User_model->get_user_detail($this->session->userdata('user_id'));
		$data['created_lobbys'] = $this->General_model->created_lobbys($this->session->userdata('user_id'));
		$data['social_links'] = $this->General_model->view_data('users_social_link',array('user_id' => $this->session->userdata('user_id')));
		// $data = json_decode($this->get_default_stream($_SESSION['user_id']));
		$default_obs_stream = array_search('default_obs_stream', array_column($streamdata, 'key_option'));

		(!empty($default_obs_stream) || $default_obs_stream == '0') ? $data['default_obs_stream'] = $streamdata[$default_obs_stream] : '';
		// $data['common_settings'] = json_decode($this->session->userdata('common_settings'));
		$new_stream_key = substr(chunk_split(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 4, '-'), 0, 39);
		// substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(8/strlen($x)) )),1,8);
		(!empty($this->session->userdata('new_stream_key'))) ? $new_stream_key = $this->session->userdata('new_stream_key') : 
			$this->session->set_userdata('new_stream_key', $new_stream_key);

		$data['random_string'] = (!empty($data['default_obs_stream']->default_stream_channel)) ? $data['default_obs_stream']->default_stream_channel : $this->session->userdata('new_stream_key');
		// $data['random_string'] = (!empty($data['common_settings']->stream_key)) ? $data['common_settings']->stream_key : $this->session->userdata('new_stream_key');

		$content = $this->load->view('streamsetting/streamsetting.tpl.php',$data,true);
		$this->render($content);
	}
	public function add_data() {
		if ($_GET['add_tipimg']) {
			$img = '';
			if (isset($_FILES["new_tip_img"]["name"]) && $_FILES["new_tip_img"]["name"]!='') {
		      	$file_name = str_replace(',','',$_FILES['new_tip_img']['name']);
		      	$file_size = $_FILES['new_tip_img']['size'];
		      	$file_tmp = $_FILES['new_tip_img']['tmp_name'];
		      	$file_type = $_FILES['new_tip_img']['type'];
		      	$file_name = str_replace('_-image-ini-file_','',$file_name);
		        $img = time().'_-image-ini-file_'.basename($file_name);
		        $ext = strtolower(end(explode('.',$img)));
		        $extensions= array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
		        $errors = '';
		        if (in_array($ext,$extensions) === false){
		        	$errors = "extension not allowed, please choose a JPEG or PNG or GIF file.";
		        }
		        if ($errors == '') {
		        	$target_file = $this->target_dir.$img;
		        	$get_tipicon = $this->General_model->get_stream_setting($_SESSION['user_id'],'','tip_icon');
		        	if (move_uploaded_file($file_tmp, $target_file)) {
		        		if (count($get_tipicon) == 0) {
		        			$insert_data = array(
		        				'user_id' => $_SESSION['user_id'],
		        				'tip_icon_imgs' => $img,
								'key_option' => 'tip_icon',
							);
							$data_inserted = $this->General_model->insert_data('stream_settings', $insert_data);
						} else {
							$tipicon_arr = $img;
							if (count($get_tipicon[0]->tip_icon_imgs) != 0) {
								$tipicon_arr = explode(',', $get_tipicon[0]->tip_icon_imgs);
								array_push($tipicon_arr, $img);
								$tipicon_arr = implode(',', $tipicon_arr);
							}
							$update_data_where = array('user_id' => $_SESSION['user_id'], 'key_option' => 'tip_icon');
							$update_row = array(
								'tip_icon_imgs' => $tipicon_arr
							);
							$update = $this->General_model->update_data('stream_settings', $update_row, $update_data_where);
						}
					}
					$this->session->set_flashdata('message_succ', 'Tip icon added');
				} else {
					$this->session->set_flashdata('message_err', $errors);
				}
				redirect(base_url().'Streamsetting');
			}
		} else if ($_GET['add_sound']) {
			$file = '';	
			$key_check = $_GET['add_sound'];
			$msg_var = ($key_check == 'tip_sound') ? 'Tip' : (($key_check == 'sub_sound') ? 'Subcription' : '' );
			if(isset($_FILES["new_audio"]["name"]) && $_FILES["new_audio"]["name"]!='') {
		      	$file_name = str_replace(',','',$_FILES['new_audio']['name']);
		      	$file_size = $_FILES['new_audio']['size'];
		      	$file_tmp = $_FILES['new_audio']['tmp_name'];
		      	$file_type = $_FILES['new_audio']['type'];
		      	$file_name = str_replace('_-audio-ini-mp3_','',$file_name);
		      	$file = time().'_-audio-ini-mp3_'.basename($file_name);
		        $ext = strtolower(end(explode('.',$file)));
		        $extensions = array("wav","mp3","wma","ogg","WAV","MP3","WMA","OGG");
		        $errors = '';
		        if (in_array($ext,$extensions)=== false) {
		        	$errors = "extension not allowed, please choose a MP3 or OGG or WMA file.";
		        }
		        if ($errors == '') {
		        	$target_file = $this->target_dir.$file;
		        	if ($key_check == 'tipsound') {

		        	}
		        	$getsound = $this->General_model->get_stream_setting($_SESSION['user_id'],'',$key_check);        	
		        	if (move_uploaded_file($file_tmp, $target_file)) {
		        		if (count($getsound) == 0) {
		        			$insert_data = array(
					            'user_id' => $_SESSION['user_id'],
					            'sound_audios' => $file,
								'key_option' => $key_check,
							);
							$data_inserted = $this->General_model->insert_data('stream_settings', $insert_data);
							if (!$data_inserted) {
								$errors = "error to insert";
							}
						} else {
				    		$tipsound_arr = $file;
					    	if (count($getsound[0]->sound_audios) != 0) {
						    	$tipsound_arr = explode(',', $getsound[0]->sound_audios);
						    	array_push($tipsound_arr, $file);
						    	$tipsound_arr = implode(',', $tipsound_arr);
					    	}
					    	$update_data_where = array('user_id' => $_SESSION['user_id'], 'key_option' => $key_check);
				            $update_row['sound_audios'] = $tipsound_arr;
				            $update = $this->General_model->update_data('stream_settings', $update_row, $update_data_where);
				            if (!$update) {
				            	$errors = "error to update";
				        	}
			      		}
			    	}	
			    	$this->session->set_flashdata('message_succ', $msg_var.' Sound added');
				} else {
					$this->session->set_flashdata('message_err', $errors);
				}
				redirect(base_url().'Streamsetting');
			}
		}
	}
	public function change_file_name() 
	{
		$key_type = $_GET['type'];
		if (!empty($_POST)) {
			$file_pattern = ($key_type == 'tip_icon')?'_-image-ini-file_':'';
			$file_pattern = ($key_type == 'tip_sound' || $key_type == 'sub_sound')?'_-audio-ini-mp3_':$file_pattern;

			$old_file_name = $_POST['old_file_name'];
			$edit_file_name = $_POST['edit_file_name'];

			$arr = array(
				'key_type' => $key_type,
				'user_id' => $this->session->userdata('user_id'),
				'file_name' => $old_file_name
			);
			$get_data = $this->General_model->get_chat_file($arr);
			if (!empty($get_data)) {
				$ext = strtolower(end(explode('.',$old_file_name)));
				$edit_file_name = str_replace(' ', '-', $edit_file_name);
				$edit_file_name = preg_replace('/[^A-Za-z0-9\-]/', '', $edit_file_name);
				$new_file_name = time().$file_pattern.$edit_file_name.'.'.$ext;
				$update_data_where = array(
					'user_id' => $this->session->userdata('user_id'),
					'key_option' => $key_type
				);
				if ($key_type == 'tip_icon') {
					$files_arr = str_replace($old_file_name,$new_file_name,$get_data[0]->tip_icon_imgs);
					$update_row['tip_icon_imgs'] = $files_arr;
					if ($old_file_name == $get_data[0]->tip_selicon_img) {
						$update_row['tip_selicon_img'] = $new_file_name;
					}
				} else if ($key_type == 'tip_sound' || $key_type == 'sub_sound') {
					$files_arr = str_replace($old_file_name,$new_file_name,$get_data[0]->sound_audios);
					$update_row['sound_audios'] = $files_arr;
					if ($old_file_name == $get_data[0]->selsound_audio) {
						$update_row['selsound_audio'] = $new_file_name;
					}
				}
				$update = $this->General_model->update_data('stream_settings', $update_row, $update_data_where);
				
	            if ($update && file_exists($this->target_dir.$old_file_name)) {
	            	$file_updated = rename($this->target_dir.$old_file_name,$this->target_dir.$new_file_name);
	            	if ($file_updated) {
	            		$this->session->set_flashdata('message_succ', 'file name updated');
	            	} else {
	            		$this->session->set_flashdata('message_err', 'file name not updated');
	            	}
	            } else {
	            	$this->session->set_flashdata('message_err', 'file name not updated');
	            }
	            redirect(base_url().'Streamsetting');
			}
		}
	}
	public function update_settings() {
		if (!empty($_POST)) {
			$key_option = $_GET['update_data'];
			$msg_var = ($key_option == 'tip_icon') ? 'Tip icon' : (($key_option == 'tip_sound')? 'Tip sound' : (($key_option == 'sub_sound')? 'Subcription sound' : '' ));
			$update_data_where = array(
				'user_id' => $_SESSION['user_id'],
				'key_option' => $key_option
			);
			if ($key_option == 'tip_icon') {
				$update_row['tip_selicon_img'] = $_POST['seltipicon'];
			} else if ($key_option == 'tip_sound' || $key_option == 'sub_sound') {
				$update_row['selsound_audio'] = $_POST['selsound'];
			}
            $update = $this->General_model->update_data('stream_settings', $update_row, $update_data_where);
            if ($update) {
            	$this->session->set_flashdata('message_succ', $msg_var.' Updated');
            } else {
            	$this->session->set_flashdata('message_err', $msg_var.' Not Updated');
            }
            header('location:' . base_url().'Streamsetting');
		}
	}	
	public function get_default_stream($user_id) {
		$res['get_defaultstream'] = $this->General_model->get_stream_setting($user_id,'','default_stream')[0];
		$res['count_get_defaultstream'] = count($res['get_defaultstream']);
		// call from ajax
		if ($_POST['data'] == 'json') {
			echo json_encode($res);
		} else {
			return json_encode($res);
		}
	}
	public function get_default_obs_stream($user_id) {
		$res['get_defaultstream'] = $this->General_model->get_stream_setting($user_id,'','default_obs_stream')[0];
		$res['count_get_defaultobsstream'] = count($res['get_defaultstream']);
		// call from ajax
		if ($_POST['data'] == 'json') {
			echo json_encode($res);
		} else {
			return json_encode($res);
		}
	}
	public function update_default_stream() {
		if (!empty($_POST)) {
			$data = $this->get_default_stream($_SESSION['user_id']);
			$data = json_decode($data);
			if (empty($data->get_defaultstream)) {
				$insert_data = array(
		            'user_id' => $_SESSION['user_id'],
		            'default_stream_channel' => ($_POST['default_stream'] != '') ? $_POST['default_stream'] : null,
					'key_option' => 'default_stream',
		        );
		        $inserted = $this->General_model->insert_data('stream_settings', $insert_data);
		        $r = ($inserted) ? $this->session->set_flashdata('message_succ', 'Default Stream Inserted') : $this->session->set_flashdata('message_err', 'Error to Insert Default Stream') ;
			} else {
				$update_data_where = array('user_id' => $_SESSION['user_id'], 'key_option' => 'default_stream');
	            $update_row['default_stream_channel'] = ($_POST['default_stream'] != '') ? $_POST['default_stream'] : null;

	            $update = $this->General_model->update_data('stream_settings', $update_row, $update_data_where);
	            ($update) ? $this->session->set_flashdata('message_succ', 'Default Stream Updated') : $this->session->set_flashdata('message_err', 'Error to Updated Default Stream');
			}
			redirect(base_url().'Streamsetting');
		}
	}
	public function delete_data() {
		if ((isset($_GET['sound']) || isset($_GET['img'])) && isset($_GET['delete_file'])) {
			$key_option = ($_GET['sound']) ? $_GET['sound'] : $_GET['img'];
			$delete_file = $_GET['delete_file'];
			$msg_var = ($key_option == 'tip_icon') ? 'Tip icon' : (($key_option == 'tip_sound')? 'Tip sound' : (($key_option == 'sub_sound')? 'Subcription sound' : '' ));
			$update_data_where = array('user_id' => $_SESSION['user_id'], 'key_option' => $key_option);
			$get_files = $this->General_model->get_stream_setting($_SESSION['user_id'],'',$key_option);
			
			if ($key_option == 'tip_icon') {
				$files_arr = explode(',', $get_files[0]->tip_icon_imgs);
				if (($key = array_search($delete_file, $files_arr)) !== false) {
				    unset($files_arr[$key]);
				    unlink($this->target_dir.$delete_file);
				}
				$files_array = implode(',', $files_arr);
				$update_row['tip_icon_imgs'] = $files_array;
			   
			    if ($get_files[0]->tip_selicon_img == $delete_file) {
			        $update_row = array(
			        	'tip_selicon_img' => null,
			            'tip_icon_imgs' => implode(',', $files_arr)
			        );
				} else if (count($files_arr) == 0) {
					$update_row = array(
			            'tip_icon_imgs' => null
			        );
				}
			} else if ($key_option == 'tip_sound' || $key_option == 'sub_sound') {
				$files_arr = explode(',', $get_files[0]->sound_audios);
				if (($key = array_search($delete_file, $files_arr)) !== false) {
				    unset($files_arr[$key]);
				    unlink($this->target_dir.$delete_file);
				}
				$files_array = implode(',', $files_arr);
				$update_row['sound_audios'] = $files_array;

				if ($get_files[0]->selsound_audio == $delete_file) {
			        $update_row = array(
			        	'selsound_audio' => null,
			            'sound_audios' => implode(',', $files_arr)
			        );
				} else if (count($files_arr) == 0) {
					$update_row = array(
			            'sound_audios' => null
			        );
				}
			}
	        $update = $this->General_model->update_data('stream_settings', $update_row, $update_data_where);
	        if ($update) {
	        	$this->session->set_flashdata('message_succ', $msg_var.' Deleted');
	        } else {
	        	$this->session->set_flashdata('message_succ', $msg_var.' Not Deleted');
	        }
	        header('location:' . base_url().'Streamsetting');
		}
	}

	public function update_lobby_url() {
		$lobby_id = $_POST['lobby_id'];
		preg_replace(['/[^a-z0-9\s]/gi','/[_\s]/g'], ['_', '_'], $_POST['custom_name']);
		$custom_name = $_POST['custom_name'];
		$query_check_custom_name = $this->db->query('SELECT custom_name FROM lobby where status =1 and id != '.$lobby_id.' and custom_name="'.$custom_name.'"');
		$result_check_custom_name = $query_check_custom_name->result();
		if (!empty($result_check_custom_name)){
			$this->session->set_userdata('message_err', 'Live Lobby link already exists');
			redirect(base_url().'livelobby/lobbyadd');
		}
		$update_row = array('custom_name' => strtolower($custom_name));
		if (!empty($lobby_id) && !empty($custom_name)) {
			$update_data_where = array('user_id' => $_SESSION['user_id'], 'id' => $lobby_id);
			$update = $this->General_model->update_data('lobby', $update_row, $update_data_where);
		} else {
			$update_data_where = array('user_id' => $_SESSION['user_id']);           
            $update = $this->General_model->update_data('user_detail', $update_row, $update_data_where);
		}
		($update) ? $this->session->set_flashdata('message_succ', 'Link Updated') : $this->session->set_flashdata('message_succ', 'Error to link Updated') ;
            redirect(base_url().'Streamsetting');
	}
	public function check_lobby_name() {
		$lobby_name = $_POST['lobby_name'];
		$res['count_lby'] = 0;
		if (!empty($lobby_name)) {
			$this->db->select('l.*');
			$this->db->from('lobby l');
			$this->db->join('group_bet gb','gb.lobby_id = l.id');
    	$this->db->where('l.status',1);
    	$this->db->where('gb.bet_status',1);
    	$this->db->where('l.custom_name',strtolower($lobby_name));
    	// $this->db->where('l.user_id !=',$this->session->userdata('user_id'));
    	$res['view_data'] = $this->db->get()->result();
    	$res['count_lby'] = count($res['view_data']);
		}
		if ($res['count_lby'] && $res['count_lby'] != 0) {
			if ($res['view_data'][0]->user_id == $this->session->userdata('user_id')) {
				$res['error'] = 'You already have lobby with this name or you are trying same name.';
				if (!empty($_POST['lobby_id']) && $res['view_data'][0]->id == $_POST['lobby_id']) {
					$res['error'] = '';
				}
			} else {
				$res['error'] = 'Lobby is exist with this name. Please try another name..';
			}
		} else if (strpos($lobby_name, 'account') !== false) {
			$res['error'] = "Can't use Account keyword. Please try another name..";
		} else {
			$res['error'] = '';
		}
		echo json_encode($res);
		exit();
	}
	public function check_default_user_link() {
		$default_link_name = $_POST['default_link_name'];
		$res['count_lby'] = 0;
		if (!empty($default_link_name)) {
			$this->db->select('ud.*');
			$this->db->from('user_detail ud');
			$this->db->where('ud.custom_name',strtolower($default_link_name));
	    	$user_custom_detail = $this->db->get()->result();
	    	$this->db->select('l.*');
			$this->db->from('lobby l');
			$this->db->join('group_bet gb','gb.lobby_id = l.id');
	    	$this->db->where('l.status',1);
	    	$this->db->where('gb.bet_status',1);
	    	$this->db->where('l.custom_name',strtolower($default_link_name));
	    	$lobby_exist_detail = $this->db->get()->result();
	    	$res['view_data'] = array_merge($user_custom_detail,$lobby_exist_detail);
	        $res['count_lby'] = count($res['view_data']);
		}
		if ($res['count_lby'] && $res['count_lby'] != 0) {
			$res['error'] = 'User or live lobby is exist with this name. Please try another name..';
		} else if (strpos($lobby_name, 'account') !== false) {
			$res['error'] = "Can't use Account keyword. Please try another name..";
		} else {
			$res['error'] = '';
		}
		echo json_encode($res);
		exit();
	}
	public function update_social_link() {

		 // $resultArr = array_combine($_POST['social_id'], $_POST['option_label']);
	
		
		$user_id = $this->session->userdata('user_id');
		$r = $this->General_model->delete_data('users_social_link',array('user_id'=>$user_id));
		foreach($_POST['social_id'] as $key => $val){

			$insertArr[] = array(
				'social_link' => $_POST['option_label'][$key],
				'user_id' => $user_id,
				'social_id' => $val,
				'created_at' => date('Y-m-d H:i:s')
			);
		}
		
		$this->db->insert_batch('users_social_link',$insertArr);
		$this->session->set_flashdata('message_succ', 'Social Link Updated');
		redirect(base_url().'Streamsetting');
	}
	public function getFollowUserDetails() {
		$user_id = $this->session->userdata('user_id');
		$follow_id = $_POST['follow_id'];
		$totalRow = $this->General_model->get_num_rows('follower_users',array('followers_id' => $user_id,'following_id' => $follow_id));
		$result['status'] = 0;
		if ($totalRow == 0) {
			$insert_id = $this->General_model->insert_data('follower_users',array('followers_id' => $user_id,'following_id' => $follow_id,'	is_follow' => 1,'created_at' => date('Y-m-d H:i:s')));
			$result['status'] = 1;
		}
		// $social_link = $this->General_model->view_data('users_social_link',array('user_id' => $follow_id));
		$this->db->select('sl.*, ss.*');
		$this->db->from('users_social_link sl');
		$this->db->join('social_service ss','ss.id=sl.social_id');
		$this->db->where('sl.user_id',$follow_id);
		$query=$this->db->get();
		$social_link = $query->result();
		
		$result['social_link_div'] = '';
		if (!empty($social_link)) {
			$result['social_link_div'] = '<div class="col-sm-12"><h2 class="text-left social_links_plyr">';
			foreach ($social_link as $key => $sl) {
				if($sl->image == ''){
					$sl->image = 'default_social.png';
				}
				$result['social_link_div'] .= '<a target="_blank" href="'.$sl->social_link.'" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="'.$sl->name.'"><span><img src="'.base_url().'upload/all_images/'.$sl->image.'" height="50px" width="50px"></span></a>';
			}
			$result['social_link_div'] .='</h2></div>';
			$result['data'] = $social_link; 
		} else {
			$result['data'] = [];
		}
		echo json_encode($result);
		exit();
	}
	public function getfollower_following() {
		$this->db->select('COUNT(fu.id) as followercount, GROUP_CONCAT(fu.followers_id SEPARATOR ",") as followers_arr');
		$this->db->from('follower_users fu');
		($table_cat !='') ? $this->db->where('lg.table_cat',$table_cat) : '';
		($user_id !='') ? $this->db->where('lg.user_id',$user_id) : $this->db->where('lg.user_id',$this->session->userdata('user_id'));
		$query = $this->db->get();
		$res = ($numrow=='single') ? $query->row() : $query->result();
	}	

	public function remove_all_social_links() {
		$user_id = $this->session->userdata('user_id');
		$r = $this->General_model->delete_data('users_social_link',array('user_id'=>$user_id));
		$this->session->set_flashdata('message_succ', 'Social links removed successfully');
		redirect(base_url().'Streamsetting');
	}
	public function streaming_key_update(){
		if (!empty($_POST['streaming_key'])) {
			$streaming_key = $_POST['streaming_key'];
			$data = $this->get_default_obs_stream($_SESSION['user_id']);
			$data = json_decode($data);
			if (empty($data->get_defaultobsstream)) {
				$insert_data = array(
		            'user_id' => $_SESSION['user_id'],
					'key_option' => 'default_obs_stream',
					'default_stream_channel' => $streaming_key,
		        );
		        $r = $this->General_model->insert_data('stream_settings', $insert_data);
			} else {
				$update_data_where = array('user_id' => $this->session->userdata('user_id'), 'key_option' => 'default_obs_stream');
				$update_row['default_stream_channel'] = $streaming_key;
	            $r = $this->General_model->update_data('stream_settings', $update_row, $update_data_where);
	        }
	        if ($r) {
				$update_data_where = array('user_id' => $this->session->userdata('user_id'));
				$update_row['obs_stream_channel'] = $streaming_key;
				$this->General_model->update_data('lobby_fans', $update_row, $update_data_where);
				$res['streaming_key'] = $streaming_key;
				echo json_encode($res);
				exit();
			}
	    }		
		// if (!empty($_POST['streaming_key'])) {
		// 	$streaming_key = $_POST['streaming_key'];
		// 	$common_settings = json_decode($this->session->userdata('common_settings'));
		// 	$common_settings->stream_key = $streaming_key;
		// 	$r = $this->General_model->update_data('user_detail',array('common_settings' => json_encode($common_settings)),array('user_id'=>$this->session->userdata('user_id')));
		// 	if ($r) {
		// 		$res['common_settings'] = $common_settings;
		// 		echo json_encode($res);
		// 		exit();
		// 	}
		// }
	}
	public function reset_streaming_key(){
		$del_where = array('key_option' => 'default_obs_stream', 'user_id' => $this->session->userdata('user_id'));
		$r = $this->General_model->delete_data('stream_settings', $del_where);
		if($r){
			$this->session->set_flashdata('message_succ', 'Stream reset successfully');
		}else{
			$this->session->set_flashdata('message_succ', 'Unable to reset') ;
		}
		$this->session->unset_userdata('new_stream_key');
		$streaming_key = substr(chunk_split(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 4, '-'), 0, 39);
		$data = $this->get_default_obs_stream($_SESSION['user_id']);
		$data = json_decode($data);
		if (empty($data->get_defaultobsstream)) {
			$insert_data = array(
	            'user_id' => $_SESSION['user_id'],
				'key_option' => 'default_obs_stream',
				'default_stream_channel' => $streaming_key,
	        );
	        $r = $this->General_model->insert_data('stream_settings', $insert_data);
		} else {
			$update_data_where = array('user_id' => $this->session->userdata('user_id'), 'key_option' => 'default_obs_stream');
			$update_row['default_stream_channel'] = $streaming_key;
            $r = $this->General_model->update_data('stream_settings', $update_row, $update_data_where);
        }
        if ($r) {
			$update_data_where = array('user_id' => $this->session->userdata('user_id'));
			$update_row['obs_stream_channel'] = $streaming_key;
			$this->General_model->update_data('lobby_fans', $update_row, $update_data_where);
		}
		redirect(base_url().'Streamsetting');

		// $common_settings = json_decode($this->session->userdata('common_settings'));
		// unset($common_settings->stream_key);
		// $r = $this->General_model->update_data('user_detail',array('common_settings' => json_encode($common_settings)),array('user_id'=>$this->session->userdata('user_id')));
		// ($r) ? $this->session->set_flashdata('message_succ', 'Stream reset successfully') : $this->session->set_flashdata('message_succ', 'Unable to reset') ;
		// redirect(base_url().'Streamsetting');
	}

	public function clip($savedStream=''){
		if($savedStream != '' && $savedStream != null){
			if(file_exists('upload/streams/recorded_streams/'.$savedStream)){
				$data['stream_url'] = base_url().'upload/streams/recorded_streams/'.$savedStream;
			}else{
				$this->session->set_flashdata('message_err', 'Stream offline or not available');
	        	redirect($_SERVER['HTTP_REFERER']);
			}
		}else{
			// get stream key
			// $stream_setting=$this->General_model->get_stream_setting($_GET['streamer'],'','default_obs_stream')[0];
			$stream_setting=$this->General_model->view_data('lobby_fans',array('user_id'=>$_GET['streamer'],'lobby_id'=>$_GET['lobby_id']))[0];
		    // print_r($stream_setting->default_stream_channel);exit;
		    // root path where we need to store stream
		    $output_dir = str_replace(array('application\controllers','application/controllers'),'upload/streams',__DIR__);
		    if(count(glob($output_dir."/".$stream_setting['obs_stream_channel']."/*.ts")) == 0){
		    	$this->session->set_flashdata('message_err', 'Stream offline or not available');
	        	redirect($_SERVER['HTTP_REFERER']);
		    }

		    // '|' saperated file names with whole path (ts file chunks)
		    $filelist = 'concat:';
		    if ($handle = opendir($output_dir.'/'.$stream_setting['obs_stream_channel'].'/')) {
		        while ($entry = readdir($handle)) {
		            if ($entry != "." && $entry != ".." && $entry != 'FileName.webm' && $entry != 'stream') {
		              $filelist .= $output_dir.'/'.$stream_setting['obs_stream_channel'].'/'.$entry.'|';
		            }
		        }
		        closedir($handle);
		    }
		    $filelist = rtrim($filelist,'|');

		    // process to convert ts files in mp4 stream
		    $filename = uniqid().'.mkv';
		    $output = $output_dir.'/'.$stream_setting['obs_stream_channel'].'/stream/'.$filename;
		    $process = exec('ffmpeg -i "'.$filelist.'" -c:a copy -c:v libx264 -preset ultrafast '.$output, $result);
		    $filename1 = uniqid().'.mp4';
		    $output1 = $output_dir.'/'.$stream_setting['obs_stream_channel'].'/stream/'.$filename1;
		    $process1 = exec("ffmpeg -sseof -120 -i ".$output." -c:a copy -c:v copy ".$output1."", $result1);
		    unlink($output);
		    $data['stream_url'] = base_url().'upload/streams/'.$stream_setting['obs_stream_channel'].'/stream/'.$filename1;
		}
		$content=$this->load->view('streamsetting/create_clip.tpl.php',$data,true);
		$this->render($content);
	}

	public function create_clip(){
		$data = $this->input->post();

		// root path where we need to store stream
		$output_dir = str_replace(array('application\controllers','application/controllers'),"upload/clips",__DIR__);
		$default='mp4';
        $video_file = $data['stream_url'];
        $date = date("Y-m-d H:i:s");
        $uploadtime = strtotime($date);
        
        // upload path

        // if(!isset($_POST["extension"]) || $_POST["extension"] == ""){
        //     echo "Please set the output extension.";
        //     return;
        // }
        $ext = $data["extension"]; // output extension    
        if($ext == "none") {
            $ext = $default;
        }            
        $name = $uploadtime ."_". uniqid();
        
        // put file to input directory to make it easier to be processed with ffmpeg
        // echo "done";
        // change php working directory to where ffmpeg binary file reside
        chdir("upload/binaries");
        $start_from = "00:00:00";                
        // check the specified starting time
        if(isset($data["start_from"]) && $data["start_from"] != ""){
            $start_from = $data["start_from"];
        }                
        
        $length = 30;
        // check the specified duration
        if(isset($data["length"]) && $data["length"] != ""){
            $length = $data["length"];
        }
        $output = "$output_dir/$name.$ext";
        $process = exec("ffmpeg -t $length -ss $start_from -i $video_file -b:v 2048k -ar 22050 -strict -2 $output 2>&1", $result);
        // delete uploaded file from input folder to reserve disk space
        // print_r($result);exit;
        $insert_data = array(
			'user_id' => $_SESSION['user_id'],
			'lobby_id' => $data['lobby_id'],
			'streamer_id' => $data['streamer_id'],
			'clip' => $name.'.'.$ext,
			'description' => $data["description"]
		);
		$data_inserted = $this->General_model->insert_data('stream_clips', $insert_data);         
        $response['url'] = base_url().'streamsetting/viewClip/'.$name.'.'.$ext;
        $response['description'] = $data["description"];
        echo json_encode($response);
	}

	public function uploadBlob(){
		// function to make chunks of ts files from recorded webm files

		$data = $this->input->post();

		// root path where we need to store stream
		$output_dir = str_replace(array('application\controllers','application/controllers'),"upload/streams",__DIR__);
		$ext = 'ts';

		$fileName = '';
	    $tempName = '';
	    $file_idx = '';
	    if (!empty($_FILES['file'])) {
	        $file_idx = 'file';
	        $fileName = $_POST['video-filename'];
	        $tempName = $_FILES[$file_idx]['tmp_name'];
	    }
	    if (empty($fileName) || empty($tempName)) {
	        if(empty($tempName)) {
	            echo 'Invalid temp_name: '.$tempName;
	            // return;
	        }

	        echo 'Invalid file name: '.$fileName;
	        // return;
	    }

	    $filePath = './upload/streams/'.$data['stream_key'].'/'. $fileName;
	    if (!is_dir('./upload/streams/'.$data['stream_key'].'/')) {
            mkdir('./upload/streams/'.$data['stream_key'].'/', 0777, true);
            mkdir('./upload/streams/'.$data['stream_key'].'/stream/', 0777, true);
        }
	    // make sure that one can upload only allowed audio/video files
	    $allowed = array(
	        'webm',
	        'wav',
	        'mp4',
	        'mkv',
	        'mp3',
	        'ogg'
	    );
	    $extension = pathinfo($filePath, PATHINFO_EXTENSION);
	    if (!$extension || empty($extension) || !in_array($extension, $allowed)) {
	        echo 'Invalid file extension: '.$extension;
	        // return;
	    }
	    $filelist = array();
		if ($handle = opendir($output_dir.'/'.$data['stream_key'].'/')) {
		    while ($entry = readdir($handle)) {
	        	if ($entry != "." && $entry != ".." && $entry != $fileName && $entry != 'stream') {
	        		$filelist[] = $entry;
	        	}
		    }
		    closedir($handle);
		}
	    if (!move_uploaded_file($tempName, $filePath)) {
	        if(!empty($_FILES["file"]["error"])) {
	            $listOfErrors = array(
	                '1' => 'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
	                '2' => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
	                '3' => 'The uploaded file was only partially uploaded.',
	                '4' => 'No file was uploaded.',
	                '6' => 'Missing a temporary folder. Introduced in PHP 5.0.3.',
	                '7' => 'Failed to write file to disk. Introduced in PHP 5.1.0.',
	                '8' => 'A PHP extension stopped the file upload. PHP does not provide a way to ascertain which extension caused the file upload to stop; examining the list of loaded extensions with phpinfo() may help.'
	            );
	            $error = $_FILES["file"]["error"];

	            if(!empty($listOfErrors[$error])) {
	                echo $listOfErrors[$error];
	            }
	            else {
	                echo 'Not uploaded because of error #'.$_FILES["file"]["error"];
	            }
	        }else {
	            echo 'Problem saving file: '.$tempName;
	        }
	        echo 'success';
	    }
	    if(count($filelist) > 0){
			rsort($filelist);
			$name = pathinfo($filelist[0], PATHINFO_FILENAME) + 1;
		}else{
			$name = '1';
		}
		$input = $output_dir."/".$data['stream_key']."/".$fileName;
		$output = $output_dir."/".$data['stream_key']."/".$name.'.'.$ext;
        $process = exec("ffmpeg -i $input $output 2>&1", $result);
        unlink('./upload/streams/'.$data['stream_key'].'/'.$fileName);
	}

	public function viewClip($clip=''){
		if($clip != '' && $clip != null){
			$clip_data = $this->General_model->view_data('stream_clips',array('clip'=>$clip))[0];
			if(!empty($clip_data) && file_exists('upload/clips/'.$clip)){
				$data['clip_url'] = base_url().'upload/clips/'.$clip;
				$data['description'] = $clip_data['description'];
				$data['success'] = 1;
			}else{
				$data['success'] = 0;
				$data['error_message'] = 'Clip not available or moved';
			}
		}else{
			$data['success'] = 0;
			$data['error_message'] = 'Clip not found for given URL';
		}
		$content = $this->load->view('streamsetting/view_clip.tpl.php',$data,true);
		$this->render($content);
	}

	public function removeStreams(){
		// membership data
		$membership = $this->User_model->get_activated_membership($this->input->post('streamer'));
		// root path where we need to store stream
		$output_dir = str_replace(array('application\controllers','application/controllers'),'upload/streams',__DIR__);

		// '|' saperated file names with whole path (ts file chunks)
	    $filelist = 'concat:';
	    if ($handle = opendir($output_dir.'/'.$this->input->post('key').'/')) {
	        while ($entry = readdir($handle)) {
	            if ($entry != "." && $entry != ".." && $entry != 'FileName.webm' && $entry != 'stream') {
	              $filelist .= $output_dir.'/'.$this->input->post('key').'/'.$entry.'|';
	            }
	        }
	        closedir($handle);
	    }
	    $filelist = rtrim($filelist,'|');

	    // make whole stream from ts files to recorded_streams directory
	    $filename = uniqid().'_'.time().'_'.$this->input->post('streamer').'.mkv';
	    $output = $output_dir.'/recorded_streams/'.$filename;
	    $process = exec('ffmpeg -i "'.$filelist.'" -c:a copy -c:v libx264 -preset ultrafast '.$output, $result);
	    $filename1 = uniqid().'_'.time().'_'.$this->input->post('streamer').'.mp4';
	    $output1 = $output_dir.'/recorded_streams/'.$filename1;
	    $process1 = exec("ffmpeg -i ".$output." -c:a copy -c:v copy ".$output1."", $result1);
	    unlink($output);
	    
	    if(file_exists('upload/streams/recorded_streams/'.$filename1)){
	    	$get_stream_setting = $this->General_model->view_data('site_settings', array('option_title' => 'default_stream_record_limit'))[0];
	    	$stream_lmt = (!empty($membership)) ? $membership->stream_record_limit : $get_stream_setting['option_value'];
	    	if($stream_lmt > 0){
	    		// no of saved recordings for perticular user
	    		$total_recordings = $this->db->where('streamer_id', $this->input->post('streamer'))->from("stream_recordings")->count_all_results();

	    		// check if limit over or not for current plan of membership
	    		if($total_recordings >= $stream_lmt){
	    			// unlink and remove past record(stream) on add new record(stream)
	    			$old_rows = $this->db->query('SELECT * FROM `stream_recordings` WHERE streamer_id = '.$this->input->post('streamer').' AND id NOT IN(SELECT * FROM (SELECT id FROM stream_recordings s2 where s2.streamer_id = '.$this->input->post('streamer').' ORDER BY id DESC LIMIT '.($stream_lmt - 1).') as s3)');
					$old_rows = $old_rows->result_array();
					foreach ($old_rows as $value) {
						unlink($output_dir.'/recorded_streams/'.$value['stream']);
						$this->db->where('id', $value['id'])->from("stream_recordings")->delete();
					}
	    		}

	    		// insert stream recording to DB table
	    		$lobby_detail = $this->General_model->lobby_details($this->input->post('lobby'));
	    		$folder_stat = stat($output_dir.'/recorded_streams');
			    $insert_data = array(
					'stream' => $filename1,
					'streamer_id' => $this->input->post('streamer'),
					'lobby_id' => $this->input->post('lobby'),
					'description'=>$lobby_detail[0]['custom_name']
				);
				$data_inserted = $this->General_model->insert_data('stream_recordings', $insert_data);
	    	}else{
	    		unlink($output1);
	    	}

			// remove all data from stream key folder of perticular key
			$path = './upload/streams/'.$this->input->post('key').'/';
			if (is_dir($path) === true)
		    {
		        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::CHILD_FIRST);

		        foreach ($files as $file)
		        {
		            if (in_array($file->getBasename(), array('.', '..')) !== true)
		            {
		                if ($file->isDir() === true)
		                {
		                    rmdir($file->getPathName());
		                }

		                else if (($file->isFile() === true) || ($file->isLink() === true))
		                {
		                    unlink($file->getPathname());
		                }
		            }
		        }

		        return rmdir($path);
		    }

		    else if ((is_file($path) === true) || (is_link($path) === true))
		    {
		        return unlink($path);
		    }

		    return false;
	    }
	    return false;
	}

	public function savedStreams($stream=''){
		if($stream != '' && $stream != null){
			$stream_data = $this->General_model->view_data('stream_recordings',array('stream'=>$stream))[0];
			if(!empty($stream_data) && file_exists('upload/streams/recorded_streams/'.$stream)){
				$data['stream_url'] = base_url().'upload/streams/recorded_streams/'.$stream;
				$data['description'] = $stream_data['description'];
				$data['success'] = 2;
			}else{
				$data['success'] = 0;
				$data['error_message'] = 'Stream not available or moved';
			}
		}else{
			$streams = $this->General_model->view_data_by_order('stream_recordings',array('streamer_id' => $this->session->userdata('user_id')),'id','DESC');
			$data['streams'] = $streams;
			$data['success'] = 1;
		}
		$content = $this->load->view('streamsetting/saved_streams.tpl.php',$data,true);
		$this->render($content);
	}

	public function save_stream_description(){
		$update_data_where = array('id' => $_POST['id']);
		$update_row = array(
			'description' => $_POST['description']
		);
		$update = $this->General_model->update_data('stream_recordings', $update_row, $update_data_where);
		echo $_POST['id'];
	}
	public function force_start_stream() {
		if (!empty($_POST['force_start_stream'])) {
			$force_start_stream = $_POST['force_start_stream'];
			$data = $this->General_model->view_data('stream_settings', array('key_option' => 'force_start_stream', 'user_id' => $this->session->userdata('user_id')));
			
			if (empty($data)) {
				$insert_data = array(
		            'user_id' => $this->session->userdata('user_id'),
					'key_option' => 'force_start_stream',
					'default_value' => $force_start_stream,
		        );
		        $r = $this->General_model->insert_data('stream_settings', $insert_data);
			} else {
				$update_data_where = array('user_id' => $this->session->userdata('user_id'), 'key_option' => 'force_start_stream');
				$update_row['default_value'] = $force_start_stream;
	            $r = $this->General_model->update_data('stream_settings', $update_row, $update_data_where);
	        }
	        
	        $res['force_start_stream'] = $force_start_stream;
        	echo json_encode($res);
        	exit();
		}

	}	

	public function auto_sync_stream(){
		if (!empty($_POST['auto_sync_stream'])) {
			$common_settings = json_decode($this->session->userdata('common_settings'), true);
            $common_settings['auto_sync_stream'] = $_POST['auto_sync_stream'];
            $this->General_model->update_data('user_detail',array('common_settings' => json_encode($common_settings)),array('user_id'=>$this->session->userdata('user_id')));
            $res['auto_sync_stream'] = $_POST['auto_sync_stream'];
        	echo json_encode($res);
        	exit();
		}
	}

}

