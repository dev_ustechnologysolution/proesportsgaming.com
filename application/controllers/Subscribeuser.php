<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Subscribeuser extends My_Controller {
    function __construct() {
        parent::__construct();
        // Load helpers
        $this->load->helper('url');
        $this->load->helper('file');
        $this->load->model('Paypal_payment_model');
    }
    function Set_player_subscription_plan() {
        if ($_POST['user_to'] !='' && $_POST['user_from'] !='' && $_POST['subscribe_amount'] && $_POST['subscribe_plan']) {
            $_SESSION['redirect_lobby_id'] = ($_POST['redirect_lobby_id']) ? $_POST['redirect_lobby_id'] : '' ;

            if (!empty($_POST['payment_id']) && !empty($_POST['subscribed_plan'])) {
                $_SESSION['payment_id'] = $_POST['payment_id'];
                $_SESSION['subscribed_plan'] = $_POST['subscribed_plan'];
            } else {
                unset($_SESSION['payment_id']);
                unset($_SESSION['subscribed_plan']);
            }

            $subscribe_plan_id = $_POST['subscribe_plan'];
            $subscribe_plan_detail = $this->General_model->view_single_row('subscription_plans', array('id'=>$subscribe_plan_id),'*');

            $user_from = $_SESSION['user_id'];
            $user_to = $_POST['user_to'];
            
            $data['subscribe_amount'] = $subscribe_plan_detail['amount']+$subscribe_plan_detail['fees'];
            $data['subscribe_plan'] = $subscribe_plan_detail['id'];

            $payer_detail = $this->User_model->user_detail();
            $data['payer_email'] = $payer_detail[0]['email'];
            $data['returnurl'] = $data['cancelurl'] = base_url().'subscribeuser/get_subscribe_profile_active';
            $data['custom'] = 'userto&'.$user_to.'&subscribe_plan&'.$subscribe_plan_detail['id'];
            $data['title'] = $subscribe_plan_detail['title'].' Subscription Plan.';

            $data['notifyurl'] = base_url().'subscribeuser/Getdata';
            $data['billingtype'] = 'RecurringPayments';
            $response = $this->Paypal_payment_model->Set_express_checkout($data);
            if($response['redirect'] != ''){
                $res['redirect'] = $response['redirect'];
            } else {
                $res['errors'] = array('Errors'=>$response['errors']);
            }
            echo json_encode($res);
            exit();
        }
    }
    function get_subscribe_profile_active($token=''){
        if ($_GET['token']) {
            $token = $_GET['token'];
        } 
        $result = $this->Paypal_payment_model->Get_express_checkout_details($token);
        if($result['status'] == 1) {
            $PayPalResult = $result['data'];
            $implode_custom_data = explode('&', $PayPalResult['CUSTOM']);

            $subscribe_plan_detail = $this->General_model->view_single_row('subscription_plans', array('id'=>$implode_custom_data[3]),'*');
            $my_user_name = $this->General_model->get_username($_SESSION['user_id']);
            $plan_description = $subscribe_plan_detail['title']. ' Subscription Plan.';
            $response = $this->Paypal_payment_model->CreateRecurringPaymentsProfile($PayPalResult,$my_user_name,$plan_description,'Month');
            if($response['status'] == 0) {
                $this->error($response['data']);
            } else {
                $this->save_paypal_subscription($response['data']);
            } 
        } else {
            $this->error($result['data']);
        }
    }
    function error($errors){
        $content=$this->load->view('subscribe/subscription_error.tpl.php',true);
        $this->render($content);
    }
    function save_paypal_subscription($getdetails) {
        $this->check_user_page_access();
        if (isset($_SESSION['payment_id']) && $_SESSION['payment_id'] !='' && isset($_SESSION['subscribed_plan']) && $_SESSION['subscribed_plan'] !='') {
            $payment_id = $_SESSION['payment_id'];
            $plan_id = $_SESSION['subscribed_plan'];

            $data['user_subscription_id'] = $payment_id;
            $data['user_subscription_opt'] = 'Unsubscribe';
            $data['redirect'] = 'false';
            $this->unsubscribe_subscribe_plan($data); 
        }
        $getdetails[] = $this->paypal_pro->GetExpressCheckoutDetails($_GET['token']);
        $implode_custom_data = explode('&', $getdetails[0]['CUSTOM']);
        $user_to = $implode_custom_data[1];
        $plan = $implode_custom_data[3];
        $subscribe_plan_detail = $this->General_model->view_single_row('subscription_plans', array('id'=>$plan),'*');
        $planamt = $subscribe_plan_detail['amount'];
        $planamt = number_format((float) $planamt, 2, '.', '');
        $paypal_subscriptions_data = array(
            'token_id' => $getdetails[0]['TOKEN'],
            'transaction_id' => $getdetails['PROFILEID'],
            'user_from' => $this->session->userdata('user_id'),
            'user_to' => $user_to,
            'subscribption_plan_id' => $plan,
            'payment_method' => '',
            'validity' => '',
            'valid_from' => $getdetails['REQUESTDATA']['PROFILESTARTDATE'],
            'valid_to' => '',
            'amount' => $planamt,
            'fee' => $subscribe_plan_detail['fees'],
            'currency_code' => $getdetails[0]['CURRENCYCODE'],
            'payer_email' => $getdetails['REQUESTDATA']['EMAIL'],
            'subscribe_status' => 1,
            'created_at' => date('Y-m-d H:i:s', time())
        );
        $r = $this->General_model->insert_data('paypal_subscriptions', $paypal_subscriptions_data);
        if ($r) {
            $data['getdetails'] = $getdetails;
            $data['plan'] = $subscribe_plan_detail['title'];
            $lobby_fee = $this->General_model->view_all_data('admin_lobby_fee', 'id', 'asc');
            $data['planamt'] = $planamt; 
            $data['subscribe_amount'] = $getdetails['REQUESTDATA']['AMT'];
            if ($_SESSION['redirect_lobby_id'] != '') {
                $subsentuser = '<b>'.$this->General_model->get_my_fan_tag($_SESSION['redirect_lobby_id'],$user_to)->fan_tag.'</b>';
                $insert_data = array(
                    'message' => 'Subscribed to '.$subsentuser.' with '.$subscribe_plan_detail['title'].' plan for amount of <span class="tipamount">$ '.$planamt.'</span>',
                    'attachment' => '',
                    'message_type' => 'subscription_message'
                );
                $message_id = $this->General_model->insert_data('xwb_messages', $insert_data);
                if ($message_id) {
                    $lobby_insert_data           = array(
                        'lobby_id' => $_SESSION['redirect_lobby_id'],
                        'user_id' => $_SESSION['user_id'],
                        'message_id' => $message_id,
                        'status' => 1,
                        'date' => date('Y-m-d H:i:s', time())
                    );
                    $lobby_group_conversation_id = $this->General_model->insert_data('lobby_group_conversation', $lobby_insert_data);
                    if ($lobby_group_conversation_id) {
                        $options                   = array(
                            'select' => 'lgp.*,xm.message,xm.attachment,xm.message_type,ud.name,ud.is_admin',
                            'table' => 'lobby_group_conversation lgp',
                            'join' => array(
                                'xwb_messages xm' => 'xm.id = lgp.message_id',
                                'user_detail ud' => 'ud.user_id = lgp.user_id'
                            ),
                            'where' => array(
                                'lgp.id' => $lobby_group_conversation_id,
                                'lgp.status' => 1
                            ),
                            'single' => true
                        );
                        $data['lobby_get_chat'] = $this->General_model->commonGet($options);
                        $data['fan_tag'] = '<b>'.$this->General_model->get_my_fan_tag($_SESSION['redirect_lobby_id'],$_SESSION['user_id'])->fan_tag.'</b>';
                    }
                }

                $data['receiver_name'] = $this->General_model->get_my_fan_tag($_SESSION['redirect_lobby_id'],$_SESSION['user_id'])->fan_tag;
                $data['lobby_detail'] = $this->General_model->view_data('lobby', array(
                    'id' => $_SESSION['redirect_lobby_id']
                ));

                //get Defualt Sub sound.
                $data['getmysub_sound'] = $this->General_model->view_single_row_single_column('stream_settings', array('user_id' => $_SESSION['user_id'],'key_option' => 'sub_sound'), 'selsound_audio')['selsound_audio'];

                $data['redirect_url'] = base_url().'livelobby/'.$data['lobby_detail'][0]['custom_name'];
            } else {
                $data['receiver_detail'] = $this->General_model->view_data('user_detail', array(
                    'id' => $user_to
                ));
                $data['receiver_name'] = $data['receiver_detail'][0]['name'];
                $data['redirect_url'] = base_url().'Subscribeuser/subscriptionlist/';
            }
            $content=$this->load->view('subscribe/subscription_success.tpl.php',$data,true);
            $this->render($content);
        }
    }

    function Getdata() {
        // write_file(./paypaldata.php, print_r($_REQUEST,true), 'a');
        $gettransaction_plan_detail = $this->General_model->view_data('paypal_subscriptions', array(
            'transaction_id' => $_REQUEST['recurring_payment_id']
        ));
        $getMembershipHistory = $this->General_model->view_data('player_membership_subscriptions', array(
            'transaction_id' => $_REQUEST['recurring_payment_id']
        ));
        if(count($getMembershipHistory) > 0){
            $player_subscriptions_data = array(
                'token_id' => '',
                'transaction_id' => $_REQUEST['recurring_payment_id'],
                'user_id' => $getMembershipHistory[0]['user_id'],
                'plan_id' => $getMembershipHistory[0]['plan_id'],            
                'validity' => '',
                'valid_from' => date('Y-m-d H:i:s', time()),
                'valid_to' => '',
                'amount' => $_REQUEST['mc_gross'],
                'currency_code' => $_REQUEST['mc_currency'],
                'payer_email' => $_REQUEST['payer_email'],
                'create_at' => date('Y-m-d H:i:s', time()),
                'update_at' => date('Y-m-d H:i:s', time()),
                'payment_status' => $_REQUEST['payment_status'],
                'payment_date' => date('Y-m-d H:i:s', time()) 
            );        
            $r = $this->General_model->insert_data('player_membership_subscriptions', $player_subscriptions_data);
        }
        if(count($gettransaction_plan_detail) > 0){
            $paypal_subscriptionshistory_data = array(
                'transaction_id' => $_REQUEST['recurring_payment_id'],
                'transaction_subject' => $_REQUEST['transaction_subject'],
                'user_from' => $gettransaction_plan_detail[0]['user_from'],
                'user_to' => $gettransaction_plan_detail[0]['user_to'],
                'subscribption_plan_id' => $gettransaction_plan_detail[0]['subscribption_plan_id'],
                'amount' => $_REQUEST['mc_gross'],
                'fee' => $gettransaction_plan_detail['fee'],
                'currency_code' => $_REQUEST['mc_currency'],
                'payment_status' => $_REQUEST['payment_status'],
                'payment_date' => date('Y-m-d H:i:s', time()),
                'is_history' => 1,
                'subscribe_status' => $gettransaction_plan_detail[0]['subscribe_status'],
                'created_at' => date('Y-m-d H:i:s', time())
            );
            $r = $this->General_model->insert_data('paypal_subscriptions', $paypal_subscriptionshistory_data);
            if ($_REQUEST['payment_status'] == "Completed") {
                $user_to = $gettransaction_plan_detail[0]['user_to'];
                $plan = $gettransaction_plan_detail[0]['subscribption_plan_id'];

                $subscribe_plan_detail = $this->General_model->view_single_row('subscription_plans', array('id'=>$plan),'*');

                $getreciever_data = $this->General_model->view_data('user_detail', array(
                    'user_id' => $user_to
                ));
                $amount = $getreciever_data[0]['total_balance']+$subscribe_plan_detail['amount'];
                $update_data = array(
                    'total_balance' => number_format((float) $amount, 2, '.', '')
                );
                $r = $this->General_model->update_data('user_detail', $update_data, array(
                    'user_id' => $user_to
                ));
            }
        }
    }
    function manually_unsubscribe_plan(){
        if (!empty($_POST)) {
            $data = $_POST;
            $result = $this->unsubscribe_subscribe_plan($data);
            redirect($result);
        }
    }
    function unsubscribe_subscribe_plan($data){
        $payment_id = $data['user_subscription_id'];            
        $PayPalResponseResult = $this->Paypal_payment_model->get_recurring_payments_profile_details($payment_id);

        $subscription_opt = $data['user_subscription_opt'];
        if ($subscription_opt == 'Unsubscribe') {
            $subscription_opt = 'Cancel';
        }
      
        if($PayPalResponseResult['status'] == 1){
            $PayPalResult = $PayPalResponseResult['data'];
            
            if ($PayPalResult['STATUS'] != 'Cancelled') {
                $this->Paypal_payment_model->Manage_recurring_payments_profile_status($payment_id,$subscription_opt);
            }
            $this->General_model->update_data('paypal_subscriptions', array('subscribe_status' => '0','updated_at' => date('Y-m-d H:i:s', time())), array('transaction_id' => $payment_id));
            if(isset($data['redirect'])){
                return $data['redirect'];
            } else {
                redirect(base_url().'Subscribeuser/subscriptionlist');
            }
        } else {
            $this->error($PayPalResponseResult['data']);
        }
    }

    function subscriptionlist() {
        $this->check_user_page_access();
        $data['lobby_fee'] = $this->General_model->view_all_data('admin_lobby_fee', 'id', 'asc');
        $data['subscribedlist'] = $this->General_model->subscribedlist($_SESSION['user_id']);
        $res = array(
            'user_to' => $_SESSION['user_id'],
            'is_history' => 0,
            'subscribe_status' => 1,
        );
        $data['subscribedbylist'] = $this->General_model->getsubscribedlist($res);
        $content = $this->load->view('subscribe/subscriptionlist.tpl.php',$data,true);
        $this->render($content);
    }
    function get_subscription_plans(){
        $this->check_user_page_access();
        if (!empty($_POST)) {
            $user_id = $_SESSION['user_id'];
            $user_to = $_POST['user_to'];
            $subscribe_status = $_POST['subscribe_status'];
            $arr = array(
                'user_from' => $user_id,
                'user_to' => $user_to,
                'subscribe_status' => $subscribe_status,
                'is_history' => 0,
                'single' => true,
            );
            $data['subscription_plan'] = $this->General_model->getsubscribedlist($arr);
            $data['all_plan'] = $this->General_model->view_all_data('subscription_plans', 'id', 'asc');
            echo json_encode($data);
            exit();
        }
    }
}