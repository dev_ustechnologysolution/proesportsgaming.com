<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Tournaments extends My_Controller {
  public function __construct() {
    parent::__construct();
    $this->check_cover_page_access();
    $this->load->model('Tournaments_model');
  }
  //page loading
  public function index() {
    $data['custsysList']= $this->General_model->matches_custsyslist_list();
    $data['systemList'] = $this->General_model->matches_system_list();
    $data['lobby_list'] = $this->Tournaments_model->get_tournaments_event();
    
    $archive_lobby_list = $this->Tournaments_model->archive_get_tournaments_event();
    $archive_lobby_list = array_map(function ($value) {
      $value->start = $this->User_model->custom_date(array('date' => $value->start,'format' => 'Y-m-d\TH:i:sP'));
      $value->end =  $this->User_model->custom_date(array('date' => $value->end,'format' => 'Y-m-d\TH:i:sP'));
      return $value;
    }, $archive_lobby_list);

    $getcustomevents = $this->Tournaments_model->get_custom_events();
    $getcustomevents = array_map(function ($value) {
      $value->start = $this->User_model->custom_date(array('date' => $value->start,'format' => 'Y-m-d\TH:i:sP'));
      $value->end =  $this->User_model->custom_date(array('date' => $value->end,'format' => 'Y-m-d\TH:i:sP'));
      return $value;
    }, $getcustomevents);

    $data['archive_lobby_list'] = array_map(function ($value) {
      if ($value->is_archive == 0) {
        return $value;
      }
    }, $archive_lobby_list);
    $data['archive_lobby_list'] = array_values(array_filter($data['archive_lobby_list']));
    $data['getcustomevents'] = $getcustomevents;

    $arr = array_merge($data['archive_lobby_list'], $data['getcustomevents']);
    // $arr = array_map(function ($value) {
    //     $value->start = $this->User_model->custom_date(array('date' => $value->start,'format' => 'Y-m-d\TH:i:sP'));
    //     $value->end =  $this->User_model->custom_date(array('date' => $value->end,'format' => 'Y-m-d\TH:i:sP'));
    //   return $value;
    // }, $arr);
    $arr1 = [];
    foreach ($arr as $key => $ev) {
      if ($ev->start != '') {
        // $bg_color = ($ev->bg_color != '') ? $ev->bg_color : '#ff5000';
      ($ev->is_archive==0) ? $bg_color = $backgcolor = ($ev->bg_color != '') ? $ev->bg_color : '#ff5000' :$bg_color = $backgcolor =  $ev->bg_color = 'lightgray';
        if (strpos($ev->bg_color, '_') !== false) {
          $arrcolor = explode('_', $ev->bg_color);
          $bg_color = 'linear-gradient(200deg, '.$arrcolor[0].', '.$arrcolor[1].') 0% 0% / 200% 200%';
        }

        ($ev->event_key != 0 && $ev->key_image !='') ? $img = base_url().'upload/event_key_img/'.$ev->key_image : (($ev->proplayer == 1) ? $img = base_url().'assets/frontend/images/pro_player.png' : $img = '');
        $event['is_proplayre'] = ($img != '') ? '<span class="made_by_proplayer"><img width="40px" height="40px" src="'.$img.'" alt="Pro Player"></span>' : '';
        $temparray = explode("T",$ev->start);
        $temparray_two = explode("+",$temparray[1]); 
        
        $onlyDate = false;
        $startDate = date('Y-m-d H:i:s', strtotime($ev->start));
        $endDate = date('Y-m-d H:i:s', strtotime($ev->end));

        if ($temparray_two == '' || $temparray_two[0] == '' || $temparray_two[0] == '00:00:00') {
          $onlyDate = true;
          $startDate = (!empty($daylight_hour) && $daylight_hour != 0) ? date('Y-m-d', strtotime($daylight_hour.' hour', strtotime($ev->start))) : date('Y-m-d', strtotime($ev->start));
          $endDate = (!empty($daylight_hour) && $daylight_hour != 0) ? date('Y-m-d', strtotime($ev->end.' '.$daylight_hour.' hour')) : date('Y-m-d', strtotime($ev->end));          
          
          // $startDate = date('Y-m-d', strtotime($ev->start));
          // $endDate = date('Y-m-d', strtotime($ev->end));
        }

        $event['id'] = $ev->id;
        $event['title'] = $ev->title;
        $event['start'] = $startDate;
        $event['end'] = $endDate;
        $event['color'] = $bg_color;
        $event['is_archive'] = $ev->is_archive;
        $event['bg_color'] = ($ev->event_type == 'lobby_event') ? '#ff5000' : $ev->bg_color;
        $event['description'] = $ev->description;
        $event['event_bg_img'] = ($ev->event_bg_img != '') ? $ev->event_bg_img : '';
        $event['url'] = ($ev->url !='') ? $ev->url : (($ev->event_type == 'lobby_event') ? base_url().'livelobby/watchLobby/'.$ev->id : '');
        $event['event_key'] = $ev->event_key;
        $event['event_type'] = $ev->event_type;
        $event['event_is_expired'] = (strtotime('now') > strtotime($endDate)) ? 'yes' : 'no';
        $arr1[] = $event;
      }
    }

    $data['global_dst_hour'] = $this->General_model->view_data('site_settings', array('option_title' => 'global_dst_hour'))[0];
    // Get the Daylight option is on for user
    $data['global_dst_option'] = $this->General_model->view_data('site_settings', array('option_title' => 'global_dst_option'))[0];
        
    $data['events'] = json_encode($arr1); 
    $data['keylist'] = $this->Tournaments_model->get_key_list(array());
    
    $content = $this->load->view('tournaments/tournaments.tpl.php',$data,true);
    $this->render($content);
  }
}