<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include BASEPATH.'../application/controllers/My_Controller.php';

class Trade extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->check_user_page_access();
		$this->load->library(array('form_validation'));
		$this->load->model('General_model');
		$this->target_dir=BASEPATH.'../upload/profile_img/';
	}

	public function tradeHistory() {
		$data['banner_data'] = $this->General_model->tab_banner();
		$data['my_data'] = $this->User_model->user_detail()[0];
		$data['get_sent_history'] = $this->General_model->get_mytradelist($_SESSION['user_id'],'sent','created');
		$data['get_receive_history'] = $this->General_model->get_mytradelist($_SESSION['user_id'],'receive','created');
		$data['trade_data'] = $this->General_model->array_merge_custom($data['get_sent_history'],$data['get_receive_history'],'id');
		$content=$this->load->view('trade/tradelist.tpl.php',$data,true);
		$this->render($content);
	}
	public function tipHistory(){
		$data['banner_data'] = $this->General_model->tab_banner();
		$data['my_data'] = $this->User_model->user_detail()[0];
		$data['tip_send'] = $this->General_model->get_tip_history($_SESSION['user_id'],'th.user_from','th.user_to');
		$data['tip_receive'] = $this->General_model->get_tip_history($_SESSION['user_id'],'th.user_to','th.user_from');
		$content=$this->load->view('trade/tiphistorylist.tpl.php',$data,true);
		$this->render($content);

	}

	public function download_tip_history(){
	    // file name 
	    $filename = 'tip_history_'.time().'_'.date('d-m-Y').'.csv'; 
	    header("Content-Description: File Transfer"); 
	    header("Content-Disposition: attachment; filename=$filename"); 
	    header( "Content-Type: text/csv;charset=utf-8" );

	    // get data 
	    $data['tip_send'] = $this->General_model->get_tip_history($_SESSION['user_id'],'th.user_from','th.user_to');
	    $data['tip_receive'] = $this->General_model->get_tip_history($_SESSION['user_id'],'th.user_to','th.user_from');
	    $tipData = array_merge($data['tip_send'], $data['tip_receive']);
	    function date_compare($a, $b)
		{
		    $t1 = strtotime($a->created);
		    $t2 = strtotime($b->created);
		    return $t2 - $t1;
		}    
		usort($tipData, 'date_compare');
	    // file creation 
	    $file = fopen('php://output', 'w');
 	 
	    $header = array("Sender", "Sender Acc. No.", "Receiver", "Receiver Acc. No.", "Amount", "Status", "Date"); 
	    fputcsv($file, $header);
	    foreach ($tipData as $line){
 	   		if($line->user_from == $_SESSION['user_id']){
 	   			$obj['sender'] = $_SESSION['name'];
 	   			$obj['sender_acc'] = $_SESSION['account_no'];
 	   			$obj['receiver'] = $line->name;
 	   			$obj['receiver_acc'] = $line->account_no;
 	   			$obj['amount'] = $line->tip_amt;
 	   			$obj['status'] = 'Sent';
 	   			$obj['date'] = date('d-m-Y', strtotime($line->created));
 	   		}else{
 	   			$obj['sender'] = $line->name;
 	   			$obj['sender_acc'] = $line->account_no;
 	   			$obj['receiver'] = $_SESSION['name'];
 	   			$obj['receiver_acc'] = $_SESSION['account_no'];
 	   			$obj['amount'] = $line->tip_amt;
 	   			$obj['status'] = 'Received';
 	   			$obj['date'] = date('d-m-Y', strtotime($line->created));
 	   		}
			fputcsv($file,$obj); 
	    }
	    fclose($file); 
	    exit; 
	}

}

