<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
// require_once(FCPATH.'/vendor/autoload.php');

class User extends My_Controller  {
	function __construct() {
		parent::__construct();
		$this->load->model('User_model');
		$this->target_dir = BASEPATH.'../upload/profile_img/';
	}
	//profile view
	function myprofile() {
		$this->check_user_page_access();
		$user_id = $_SESSION['user_id'];
		$data['friend_profile']=$this->General_model->friend_profile($user_id);
		$data['banner_data'] = $this->General_model->tab_banner();
		$content=$this->load->view('user/myprofile.tpl.php',$data,true);
		$this->render($content);
	}
	function profile() {
		$this->check_user_page_access();
		$data['country_list'] = $this->User_model->country_list();
		$data['user_detail'] = $this->User_model->user_detail();
		$data['common_settings'] = json_decode($this->session->userdata('common_settings'));
		$data['mail_categories_fun'] = $this->User_model->mail_categories_fun();

		$opt_where['option_title'] = 'global_dst_option';
        $data['global_dst_option'] = $this->General_model->view_data('site_settings', $opt_where)[0];

		$data['state'] = $this->General_model->view_data('state',array('country_id' => $data['user_detail'][0]['country']));
		$content = $this->load->view('user/profile.tpl.php',$data,true);
		$this->render($content);
	}

	//profile edit
	function profile_edit() {

		$this->check_user_page_access();
		$img = $this->input->post('old_image', TRUE) != '' ? $this->input->post('old_image', TRUE) : $this->General_model->get_user_img();

		if (isset($_FILES["image"]["name"]) && $_FILES["image"]["name"]!='') {
			$img=time().basename($_FILES["image"]["name"]);
			$ext = end((explode(".", $img)));
			if ($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg') {
				$target_file = $this->target_dir.$img;
				if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
					if (file_exists($this->target_dir.$this->input->post('old_image', TRUE))) {
						unlink($this->target_dir.$this->input->post('old_image', TRUE));
					}
				}
			} else {
				$this->session->set_flashdata('msg', 'This is not a image file');	
				redirect(base_url().'user/profile');
			}
		}
		//update email and password into user table
		$email = array('email'=>$this->input->post('email', TRUE));
		$this->General_model->update_data('user',$email,array('id'=>$_SESSION['user_id']));
		if($this->input->post('password', TRUE) != '') {
			$password=array('password'=>md5($this->input->post('password', TRUE)));
			$this->General_model->update_data('user',$password,array('id'=>$_SESSION['user_id']));
		}
		$friends_mail = (isset($_POST['friends_mail'])) ? 'yes' : 'no';
		$team_mail = (isset($_POST['team_mail'])) ? 'yes' : 'no';
		$challenge_mail = (isset($_POST['challenge_mail'])) ? 'yes' : 'no';
		$select_mail_categories = $this->General_model->view_data('mail_categories',array('user_id'=>$_SESSION['user_id']));
		
		//update user info into user detail tables..
		$mail_cate_data=array(
			'user_id' => $_SESSION['user_id'],
			'friends_mail' => $friends_mail,
			'team_mail' => $team_mail,
			'challenge_mail' => $challenge_mail
		);
		if ($this->input->post('display_name_status') == 'on') {
			$display_name=$this->input->post('display_name', TRUE);
			$display_name_status=1;
			$_SESSION['display_name'] = $display_name;
		} else {
			$display_name='';
			$display_name_status=0;
			if ($_SESSION['display_name']) {
				unset($_SESSION['display_name']);
			}
		}

		$data = array(
			'name' => $this->input->post('name', TRUE),
			'display_name' => $display_name,
			'display_name_status' => $display_name_status,
			'number' => $this->input->post('number', TRUE),
			'team_name' => $this->input->post('team_name', TRUE),
			'location' => $this->input->post('location', TRUE),			
			'country' => $this->input->post('country', TRUE),
			'state' => $this->input->post('state', TRUE),
			'city' => $this->input->post('city', TRUE),
			'zip_code' => $this->input->post('zip_code', TRUE),
			'taxid' => $this->input->post('taxid', TRUE),
			'discord_id' => $this->input->post('discord_id', TRUE),
			'timezone' => $this->input->post('timezone', TRUE),
			'image' => $img
		);

		$opt_where['option_title'] = 'global_dst_option';
		$global_dst_option = $this->General_model->view_data('site_settings', $opt_where)[0];
		$common_settings = json_decode($this->session->userdata('common_settings'));
		
		if (!empty($global_dst_option) && $global_dst_option['option_value'] == 'on') {
			$common_settings->daylight_option = (!empty($_POST['daylight_saving_settings']) && $_POST['daylight_saving_settings'] == 'on') ? 'on' : 'off';
			$data['common_settings'] = json_encode($common_settings);
		}

		$this->session->set_userdata($data);
		$this->session->set_userdata('user_image', $img);
		$r = $this->General_model->update_data('user_detail',$data,array('user_id'=>$_SESSION['user_id']));
		$r .= (count($select_mail_categories)==0) ? $this->General_model->insert_data('mail_categories',$mail_cate_data) : $this->General_model->update_data('mail_categories',$mail_cate_data,array('user_id' => $_SESSION['user_id']));
		($r) ? $this->session->set_flashdata('message_succ', 'Update Successful') : $this->session->set_flashdata('message_err', 'Unable to update');
		redirect(base_url().'user/profile');
	}
	//view change password
 	function changePassword() {
		$this->check_user_page_access();
		$data['data'] = '';
		$content = $this->load->view('user/change_password.tpl.php',$data,true);
		$this->render($content);
	}
	// // Day Light Saving
 // 	function day_light_saving() {
	// 	$this->check_user_page_access();
	// 	if (!empty($_POST['submit'])) {
	// 		$daylight_opt = ($_POST['daylight_option'] == 'plus_one') ? '+1' : (($_POST['daylight_option'] == 'minus_one') ? '-1' : '0');
	// 		($_POST['submit'] == 'Skip') ? $daylight_opt = '0' : '';
	// 		$common_settings = json_decode($this->session->userdata('common_settings'));
	// 		$common_settings->daylight_hour = $daylight_opt;
	// 		$data['common_settings'] = json_encode($common_settings);
	// 		$r = $this->General_model->update_data('user_detail', $data ,array('user_id' => $this->session->userdata('user_id')));
	// 		($r) ? $this->session->set_flashdata('message_succ', 'Day Light Updated') : $this->session->set_flashdata('message_err', 'Something wrong');
	// 		redirect($_SERVER['HTTP_REFERER']);
	// 	}
	// }

	//change password
	function change_password() {
		$this->check_user_page_access();
		//check new_password and confirm_password
		if($this->input->post('new_password', TRUE)==$this->input->post('confirm_password', TRUE))
		{
			//check current password
			$r=$this->User_model->check_password('user',array('id'=>$_SESSION['user_id'],'password'=>md5($this->input->post('current_password', TRUE))));
			if($r==1)
			{
				$data=array('password'=>md5($this->input->post('new_password', TRUE)),);
				$r=$this->General_model->update_data('user',$data,array('id'=>$_SESSION['user_id']));

				if($r)
				{
					echo '<div class="box box-success">
							<div class="box-header with-border">
								<h3 class="box-title">Password changed successfully</h3>
								<div class="box-tools pull-right">
									<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
								</div>
							</div>
						</div>';
			  	}
			} else {
				echo '<div class="box box-danger">
                		<div class="box-header with-border">
                  			<h3 class="box-title">Current password is not correct</h3>
							<div class="box-tools pull-right">
								<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
							</div>
						</div>
					  </div>';
			}
		} else {
			echo '<div class="box box-danger">
					<div class="box-header with-border">
					<h3 class="box-title">New password and confirm password not match</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
					</div>
					</div>
				</div>';
		}
	}
	//forgot password
	function forgot_password()
    {
		$view_data=array();
		if($this->input->post('email', TRUE)!='')
		{
			$user=$this->General_model->view_data('user',array('email'=>$this->input->post('email', TRUE)));
			if(isset($user)&& count($user)>0)
			{
				$user_id=$user[0]['id'];
				$user_name=$this->General_model->view_data('user_detail',array('user_id'=>$user_id));
				$user_name=$user_name[0]['name'];

				$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
				$password = substr(str_shuffle($chars), 0, 6);    		    		
				$email = $this->input->post('email', TRUE);
				$token = array('token'=>$password);

				$this->General_model->update_data('user',$token , array('email'=>$email));			
				$data = array('password'=>md5($password));
				if($this->General_model->update_data('user',$data,array('email'=>$this->input->post('email', TRUE))))
					$to =$email;
					$header = base_url().'/assets/frontend/images/email/header.png';
				$subject = "Recovery Password";
				$message ='<html><body><table cellpadding="0" cellspacing="0" broder="0" width="600">';
				$message .='<tr><td><img src="'.$header.'" style="width:100%;"></td></tr>';
				$message .='<tr><td><p> Hello '.$user_name.'</p><p>Please click on the below link to reset your password.</p><a href='.base_url().'user/reset_password/'.$password.'>Click here</a><p>Thanks,</p></td></tr>';
				$message .='<tr><td><img src="'.base_url().'/assets/frontend/images/email/footer.png" style="width:100%;"></td></tr>';
				$message .='</table></body></html>';

				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers .= 'From: <noreply@ProEsportsGaming.com>' . "\r\n";
				if(mail($to,$subject,$message,$headers)){
					$view_data['err']='<span style="color:green">please check your mail</span>';
				} else {
					$re = $this->General_model->sendMail($to,$user_name,$subject,$message);
					if($re == 1){
						$view_data['err']='<span style="color:green">please check your mail</span>';
					} else {

						$view_data['err']='<span style="color:red">mail not send </span>';
					}
				}
			} else {
				$view_data['err']='<span style="color:red">Give proper email</span>';
			}
		}
		$content=$this->load->view('login/forgot_pass',$view_data,true);
		$this->render($content);
	}
	
	function sendMail1($to,$subject,$message)
	{
		$this->load->library('email');
		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'smtp.sendgrid.net',
			'smtp_port' => 587,
			'smtp_user' => 'archita@aspireedge.com', // change it to yours
			'smtp_pass' => 'archita123', // change it to yours
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'wordwrap' => TRUE,
		);
		$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		$this->email->from('noreply@ProEsportsGaming.com'); // change it to yours
		$this->email->to($to);// change it to yours
		$this->email->subject($subject);
		$this->email->message($message);
		if($this->email->send()) {
			$result = 1;
		} else {
			$result = 0;
		//  show_error($this->email->print_debugger());
		}
		return $result;
	}

	//reset password
	function reset_password($token='')
    {
		$view_data=array();
		$user = $this->General_model->view_data('user',array('token'=>$token));
		
		if(isset($user)&& count($user)>0)
		{
		
			if($this->input->post('new_password', TRUE)!='')
			{
				if($this->input->post('new_password', TRUE)==$this->input->post('confirm_password', TRUE))
				{
					$email= $user[0]['email'];
					$data=array('token'=>'','password'=>md5($this->input->post('new_password', TRUE)));
					if($this->General_model->update_data('user',$data,array('email'=>$email)))
					header('location:'.base_url('login'));
				} else {
					$view_data['err']='<span style="color:red">Passwords not matching </span>';
					$this->load->view('login/reset_pass',$view_data);
				}
			}
		} else {
			header('location:'.base_url());
		}
	    $this->load->view('login/reset_pass',$view_data);
	}

	function user_profile() {
		$this->check_user_page_access();
		if(isset($_GET['lobby_id'])){
			$id = $_GET['lobby_id'];
      $fans_id = $_GET['fans_id'];
      $data['view_lby_det'] = $this->General_model->view_data('lobby', array(
          'id' => $id,
          'status' => 1
      ));
      if(count($data['view_lby_det'])>0){
          $view_data = $this->General_model->view_data('lobby_fans', array(
              'lobby_id' => $id,
              'user_id' => $this->session->userdata('user_id')
          ));
          if (count($view_data) > 0) {
              $checkcountry = $this->General_model->view_data('user_detail', array('user_id' => $fans_id));
              if (is_numeric($checkcountry[0]['country'])) {
                  $options             = array(
                      'select' => 'lf.id,lf.fan_tag,lf.lobby_id,lf.status,ud.*,u.*,ud.image,l.status as lobby_status, c.sortname as country, s.statename as state',
                      'table' => 'lobby_fans lf',
                      'join' => array(
                      'user_detail ud' => 'ud.user_id = lf.user_id',
                      'user u' => 'u.id = lf.user_id',
                      'lobby l' => 'l.id = lf.lobby_id',
                      'country c' => 'c.country_id = ud.country',
                      'state s' => 's.country_id = c.country_id',
                  ),
                      'where' => array(
                          'lf.lobby_id' => $id,
                          'lf.user_id' => $fans_id, 
                          'l.status' => 1
                      )
                  );
              } else {
                  $options             = array(
                  'select' => 'lf.id,lf.fan_tag,lf.lobby_id,lf.status,ud.*,u.*,ud.image,l.status as lobby_status',
                  'table' => 'lobby_fans lf',
                  'join' => array(
                  'user_detail ud' => 'ud.user_id = lf.user_id',
                  'user u' => 'u.id = lf.user_id',
                  'lobby l' => 'l.id = lf.lobby_id',
                  ),
                      'where' => array(
                          'lf.lobby_id' => $id,
                          'lf.user_id' => $fans_id, 
                          'l.status' => 1
                      )
                  );
              }
              $data['view_lobby_fan_profile'] = $this->General_model->commonGet($options);
              $data['view_lobby_fan_profile'] = $data['view_lobby_fan_profile'][0];
              $data['ordinal_lobby_id'] = $this->General_model->ordinal($id);
              
              $follower_data = $this->User_model->getfollower_and_following($fans_id);
              $data['followercount'] = $follower_data->followercount;
              $data['is_followed'] = (in_array($this->session->userdata('user_id'), explode(',', $follower_data->followers_arr))) ? 'following' : 'follow';
              $content = $this->load->view('user/user_profile.tpl.php', $data, true);
              $this->render($content);
          }
      } else {
        redirect(base_url());
        exit();
      }
		}else{
			$this->check_user_page_access();
			if(isset($_GET['fans_id']) && $_GET['fans_id'] != null && $_GET['fans_id'] != ''){
				$user_id = $_GET['fans_id'];
			} else {
				$user_id = $_SESSION['fans_id'];
			}
      $data['view_lobby_fan_profile'] = $this->User_model->get_user_detail($user_id);
      $data['view_lobby_fan_profile']->id = $user_id;
      $follower_data = $this->User_model->getfollower_and_following($user_id);
      $data['followercount'] = $follower_data->followercount;
      $data['is_followed'] = (in_array($this->session->userdata('user_id'), explode(',', $follower_data->followers_arr))) ? 'following' : 'follow';
      $content = $this->load->view('user/user_profile.tpl.php', $data, true);
      $this->render($content);
		}
	}
	public function update_timezone() {
		if (!empty($_POST['timezone'])) {
			$timezoneName = $_POST['timezone'];
			date_default_timezone_set($timezoneName);
			if ($_POST['save_forever'] == 'on') {
				($this->session->userdata('user_id')) ? $this->General_model->update_data('user_detail', array('timezone' => $timezoneName), array('user_id' => $this->session->userdata('user_id'))) : '';
			}
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	function paypal_data_get_andreset() {
		$dir = BASEPATH."../paypal_data/";
		$paypal_data = [];
		// Open a directory, and read its contents
		if (is_dir($dir)){
		  if ($dh = opendir($dir)){
		    while (($file = readdir($dh)) !== false){
		      $handle = fopen($dir."/".$file,"r");
					if ($handle) {
					    while (($line = fgets($handle)) !== false) {
					    	if ($line != '' && strpos($line, 'Purchase Package Paypal Checkout') !== false) {
					    		$arr = explode("Purchase Package Paypal Checkout",$line);
					    		$jsondataresult = end($arr);
					    		$result = json_decode($jsondataresult);
					    		if (!empty($result)) {
					    			$paypalnewarr['user_id'] = $result->user_id;
					    			$paypalnewarr['order_id'] = $result->order_id;
					    			$paypal_data[] = $paypalnewarr;
					    		}
					    	}
					    }
					    fclose($handle);
					} else {
					    // error opening the file.
					} 

		    }
		    closedir($dh);
		  }
		}
		if (!empty($paypal_data)) {
			$count = 0;
			$uparr = [];
			foreach ($paypal_data as $key => $pd) {
				if (!empty($pd['order_id'])) {
					$payment_data = $this->General_model->view_single_row('payment', array('order_id' => $pd['order_id']),'*');
					if (!empty($payment_data) && $payment_data['user_id'] != $pd['user_id']) {
						$update_data['user_id'] = $pd['user_id'];
						$this->General_model->update_data('payment', $update_data, array('order_id' => $pd['order_id']));
						$count++;
						$uparr[] = $pd;
					}
				}
			}
			echo $count.' record has been updated <br>';
			echo "<pre>";
			print_r($uparr);
		}
		exit();
	}
	function all_transactions () {
		echo "<pre>";
		$response = $this->Paypal_payment_model->get_transactions();
		$custom = json_decode($response['data']['CUSTOM']);
		print_r($custom->user_id);
		print_r($response);
		exit();
		// $this->db->select('*');
		// $this->db->from('payment');
		// $payment_data = $this->db->get()->result_array();
		// echo "<pre>";
		// foreach ($payment_data as $key => $pd) {
		// 	if (!empty($pd['transaction_id'])) {
		// 		$arrdata = array('payment_id' => '6FU79872UY424363K');
		// 		$response = $this->Paypal_payment_model->get_transaction_details($arrdata);
		// 		$custom = json_decode($response['data']['CUSTOM']);
		// 		print_r($custom->user_id);
		// 		print_r($response);
		// 		// print_r(explode('&', urldecode($response['data']['RAWREQUEST'])));
		// 		// print_r(explode('&', urldecode($response['data']['RAWRESPONSE'])));
		// 		exit();
		// 	}
		// }
		// exit();
	}
	function custom_transactions () {
		echo "<pre>";
		$response = $this->Paypal_payment_model->get_transaction_details();
		$custom = json_decode($response['data']['CUSTOM']);
		print_r($custom->user_id);
		print_r($response);
		exit();

		// $this->db->select('*');
		// $this->db->from('payment');
		// $payment_data = $this->db->get()->result_array();
		// echo "<pre>";
		// foreach ($payment_data as $key => $pd) {
		// 	if (!empty($pd['transaction_id'])) {
		// 		$arrdata = array('payment_id' => '6FU79872UY424363K');
		// 		$response = $this->Paypal_payment_model->get_transaction_details($arrdata);
		// 		$custom = json_decode($response['data']['CUSTOM']);
		// 		print_r($custom->user_id);
		// 		print_r($response);
		// 		// print_r(explode('&', urldecode($response['data']['RAWREQUEST'])));
		// 		// print_r(explode('&', urldecode($response['data']['RAWRESPONSE'])));
		// 		exit();
		// 	}
		// }
		// exit();
	}

	public function show_available_balance(){
		if (!empty($_POST['show_available_balance'])) {
			$common_settings = json_decode($this->session->userdata('common_settings'), true);
            $common_settings['show_available_balance'] = $_POST['show_available_balance'];
            $this->General_model->update_data('user_detail',array('common_settings' => json_encode($common_settings)),array('user_id'=>$this->session->userdata('user_id')));
            $res['show_available_balance'] = $_POST['show_available_balance'];
        	echo json_encode($res);
        	exit();
		}
	}
}

