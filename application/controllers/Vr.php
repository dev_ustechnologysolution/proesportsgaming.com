<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include BASEPATH.'../application/controllers/My_Controller.php';

class Vr extends My_Controller {

	public function __construct() {

		parent::__construct();
		// $this->check_user_page_access();
        $this->load->model('My_model');
        $this->load->library('pagination');
        $this->pageLimit=20;
    }

	//page loading

	public function index($type='',$page=0) {
		
		$data['dt']=1;
		// get all game under category pctab
		
		$start = ($page==0)?0:$page;
		$offset = 5;
        /*
		$condition = "ag.play_status='0' AND ag.game_category_id='3' LIMIT ".$start.",".$offset;
		$data['gameList']=$this->General_model->admin_windows_game_list($condition);
        */
        /***************Game list on PcTab Category***********************/
        $page_one=$page;
		  
		if($type!='' && $type=='game')
        {
           $page=0;
        }
		else if($type!='' && $type=='challange')
        {
           $page_one=0;
        }
        $conditions='';
        $data['gameListDpPoser']='';
        if(isset($_POST['game_id']) && $_POST['game_id']!='')
        {
            $conditions=" AND ag.id='".$_POST['game_id']."'";
            $data['gameListDpPoser']=$_POST['game_id'];
        }

        $data['gameListDp']= $this->My_model->getPcTabGame(-1,$page_one,$category_id=4);
        $data['gameList']= $this->My_model->getPcTabGame($this->pageLimit,$page_one,$category_id=4,$conditions);
        if(!isset($data['gameList']['tot']))
        {
            if($page_one>=$data['gameList']['tot'] && $page_one!=0)
            {
                $page_one=$data['gameList']['tot'] - $this->pageLimit;
                if($page_one<0)
                    $page_one=0;
                header('location:'.base_url().'Vr/index/game/'.$page_one);
                exit;
            }
        }
        paggingInitialization($this,
            array('base_url'=>base_url().'Vr/index/game',
                'total_row'=>$data['gameList']['tot'],
                'per_page'=>$this->pageLimit,
                'uri_segment'=>4,
                'next_link'=>'<i class="fa fa-chevron-right"></i>',
                'prev_link'=>'<i class="fa fa-chevron-left"></i>'
            )
        );

        $data['paging_one']= $this->pagination->create_links();
        /***************Game list on PcTab Category***********************/

        /***************Game challange list on pctab**************************/
        
        $conditions='';
        $data['challangeDpPoser']='';
        if(isset($_POST['challange_id']) && $_POST['challange_id']!='')
        {
            $conditions=" AND g.id='".$_POST['challange_id']."'";
            $data['challangeDpPoser']=$_POST['challange_id'];
        }

        $data['challangetDp']= $this->My_model->getPcTabGameChallange(-1,$page,$category_id=4);
        $data['pc_game']=$this->My_model->getPcTabGameChallange($this->pageLimit,$page,$category_id=4,$conditions);

        if(!isset($data['pc_game']['tot']))
        {
            if($page>=$data['pc_game']['tot'] && $page!=0)
            {
                $page=$data['pc_game']['tot']-$this->pageLimit;
                if($page<0)
                    $page=0;
                header('location:'.base_url().'Vr/index/challange/'.$page);
                exit;
            }
        }
        paggingInitialization($this,
            array('base_url'=>base_url().'Vr/index/challange/',
                'total_row'=>$data['pc_game']['tot'],
                'per_page'=>$this->pageLimit,
                'uri_segment'=>4,
                'next_link'=>'<i class="fa fa-chevron-right"></i>',
                'prev_link'=>'<i class="fa fa-chevron-left"></i>'
            )
        );
        $data['paging_two']= $this->pagination->create_links();
        /***************Game challange list on pctab**************************/
		// GET TOTAL NO OF ROWS
		$tot_game = $this->General_model->get_num_rows('game',array('game_category_id'=>4));
		
		// CALCULATE NO OF PAGES
		$tot_page = ceil($tot_game/$offset);
		
		$data['totGame'] = $tot_game;
		$data['totPage'] = $tot_page;
		
		// GET ALL CATEGORY LIST
		$data['catList'] = $this->General_model->view_all_data('admin_game_category','id','ASC');

        /**/

		$content=$this->load->view('vr/vr.tpl.php',$data,true);
		$this->render($content);

	}
	
	// pagination function
	public function page($page_id) {
		$data['dt']=1;
		// get all game under category pctab
		
		$offset = 5;
		$start = ($page_id*$offset)-$offset;
		
		$condition = "1 AND game_category_id='4' LIMIT ".$start.",".$offset;
		$data['gameList']=$this->General_model->admin_windows_game_list($condition);
		
		// GET TOTAL NO OF ROWS
		$tot_game = $this->General_model->get_num_rows('game',array('game_category_id'=>4));
		
		// CALCULATE NO OF PAGES
		$tot_page = ceil($tot_game/$offset);
		
		$data['totGame'] = $tot_game;
		$data['totPage'] = $tot_page;
		
		// GET ALL CATEGORY LIST
		$data['catList'] = $this->General_model->view_all_data('admin_game_category','id','ASC');
		
		$content=$this->load->view('vr/vr.tpl.php',$data,true);

		$this->render($content);
	}

}

