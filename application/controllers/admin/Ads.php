<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Ads extends My_Controller {
	function __construct(){
        parent::__construct();
        $this->load->model('User_model');
        $this->ads_master_upload = BASEPATH.'../upload/ads_master_upload/';
        $this->check_user_page_access();
    }
    public function ads_list() {
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Ads List', 'admin/ads/ads_list');
        $data['active'] = 'ads_list';
        $data['page_name'] = 'Ads';
        $options = array(
            'select' => 'at.*',
            'table' => 'ads_tbl at',
            'where_not_in' => array('at.ads_type' => 'product_ads'),
        );
        $data['ads_list'] = $this->General_model->commonGet($options);
        $content=$this->load->view('admin/ads_management/ads_list.tpl.php',$data,true);
        $this->render($content);
    }
	public function create_ads(){
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        if ($_GET['select_ads_type'] == 'product_ads') {
            $this->breadcrumbs->push('Product Ads List', 'admin/products/product_ads');
            $this->breadcrumbs->push('Create Product Ads', 'admin/products/create_product_ads');
            $data['active'] = 'product_ads';
            $data['page_name'] = 'Create Product Ads';
            $arr['parent_id'] = '0';
            $data['main_catlist'] = $this->Product_model->category_list($arr);
        } else {
            $this->breadcrumbs->push('Ads List', 'admin/ads/ads_list');
            $this->breadcrumbs->push('Create Ads', 'admin/ads/create_ads');
            $data['active'] = 'ads_list';
            $data['page_name'] = 'Create Ads';
       }
       $content = $this->load->view('admin/ads_management/create_ads.tpl.php',$data,true);
       $this->render($content);
    }
    public function insert_ads(){
        if (!empty($_POST)) {
            $custom_vids = $upload_sponsered_by_logo = '';
            $data_insert = array(
                'ads_type' => $_POST['select_ads_type'],
                'sponsered_by_logo_title' => $_POST['sponsered_by_logo_title'],
                'sponsered_by_logo_link' => $_POST['sponsered_by_logo_link'],
                'sponsered_by_logo_description' => $_POST['sponsered_by_logo_description'],
                'created_date' => date('Y-m-d H:i:s')
            );
            $upload_sponsered_by_logo = '';
            if(isset($_FILES["upload_sponsered_by_logo"]["name"]) && $_FILES["upload_sponsered_by_logo"]["name"]!='') {
                $upload_sponsered_by_logo = time().basename($_FILES["upload_sponsered_by_logo"]["name"]);
                $ext = end((explode(".", $upload_sponsered_by_logo)));
                if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg'){
                    $target_file_log = $this->ads_master_upload.$upload_sponsered_by_logo;
                    $r = move_uploaded_file($_FILES["upload_sponsered_by_logo"]["tmp_name"], $target_file_log);
                    if (!$r) {
                        $this->session->set_userdata('message_err', 'Failure to Add');
                        redirect(base_url().'admin/ads/create_ads');
                    }
                } else {
                    $this->session->set_userdata('message_err', 'This is not a image file');
                    redirect(base_url().'admin/ads/create_ads');
                }
            }
            $custom_vids = '';
            if ($_POST['select_vids_type'] == 'youtube_link') {
                $data_insert['youtube_link'] = $_POST['youtube_link'];
            } else {
                if ($_FILES['custom_vids']['size'] != 0) {
                    $custom_vids = time().basename($_FILES["custom_vids"]["name"]);
                    $ext = strtolower(end((explode(".", $custom_vids))));
                    if ($ext == 'mp4' || $ext == '3gp' || $ext == 'ogg') {
                        $target_file = $this->ads_master_upload.$custom_vids;
                        $r = move_uploaded_file($_FILES["custom_vids"]["tmp_name"], $target_file);
                        if (!$r) {
                            $this->session->set_userdata('message_err', 'Failure to Add');
                            redirect(base_url().'admin/ads/create_ads');
                        }
                    } else {
                        $this->session->set_userdata('message_err', 'This is not a video file, please upload MP4, 3GP or OGG file');
                        redirect(base_url().'admin/ads/create_ads');
                    }
                    $data_insert['custom_commercial'] = $custom_vids;
                }
            }
            $data_insert['upload_sponsered_by_logo'] = $upload_sponsered_by_logo;
            if ($_POST['select_type'] == 'commercial_ads') {
                $data_insert['sponsered_by_logo_title'] = '';
                $data_insert['sponsered_by_logo_link'] = '';
                $data_insert['sponsered_by_logo_description'] = '';
            }
            if (isset($_POST['select_product_id']) && !empty($_POST['select_product_id']) && $_POST['select_sub_cat_id'] !='') {
                $select_product_id = implode(',', $_POST['select_product_id']);
                $data_insert['product_id'] = $select_product_id;
                $data_insert['product_cat_id'] = $_POST['select_sub_cat_id'];
            }
            if (isset($_POST['submit'])) {
                $g = $this->General_model->insert_data('ads_tbl',$data_insert);
                if ($g) {
                    $this->session->set_userdata('message_succ', 'Successfully inserted');
                    if ($_POST['select_ads_type'] == 'product_ads') {
                        redirect(base_url().'admin/products/product_ads');
                    } else {
                        redirect(base_url().'admin/ads/ads_list');
                    }
                }
            }
        }
    }
    public function edit_ads(){
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $data['edit_id'] = $_GET['id'];
        $data['ads_single_row'] = $this->General_model->view_data('ads_tbl', array(
            'id' => $data['edit_id']
        ));
        if ($_GET['select_ads_type'] == 'product_ads') {
            $this->breadcrumbs->push('Product Ads List', 'admin/products/product_ads');
            $this->breadcrumbs->push('Edit Product Ads', 'admin/products/edit_product_ads');
            $data['active'] = 'product_ads';
            $data['page_name'] = 'Edit Product Ads';

            $excat = explode(',', $data['ads_single_row'][0]['product_cat_id']);
            $data['sel_child_cat'] = $excat[0];
            $data['sel_parent_cat'] = $excat[1];

            $arr['parent_id'] = '0';
            $data['main_catlist'] = $this->Product_model->category_list($arr);

            $arr['parent_id'] = $data['sel_parent_cat'];
            $data['sub_catlist'] = $this->Product_model->category_list($arr);

            $list_arr['main_cat'] = $data['sel_child_cat'];
            $data['product_list'] = $this->Product_model->SearchProductArr($list_arr);
        } else {
            $this->breadcrumbs->push('Ads List', 'admin/ads/ads_list');
            $this->breadcrumbs->push('Edit Ads', 'admin/ads/edit_ads');
            $data['active'] = 'ads_list';
            $data['page_name'] = 'Edit Ads';
        }
        $content = $this->load->view('admin/ads_management/edit_ads.tpl.php',$data,true);
        $this->render($content);
    }
    public function update_single_data(){
        $data_update = array(
            'sponsered_by_logo_title' => $_POST['sponsered_by_logo_title'],
            'sponsered_by_logo_link' => $_POST['sponsered_by_logo_link'],
            'sponsered_by_logo_description' => $_POST['sponsered_by_logo_description'],
            'ads_type' => $_POST['select_ads_type'],
            'created_date' => date('Y-m-d H:i:s')
        );
        $upload_sponsered_by_logo = $_POST['upload_sponsered_by_logo_image'];
        if (isset($_FILES["upload_sponsered_by_logo"]["name"])) {
            if ($_FILES["upload_sponsered_by_logo"]["name"]!='') {
                $upload_sponsered_by_logo = time().basename($_FILES["upload_sponsered_by_logo"]["name"]);
                $ext = strtolower(end((explode(".", $upload_sponsered_by_logo))));
                if($ext=='jpg' || $ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg'){
                    $target_file_log = $this->ads_master_upload.$upload_sponsered_by_logo;
                    if(move_uploaded_file($_FILES["upload_sponsered_by_logo"]["tmp_name"], $target_file_log)){
                     if(file_exists($this->ads_master_upload.$_POST['upload_sponsered_by_logo_image'])){
                            unlink($this->ads_master_upload.$_POST['upload_sponsered_by_logo_image']);
                        }
                    }
                } else {
                    $this->session->set_userdata('message_err', 'This is not a image file');
                    redirect(base_url().'admin/ads/edit_ads/?id='.$_POST['id']);
                }
            }
        }

        $custom_vids == $_POST['old_custom_commercial'];
        if ($_POST['select_vids_type'] == 'youtube_link') {
            $data_update['youtube_link'] = $_POST['youtube_link'];
            $data_update['custom_commercial'] = '';
        } else {
            if ($_FILES['custom_vids']['size'] != 0) {
                $custom_vids = time().basename($_FILES["custom_vids"]["name"]);
                $ext = strtolower(end((explode(".", $custom_vids))));
                if ($ext == 'mp4' || $ext == '3gp' || $ext == 'ogg') {
                    $target_file = $this->ads_master_upload.$custom_vids;
                    $r = move_uploaded_file($_FILES["custom_vids"]["tmp_name"], $target_file);
                    if (!$r) {
                        $this->session->set_userdata('message_err', 'Failure to Add');
                        redirect(base_url().'admin/ads/edit_ads/?id='.$_POST['id']);
                    }
                } else {
                    $this->session->set_userdata('message_err', 'This is not a video file, please upload MP4, 3GP or OGG file');
                    redirect(base_url().'admin/ads/edit_ads/?id='.$_POST['id']);
                }
            }
            $data_update['youtube_link'] = '';
            $data_update['custom_commercial'] = $custom_vids; 
        }

        if ($_POST['select_type'] == 'commercial_ads') {
            $data_update['sponsered_by_logo_title'] = '';
            $data_update['sponsered_by_logo_link'] = '';
            $data_update['sponsered_by_logo_description'] = '';
        }
        if (isset($_POST['select_product_id']) && !empty($_POST['select_product_id'])) {
            $data_update['product_cat_id'] = $_POST['select_sub_cat_id'];
            $select_product_id = implode(',', $_POST['select_product_id']);
            $data_update['product_id'] = $select_product_id;
        }
        if (isset($_POST['submit'])) {
            $r = $this->General_model->update_data('ads_tbl',$data_update,array('id' => $_POST['id']));
            ($r) ? $this->session->set_userdata('message_succ', 'Successfully updated') : $this->session->set_userdata('message_succ', 'Successfully updated');
            if ($_POST['select_ads_type'] == 'product_ads') {
                redirect(base_url().'admin/products/product_ads');
            } else {
                redirect(base_url().'admin/ads/ads_list');
            }
        }
    }
    public function delete_ads_data($id){
    	$this->db->select('*');
        $this->db->from('ads_tbl');
        $this->db->where('id',$id);
        $query = $this->db->get();
        $ads_data = $query->row();
        unlink($this->ads_master_upload.$ads_data->upload_sponsered_by_logo);
        unlink($this->ads_master_upload.$ads_data->custom_commercial);
        $r = $this->db->delete('ads_tbl', array('id' => $id));
        if ($r) {
            $this->session->set_userdata('message_succ', 'Deleted Successfully');
            if ($ads_data->product_id != '' && $ads_data->product_id != NULL) {
                redirect(base_url().'admin/products/product_ads');
            } else {
                redirect(base_url().'admin/ads/ads_list');
            }
        }
    }    
} 