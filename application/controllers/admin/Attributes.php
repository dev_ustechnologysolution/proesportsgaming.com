<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Attributes extends My_Controller {
	function __construct() {
		parent::__construct();
	}
	public function index() {
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
		$this->breadcrumbs->push('Attributes List', 'admin/attributes');
		$data['active']='attributes_list';
		$data['page_name']='Attributes List';
		$data['list']=$this->General_model->view_all_data('attributes_tbl','id','desc');
		$content=$this->load->view('admin/attributes/attributes_list.tpl.php',$data,true);
		$this->render($content);
	}
	
	public function attributeAdd() {
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
		$this->breadcrumbs->push('Attributes List', 'admin/attributes');
		$this->breadcrumbs->push('Add Attribute', 'admin/attributes/attributeAdd');
		$data['active']='attributes_list';
		$id = $_REQUEST['id'];
		$data['attributes_data']=$this->General_model->view_data('attributes_tbl',array('id'=>$id));
		$data['attr_id'] = $id;
		$data['page_name']='Add Attribute';		
		$content=$this->load->view('admin/attributes/attributes.tpl.php',$data,true);
		$this->render($content);
	}
	public function attributeEdit() {
		if (!empty($_GET['id'])) {
			$this->breadcrumbs->push('Home', 'admin/dashboard');
			$this->breadcrumbs->push('Attributes List', 'admin/attributes');
			$this->breadcrumbs->push('Edit Attribute', 'admin/attributes/attributeEdit');
			$id = $_GET['id'];
			$data['attributes_data'] = $this->General_model->view_data('attributes_tbl',array('id'=>$id));
			// $option_data = $this->General_model->view_data('product_attributes_opt_tbl',array('attr_id'=>$id));
			$option_data = $this->General_model->view_data_by_order('product_attributes_opt_tbl', array('attr_id'=>$id),'opt_order', 'asc');
			$data['option_data'] = $option_data;
			$data['active'] = 'attributes_list';
			$data['page_name'] = 'Attribute Edit';
			$content = $this->load->view('admin/attributes/attributes_edit.tpl.php',$data,true);
			$this->render($content);
		}
	}
	
	public function attributeInsert() {
		try {
			$this->check_user_page_access();
			if ($_POST['type'] == 'attribute') {
				$max_order = $this->db->select_max('attr_order')->from('attributes_tbl')->get()->row()->attr_order;
				$query = $this->db->get_where('attributes_tbl', array('title' => strtolower(trim($_POST['title'])))); 
				if ($query->num_rows() == 0 ) {
					$data = array(
						'type' => (!empty($_POST['select_attr_type'])) ? $_POST['select_attr_type'] : 0,
						'title' => strtolower(trim($_POST['title'])),
						'status' => 1,
						'attr_order' => $max_order+1,
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s')
					);	
					(!empty($_POST['upload_img_opt']) && $_POST['upload_img_opt'] == 'on') ? $data['image_upload'] = 1 : '';
					$attr_id = $this->General_model->insert_data('attributes_tbl',$data);
					($attr_id) ? $this->session->set_userdata('message_succ', 'Insert successfull') : $this->session->set_userdata('message_err', 'Failure to update');
				} else {
					$this->session->set_userdata('message_err', 'Given Attribute is existing.Please Enter another attribute.');	
				}
				redirect(base_url().'admin/attributes');
			} else {
				if (!empty($_POST['option_label'][0])){
					$max_order = $this->db->select_max('opt_order')->from('product_attributes_opt_tbl')->get()->row()->opt_order;
					foreach ($_POST['option_label'] as $key => $value) {
						$option_data[] = array(
							'attr_id' => $_POST["attr_id"],
							'opt_order' => $max_order+1,
							'extra_amount' => $_POST['value_amount'][$key],
							'option_label' => $value,
							'created_at' => date('Y-m-d H:i:s'),
							'updated_at' => date('Y-m-d H:i:s')
						);
						$max_order++;
					}
				 	$result = $this->db->insert_batch('product_attributes_opt_tbl',$option_data);
				 	($result) ? $this->session->set_userdata('message_succ', 'Insert successfull') : $this->session->set_userdata('message_err', 'Failure to update');
				 	redirect(base_url().'admin/attributes');
				}
			}
		} catch (Exception $e) { 
			$this->session->set_userdata('message_err', $e->getMessage());
			redirect(base_url().'admin/attributes');
    }
	}
	
	public function attributeUpdate() {
		
		try {
			$this->check_user_page_access();
			$id = $this->input->post('id', TRUE);
			$attribute_data = $this->General_model->view_data('product_attributes_opt_tbl',array('attr_id'=>$id));
			$list_option_id = array_column($attribute_data, 'option_id');
			$insert_data_arr = (!empty($_POST['option_data'])) ? array_diff($list_option_id,$_POST['option_data']) : $list_option_id;
			(!empty($insert_data_arr)) ? $this->db->where_in('option_id', $insert_data_arr)->delete('product_attributes_opt_tbl') : '';

			$data = array(
				'type' => 3,
				'title' => trim($_POST['title']),
				'type' => $_POST['select_attr_type'],
				'image_upload' => (!empty($_POST['upload_img_opt']) && $_POST['upload_img_opt'] == 'on') ? 1 : 0,
				'updated_at' => date('Y-m-d H:i:s')
			);
			$r = $this->General_model->update_data('attributes_tbl',$data,array('id'=>$id));
			if ($r) {
				$insert_data = $update_data = array();
				if (!empty($_POST['option_label'][0])) {
					foreach($_POST['option_label'] as $key => $value){						
						foreach ($_POST['option_data'] as $k => $v) {							
							if($k == $key) {
								$update_data[] = array(
									'option_id' => $v,
									'opt_order' => $_POST['opt_order'][$k],
									'option_label' => $value,
									'extra_amount' => $_POST['value_amount'][$k],
									'updated_at' => date('Y-m-d H:i:s')
								);
							} 
						}	
					}
					$insert_data_arr = array_diff_key($_POST['option_label'],$_POST['option_data']);
					if (!empty($insert_data_arr)) {
						foreach ($insert_data_arr as $label) {
						 	$insert_data[] = array(
								'attr_id' => $id,
								'opt_order' => $_POST['opt_order'][$k],
								'option_label' => $label,
								'extra_amount' => $_POST['value_amount'][$k],
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
							);
						}
						$this->db->insert_batch('product_attributes_opt_tbl',$insert_data);	 
					}
					if (!empty($update_data)) {
						$this->db->update_batch('product_attributes_opt_tbl',$update_data, 'option_id'); 
					}
				}
				$this->session->set_userdata('message_succ', 'Update successfull');
				redirect(base_url().'admin/attributes/attributeEdit?id='.$this->input->post('id', TRUE));
			} else {
				$this->session->set_userdata('message_err', 'Failure to update');	
				redirect(base_url().'admin/attributes/attributeEdit?id='.$this->input->post('id', TRUE));
			}
		} catch (Exception $e) { 
      $this->session->set_userdata('message_err', $e->getMessage());
      redirect(base_url().'admin/attributes/attributeEdit?id='.$this->input->post('id', TRUE));
    }
	}
	public function delete() 
	{
		$id = $_GET['id'];
		$this->check_user_page_access();
		$r=$this->General_model->delete_data('attributes_tbl',array('id'=> $id));
		if ($r) {
			$this->session->set_userdata('message_succ', 'Delete Successfull');	
			header('location:'.base_url().'admin/attributes');
		} else {
			$this->session->set_userdata('message_err', 'Failure to delete');	
			header('location:'.base_url().'admin/attributes');
		}
	}
	public function updateIsStatus()
	{
		$id 	= $_REQUEST['id'];
		$type 	= $_REQUEST['type'];
		$status = ($type == 'check') ? 1 : 0;
		$data=array(
			'status'=>$status,
		);
		$this->General_model->update_data('attributes_tbl',$data,array('id'=>$id));		
	}

	public function ChangeAttributeListOrder() {
        $id = $_REQUEST['id'];
        $attr_data = $this->General_model->view_data('attributes_tbl',array('id'=>$id));
        $data_value = '<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Attribute Order   '.$attr_data[0]['attr_order'].'</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <form action="#" id="change_attr_order" name="change_attr_order" method="POST">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Change Attribute Order </label>
                            <input type="hidden" id="attr_id" class="form-control" name="attr_id" value="'.$id.'" required>
                            <input type="text" id="new_order" class="form-control" name="new_order" value="" required>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input class="btn btn-primary" type="submit" value="Submit" >
                        <button type="button" class="btn btn-info pull-right" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>';
        echo $data_value;
    }
	
	public function UpdateAttributeOrder(){
        $attr_id   = $_REQUEST['attr_id'];
        $new_order  = $_REQUEST['new_order'];
        $attr_data = $this->General_model->view_data('attributes_tbl',array('id'=>$this->input->post('attr_id', TRUE)));
        if ($attr_data[0]['attr_order'] != $_REQUEST['new_order']) {
            $this->General_model->update_data('attributes_tbl',array('attr_order' => $attr_data[0]['attr_order']),array('attr_order'=>$_REQUEST['new_order']));
        } 
        $data['attr_order'] = $new_order;
        $this->General_model->update_data('attributes_tbl',$data,array('id'=>$attr_id));
    }
}

