<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Backup extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('file');
		$this->load->library('zip');
		$this->load->model('Sendinblue_model');
	}
  public function index() {
  	$this->check_user_page_access();
		// add breadcrumbs
		$this->breadcrumbs->push('Home', 'admin/dashboard');
		$this->breadcrumbs->push('Site Backup', 'admin/Backup');
		$data['active']='site_backup';
		$data['page_name']='Site Backup';
		$data['backup_cat']='';
		$content=$this->load->view('admin/site_backup/site_backup.tpl.php',$data,true);
		$this->render($content);
	}
	public function file_backup() {
		$this->check_user_page_access();
		$this->load->library('zip');
		$file_name='backup['.date('d-m-Y-H-i-s').'].zip';
		$this->zip->read_dir($_SERVER['DOCUMENT_ROOT'],TRUE);
		$this->zip->download($file_name);
		exit();	
	}
	public function db_backup() {
		$this->check_user_page_access();
	 	$this->load->dbutil();
		$prefs = array(
			'format' => 'sql',
			'filename' => 'db_backup.sql'
		);
		$backup = & $this->dbutil->backup($prefs);
		$db_name = 'db_backup-on-' . date("Y-m-d-H-i-s") . '.sql';
		$save = $_SERVER['DOCUMENT_ROOT'].FCPATH.'/assets/dwnld/' . $db_name;
		write_file($save, $backup);
		$this->load->helper('download');
		force_download($db_name, $backup);
	}
	public function db_backup_cron() {
		$main_site_url = base_url();
		// $db_saved_path = (ENVIRONMENT == 'production') ? $_SERVER['DOCUMENT_ROOT'].'/database-proesportsgaming-live-website/' : ((ENVIRONMENT == 'testing') ? $_SERVER['DOCUMENT_ROOT'].'/database-proesportsgaming-test-website/' : $_SERVER['DOCUMENT_ROOT'].'/database-proesportsgaming-test-website/');
		$env = ($_SERVER['SERVER_NAME'] == 'proesports.com' || $_SERVER['SERVER_NAME'] == 'proesportsgaming.com') ? 'production' : 'development'; 

		$db_saved_path = ($env == 'production') ? $_SERVER['DOCUMENT_ROOT'].'/database-proesportsgaming-live-website/' : $_SERVER['DOCUMENT_ROOT'].'/database-proesportsgaming-test-website/';

		if (is_dir($db_saved_path)) {
			$this->load->dbutil();
			$prefs = array(
				'format' => 'sql',
				'filename' => 'db_backup.sql'
			);
			$backup = $this->dbutil->backup($prefs);
			$db_name = 'db_backup-on-' . date("Y-m-d-H-i-s") . '.sql';
			$zipfilename = 'db_backup-on-' . date("Y-m-d-H-i-s") . '.zip';
			// $save = $db_saved_path . $db_name;
			// $res = write_file($save, $backup);
			$this->zip->add_data($db_name, $backup);
			$res = $this->zip->archive($db_saved_path.$zipfilename);
			if ($res) {
				$send_email = 'success';
				// $twomonthdate = date("Y-m-d H:i:s", strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . "-2 months"));
				$twomonthdate = date("Y-m-d H:i:s", strtotime("-2 months"));
				$dh = opendir($db_saved_path);
				while (($file = readdir($dh)) !== false) {
					$expld = explode('.', $file);
					// if ($expld[1] == 'sql') {
					// 	$zipfile = $expld[0].'.zip';
					// 	$this->zip->add_data($file);
					// 	$this->zip->archive($db_saved_path.$zipfile);
					// }
					// if ($expld[1] == 'zip') {
						$getfiledatetime = trim(str_replace("db_backup-on-","",$file),'.'.$expld[1]);
						preg_match_all('/(\d+)-(\d+)-(\d+)-(\d+)-(\d+)-(\d+)/', $getfiledatetime, $out, PREG_SET_ORDER);
						$out = $out[0];
						$time = mktime($out[4], $out[5], $out[6], $out[2], $out[3], $out[1]);
						$getfiledatetime = date('Y-m-d H:i:s', $time);
						if (strtotime($twomonthdate) > strtotime($getfiledatetime)) {
							unlink($db_saved_path.$file);
						}
					// }
				}
			} else {
				$send_email = 'error';
				echo $save;
			}
			$subject = ($env == 'production') ? "Daily Production database backup" : (($env == 'testing') ? "Daily Testing database backup" : "Daily Development database backup");
			$message = ($send_email != 'error') ? 'Please find attached Zip file of database' : 'Error To save the database';
			$to = array("payments@proesportsgaming.com" => 'User');
			// $to = array("vaghelamehul250@gmail.com" => 'User');
			$cc = array("mehul@aspireedge.com" => "user2", "amit@aspireedge.com" => "user3");
			$fh = file_get_contents($db_saved_path.$zipfilename);
			$attachment = ($send_email != 'error') ? array($zipfilename=>chunk_split(base64_encode($fh))) : '';
			$data = array(
				'to' => $to,
				"cc" => $cc,
				'from' => array('noreply@ProEsportsGaming.com','Pro Esports Gaming'),
				'subject' => $subject,
				'html' => $message,
				"attachment" => $attachment,
				"headers" => array("Content-Type"=> "text/html; charset=iso-8859-1","X-param1"=> "value1", "X-param2"=> "value2","X-Mailin-custom"=>"my custom value", "X-Mailin-IP"=> "102.102.1.2", "X-Mailin-Tag" => "My tag"),
			);
			$testmail = $this->Sendinblue_model->send_email($data);
			closedir($dh);
			echo ($testmail) ? "Database exported and mail sent" : "Database exported but mail error";
			exit();
		} else {
			echo $db_saved_path.' <b> Not found</b>';
			exit();
		}
	}
}