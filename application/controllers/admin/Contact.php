<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Contact extends My_Controller 
{
  function __construct()
    {
	parent::__construct();
	}

	public function index()
	{
		$this->check_user_page_access();
		// add breadcrumbs
		$this->breadcrumbs->push('Home', 'dashboard');
        $this->breadcrumbs->push('Contact List', 'admin/contact/contact_list');
		$this->breadcrumbs->push('Add Contact', 'contact');
		$data['active']='contact';
		$content=$this->load->view('admin/contact/contact.tpl.php',$data,true);
		$this->render($content);
	}
	
	public function add_contact()
	{
		$this->check_user_page_access();
		$data=array(
		'name'=>$this->input->post('name', TRUE),
		'email'=>$this->input->post('email', TRUE),
		'phone'=>$this->input->post('phone', TRUE),
		'user_id'=>$this->session->userdata('user_id'),
		 );
		 if($this->input->post('name', TRUE)!='')
		 {
			$r=$this->General_model->insert_data('cre_contact',$data);
			if($r)
			{
			$this->session->set_userdata('message_succ', 'Successfully added');	
			header('location:'.base_url().'admin/contact/contact_edit/'.$r);
			}
			else
			{
			$this->session->set_userdata('message_err', 'Failure to add');	
			header('location:'.base_url().'admin/contact');
			}
		}
		else
		{
			header('location:'.base_url().'admin/contact');
		}
		
	}
	
	public function contact_list()
	{
		$this->check_user_page_access();
		// add breadcrumbs
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Contact List', 'admin/contact/contact_list');
		$data['active']='contact_list';
		$data['list']=$this->General_model->view_all_data('cre_contact');
		$content=$this->load->view('admin/contact/contact_list.tpl.php',$data,true);
		$this->render($content);
		
	}
	//load edit page
	public function contact_edit($id='-1')
	{
		$this->check_user_page_access();
		// add breadcrumbs
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Contact List', 'admin/contact/contact_list');
		$this->breadcrumbs->push('Edit Contact', 'admin/contact');
		$data['active']='contact_edit';
		$data['contact']=$this->General_model->view_data('cre_contact',array('id'=>$id));
		$content=$this->load->view('admin/contact/contact_edit.tpl.php',$data,true);
		$this->render($content);
	}
	
	//edit 
	public function contact_update()
	{
		$this->check_user_page_access();
		$data['active']='contact_edit';
		$data=array(
		'name'=>$this->input->post('name', TRUE),
		'email'=>$this->input->post('email', TRUE),
		'phone'=>$this->input->post('phone', TRUE),
		'user_id'=>$this->session->userdata('user_id'),
		 );
		 $r=$this->General_model->update_data('cre_contact',$data,array('id'=>$this->input->post('id', TRUE)));
		if($r)
			{
			$this->session->set_userdata('message_succ', 'Update successfull');	
			header('location:'.base_url().'admin/contact/contact_edit/'.$this->input->post('id', TRUE));
			}
			else
			{
			$this->session->set_userdata('message_err', 'Failure to update');	
			header('location:'.base_url().'admin/contact/contact_edit/'.$this->input->post('id', TRUE));
			}
		
	}
	//delete
	public function delete($id)
	{
		$this->check_user_page_access();
		
		 $r=$this->General_model->delete_data('cre_contact',array('id'=>$id));
		if($r)
			{
			$this->session->set_userdata('message_succ', 'Delete Successfull');	
			header('location:'.base_url().'admin/contact/contact_list/');
			}
			else
			{
			$this->session->set_userdata('message_err', 'Failure to delete');	
			header('location:'.base_url().'admin/contact/contact_list/');
			}
		
	}
	
}
