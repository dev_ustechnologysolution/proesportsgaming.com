<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Content extends My_Controller {
  	function __construct() {
		parent::__construct();
    	$this->target_dir=BASEPATH.'../upload/admin_dashboard/';
	}
	public function index(){
		$this->check_user_page_access();
		// add breadcrumbs
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Content List', 'admin/content');

		$data['active']='content_list';
		$data['page_name']='Content List';

		

		$data['list']=$this->General_model->view_all_data('footer_content','id','asc');
		$content=$this->load->view('admin/footercontent/content_list.tpl.php',$data,true);
		$this->render($content);
	}

	public function admin_map_video() {
		$this->check_user_page_access();
		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('Admin Map Video List', 'admin/content/admin_map_video');


		$data['active']='admin_map';

		$data['page_name']='Admin Map Video List';

		

		$data['list']=$this->General_model->view_all_data('footer_content','id','asc');

		$content=$this->load->view('admin/footercontent/admin_map_list.tpl.php',$data,true);

		$this->render($content);

	}

	public function cover_password() {
		$this->check_user_page_access();
		// add breadcrumbs
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Cover Password', 'admin/content/cover_password');
		$data['active'] = 'cover_password';
		$data['page_name'] = 'Cover Page';
		$data['list'] = $this->General_model->view_data('cover',array('server_name' => $_SERVER['SERVER_NAME']))[0];
		$content = $this->load->view('admin/cover/cover.tpl.php',$data,true);
		$this->render($content);
	}	

	public function changeCoverStatus($status){
		$status_data['status'] = ($status == 'enable') ? '1' : '0';
		$this->General_model->update_data('cover', $status_data, array('server_name' => $_SERVER['SERVER_NAME']));
	}

	public function cover_password_edit($id)
	{
		echo '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="cover_pw_id" name="cover_pw_id" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Admin Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
								<input type="hidden" id="old_fees" class="form-control" name="old_fees" value="'.$id.'" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
	}

	public function cover_password_change()
	{
		$security_password 	= $_REQUEST['security_password'];
		$old_fees = $_REQUEST['old_fees'];
		$security_password_db = $this->General_model->view_all_data('admin_security_password','id','asc');
		if($security_password == $security_password_db[0]['password']){
            $cover_data = $this->General_model->view_data('cover',array('server_name' => $_SERVER['SERVER_NAME']))[0];

			$data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Current Cover Password :-  '.$cover_data['password'].'</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="editpw_id" name="editpw_id" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Edit Cover Password</label>
								<input type="text" id="new_password" class="form-control" name="new_password" value="" required>
								
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
		} else{
			$data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="cover_pw_id" name="cover_pw_id" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Admin Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
								
							</div>
								<label for="name">Password Error</label>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
		}	
		echo $data_value;
	}



	public function update_cover_pw() {
		$new_password = trim($_REQUEST['new_password']);
		if (!empty($new_password)) {
			$server_name = $_SERVER['SERVER_NAME'];
			$data = array(
				'password' => $new_password,
				'create_date' => date('Y-m-d H:i:s')
			);
			$r = $this->General_model->update_data('cover', $data, array('server_name' => $server_name));
		}
	}

	
	//load edit page

	public function contentEdit($id='-1')

	{

		$this->check_user_page_access();

		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('Content List', 'admin/content');

		$this->breadcrumbs->push('Edit Content', 'admin/content/contentEdit');



		$data['active']='content_edit';

		$data['page_name']='Edit Page';



		$data['content_data']=$this->General_model->view_data('footer_content',array('id'=>$id));

		$content=$this->load->view('admin/footercontent/content_edit.tpl.php',$data,true);

		$this->render($content);

	}

	//edit 

	public function contentUpdate()

	{

		$this->check_user_page_access();

		$data['active']='game_edit';

		$data=array(

			'content'=>$this->input->post('content', TRUE)
		 );

		 $r=$this->General_model->update_data('footer_content',$data,array('id'=>$this->input->post('id', TRUE)));

		if($r)

		{

			$this->session->set_userdata('message_succ', 'Update successfull');	

			redirect(base_url().'admin/content/contentEdit/'.$this->input->post('id', TRUE));

		}

		else

		{

			$this->session->set_userdata('message_err', 'Failure to update');	

			redirect(base_url().'admin/content/contentEdit/'.$this->input->post('id', TRUE));

		}	

	}
	public function adminmapEdit($id='-1')

	{

		$this->check_user_page_access();

		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('Admin Map Video List', 'admin/content/admin_map_video');

		$this->breadcrumbs->push('Edit Admin Map Video', 'admin/content/adminmapEdit');



		$data['active']='admin_map';

		$data['page_name']='Edit Admin Map Video ';



		$data['content_data']=$this->General_model->view_data('footer_content',array('id'=>$id));

		$content=$this->load->view('admin/footercontent/admin_map_edit.tpl.php',$data,true);

		$this->render($content);

	}

	public function changeStatus($active,$deactive)
	{
		$active_data=array(
            'status'=>'1',
            );
        $this->General_model->update_data('footer_content',$active_data,array('id'=>$active));

 		$deactive_data=array(
            'status'=>'0',
            );
        $this->General_model->update_data('footer_content',$deactive_data,array('id'=>$deactive));
	}

	//edit 

	public function adminmapUpdate()

	{
		$this->check_user_page_access();

		$data['active']='admin_map';
	
		$vid='';

		if(isset($_FILES["admin_map_video"]["name"]) && $_FILES["admin_map_video"]["name"]!='')

		{

			$vid=time().basename($_FILES["admin_map_video"]["name"]);

			$ext_vid = end((explode(".", $vid)));

			if($ext_vid=='wmv' || $ext_vid=='mp4' || $ext_vid=='avi' || $ext_vid=='mov' || $ext_vid=='3gp')
				{

					$target_file = $this->target_dir.$vid;

			        if (move_uploaded_file($_FILES["admin_map_video"]["tmp_name"], $target_file)) {
			        	
			        	if(file_exists($this->target_dir.$this->input->post('old_admin_map_video', TRUE)))

						{

							unlink($this->target_dir.$this->input->post('old_admin_map_video', TRUE));

						}

			        }
			    }
			    else
			    {
			    	$this->session->set_flashdata('msg', 'This is not a video file');	
					redirect(base_url().'admin/banner/bannerAdd');
			    }

		}


		$data=array(

			'content'=>$vid
		 );

		 $r=$this->General_model->update_data('footer_content',$data,array('id'=>$this->input->post('id', TRUE)));

		if($r)

		{

			$this->session->set_userdata('message_succ', 'Update successfull');	

			redirect(base_url().'admin/content/adminmapEdit/'.$this->input->post('id', TRUE));

		}

		else

		{

			$this->session->set_userdata('message_err', 'Failure to update');	

			redirect(base_url().'admin/content/adminmapEdit/'.$this->input->post('id', TRUE));

		}	

	}
	
}

