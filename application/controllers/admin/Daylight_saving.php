<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Daylight_saving extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->check_user_page_access();
	}
	public function index() {
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Daylight Saving Option', 'admin/Daylight_saving');
        $data['active'] = 'dst_option';
		$data['page_name'] = 'Daylight Saving Option';

        $opt_where['option_title'] = 'global_dst_option';
        $data['global_dst_option'] = $this->General_model->view_data('site_settings', $opt_where)[0];

        $opthr_where['option_title'] = 'global_dst_hour';
        $data['global_dst_hour'] = $this->General_model->view_data('site_settings', $opthr_where)[0];

		$content = $this->load->view('admin/site_settings/dst.tpl.php',$data,true);
		$this->render($content);
    }
    public function global_dst_hour_change() {
    	if (!empty($_POST['global_dst_hour'])) {
    		$global_dst_hour = $_POST['global_dst_hour'];
    		$optdata['option_title'] = 'global_dst_hour';
    		$get_global_dst_hour = $this->General_model->view_data('site_settings', $optdata);

    		if (!empty($get_global_dst_hour)) {
    			$update_data['option_value'] = $global_dst_hour;
                $r = $this->General_model->update_data('site_settings', $update_data, $optdata);
    		} else {
    			$optdata['option_value'] = $global_dst_hour;
	            $r = $this->General_model->insert_data('site_settings', $optdata);
    		}
    		($r) ? $this->session->set_flashdata('message_succ', 'Update successfully.'): $this->session->set_flashdata('message_err', 'Fail to Update');

    		redirect(base_url().'admin/daylight_saving');
    	}
    }
    public function global_dst_opt_change() {
    	if (!empty($_POST['global_dst_option'])) {
    		$global_dst_option = $_POST['global_dst_option'];
    		$optdata['option_title'] = 'global_dst_option';
    		$get_global_dst_option = $this->General_model->view_data('site_settings', $optdata);

    		if (!empty($get_global_dst_option)) {
    			$update_data['option_value'] = $global_dst_option;
                $this->General_model->update_data('site_settings', $update_data, $optdata);
    		} else {
    			$optdata['option_value'] = $global_dst_option;
	            $r = $this->General_model->insert_data('site_settings', $optdata);
    		}
    	}
    }
}