<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Default_Settings extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->check_user_page_access();
	}
	public function index() {
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Default Settings', 'admin/Default_Settings');
        $data['active'] = 'default_settings';
		$data['page_name'] = 'Default Settings';

        $opt_where['option_title'] = 'default_stream_record_limit';
        $data['default_stream_record_limit'] = $this->General_model->view_data('site_settings', $opt_where)[0];

        $opt_where['option_title'] = 'default_lobby_creation_limit';
        $data['default_lobby_creation_limit'] = $this->General_model->view_data('site_settings', $opt_where)[0];

        $opt_where['option_title'] = 'default_lobby_join_limit';
        $data['default_lobby_join_limit'] = $this->General_model->view_data('site_settings', $opt_where)[0];

		$content = $this->load->view('admin/site_settings/default_settings.tpl.php',$data,true);
		$this->render($content);
    }
    public function set_default_values() {
    	if (!empty($_POST['default_stream_record_limit']) && !empty($_POST['default_lobby_creation_limit']) && !empty($_POST['default_lobby_join_limit'])) {
    		$default_stream_record_limit = $_POST['default_stream_record_limit'];
    		$optdata['option_title'] = 'default_stream_record_limit';
    		$get_default_stream_record_limit = $this->General_model->view_data('site_settings', $optdata);

    		if (!empty($get_default_stream_record_limit)) {
    			$update_data['option_value'] = $default_stream_record_limit;
                $r = $this->General_model->update_data('site_settings', $update_data, $optdata);
    		} else {
    			$optdata['option_value'] = $default_stream_record_limit;
	            $r = $this->General_model->insert_data('site_settings', $optdata);
    		}

            $default_lobby_creation_limit = $_POST['default_lobby_creation_limit'];
            $optdata['option_title'] = 'default_lobby_creation_limit';
            $get_default_lobby_creation_limit = $this->General_model->view_data('site_settings', $optdata);

            if (!empty($get_default_lobby_creation_limit)) {
                $update_data['option_value'] = $default_lobby_creation_limit;
                $r = $this->General_model->update_data('site_settings', $update_data, $optdata);
            } else {
                $optdata['option_value'] = $default_lobby_creation_limit;
                $r = $this->General_model->insert_data('site_settings', $optdata);
            }

            $default_lobby_join_limit = $_POST['default_lobby_join_limit'];
            $optdata['option_title'] = 'default_lobby_join_limit';
            $get_default_lobby_join_limit = $this->General_model->view_data('site_settings', $optdata);

            if (!empty($get_default_lobby_join_limit)) {
                $update_data['option_value'] = $default_lobby_join_limit;
                $r = $this->General_model->update_data('site_settings', $update_data, $optdata);
            } else {
                $optdata['option_value'] = $default_lobby_join_limit;
                $r = $this->General_model->insert_data('site_settings', $optdata);
            }

    		($r) ? $this->session->set_flashdata('message_succ', 'Update successfully.'): $this->session->set_flashdata('message_err', 'Fail to Update');

    		redirect(base_url().'admin/Default_Settings');
    	}
    }
}