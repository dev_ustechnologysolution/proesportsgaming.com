<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Events extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->check_user_page_access();
		$this->load->model('Tournaments_model');
		$this->eveimg_target_dir = BASEPATH.'../upload/event_key_img/';
		// echo '<br>'.date_default_timezone_get().'<br>';
		// date_default_timezone_set('America/Los_Angeles');
	}
	public function index() {

		$this->breadcrumbs->push('Home', 'admin/dashboard');
	    $this->breadcrumbs->push('Event List', 'admin/events');
	    $data['active'] = 'event_list';
	    $data['page_name'] = 'Event List';
	    $lobby_event_list = $this->Tournaments_model->get_tournaments_event();
	    // echo"<pre>";
	    // print_r($lobby_event_list);
	    // exit();
	    $getevents = $this->Tournaments_model->get_custom_events();
	    $arra = array_merge($lobby_event_list,$getevents);
	    $arr = array_map(function ($value) {
	        $value->start = $this->User_model->custom_date(array('date' => $value->start,'format' => 'Y-m-d\TH:i:sP'));
	        $value->end =  $this->User_model->custom_date(array('date' => $value->end,'format' => 'Y-m-d\TH:i:sP'));
	      return $value;
	    }, $arra);
	    $arr1 = [];
	   
	    foreach ($arr as $key => $ev) {
	    	
	    	if ($ev->title !='') {
	    		// if($ev->is_archive==0)
	    		// {

		     //  		$bg_color = $backgcolor = ($ev->bg_color != '') ? $ev->bg_color : '#ff5000';
	    		// }
	    		// else
	    		// {
	    		// 	$bg_color = $backgcolor =$ev->bg_color='blue';
	    		// }
	    		($ev->is_archive==0) ? $bg_color = $backgcolor = ($ev->bg_color != '') ? $ev->bg_color : '#ff5000' :$bg_color = $backgcolor =  $ev->bg_color = 'lightgray';

		      	if (strpos($ev->bg_color, '_') !== false) {
		          $arrcolor = explode('_', $ev->bg_color);
		          $backgcolor = 'linear-gradient(200deg, '.$arrcolor[0].', '.$arrcolor[1].') 0% 0% / 200% 200%';
		        }

		        ($ev->event_key != 0 && $ev->key_image !='') ? $img = base_url().'upload/event_key_img/'.$ev->key_image : (($ev->proplayer == 1) ? $img = base_url().'assets/frontend/images/pro_player.png' : $img = '');
		        $event['is_proplayre'] = ($img != '') ? '<span class="made_by_proplayer"><img width="20px" height="20px" src="'.$img.'" alt="Pro Player"></span>' : '';

		        $onlyDate = false;
		        $startDate = date('Y-m-d H:i:s', strtotime($ev->start));
	     		$endDate = date('Y-m-d H:i:s', strtotime($ev->end));
		        if (date('H:i:s', strtotime($ev->start)) == '00:00:00' && date('H:i:s', strtotime($ev->end)) == '00:00:00') {
					$onlyDate = true;
		       		$startDate = date('Y-m-d', strtotime($ev->start));
		       		$endDate = date('Y-m-d', strtotime($ev->end));		        	
		        }
		        // $formate = (date('H:i:s', strtotime($ev->start)) == '00:00:00' && date('H:i:s', strtotime($ev->end)) == '00:00:00') ? 'Y-m-d' : 'Y-m-d H:i:s';

		      //   $temparray = explode("T",$ev->start);
		      //   $temparray_two = (strpos($temparray[1], '+') !== false) ? explode("+",$temparray[1]) : ((strpos($temparray[1], '-') !== false) ? explode("-",$temparray[1]) : '');
		      //   $onlyDate = false;
	     		// $startDate = date('Y-m-d H:i:s', strtotime($ev->start));
	     		// $endDate = date('Y-m-d H:i:s', strtotime($ev->end));
	     		// if ($temparray_two == '' || $temparray_two[0] == '' || $temparray_two[0] == '00:00:00') {
		      //  		$onlyDate = true;
		      //  		$startDate = date('Y-m-d', strtotime($ev->start));
		      //  		$endDate = date('Y-m-d', strtotime($ev->end));
	       // 		}


		      	$event['id'] = ($ev->id) ? $ev->id : $ev->lobby_id;
		        $event['title'] = $ev->title;
		        $event['start'] = $startDate;
		        $event['end'] = $endDate;
		        $event['color'] = $bg_color;
		        $event['is_archive'] = $ev->is_archive;
		        $event['bg_color'] = $ev->bg_color;
		        $event['css_backgcolor'] = $backgcolor;
		        $event['event_bg_img'] = ($ev->event_bg_img != '') ? $ev->event_bg_img : '';
		        $event['description'] = $ev->description;
		        $event['url'] = $ev->url;
		        $event['event_key'] = $ev->event_key;
		        $event['event_type'] = ($ev->lobby_id) ? 'lobby_event' : 'custom_event';
		        $event['event_is_expired'] = (strtotime('now') > strtotime($endDate)) ? 'yes' : 'no';
		        $event['onlyDate'] = $onlyDate;
		        
		        $arr1[] = $event;
		    }
	    }
	    $data['evelist'] = json_encode($arr1);
	    $data['keylist'] = $this->Tournaments_model->get_key_list(array());
	    $getdefault_keylist = $this->Tournaments_model->getdefault_keylist(array());
	    $data['getdefault_keylist'] = array_combine(array_column($getdefault_keylist, 'user_id'), $getdefault_keylist);
	    $data['proplayers'] = $this->General_model->view_data('user_detail',array('is_admin'=>1));

	    $content = $this->load->view('admin/event/event_list.tpl.php',$data,true);
	    $this->render($content);
	}
	public function add_events(){
		if (!empty($_POST)) {
			if (!empty($_POST['start_date']) && !empty($_POST['end_date'])) {
				if (strtotime($_POST['start_date']) >= strtotime($_POST['end_date'])) {
					$this->session->set_userdata('message_err', 'End Date must be Greater then Start Date');
					redirect(base_url().'admin/events/');
				}

				$bg_color = (!empty($_POST['eve_bg_color'])) ? ((!empty($_POST['gradient_color'])) ?  $_POST['eve_bg_color'].'_'.$_POST['gradient_color'] : $_POST['eve_bg_color']) : '#ff5000';
				$arr = array(
					'admin_user_id' => $this->session->userdata('admin_user_id'),
					'event_key' => trim($_POST['choose_key']),
					'title' => trim($_POST['eve_title']),
					'description' => (trim($_POST['description']) != '') ? trim($_POST['description']) : '',
					'start' => ($_POST['start_date'] != '') ? date('Y-m-d\TH:i:sP',strtotime($_POST['start_date'])) : '',
					'end' => ($_POST['end_date'] != '') ? date('Y-m-d\TH:i:sP',strtotime($_POST['end_date'])) : '',
					'bg_color' => $bg_color,
					'url' => ($_POST['eve_url'] != '') ? $_POST['eve_url'] : ''
				);

	            $start_arr = array('format' => 'Y-m-d\TH:i:sP', 'date' => $arr['start'], 'reversetime' => 'yes');
	            $arr['start'] = $this->User_model->custom_date($start_arr);

	            $end_arr = array('format' => 'Y-m-d\TH:i:sP', 'date' => $arr['end'], 'reversetime' => 'yes');
	            $arr['end'] = $this->User_model->custom_date($end_arr);

				($_POST['event_bg_img'] != '') ? $arr['event_bg_img'] = $_POST['event_bg_img'] : '';
			    if (isset($_FILES["event_bg_img"]["name"]) && $_FILES["event_bg_img"]["name"]!='') {
			    	$upimg = time().basename($_FILES["event_bg_img"]["name"]);
			    	$ext = end((explode(".", $upimg)));
			    	$extarr = array('gif','jpeg','png','jpg');
			    	if (in_array(strtolower($ext), $extarr)) {
			    		$target_file = $this->eveimg_target_dir.$upimg;
			    		(move_uploaded_file($_FILES["event_bg_img"]["tmp_name"], $target_file)) ? $arr['event_bg_img'] = $upimg : '';
		        	}
			    }
				$r = $this->General_model->insert_data('custom_events',$arr);
				($r) ? $this->session->set_userdata('message_succ', 'Successfully Added') : $this->session->set_userdata('message_err', 'Failure to Add');
			} else {
				$this->session->set_userdata('message_err', 'Failure to Update');
			}
			redirect(base_url().'admin/events/');
		}
	}
	public function edit_events() {
		if (!empty($_POST)) {
			if (!empty($_POST['start_date']) && !empty($_POST['end_date'])) {
				if (strtotime($_POST['start_date']) > strtotime($_POST['end_date']) && $_POST['onlydate'] != 'on') {
					$this->session->set_userdata('message_err', 'End Date must be Greater then Start Date');
					redirect(base_url().'admin/events/');
				}
					
				$formate = ($_POST['onlydate'] == 'on') ? 'Y-m-d' : 'Y-m-d\TH:i:sP';

				$update_start = date($formate,strtotime($_POST['start_date']));
				$update_end = date($formate,strtotime($_POST['end_date']));
				$getevedetail = $this->Tournaments_model->event_start_end_date($_POST)[0];
				if ($getevedetail->timezone != '') {	
					date_default_timezone_set($getevedetail->timezone);
					$update_start = date($formate,strtotime($update_start));
					$update_end = date($formate,strtotime($update_end));
				}

	            $start_arr = array('format' => 'Y-m-d\TH:i:sP', 'date' => $update_start, 'reversetime' => 'yes');
	            $update_start = $this->User_model->custom_date($start_arr);
	            $end_arr = array('format' => 'Y-m-d\TH:i:sP', 'date' => $update_end, 'reversetime' => 'yes');
	            $update_end = $this->User_model->custom_date($end_arr);

				$bg_color = (!empty($_POST['eve_bg_color'])) ? ((!empty($_POST['gradient_color'])) ? $_POST['eve_bg_color'].'_'.$_POST['gradient_color'] : $_POST['eve_bg_color']) : '#ff5000';
				if (isset($_FILES["event_bg_img"]["name"]) && $_FILES["event_bg_img"]["name"]!='') {
					$upimg = time().basename($_FILES["event_bg_img"]["name"]);
			    	$ext = end((explode(".", $upimg)));
			    	$extarr = array('gif','jpeg','png','jpg');
			    	if (in_array(strtolower($ext), $extarr)) {
			    		$target_file = $this->eveimg_target_dir.$upimg;
			    		(move_uploaded_file($_FILES["event_bg_img"]["tmp_name"], $target_file)) ? $arr['event_bg_img'] = $upimg : '';
			    	}
			    }
		
				if (!empty($_GET['cust_event_id'])) {
					$id = $_GET['cust_event_id'];
					$data = array(
						'admin_user_id' => $this->session->userdata('admin_user_id'),
						'title' => trim($_POST['eve_title']),
						'event_key' => trim($_POST['choose_key']),
						'description' => (trim($_POST['description']) != '') ? trim($_POST['description']) : '',
						'start' => $update_start,
						'end' => $update_end,
						'bg_color' => $bg_color,
						'url' => (trim($_POST['eve_url']) != '') ? trim($_POST['eve_url']) : ''
					);
					(isset($arr['event_bg_img']) && $arr['event_bg_img'] != '') ? $data['event_bg_img'] = $arr['event_bg_img'] : '';
					$r = $this->General_model->update_data('custom_events',$data,array('id' => $id));
				} else if (!empty($_GET['lobby_event_id'])) {
					$id = $_GET['lobby_event_id'];
					$data = array(
						'event_title' => trim($_POST['eve_title']),
						'event_description' => (trim($_POST['description']) != '') ? trim($_POST['description']) : '',
						'event_start_date' => $update_start,
						'event_key' => trim($_POST['choose_key']),
						'event_end_date' => $update_end,
						'bg_color' => $bg_color
					);
					(isset($arr['event_bg_img']) && $arr['event_bg_img'] != '') ? $data['event_bg_img'] = $arr['event_bg_img'] : '';
					$r = $this->General_model->update_data('lobby',$data,array('id' => $id));
				}
				($r) ? $this->session->set_userdata('message_succ', 'Successfully Updated') : $this->session->set_userdata('message_err', 'Failure to Update');
			}
			redirect(base_url().'admin/events/');
		}
	}
	public function delete_events($type,$id) {
		if ($type == 'lobby_event') {
			$update_arr = array('is_spectator' => 0, 'is_event' => 0);
			$r = $this->General_model->update_data('lobby',$update_arr,array('id'=>$id));
		} else {
			$r = $this->General_model->delete_data('custom_events',array('id' => $id));
		}
		($r) ? $this->session->set_userdata('message_succ', 'Successfully deleted') : $this->session->set_userdata('message_err', 'Failure to Deleted');
			redirect(base_url().'admin/events/');
	}
	public function add_event_key() {
		if (!empty($_POST['eve_key_title'])) {
			$max_order = $this->db->select_max('order')->get('event_keys')->row();
			$eve_key_title = trim($_POST['eve_key_title']);
			$eve_key_image = '';
	    if (!empty($_FILES["eve_key_image"]["name"])) {
	    	$upimg = time().basename($_FILES["eve_key_image"]["name"]);
	    	$ext = end((explode(".", $upimg)));
	    	$extarr = array('gif','jpeg','png','jpg');
	    	if (in_array(strtolower($ext), $extarr)) {
	    		$target_file = $this->eveimg_target_dir.$upimg;
	    		(move_uploaded_file($_FILES["eve_key_image"]["tmp_name"], $target_file)) ? $eve_key_image = $upimg : '';
	      } else {
	      	$this->session->set_userdata('message_err', 'Please select valid image');
	      	redirect(base_url().'admin/events/');
	      }
	    }
	    $arr = array(
				'key_title' => $eve_key_title,
				'key_image' => $eve_key_image,
				'order' => $max_order->order+1,
			);
			($this->General_model->insert_data('event_keys',$arr)) ? $this->session->set_userdata('message_succ', 'Successfully Added') : $this->session->set_userdata('message_err', 'Failure to Add');
			redirect(base_url().'admin/events/');
		}
	}
	public function edit_event_key($id) {
		if (!empty($_POST['eve_key_title'])) {
			$arr['key_title'] = trim($_POST['eve_key_title']);
			$key_data = $this->Tournaments_model->get_key_list(array('id' => $id))[0];
			if (!empty($_FILES["eve_key_image"]["name"])) {
	    	$upimg = time().basename($_FILES["eve_key_image"]["name"]);
	    	$ext = end((explode(".", $upimg)));
	    	$extarr = array('gif','jpeg','png','jpg');
	    	if (in_array(strtolower($ext), $extarr)) {
	    		$target_file = $this->eveimg_target_dir.$upimg;
	    		if (move_uploaded_file($_FILES["eve_key_image"]["tmp_name"], $target_file)) {
	    			$arr['key_image'] = $upimg;
	    			unlink($this->eveimg_target_dir.$key_data->key_image);
	    		} else {
	    			$this->session->set_userdata('message_err', 'Unable to upload file');
	    			redirect(base_url().'admin/events/');					    			
	    		}
	      } else {
	      	$this->session->set_userdata('message_err', 'Please select valid image');
	      	redirect(base_url().'admin/events/');
	      }
	    }
	    
			($this->General_model->update_data('event_keys',$arr,array('id' => $id))) ? $this->session->set_userdata('message_succ', 'Successfully Updated') : $this->session->set_userdata('message_err', 'Failure to Updated');
			redirect(base_url().'admin/events/');
		}
	}	
	public function delete_key($id) {
		$key_data = $this->Tournaments_model->get_key_list(array('id' => $id))[0];
		$r = $this->General_model->delete_data('event_keys',array('id' => $id));
		unlink($this->eveimg_target_dir.$key_data->key_image);
		($r) ? $this->session->set_userdata('message_succ', 'Successfully deleted') : $this->session->set_userdata('message_err', 'Failure to Deleted');
		redirect(base_url().'admin/events/');	
	}
	public function edit_events_drag_resize() {
		if (!empty($_POST)) {
			$update_start = date('Y-m-d\TH:i:sP',strtotime($_POST['update_start']));
			$update_end = date('Y-m-d\TH:i:sP',strtotime($_POST['update_end']));
			$getevedetail = $this->Tournaments_model->event_start_end_date($_POST)[0];

			if ($getevedetail->timezone != '') {
				date_default_timezone_set($getevedetail->timezone);
				$update_start = date('Y-m-d\TH:i:sP',strtotime($update_start));
				$update_end = date('Y-m-d\TH:i:sP',strtotime($update_end));
			}

            $start_arr = array('format' => 'Y-m-d\TH:i:sP', 'date' => $update_start, 'reversetime' => 'yes');
            $update_start = $this->User_model->custom_date($start_arr);

            $end_arr = array('format' => 'Y-m-d\TH:i:sP', 'date' => $update_end, 'reversetime' => 'yes');
            $update_end = $this->User_model->custom_date($end_arr);


			if ($_POST['event_type'] == 'lobby_event') {
				$data['event_start_date'] = $update_start;
				$data['event_end_date'] = $update_end;
				$tbl_name = 'lobby';
			} else if ($_POST['event_type'] == 'custom_event') {
				$data['start'] = $update_start;
				$data['end'] = $update_end;
				$tbl_name = "custom_events";
			}
			if ($tbl_name) {
				$r = $this->General_model->update_data($tbl_name,$data,array('id' => $_POST['id']));
				($r) ? $this->session->set_userdata('message_succ', 'Successfully Updated') : $this->session->set_userdata('message_err', 'Failure to Update');
				// redirect(base_url().'admin/events/');
			}
		}
	}
	public function change_key_order(){
		if (!empty($_POST)) {
			$keyorder_exist = $this->General_model->view_data('event_keys',array('order'=>$_POST['change_key_order']))[0];
			if (!empty($keyorder_exist)) {
				$exupdate_data['order'] = $_POST['old_key_order'];
				$this->General_model->update_data('event_keys', $exupdate_data, array('id' => $keyorder_exist['id']));
			}
			$update_data['order'] = $_POST['change_key_order'];
			$r = $this->General_model->update_data('event_keys',$update_data,array('id' => $_POST['key_id']));
			($r) ? $this->session->set_userdata('message_succ', 'Successfully Updated') : $this->session->set_userdata('message_err', 'Failure to Update');
			redirect(base_url().'admin/events');
		}
	}
	public function add_default_key() {
		if (!empty($_POST['choose_default_eve_key'])) {
			$choose_default_eve_key = ($_GET['key'] = 'proplayers' && is_array($_POST['choose_default_eve_key'])) ? $_POST['choose_default_eve_key'] : array('0_'.$_POST['choose_default_eve_key']);
			foreach ($choose_default_eve_key as $key => $cdek) {
				if (!empty($cdek)) {
					$cdekarr = explode('_', $cdek);
					$check_userid = trim($cdekarr[0]);
					$choosed_keyid = trim($cdekarr[1]);
					$id_arr =  array($check_userid);
					$get_data = $this->General_model->view_data('default_key_selected',array('user_id' => $check_userid));
					$r = (!empty($get_data)) ? $this->General_model->update_data('default_key_selected',array('key_id' => $choosed_keyid),array('user_id' => $check_userid)) : $this->General_model->insert_data('default_key_selected',array('key_id' => $choosed_keyid,'user_id' => $check_userid));
				}
			}
		}
		($r) ? $this->session->set_userdata('message_succ', 'Successfully Added') : $this->session->set_userdata('message_err', 'Failure to Add');
		redirect(base_url().'admin/events/');
	}
	// public function add_default_key_1() {
	// 	if (!empty($_POST['choose_default_eve_key'])) {
	// 		// $choose_default_eve_key = $_POST['choose_default_eve_key'];
	// 		$choose_default_eve_key = ($_GET['key'] = 'proplayers' && is_array($_POST['choose_default_eve_key'])) ? $_POST['choose_default_eve_key'] : array('0_'.$_POST['choose_default_eve_key']);
			
	// 		if (!empty($choose_default_eve_key)) { 
	// 			$keys = $this->Tournaments_model->get_key_list(array());
	// 			foreach ($choose_default_eve_key as $key => $cdek) {
	// 				if (!empty($cdek)) {
	// 					$cdekarr = explode('_', $cdek);
	// 					$check_userid = trim($cdekarr[0]);
	// 					$choosed_keyid = trim($cdekarr[1]);
	// 					$id_arr =  array($check_userid);
						
	// 					foreach ($keys as $val) {
	// 						if ($val->default_assigned_to != null && $val->default_assigned_to != '') {
	// 							$assigned_to_array = json_decode($val->default_assigned_to);
	// 							if (in_array($check_userid, $assigned_to_array) && $choosed_keyid != $val->id) {
	// 								$arr = array_diff($assigned_to_array, array($check_userid));
	// 								if (empty($arr)) {
	// 									$this->General_model->update_data('event_keys',array('default_assigned_to' => NULL),array('id' => $val->id));
	// 								} else {
	// 									$this->General_model->update_data('event_keys',array('default_assigned_to' => json_encode($arr)),array('id' => $val->id));
	// 								}
	// 							}
	// 						}
	// 					}
	// 					$add_key = $this->Tournaments_model->get_key_list(array('id' => $choosed_keyid));
	// 					echo "<pre>";
	// 					print_r($add_key);
	// 					$idarr = json_decode($add_key[0]->default_assigned_to);
	// 					if (!empty($idarr)) {
	// 					 array_push($idarr,$check_userid);
	// 					} else {
	// 						$idarr = array($check_userid);
	// 					} 
	// 					print_r($idarr);
	// 					$this->General_model->update_data('event_keys',array('default_assigned_to' => json_encode($idarr)), array('id' => $choosed_keyid));						
	// 					print_r($this->db->last_query());
	// 					echo "</pre>";
	// 				}
	// 			}
	// 		}
	// 		// print_r($_GET['key']);
	// 		// print_r($_POST);
	// 		// print_r($choose_default_eve_key);
	// 		exit();
	// 		// ($this->General_model->insert_data('event_keys',$arr)) ? $this->session->set_userdata('message_succ', 'Successfully Added') : $this->session->set_userdata('message_err', 'Failure to Add');
	// 		redirect(base_url().'admin/events/');
	// 	}
	// }
}