<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class FrontUser extends My_Controller  {
  function __construct() {
		parent::__construct();
		$this->target_dir=BASEPATH.'../upload/usr/';
		$this->load->library(array('form_validation','session'));
	}

	public function index() {
		$this->check_user_page_access();
		// add breadcrumbs
		$this->breadcrumbs->push('Home', 'admin/dashboard');
		$this->breadcrumbs->push('User List', 'admin/FrontUser');
		$data['active'] = 'usr_list';
		$data['page_name'] = 'User List';
		$data['list'] = $this->User_model->user_view_admin();

		$getm_plans = $this->General_model->view_data('player_membership_subscriptions',array('payment_status' => 'Active'));
		$data['active_mplans'] = array_column($getm_plans, 'user_id');
		$content = $this->load->view('admin/usr/usr_list.tpl.php',$data,true);
		$this->render($content);
	}

	function getLists(){
        $data = $row = array();
        
        $userData = $this->User_model->getRows($_POST);
        
        $i = $_POST['start'];
        foreach($userData as $user){
        	$i++;
			$img = ($user->is_admin == 1) ? base_url().'assets/frontend/images/pro_player.png' : '';
			$image = ($img != '') ? '<img width="15" height="15" src="'.$img.'">' : '';
			$account_no = $user->account_no.'<a class="account_no" id="'.$user->id.'"><i class="fa fa-pencil fa-fw"></i></a>'.$image;
			$display_name = $user->display_name.'<a class="change_display_name" id="'.$user->id.'" display_name="'.$user->display_name.'" style="margin-left: 4px;"><i class="fa fa-pencil fa-fw"></i></a>';
			$team_name = $user->team_name.'<a class="change_team_name" id="'.$user->id.'" team_name="'.$user->team_name.'" style="margin-left: 4px;"><i class="fa fa-pencil fa-fw"></i></a>';
			$is_18 = ($user->is_18 == 1) ? '18+' :'Under 18';
			$membership = (in_array($user->id, $active_mplans)) ? '<img src="'.base_url().'assets/frontend/images/green.png" width="10"/>&nbsp;' : '<img src="'.base_url().'assets/frontend/images/red.png" width="10"/>&nbsp&nbsp';
			$tax = ($user->taxid != '') ? '<img src="'.base_url().'assets/frontend/images/green.png" width="10"/>&nbsp' : '<img src="'.base_url().'assets/frontend/images/red.png" width="10"/>&nbsp&nbsp';
			$total_balance = number_format($user->total_balance, 2, '.', '').'<a href="#" class="view" id="'.$user->id.'"><i class="fa fa-pencil fa-fw"></i></a>';
			$active = ($user->is_active == 0 && $user->ban_option == 0) ? 'Active' : 'Banned';
			$lastlogged = date('m-d-Y H:i a',strtotime($user->last_login));
			$action = '<a href="'.base_url().'admin/FrontUser/userEdit/'.$user->id.'"><i class="fa fa-pencil fa-fw"></i></a><a href="javascript:void(0)" class="delete" id="del_'.$user->id.'"><i class="fa fa-trash-o fa-fw"></i></a><a class="edit-data" action="exp_plyr_history" id="'.$user->user_id.'" winre_loc="'.base_url().'admin/FrontUser/exportGameHistory/"><i class="fa fa-print"></i>';
            $data[] = array($account_no,$user->name,$display_name,$team_name,$is_18,$user->email,$user->discord_id,$user->number,$membership,$user->city,$user->statename,$user->countryname,$tax,$user->won_amt,$user->lost_amt,$user->withdrawal_amt,$total_balance,$active,$lastlogged,$action);

        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->User_model->countAll(),
            "recordsFiltered" => $this->User_model->countFiltered($_POST),
            "data" => $data,
        );
        
        // Output to JSON format
        echo json_encode($output);
    }

	public function security() {
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
		$this->breadcrumbs->push('User Security', 'admin/FrontUser/security');
		$data['active']='usr_security';
		$data['page_name']='Security List';
		$this->form_validation->set_rules('password', 'Enter the Security Password', 'required');
		
		if ($this->form_validation->run() == true) {
			$password 	= $this->input->post('password', TRUE);
			$id		 	= $this->input->post('id', TRUE);
			
			$data_update = array(
				'password'	=> $password,
			);
			$data_condition = array(
				'id'	=> $id,
			);
			$this->General_model->update_data('admin_security_password',$data_update,$data_condition);
			$this->session->set_userdata('message_succ', 'Password Successfully Updated');
			header('location:'.base_url().'admin/FrontUser/security/','refresh');
		}
		
		$data['security_password'] = $this->General_model->view_all_data('admin_security_password','id','asc');
		
		$content=$this->load->view('admin/usr/usr_security.tpl.php',$data,true);
		$this->render($content);
	}


	
	//load edit page
	public function userEdit($id='-1') {
		$this->check_user_page_access();
		// add breadcrumbs
		$this->breadcrumbs->push('Home', 'admin/dashboard');
		$this->breadcrumbs->push('User List', 'admin/FrontUser');
		$this->breadcrumbs->push('Edit User', 'admin/FrontUser/userEdit');
		$data['country_list'] = $this->User_model->country_list();
		$data['active']='usr_edit';
		$data['page_name']='User Edit';
		$data['details']=$this->General_model->view_data('user',array('id'=>$id));
		$data['usr'] = $this->General_model->view_data('user_detail',array('user_id'=>$id));
		$data['state'] = $this->General_model->view_data('state',array('country_id' => $data['usr'][0]['country']));
		$content=$this->load->view('admin/usr/usr_edit.tpl.php',$data,true);
		$this->render($content);
	}
	
	//edit 
	public function userUpdate() {
		$this->check_user_page_access();
		$data['active'] = 'usr_edit';
		$user_data	= $this->General_model->view_data('user',array('id'=>$this->input->post('id', TRUE)));
		$userdetails_data	= $this->General_model->view_data('user_detail',array('user_id'=>$user_data[0]['id']))[0];
		$user_det	= (!empty($user_data)) ? $user_data : array('email'=> '','email'=> '','mobile'=>'','staff_name'=> '');
		$is_unique = ($this->input->post('email') != $user_det[0]['email']) ? '|is_unique[user.email]' : '';
		
		$this->form_validation->set_rules('email','Email','required|trim'.$is_unique);
		if ($this->form_validation->run() == true){
			$common_settings = json_decode($userdetails_data['common_settings'], true);
			$common_settings['free_membership'] = $_POST['free_membership'] == 'on' ? 'on' : 'off';

			$is_admin = ($_REQUEST['is_admin'] == 'on') ? 1 : 0;
			$data = array(
				'name'=>$this->input->post('name', TRUE),
				'number'=>$this->input->post('number', TRUE),		
				'location'=>$this->input->post('location', TRUE),
				'taxid'=>$this->input->post('taxid', TRUE),
				'country'=>$this->input->post('country', TRUE),
				'state'=>$this->input->post('state', TRUE),
				'city'=>$this->input->post('city', TRUE),
				'zip_code'=>$this->input->post('zip_code', TRUE),
				'timezone' => $this->input->post('timezone', TRUE),
				'dob'=>$this->input->post('dob', TRUE),
				'is_admin'=>$is_admin,
				'common_settings' => json_encode($common_settings),
			);
			if($this->session->userdata('admin_user_id') == 500000){ 
				$data['taxid'] = $this->input->post('taxid', TRUE);
			}
			if($this->input->post('is_active') == 1){
				$ban_option = $this->input->post('ban_option', TRUE);
			} else {
				$ban_option = 0;
			}
			$is_active = $this->input->post('is_active', TRUE);
			if ($_POST['ban_option'] == 1) {
				$is_active = 0;
				$ban_time = '0000-00-00 00:00:00';
			} else if ($_POST['ban_option'] > 1 && $_POST['ban_option'] <= 336) {
				$ban_time = date("Y-m-d H:i:s", strtotime('+'.$_POST['ban_option'].' hour'));
			}
			$user_data = array(
				'email'	=>	$this->input->post('email', TRUE),
				'is_active'	=>	$is_active,
				'ban_option' =>	$ban_option,
				'ban_time' => $ban_time,
			);
			if ($user_data['ban_option'] == '1') {
				$get_subscribed_data = $this->General_model->subscribedlist($this->input->post('id', TRUE));
				foreach ($get_subscribed_data as $key => $gsd) {
					$payment_id = $gsd->payment_id;
					$PayPalResult = $this->General_model->get_recurring_payments_profile_details($payment_id);
					$subscription_opt = 'Cancel';
          			if ($PayPalResult['STATUS'] != 'Cancelled') {
              			$this->General_model->Manage_recurring_payments_profile_status($payment_id,$subscription_opt);
          			}
		          	$del_msg = $this->General_model->delete_data('paypal_subscriptions', array(
		              'payment_id' => $payment_id
		          	));
				}
				$get_subscribedby_data = $this->General_model->subscribedbylist($this->input->post('id', TRUE));
				foreach ($get_subscribedby_data as $key => $gsbd) {
					$payment_id = $gsbd->payment_id;
					$PayPalResult = $this->General_model->get_recurring_payments_profile_details($payment_id);
					$subscription_opt = 'Cancel';
			        if ($PayPalResult['STATUS'] != 'Cancelled') {
			            $this->General_model->Manage_recurring_payments_profile_status($payment_id,$subscription_opt);
			        }
		          	$del_msg = $this->General_model->delete_data('paypal_subscriptions', array(
		            	'payment_id' => $payment_id
		          	));
				}
			}
			$this->General_model->update_data('user',$user_data,array('id'=>$this->input->post('id', TRUE)));
			$r = $this->General_model->update_data('user_detail',$data,array('user_id'=>$this->input->post('id', TRUE)));
			($r) ? $this->session->set_flashdata('message_succ', 'Update successfull') :  $this->session->set_flashdata('message_err', 'Failure to update');
		} else {
			$this->session->set_flashdata('message_err', 'This Email ID Already Assigned or not Acceptable');	
		}
		redirect(base_url().'admin/FrontUser/userEdit/'.$this->input->post('id', TRUE));
		
	}
	//delete
	public function delete($id=NULL) {
		if($_SESSION['admin_user_role_id'] == '1'){ 
			$this->check_user_page_access();
			$security_password 	= $_REQUEST['security_password'];
			$userid 			= $_REQUEST['userid'];
			
			$security_password_db = $this->General_model->view_all_data('admin_security_password','id','asc');
			
			
			if($security_password == $security_password_db[0]['password']){
				
				$r=$this->General_model->delete_data('user',array('id'=>$userid));
				$r.=$this->General_model->delete_data('user_detail',array('user_id'=>$userid));
				$r.=$this->General_model->delete_data('mail_categories',array('user_id'=>$userid));
				
				$get_subscribed_data = $this->General_model->subscribedlist($userid);
				foreach ($get_subscribed_data as $key => $gsd) {
					$payment_id = $gsd->payment_id;
					$PayPalResult = $this->General_model->get_recurring_payments_profile_details($payment_id);
					$subscription_opt = 'Cancel';
          if ($PayPalResult['STATUS'] != 'Cancelled') {
              $this->General_model->Manage_recurring_payments_profile_status($payment_id,$subscription_opt);
          }
          $del_msg = $this->General_model->delete_data('paypal_subscriptions', array(
              'payment_id' => $payment_id
          ));
				}
				$get_subscribedby_data = $this->General_model->subscribedbylist($userid);
				foreach ($get_subscribedby_data as $key => $gsbd) {
					$payment_id = $gsbd->payment_id;
					$PayPalResult = $this->General_model->get_recurring_payments_profile_details($payment_id);
					$subscription_opt = 'Cancel';
          if ($PayPalResult['STATUS'] != 'Cancelled') {
              $this->General_model->Manage_recurring_payments_profile_status($payment_id,$subscription_opt);
          }
          $del_msg = $this->General_model->delete_data('paypal_subscriptions', array(
              'payment_id' => $payment_id
          ));
				}
				
				if($r) {
					$this->session->set_userdata('message_succ', 'Delete Successfull');	
					$data_value = true;
				} else {
					$this->session->set_userdata('message_err', 'Failure to delete');	
					$data_value = false;
				}
				
			} else {
				$data_value['pass_err'] = 'Password Error';
				$data_value = '<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<form action="#" id="security_id" name="security_id" method="POST" >
						<div class="form-group" style="margin: 0 12px 15px;"><label style="color:red;">'.$data_value['pass_err'].'</label></div>
							<div class="box-body">
								<div class="form-group">
									<label for="name">Admin Password</label>
									<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
									<input type="hidden" id="userid" class="form-control" name="userid" value="'.$userid.'" required>
								</div>
							</div>
							<div class="box-footer">
								<input class="btn btn-primary" type="submit" value="Submit" >
							</div>
						</form>
					</div>
				</div>			
				<div class="modal-footer">
					<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
				</div>';
				
			}
			echo $data_value;
		}
	}
	
	function get_cron_details(){	
		$user_list = $this->User_model->user_view_admin();
		if(!empty($user_list)){
			foreach($user_list as $user_li){
				$won_sum[]	= '';
				$lost_sum[]	= '';
				$withdrawal_sum[]	= '';
				
				$win_amount = '';
				$lost_amount = '';
				$with_amount = '';
				
				$won_array = $this->User_model->get_all_won_amt($user_li['id']);
				
				if(!empty($won_array)){
					foreach($won_array as $won_arr){
						$won_sum[]	= $won_arr['price'];
						
						$update_data = array(
							'is_win_cron'	=> '1',
						);
						
						$update_cond = array(
							'id'	=> $won_arr['id'],
						);
						
						$this->General_model->update_data('bet',$update_data,$update_cond);
						
					}
					$win_amount = array_sum($won_sum);
					unset($won_sum);
				}
				
				$lost_array = $this->User_model->get_all_lost_amt($user_li['id']);
				
				if(!empty($lost_array)){
					foreach($lost_array as $lost_arr){
						$lost_sum[]	= $lost_arr['price'];
						
						$update_data = array(
							'is_lose_cron'	=> '1',
						);
						
						$update_cond = array(
							'id'	=> $lost_arr['id'],
						);
						
						$this->General_model->update_data('bet',$update_data,$update_cond);
						
					}
					$lost_amount =  array_sum($lost_sum);
					unset($lost_sum);
				}
				
				$withdrawal_array = $this->User_model->get_all_withdrawal_amt($user_li['id']);
				if(!empty($withdrawal_array)){
					foreach($withdrawal_array as $withdrawal_arr){
						$withdrawal_sum[]	= $withdrawal_arr['amount_paid'];
						
						$withdraw_data = array(
							'is_with_cron'	=> '1',
						);
						
						$withdraw_cond = array(
							'id'	=> $withdrawal_arr['id'],
						);
						
						$this->General_model->update_data('withdraw_request',$withdraw_data,$withdraw_cond);
						
						
					}
					$with_amount = array_sum($withdrawal_sum);
					unset($withdrawal_sum);
				}
				
				
				$get_previous_deta = $this->General_model->view_single_row('user_detail','user_id',$user_li['id']);
					
					
					$final_won	 = $win_amount + $get_previous_deta['won_amt'];
					$final_loat	 = $lost_amount + $get_previous_deta['lost_amt'];
					
				$data =array(
					'won_amt' 			=> $final_won,
					'lost_amt' 			=> $final_loat,
					'withdrawal_amt' 	=> $with_amount,
				);
				
				$data_where = array(
					'user_id'	=> $user_li['id'],
				);
				
				$this->General_model->update_data('user_detail',$data,$data_where);

			}
			redirect(base_url().'admin/FrontUser');
			//print_r($data); die;
		}
	
	}
	
	function security_data($id){
		echo '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="security_update_bal_id" name="security_id" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Admin Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
								<input type="hidden" id="userid" class="form-control" name="userid" value="'.$id.'" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
	}
	function account_security_data($id){
		echo '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="account_security_id" name="account_security_id" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Admin Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
								<input type="hidden" id="userid" class="form-control" name="userid" value="'.$id.'" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
							<button type="button" class="btn btn-info pull-right" data-dismiss="modal">Close</button>		
						</div>
					</form>
				</div>
			</div>';
	}
	function security_pass($id){
		echo '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="security_pass" name="security_pass" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Admin Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required >
								<input type="hidden" id="action_value" class="form-control" name="action_value" value="'.$id.'" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
	}

	function chat_security_pass($id){

		echo '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;"> Password Required</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="'.base_url().'admin/user/agentlogin" id="chat_security_pass" name="chat_security_pass" method="POST">
						<div class="box-body">
							<div class="form-group">
								<label for="name">Agent Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required >
								<input type="hidden" id="action_value" class="form-control" name="action_value" value="'.$id.'" required>
							</div>
						</div>
						<div class="box-footer">
							<input type="submit" class="btn btn-primary" value="Submit">
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
	}
	
	function get_reset_data(){
		
		$security_password 		= $_REQUEST['security_password'];
		$action_value 			= $_REQUEST['action_value'];
		
		$security_password_db = $this->General_model->view_all_data('admin_security_password','id','asc');
		if($security_password == $security_password_db[0]['password']){
			$data_value = 'success';			
		} else{
			$data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="security_pass" name="security_pass" method="POST" >
						<div class="box-body">
							<div class="form-group">

								<label for="name">Admin Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
								<input type="hidden" id="action_value" class="form-control" name="action_value" value="'.$action_value.'" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';

		}	
		echo $data_value;
	}
	
	
	
	function delete_data($id){
		
		$result = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="security_password_id" name="security_password_id" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Admin Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
								<input type="hidden" id="userid" class="form-control" name="userid" value="'.$id.'" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
			echo $result;
	}
	
	function get_reset_all(){
		$user_list = $this->User_model->user_view_admin();
		if(!empty($user_list)){
			foreach($user_list as $user_li){		
				$update_data = array(
					'won_amt'			=> '0',
					'lost_amt'			=> '0',
					'withdrawal_amt'	=> '0',
				);				
				$update_cond = array(
					'user_id'	=> $user_li['id'],
				);				
				$this->General_model->update_data('user_detail',$update_data,$update_cond);
			}
		}
		redirect(base_url().'admin/FrontUser');
	}
	
	function get_reset_won(){		
		$user_list = $this->User_model->user_view_admin();
		if(!empty($user_list)){
			foreach($user_list as $user_li){		
				$update_data = array(
					'won_amt'	=> '0',
				);
				
				$update_cond = array(
					'user_id'	=> $user_li['id'],
				);
				
				$this->General_model->update_data('user_detail',$update_data,$update_cond);
				
			}
		}
		redirect(base_url().'admin/FrontUser');
	}
	function update_display_name() {
		if (!empty($_POST['user_id']) && !empty(trim($_POST['display_name']))) {
			$update_data['display_name'] = trim($_POST['display_name']);
			$update_cond['user_id'] = $_POST['user_id'];
			$r = $this->General_model->update_data('user_detail', $update_data ,$update_cond);
		}
		($r) ? $this->session->set_userdata('message_succ', 'Display name Updated'):$this->session->set_userdata('message_err', 'Failure to update');
		redirect($_SERVER['HTTP_REFERER']);
	}
	//update team name
	function update_team_name() {

		if (!empty($_POST['user_id']) && !empty(trim($_POST['team_name']))) {
			$update_data['team_name'] = trim($_POST['team_name']);
			$update_cond['user_id'] = $_POST['user_id'];
			$r = $this->General_model->update_data('user_detail', $update_data ,$update_cond);
		}
		($r) ? $this->session->set_userdata('message_succ', 'Team name Updated'):$this->session->set_userdata('message_err', 'Failure to update');
		redirect($_SERVER['HTTP_REFERER']);
	}
	function get_reset_lost(){		
		$user_list = $this->User_model->user_view_admin();
		if(!empty($user_list)){
			foreach($user_list as $user_li){		
				$update_data = array(
					'lost_amt'	=> '0',
				);
				
				$update_cond = array(
					'user_id'	=> $user_li['id'],
				);
				
				$this->General_model->update_data('user_detail',$update_data,$update_cond);
				
			}
		}
		redirect(base_url().'admin/FrontUser');
	}
	
	function get_reset_with(){		
		$user_list = $this->User_model->user_view_admin();
		if(!empty($user_list)){
			foreach($user_list as $user_li){		
				$update_data = array(
					'withdrawal_amt'	=> '0',
				);
				
				$update_cond = array(
					'user_id'	=> $user_li['id'],
				);
				
				$this->General_model->update_data('user_detail',$update_data,$update_cond);
				
			}
		}
		redirect(base_url().'admin/FrontUser');
	}
	
	function ChangeBalance(){
		$security_password 	= $_REQUEST['security_password'];
		$userid 			= $_REQUEST['userid'];
		$security_password_db = $this->General_model->view_all_data('admin_security_password','id','asc');
		if ($security_password == $security_password_db[0]['password']) {
			$user_data	= $this->General_model->view_data('user_detail',array('user_id'=>$userid));
			$data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Current Balance :  '.$user_data[0]['total_balance'].'</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="changebalance_id" name="changebalance_id" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Upgrade Balance</label>
								<input type="hidden" id="userid" class="form-control" name="userid" value="'.$userid.'" required>
								<input type="number" step="any" min="0" id="balance" class="form-control" name="balance" value="" required onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46">
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
		} else {
			$data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="security_update_bal_id" name="security_id" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Admin Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
								<input type="hidden" id="userid" class="form-control" name="userid" value="'.$userid.'" required>
							</div>
							<div class="form-group"> <label class="alert alert-danger"> Incorrect Password </label> </div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
		}	
		echo $data_value;
	}
	function ChangeAccountNo(){
		$security_password 	= $_REQUEST['security_password'];
		$userid 			= $_REQUEST['userid'];
		$security_password_db = $this->General_model->view_all_data('admin_security_password','id','asc');
		if($security_password == $security_password_db[0]['password']){
			
			$user_data	= $this->General_model->view_data('user_detail',array('user_id'=>$userid));
			
			$data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Current Account No -  '.$user_data[0]['account_no'].'</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="change_account_no" name="change_account_no" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Upgrade Account No</label>
								<input type="hidden" id="userid" class="form-control" name="userid" value="'.$userid.'" required>
								<input type="text" id="account_no" class="form-control" name="account_no" value="" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
							<button type="button" class="btn btn-info pull-right" data-dismiss="modal">Close</button>		
						</div>
					</form>
				</div>
			</div>';
		} else{
			$data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="account_security_id" name="account_security_id" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Admin Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
								<input type="hidden" id="userid" class="form-control" name="userid" value="'.$userid.'" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
							<button type="button" class="btn btn-info pull-right" data-dismiss="modal">Close</button>		
						</div>
					</form>
				</div>
			</div>';
		}	
		echo $data_value;
	}
	function Change_password(){
		if (!empty($_REQUEST['new_password'])) {
			$new_password = $_REQUEST['new_password'];
			$userid 		= $_REQUEST['userid'];
			$data = array(
				'password'=>md5($new_password),
			);
			$r=$this->General_model->update_data('user',$data,array('id'=>$userid));
			if ($r) {
				$this->session->set_userdata('message_succ', 'User Password Updated Successfully');	
				echo True;
			}
		}
	}
	function UpdateBalance(){
		$userid = $_REQUEST['userid'];
		$balance = number_format((float) $_REQUEST['balance'], 2, '.', '');
		$data['total_balance'] = $balance;
    $get_arr['user_id'] = $userid;
    $get_user_data = $this->General_model->view_data('user_detail', $get_arr)[0];

  	$affected_bal = $get_user_data['total_balance'] - $balance;
  	$affect = '0';
    if ($balance > $get_user_data['total_balance']) {
    	$affected_bal = $balance - $get_user_data['total_balance'];
    	$affect = '1';
    }
		$r = $this->General_model->update_data('user_detail',$data,array('user_id'=>$userid));
		if ($r) {
			$this->session->set_userdata('message_succ', 'Balance Updated Successfully');
			$add_data = array(
	      'affect' => $affect,
	      'user_id' => $userid,
	      'amount' => number_format((float) $affected_bal, 2, '.', ''),
	      'created_at' => date('Y-m-d h:i:s')
	    );
	    $this->General_model->insert_data('balance_update',$add_data);
		} else {
			$this->session->set_userdata('message_err', 'Failure to update');
		}
		redirect(base_url().'admin/FrontUser');
	}
	function UpdateAccountNo() {
		$userid 	= $_REQUEST['userid'];
		$account_no 	= $_REQUEST['account_no'];
		$data=array(
			'account_no'=>$account_no,
		);
		$user_data	= $this->General_model->view_data('user_detail',array('account_no'=>$account_no));
		
		if(empty($user_data)) {
			
			$r=$this->General_model->update_data('user_detail',$data,array('user_id'=>$userid));
			
				$this->session->set_userdata('message_succ','Account No Updated Successfully');	
			
		} else {
			$data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Current Account No -  '.$user_data[0]['account_no'].'</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="change_account_no" name="change_account_no" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Upgrade Account No</label>
								<input type="hidden" id="userid" class="form-control" name="userid" value="'.$userid.'" required>
								<input type="text" id="account_no" class="form-control" name="account_no" value="" required>
								<p style="color:red;">You have Entered Account No is already Exists. Please Enter Unique Account No</p>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
							<button type="button" class="btn btn-info pull-right" data-dismiss="modal">Close</button>		
						</div>
					</form>
				</div>
			</div>';
			echo $data_value;
		}
		
	}
	
	function userExcel(){
		
		$all_front_user = $this->User_model->user_view();
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Frontend User');
		
		$header_section_row = 1; $header_section_col = 1;
		$header_section	=	'Frontend User';
		$row = 1;
		$col = 0;
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'Name');		
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'Email');		
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'Phone Number');		
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'Location');		
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'State');
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, 'Country');
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 1, 'Zip Code');
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, 1, 'Tax ID');		
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, 1, 'Won Amt');		
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, 1, 'Lost Amt');		
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(10, 1, 'Withdrawal Amt');		
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(11, 1, 'Balance');		
		
		$style_p = array('font' => array('size' => 12,'bold' => true));
		$style_a = array('font' => array('size' => 12,'bold' => true,'color' => array('rgb' => 'FFFFFF')));
		if(!empty($all_front_user)) {
			$data_row = 2;
			foreach($all_front_user as $all_front) {
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $data_row, ucwords($all_front['name']));
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $data_row, $all_front['email']);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $data_row, $all_front['number']);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $data_row, ucwords($all_front['location']));

				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, $data_row, ucwords($all_front['state']));

				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $data_row, ucwords($all_front['country']));
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $data_row, ucwords($all_front['zip_code']));

				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $data_row, ucwords($all_front['taxid']));
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $data_row, $all_front['won_amt']);				
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, $data_row, $all_front['lost_amt']);				
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(10, $data_row, $all_front['withdrawal_amt']);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(11, $data_row, $all_front['total_balance']);
				$data_row++;
			}
		}	
		
		$filename = 'frontenduser-'.date('Y-m-d',time()).'.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
				
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
	function export_history(){
		$user_id = $_GET['user_id'];
		$all_front_user = $this->User_model->user_game_history($user_id);
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0)->mergeCells('A1:C1');
		$this->excel->setActiveSheetIndex(0)->mergeCells('A2:C2');
		$this->excel->setActiveSheetIndex(0)->mergeCells('D1:E1');
		$this->excel->setActiveSheetIndex(0)->mergeCells('F1:G1');
		$this->excel->setActiveSheetIndex(0)->mergeCells('D2:E2');
		$this->excel->setActiveSheetIndex(0)->mergeCells('F2:G2');
		// $this->excel->getActiveSheet()->getCell('A1')->setValue('This is the text that I want to see in the merged cells');
		//name the worksheet

		$this->excel->getActiveSheet()->setTitle('Frontend User');
		$this->excel->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle("A2:G2")->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle("A3:G3")->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('A1:G1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$this->excel->getActiveSheet()->getStyle('A1:G1')->getFill()->getStartColor()->setARGB('#ff5000');
		$this->excel->getActiveSheet()->getStyle('A2:G2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$this->excel->getActiveSheet()->getStyle('A2:G2')->getFill()->getStartColor()->setARGB('#ff5000');
		
		$header_section_row = 1; $header_section_col = 1;
		$header_section	=	'Frontend User';
		$row = 1;
		$col = 0;

		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'Name : '.$all_front_user[0]['name']);

		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, '');
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 3, 'ID');	
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, 'CONSOLE');		
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 3, 'GAME TITLE');		
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 3, 'Sub Game');		
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, 3, 'Size');		
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 3, 'Won/Loss');
		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 3, 'Amount');	
		
		$style_p = array('font' => array('size' => 12,'bold' => true));
		$style_a = array('font' => array('size' => 12,'bold' => true,'color' => array('rgb' => 'FFFFFF')));
		if(!empty($all_front_user)) {
			$data_row = 4;
			$game_win_number = 1;
			$game_loss_number = 1;
			$total_win = 0;
			$total_loss = 0;
			foreach($all_front_user as $all_front) {
				if ($all_front['winner_id'] == $user_id) {
					$game_status = "WON";
					$game_status_count_win = $game_win_number++;
					$total_win += $all_front['price'];
				}
				if ($all_front['loser_id'] == $user_id) {
					$game_status = "LOSS";
					$game_status_count_los = $game_loss_number++;
					$total_loss += $all_front['price'];
				}
				$this->excel->getActiveSheet()->getColumnDimension($data_row)->setAutoSize(True);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $data_row, $all_front['id']);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $data_row, ucwords($all_front['device_id']));
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $data_row, $all_front['game_name']);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $data_row, $all_front['sub_game_name']);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, $data_row, $all_front['game_type'].'v'.$all_front['game_type']);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $data_row, $game_status);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $data_row, ucwords('$'.$all_front['price']));
				$data_row++;
			}
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, $game_status_count_win.' Wins');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, $game_status_count_los.' Losses');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, '$'.$total_win);
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, '$'.$total_loss);
		}
		$filename = 'frontenduser-'.date('Y-m-d',time()).'.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
				
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
	function exportGameHistory($user_id){
		$arr['user_id'] = $user_id;
		if (!empty($_POST['alldata'])) {
			$arr['alldata'] = $_POST['alldata'];
		} else {
			if (!empty($_POST['min_date']) && !empty($_POST['max_date'])) {
				$start_date = date('Y-m-d 00:00:00', strtotime($_POST['min_date']));
				$end_date = date('Y-m-d 23:59:59', strtotime($_POST['max_date']));
			} else {
				if (!empty($_POST['year'])) {
					if (!empty($_POST['month'])) {
						$start_date = date($_POST['year'].'-'.$_POST['month'].'-01 00:00:00');
						$end_date = date($_POST['year'].'-'.$_POST['month'].'-t 23:59:59');	
					} else {
						$start_date = date($_POST['year'].'-01-01 00:00:00');
						$end_date = date($_POST['year'].'-12-t 23:59:59');
					}
				} else {
					$start_date = date('Y-'.$_POST['month'].'-01 00:00:00');
					$end_date = date('Y-'.$_POST['month'].'-t 23:59:59');
				}
			}
		}
		if (!empty($start_date) && !empty($end_date)) {
			$arr['start_date'] = $start_date;
			$arr['end_date'] = $end_date;
		}
		$all_front_user = $this->Payment_history_model->user_history($arr);
		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0)->mergeCells('A1:E1')->mergeCells('A2:E2');
		if (!empty($all_front_user) && !empty($all_front_user['wallet_arr'])) {
			$heading_fontstyle = array(
				'font' => array('size' => 17,'bold' => true),
				'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('argb' => 'FFFFFF'))
			);
			$title_fontstyle = array(
				'font' => array('size' => 18,'bold' => true)
			);

			if (!empty($all_front_user['wallet_arr'])) {
				$active_sheet = $this->excel->getActiveSheet();
				foreach(range('A','G') as $columnID) {
					$active_sheet->getColumnDimension($columnID)->setAutoSize(true);
		  		}

				$active_sheet->setTitle('Frontend user Wallet history');
				$active_sheet->getStyle("A1:G1")->applyFromArray($heading_fontstyle);
				$active_sheet->getStyle("A2:G2")->applyFromArray($heading_fontstyle);
				$active_sheet->getStyle("A3:G3")->applyFromArray($title_fontstyle);

				$active_sheet->setCellValueByColumnAndRow(0, 2, '');
				$active_sheet->setCellValueByColumnAndRow(0, 3, 'IMG');	
				$active_sheet->setCellValueByColumnAndRow(1, 3, 'SR. NO.');	
				$active_sheet->setCellValueByColumnAndRow(2, 3, 'TRANSACTIONAL TITLE');
				$active_sheet->setCellValueByColumnAndRow(3, 3, 'MODULE');
				$active_sheet->setCellValueByColumnAndRow(4, 3, 'DATETIME');		
				$active_sheet->setCellValueByColumnAndRow(5, 3, 'CREDIT');		
				$active_sheet->setCellValueByColumnAndRow(6, 3, 'DEBIT');

				$data_row = 4; $count = 1;

				usort($all_front_user['wallet_arr'], function($a, $b) { return strtotime($b['created_at']) - strtotime($a['created_at']);});
				$total_balance = 0; $total_credit = 0; $total_debit = 0;
				foreach($all_front_user['wallet_arr'] as $key => $all_front) {
					$img = BASEPATH.'../assets/frontend/images/'.$all_front['module'].'.jpg';
					if (file_exists($img)) {
						$objDrawing = new PHPExcel_Worksheet_Drawing();
						$objDrawing->setName($all_front['module'].' Image');
						$objDrawing->setDescription($all_front['module'].' Image');
						
						$objDrawing->setPath($img);
						// $objDrawing->setOffsetX(20);  // setOffsetX works properly
						// $objDrawing->setOffsetY(20);  //setOffsetY has no effect
						$objDrawing->setCoordinates('A'.$data_row);
						$objDrawing->setResizeProportional(true);
						$objDrawing->setHeight(30); // logo height
						// $objDrawing->setWidth(20); // logo width
						$objDrawing->setWorksheet($this->excel->getActiveSheet());
					} else {
						$active_sheet->setCellValueByColumnAndRow(0, $data_row, 'Image not found');
					}

					$credit = $debit = '';
					if ($all_front['transaction_type'] == 'credit') {
						$credit = $all_front['amount'];
						$total_balance += $all_front['amount'];
						$total_credit += $all_front['amount'];
					} else {
						$debit = $all_front['amount'];
						$total_balance -= $all_front['amount'];
						$total_debit += $all_front['amount'];
					}

					$active_sheet->getColumnDimension($data_row)->setWidth(100);
					$active_sheet->getRowDimension($data_row)->setRowHeight(20);
					$active_sheet->setCellValueByColumnAndRow(1, $data_row, $count++);
					$active_sheet->setCellValueByColumnAndRow(2, $data_row, ucwords($all_front['transaction_title']));
					$active_sheet->setCellValueByColumnAndRow(3, $data_row, strtoupper($all_front['module']));
					$active_sheet->setCellValueByColumnAndRow(4, $data_row, $all_front['created_at']);

					$active_sheet->setCellValueByColumnAndRow(5, $data_row, number_format((float)$credit, 2, '.', ''));
					$active_sheet->setCellValueByColumnAndRow(6, $data_row, number_format((float)$debit, 2, '.', '')); 
					$data_row++;
				}
				$active_sheet->setCellValueByColumnAndRow(0, 1, 'Total Balance : $'.number_format((float)$total_balance, 2, '.', ''));
				$active_sheet->setCellValueByColumnAndRow(5, 1, 'Credit');
				$active_sheet->setCellValueByColumnAndRow(6, 1, 'Debit');
				$active_sheet->setCellValueByColumnAndRow(5, 2, '$'.$total_credit);
				$active_sheet->setCellValueByColumnAndRow(6, 2, '$'.$total_debit);
			}

			if (!empty($all_front_user['paypal_arr'])) {
				$this->excel->createSheet();
				$this->excel->setActiveSheetIndex(1)->mergeCells('A1:G1')->mergeCells('A2:G2');

				$active_sheet = $this->excel->getActiveSheet();
				foreach(range('A','G') as $columnID) {
					$active_sheet->getColumnDimension($columnID)->setAutoSize(true);
		  	}
		  	$active_sheet->setTitle('Frontend user Paypal history');
				$active_sheet->getStyle("A1:G1")->applyFromArray($heading_fontstyle);
				$active_sheet->getStyle("A2:G2")->applyFromArray($heading_fontstyle);
				$active_sheet->getStyle("A3:G3")->applyFromArray($title_fontstyle);

				$active_sheet->setCellValueByColumnAndRow(0, 2, '');
				$active_sheet->setCellValueByColumnAndRow(0, 3, 'IMG');	
				$active_sheet->setCellValueByColumnAndRow(1, 3, 'SR. NO.');	
				$active_sheet->setCellValueByColumnAndRow(2, 3, 'TRANSACTIONAL TITLE');
				$active_sheet->setCellValueByColumnAndRow(3, 3, 'MODULE');
				$active_sheet->setCellValueByColumnAndRow(4, 3, 'DATETIME');		
				$active_sheet->setCellValueByColumnAndRow(5, 3, 'CREDIT');		
				$active_sheet->setCellValueByColumnAndRow(6, 3, 'DEBIT');
				$data_row = 4; $count = 1;

				usort($all_front_user['paypal_arr'], function($a, $b) { return strtotime($b['created_at']) - strtotime($a['created_at']);});
				$total_balance = 0; $total_credit = 0; $total_debit = 0;
				foreach($all_front_user['paypal_arr'] as $key => $all_front) {
					$img = BASEPATH.'../assets/frontend/images/'.$all_front['module'].'.jpg';
					if (file_exists($img)) {
						$objDrawing = new PHPExcel_Worksheet_Drawing();
						$objDrawing->setName($all_front['module'].' Image');
						$objDrawing->setDescription($all_front['module'].' Image');
						
						$objDrawing->setPath($img);
						// $objDrawing->setOffsetX(20);  // setOffsetX works properly
						// $objDrawing->setOffsetY(20);  //setOffsetY has no effect
						$objDrawing->setCoordinates('A'.$data_row);
						$objDrawing->setResizeProportional(true);
						$objDrawing->setHeight(30); // logo height
						// $objDrawing->setWidth(20); // logo width
						$objDrawing->setWorksheet($this->excel->getActiveSheet());
					} else {
						$active_sheet->setCellValueByColumnAndRow(0, $data_row, 'Image not found');
					}
					$credit = $debit = '';
					if ($all_front['transaction_type'] == 'credit') {
						$credit = $all_front['amount'];
						$total_balance += $all_front['amount'];
						$total_credit += $all_front['amount'];
					} else {
						$debit = $all_front['amount'];
						$total_balance -= $all_front['amount'];
						$total_debit += $all_front['amount'];
					}

					$active_sheet->getColumnDimension($data_row)->setWidth(100);
					$active_sheet->getRowDimension($data_row)->setRowHeight(20);
					$active_sheet->setCellValueByColumnAndRow(1, $data_row, $count++);
					$active_sheet->setCellValueByColumnAndRow(2, $data_row, ucwords($all_front['transaction_title']));
					$active_sheet->setCellValueByColumnAndRow(3, $data_row, strtoupper($all_front['module']));
					$active_sheet->setCellValueByColumnAndRow(4, $data_row, $all_front['created_at']);
					$active_sheet->setCellValueByColumnAndRow(5, $data_row, number_format((float)$credit, 2, '.', ''));
					$active_sheet->setCellValueByColumnAndRow(6, $data_row, number_format((float)$debit, 2, '.', '')); 
					$data_row++;
				}
				$active_sheet->setCellValueByColumnAndRow(0, 1, 'Total Debit : $'.number_format((float)$total_debit, 2, '.', ''));
				$active_sheet->setCellValueByColumnAndRow(0, 2, 'Total Credit : $'.number_format((float)$total_credit, 2, '.', ''));

				// $active_sheet->setCellValueByColumnAndRow(5, 1, 'Credit');
				// $active_sheet->setCellValueByColumnAndRow(6, 1, 'Debit');
				// $active_sheet->setCellValueByColumnAndRow(5, 2, '$'.$total_credit);
				// $active_sheet->setCellValueByColumnAndRow(6, 2, '$'.$total_debit);
			}

			
			$name = ($all_front_user['user_detail']['display_name_status'] == 1 && !empty($all_front_user['user_detail']['display_name'])) ? $all_front_user['user_detail']['display_name'] : $all_front_user['user_detail']['name'];
			$name = str_replace(' ', '-', $name);
			$name = preg_replace('/[^A-Za-z0-9\-]/', '', $name);
			$filename = $name.'_history-'.date('Y-m-d_H-i-s',time()).'.xls'; //save our workbook as this file name
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
					
			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
			//force user to download the Excel file without writing it to server's HD
			$objWriter->save('php://output');
			exit();
		} else {
			$this->session->set_userdata('message_err', 'Users Data empty');
			redirect($_SERVER['HTTP_REFERER']);
			exit();
		}
	}
}
