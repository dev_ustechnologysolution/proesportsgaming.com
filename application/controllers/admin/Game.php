<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Game extends My_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('User_model');
        $this->target_dir=BASEPATH.'../upload/game/';
        $this->target_allimg_dir=BASEPATH.'../upload/all_images/';
        date_default_timezone_set('America/Los_Angeles');
    }
    public function index() {
        $this->check_user_page_access();
        // add breadcrumbs
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Game List', 'admin/game');
        $data['active'] = 'game_list';
        $data['page_name'] = 'Game List';
        //showing game list from function which is created in general_model
        $data['list']=$this->General_model->backend_game_list();
        $content = $this->load->view('admin/game/admin_game_list.tpl.php',$data,true);
        $this->render($content);
    }

    // Game Catagory Add
    public function gameCatAdd() {
        $this->check_user_page_access();
        // add breadcrumbs
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Game Category List', 'admin/game/game_cat_list');
        $this->breadcrumbs->push('Add game category', 'gameCatAdd');
        $data['active'] = 'gameCatAdd';
        $data['page_name'] = 'Add game category';
        //fetch the category for showing into game add form
        $data['cat_name']=$this->General_model->game_system_list();
        $content=$this->load->view('admin/game/game_cat_add.tpl.php',$data,true);
        $this->render($content);
    }
    public function add_game_cat(){
        $this->check_user_page_access();
        if (!empty($_POST['category_name'])) {
            $maxorder = $this->db->select_max('agc.display_order')->from('admin_game_category agc')->get()->row()->display_order+1;
            $insert_data = array(
                "display_order"=> $maxorder,
                "category_name" => $_POST['category_name'],
                "cat_slug" => $_POST['category_name'],
            );
            if (!empty($_FILES['category_img']['name'])) {
                $img = time().basename($_FILES["category_img"]["name"]);
                $ext = end((explode(".", $img)));
                $accept_arr = array('gif','jpeg','png','jpg','GIF','JPEG','PNG','JPG');
                if (in_array($ext, $accept_arr)) {
                    move_uploaded_file($_FILES["category_img"]["tmp_name"], $this->target_allimg_dir.$img);
                    $insert_data['cat_img'] = $img;
                } else {
                    $this->session->set_flashdata('message_err', 'Please select valid image');
                    redirect(base_url().'admin/game/gameCatAdd');
                }
            }
            $r = $this->General_model->insert_data("admin_game_category",$insert_data);
            ($r) ? $this->session->set_flashdata('message_succ', 'Successfully added') : $this->session->set_flashdata('message_err', 'Failure to added');
            redirect(base_url().'admin/game/game_cat_list');
        }
    }
    public function game_cat_list(){
        $this->check_user_page_access();
        // add breadcrumbs
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Game Category List', 'admin/game/game_cat_list');
        $data['active'] = 'game_cat_list';
        $data['page_name'] = 'Game Category List';
        //showing game list from function which is created in general_model
        $data['list'] = $this->General_model->backend_game_cat_list();
        $content = $this->load->view('admin/game/game_cat_list.tpl.php',$data,true);
        $this->render($content);
    }
    public function cat_display_order($id) {
        $this->check_user_page_access();
        if (!empty($_POST)) {
            $new_order = $_POST['edit_display_order'];
            $agc_data = $this->General_model->view_data('admin_game_category',array('id'=>$id));
            ($agc_data[0]['display_order'] != $new_order) ? $this->General_model->update_data('admin_game_category',array('display_order' => $agc_data[0]['display_order']),array('display_order' => $new_order)) : '';
            $data['display_order'] = $new_order;
            $update = $this->General_model->update_data('admin_game_category',$data,array('id'=>$id));
            ($update) ? $this->session->set_userdata('message_succ', 'Display order changed') : $this->session->set_userdata('message_err', 'Failure to change order');
            redirect(base_url().'admin/game/game_cat_list');
        }
    }
    // Game Category Edit
    public function gameCatEdit($id) {
        $this->check_user_page_access();
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Game Category List', 'admin/game/game_cat_list');
        $this->breadcrumbs->push('Edit Game Catedory Category', 'admin/game/gameCatEdit');
        $data['active'] = 'gameCatEdit';
        $data['page_name'] = 'Edit Game Catedory Edit Page';
        $data['game_cat_detail'] = $this->General_model->view_data('admin_game_category',array('id'=>$id));
        $content=$this->load->view('admin/game/game_cat_edit.tpl.php',$data,true);
        $this->render($content);
    }
    public function gameCatUpdate() {
        $this->check_user_page_access();
        // $cat_slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $_POST['category_name'])));
        if (!empty($_POST['category_name'])) {
            $update_data_where = array(
                'id' => $_POST['id'],
            );
            $update_data = array(
                "category_name" => $this->input->post('category_name', TRUE),
                "cat_slug" => $_POST['category_name'],
            );
            if (!empty($_FILES['category_img']['name'])) {
                $img = time().basename($_FILES["category_img"]["name"]);
                $ext = end((explode(".", $img)));
                $accept_arr = array('gif','jpeg','png','jpg','GIF','JPEG','PNG','JPG');
                if (in_array($ext, $accept_arr)) {
                    move_uploaded_file($_FILES["category_img"]["tmp_name"], $this->target_allimg_dir.$img);
                    $update_data['cat_img'] = $img;
                } else {
                    $this->session->set_flashdata('message_err', 'Please select valid image');
                    redirect(base_url().'admin/game/gameCatEdit/'.$_POST['id']);
                }
            }
            $r = $this->General_model->update_data("admin_game_category",$update_data,$update_data_where);
            ($r) ? $this->session->set_flashdata('message_succ', 'Update successfull') : $this->session->set_flashdata('message_err', 'Failure to update');
            redirect(base_url().'admin/game/gameCatEdit/'.$_POST['id']);
        }
    }
    // Delete Game Category
    public function delete_gameCat($id) {
        $this->check_user_page_access();
        $update_data_where = array(
            'id' => $id
        );
        $update_data = array(
            "is_deleted" => 1,
        );
        $r = $this->General_model->update_data("admin_game_category",$update_data,$update_data_where);
        if($r) {
            $this->session->set_userdata('message_succ', 'Delete Successfull');
        } else {
            $this->session->set_userdata('message_err', 'Failure to delete');
        }
        redirect(base_url().'admin/game/game_cat_list');
    }
    // Game add
    public function gameAdd() {
        $this->check_user_page_access();
        // add breadcrumbs
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('game List', 'admin/game');
        $this->breadcrumbs->push('Add game', 'game');
        $data['active']='game';
        $data['page_name']='Add Game';
        //fetch the category for showing into game add form
        $data['cat_name']=$this->General_model->game_system_list();
        $content=$this->load->view('admin/game/game.tpl.php',$data,true);
        $this->render($content);
    }
    public function add_game(){
        $this->check_user_page_access();
        $game_type='';
        foreach($_POST['game_type'] as $r=>$w) {
            $game_type .=$w.'|';
        }
        if($this->input->post('name', TRUE) != '')
        {
            $img='';
            foreach($_POST['category'] as $key => $cat) {
                $data=array(
                    'game_name'=>$this->input->post('name', TRUE),
                    'game_description'=>$this->input->post('description', TRUE),
                    'game_category_id'=>$cat,
                    'created_at'=>date('Y-m-d H:i:s', time()),
                    'game_type'=>$game_type,
                );
                $r=$this->General_model->insert_data('admin_game',$data);
                if ($r) {
                    for($i=0;$i<count($_FILES["image"]["name"]);$i++) {
                        if(isset($_FILES["image"]["name"][$i]) && $_FILES["image"]["name"][$i]!='') {
                            $img=time().basename($_FILES["image"]["name"][$i]);
                            $ext = end((explode(".", $img)));
                            if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg'|| $ext=='GIF' || $ext=='JPEG' || $ext=='PNG' || $ext=='JPG') {
                                $target_file = $this->target_dir.$img;
                                move_uploaded_file($_FILES["image"]["tmp_name"][$i], $target_file);
                            }
                            $insertArr=array(
                                'game_id'=>$r,
                                'game_image'=>$img,
                            );
                            $this->General_model->insert_data('game_image',$insertArr);
                        }
                    }
                } else {
                    $this->session->set_userdata('message_err', 'Failure to add');
                    redirect(base_url().'admin/game');
                }
            }
        
        $this->session->set_userdata('message_succ', 'Successfully added');
        redirect(base_url().'admin/game/');

        } else {
            header('location:'.base_url().'admin/game');
        }
    }
    //load edit page
    public function gameEdit($id) {
        $this->check_user_page_access();
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('game List', 'admin/game');
        $this->breadcrumbs->push('Edit game', 'admin/game/gameEdit');
        $data['active']='game_edit';
        $data['page_name']='Edit Page';
        $data['game']=$this->General_model->view_data('admin_game',array('id'=>$id));
        $data["game_image"]=$this->General_model->view_data('game_image',array('game_id'=>$id));
        $data['cat_name']=$this->General_model->view_data_by_order('admin_game_category',array('custom'=>null,'is_deleted'=>0),'id','asc');
        $query = "SELECT GROUP_CONCAT(game_category_id) as game_category_id FROM `admin_game` where game_name ='".$data['game'][0]['game_name']."' AND is_category_active = 1";
        $checked_cat_arr = $this->db->query($query)->result_array();
        $data['checked_cat'] = explode(',', $checked_cat_arr[0]['game_category_id']);        
        $content=$this->load->view('admin/game/game_edit.tpl.php',$data,true);
        $this->render($content);
    }
    public function gameUpdate() {
        $this->check_user_page_access();
        $data['active']='game_edit';
        $game_type='';
        $arr_game = $this->General_model->view_single_row('admin_game', array('id' => $_POST['id']),'game_name');
        $arr_game_ids = $this->General_model->view_data('admin_game', array("game_name" => $arr_game['game_name']));
        $cat_master = $this->General_model->view_data('admin_game_category', array('custom' => NULL, 'is_deleted' => 0));
        
        $game_ids_arr = array_column($arr_game_ids, 'id');
        $game_cat_ids_arr = array_column($arr_game_ids, 'game_category_id');
        $cat_master_arr = array_column($cat_master, 'id');
        foreach($_POST['game_type'] as $r=>$w) {
            $game_type .=$w.'|';
        }
        
        foreach($_POST['category'] as $key => $cat) {
            $adminexist_list = $this->General_model->view_single_row('admin_game', array('game_category_id' => $cat, 'game_name' => $arr_game["game_name"]),'');
            if (!empty($adminexist_list)) {
                // if category exist in admin game 
                if (in_array($adminexist_list['id'], $game_ids_arr)) {
                    $update_data_where = array(
                        'id' => $adminexist_list['id']
                    );
                    $update_data = array(
                        "game_name" => $this->input->post('name', TRUE),
                        "game_description" => $this->input->post('description', TRUE),
                        "game_category_id" => $cat,
                        "game_type" => $game_type,
                        "is_category_active" => 1,
                    );
                    $r = $this->General_model->update_data("admin_game",$update_data,$update_data_where);
                    $image_update_id = $adminexist_list['id'];
                }
            } else {
                // if category doen't exist in admin game 
                $data = array(
                    "game_name" => $this->input->post('name', TRUE),
                    "game_description" => $this->input->post('description', TRUE),
                    "game_category_id" => $cat,
                    "created_at" => date('Y-m-d H:i:s', time()),
                    "game_type" => $game_type,
                );
                $r = $this->General_model->insert_data('admin_game',$data);
                $image_update_id = $r;
                if ($r) {
                    $arr_sub_game = $this->General_model->view_single_row('admin_sub_game', array('game_id' => $arr_game['id']),'');

                    $sub_game_data = array(
                        "game_id" => $r,
                        "sub_game_name" => $arr_sub_game['sub_game_name'],
                        "status" => 0,
                    );
                    $this->General_model->insert_data('admin_sub_game',$sub_game_data);
                }
            }

            if (isset($_POST['image_id']) && $_POST['image_id'] !='') {
                foreach ($_POST['image_id'] as $key => $image_id) {
                    $image_name = $this->General_model->view_single_row('game_image', array('id' => $image_id),'');
                    $image_already_exist = $this->General_model->view_single_row('game_image', array('game_image' => $image_name["game_image"], 'game_id' => $adminexist_list['id']),'');
                    if (empty($image_already_exist)) {
                        $insertArr = array(
                            'game_id'=>$image_update_id,
                            'game_image'=>$image_name["game_image"],
                        );
                        $this->General_model->insert_data('game_image',$insertArr);
                    }
                }
            }

            if (isset($_FILES["image"]) && $_FILES["image"] !='') {
                for($i=0;$i<count($_FILES["image"]["name"]);$i++) {
                    if(isset($_FILES["image"]["name"][$i]) && $_FILES["image"]["name"][$i]!='') {
                        $img=time().basename($_FILES["image"]["name"][$i]);
                        $ext = end((explode(".", $img)));
                        if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg'|| $ext=='GIF' || $ext=='JPEG' || $ext=='PNG' || $ext=='JPG') {
                            $target_file = $this->target_dir.$img;
                            if (!file_exists($this->target_dir.$img)) {
                                move_uploaded_file($_FILES["image"]["tmp_name"][$i], $target_file);
                            }
                        }
                        $insertArr=array(
                            'game_id'=>$image_update_id,
                            'game_image'=>$img,
                        );
                        $this->General_model->insert_data('game_image',$insertArr);
                    }
                }
            }
        }
        
        // if uncheck catrgory
        foreach ($cat_master_arr as $key => $cma) {
            if (!in_array($cma, $_POST['category'])) {
                $adminexist_list = $this->General_model->view_single_row('admin_game', array('game_category_id' => $cma, 'game_name' => $arr_game['game_name']),'');
                
                $update_data_where = array(
                        'id' => $adminexist_list['id']
                );
                $update_data = array(
                    'is_category_active' => 0
                );
                $this->General_model->update_data('admin_game',$update_data,$update_data_where);
            }
        }

        if ($r) {
            $this->session->set_userdata('message_succ', 'Update successfull');
            header('location:'.base_url().'admin/game/gameEdit/'.$_POST['id']);
        } else {
            $this->session->set_userdata('message_err', 'Failure to update');
            header('location:'.base_url().'admin/game/gameEdit/'.$_POST['id']);
        }
    }
    //delete
    public function delete($id) {
        // $game_name = urldecode($name);
        $this->check_user_page_access();

        $arr = $this->General_model->view_single_row('admin_game', array('id' => $id),'');
        
        $game_name = $arr['game_name'];
        $game_image = $this->General_model->view_data('game_image',array('game_id'=>$arr['id']));
        unlink($this->target_dir.$game_image[0]["game_image"]);
        
        $r=$this->General_model->delete_data('admin_game',array('game_name'=>$game_name));
        if($r) {
            $this->General_model->delete_data('admin_sub_game',array('game_id'=>$id));
            $this->session->set_userdata('message_succ', 'Delete Successfull');
            header('location:'.base_url().'admin/game');
        } else {
            $this->session->set_userdata('message_err', 'Failure to delete');
            header('location:'.base_url().'admin/game');
        }
    }
    public function imageDelete($image_id) {
        $this->check_user_page_access();
        $game_image = $this->General_model->view_single_row('game_image',array('id'=>$image_id),'');

        $this->db->select('l.id,ag.game_name,ag.game_category_id,gi.game_image');
        $this->db->from('lobby l');
        $this->db->join('admin_game ag','ag.id=l.game_id');
        $this->db->join('game_image gi','gi.id=l.image_id');
        $this->db->where("l.status", 1);
        $this->db->where("l.image_id", $image_id);
        $res['lobby_list'] = $this->db->get()->result();
        
        $this->db->select('g.id,ag.game_name,ag.game_category_id');
        $this->db->from('game g');
        $this->db->join('admin_game ag','ag.id=g.game_id');
        $this->db->join('game_image gi','gi.id=g.image_id');
        $this->db->where("g.payment_status",1);
        $this->db->where("g.status",1);
        $this->db->where("g.image_id", $image_id);
        $res['match_list'] = $this->db->get()->result();
        
        $res['result'] = array_merge($res['lobby_list'],$res['match_list']);
        if (count($res['result']) == 0) {
            unlink($this->target_dir.$game_image["game_image"]);
            
            $r = $this->General_model->delete_data('game_image',array('game_image'=>$game_image["game_image"]));
            if ($r) {
                $this->session->set_userdata('message_succ', 'Delete Successfull');
                header('location:'.base_url().'admin/game/gameEdit/'.$game_image['game_id']);
            } else {
                $this->session->set_userdata('message_err', 'Failure to delete');
                header('location:'.base_url().'admin/game/gameEdit/'.$game_image['game_id']);
            }
        } else {
            $this->session->set_userdata("message_err", "Players are using this game image live now, you can't Delete it.");
            redirect(base_url().'admin/game/gameEdit/'.$game_image['game_id']);
        }
    }
    //for showing subgame for adding subgame
    public function subgameAdd() {
        $this->check_user_page_access();
        // add breadcrumbs
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('game List', 'admin/game');
        $this->breadcrumbs->push('Add Sub game', 'game');
        $data['active']='game';
        $data['page_name']='Add Sub Game';
        //fetch the game for showing into subgame add form
        $game_category_dt = $this->General_model->matches_system_list();
        if (!empty($game_category_dt)) {
            foreach($game_category_dt as $game_cate_dt) {
                $game_categ_slug = $game_cate_dt['cat_slug'];
                $this->db->select('*');
                $this->db->from('admin_game');
                $this->db->where("game_category_id",$game_cate_dt['id']);
                $this->db->where("custom",NULL);
                $this->db->order_by('game_name','asc');
                $data['game_name_dt'][$game_categ_slug] = $this->db->get()->result_array();
            }
        }
        
        $content=$this->load->view('admin/game/subgame.tpl.php',$data,true);
        $this->render($content);
    }
    //insert sungame
    public function gameInsert() {
        $this->check_user_page_access();
        $game_id_array = $this->input->post('game_id');
        if(!empty($game_id_array)){
            foreach($game_id_array as $game_id){
                if ($game_id != '') {
                    $data=array(
                        'game_id'       =>  $game_id,
                        'sub_game_name' =>  $this->input->post('sub_game_name', TRUE)
                    );
                    $r=$this->General_model->insert_data('admin_sub_game',$data);
                }
            }
        }
        if($r) {
            $this->session->set_userdata('message_succ', 'Successfully added');
            redirect(base_url().'admin/game/subgameList');
        } else {
            $this->session->set_userdata('message_err', 'Failure to add');
            redirect(base_url().'admin/game/subgameList');
        }
        header('location:'.base_url().'admin/game');
    }
    //subgame list
    public function subgameList() {
        $this->check_user_page_access();
        // add breadcrumbs
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('game List', 'admin/game');
        $this->breadcrumbs->push('Sub game List', 'admin/game/subgameList');
        $data['active']='game_list';
        $data['page_name']='Sub Game List';
        //showing game list from function which is created in general_model
        $data['list']=$this->General_model->backend_subgame_list();
        // echo '<pre>';
        // print_r($data['list']);exit;
        $content=$this->load->view('admin/game/admin_subgame_list.tpl.php',$data,true);
        $this->render($content);
    }
    //subgame edit
    public function subgameEdit($id='-1') {
        $this->check_user_page_access();
        // add breadcrumbs
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Subgame List', 'admin/game/subgameList');
        $this->breadcrumbs->push('Edit game', 'admin/game/subgameEdit');
        $data['active']='game_edit';
        $data['page_name']='Edit Page';
        $data['game_name_dt']=$this->General_model->view_all_data('admin_game','id','asc');
        //show game sub name
        $data['subgame_data']=$this->General_model->view_data('admin_sub_game',array('id'=>$id));
        $content=$this->load->view('admin/game/subgame_edit.tpl.php',$data,true);
        $this->render($content);
    }
    public function subgameUpdate() {
        $this->check_user_page_access();
        $data=array(
            'game_id'=>$this->input->post('game_id', TRUE),
            'sub_game_name'=>$this->input->post('sub_game_name', TRUE)
        );
        $r = $this->General_model->update_data('admin_sub_game',$data,array('id'=>$this->input->post('id',true)));
        if ($r) {
            $this->session->set_userdata('message_succ', 'Successfully updated');
            redirect(base_url().'admin/game/subgameEdit/'.$this->input->post('id',true));
        } else {
            $this->session->set_userdata('message_err', 'Failure to update');
            redirect(base_url().'admin/game/subgameEdit/'.$this->input->post('id',true));
        }
        header('location:'.base_url().'admin/game/subgameEdit/'.$this->input->post('id',true));
    }
    public function subgamedelete($subgame_id) {
        $this->check_user_page_access();
        $r = $this->General_model->delete_data('admin_sub_game',array('id'=>$subgame_id));
        if ($r) {
            $this->session->set_userdata('message_succ', 'Delete Successfull');
            header('location:'.base_url().'admin/game/subgameList');
        }
        else
        {
            $this->session->set_userdata('message_err', 'Failure to delete');
            header('location:'.base_url().'admin/game/subgameList');
        }
    }
    public function totalAmount() {
        $data['active']='gameamnt_list';
        $data['page_name']='Payment Calculation Table';
        $data['total_amnt'] = $this->User_model->game_total_amnt();
        // echo '<pre>';
        // print_r($data['total_amnt']);exit;
        $content = $this->load->view('admin/game/game_totalamount_list.tpl.php',$data,true);
        $this->render($content);
    }
    
    public function gameUrl() {
        
        //$data['watch_game']=$this->General_model->watch_game_info();
        //echo $this->db->last_query();
        
        $data['list']=$this->General_model->url_info();
        // echo '<pre>';
        // print_r($data['list']);exit;
        $data['active']='game_url';
        $data['page_name']='Watch Game List';
        $data['page_name1']='Watch Game Result List';
        $content=$this->load->view('admin/game/game_url_list.tpl.php',$data,true);
        $this->render($content);
    }

    public function changeStatus($v_id,$game_id,$subgame_id,$user_id,$status) {
        $this->check_user_page_access();
        // get bet id
        $arr_bet_id = $this->General_model->view_data('game',array('id'=>$game_id));
        $arr_bet_data = $this->General_model->view_data('bet',array('id'=>$arr_bet_id[0]['bet_id']));
        
        $chall_id = $arr_bet_data[0]['challenger_id'];
        $acc_id = $arr_bet_data[0]['accepter_id'];
        
        if($user_id == $status){
            
            $check_user_won_lose_info   =   $this->General_model->url_info_admin($game_id);
                
            $listuser   = explode(',',$check_user_won_lose_info->bet_user_id);
            $listvideo  = explode(',',$check_user_won_lose_info->v_video_id);
            
            $user_index = array_search($user_id,$listuser); 
            
            
            $dt=array(
                'status'    =>  3,
                'video_id'  =>  $listvideo[$user_index],
            );
            
            $this->General_model->update_data('game',$dt,array('id'=>$game_id));
                            
            $data=array('winner_id'=>$user_id,'loser_id'=>$acc_id,'win_lose_deta'=>date('Y-m-d H:i:s',time()));
            $r=$this->General_model->update_data('bet',$data,array('id'=>$arr_bet_id[0]['bet_id']));
            
            $user_data_where = array(
                'user_id'   => $user_id,
            );
            $userListDp = $this->General_model->view_data('user_detail',$user_data_where);
            foreach($userListDp as $userList){
                $user_where = array(
                    'user_id'   => $userList['user_id'],
                );
                $update_user = array(
                    'total_balance' =>  $userList['total_balance'] + (2*$arr_bet_id[0]['price']),
                );
                $this->General_model->update_data('user_detail',$update_user,$user_where);
            }               
            
        } else {
            $check_user_won_lose_info   =   $this->General_model->url_info_admin($game_id);
                
            $listuser   = explode(',',$check_user_won_lose_info->bet_user_id);
            $listvideo  = explode(',',$check_user_won_lose_info->v_video_id);
            
            $user_index = array_search($user_id,$listuser); 
            
            $dt=array(
                'status'=>3,
                'video_id'=>$listvideo[$user_index],
            );
            $this->General_model->update_data('game',$dt,array('id'=>$game_id));                
            
            $data=array('winner_id'=>$acc_id,'loser_id'=>$user_id,'win_lose_deta'=>date('Y-m-d H:i:s',time()));
            
            
            $r=$this->General_model->update_data('bet',$data,array('id'=>$arr_bet_id[0]['bet_id']));
            
            $user_data_where = array(
                'user_id'   => $acc_id,
            );
            $userListDp = $this->General_model->view_data('user_detail',$user_data_where);
            

            foreach($userListDp as $userList){
                $user_where = array(
                    'user_id'   => $userList['user_id'],
                );
                $update_user = array(
                    'total_balance' =>  $userList['total_balance'] + (2*$arr_bet_id[0]['price']),
                );

                $this->General_model->update_data('user_detail',$update_user,$user_where);
            }
        }
        
        if($r) {
            $this->session->set_flashdata('status_msg', '<p style="color:green">Status Updated successfull</p>');
            redirect(base_url().'admin/game/gameUrl');
            
            
        } else {
            $this->session->set_flashdata('status_msg', '<p style="color:red">Failure to update</p>');
            redirect(base_url().'admin/game/gameUrl');
        }
    }

    public function gameURLDelete($game_id){
        error_reporting(0);
        $game_details       =   $this->General_model->view_data('game',array('id'=>$game_id));
    
        $get_price          =   $game_details[0]['price'];
        $get_both_user_id   =   $this->General_model->view_data('bet',array('id'=>$game_details[0]['bet_id']));
        $challenger_id      =   $get_both_user_id[0]['challenger_id'];
        $accepter_id        =   $get_both_user_id[0]['accepter_id'];
        
        $challenger_id_balance = $this->General_model->view_data('user_detail',array('user_id'=>$challenger_id));
            
        $accepter_id_balance = $this->General_model->view_data('user_detail',array('user_id'=>$accepter_id));
        
        $percentage = $this->General_model->view_all_data('admin_points_percentage','id','asc');
        $lessthan5_total        = $percentage[0]['lessthan5'];  
        $morethan5_25_total     = $percentage[0]['morethan5_25'];   
        $morethan25_50_total    = $percentage[0]['morethan25_50'];  
        $morethan50_total       = $percentage[0]['morethan50']; 
        
        if($get_price < 5){
            $price_add = $get_price + $lessthan5_total;
        }else if($get_price >= 5 && $get_price < 25){
            $price_add = $get_price + $morethan5_25_total;
        } else if($get_price >= 25 && $get_price < 50){
            $price_add = $get_price + $morethan25_50_total;
        } else if($get_price >= 50){
            $price_add = $get_price + $morethan50_total;
        }
        $price_add_decimal = number_format((float)$price_add, 2, '.', '');

        $challenger_update_balance  =   $challenger_id_balance[0]['total_balance'] + $price_add_decimal;
        $accepter_update_balance    =   $accepter_id_balance[0]['total_balance'] + $price_add_decimal;

        $challenger_data_Where = array(
            'total_balance' =>  $challenger_update_balance,
        );
        
        $accepter_data_Where = array(
            'total_balance' =>  $accepter_update_balance,
        );
        
        $this->General_model->update_data('user_detail',$challenger_data_Where,array('user_id'=>$challenger_id));
        
        $this->General_model->update_data('user_detail',$accepter_data_Where,array('user_id'=>$accepter_id));
        $data = array(
            'status' => 0,
        );
        $this->General_model->update_data('game',$data,array('id'=>$game_id));
        $this->session->set_userdata('message_succ', 'Delete Successfull');
        redirect(base_url().'admin/game/gameUrl');
    }
    
    public function withdrawal() {
        $this->check_user_page_access();
        // add breadcrumbs
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Withdrawal List', 'withdrawal');
        $data['active'] = 'withdrawal';
        $data['page_name'] = 'Withdrawal Request List';

        $data['withdraw'] = $this->General_model->withdraw_requests();
        $content = $this->load->view('admin/game/withdrawal_list.tpl.php',$data,true);
        $this->render($content);
    }

    public function paid_withdrawal(){
        $this->check_user_page_access();
        // add breadcrumbs
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Withdrawal Paid List', 'withdrawal');
        $data['active']='withdrawal_paid_list';
        $data['page_name']='Withdrawal Paid List';

        $data['withdraw']=$this->General_model->withdraw_paid_requests();
        // echo '<pre>';
        // print_r($data['withdraw']);exit;
        $content=$this->load->view('admin/game/withdrawal_paid_list.tpl.php',$data,true);
        $this->render($content);
    }

    public function not_accepted_list()
    {
        $this->check_user_page_access();
        // add breadcrumbs
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Challange not Accepted', 'not_accepted');
        $data['active']='not_accepted';
        $data['page_name']='Challange not Accepted';

        $data['challenge']=$this->General_model->challange_not_accepted();
        //  echo '<pre>';
        // print_r($data['challenge']);exit;
        $content=$this->load->view('admin/game/challenge_not_accepted.tpl.php',$data,true);
        $this->render($content);
    }

    public function video_not_uploaded(){
        $this->check_user_page_access();
        // add breadcrumbs
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Video not Uploaded', 'not_uploaded');
        $data['active']='not_uploaded';
        $data['page_name']='Video not Uploaded';

        $data['video']=$this->General_model->video_not_uploaded();
        // echo '<pre>';
        // print_r($data['video']);exit;
        $content=$this->load->view('admin/game/video_not_uploaded.tpl.php',$data,true);
        $this->render($content);
    }

    public function uploaded_videos(){
        $this->check_user_page_access();
        // add breadcrumbs
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Latest Streams', 'uploaded_videos');
        $data['active']='uploaded_videos';
        $data['page_name']='Latest Streams';
        $data['video'] = $this->General_model->getvideos();
        $content=$this->load->view('admin/game/uploaded_videos.tpl.php',$data,true);
        $this->render($content);
    }
    
    function uploaded_videos_details($id){
        $data_video = $this->General_model->getvideos($id);
        $lobby_list = $this->General_model->lobby_list();
        $disable_twitch = "style='display:block;'";
        $enable_twitch = "style='display:none;'";
        $is_check = ($data_video[0]->type == 1) ? "checked" : '';
        $is_check_obs = ($data_video[0]->type == 2) ? "checked" : '';
        $videourl_val = '';
        $twitch_val = ($data_video[0]->type == 1) ? 'required' : '';
        $twitch_uname_disable = ($data_video[0]->type != 1) ? "style='display:none;'":'';
        if ($data_video[0]->type == 0) {
            $videourl_val = 'required';
            $disable_twitch = "style='display:none;'";
            $enable_twitch = "style='display:block;'";
            $twitch_val = '';
            $is_check = "";
            $is_check_obs = "";
        }

        $lobbys = '<div class="box-body lobbylistbox" '.$disable_twitch.'>
            <div class="form-group"><label for="name" style="padding: 0 15px;">Live Lobby List</label>';
        $lobbys .='<div style="margin: 10px auto;"><table class="modal_datatable table table-bordered table-striped"><thead>
                <tr>
                    <th width="10">Select lobby</th>
                    <th>Lobby Name</th>
                    <th>Creater Name</th>
                </tr>
            </thead>
            <tbody>';
            if (!empty($lobby_list)) {
                foreach ($lobby_list as $key => $lbylst) {
                    $checked = ($lbylst['id'] == $data_video[0]->lobby_id) ? 'checked' : '';
                    $creator_tag =  $this->General_model->get_my_fan_tag($lbylst['id'], $lbylst['lobby_crtor'],$lbylst['id'])->fan_tag; 
                    $lobbys .='
                        <tr>
                            <td width="10"><label class="cntnr"><input type="checkbox" name="lobbyselect" class="lobby_slct" id="lobbyselect" value="'.$lbylst['id'].'" '.$checked.'><span class="chckmrk"></span></label>
                            </td>
                            <td><span><b>'.$lbylst['game_name'].'</b></span></td>
                            <td>'.$creator_tag.'</td>
                        </tr>';
                }
            }
        $lobbys .='</tbody></table>';
        $lobbys .= '</div></div></div>';
        $is_home = ($data_video[0]->is_home == 1) ? 'checked' : '';
        $data_value = '<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Edit Video URL and Tag</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="change_vids" name="change_vids" method="POST" class="edit_vids">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Tag Name</label>
                                <input type="hidden" id="game_id" class="form-control" name="id" value="'.$data_video[0]->id.'" required>
                                <input type="text" id="balance" class="form-control" name="tag" value="'.$data_video[0]->tag.'" required>
                            </div>
                            <div class="form-group">
                                <label for="name">Home Page</label>
                                <div>
                                    <label class="switch common_switch"><input type="checkbox" name="is_home" value="" class="" '.$is_home.'><span class="slider round"></span></label>
                                </div>
                            </div>
                            <hr>
                            <div class="row form-group">
                                <div class="col-md-6">
                                    <label for="name">Twitch Enable</label>
                                    <div>
                                        <label class="switch common_switch"><input type="checkbox" name="twitch_enable" value="" class="" '.$is_check.'><span class="slider round"></span></label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="name">OBS Enable</label>
                                    <div>
                                        <label class="switch common_switch"><input type="checkbox" name="obs_enable" value="" class="" '.$is_check_obs.'><span class="slider round"></span></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group twitch_username" '.$twitch_uname_disable.'>
                                <label for="name">Twitch Username</label>
                                <input type="text" class="form-control" name="twitchusername" value="'.$data_video[0]->twitch_username.'" '.$twitch_val.'>
                            </div>
                            <div class="form-group video_url" '.$enable_twitch.'>
                                <label for="name">Video URL</label>
                                <input type="url" class="form-control" name="videourl" value="'.$data_video[0]->video_url.'" '.$videourl_val.'>
                            </div>
                        </div>';
        $data_value .=  $lobbys;         
        $data_value .=  '<div class="box-footer">
                            <input class="btn btn-primary" type="submit" value="Submit" >
                        </div>
                    </form>
                </div>
            </div>          
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>      
            </div>';
        
        echo $data_value;
    }
    function add_uploaded_videos() {
        $lobby_list = $this->General_model->lobby_list();
        $lobbys = '<div class="box-body lobbylistbox" style="display:none;"> <div class="form-group"><label for="name" style="padding: 0 15px;">Live Lobby List</label>';
        $lobbys .='<div style="margin: 10px auto;"><table class="modal_datatable table table-bordered table-striped"><thead>
                <tr>
                    <th width="10">Select lobby</th>
                    <th>Lobby Name</th>
                    <th>Creater Name</th>
                </tr>
            </thead>
        <tbody>';
        if (!empty($lobby_list)) {
            foreach ($lobby_list as $key => $lbylst) {
                $creator_tag =  $this->General_model->get_my_fan_tag($lbylst['id'], $lbylst['lobby_crtor'],$lbylst['id'])->fan_tag; 
                $lobbys .=' <tr>
                    <td width="10"><label class="cntnr"><input type="checkbox" name="lobbyselect" class="lobby_slct" id="lobbyselect" value="'.$lbylst['id'].'"><span class="chckmrk"></span></label>
                    </td>
                    <td><span><b>'.$lbylst['game_name'].'</b></span></td>
                    <td>'.$creator_tag.'</td>
                </tr>';
            }
        }
        $lobbys .='</tbody></table>';
        $lobbys .= '</div></div></div>';
        $data_value = '<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Add Video URL and Tag</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form action="#" id="change_vids" method="POST" class="insert_vids">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Tag Name</label>
                                <input type="text" id="balance" class="form-control" name="tag" value="" required>
                            </div>
                            <div class="form-group">
                                <label for="name">Home Page</label>
                                <div>
                                    <label class="switch common_switch"><input type="checkbox" name="is_home" value="" class=""><span class="slider round"></span></label>
                                </div>
                            </div>
                            <hr>
                            <div class="row form-group">
                                <div class="col-md-6">
                                    <label for="name">Twitch Enable</label>
                                    <div>
                                        <label class="switch common_switch"><input type="checkbox" name="twitch_enable" value="twitch_enable" class=""><span class="slider round"></span></label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="name">OBS Enable</label>
                                    <div>
                                        <label class="switch common_switch"><input type="checkbox" name="obs_enable" value="obs_enable" class=""><span class="slider round"></span></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group twitch_username" style="display:none;">
                                <label for="name">Twitch Username</label>
                                <input type="text" class="form-control" name="twitchusername" value="">
                            </div>
                            <div class="form-group video_url">
                                <label for="name">Video URL</label>
                                <input type="url" class="form-control" name="videourl" value="" required>
                            </div>

                        </div>';
                        $data_value .= $lobbys;
                        $data_value .= '<div class="box-footer">
                            <input class="btn btn-primary" type="submit" value="Submit">
                        </div>
                    </form>
                </div>
            </div>          
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>      
            </div>';
        
        echo $data_value;
    }    
    function uploaded_videos_order($id){
        $data_video = $this->General_model->getvideos($id);
        $data_value = '<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Edit Video Order
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form action="#" id="change_vids" name="change_vids" method="POST" class="edit_vids">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="order">Order</label>
                                <input type="hidden" id="game_id" class="form-control" name="id" value="'.$data_video[0]->id.'" required>
                                <input type="text" id="balance" class="form-control" name="order" value="'.$data_video[0]->order_by.'" required>
                            </div>
                        </div>
                        <div class="box-footer">
                            <input class="btn btn-primary" type="submit" value="Submit" >
                        </div>
                    </form>
                </div>
            </div>          
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>      
            </div>';
        
        echo $data_value;
    }
    function videos_details_lock($id){
        $data_video     =   $this->General_model->view_single_row('video','id',$id);
        
        if ($data_video['lock'] == '') {
            $lock = '1';
        } else if($data_video['lock'] == '1') {
            $lock = '0';
        } else {
            $lock = '1';
        }
        
        $data1 = array(
            'lock'=>$lock,
        );
        $r1 =  $this->General_model->update_data('video',$data1,array('id'=>$id));
        if($r1) {
            if($lock == '1') {
                $this->session->set_userdata('message_succ', 'Game URL Locked ');   
            } else {
                $this->session->set_userdata('message_succ', 'Game URL Unlocked ');
            }
            echo $lock;
            redirect(base_url().'admin/game/uploaded_videos');
        }
    }
    function insert_new_upvideo() {    
        $this->check_user_page_access();
        $maxorder = $this->db->select_max('v.order_by')->from('videos v')->get()->row()->order_by+1;
        $data_insert = array(
            'order_by'=> $maxorder,
            'video_url' => isset($_POST['videourl'])?$_POST['videourl']:'',
            'tag' => $_POST['tag'],
            'type' => (isset($_POST['twitch_enable'])? '1' : (isset($_POST['obs_enable']) ? '2' : '0')),
            'lobby_id' => isset($_POST['lobbyselect'])?$_POST['lobbyselect']:'',
            'twitch_username' => isset($_POST['twitchusername'])?$_POST['twitchusername']:'',
            'is_home' => (isset($_POST['is_home'])) ? 1 : 0,
            'created_at' => date('Y-m-d H:i:s')
        );
        $insert = $this->General_model->insert_data('videos',$data_insert);
        if($insert) {
            $this->session->set_userdata('message_succ', 'Video Data Added');
        } else {
           $this->session->set_userdata('message_err', 'Failure to Add Video Data');
        }
    }
    function uploaded_videos_details_update(){
        $this->check_user_page_access();
        $id = $_REQUEST['id'];
        $tag = $_REQUEST['tag'];
        $videourl = $_REQUEST['videourl'];
        $order_by = $_REQUEST['order'];
        $data = array(
            'id'=>$id,
        );
        $update_data = array(
            'tag'=>$tag,
            'video_url'=>$videourl
        );
        if (isset($_REQUEST['order']) && $_REQUEST['order'] != '') {
            $update_data = array(
                'order_by'=>$order_by
            );
        } else if (isset($_REQUEST['twitchusername']) && $_REQUEST['twitchusername'] != '') {
            $update_data = array(
                'tag'=>$tag,
                'video_url'=>'',
                'lobby_id'=>$_REQUEST['lobbyselect'],
                'type'=>1,
                'is_home' => (isset($_REQUEST['is_home'])) ? 1 :0,
                'twitch_username'=>$_REQUEST['twitchusername']
            );
        } else if (isset($_REQUEST['videourl']) && $_REQUEST['videourl'] != '') {
            $update_data = array(
                'tag'=>$tag,
                'twitch_username'=>'',
                'lobby_id'=>'',
                'type'=>0,
                'is_home' => (isset($_REQUEST['is_home'])) ? 1 :0,
                'video_url'=>$_REQUEST['videourl']
            );
        }else{
            $update_data = array(
                'tag'=>$tag,
                'twitch_username'=>'',
                'lobby_id'=>$_REQUEST['lobbyselect'],
                'type'=>2,
                'is_home' => (isset($_REQUEST['is_home'])) ? 1 :0,
                'video_url'=>''
            );
        }
        ($this->General_model->update_data('videos',$update_data,$data)) ? $this->session->set_userdata('message_succ', 'Video Data Updated') :  $this->session->set_userdata('message_err', 'Failure to Update Video Data');
    }
    
    function change_videodisplay_set($id,$val) {
        $this->check_user_page_access();
        $data = array(
            'id'=>$id,
        );
        $update_data = array(
            'is_display'=>$val
        );
        $r=$this->General_model->update_data('videos',$update_data,$data);
        if ($r) {
            $result['display_val'] = $val;
        }
        echo json_encode($result);
        exit();
    }

    public function deleteVideo($video_id) {
        $this->check_user_page_access();
        $r=$this->General_model->delete_data('videos',array('id'=>$video_id));
        if($r) {
            $this->session->set_userdata('message_succ', 'Delete Successfull');
            header('location:'.base_url().'admin/game/uploaded_videos');
        } else {
            $this->session->set_userdata('message_err', 'Failure to delete');
            header('location:'.base_url().'admin/game/uploaded_videos');
        }
    }
    
    public function get_reset_withdrawal() {
        $data=array(
            'reset' => '0',
        );
        $r = $this->General_model->update_data('withdraw_request',$data,array('reset'=>'1'));
        if($r) {
            header('location:'.base_url().'admin/game/paid_withdrawal');
        }
        
    }
    
    
    
    function withdrawalExcel(){
        
        $withdraw = $this->General_model->withdraw_requests();
        
        
        $all_front_user = $this->User_model->user_view();
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Frontend User');
        
        $header_section_row = 1; $header_section_col = 1;
        $header_section =   'Frontend User';
        $row = 1;
        $col = 0;
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'Acct No');        
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'Name');       
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'Amount Request');     
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'Amount Paid');        
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'Request Date');       
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, 'Paid Date');      
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 1, 'Total');      
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, 1, 'status');     
        
        $style_p = array('font' => array('size' => 12,'bold' => true));
        $style_a = array('font' => array('size' => 12,'bold' => true,'color' => array('rgb' => 'FFFFFF')));
        if(!empty($withdraw)) {
            $data_row = 2;
            foreach($withdraw as $withd) {
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $data_row, ucwords($withd['user_id']));
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $data_row, ucwords($withd['name']));
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $data_row, $withd['amount_requested']);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $data_row, $withd['amount_paid']);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, $data_row, ucwords($withd['req_date']));
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $data_row, ucwords($withd['paid_date']));
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $data_row, ucwords($withd['total_balance']));
                
                if($withd['status'] == '1'){
                    $data_with = 'You have already paid';
                } else {
                    $data_with = 'Payment is due';
                }
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $data_row, $data_with);           
                
                $data_row++;
            }
        }   
        
        $filename = 'withdrawal-'.date('Y-m-d',time()).'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
                
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    function withdrawalPaidExcel(){
        $withdraw = $this->General_model->withdraw_paid_requests();
        $deposit = $this->General_model->paid_deposit();
        $activeplayers=$this->User_model->active_all_users();
        foreach ($withdraw as $key => $value) {
            array_push($withdraw[$key]['payment_type']='Withdraw');
            array_push($withdraw[$key]['fee_type']='Paypal Fee');
        }
        foreach ($deposit as $key => $value) {
            array_push($deposit[$key]['payment_type']='Deposit');
            array_push($deposit[$key]['fee_type']='Packages Fee');
        }
        // print_r(array_merge($withdraw,$deposit));

        $data = array_merge($withdraw,$deposit);

        $total_bal=$this->User_model->totalbalance();
        $totalfees=$this->User_model->totalfees();


        $original_totalbalance=$this->User_model->originaltotalbalance();
        $original_totalfees=$this->User_model->originaltotalfees();
        $all_front_user = $this->User_model->user_view();
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1



        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:C1');
        $this->excel->setActiveSheetIndex(0)->mergeCells('A2:C2');
        $this->excel->setActiveSheetIndex(0)->mergeCells('D1:F1');
        $this->excel->setActiveSheetIndex(0)->mergeCells('D2:F2');
        $this->excel->getActiveSheet()->setAutoFilter('A3:G3');
        // $this->excel->getActiveSheet()->getCell('A1')->setValue('This is the text that I want to see in the merged cells');
        //name the worksheet

        $this->excel->getActiveSheet()->setTitle('Frontend User');
        $this->excel->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("A2:G2")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("A3:G3")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1:G1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->excel->getActiveSheet()->getStyle('A1:G1')->getFill()->getStartColor()->setARGB('#228b22');
        $this->excel->getActiveSheet()->getStyle('A2:G2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->excel->getActiveSheet()->getStyle('A2:G2')->getFill()->getStartColor()->setARGB('#228b22');
        
        $styleArray = array(
          'borders' => array(
            'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_MEDIUM
            )
          )
        );
        $styleArray_2 = array(
          'borders' => array(
            'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );
        $this->excel->getActiveSheet()->getStyle('A1:G1')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getStyle('A2:G2')->applyFromArray($styleArray);
        unset($styleArray);

        
        $header_section_row = 1; $header_section_col = 1;
        $header_section =   'Frontend User';
        $row = 1;
        $col = 0;
       
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'Balance');        
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, '$'.$original_totalbalance);        
        

        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 3, 'index');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, 'Acct No');        
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 3, 'Name');       
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 3, 'Payment');     
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, 3, 'Fee');        
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 3, 'Fee Amount');       
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 3, 'Date');  
        

        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'Fees Collected');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, '$'.$original_totalfees);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 1, 'Active Players');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, $activeplayers);

        

        $style_p = array('font' => array('size' => 12,'bold' => true));
        $style_a = array('font' => array('size' => 12,'bold' => true,'color' => array('rgb' => 'FFFFFF')));
        if(!empty($data)) {
            $data_row = 4;
            $i = 1;
            foreach($data as $withd) {
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $data_row, ucwords($i));
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $data_row, ucwords($withd['user_id']));
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $data_row, ucwords($withd['name']));
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $data_row, $withd['payment_type']);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, $data_row, $withd['fee_type']);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $data_row, $withd['collected_fees']);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $data_row, ucwords(date("Y-m-d", strtotime($withd['paid_date']))));
                $data_row++;
                $i++;
            }
                
                $this->excel->setActiveSheetIndex(0)->mergeCells('A'.$data_row.':B'.$data_row);
                $this->excel->setActiveSheetIndex(0)->mergeCells('A'.($data_row+1).':B'.($data_row+1));
                $this->excel->setActiveSheetIndex(0)->mergeCells('D'.$data_row.':E'.$data_row);
                $this->excel->setActiveSheetIndex(0)->mergeCells('D'.($data_row+1).':E'.($data_row+1));                


                $this->excel->getActiveSheet()->getStyle('A'.$data_row.':G'.$data_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $this->excel->getActiveSheet()->getStyle('A'.$data_row.':G'.$data_row)->getFill()->getStartColor()->setARGB('#ff5000');
                $this->excel->getActiveSheet()->getStyle('A'.($data_row+1).':G'.($data_row+1))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $this->excel->getActiveSheet()->getStyle('A'.($data_row+1).':G'.($data_row+1))->getFill()->getStartColor()->setARGB('#ff5000');
                // $this->excel->getActiveSheet()->getStyle('A'.$data_row.':G'.$data_row)->applyFromArray($styleArray_2);
                // $this->excel->getActiveSheet()->getStyle('A'.($data_row+1).':G'.($data_row+1))->applyFromArray($styleArray_2);
                unset($styleArray_2);

                $this->excel->getActiveSheet()->getDefaultColumnDimension(0)->setWidth('15px');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $data_row, 'Admin');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $data_row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $data_row, 'Adjust');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $data_row, 'Balance');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, $data_row, 'test');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $data_row, '$'.$total_bal['total_balance']);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $data_row, ucwords(date("Y-m-d", strtotime($total_bal['que_date']))));
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $data_row+1, 'Admin');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $data_row+1, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $data_row+1, 'Adjust');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $data_row+1, 'Fees Collected');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, $data_row+1, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $data_row+1, '$'.$totalfees);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $data_row+1, ucwords(date("Y-m-d", strtotime($total_bal['que_date']))));  
                
        }   
        
        $filename = 'withdrawal-'.date('Y-m-d',time()).'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
                
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }
    
    function get_game_admin_message()
    {
        $id = $_REQUEST['id'];
        echo $this->General_model->get_admin_data($id);
    }
    function validate_game() {
        $this->check_user_page_access();
        if (isset($_POST['game_name']) && $_POST['game_name']) {
            $data = $this->General_model->view_data('admin_game',array('game_name'=>$_POST['game_name'], 'custom'=> null));
            if ($data) {
                $data['game_already_exist'] = 'yes';
            } else {
                $data['game_already_exist'] = 'no';
            }
        }
        echo json_encode($data);
        exit();
    }
    function validate_game_cat() {
        $this->check_user_page_access();
        if (isset($_POST['game_cat_name']) && $_POST['game_cat_name']) {
            $game_cat_name = strtolower($_POST['game_cat_name']);
            $r = $this->General_model->view_data('admin_game_category',array('category_name'=>$game_cat_name, 'is_deleted'=>0, 'custom'=>null));
            $data['cat_already_exist'] = ($r) ? 'yes' : 'no';
        }
        echo json_encode($data);
        exit();
    }
    function check_game_category() {
        $this->check_user_page_access();
        $category_id = $_POST['category_id'];
        $game_id = $_POST['game_id'];
        $old_game_name = $_POST['old_game_name'];
        if ($category_id !='' && $game_id !='' && $old_game_name !='') {
            $this->db->select('l.id,ag.game_name,ag.game_category_id');
            $this->db->from('lobby l');
            $this->db->join('admin_game ag','ag.id=l.game_id');
            $this->db->where("l.status", 1);
            $this->db->where("l.game_category_id", $category_id);
            $this->db->where("ag.game_name", $old_game_name);
            $res['game_name_lobby_list'] = $this->db->get()->result();

            $this->db->select('g.id,ag.game_name,ag.game_category_id');
            $this->db->from('game g');
            $this->db->join('admin_game ag','ag.id=g.game_id');
            $this->db->where("g.payment_status",1);
            $this->db->where("g.status",1);
            $this->db->where("g.game_category_id", $category_id);
            $this->db->where("ag.game_name", $old_game_name);
            $res['game_name_match_list'] = $this->db->get()->result();
            
            $res['result'] = array_merge($res['game_name_lobby_list'],$res['game_name_match_list']);

            echo json_encode($res);
            exit();
        }
    }
    function security_pass($id){
        echo '<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form action="#" id="security_pass_with" name="security_pass_with" method="POST" >
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Admin Password</label>
                                <input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required >
                                <input type="hidden" id="action_value" class="form-control" name="action_value" value="'.$id.'" required>
                            </div>
                        </div>
                        <div class="box-footer">
                            <input class="btn btn-primary" type="submit" value="Submit" >
                        </div>
                    </form>
                </div>
            </div>          
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>      
            </div>';
    }
    function remove_withdrawal(){
        $security_password  = $_REQUEST['security_password'];
        $id = $_REQUEST['action_value'];
        $security_password_db = $this->General_model->view_all_data('admin_security_password','id','asc');
        $security_password_db[0]['password'];
        if($security_password == $security_password_db[0]['password'])
        {
            $this->General_model->delete_data('withdraw_request',array('id'=>$id));
            $data_value ='success'; 
        } else{
            $data_value = '<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form action="#" id="security_pass_with" name="security_pass_with" method="POST" >
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Admin Password</label>
                                <input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
                                
                            </div>
                                <label for="name">Password Error</label>
                        </div>
                        <div class="box-footer">
                            <input class="btn btn-primary" type="submit" value="Submit" >
                        </div>
                    </form>
                </div>
            </div>          
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>      
            </div>';
        }   
        echo $data_value;
    }

    public function remove_withdrawal_user($id){
        $r = $this->General_model->delete_data('withdraw_request',array('id'=> $id));
        ($r) ? $this->session->set_flashdata('message_succ', 'Withdrawal request removed successfully') : $this->session->set_flashdata('message_err', 'Fail to remove withdrawal request');
        header('location:'.base_url().'points/withdrawal');
    }

}