<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Giveaway extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->check_user_page_access();
	}
	public function index() {
		$this->breadcrumbs->push('Home', 'admin/dashboard');
	    $this->breadcrumbs->push('Giveaway List', 'admin/giveaway/');
	    $data['active']='giveaway';
	    $data['page_name']='Giveaway List';
	    $data['global_gafees'] = $this->General_model->view_data('site_settings', array('option_title' => 'global_giveaway_fees'))[0];
	    $data['ga_fee_collector'] = $this->General_model->view_data('site_settings', array('option_title' => 'global_giveaway_fee_collector'))[0];
		$data['get_all_giveaway'] = $this->Giveaway_model->get_ga_data(array('order_by' => 'l.id'));

		$content = $this->load->view('admin/giveaway/giveaway_list.tpl.php',$data,true);
		$this->render($content);
	}
	public function ga_winners() {
		if (!empty($_POST['id'])) {
			$arr = explode('&', $_POST['id']);
			$data['giveaway_id'] = end(explode('_', $arr[0]));
			$data['lobby_id'] = end(explode('_', $arr[1]));
			$data['get_winners'] = $this->Giveaway_model->get_ga_winner($data);
			echo json_encode($data);
			exit();
		}
	}
	public function giveaway_users($lobby_id) {
		$this->breadcrumbs->push('Home', 'admin/dashboard');
		$this->breadcrumbs->push('Giveaway List', 'admin/giveaway/');
	    $this->breadcrumbs->push('Giveaway Users', 'admin/giveaway/giveaway_users');
	    $data['active'] = 'giveaway';
	    $data['page_name'] = 'Giveaway Users List';
		$data['giveaway_all_users'] = $this->Giveaway_model->giveaway_all_users(array('lobby_id' => $lobby_id));
		$data['lobby_id'] = $lobby_id;
		$data['giveaway_id'] = $data['giveaway_all_users'][0]['giveaway_id'];
		$content = $this->load->view('admin/giveaway/giveaway_users.tpl.php',$data,true);
		$this->render($content);
	}
	public function remove_giveaway_users() {
		if (!empty($_POST['type']) && !empty($_POST['selected_guids']) && !empty($_POST['selected_user_ids']) && !empty($_POST['lobby_id'])) {
			
			$selected_guids = $_POST['selected_user_ids'];
			$user_ids = explode(',', $selected_guids);
			$guids_ids = explode(',', $_POST['selected_guids']);
			$lobby_id = $_POST['lobby_id'];
			
			if ($_POST['type'] == 'refund_users') {
				$get_giveaway = $this->Giveaway_model->get_ga_data(array('id' => $lobby_id))[0];

				// ==== Giveaway Fee collector code =====
				// ============== start =================
				$global_gafees = $this->General_model->view_data('site_settings', array('option_title' => 'global_giveaway_fees'))[0];
				$global_gafees = explode('_', $global_gafees['option_value']);

				$ga_feeclctr = $this->General_model->view_data('site_settings', array('option_title' => 'global_giveaway_fee_collector'))[0]['option_value'];
				$ga_feeclctr = (!empty($ga_feeclctr)) ? $ga_feeclctr : 0;
				$ga_fee_clctr_detail = $this->General_model->view_single_row('user_detail', array('account_no' => $ga_feeclctr),'*');

				$ga_deductible_fee = ($global_gafees[0] == 1) ? number_format((float) count($user_ids) * $global_gafees[1], 2, '.', '') : number_format((float) count($user_ids) * ($get_giveaway['gw_amount']*$global_gafees[1]/100), 2, '.', '');

				if ($ga_deductible_fee > $ga_fee_clctr_detail['total_balance']) {
					$this->session->set_userdata('message_err', 'Giveawy Fee Collector Account Number (#'.$ga_feeclctr.') have insufficient balance to refund.');
					redirect(base_url().'admin/giveaway/giveaway_users/'.$lobby_id);
				}
				// ============== end =================
				// ==== Giveaway Fee collector code ===


				// ==== Giveaway amount collector code ===
				// ============== start ==================
				$ga_deductible_amnt = ($get_giveaway['gw_amount'] * count($user_ids)) - $ga_deductible_fee;
				if ($ga_deductible_amnt > $get_giveaway['ga_crtr_balance']) {
					$this->session->set_userdata('message_err', 'Giveawy Amount Collector Account Number (#'.$get_giveaway['creator_acc'].') have insufficient balance to refund.');
					redirect(base_url().'admin/giveaway/giveaway_users/'.$lobby_id);
				}
				// ============== end ==================
				// ==== Giveaway amount collector code ===

				foreach ($user_ids as $key => $uid) {
					$r[] = $this->db->set("total_balance", "total_balance + ".$get_giveaway['gw_amount'], FALSE)->where('user_id', $uid)->update('user_detail');
				}
				$r[] = $this->db->set("total_balance", "total_balance - ".$ga_deductible_amnt, FALSE)->where('user_id', $get_giveaway['ga_crtr'])->update('user_detail');
				$r[] = $this->db->set("total_balance", "total_balance - ".$ga_deductible_fee, FALSE)->where('account_no', $ga_feeclctr)->update('user_detail');
			}
			$r[] = $this->db->where_in('id', $guids_ids)->delete('giveaway_users');

			(!empty($r)) ? $this->session->set_userdata('message_succ', (($_POST['type'] == 'refund_users') ? 'Refund ' : 'Reset ').'Successful') : $this->session->set_userdata('message_err', 'Failed');
			redirect(base_url().'admin/giveaway/giveaway_users/'.$lobby_id);
		}
	}
	public function reset_all_player($gw_id) {
		$r = $this->db->where_in('giveaway_id', $gw_id)->delete('giveaway_users');
		($r) ? $this->session->set_userdata('message_succ', 'Reset Successful') : $this->session->set_userdata('message_err', 'Failed');
		redirect($_SERVER['HTTP_REFERER']);
	}
	public function description($lobby_id) {
		$this->breadcrumbs->push('Home', 'admin/dashboard');
    $this->breadcrumbs->push('Giveaway List', 'admin/giveaway');
    $this->breadcrumbs->push('Giveaway Description', 'admin/giveaway/description');
    $data['active'] = 'giveaway';
    $data['page_name'] = 'Giveaway Description of Prize & Rules';
		$data['giveaway_detail'] = $this->Giveaway_model->get_ga_data(array('id' => $lobby_id))[0];
		$content = $this->load->view('admin/giveaway/giveaway_description.tpl.php',$data,true);
		$this->render($content);
	}
  public function reset_giveaway($lobby_id) {
    $update_data['reset'] = 1;
    $updated_where['lobby_id'] = $lobby_id;
    $r = $this->General_model->update_data('giveaway', $update_data, $updated_where);
    ($r) ? $this->session->set_userdata('message_succ', 'Giveaway Reset successfully.') : $this->session->set_userdata('message_err', 'Unable to Reset');
    redirect($_SERVER['HTTP_REFERER']);
  }
	public function update_ga_fee(){
	  $global_gafees = $this->General_model->view_data('site_settings', array('option_title' => 'global_giveaway_fees'));
    $insert_data = array('option_value' => $_REQUEST['default_fee_collect_account_no']);
		$insert_data = array('option_value' => $_REQUEST['type'].'_'.$_REQUEST['event_fee']);
	  if (empty($global_gafees)) {
	  	$insert_data['option_title'] = 'global_giveaway_fees';
	  	$this->General_model->insert_data('site_settings', $insert_data);
	  } else {
	  	$this->General_model->update_data('site_settings', $insert_data, array('option_title' => 'global_giveaway_fees'));
	  }

	  $ga_fee_collector = $this->General_model->view_data('site_settings', array('option_title' => 'global_giveaway_fee_collector'))[0];
    
    if (empty($ga_fee_collector))
    	$this->General_model->insert_data('site_settings', array('option_title' => 'global_giveaway_fee_collector', 'option_value' => $_REQUEST['default_fee_collect_account_no']));
    else
    	$this->General_model->update_data('site_settings', array('option_value' => $_REQUEST['default_fee_collect_account_no']), array('option_title' => 'global_giveaway_fee_collector'));

		// $this->General_model->update_data('site_settings',array('option_value' => $_REQUEST['default_event_account_no']),array('option_title' => 'default_event_account_no'));
		// $this->General_model->update_data('site_settings',array('option_value' => $_REQUEST['default_fee_collect_account_no']),array('option_title' => 'default_fee_collect_account_no'));
		$this->session->set_userdata('message_succ', 'Global Giveaway Fee updated successfully.');
		redirect($_SERVER['HTTP_REFERER']);
	}
}