<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Live_lobby extends My_Controller{
	function __construct(){
        parent::__construct();
        $this->load->model('User_model');
        $this->target_dir=BASEPATH.'../upload/game/';
        $this->target_add_sponsered_by_logo_dir=BASEPATH.'../upload/ads_sponsered_by_logo/';
        date_default_timezone_set('America/Los_Angeles');
    }
    function get_grp_bet_admin_message(){
        $id = $_REQUEST['id'];
        echo $this->General_model->get_lobby_admin_data($id);
    }
    public function index(){
    	$this->check_user_page_access();
    	$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Live Lobby List', 'admin/live_lobby');
        $data['active']='lvlby_bet_url';
        $data['page_name']='Live Lobby List';
        $data['list']=$this->General_model->backend_game_list();
        $content=$this->load->view('admin/game/admin_game_list.tpl.php',$data,true);
        $this->render($content);
    }
    public function group_bet_url() {
    	$this->check_user_page_access();
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Live Lobby List', 'admin/live_lobby/group_bet_url');
    	$data['list']=$this->General_model->lobby_groupbeturl_info();
        $data['active']='lvlby_bet_url';
        $data['page_name']='Live Lobby List';
        $data['page_name1']='Live Lobby Result List';
        $content=$this->load->view('admin/live_lobby/watch_live_lobby_list.tpl.php',$data,true);
        $this->render($content);
    }
    public function lobby_report_timer() {
        $this->check_user_page_access();
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Live Lobby Report Timer', 'admin/live_lobby/lobby_report_timer');
        $data['data']=$this->General_model->lobby_report_timer();
        $data['active']='lobby_report_timer';
        $data['page_name']='Live Lobby Report Timer';
        $content=$this->load->view('admin/live_lobby/live_lobby_report_timer.tpl.php',$data,true);
        $this->render($content);
    }
    public function lobby_report_timer_update() {
        $this->check_user_page_access();
        if (isset($_POST) && isset($_POST['hours']) && $_POST['hours'] !='' && isset($_POST['minutes']) && $_POST['minutes'] !='' ) {
            $update_data = array(
                'minutes' =>  $_POST['minutes'],
                'hours' =>  $_POST['hours'],
            );
            $r = $this->General_model->update_data('live_lobby_report_timer',$update_data,array('id'=>1));
        }
        if ($r) {
            $this->session->set_userdata('status_msg', 'Update Successfull');
            redirect(base_url().'admin/live_lobby/lobby_report_timer');
        }
    }
    public function grpbetURLDelete($grp_bet_id){
        $group_bet_details       =   $this->General_model->view_data('group_bet',array('id'=>$grp_bet_id));
        $get_lobby_info   =   $this->General_model->view_data('lobby',array('id'=>$group_bet_details[0]['lobby_id']));
        $get_price          =   $get_lobby_info[0]['price'];

        $lobby_creat_fee = $this->General_model->view_all_data('admin_lobby_fee', 'id', 'asc');
        $fee_per_game = $lobby_creat_fee[0]['fee_per_game'];
        $price_add = $get_price + $fee_per_game;
        $price_add_decimal = number_format((float)$price_add, 2, '.', '');
        
        $options = array(
            'select' => 'lg.user_id',
            'table' => 'group_bet gb',
            'join' => array('lobby_group lg' => 'lg.group_bet_id = gb.id'),
            'where' => array('lg.group_bet_id'=>$grp_bet_id,'gb.bet_status' => 2)
        );
        $lobby_bet_get_grp = $this->General_model->commonGet($options);
        foreach ($lobby_bet_get_grp as $key => $value) {
            $get_balance = $this->General_model->view_data('user_detail',array('user_id'=>$value->user_id));
            $update_total_balance = $get_balance[0]['total_balance'] + $price_add_decimal;
            $update_data = array(
                'total_balance' =>  $update_total_balance,
            );
            $r = $this->General_model->update_data('user_detail',$update_data,array('user_id'=>$value->user_id));
        }
        $data=array(
            'bet_status'=> 0,
        );
        $this->General_model->update_data('group_bet',$data,array('id'=>$grp_bet_id));
        $this->session->set_userdata('status_msg', 'Delete Successfull');
        redirect(base_url().'admin/live_lobby/group_bet_url');
    }
    public function changeStatus($v_id,$grp_bet_id,$winner_tab,$status) {
        $this->check_user_page_access();
        $arr_bet_id = $this->General_model->view_data('group_bet',array('id'=>$grp_bet_id));
        $arr_bet_data = $this->General_model->view_data('lobby',array('id'=>$arr_bet_id[0]['lobby_id']));
        $check_lobby_group_table_won_lose = $this->General_model->lobby_url_info_admin($grp_bet_id);
        $listuser   = explode(',',$check_lobby_group_table_won_lose->bet_user_table);
        $listvideo  = explode(',',$check_lobby_group_table_won_lose->lv_video_id);
        $user_index = array_search($winner_tab,$listuser); 
        $another_table = 'LEFT';
        if ($winner_tab == 'LEFT') {
            $another_table = 'RIGHT';    
        }
        if($winner_tab == $status){
            $dt=array(
                'winner_table' => $winner_tab,
                'loser_table' => $another_table,
                'bet_status' => 3,
                'video_id' => $listvideo[$user_index],
                'win_lose_deta'=>date('Y-m-d H:i:s',time()),
            );
            $r = $this->General_model->update_data('group_bet',$dt,array('id'=>$grp_bet_id));
            $lobby_bet_winner_list = $this->General_model->view_data('lobby_group',array('table_cat' => $dt['winner_table'], 'group_bet_id' => $grp_bet_id, 'status' => 1));
            foreach($lobby_bet_winner_list as $key => $list){
                $user_data_where = array('user_id' => $list['user_id']);
                $userListDp = $this->General_model->view_data('user_detail',$user_data_where);
                foreach ($userListDp as $key => $value) {
                    $update_user = array('total_balance' => $value['total_balance'] + (2*$arr_bet_data[0]['price']));
                    $this->General_model->update_data('user_detail',$update_user,$user_data_where);
                }
            }
        } else { 
            $dt=array(
                'winner_table'=>$another_table,
                'loser_table'=>$winner_tab,
                'bet_status'=>3,
                'video_id'=>$listvideo[$user_index],
                'win_lose_deta'=>date('Y-m-d H:i:s',time())
            );
            $r = $this->General_model->update_data('group_bet',$dt,array('id'=>$grp_bet_id));
            $lobby_bet_winner_list = $this->General_model->view_data('lobby_group',array(
                'table_cat' => $dt['winner_table'],
                'group_bet_id' => $grp_bet_id, 'status' => 1
            ));
            foreach($lobby_bet_winner_list as $key => $list){
                $user_data_where = array('user_id' => $list['user_id']);
                $userListDp = $this->General_model->view_data('user_detail',$user_data_where);
                foreach ($userListDp as $key => $value) {
                    $update_user = array('total_balance' => $value['total_balance'] + (2*$arr_bet_data[0]['price']));
                    $this->General_model->update_data('user_detail',$update_user,$user_data_where);
                 }
            }
        }
        
        if($r) {
            $this->session->set_flashdata('status_msg', '<p style="color:green">Status Updated successfull</p>');
            redirect(base_url().'admin/live_lobby/group_bet_url');
            
            
        } else {
            $this->session->set_flashdata('status_msg', '<p style="color:red">Failure to update</p>');
            redirect(base_url().'admin/live_lobby/group_bet_url');
        }
    }
    public function live_lobby_list(){
       $this->check_user_page_access(); 
       $this->breadcrumbs->push('Home', 'admin/dashboard');
       $this->breadcrumbs->push('Live Lobby List', 'admin/live_lobby/live_lobby_list');
       $data['active']='live_lobby_list';
       $data['page_name']='Live Lobby List';
       $options = array(
        'select' => 'l.*, ag.game_name, ud.account_no as creator_accno',
        'table' => 'lobby l',
        'join' => array('admin_game ag' => 'ag.id = l.game_id', 'user_detail ud' => 'ud.user_id = l.user_id'),
        'where' => array('l.status' => 1, 'l.payment_status' => 1,'l.is_archive' => 0),
        'order_by' => array('l.lobby_order' => 'asc')
        );
       $data['live_lobby_list'] = $this->General_model->commonGet($options);
       $content=$this->load->view('admin/live_lobby/live_lobby_list.tpl.php',$data,true);
       $this->render($content);
    }
    public function delete_lobby_data($id){
        if ($id != '') {
            $data_value = '1';
            $r = $this->General_model->update_data('group_bet', array('bet_status' => 1), array('lobby_id' => $id));
            $r1 = $this->General_model->update_data('lobby', array('status' => 0), array('id' => $id));
            ($r && $r1) ? $this->session->set_userdata('message_succ', 'Delete Successfull') : $this->session->set_userdata('message_err', 'Failure to delete');
            redirect($_SERVER['HTTP_REFERER']);

            // $this->db->select('*');
            // $this->db->from('group_bet');
            // $this->db->where('lobby_id',$id);
            // $group_bet_data = $this->db->get()->result_array();
            // foreach ($group_bet_data as $key => $group_bet) {
            //     $this->db->delete('lobby_group',array('group_bet_id'=>$group_bet['id']));
            // }
            // $this->db->delete('group_bet', array('lobby_id' => $id));
            // $this->db->delete('lobby_group_conversation', array('lobby_id' => $id));
            // $this->db->delete('lobby_fans', array('lobby_id' => $id));
            // $this->db->delete('ads_tbl', array('id' => $id));
            // $this->db->delete('lobby_video', array('lobby_id' => $id));
            // $r = $this->db->delete('lobby', array('id' => $id));
            // ($r) ? $this->session->set_userdata('message_succ', 'Successfull Deleted') : $this->session->set_userdata('message_err', 'Failure to Delete');
            // redirect(base_url().'admin/live_lobby/live_lobby_list/');
        }
    }
    public function updateStatusPasswordRequire() {
        $id=$_REQUEST['id'];
        $type = $_REQUEST['type'];
        echo '<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form action="#" id="status_update" name="security_id" method="POST" >
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Admin Password</label>
                                <input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
                                <input type="hidden" id="userid" class="form-control" name="userid" value="'.$id.'" required>
                                <input type="hidden" id="userid" class="form-control" name="type" value="'.$type.'" required>
                            </div>
                        </div>
                        <div class="box-footer">
                            <input class="btn btn-primary" type="submit" value="Submit" >
                            <button type="button" class="btn btn-info pull-right" data-dismiss="modal">Close</button> 
                        </div>
                    </form>
                </div>
            </div>';

    }
    public function updateLiveLobbyStatus() {
        $security_password  = $_REQUEST['security_password'];
        $userid             = $_REQUEST['userid'];
        $security_password_db = $this->General_model->view_all_data('admin_security_password','id','asc');
        if ($security_password == $security_password_db[0]['password']) {
            $id = $_REQUEST['userid'];
            $data_value = '1';
            $status   = $_REQUEST['type'];
            $data=array(
                'status'=>$status,
            );
            $this->General_model->update_data('lobby',$data,array('id'=>$id));
        } else {
            $data_value = '<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form action="#" id="security_id" name="security_id" method="POST" >
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Admin Password</label>
                                <input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
                                <input type="hidden" id="userid" class="form-control" name="userid" value="'.$userid.'" required>
                            </div>
                        </div>
                        <div class="box-footer">
                            <input class="btn btn-primary" type="submit" value="Submit" >
                        </div>
                    </form>
                </div>
            </div>          
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>      
            </div>';
        }
        echo $data_value;
        $result['data_value'] = $data_value;
        return $result;
    }
    public function ChangeLobbyListOrder() {
        $id = $_REQUEST['id'];
        $lobby_data = $this->General_model->view_data('lobby',array('id'=>$id));
        $data_value = '<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Lobby Order   '.$lobby_data[0]['lobby_order'].'</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <form action="#" id="change_lobby_order" name="change_lobby_order" method="POST">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Change Lobby Order </label>
                            <input type="hidden" id="lobby_id" class="form-control" name="lobby_id" value="'.$id.'" required>
                            <input type="text" id="new_order" class="form-control" name="new_order" value="" required>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input class="btn btn-primary" type="submit" value="Submit" >
                        <button type="button" class="btn btn-info pull-right" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>';
        echo $data_value;
    }
    public function UpdateLobbyListOrder(){
        $lobby_id   = $_REQUEST['lobby_id'];
        $new_order  = $_REQUEST['new_order'];
        $lobby_data = $this->General_model->view_data('lobby',array('id'=>$this->input->post('lobby_id', TRUE)));

        if ($lobby_data[0]['lobby_order'] != $_REQUEST['new_order']) {
            $this->General_model->update_data('lobby',array('lobby_order' => $lobby_data[0]['lobby_order']),array('lobby_order'=>$_REQUEST['new_order']));
        } 
        $data['lobby_order'] = $new_order;
        $this->General_model->update_data('lobby',$data,array('id'=>$lobby_id));
    }
    
}