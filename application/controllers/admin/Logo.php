<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include BASEPATH.'../application/controllers/admin/My_Controller.php';

class Logo extends My_Controller 

{

  	function __construct()

    {

	parent::__construct();

    $this->target_dir=BASEPATH.'../upload/logo/';

	}



	public function index()

	{

		$this->check_user_page_access();

		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('Logo List', 'admin/logo');



		$data['active']='logo_list';

		$data['page_name']='Logo List';

		

		$data['list']=$this->General_model->view_all_data('logo','id','asc');

		$content=$this->load->view('admin/logo/logo_list.tpl.php',$data,true);

		$this->render($content);

	}

	//load edit page

	public function logoEdit($id='-1')

	{

		$this->check_user_page_access();

		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('Logo List', 'admin/logo');

		$this->breadcrumbs->push('Edit Logo', 'admin/logo/logoEdit');



		$data['active']='logo_edit';

		$data['page_name']='Edit Page';



		$data['logo_data']=$this->General_model->view_data('logo',array('id'=>$id));

		$content=$this->load->view('admin/logo/logo_edit.tpl.php',$data,true);

		$this->render($content);

	}

	//edit 

	public function logoUpdate()

	{

		$this->check_user_page_access();

		//$data['active']='game_edit';

		$img=$this->input->post('old_image', TRUE);

		if(isset($_FILES["image"]["name"]) && $_FILES["image"]["name"]!='')

		{

			$img=time().basename($_FILES["image"]["name"]);
			$ext = end((explode(".", $img)));
		
			if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg')
			{

				$target_file = $this->target_dir.$img;

		        if(move_uploaded_file($_FILES["image"]["tmp_name"], $target_file))

				{

					if(file_exists($this->target_dir.$this->input->post('old_image', TRUE)))

					{

						unlink($this->target_dir.$this->input->post('old_image', TRUE));

					}

				}
			}
			else 
			{

				$this->session->set_flashdata('msg', 'This is not a image file');	
				redirect(base_url().'admin/logo/logoEdit/'.$this->input->post('id', TRUE));
			}

		}

		$data=array(

			'image'=>$img
		 );

		 $r=$this->General_model->update_data('logo',$data,array('id'=>$this->input->post('id', TRUE)));

		if($r)

		{

			$this->session->set_userdata('message_succ', 'Update successfull');	

			redirect(base_url().'admin/logo/logoEdit/'.$this->input->post('id', TRUE));

		}

		else

		{

			$this->session->set_userdata('message_err', 'Failure to update');	

			redirect(base_url().'admin/logo/logoEdit/'.$this->input->post('id', TRUE));

		}	

	}
	
}

