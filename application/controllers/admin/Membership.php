<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Membership extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->target_dir=BASEPATH.'../upload/membership/';
	}
	public function index(){
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
		$this->breadcrumbs->push('Pro Esports Membership List', 'admin/membership');

		$data['active']='pro_esports_membership';
		$data['page_name']='Pro Esports Membership List';

		$data['list']=$this->General_model->view_all_data('membership','id','desc');

		$this->db->select('pms.*, ud.account_no, ud.name');
		$this->db->from('player_membership_subscriptions pms');
		$this->db->join('user_detail ud','ud.user_id = pms.user_id');
		$this->db->where('pms.payment_status','Active');
		$this->db->order_by('pms.id',DESC);
		$data['plyer_list'] = $this->db->get()->result_array();

		$data['cancelled_plyer_list'] = $this->General_model->view_data('player_membership_subscriptions',array('payment_status' => 'Inactive'));
		$content = $this->load->view('admin/membership/membership_list.tpl.php',$data,true);
		$this->render($content);
	}
	public function addMembership() {
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
	    $this->breadcrumbs->push('Pro Esports Membership List', 'admin/membership');
	    $this->breadcrumbs->push('Add Membership', 'admin/membership/addMembership');

		$data['active']='pro_esports_membership';
		$data['page_name']='Membership Add';

		$data['max_lobby_creation_limit'] = $this->db->select_max('lobby_creation_limit')->from('membership')->get()->row()->lobby_creation_limit;
		$data['max_lobby_joining_limit'] = $this->db->select_max('lobby_joining_limit')->from('membership')->get()->row()->lobby_joining_limit;
		$content=$this->load->view('admin/membership/membership_create.tpl.php',$data,true);
		$this->render($content);
	}
	public function create(){
		$this->check_user_page_access();
		if (!empty($_FILES["image"])) {
		  	$data = array(
		  		'title'=>$this->input->post('title',TRUE),
					'status' => '1',
					'description' => $this->input->post('description',TRUE),
					'amount' => $this->input->post('amount',TRUE),
					'fees' => $this->input->post('fees',TRUE),
					'terms_condition' => $this->input->post('terms_condition',TRUE),
					'start_date' => date('Y-m-d h:i'),
					'expire_date' => date('Y-m-d h:i'),
					'time_duration' => $this->input->post('time_duration',TRUE),
					'lobby_creation_limit' => $this->input->post('creation_limit',TRUE),
					'lobby_joining_limit' => $this->input->post('joining_limit',TRUE),
					'stream_record_limit' => $this->input->post('stream_record_limit',TRUE),
				);
		  	if ($_POST['set_discount'] == 'on') {
		  		if ($_POST['free_ticket'] == 'on') {
			  		$data['tournament_ticket_discount_type'] = '3';
			  		$data['tournament_ticket_discount_amount'] = '0';
		  		} else {
		  			$data['tournament_ticket_discount_type'] = $_POST['discnt_type'];
			  		$data['tournament_ticket_discount_amount'] = $_POST['discout_amount'];
			  		if ($data['tournament_ticket_discount_type'] == '2' && $data['tournament_ticket_discount_amount'] > 100) {
						$this->session->set_userdata('message_err', 'Discount percentage exceeded maximum limit');
		  				redirect(base_url().'admin/membership/bannerAdd');
		  			}
		  		}
		  	} else {
		  		$data['tournament_ticket_discount_type'] = '1';
		  		$data['tournament_ticket_discount_amount'] = '0';
		  	}
			if (isset($_FILES["image"]["name"]) && $_FILES["image"]["name"]!='') {
				$img = time().basename($_FILES["image"]["name"]);
				$ext = end((explode(".", $img)));

				if ($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg') {
					$target_file = $this->target_dir.$img;
					move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
				} else {
		    	$this->session->set_flashdata('msg', 'This is not a image file');
		    	redirect(base_url().'admin/membership/bannerAdd');
	    	}
	    	$data['image'] = $img;
	    }
			$r = $this->General_model->insert_data('membership',$data);
			($r) ? $this->session->set_userdata('message_succ', 'Successfull Added') : $this->session->set_userdata('message_err', 'Failure to Create');
		} else {
			$this->session->set_userdata('message_err', 'Please Upload Membership image');
		}
		redirect(base_url().'admin/membership');
	}

	public function membershipEdit($id = '-1'){
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
		$this->breadcrumbs->push('Pro Esports Membership List', 'admin/membership');
		$this->breadcrumbs->push('Edit Pro Esports Membership', 'admin/membership/membershipEdit');

		$data['active'] = 'pro_esports_membership';
		$data['page_name'] = 'Edit Pro Esports Membership';
		$data['membership_data'] = $this->General_model->view_data('membership', array('id' => $id))[0];

		$condition = array(
			'payment_status' => 'Active',
			'plan_id' => $id,
		);
		$data['plan_activated_users'] = $this->General_model->view_data('player_membership_subscriptions',$condition);

		$content = $this->load->view('admin/membership/membership_edit.tpl.php',$data,true);
		$this->render($content);
	}
	public function membership_activated_player_list($id = '-1'){
		$this->check_user_page_access();
		$data['membership_data'] = $this->General_model->view_data('membership', array('id' => $id))[0];
		
		$this->breadcrumbs->push('Home', 'admin/dashboard');
		$this->breadcrumbs->push('Pro Esports Membership List', 'admin/membership');
		$this->breadcrumbs->push('Edit Pro Esports Membership', 'admin/membership/membershipEdit/'.$id);
		$this->breadcrumbs->push($data['membership_data']['title'].' Plan activated player list', 'admin/membership/membership_activated_player_list');

		$data['active'] = 'pro_esports_membership';
		$data['page_name'] = $data['membership_data']['title'].' Plan activated player list';

		$this->db->select('pms.*, ud.account_no, ud.name');
		$this->db->from('player_membership_subscriptions pms');
		$this->db->join('user_detail ud','ud.user_id = pms.user_id');
		$this->db->where('pms.payment_status','Active');
		$this->db->where('pms.plan_id',$id);
		$this->db->order_by('pms.id',DESC);
		$data['plan_activated_users'] = $this->db->get()->result_array();

		$content = $this->load->view('admin/membership/membership_activated_player_list.tpl.php',$data,true);
		$this->render($content);
	}
	public function membership_display_order($id) {
    	$this->check_user_page_access();
    	if (!empty($_POST)) {
        	$new_order = $_POST['edit_display_order'];
        	$agc_data = $this->General_model->view_data('membership',array('id'=>$id));
        	($agc_data[0]['display_order'] != $new_order) ? $this->General_model->update_data('membership',array('display_order' => $agc_data[0]['display_order']),array('display_order' => $new_order)) : '';
        	$data['display_order'] = $new_order;
        	$update = $this->General_model->update_data('membership',$data,array('id'=>$id));
        	($update) ? $this->session->set_userdata('message_succ', 'Display order changed') : $this->session->set_userdata('message_err', 'Failure to change order');
        	redirect(base_url().'admin/membership');
    	}
	}
	public function membershipUpdate(){
		$this->check_user_page_access();		
		$trim = strip_tags($_REQUEST['editor1']);
		$trim = str_replace([" ","\n","\t","&ndash;","&rsquo;","&#39;","&quot;","&nbsp;"], '', $trim);
		$totalCharacter = strlen(trim($trim));
		if ($this->input->post('type',TRUE) == 'static' && $totalCharacter == 0 ) {
			$this->session->set_userdata('message_err', 'Please Enter Content');
			redirect(base_url().'admin/membership/membershipEdit/'.$this->input->post('id', TRUE));
		}
		$data['active']='pro_esports_membership';
		$data = array(
			'title' => $this->input->post('title',TRUE),
			'status' => '1',
			'description' => $this->input->post('description',TRUE),
			'amount' => $this->input->post('amount',TRUE),
			'fees' => $this->input->post('fees',TRUE),
			'terms_condition' => $this->input->post('terms_condition',TRUE),
			'start_date' => date('Y-m-d h:i'),
			'expire_date' => date('Y-m-d h:i'),
			'lobby_creation_limit' => $this->input->post('creation_limit',TRUE),
			'lobby_joining_limit' => $this->input->post('joining_limit',TRUE),
			'stream_record_limit' => $this->input->post('stream_record_limit',TRUE),
		);
		(!empty('time_duration')) ? $data['time_duration'] = $this->input->post('time_duration', TRUE) : '';

  	if ($_POST['set_discount'] == 'on') {
  		if ($_POST['free_ticket'] == 'on') {
	  		$data['tournament_ticket_discount_type'] = '3';
	  		$data['tournament_ticket_discount_amount'] = '0';
  		} else {
  			$data['tournament_ticket_discount_type'] = $_POST['discnt_type'];
	  		$data['tournament_ticket_discount_amount'] = $_POST['discout_amount'];
	  		if ($data['tournament_ticket_discount_type'] == '2' && $data['tournament_ticket_discount_amount'] > 100) {
				$this->session->set_userdata('message_err', 'Discount percentage exceeded maximum limit');
  				redirect(base_url().'admin/membership/membershipEdit/'.$this->input->post('id', TRUE));
  			}
  		}
  	} else {
  		$data['tournament_ticket_discount_type'] = '1';
  		$data['tournament_ticket_discount_amount'] = '0';
  	}
		
		if ((isset($_FILES["image"]["name"])) && ($_FILES["image"]["name"] != '')) {
			$img = time().basename($_FILES["image"]["name"]);
			$ext = end((explode(".", $img)));
			if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg') {
				$target_file = $this->target_dir.$img;
				if(move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
					if(file_exists($this->target_dir.$this->input->post('old_image', TRUE))) {
						unlink($this->target_dir.$this->input->post('old_image', TRUE));
					}
				}
			} else {
		    	$this->session->set_flashdata('msg', 'This is not a image file');
		    	redirect(base_url().'admin/membership/membershipEdit/'.$this->input->post('id', TRUE));
	    	}
		} else {
			$img = $this->input->post('image', TRUE);
		}
		$data['image'] = $img;

		$r = $this->General_model->update_data('membership',$data,array('id'=>$this->input->post('id', TRUE)));
		($r) ? $this->session->set_userdata('message_succ', 'Update successfull') : $this->session->set_userdata('message_err', 'Failure to update');
		redirect(base_url().'admin/membership/membershipEdit/'.$this->input->post('id', TRUE));
	}
	// public function change_player_membership($id,$amount) {
	// 	$arr = array(
 //      'PROFILEID' => $id,
 //      'AMT' => $amount
 //    );
 //    $paypal_res = $this->Paypal_payment_model->update_recurring_payments_profile($arr);
 //    $arr1['TRANSACTIONID'] = $paypal_res['PROFILEID'];
 //    $get_transaction_details = $this->Paypal_payment_model->get_transaction_details($arr1);
 //    echo "<pre>";
 //    print_r($paypal_res);
 //    print_r($get_transaction_details);
	// 	exit();
	// }
	public function delete_data($id){
		$result = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="security_password_id" name="security_password_id" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Admin Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
								<input type="hidden" id="membership_id" class="form-control" name="membership_id" value="'.$id.'" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
							<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>	
						</div>
					</form>
				</div>
			</div>';
			echo $result;
	}
	public function delete(){
		
		$security_password 	= $_REQUEST['security_password'];
		$membership_id 			= $_REQUEST['membership_id'];
			
		$security_password_db = $this->General_model->view_all_data('admin_security_password','id','asc');
		
		if($security_password == $security_password_db[0]['password'])
		{
			$r=$this->General_model->delete_data('membership',array('id'=>$membership_id));

			if($r) 
			{
				$this->session->set_userdata('message_succ', 'Delete Successfull');	
				$data_value = true;
			} else {
				$this->session->set_userdata('message_err', 'Failure to delete');	
				$data_value = false;
			}
		} else {
			$data_value['pass_err'] = 'Password Error';
			$data_value = '<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<form action="#" id="security_password_id" name="security_password_id" method="POST" >
						<div class="form-group" style="margin: 0 12px 15px;"><label style="color:red;">'.$data_value['pass_err'].'</label></div>
							<div class="box-body">
								<div class="form-group">
									<label for="name">Admin Password</label>
									<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
									<input type="hidden" id="membership_id" class="form-control" name="membership_id" value="'.$membership_id.'" required>
								</div>
							</div>
							<div class="box-footer">
								<input class="btn btn-primary" type="submit" value="Submit" >
								<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>	
							</div>
						</form>
					</div>
				</div>';
				
			}
			echo $data_value; die;
	}
	public function getMembershipPlayer(){
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
		$this->breadcrumbs->push('Players Membership List', 'admin/membership');

		$data['active']='players_membership';
		$data['page_name']='Players Membership List';		

		$condition = array('payment_status' => 'Active');
		$data['list'] = $this->General_model->view_data('player_membership_subscriptions',$condition);		
		$content=$this->load->view('admin/membership/player_membership_list.tpl.php',$data,true);
		$this->render($content);
	}
	public function membershipHistory(){
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
    $this->breadcrumbs->push('Players Membership List', 'admin/membership');
    $this->breadcrumbs->push('Players History List', 'admin/membership/membershipHistory');

		$data['active']='players_membership';
		$data['page_name']='Players History List';		

		$condition = array('user_id' => $_GET['user_id']);
		$data['list'] = $this->General_model->view_data('player_membership_subscriptions',$condition);		
		$content=$this->load->view('admin/membership/player_history_list.tpl.php',$data,true);

		$this->render($content);
	}
	public function cancel_player_membership($transaction_id) {
    $this->check_user_page_access();
    $PayPalResponseResult = $this->Paypal_payment_model->get_recurring_payments_profile_details($transaction_id);
    $subscription_opt = 'Cancel';
    if($PayPalResponseResult['status'] == 1) {
        $PayPalResult = $PayPalResponseResult['data'];
        if ($PayPalResult['STATUS'] != 'Cancelled') {
        	$this->Paypal_payment_model->Manage_recurring_payments_profile_status($transaction_id,$subscription_opt);
        }
				$r = $this->General_model->update_data('player_membership_subscriptions', array('payment_status' => 'Inactive','payment_date' => date('Y-m-d H:i:s', time())), array('transaction_id' => $transaction_id));

        ($r) ? $this->session->set_userdata('message_succ', 'Unsubscribe successfull') : $this->session->set_userdata('message_err', 'Unable to Unsubscribe');
    } else {
    	$this->session->set_userdata('message_err', 'Somthing wrong');
    }
    redirect(base_url().'admin/membership/#membership_players_plans');
	}
}

