<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Menu extends My_Controller {
	function __construct() {
		parent::__construct();
    $this->target_dir=BASEPATH.'../upload/banner/';
  }
	public function index()	{
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
		$this->breadcrumbs->push('Menu List', 'admin/menu');

		$data['active']='menu_list';
		$data['page_name']='Menu List';		

		$data['list']=$this->General_model->view_all_data('menu','id','desc');
		$content=$this->load->view('admin/menu/menu_list.tpl.php',$data,true);

		$this->render($content);
	}
	public function addMenu() {
		$this->check_user_page_access();

		$this->breadcrumbs->push('Home', 'admin/dashboard');
    $this->breadcrumbs->push('Menu List', 'admin/menu');
    $this->breadcrumbs->push('Add Menu', 'admin/menu/addMenu');

		$data['active']='menu';
		$data['page_name']='Menu Add';

		$content=$this->load->view('admin/menu/menu_create.tpl.php',$data,true);
		$this->render($content);
	}

	public function create() {
		$this->check_user_page_access();
		$data = array(
			'title' => $this->input->post('title',TRUE),
			'status' => 'deactive',
			'link' => str_replace(" ","_",strtolower($this->input->post('title',TRUE)))
		);
		$r = $this->General_model->insert_data('menu',$data);

		if($r) {
			$this->session->set_userdata('message_succ', 'successfull Added');
			redirect(base_url().'admin/menu');
		} else {
			$this->session->set_userdata('message_err', 'Failure to Create');
			redirect(base_url().'admin/menu');
		}
	}

	//load edit page
	public function menuEdit($id='-1') {
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
		$this->breadcrumbs->push('Menu List', 'admin/menu');
		$this->breadcrumbs->push('Edit Menu', 'admin/menu/menuEdit');

		$data['active']='menu_edit';
		$data['page_name']='Edit Page';

		$data['menu_data']=$this->General_model->view_data('menu',array('id'=>$id));
		$content=$this->load->view('admin/menu/menu_edit.tpl.php',$data,true);
		$this->render($content);
	}
	public function menuUpdate() {
		$this->check_user_page_access();
		$trim = strip_tags($_REQUEST['editor1']);
		$trim = str_replace([" ","\n","\t","&ndash;","&rsquo;","&#39;","&quot;","&nbsp;"], '', $trim);
		$totalCharacter = strlen(trim($trim));
    	
		if ($this->input->post('type',TRUE) == 'static' && $totalCharacter == 0 ) {
			$this->session->set_userdata('message_err', 'Please Enter Content');	
			redirect(base_url().'admin/menu/menuEdit/'.$this->input->post('id', TRUE));
		}

		$data['active']='menu_edit';
		$menu_data = $this->General_model->view_data('menu',array('id'=>$this->input->post('id', TRUE)));
		
		if ($menu_data[0]['menu_order'] != $_REQUEST['menu_order']) {
			$this->General_model->update_data('menu',array('menu_order' => $menu_data[0]['menu_order']),array('menu_order'=>$_REQUEST['menu_order']));
		}

		$data=array(
			'title'=>$this->input->post('title',TRUE),
			'menu_order'=>$this->input->post('menu_order',TRUE),
			'content'=>$_REQUEST['editor1'],
			'type'=>$this->input->post('type',TRUE),
			'position'=>$this->input->post('menu_position',TRUE)
		);
		$r = $this->General_model->update_data('menu',$data,array('id'=>$this->input->post('id', TRUE)));

		if ($r) {
			$this->session->set_userdata('message_succ', 'Update successfull');	
			redirect(base_url().'admin/menu/menuEdit/'.$this->input->post('id', TRUE));
		} else {
			$this->session->set_userdata('message_err', 'Failure to update');	
			redirect(base_url().'admin/menu/menuEdit/'.$this->input->post('id', TRUE));
		}	
	}

	public function change_to_deactive() {
		$data = array( 'status'=>'deactive' );
		$this->General_model->update_data('menu',$data,array('id'=>$_POST['menu_status_id']));
	}

	public function change_to_active() {
		$data = array(
			'status'=>'active'
		);
		$this->General_model->update_data('menu',$data,array('id'=>$_POST['manustatus_id']));
	}
	public function delete_data($id) {
		$result = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="security_password_id" name="security_password_id" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Admin Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
								<input type="hidden" id="menu_id" class="form-control" name="menu_id" value="'.$id.'" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
							<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>	
						</div>
					</form>
				</div>
			</div>';
			echo $result;
	}
	public function delete() {
		$security_password 	= $_REQUEST['security_password'];
		$menu_id 			= $_REQUEST['menu_id'];
		$security_password_db = $this->General_model->view_all_data('admin_security_password','id','asc');
		
		if($security_password == $security_password_db[0]['password']) {
			$r=$this->General_model->delete_data('menu',array('id'=>$menu_id));

			if($r) {
				$this->session->set_userdata('message_succ', 'Delete Successfull');	
				$data_value = true;
			} else {
				$this->session->set_userdata('message_err', 'Failure to delete');	
				$data_value = false;
			}
		} else {
			$data_value['pass_err'] = 'Password Error';
			$data_value = '<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="security_password_id" name="security_password_id" method="POST" >
					<div class="form-group" style="margin: 0 12px 15px;"><label style="color:red;">'.$data_value['pass_err'].'</label></div>
						<div class="box-body">
							<div class="form-group">
								<label for="name">Admin Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
								<input type="hidden" id="userid" class="form-control" name="menu_id" value="'.$menu_id.'" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
							<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>	
						</div>
					</form>
				</div>
			</div>';
		}
		echo $data_value; die;
	}
}

