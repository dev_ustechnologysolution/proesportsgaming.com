<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class My_Controller extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('General_model');
		$this->load->library('breadcrumbs');
		$this->load->library('gameimage');
		$this->load->model('User_model');
		$this->timezone();
	}
	//load view
	public function render($content) {
		$view_data = array('content' =>$content);
		$view_data['activeplayers']=$this->User_model->active_all_users();
		$view_data['totalbalance']=$this->User_model->totalbalance()['total_balance'];
		$view_data['totalfees']=$this->User_model->totalfees();
		$view_data['admin_map']=$this->User_model->admin_map_video();
		$this->load->view('admin/layout',$view_data);
	}
	// 	login status check
	protected function check_user_page_access() {
		if($this->session->userdata('admin_user_id')=='') {
			header('location:'.base_url().'admin/');
			exit;
		}
	}
	public function searchcountry() {
		$searchcountry = $this->input->post('searchcountry');
		$this->db->select('c.country_id, c.countryname, c.status');
		$this->db->from('country c');
		if ($searchcountry != '') {
			$this->db->where("c.country_id LIKE '%".$searchcountry."%' OR c.countryname LIKE '%".$searchcountry."%' OR c.sortname LIKE '%".$searchcountry."%'");
			$query_country = $this->db->get()->result();
		}
		// $countries = [];
		foreach ($query_country as $key => $value) {
			$countries = '<li><div class="country-name">'.$value->countryname.'</div><label class="switch"><input type="checkbox" name="country_status" id="'.$value->country_id.'" value="country_status"';
			if($value->status == '0') {
				$countries .= 'checked';
			}
			$countries .= '><span class="slider round"></span></label></li>';
			$countries_array[]= $countries;
		}
		$res['country'] = $countries_array;
		echo json_encode($res);
	}
	public function searchstate() {
		$searchstate = $this->input->post('searchstate');
		$this->db->select('s.*');
		$this->db->from('state s');
		$this->db->join('country c', 'c.country_id=s.country_id');
		if ($searchstate != '') {
			$this->db->where("s.stateid LIKE '%".$searchstate."%' OR s.statename LIKE '%".$searchstate."%'");
			$this->db->where('s.country_id','231');
			$this->db->where('c.status','0');
			$query_country = $this->db->get()->result();
		}
		// $countries = [];
		foreach ($query_country as $key => $value) {
			$states = '<li><div class="state-name">'.$value->statename.'</div><label class="switch"><input type="checkbox" name="state_status" id="'.$value->stateid.'" value="state_status"';
			if ($value->status == '0') {
				$states .= 'checked';
			}
			$states .= '><span class="slider round"></span></label></li>';
			$states_array[]= $states;
		}
		$res['country'] = $states_array;
		echo json_encode($res);
	}
	public function country_status_active($country_id) {
		$status = array(
			'status'	=>	'0',
		);
		$this->General_model->update_data('country',$status,array('country_id'=>$country_id));
	}
	public function country_status_dective($country_id) {
		$status=array(
			'status'	=>	'1',
		);
		$this->General_model->update_data('country',$status,array('country_id'=>$country_id));
	}
	public function state_status_active($state_id) {
		$status=array(
			'status'	=>	'0',
		);
		$this->General_model->update_data('state',$status,array('stateid'=>$state_id));
	}
	public function state_status_dective($state_id) {
		$status=array(
			'status'	=>	'1',
		);
		$this->General_model->update_data('state',$status,array('stateid'=>$state_id));
	}
	public function security_pw_auth($security_password='',$dataid='') {
		$data['result'] = $this->General_model->security_pw_auth($security_password,$dataid);
		echo json_encode($data);
		exit();
	}
	public function timezone() {
		if ($this->session->userdata('admin_user_id')) {
			$this->get_userData = $this->General_model->view_data('cre_admin_detail',array('user_id'=>$this->session->userdata('admin_user_id')));
			$arra = $this->get_userData[0];
			$arra1['admin_timezone'] = $arra['timezone'];
			$this->session->set_userdata($arra1);
		}
	    if (!empty($_COOKIE['time_zne'])) {
		    $timezone_offset_minutes = $_COOKIE['time_zne'];
		    $timezoneName = timezone_name_from_abbr('', $timezone_offset_minutes * 60, false);
		    if (!empty($this->get_userData[0]['timezone'])) {
		    	date_default_timezone_set($this->get_userData[0]['timezone']);
		    } else {
				  $dt['timezone'] = $timezoneName;
				  $this->General_model->update_data('cre_admin_detail', $dt, array('user_id' => $this->get_userData[0]['user_id']));
				  date_default_timezone_set($timezoneName);
		    }
	    }
  	}
}

