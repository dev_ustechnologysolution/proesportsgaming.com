<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class My_worker extends \yidas\queue\worker\Controller
{
    // Setting for that a listener could fork up to 10 workers
    public $workerMaxNum = 10;
    
    // Enable text log writen into specified file for listener and worker
    public $logPath = 'tmp/my-worker.log';
    protected function init()
    {
        // Optional autoload (Load your own libraries or models)
        $this->load->model('User_model');
    }
    protected function handleWork()
    {
        // Your own method to get a job from your queue in the application
        $job = $this->User_model->user_view_admin();
        
        // return `false` for job not found, which would close the worker itself.
        if (!$job)
            return false;
        
        // Your own method to process a job
        $this->processJob($job);
        
        // return `true` for job existing, which would keep handling.
        return true;
    }
    protected function processJob($job){
        echo "<pre>"; print_r($job);
    }
}