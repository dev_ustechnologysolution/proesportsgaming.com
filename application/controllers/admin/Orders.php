<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Orders extends My_Controller  {
	function __construct() {
		parent::__construct();
		$this->load->model('Product_model');
		$this->load->model('Email_model');
		$this->load->model('Order_model');
		$this->check_user_page_access();
	}
	public function index() {
		$this->breadcrumbs->push('Home', 'admin/dashboard');
		$this->breadcrumbs->push('Pro New Orders', 'admin/orders');
		$data['active'] = 'orders_list';
		$data['page_name'] = 'Pro Esports Orders List';
		$arr = array(
			'sold_by' => '0',
			'get_items' => 'yes',
			'order_status' => 'placed',
			'get_items_by_order' => 'yes'
		);
		$list = $this->Order_model->getorderitems($arr);
		$list1 = array_unique(array_column($list, 'order_id'));
		$data['list'] = array_intersect_key($list, $list1);
		$content = $this->load->view('admin/orders/orders_list.tpl.php',$data,true);
		$this->render($content);
	}
  public function completedOrders(){
  	$this->check_user_page_access();
  	$this->breadcrumbs->push('Home', 'admin/dashboard');
  	$this->breadcrumbs->push('Completed Orders', 'admin/orders/completedOrders');
		$data['active'] = 'completedOrders';
		$data['page_name'] = 'Pro Esports Completed Orders List';
		$arr = array(
			'sold_by' => '0',
			'get_items' => 'yes',
			'order_status' => 'completed',
			'get_items_by_order' => 'yes'
		);
		$list = $this->Order_model->getorderitems($arr);
		$list1 = array_unique(array_column($list, 'order_id'));
		$data['list'] = array_intersect_key($list, $list1);
		$content=$this->load->view('admin/orders/complete_orders_list.tpl.php',$data,true);
		$this->render($content);
  }

  public function player_pro_orders_list(){
  	$this->check_user_page_access();
  	$this->breadcrumbs->push('Home', 'admin/dashboard');
  	$this->breadcrumbs->push('Player Pro Orders', 'admin/orders/player_pro_orders_list');
		$data['active'] = 'player_pro_orders_list';
		$data['page_name'] = 'Player Pro Esports Orders List';
		$arr = array(
			'sold_by' => 'players',
			'get_items' => 'yes',
			'order_status' => 'placed',
			'get_items_by_order' => 'yes'
		);
		$list = $this->Order_model->getorderitems($arr);
		$list1 = array_unique(array_column($list, 'order_id'));
		$data['list'] = array_intersect_key($list, $list1);
		$content=$this->load->view('admin/orders/player_pro_orders_list.tpl.php',$data,true);
		$this->render($content);
  }
  public function canceledorders(){
  	$this->check_user_page_access();
  	$this->breadcrumbs->push('Home', 'admin/dashboard');
  	$this->breadcrumbs->push('Canceled Orders', 'admin/orders/canceledorders');
		$data['active'] = 'canceledorders';
		$data['page_name'] ='Pro Esports Canceled Orders List';
		$arr = array(
			'sold_by' => '0',
			'get_items' => 'yes',
			'order_status' => 'canceled',
			'get_items_by_order' => 'yes'
		);
		$list = $this->Order_model->getorderitems($arr);
		$list1 = array_unique(array_column($list, 'order_id'));
		$data['list'] = array_intersect_key($list, $list1);
		$content = $this->load->view('admin/orders/cancele_orders_list.tpl.php',$data,true);
		$this->render($content);
  }
  public function player_orders_list(){
  	$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
		$this->breadcrumbs->push('Player New Orders', 'admin/orders/player_orders_list');
		$data['active'] = 'player_orders_list';
		$data['page_name'] = 'Player Orders List';
		$arr = array(
			'sold_by' => 'players',
			'get_items' => 'yes',
			'order_status' => 'placed',
			'get_items_by_order' => 'yes'
		);
		$list = $this->Order_model->getorderitems($arr);
		$list1 = array_unique(array_column($list, 'order_id'));
		$data['list'] = array_intersect_key($list, $list1);
		$content=$this->load->view('admin/orders/player_orders_list.tpl.php',$data,true);
		$this->render($content);
  }
  public function player_completedOrders(){
  	$this->check_user_page_access();
  	$this->breadcrumbs->push('Home', 'admin/dashboard');
  	$this->breadcrumbs->push('Player Completed Orders', 'admin/orders/player_completedOrders');
		$data['active'] = 'player_completedOrders';
		$data['page_name'] = 'Player Completed Orders List';

		$arr = array(
			'sold_by' => 'players',
			'get_items' => 'yes',
			'order_status' => 'completed',
			'get_items_by_order' => 'yes'
		);
		$list = $this->Order_model->getorderitems($arr);
		$list1 = array_unique(array_column($list, 'order_id'));
		$data['list'] = array_intersect_key($list, $list1);
		$content=$this->load->view('admin/orders/player_complete_orders_list.tpl.php',$data,true);
		$this->render($content);
  }
  public function player_canceledorders(){
  	$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
		$this->breadcrumbs->push('Player Canceled Orders', 'admin/orders/player_canceledorders');
		$data['active'] = 'player_canceledorders';
		$data['page_name'] = 'Player Canceled Orders List';
			$arr = array(
			'sold_by' => 'players',
			'get_items' => 'yes',
			'order_status' => 'canceled',
			'get_items_by_order' => 'yes'
		);
		$list = $this->Order_model->getorderitems($arr);
		$list1 = array_unique(array_column($list, 'order_id'));
		$data['list'] = array_intersect_key($list, $list1);
		$content=$this->load->view('admin/orders/player_cancele_orders_list.tpl.php',$data,true);
		$this->render($content);
  }
  public function orderItemDetail(){
  	$sender_arr = $this->General_model->view_single_row('orders_items_tbl','id',$_GET['id']);
		$product_att = explode(',', $sender_arr['product_attributes']);
		$data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Product Details</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="change_product_order" name="change_product_order" method="POST">
						<div class="box-body">
							<div class="form-group col-md-4">
								<div >
									<img class="img img-responsive" src="'.base_url().'upload/products/'.$sender_arr['product_image'].'">
								</div>
							</div>
							<div class="form-group col-md-8">
								<h3 style="margin-top:0px;">'.$sender_arr['product_name'].'</h3>	
								<h3>$'.$sender_arr['amount'].'</h3>';
								if(count($product_att) > 0){
									$data_value .='<h3>Product Attributes</h3>';
								foreach ($product_att as $key => $value) {
									$att = explode('_', $value);
									$data_value .='<div><h4>'.$att[0].' : '.$att[1].'</h4></div>';
								}
							}
							$data_value .='</div>
						</div>
						
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
		echo $data_value;
	}

	public function reward_calculation($total_sold_items, $user_id, $total_reward, $user_level){

		$level_detail = $this->General_model->view_single_row('store_levels','level',$user_level[0]['current_level']);

		if($total_sold_items >= $level_detail['item_sold']){
	     	if(($level_detail['item_sold'] - $user_level[0]['sold_items']) >= 0) {
	     		if(($level_detail['item_sold'] - $user_level[0]['sold_items']) != 0){
	     			$lvl_items = $this->General_model->view_data('user_level_items',array('level' => $user_level[0]['current_level'], 'user_level_id'=>$user_level[0]['id']));
	     			$item_data = array(
						'user_level_id' => $user_level[0]['id'],
						'user_id' => $user_id,
						'sold_items' => (count($lvl_items) > 0) ? ($level_detail['item_sold'] - $user_level[0]['sold_items'] <= $total_sold_items) ? ($level_detail['item_sold'] - $user_level[0]['sold_items']) : ($total_sold_items - $user_level[0]['sold_items']) : $level_detail['item_sold'],
						'level' => $user_level[0]['current_level'],
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s')
					);
					$this->General_model->insert_data('user_level_items',$item_data);
					for ($i = $user_level[0]['current_level']; $i > 0 ; $i--) {
						$reward_level_detail = $this->General_model->view_single_row('store_levels','level',$i);

						$total_reward = $total_reward + ($level_detail['item_sold'] * $reward_level_detail['reward']);
						// print_r($level_detail['item_sold'] * $reward_level_detail['reward']);
					}
	     		} 
				if($user_level[0]['sold_items'] >= $level_detail['item_sold']){
					$this->General_model->update_data('user_level', array('sold_items' => $level_detail['item_sold'], 'current_level' => $user_level[0]['current_level'] + 1), array('user_id' => $user_id));
				} else {
					$this->General_model->update_data('user_level', array('sold_items' => 0,'current_level' => $user_level[0]['current_level'] + 1), array('user_id' => $user_id));
				}
	     	}
	     	$user_level = $this->General_model->view_data('user_level',array('user_id' => $user_id));
	     	$this->reward_calculation($total_sold_items - $level_detail['item_sold'], $user_id, $total_reward, $user_level);
	     	
	     	$this->db->where('user_id', $user_id);
			$this->db->set('total_balance', 'total_balance + '.$total_reward, FALSE);
			$this->db->update('user_detail');
	     }else{
			$item_data = array(
				'user_level_id' => $user_level[0]['id'],
				'user_id' => $user_id,
				'sold_items' => ($user_level[0]['sold_items'] > $total_sold_items) ? $total_sold_items : $total_sold_items - $user_level[0]['sold_items'],
				'level' => $user_level[0]['current_level'],
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			);
			$this->General_model->insert_data('user_level_items',$item_data);

	     	$this->db->where('user_id', $user_id);
			$this->db->set('sold_items', $total_sold_items, FALSE);
			$this->db->update('user_level');

	     }
	}
	
	function edit_tracking_id() {
		if (!empty($_POST)) {
			$qty = 0;
			if ($_POST['editid'] !='' && $_POST['edit_traking_id']) {
				$odata = $this->General_model->view_single_row('orders_tbl','id',$_POST['editid']);
				if(($odata['tracking_id'] == null || empty($odata['tracking_id'])) && ($_POST['item_sold_by'] !='' && $_POST['grand_total'] !='' && $_POST['ref_seller'] !='')){
					if($_POST['item_sold_by'] == '0' && $_POST['ref_seller'] != '0'){

						//Users get reward based on sum of reward of level they reached like if users is on 2nd level then they will get reward of level one and level 2 both(level 1 0.01 + level 2 0.02 = 0.03)

						$items = $this->General_model->view_data('orders_items_tbl',array('order_id' => $_POST['editid']));
						
						foreach ($items as $item) {
							$qty += $item['qty'];
						}

						$user_level = $this->General_model->view_data('user_level',array('user_id' => $_POST['ref_seller']));

						if(count($user_level) > 0 && $user_level != null){

							$total_sold_items = $user_level[0]['sold_items'] + $qty;
							$m = $this->reward_calculation($total_sold_items, $_POST['ref_seller'], $total_reward = 0, $user_level);
	       //                   if($total_sold_items >= $level_detail['item_sold']){

	       //                   	if(($level_detail['item_sold'] - $user_level[0]['sold_items']) > 0){
	       //                   		$item_data = array(
								// 		'user_level_id' => $user_level[0]['id'],
								// 		'sold_items' => ($level_detail['item_sold'] - $user_level[0]['sold_items']),
								// 		'level' => $user_level[0]['current_level'],
								// 		'created_at' => date('Y-m-d H:i:s'),
								// 		'updated_at' => date('Y-m-d H:i:s')
								// 	);

								// 	$this->General_model->insert_data('user_level_items',$item_data);
	       //                   	}
								
								// $item_data = array(
								// 	'user_level_id' => $user_level[0]['id'],
								// 	'sold_items' => $qty - ($level_detail['item_sold'] - $user_level[0]['sold_items']),
								// 	'level' => $user_level[0]['current_level'] + 1,
								// 	'created_at' => date('Y-m-d H:i:s'),
								// 	'updated_at' => date('Y-m-d H:i:s')
								// );

								// $this->General_model->insert_data('user_level_items',$item_data);

	       //                   	$this->db->where('user_id', $_POST['ref_seller']);
								// $this->db->set('sold_items', $total_sold_items, FALSE);
								// $this->db->set('current_level', $user_level[0]['current_level'] + 1, FALSE);
								// $this->db->update('user_level');

	       //                   }else{

								// $item_data = array(
								// 	'user_level_id' => $user_level[0]['id'],
								// 	'sold_items' => $qty,
								// 	'level' => $user_level[0]['current_level'],
								// 	'created_at' => date('Y-m-d H:i:s'),
								// 	'updated_at' => date('Y-m-d H:i:s')
								// );

	       //                   	$this->db->where('user_id', $_POST['ref_seller']);
								// $this->db->set('sold_items', $total_sold_items, FALSE);
								// $this->db->update('user_level');

								// $this->General_model->insert_data('user_level_items',$item_data);
	       //                   }

						}else{

							$data = array(
								'user_id' => $_POST['ref_seller'],
								'sold_items' => 0,
								'current_level' => 1,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
							);
							$this->General_model->insert_data('user_level',$data);
							$user_level = $this->General_model->view_data('user_level',array('user_id' => $_POST['ref_seller']));
							$m = $this->reward_calculation($qty, $_POST['ref_seller'], $total_reward = 0, $r);
						}
					}else{
						$global_store_fee = $this->General_model->view_single_row('site_settings','option_title','global_store_fee');
						$this->db->where('user_id', $_POST['item_sold_by']);
						$this->db->set('total_balance', 'total_balance + '.($_POST['grand_total'] - $global_store_fee['option_value']), FALSE);
						$this->db->update('user_detail');
					}
				}
				$r = $this->General_model->update_data('orders_tbl', array('tracking_id' => $_POST['edit_traking_id']), array('id' => $_POST['editid']));
				($r) ? $this->session->set_userdata('message_succ', 'Tracking info updated') : $this->session->set_userdata('message_err', 'Unable to updated Tracking info');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
	}
	public function savedata() {
		if ($_GET['id'] != '' && $_GET['action'] != '') {
			($_GET['action'] == 'canceled') ? $this->Cart_model->cancelorder(array('order_id' => $_GET['id'])) : '';
			$id = $_GET['id'];
			$fields['order_status'] = $_GET['action'];
			$fields['updated_at'] = date('Y-m-d H:i:s');
	        $update_data_where['id'] = $id;
	        $updatedata = $this->General_model->update_data('orders_tbl', $fields, $update_data_where);
	        $this->Email_model->order_action_mail(array('order_id' => $_GET['id'], 'mail_action' => $_GET['action']));
	        ($updatedata) ? $this->session->set_userdata('message_succ', 'Order status has been '.$fields['order_status']) : $this->session->set_userdata('message_err', 'Unable to updated Order status');
		    redirect($_SERVER['HTTP_REFERER']);
		    exit();
		}
    }
    public function orderdetails() {
	  	$order_id = $this->input->get('id');
	  	$this->check_user_page_access();
			$this->breadcrumbs->push('Home', 'admin/dashboard');
			$this->breadcrumbs->push('Order Details', 'admin/orders');
			$data['active'] = 'orders_list';
			$data['page_name'] = 'Orders Details';
			$data['list'] = $this->General_model->view_data('orders_items_tbl',array('order_id' => $order_id));
			$content=$this->load->view('admin/orders/orders_details.tpl.php',$data,true);
			$this->render($content);
    }
}

