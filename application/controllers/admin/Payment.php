<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Payment extends My_Controller  {
	function __construct() {
		parent::__construct();
		$this->load->model('User_model');
		$this->load->library('paypal_lib');
	}

	//page loading
	public function index($id,$betid) {

		$dat['dt']=$this->General_model->view_data('bet',array('id'=>$betid,'winner_id' =>$id,'flag'=>0));

		$dat['game_dt']=$this->General_model->view_data('admin_game',array('id' =>$dat['dt'][0]['game_id']));

		$dat['user_dt']=$this->General_model->view_data('user_detail',array('user_id' =>$dat['dt'][0]['winner_id']));

		$dat['payment_dt']=$this->General_model->view_data('payment',
		array(
			'user_id' =>$dat['dt'][0]['winner_id']
		));

		$order="orderID".time();

		$data = array(
			'user_id' => $dat['user_dt'][0]['user_id'],
			'bet_id' => $dat['dt'][0]['id'],
			'game_id' => $dat['game_dt'][0]['id'],
			'order_id' => $order,
			'receiver_email' => $dat['payment_dt'][0]['payer_email'],
			'payment_status' => 'due'
		);
		$this->General_model->insert_data('admin_payment_history',$data);
		$dat['info'] = $order;
		$content = $this->load->view('admin/payment/payment.tpl.php',$dat,true);
		$this->render($content);
	}

	public function payment_paypal(){
		//Set variables for paypal form
		$returnURL 	= $_REQUEST['return']; //payment success url
		$cancelURL 	= $_REQUEST['cancel_return'];  //payment cancel url
		$notifyURL 	= $_REQUEST['notify_url']; //ipn url
		$item_name 	= $_REQUEST['invoice'];
		$amount    	= $_REQUEST['amount'];
		$custom    	= $_REQUEST['custom'];
		$business 	= trim($_REQUEST['business']);
		$paypal_url = $_REQUEST['paypal_url'];
		$percentage = $_REQUEST['withdrawpercentage'];
		$no_shipping= $_REQUEST['no_shipping'];
		$userID 	= $this->session->userdata('user_id'); //current user id
		$logo 		= base_url().'assets/frontend/images/logo.png';
		$payment_receiver_id = $_REQUEST['receiver_id'];

		if (isset($_REQUEST['withdrawpercentage'])) {
			$dat = array(
				'collected_fees'=> $_REQUEST['withdrawamount']-$_REQUEST['amount'],
				'fees_percentage'=> $_REQUEST['withdrawpercentage']
			);
			$this->General_model->update_data('withdraw_request',$dat,array('invoice' => $_REQUEST['invoice']));
		}

		$this->paypal_lib->add_field('return', $returnURL);
		$this->paypal_lib->add_field('cancel_return', $cancelURL);
		$this->paypal_lib->add_field('notify_url', $notifyURL);
		$this->paypal_lib->add_field('item_name', $item_name);
		$this->paypal_lib->add_field('custom', $custom);
		$this->paypal_lib->add_field('user_id',  $userID);
		$this->paypal_lib->add_field('payment_receiver_id',  $payment_receiver_id);
		$this->paypal_lib->add_field('business',  $business);
		$this->paypal_lib->add_field('amount',  $amount);
		$this->paypal_lib->add_field('percentage',  $percentage);
		$this->paypal_lib->add_field('no_shipping', $no_shipping);
		$this->paypal_lib->image($logo);
		$this->paypal_lib->paypal_url = $paypal_url;
		$this->paypal_lib->paypal_auto_form();

		$paypalInfo	= $this->input->post();
		$paypalURL = $this->paypal_lib->paypal_url;		
		$result	= $this->paypal_lib->curlPost($paypalURL,$paypalInfo);
	}
	
	public function withdrawPayment($withdraw_id) {
		$order = "orderID".time();		
		$this->db->select('u.email, wr.amount_requested, ud.name ,ud.total_balance ,ud.user_id');
		$this->db->from('withdraw_request wr');
		$this->db->join('user u','u.id = wr.user_id');
		$this->db->join('user_detail ud','ud.user_id = wr.user_id');
		$this->db->where('wr.id',$withdraw_id);
		$query = $this->db->get();
		$withdraw_data = $query->result_array();
		$dat = array(
			'invoice'=> $order
		);
		$this->General_model->update_data('withdraw_request',$dat,array('id' => $withdraw_id));
		$dat['info']=$order;
		$dat['withdrawPayment'] = $withdraw_data[0];
		$content = $this->load->view('admin/payment/withdrawal.tpl.php',$dat,true);
		$this->render($content);
	}

	public function success_withrawal()	{
		if ($_GET['receiver_id'] !='' && !empty($_REQUEST)) {
			$receiver_id        = $_GET['receiver_id'];
			$invoice            = (isset($_REQUEST['item_name1']) && !empty($_REQUEST['item_name1'])) ? $_REQUEST['item_name1'] : $_REQUEST['item_name'];
			$item_transaction   = $_REQUEST['txn_id']; // Paypal transaction ID
			$item_currency      = $_REQUEST['mc_currency'];
			$payer_email      	= $_REQUEST['payer_email'];
			// $paid_to_email     	= $_REQUEST['business'];
			//update data when admin pay payment
			$reqData = $this->General_model->view_data('withdraw_request',array('invoice'=>$invoice));
			$item_price         = $reqData[0]['amount_requested']; // Paypal received amount
			$dat = array(
				'transaction_id' => $item_transaction,
				'amount_paid' => $item_price,
				'currency' => $item_currency,
				'paid_date' => date('Y-m-d h:i:s'),
				'payer_email' => trim($payer_email),
				// 'paid_to_email' => $paid_to_email,
				'reset' => 1,
				'status' => 1
			);
			(!empty($_REQUEST['business'])) ? $dat['paid_to_email'] = trim($_REQUEST['business']) : '';
			$this->General_model->paypal_fallback_data(json_encode($_REQUEST), 'Withdrawal Request Paid');

			$r = $this->General_model->update_data('withdraw_request',$dat,array('invoice' => $invoice));
			if ($r) {
				$userData = $this->General_model->view_data('user_detail',array('user_id' => $receiver_id));
				$totBalance = $userData[0]['total_balance'] - $item_price;
				$updatedbal['total_balance'] = $totBalance;
        		$this->General_model->update_data('user_detail', $updatedbal, array('user_id' => $receiver_id));
        		$get_userData = $this->General_model->view_data('cre_admin_detail', array('user_id' => $_REQUEST['custom']))[0];
        		$cre_admin_userData = $this->General_model->view_data('cre_admin_user', array('id' => $_REQUEST['custom']))[0];
			    $session_data = array(
			    	'admin_user_id' => $get_userData['user_id'],
		            'admin_user_name' => $get_userData['name'],
		            'admin_user_role_id' => $cre_admin_userData['role_id'],
					'img' => $get_userData['img'],
		        );
		        $this->session->set_userdata($session_data);
        
				$data['dt'] = 'Payment Successful';
				$content = $this->load->view('admin/payment/payment_success.tpl.php',$data,true);
				$this->render($content);
			}
		}
	}
	public function failure() {
		$data['dt'] = 'Payment Fail';
		$content = $this->load->view('admin/payment/payment_failure.tpl.php',$data,true);
		$this->render($content);
	}

	public function history() {
		$data['active']='payment_list';
		$data['page_name']='Admin Payment List';
		$data['payment_history_data'] = $this->User_model->payment_history();
		$content = $this->load->view('admin/payment/payment_history_list.tpl.php',$data,true);
		$this->render($content);
	}

	public function success() {

		// $item_no            = $_REQUEST['item_number'];

		$item_transaction   = $_REQUEST['txn_id']; // Paypal transaction ID

		$item_price         = (isset($_REQUEST['mc_gross_1']) && !empty($_REQUEST['mc_gross_1'])) ? $_REQUEST['mc_gross_1'] : $_REQUEST['mc_gross'];
		// $item_price         = $_REQUEST['mc_gross']; // Paypal received amount

		$item_currency      = $_REQUEST['mc_currency'];

		$payer_email      = $_REQUEST['payer_email'];

		//update data when admin pay payment

		$dat=array(

			'transaction_id'=>$item_transaction,

			'amount'=>$item_price,

			'currency'=>$item_currency,

			'payment_date'=>date('Y-m-d'),

			'payer_email'=>$payer_email,

			'payment_status'=>'completed'

		);

		$r = $this->General_model->update_data('admin_payment_history',$dat,array('order_id' => $this->input->post('invoice',TRUE)));
		$dt = array( 
			'flag' => 1
		);
		$this->General_model->update_data('bet',$dt,array('id' => $this->input->post('custom',TRUE)));

		$data['dt']='Payment Successful';

	
		$content=$this->load->view('admin/payment/payment_success.tpl.php',$data,true);

		$this->render($content);

	}

	public function paymentHistory() {

		$data['active']='user_payment_list';

		$data['page_name']='User Payment List';

		$data['user_payment_history_data'] = $this->User_model->user_payment_history();

		$content = $this->load->view('admin/payment/user_payment_history_list.tpl.php',$data,true);
		
		$this->render($content);
	}
	public function edittotalbalance_security_data($id){
		echo '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="balance_id" name="balance_id" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Admin Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
								<input type="hidden" id="old_fees" class="form-control" name="old_fees" value="'.$id.'" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
	}
	public function edittotalfees_security_data($id){
		echo '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="fees_id" name="fees_id" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Admin Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
								<input type="hidden" id="old_balance" class="form-control" name="old_balance" value="'.$id.'" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
	}

	public function edit_balance()
	{
		$security_password 	= $_REQUEST['security_password'];
		$old_fees = $_REQUEST['old_fees'];
		$security_password_db = $this->General_model->view_all_data('admin_security_password','id','asc');
		if($security_password == $security_password_db[0]['password']){
			
			$view_data['totalbalance']=$this->User_model->totalbalance();

			$data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Current Total Balance :-  '.$view_data['totalbalance'].'</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="editbalance_id" name="editbalance_id" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Edit Total Balance</label>
								<input type="text" id="total_balance" class="form-control" name="total_balance" value="" required>
								<input type="hidden" id="old_fees" class="form-control" name="old_fees" value="'.$old_fees.'">
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
		} else{
			$data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="balance_id" name="balance_id" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Admin Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
								<input type="hidden" id="old_fees" class="form-control" name="old_fees" value="'.$old_fees.'">
							</div>
								<label for="name">Password Error</label>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
		}	
		echo $data_value;
	}
	public function edit_fees()
	{
		$security_password 	= $_REQUEST['security_password'];
		$old_balance = $_REQUEST['old_balance'];
		$security_password_db = $this->General_model->view_all_data('admin_security_password','id','asc');
		if($security_password == $security_password_db[0]['password']){
			
			$view_data['totalfees']=$this->User_model->totalfees();

			$data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Current Total Fees :-  '.$view_data['totalfees'].'</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="editfees_id" name="editfees_id" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Edit Total Fees</label>
								<input type="text" id="total_fees" class="form-control" name="total_fees" value="" required>
								<input type="hidden" id="old_balance" class="form-control" name="old_balance" value="'.$old_balance.'" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
		} else{
			$data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="fees_id" name="fees_id" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Admin Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
								<input type="hidden" id="userid" class="form-control" name="userid" value="'.$userid.'" required>
								<input type="hidden" id="old_balance" class="form-control" name="old_balance" value="'.$old_balance.'" required>
							</div>
								<label for="name">Password Error</label>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
		}	
		echo $data_value;
	}
	public function update_balance()
	{
		$balance = $_REQUEST['total_balance'];
		$old_fees= $_REQUEST['old_fees'];
		$data=array(
			'total_fees'=>$old_fees,
			'total_amount'=>$balance,
			'date'=>date('Y-m-d H:i:s')
		);
		$r=$this->General_model->insert_data('admin_amount',$data);

	}
	public function update_fees()
	{
		$fees 	= $_REQUEST['total_fees'];
		$balance = $_REQUEST['old_balance'];
		$data=array(
			'total_fees'=>$fees,
			'total_amount'=>$balance,
			'date'=>date('Y-m-d H:i:s')
		);
		$r=$this->General_model->insert_data('admin_amount',$data);

	}
}