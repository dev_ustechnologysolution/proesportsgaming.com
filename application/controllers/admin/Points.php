<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Points extends My_Controller
{
		function __construct()
	    {
	        parent::__construct();
					$this->load->model('Admin_user_model');
	    }
	    // List all packages
        public function index() {
        		$this->check_user_page_access();
        		// add breadcrumbs
		        $this->breadcrumbs->push('Home', 'admin/dashboard');
		        $this->breadcrumbs->push('Packages List', 'admin/points');
		        $data['active']='package_list';
		        $data['page_name']='Packages List';
                $data['package_data']=$this->General_model->view_all_data('admin_packages','id','asc');
        		$content=$this->load->view('admin/package/package.tpl.php',$data,true);
        		$this->render($content);
        }

        // Load Add Package Page
        public function addPackage() {
    		$this->check_user_page_access();
    		// add breadcrumbs
	        $this->breadcrumbs->push('Home', 'admin/dashboard');
	        $this->breadcrumbs->push('Packages List', 'admin/points');
	        $this->breadcrumbs->push('Add Package', 'add_package');
	        $data['active']='add_package';
        	$data['page_name']='Add Package';
	        $content=$this->load->view('admin/package/add_package.tpl.php',$data,true);
	        $this->render($content);
    	}

    	// Store Package data
    	public function add_package(){
    		$this->check_user_page_access();
    		// $percentage = ($this->input->post('percentage', TRUE)*100)/$this->input->post('amount', TRUE);
    		$percentage = $this->input->post('percentage', TRUE);
	        $data=array(
	            'point_title'=>$this->input->post('title', TRUE),
	            'point_description'=>$this->input->post('description', TRUE),
	            'amount'=>$this->input->post('amount', TRUE),
	            'points'=>'',
	            'percentage'=>$percentage,
	            'created_at'=>date("Y-m-d h:i:s"),
	        );
	        if($this->input->post('title', TRUE)!='') {
	            //insert game data into admin game table
	            $r=$this->General_model->insert_data('admin_packages',$data);

	            if($r) {
	                $this->session->set_userdata('message_succ', 'Successfully added');
	                redirect(base_url().'admin/points');
	            } else {
	                $this->session->set_userdata('message_err', 'Failure to add');
	                redirect(base_url().'admin/points');
	            }
	        }
	        else {
	            header('location:'.base_url().'admin/points');
	        }
    	}
    	public function datachange() {
    		$package_data=$this->General_model->view_all_data('admin_packages','id','asc');
    		foreach ($package_data as $key => $pd) {
    			$fee = $pd['amount'] * ($pd['percentage']/100);
    			$data=array(
		            'percentage'=>$fee
		        );
		        $this->General_model->update_data('admin_packages',$data,array('id'=>$pd['id']));
    		}
    		exit();
    	}
    	// Load Edit Package Page
	    public function packageEdit($id='-1') {
	        $this->check_user_page_access();

	        // add breadcrumbs
	        $this->breadcrumbs->push('Home', 'admin/dashboard');
	        $this->breadcrumbs->push('Packages List', 'admin/points');
	        $this->breadcrumbs->push('Edit Package', 'edit_package');
	        $data['active']='edit_package';
	        $data['page_name']='Edit Package';

	        //show data from admin package table
	        $data['package']=$this->General_model->view_data('admin_packages',array('id'=>$id));
	        $content=$this->load->view('admin/package/edit_package.tpl.php',$data,true);
	        $this->render($content);
	    }

	    // Update Package data
	    public function edit_package() 	    {
	        $this->check_user_page_access();
	        $data['active']='edit_package';

	        //package info update into admin package table
	        // $percentage = ($this->input->post('percentage', TRUE)*100)/$this->input->post('amount', TRUE);
	        $percentage = $this->input->post('percentage', TRUE);
	        $data=array(
	            'point_title'=>$this->input->post('title', TRUE),
	            'point_description'=>$this->input->post('description', TRUE),
	            'amount'=>$this->input->post('amount', TRUE),
	            'points'=>$this->input->post('points', TRUE),
	            'percentage'=>$percentage,
	            'created_at'=>date("Y-m-d h:i:s"),
	        );

	        $r=$this->General_model->update_data('admin_packages',$data,array('id'=>$this->input->post('id', TRUE)));
	        if($r) {
	            $this->session->set_userdata('message_succ', 'Update successfull');
	            header('location:'.base_url().'admin/points/packageEdit/'.$this->input->post('id', TRUE));
	        } else {
	            $this->session->set_userdata('message_err', 'Failure to update');
	            header('location:'.base_url().'admin/points/packageEdit/'.$this->input->post('id', TRUE));
	        }
	    }

	    // Delete Package
	    public function delete($id) {
	        $this->check_user_page_access();
	        $r=$this->General_model->delete_data('admin_packages',array('id'=>$id));
	        if($r) {
	            $this->session->set_userdata('message_succ', 'Delete Successfull');
	            header('location:'.base_url().'admin/points');
	        } else {
	            $this->session->set_userdata('message_err', 'Failure to delete');
	            header('location:'.base_url().'admin/points');
	        }
	    }

	    // Load Point Percentage Page
	    public function pointPercentage($id='1'){
	    	$this->check_user_page_access();
    		// add breadcrumbs
	        $this->breadcrumbs->push('Home', 'admin/dashboard');
	        $this->breadcrumbs->push('Manuel Credit Purchase Fee', 'points_percentage');
	        $data['active']='points_percentage';
        	$data['page_name']='Manuel Credit Purchase Fee';

	        //show data from admin package table
	        $fee_type = 'manual_purchase_fee';
	        $data['points']=$this->General_model->view_data('admin_fee_master',array('fee_type'=>$fee_type));
	        $content=$this->load->view('admin/package/point_percentage.tpl.php',$data,true);
	        $this->render($content);
	    }
		
		// Load Point Percentage Page
	    public function fee_per_game($id='1'){
	    	$this->check_user_page_access();
    		// add breadcrumbs
	        $this->breadcrumbs->push('Home', 'admin/dashboard');
	        $this->breadcrumbs->push('Fee Per Game', 'fee_per_game');
	        $data['active']='fee_per_game';
        	$data['page_name']='Fee Per Game';

	        //show data from admin package table
	        $data['points']=$this->General_model->view_data('admin_points_percentage',array('id'=>$id));
	        $content=$this->load->view('admin/package/enter_per_game_fees_percentage.tpl.php',$data,true);
	        $this->render($content);
	    }

	    // Load Point Percentage Page
	    public function fee_per_lobby($id='1'){
	    	$this->check_user_page_access();
    		// add breadcrumbs
	        $this->breadcrumbs->push('Home', 'admin/dashboard');
	        $this->breadcrumbs->push('Live Lobby Fee', 'fee_per_Lobby');
	        $data['active']='fee_per_lobby';
        	$data['page_name']='Live Lobby Fee';

	        //show data from admin package table
			$data['lobby_fee']=$this->General_model->view_data('admin_lobby_fee',array('id'=>$id));
	        $content=$this->load->view('admin/package/enter_per_lobby_fees_percentage.tpl.php',$data,true);
	        $this->render($content);
	    }

	    // Update Point Percentage
	    public function update_point_percentage(){
	    	$this->check_user_page_access();
	    	if (!empty($_POST)) {
	    		$fee_type = 'manual_purchase_fee';
	    		$fee_calc_type = $_POST['calculation_type'];
	    		$fee_value = $_POST['fee_value'];

	    		for ($i=0; $i < count($_POST['fee_value']); $i++) {
	    			$fee_range = $_POST['min-range'][$i].'-'.$_POST['max-range'][$i];
	    			$get_manual_purchase_fee = $this->General_model->view_single_row('admin_fee_master',array('id' => $_POST['id'][$i]),'*');
	    			if ($get_manual_purchase_fee && count($get_manual_purchase_fee) != 0) {
	    				$add_manual_purchase_fee = array(
	    					'fee_type'=>$fee_type,
	    					'fee_range'=>$fee_range,
	    					'fee_calc_type'=>$fee_calc_type[$i],
	    					'fee_value'=>$fee_value[$i],
				        );
						$r = $this->General_model->update_data('admin_fee_master',$add_manual_purchase_fee,array('id'=>$_POST['id'][$i]));
	    			} else {
	    				$add_manual_purchase_fee = array(
	    					'fee_type'=>$fee_type,
	    					'fee_range'=>$fee_range,
	    					'fee_calc_type'=>$fee_calc_type[$i],
	    					'fee_value'=>$fee_value[$i],
				        );
						$r = $this->General_model->insert_data('admin_fee_master',$add_manual_purchase_fee);
	    			}
	    		}
	    		
	    	}
	        $data['active']='points_percentage';
	        if ($r) {
	            $this->session->set_userdata('message_succ', 'Update successfull');
	            header('location:'.base_url().'admin/points/pointPercentage');
	        } else {
	            $this->session->set_userdata('message_err', 'Failure to update');
	            header('location:'.base_url().'admin/points/pointPercentage');
	        }
	    }


		// Update Point Percentage
	    public function update_fee_per_percentage(){
	    	$this->check_user_page_access();
	        $data['active']='points_percentage';

	        //package info update into admin package table
	        $data=array(
	            'lessthan5'		=>	$this->input->post('lessthan5', TRUE),
	            'morethan5_25'	=>	$this->input->post('morethan5_25', TRUE),
	            'morethan25_50'	=>	$this->input->post('morethan25_50', TRUE),
	            'morethan50'	=>	$this->input->post('morethan50', TRUE),
	            'created_at'	=>	date("Y-m-d h:i:s"),
	        );

	        $r=$this->General_model->update_data('admin_points_percentage',$data,array('id'=>$this->input->post('id', TRUE)));
	        if($r) {
	            $this->session->set_userdata('message_succ', 'Update successfull');
	            header('location:'.base_url().'admin/points/fee_per_game');
	        } else {
	            $this->session->set_userdata('message_err', 'Failure to update');
	            header('location:'.base_url().'admin/points/fee_per_game');
	        }
	    }

	    // Update Lobby Point Percentage
	    public function update_fee_per_lobby_percentage(){
	    	$this->check_user_page_access();
	        $data['active']='points_percentage';
	        if ($_POST['fee_for_creating_game'] !='' && $_POST['fee_per_game'] !='' && $_POST['tip_fees'] !='') {
	        	$data = array(
		            'fee_for_creating_game'		=>	$this->input->post('fee_for_creating_game', TRUE),
		            'fee_per_game'	=>	$this->input->post('fee_per_game', TRUE),
					'tip_fees'	=>	$this->input->post('tip_fees', TRUE),
		        );
	        } else if ($_POST['basic_plan_amount'] !='' && $_POST['basic_plan_fee'] !='' && $_POST['epic_plan_amount'] !='' && $_POST['epic_plan_fee'] !='' && $_POST['legendary_plan_amount'] !='' && $_POST['legendary_plan_fee']) {
	        	$data = array(
					'basic_subscription_amount'	=>	$this->input->post('basic_plan_amount', TRUE),
					'basic_subscription_fee'	=>	$this->input->post('basic_plan_fee', TRUE),
					'epic_subscription_amount' =>	$this->input->post('epic_plan_amount', TRUE),
					'epic_subscription_fee' =>	$this->input->post('epic_plan_fee', TRUE),
					'legendary_subscription_amount' => $this->input->post('legendary_plan_amount', TRUE),
					'legendary_subscription_fee' => $this->input->post('legendary_plan_fee', TRUE),
		        );
	        }
	        $r=$this->General_model->update_data('admin_lobby_fee',$data,array('id'=>$this->input->post('id', TRUE)));
	        if($r) {
	            $this->session->set_userdata('message_succ', 'Update successfull');
	            header('location:'.base_url().'admin/points/fee_per_lobby');
	        } else {
	            $this->session->set_userdata('message_err', 'Failure to update');
	            header('location:'.base_url().'admin/points/fee_per_lobby');
	        }
	    }
		
		public function user_point_list(){
			$this->check_user_page_access();

			// add breadcrumbs
			$this->breadcrumbs->push('Home', 'admin/dashboard');
			$this->breadcrumbs->push('Player Purchase', 'user_point_list');
			$data['active']='user_point_list';
			$data['page_name']='Player Purchase';

			$data['points'] = $this->Admin_user_model->user_point_list();
			$content=$this->load->view('admin/package/user_point_list.tpl.php',$data,true);
			$this->render($content);
		}

		public function deletepointmanualPercentage($id) {
			$this->check_user_page_access();
	        $r = $this->General_model->delete_data('admin_fee_master',array('id'=>$id));
	        if($r) {
	            $this->session->set_userdata('message_succ', 'Delete Successfull');
	            header('location:'.base_url().'admin/points/pointPercentage');
	        } else {
	            $this->session->set_userdata('message_err', 'Failure to delete');
	            header('location:'.base_url().'admin/points/pointPercentage');
	        }
		}
}
