<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include BASEPATH.'../application/controllers/admin/My_Controller.php';

class Privacy extends My_Controller 

{

  	function __construct()

    {

	parent::__construct();

    $this->target_dir=BASEPATH.'../upload/banner/';

	}



	public function index()

	{

		$this->check_user_page_access();

		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('Privacy List', 'admin/privacy');



		$data['active']='privacy_list';

		$data['page_name']='Privacy List';

		

		$data['list']=$this->General_model->view_all_data('privacy','id','asc');

		$content=$this->load->view('admin/privacy/privacy_list.tpl.php',$data,true);

		$this->render($content);

	}

	//load edit page

	public function privacyEdit($id='-1')

	{

		$this->check_user_page_access();

		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('Privacy List', 'admin/privacy');

		$this->breadcrumbs->push('Edit Privacy', 'admin/privacy/privacyEdit');



		$data['active']='privacy_edit';

		$data['page_name']='Edit Page';



		$data['privacy_data']=$this->General_model->view_data('privacy',array('id'=>$id));

		$content=$this->load->view('admin/privacy/privacy_edit.tpl.php',$data,true);

		$this->render($content);

	}

	//edit 

	public function privacyUpdate()

	{

		$this->check_user_page_access();

		$data['active']='game_edit';

		$data=array(

			'content'=>$_POST['content'] //php post
		 );

		 $r=$this->General_model->update_data('privacy',$data,array('id'=>$this->input->post('id', TRUE)));

		if($r)

		{

			$this->session->set_userdata('message_succ', 'Update successfull');	

			redirect(base_url().'admin/privacy/privacyEdit/'.$this->input->post('id', TRUE));

		}

		else

		{

			$this->session->set_userdata('message_err', 'Failure to update');	

			redirect(base_url().'admin/privacy/privacyEdit/'.$this->input->post('id', TRUE));

		}	

	}
	
}

