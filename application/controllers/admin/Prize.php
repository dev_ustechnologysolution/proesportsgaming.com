<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include BASEPATH.'../application/controllers/admin/My_Controller.php';

class Prize extends My_Controller 

{

  	function __construct()

    {

	parent::__construct();

    $this->target_dir=BASEPATH.'../upload/gameflow/';

	}



	public function index()

	{

		$this->check_user_page_access();

		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('Image List', 'admin/Prize');



		$data['active']='work_list';

		$data['page_name']='Image List';

		

		$data['list']=$this->General_model->view_all_data('flow_of_work','id','desc');

		$content=$this->load->view('admin/work/work_list.tpl.php',$data,true);

		$this->render($content);

	}

	//load edit page

	public function contentEdit($id='-1')

	{

		$this->check_user_page_access();

		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('Image List', 'admin/prize');

		$this->breadcrumbs->push('Edit Details', 'admin/prize/contentEdit');



		$data['active']='work_edit';

		$data['page_name']='Edit Page';



		$data['work_data']=$this->General_model->view_data('flow_of_work',array('id'=>$id));

		$content=$this->load->view('admin/work/work_edit.tpl.php',$data,true);

		$this->render($content);

	}

	//edit 

	public function contentUpdate()

	{

		$this->check_user_page_access();

		//$data['active']='game_edit';

		$img=$this->input->post('old_image', TRUE);

		if(isset($_FILES["image"]["name"]) && $_FILES["image"]["name"]!='')

		{

			$img=time().basename($_FILES["image"]["name"]);
			$ext = end((explode(".", $img)));
		
			if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg')
			{

				$target_file = $this->target_dir.$img;

		        if(move_uploaded_file($_FILES["image"]["tmp_name"], $target_file))

				{

					if(file_exists($this->target_dir.$this->input->post('old_image', TRUE)))

					{

						unlink($this->target_dir.$this->input->post('old_image', TRUE));

					}

				}
			}else {

				$this->session->set_flashdata('msg', 'This is not a image file');	
				redirect(base_url().'admin/prize/contentEdit/'.$this->input->post('id', TRUE));
			}

		}

		$data=array(

			'description'=>$this->input->post('description', TRUE),

			'image'=>$img

		 );

		 $r=$this->General_model->update_data('flow_of_work',$data,array('id'=>$this->input->post('id', TRUE)));

		if($r)

		{

			$this->session->set_userdata('message_succ', 'Update successfull');	

			redirect(base_url().'admin/prize/contentEdit/'.$this->input->post('id', TRUE));

		}

		else

		{

			$this->session->set_userdata('message_err', 'Failure to update');	

			redirect(base_url().'admin/prize/contentEdit/'.$this->input->post('id', TRUE));

		}	

	}

	
}

