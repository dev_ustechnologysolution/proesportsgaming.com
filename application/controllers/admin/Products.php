<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Products extends My_Controller  {
	function __construct() {
		parent::__construct();
		$this->load->model('Product_model');
		$this->target_dir=BASEPATH.'../upload/products/';
		$this->check_user_page_access();
	}
	public function index() {
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
	    $this->breadcrumbs->push('Products List', 'admin/products');
		$data['active']='products_list';
		$data['page_name']='Products List';
		// $list_arr = array(
  		//	'order_by' => 'product_order',
  		// );
  		// $data['list'] = $this->Product_model->SearchProductArr($list_arr);
		$data['list'] = $this->General_model->view_all_data('products_tbl','id','asc');
		$content = $this->load->view('admin/products/products_list.tpl.php',$data,true);
		$this->render($content);
	}
	public function productAdd()  {
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
    $this->breadcrumbs->push('Products List', 'admin/products');
		$this->breadcrumbs->push('Add Product', 'admin/products/productAdd');
		$data['active'] = 'products_list';
		$data['page_name'] = 'Add Product';
		$data['category_list'] = $this->Product_model->getCategoryByParent(0);
		$options = array(
      'select' => 'at.title, at.type, at.image_upload, at.attr_order, paot.attr_id, paot.option_label, paot.option_id',
      'table' => 'attributes_tbl at',
      'join' => array(
      	'product_attributes_opt_tbl paot' => 'paot.attr_id = at.id',
      ),
      'order' => array('at.attr_order' => 'asc', 'paot.opt_order' => 'asc'),
      'where' => array('at.status' => 1)
    );
    $attributes_list = $this->General_model->commonGet($options);
    $result = array();
    foreach($attributes_list as $attribute){        
    	$result[$attribute->title.'_'.$attribute->type][] = $attribute; 
    }
    $data['attribute_list'] = $result;
    $data['type'] = $_GET['type'];
		$content=$this->load->view('admin/products/product.tpl.php',$data,true);
		$this->render($content);
	}
	public function fetchSubcat() {
	  if($this->input->post('category_id')) {
	  	$data = $this->Product_model->getCategoryByParent($this->input->post('category_id'));
	   	$output = '<option value="">Select Sub-category</option>';
	   	if (count($data) > 0) {
	   		foreach ($data as $key => $value) {
	   			$output .= '<option value="'.$value['id'].'">'.$value['name'].'</option>';
	   		}
	   	} 
	   	echo $output;
  	}
	}
	public function productEdit() {
		$this->breadcrumbs->push('Home', 'admin/dashboard');
		$this->breadcrumbs->push('Products List', 'admin/products');
		$this->breadcrumbs->push('Edit Products', 'admin/products/productEdit');
		$id = $_REQUEST['id'];
		$data['product_data'] = $this->General_model->view_data('products_tbl',array('id'=>$id));
		$data['product_image_data'] = $this->General_model->view_data('product_images',array('product_id'=>$id));
		$data['product_attribute_data'] = $this->General_model->view_data('product_attributes_tbl',array('product_id' => $id));
		$data['category_list'] = $this->General_model->view_all_data('product_categories_tbl','id','asc');
		$options = array(
      'select' => 'at.title, at.type, at.image_upload, at.attr_order, paot.attr_id, paot.option_label, paot.option_id',
      'table' => 'attributes_tbl at',
      'join' => array(
      	'product_attributes_opt_tbl paot' => 'paot.attr_id = at.id',            
      ),
      'order' => array('at.attr_order' => 'asc', 'paot.opt_order' => 'asc'),
      'where' => array('at.status' => 1)
    );
    $attributes_list = $this->General_model->commonGet($options);
    $result = array();
    foreach($attributes_list as $attribute){        
    	$result[$attribute->title.'_'.$attribute->type][]= $attribute; 
    }
    $data['attribute_list'] = $result;
		$data['active'] = 'products_list';
		$data['page_name'] = 'Product Edit';
	
		$content=$this->load->view('admin/products/product_edit.tpl.php',$data,true);
		$this->render($content);
	}
	public function productInsert() {	
		$this->check_user_page_access();
		$query  = $this->db->query('SELECT max(product_order) as product_order FROM products_tbl');
		$current_product_order = $query->result();
		$product_order = $current_product_order[0]->product_order + 1;
		$data = array(
			'category_id' => $_POST['category_id'].','.$_POST['subcategory_id'],
			'name' => trim($_POST['name']),
			'description' => trim($_POST['description']),
			'price' => trim($_POST['price']),
			'final_price' => (isset($_POST['final_price']) && $_POST['final_price'] != '') ? trim($_POST['final_price']) : '0.00',
			'special_price' => ($_POST['special_price'] != '') ? trim($_POST['special_price']) : '0.00',
			'qty' => trim($_POST['qty']),
			'fee' => trim($_POST['fee']),
			'is_customizable' => 0,
			'status' => 1,
			'is_18' => $_POST['is_18'],
			// 'seller_id' => ($_POST['seller_id'] != '') ? trim($_POST['seller_id']) : 0,
			'type' =>$_POST['type'],
			'product_order' =>$product_order,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		);	

		if (!empty($_POST['is_customizable']) && $_POST['is_customizable'] == 'on') {
			$data['is_customizable'] = 1;
			(!empty($_POST['customizable_tag'])) ? $data['customizable_tag'] = str_replace('"',"'",$_POST['customizable_tag']) : '';
			(!empty($_POST['customizable_tag_description'])) ? $data['customizable_tag_description'] = str_replace('"',"'",$_POST['customizable_tag_description']) : '';
		}
		$r = $this->General_model->insert_data('products_tbl',$data);
		if($r){	

			if(count($_POST['attr_id']) > 0){
				foreach ($_POST['attr_id'] as $key => $value) {
					$arr_attr_data = explode('_', $key);
					$insert_attr_data[] = array(
						'product_id' => $r,
						'attribute_id' => $arr_attr_data[0],
						'option_id' => $arr_attr_data[1],
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s')
					);										
				}
				if(count($insert_attr_data) > 0){
					$this->db->insert_batch('product_attributes_tbl',$insert_attr_data);
				}
			}
			
			// if(isset($_FILES['main_image']["name"])){
			// 	$main_img=time().'_'.$r.basename($_FILES["main_image"]["name"]);				
			// 	$target_file = $this->target_dir.$main_img;

			//     move_uploaded_file($_FILES["main_image"]["tmp_name"], $target_file);
		 //        $main_img_data = array(
		 //        	'product_id' => $r,
		 //        	'image' => $main_img,
		 //        	'is_default' => '1',
		 //        	'created_at' => date('Y-m-d H:i:s'),
			// 		'updated_at' => date('Y-m-d H:i:s')
		 //        );
		 //        $this->General_model->insert_data('product_images',$main_img_data);
			// }

			if(isset($_FILES['game_file']["name"])){
				$game_name=time().'_'.$r.basename($_FILES["game_file"]["name"]);				
				$target_file = $this->target_dir.$game_name;

			    move_uploaded_file($_FILES["game_file"]["tmp_name"], $target_file);

		        
		        $this->General_model->update_data('products_tbl',array('game_name' => $game_name),array('id' => $r));
		        
			}

			// if(count($_FILES['gallery_image']["name"]) > 0)
			// {
			// 	$img_data = array();
			// 	for($i=0;$i<count($_FILES["gallery_image"]["name"]);$i++)
			// 	{
			// 		$img=time().'_'.$r.basename($_FILES["gallery_image"]["name"][$i]);
				
			// 		$target_file = $this->target_dir.$img;

			//         move_uploaded_file($_FILES["gallery_image"]["tmp_name"][$i], $target_file);
			//         $img_data[] = array(
			//         	'product_id' => $r,
			//         	'image' => $img,
			//         	'is_default' => '0',
			//         	'created_at' => date('Y-m-d H:i:s'),
			// 			'updated_at' => date('Y-m-d H:i:s')
			//         );			   
			// 	}
			// 	if(count($img_data) > 0){
			// 		$this->db->insert_batch('product_images',$img_data);
			// 	}
				
			// }
			$this->General_model->update_data('product_images',array('product_id' => $r),array('product_id' => 0));
			$this->General_model->update_data('product_attributes_tbl',array('product_id' => $r),array('product_id' => 0));
			
			// if(count($_FILES['attr_image']["name"]) > 0){

			// 	foreach ($_FILES['attr_image']["name"] as $key => $value) {
			// 		$arr_attr_data = explode('_', $key);
			// 		$option_img1 = array();
			// 		foreach($value as $k => $v){
			// 			$option_img = time().'_'.$r.'_'.basename($v);
			// 			$target_file = $this->target_dir.$option_img;
			//         	move_uploaded_file($_FILES["attr_image"]['tmp_name'][$key][$k], $target_file);
			//         	$option_img1[] = $option_img;

			// 		}
			// 		$insert_attr_data = array(
			// 			'option_image' => implode(',', $option_img1),
			// 			'updated_at' => date('Y-m-d H:i:s')
			// 		);
						
			// 		$this->General_model->update_data('product_attributes_tbl',$insert_attr_data,array('product_id' => $r,'option_id' => $arr_attr_data[1]));
			// 	}				
			// }
			
			$this->session->set_userdata('message_succ', 'Insert successfull');
			redirect(base_url().'admin/products');
		} else {
			$this->session->set_userdata('message_err', 'Failure to Insert');	
			redirect(base_url().'admin/products');
		}
	}
	public function productUpdate() {
		$this->check_user_page_access();
		$id = $this->input->post('id', TRUE);
		$data = array(
			'category_id' => $_POST['category_id'].','.$_POST['subcategory_id'],
			'name' => trim($_POST['name']),
			'description' => trim($_POST['description']),
			'price' => trim($_POST['price']),
			'final_price' => ($_POST['final_price'] != '') ? trim($_POST['final_price']) : '0.00',
			'special_price' => ($_POST['special_price'] != '') ? trim($_POST['special_price']) : '0.00',
			'qty' => trim($_POST['qty']),
			'fee' => trim($_POST['fee']),
			'is_customizable' => 0,
			// 'int_fee' => (isset($_POST['int_fee'])) ? trim($_POST['int_fee']) : '0.00' ,
			'status' => 1,
			'is_18' => $_POST['is_18'],
			// 'seller_id' => ($_POST['seller_id'] != '') ? trim($_POST['seller_id']) : 0,
			'updated_at' => date('Y-m-d H:i:s')
		);
		if (!empty($_POST['is_customizable']) && $_POST['is_customizable'] == 'on') {
			$data['is_customizable'] = 1;
			(!empty($_POST['customizable_tag'])) ? $data['customizable_tag'] = str_replace('"',"'",$_POST['customizable_tag']) : '';
			(!empty($_POST['customizable_tag_description'])) ? $data['customizable_tag_description'] = str_replace('"',"'",$_POST['customizable_tag_description']) : '';
		}
		$r = $this->General_model->update_data('products_tbl',$data,array('id'=>$id));
		if ($r) {

			// if($_FILES['main_image']["name"] != ''){
			// 	$this->General_model->delete_data('product_images',array('product_id'=> $id,'is_default' => '1'));
			// 	$main_img=time().'_'.$id.basename($_FILES["main_image"]["name"]);				
			// 	$target_file = $this->target_dir.$main_img;

			//     move_uploaded_file($_FILES["main_image"]["tmp_name"], $target_file);
		 //        $main_img_data = array(
		 //        	'image' => $main_img,		        	
			// 		'updated_at' => date('Y-m-d H:i:s')
		 //        );
		 //        $this->General_model->update_data('product_images',$main_img_data,array('product_id' => $id,'is_default' => '1'));
			// }
			if ($_FILES['game_file']["name"] != '') {
				$game_name = time().'_'.$r.basename($_FILES["game_file"]["name"]);
				$target_file = $this->target_dir.$game_name;
				move_uploaded_file($_FILES["game_file"]["tmp_name"], $target_file);
				$this->General_model->update_data('products_tbl',array('game_name' => $game_name),array('id' => $id));
			}

			$product_img_data = $this->General_model->view_data('product_images',array('product_id'=>$id,'is_default' => '0'));
			$old_gallery_image = explode(',', $_POST['old_gallery_image']);
			$t = array_diff(array_column($product_img_data,'id'), $old_gallery_image);

			// foreach($t as $single_img){
			// 	$this->General_model->delete_data('product_images',array('id'=> $single_img,'is_default' => '0'));
			// }

			
			// if(count($_FILES['gallery_image']["name"]) > 0 && $_FILES['gallery_image']["name"][0] != '' )
			// {
			// 	$img_data = array();
			// 	for($i=0;$i<count($_FILES["gallery_image"]["name"]);$i++)
			// 	{
			// 		$img=time().'_'.$id.basename($_FILES["gallery_image"]["name"][$i]);
				
			// 		$target_file = $this->target_dir.$img;

			//         move_uploaded_file($_FILES["gallery_image"]["tmp_name"][$i], $target_file);
			//         $img_data[] = array(
			//         	'product_id' => $id,
			//         	'image' => $img,
			//         	'is_default' => '0',
			//         	'created_at' => date('Y-m-d H:i:s'),
			// 			'updated_at' => date('Y-m-d H:i:s')
			//         );			   
			// 	}
				
			// 	if(count($img_data) > 0){
			// 		$this->db->insert_batch('product_images',$img_data);
			// 	}
				
			// }

			$product_attribute_data = $this->General_model->view_data('product_attributes_tbl',array('product_id'=>$id));

			foreach ($product_attribute_data as $v) {
				if(empty($_POST['attr_id']) || !in_array($v['attribute_id'].'_'.$v['option_id'], array_keys($_POST['attr_id']))) {
					$this->General_model->delete_data('product_attributes_tbl',array('id'=> $v['id']));
				} else {
					$update_attr_data = array(
						'attribute_id' => $v['attribute_id'],
						'option_id' => $v['option_id'],
						'updated_at' => date('Y-m-d H:i:s')
					);
					$this->General_model->update_data('product_attributes_tbl',$update_attr_data,array('product_id'=>$id,'option_id' => $arr_attr_data[1]));
				}
			}

			$arr_option = array_column($product_attribute_data,'option_id');
			if (!empty($_POST['attr_id'])) {
				foreach ($_POST['attr_id'] as $key => $value) {
					$arr_attr_data = explode('_', $key);
					if (!in_array($arr_attr_data[1], $arr_option))  {
						$insert_attr_data = array(
							'product_id' => $id,
							'attribute_id' => $arr_attr_data[0],
							'option_id' => $arr_attr_data[1],
							'updated_at' => date('Y-m-d H:i:s'),
							'created_at' => date('Y-m-d H:i:s')
						);
						$this->General_model->insert_data('product_attributes_tbl',$insert_attr_data);	
					}
				}
			}
			
			// $old_attr_image = explode(',', $_POST['old_attr_image']);
			// if(count($old_attr_image) > 0)
			// {
			// 	foreach($product_attribute_data as $single_img1)
			// 	{
			// 		$arr_img = explode(",", $single_img1['option_image']);
			// 		$new = array();
			// 		foreach($arr_img as $k => $t2)
			// 		{
			// 			if($t2 != '')
			// 			{							
			// 				if(in_array($single_img1['option_id'].'_'.$k, $old_attr_image))
			// 				{
			// 					$new[] = $t2;
			// 				} else {
			// 					$new[] = '';
			// 				}
			// 			} 
						
			// 			if(!empty($new)){
			// 				$option_image = implode(',', $new);
			// 			} else {
			// 				$option_image = '';
			// 			}
					
			// 			$update_attr_data_img1 = array(
			// 					'option_image' => $option_image,
			// 					'updated_at' => date('Y-m-d H:i:s')
			// 				);
			// 			$this->General_model->update_data('product_attributes_tbl',$update_attr_data_img1,array('product_id' => $single_img1['product_id'],'option_id' => $single_img1['option_id']));
			// 		}	
			// 	}
			// }
			// if(count($_FILES['attr_image']["name"]) > 0)
			// {
			// 	foreach ($_FILES['attr_image']["name"] as $key => $value) {
			// 		$arr_attr_data = explode('_', $key);
			// 		foreach($value as $k => $v){
			// 			$at_img = array();
			// 			if($v != ''){
			// 				$option_img = time().'_'.$id.basename($v);
			// 				$target_file = $this->target_dir.$option_img;
			// 	        	move_uploaded_file($_FILES["attr_image"]['tmp_name'][$key][$k], $target_file);
			// 	        	$at_img[] = $option_img;
			// 			}
			// 		}

			// 		$single_row = $this->General_model->view_data('product_attributes_tbl',array('product_id' => $id,'option_id' => $arr_attr_data[1]));
			// 		$arr1 = explode(',', $single_row[0]['option_image']);
			// 		$final_img_arr = array_merge($arr1,$at_img);
					
			// 		$update_attr_data_img = array(
			// 			'option_image' => implode(',', $final_img_arr),
			// 			'updated_at' => date('Y-m-d H:i:s')
			// 		);
			// 		$this->General_model->update_data('product_attributes_tbl',$update_attr_data_img,array('product_id' => $id,'option_id' => $arr_attr_data[1]));
			// 	}				
			// }
			
			$this->session->set_userdata('message_succ', 'Update successfull');
			redirect(base_url().'admin/products/productEdit?id='.$this->input->post('id', TRUE));
		} else {
			$this->session->set_userdata('message_err', 'Failure to update');	
			redirect(base_url().'admin/products/productEdit?id='.$this->input->post('id', TRUE));
		}
	}
	public function delete() 
	{
		$id = $_GET['id'];
		$this->check_user_page_access();
		$r=$this->General_model->delete_data('products_tbl',array('id'=> $id));
		if ($r) {
			$this->General_model->delete_data('product_images',array('product_id'=> $id));
			$this->General_model->delete_data('product_attributes_tbl',array('product_id'=> $id));
			$this->session->set_userdata('message_succ', 'Delete Successfull');	
			header('location:'.base_url().'admin/products');
		} else {
			$this->session->set_userdata('message_err', 'Failure to delete');	
			header('location:'.base_url().'admin/products');
		}
	}
	public function updateqty($id) {
		$this->check_user_page_access();
		$r = $this->General_model->update_data('products_tbl',array('qty' => $_POST['qty']),array('id' => $id));
		($r) ? $this->session->set_userdata('message_succ', 'Successfully Updated') : $this->session->set_userdata('message_err', 'Failure to Update');	
		redirect(base_url().'admin/products/');
	}
	public function ChangeProductOrder(){
			$id = $_REQUEST['id'];
			$product_data	= $this->General_model->view_data('products_tbl',array('id'=>$id));
			$data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Product Order   '.$product_data[0]['product_order'].'</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="change_product_order" name="change_product_order" method="POST">
						<div class="box-body">
							<div class="form-group">
								<label>Change Product Order </label>
								<input type="hidden" id="banner_id" class="form-control" name="product_id" value="'.$id.'" required>
								<input type="text" id="new_order" class="form-control" name="new_order" value="" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
		echo $data_value;
	}
	public function UpdateProductOrder(){
		$product_id 	= $_REQUEST['product_id'];
		$new_order 	= $_REQUEST['new_order'];
		$product_data = $this->General_model->view_data('products_tbl',array('id'=>$product_id));
		if($product_data[0]['product_order'] != $new_order){
			$this->General_model->update_data('products_tbl',array('product_order' => $product_data[0]['product_order']),array('product_order'=>$new_order));
		} 
		$data = array(
			'product_order'=>$new_order
		);
		$r = $this->General_model->update_data('products_tbl',$data,array('id'=>$product_id));
		($r) ? $this->session->set_userdata('message_succ', 'Order change successfully') : $this->session->set_userdata('message_err', 'Failure to Order change');	
	}
	public function update_odr() {
		if (!empty($_POST['old']) && !empty($_POST['new'])) {
			$old_ordr_arr = $_POST['old'];
			$new_ordr_arr = $_POST['new'];
			$this->db->select('id,product_order')->from('products_tbl');
			$this->db->where_in('product_order',$_POST['old']);
			$this->db->order_by('product_order');
    		$get_old_products_order = $this->db->get()->result_array();
    		
    		$get_old_products_order = array_combine(array_column($get_old_products_order, 'product_order'), $get_old_products_order);
    		foreach ($old_ordr_arr as $key => $olda) {
    			$this->General_model->update_data('products_tbl', array('product_order' => $new_ordr_arr[$key]), array('id' => $get_old_products_order[$olda]['id']));
			}
			$data['updated'] = true;
			echo json_encode($data);
			exit();
		}
	}
	public function updateIs18() {
		$id 	= $_REQUEST['id'];
		$type 	= $_REQUEST['type'];
		// $status = ($type == 'check') ? 1 : 0;
		$data=array(
			'is_18'=>$type,
		);
		$this->General_model->update_data('products_tbl',$data,array('id'=>$id));		
	}
	public function getProductPrice()
	{
		$id = $_REQUEST['id'];
		$product_data	= $this->General_model->view_data('products_tbl',array('id'=>$id));		
		
		$data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Change Product Price Details</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="change_event_price" name="change_event_price" method="POST">
						<div class="box-body">
						<label>Change Product Price </label>
							<div class="form-group">
								
								<input type="hidden" id="product_id" class="form-control" name="id" value="'.$id.'" required>
								<input type="hidden" class="form-control" name="name" value="'.$product_data[0]["name"].'" required>
								<input type="hidden" class="form-control" name="category_id" value="'.$product_data[0]["category_id"].'" required>
								<input type="hidden" class="form-control" name="description" value="'.$product_data[0]["description"].'" required>
								<input type="hidden" class="form-control" name="qty" value="'.$product_data[0]["qty"].'" required>
								<input type="hidden" class="form-control" name="is_18" value="'.$product_data[0]["is_18"].'" required>
								<input type="hidden" class="form-control" name="seller_id" value="'.$product_data[0]["seller_id"].'" required>
								
								
								<div class="form-group col-md-4">
                  <label>Price</label>
                  <input required onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46" type="text" name="price" class="form-control" value="'.$product_data[0]["price"].'" placeholder="Please Enter Price">
                </div>
                
                <div class="form-group col-md-4">
                  <label>Fee</label>
                  <input onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46" name="fee" required type="text" class="form-control" placeholder="Please Enter Fee" value="'.$product_data[0]['fee'].'">
                </div>
                                 
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
							<button type="button" class="btn btn-info pull-right" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>';
		echo $data_value;
	}
	public function getProductFee($type){
		$type_msg = $type.'_us_product_fees';
		$global_event_fees=$this->General_model->view_single_row('site_settings','option_title',$type_msg);
		$arr_result = explode("_", $global_event_fees['option_value']);
		$type_1 = $type_2 = '';
		if($arr_result[0] == 1)
		{
			$type_1 = 'selected';
		} else {
			$type_2 = 'selected';
		}
		$data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Set '.ucfirst($type).' US Fee</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="'.base_url("admin/products/updateIsProductFee/".$type).'" id="change_product_fee" name="change_product_fee" method="POST">
						<div class="box-body">						      
							<div class="form-group row">
								<label class="col-sm-2">Type </label>	
								<div class="col-sm-10">								
								   <select name="type" class="form-control">
								   <option '.$type_1.' value="1">Fix Amount</option>
								   <option '.$type_2.' value="2">Percentage</option>
								   </select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2">Fee </label>	
								<div class="col-sm-10">								
								   <input required type="text" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46"  class="form-control event_fee" name="product_fee" value="'.$arr_result[1].'">
								</div>
							</div>
							
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
							<button type="button" class="btn btn-info pull-right" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>';
		echo $data_value;
	}
	public function updateIsProductFee($type){
		$type_msg = $type.'_us_product_fees';
		$insert_data =array('option_value' => $_REQUEST['type'].'_'.$_REQUEST['product_fee']);
		$this->General_model->update_data('site_settings',$insert_data,array('option_title' => $type_msg));
		// $query  = $this->db->query('SELECT * from products_tbl' );
		
		// $isEventLobby = $query->result();
		// if(count($isEventLobby) > 0){
		// 	foreach($isEventLobby as $k => $event){
		// 		if($_REQUEST['type'] == 1){
		// 			$event_fee = $_REQUEST['product_fee'];
		// 		} else {
		// 			$event_fee = $event->price * $_REQUEST['product_fee'] /100; 
		// 		}
		// 		$updateArray[] = array(
		// 			'id' => $event->id,
		// 			'int_fee' => $event_fee
		// 		);
		// 	}
		// 	$this->db->update_batch('products_tbl',$updateArray, 'id'); 
		// }
		$this->session->set_userdata('message_succ', ucfirst($type).' US Product Fee updated successfully.');
		redirect(base_url().'admin/products');
	}
	function product_ads() {
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('PRODUCT Ads', 'admin/products/product_ads');
        $data['active']    = 'product_ads';
        $data['page_name'] = 'PRODUCT Ads';
         $options = array(
            'select' => 'at.*',
            'table' => 'ads_tbl at',
            'where' => array('at.ads_type' => 'product_ads'),
        );
        $data['ads_list'] = $this->General_model->commonGet($options);
        $content = $this->load->view('admin/products/product_ads_list.tpl.php',$data,true);
        $this->render($content);
	}
	public function upload_file() {	
		if($_FILES['file']["name"] != '') {
			$main_img = time().'_'.basename($_FILES["file"]["name"]);				
			$target_file = $this->target_dir.$main_img;
			move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);

	    if ($_REQUEST['type'] == 1) {
		    $attribute_data_arr = explode('_', $_REQUEST['attr']);	
        $main_img_data = array(
        	'product_id' => (isset($_REQUEST['product_id'])) ? $_REQUEST['product_id'] : 0,
        	'attribute_id' => $attribute_data_arr[1],
					'option_id' => $attribute_data_arr[2],
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s'),
        	'option_image' => $main_img
        );
        $this->General_model->insert_data('product_attributes_tbl', $main_img_data);
			} else {
				$main_img_data = array(
        	'product_id' => (isset($_REQUEST['product_id'])) ? $_REQUEST['product_id'] : 0,
        	'image' => $main_img,
        	'is_default' => '0',
        	'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				);
				$this->General_model->insert_data('product_images',$main_img_data);
			}
			$result['id'] = $this->db->insert_id();
			$result['name'] = base_url().'upload/products/'.$main_img;
		}
		echo json_encode($result);
	}
	public function delete_file() {
		if($_REQUEST['type'] == 1 ){
			$this->General_model->delete_data('product_attributes_tbl',array('id' => $_REQUEST['id']));
		} else {
			$this->General_model->delete_data('product_images',array('id'=> $_REQUEST['id']));
		}
	}
	function search_category() {
		if (!empty($_POST) && $_POST['category'] !='') {
			$category = $_POST['category'];
			if ($_POST['category_type'] == 'parent') {
	            $data['suboption'] = '<option value="">Please select a Sub Category</option>';
            	$arr['parent_id'] = $category;
            	$all_sub_cat = $this->Product_model->category_list($arr);
                foreach ($all_sub_cat as $key => $asc) {
                    $data['suboption'] .= '<option value="'.$asc['id'].','.$asc['parent_id'].'">'.$asc['name'].'</option>';
                }
                echo json_encode($data);
	        	exit();
			} else {
            	$list_arr['main_cat'] = explode(',', $category)[0];
                $list = $this->Product_model->SearchProductArr($list_arr);
                $selectproductname_list = '<option value="">Select Product</option>';
                if (!empty($list)) {
	            	foreach ($list as $key => $lst) {
	            		$selectproductname_list .= '<option value="'.$lst['id'].'">'.$lst['name'].'</option>';
	            	}
	            }
	            $data['productname_list'] = $selectproductname_list;
	            echo json_encode($data);
	        	exit();
			}
		}
	}
}

