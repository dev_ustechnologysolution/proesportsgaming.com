<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Sendmail extends My_Controller 
{	
  	function __construct()
    {
		parent::__construct();
		$this->load->library('email');
		$this->load->model('User_model');
		$this->load->model('General_model');
	}
	public function index()
	{
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Email User Report', 'admin/Sendmail/getBulkEmailReport');

		$data['active']='email_report';
		$data['page_name']='Email User Report';		

		$data['backup_cat']='';
		$sql = $this->db->query('SELECT ud.name,u.email,besh.template_title,beuh.user_id,beuh.status,beuh.date from bulk_email_send_history as besh inner join bulk_email_user_history as beuh on beuh.bulk_email_id = besh.id inner join user_detail as ud on ud.user_id = beuh.user_id inner join user as u on u.id = ud.user_id order by beuh.date desc');
		$data['list'] = $sql->result_array();

		$content=$this->load->view('admin/sendmail/email_user_list.tpl.php',$data,true);
		$this->render($content);
	}
	public function sendBulkEmail()
	{
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Send Email', 'admin/Sendmail');

		$data['active']='send_email';
		$data['page_name']='Send Email';		

		$data['backup_cat']='';
		$data['template_list'] = $this->General_model->view_all_data('email_template','id','desc');
		$content=$this->load->view('admin/sendmail/sendmail.tpl.php',$data,true);
		$this->render($content);
	}
	public function send_mail_password_edit()
	{
		echo '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="send_mail_mdl" name="send_mail_mdl" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Admin Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>								
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
							<button type="button" class="btn btn-info pull-right" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>			
			';
	}
	public function send_mail_password_change()
	{
		$security_password 	= $_REQUEST['security_password'];		
		$security_password_db = $this->General_model->view_all_data('admin_security_password','id','asc');
		
		if($security_password == $security_password_db[0]['password']){
			$arr_template_data = explode('_', $_REQUEST['template_id']);
			$insert_data = array(
				'template_id' => $arr_template_data[0],
				'template_title' => $arr_template_data[1],
				'date' => date("Y-m-d")
			);
			
			$this->db->insert('bulk_email_send_history', $insert_data);
   			$insertId = $this->db->insert_id();

			if($insertId)
			{
				$command = "php index.php admin Sendmail sendEmailAsBackgroud $insertId";				
        		exec($command);	
        		$this->session->set_userdata('message_succ', 'Bulk email send background process have been started');			
				$data_value = 'true';
			} else {
				 $data_value = 'false';
			}
           
		} else{
			$data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="send_mail_mdl" name="send_mail_mdl" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Admin Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
								
							</div>
								<label for="name">Password Error</label>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
							<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
						</div>
					</form>
				</div>
			</div>';
		}	
		echo $data_value; die;
	}
	public function email_template()
	{
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Email Template', 'admin/Sendmail/email_template');

		$data['active']='email_template';
		$data['page_name']='Email Template';		

		$data['backup_cat']='';
		$data['list']=$this->General_model->view_all_data('email_template','id','desc');
		$content=$this->load->view('admin/email_template/email_template_list.tpl.php',$data,true);
		$this->render($content);
	}	
	public function addEmailTemplate()
	{
		$this->check_user_page_access();

		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Email Template List', 'admin/Sendmail/email_template');
        $this->breadcrumbs->push('Add Menu', 'admin/Sendmail/addEmailTemplate');

		$data['active']='email_template';
		$data['page_name']='Email Template Add';

		$content=$this->load->view('admin/email_template/email_template_create.tpl.php',$data,true);
		$this->render($content);
	}
	public function createEmailTemplate()
	{
		$this->check_user_page_access();
		
		$data=array(
			'title'=>$this->input->post('title',TRUE),
			'status'=>1,
			'subject' => $this->input->post('subject',TRUE),			
			'body' => $_REQUEST['editor1']
		 );
		$r=$this->General_model->insert_data('email_template',$data);

		if($r)
		{
			$this->session->set_userdata('message_succ', 'successfull Added');
			$result = 'true';			
		} else {
			$result = 'false';
			$this->session->set_userdata('message_err', 'Failure to Create');
		}
		echo $result;
	}
	public function emailTemplateEdit($id=NULL)
	{
		$this->check_user_page_access();

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('Email Template List', 'admin/Sendmail/email_template');

		$this->breadcrumbs->push('Edit Email Template', 'admin/Sendmail/emailTemplateEdit');

		$data['active']='email_template';

		$data['page_name']='Edit Email Template';

		$data['email_template_data']=$this->General_model->view_data('email_template',array('id'=>$id));

		$content=$this->load->view('admin/email_template/email_template_edit.tpl.php',$data,true);

		$this->render($content);
	}
	public function emailTemplateUpdate()
	{

		$this->check_user_page_access();		

		$data['active']='email_template'; 
		$data=array(
			'title'=>$this->input->post('title',TRUE),
			'status'=>1,
			'subject' => $this->input->post('subject',TRUE),
			'body' => $_REQUEST['editor1']
		 );
		
		$r=$this->General_model->update_data('email_template',$data,array('id'=>$this->input->post('id', TRUE)));

		if($r)
		{
			$this->session->set_userdata('message_succ', 'Update successfull');			
			$result = 'true';
		}else{

			$this->session->set_userdata('message_err', 'Failure to update');			
			$result = 'false';			
		}
		echo $result;
	}
	public function delete_data()
	{
		
		$id = $_REQUEST['id'];
		$result = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="security_password_id" name="security_password_id" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Admin Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
								<input type="hidden" id="id" class="form-control" name="id" value="'.$id.'" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
							<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>	
						</div>
					</form>
				</div>
			</div>';
			echo $result;
	}
	public function delete()
	{
		$security_password 	= $_REQUEST['security_password'];
		$id = $_REQUEST['id'];
			
		$security_password_db = $this->General_model->view_all_data('admin_security_password','id','asc');
		
		if($security_password == $security_password_db[0]['password'])
		{
			$r=$this->General_model->delete_data('email_template',array('id'=>$id));

			if($r) 
			{
				$this->session->set_userdata('message_succ', 'Delete Successfull');	
				$data_value = true;
			} else {
				$this->session->set_userdata('message_err', 'Failure to delete');	
				$data_value = false;
			}
		} else {
			$data_value['pass_err'] = 'Password Error';
			$data_value = '<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<form action="#" id="security_password_id" name="security_password_id" method="POST" >
						<div class="form-group" style="margin: 0 12px 15px;"><label style="color:red;">'.$data_value['pass_err'].'</label></div>
							<div class="box-body">
								<div class="form-group">
									<label for="name">Admin Password</label>
									<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
									<input type="hidden" id="id" class="form-control" name="id" value="'.$id.'" required>
								</div>
							</div>
							<div class="box-footer">
								<input class="btn btn-primary" type="submit" value="Submit" >
								<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>	
							</div>
						</form>
					</div>
				</div>';
				
			}
		echo $data_value; die;
	}
	public function sendEmailAsBackgroud($id)
	{
		$sql = $this->db->query('SELECT et.subject,et.body from email_template as et inner join bulk_email_send_history as besh on besh.template_id = et.id where besh.id ='.$id);
		$arr_email_data = $sql->result_array();

		$user_list = $this->User_model->user_view_admin();

		foreach ($user_list as $key => $single_user) 
		{			
			if($single_user['id'] == 25 || $single_user['id'] == 67 || $single_user['id'] == 27 || $single_user['id'] == 28 || $single_user['id'] == 10 || $single_user['id'] == 13)
			{
				$data = $this->General_model->sendMail($single_user['email'],$single_user['name'],$arr_email_data[0]['subject'],$arr_email_data[0]['body']);
				if($data == 1){
					$status = '1';
				} else {
					$status = '0';
				}
				$insert_data = array(
					'bulk_email_id' => $id,
					'user_id' => $single_user['id'],
					'status' => $status
				);
				$this->db->insert('bulk_email_user_history', $insert_data);
			}
			
		}
	}
}