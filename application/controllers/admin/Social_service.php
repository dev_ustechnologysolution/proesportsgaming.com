<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include BASEPATH.'../application/controllers/admin/My_Controller.php';

class Social_service extends My_Controller 

{

  	function __construct()

    {

	parent::__construct();

    $this->target_dir=BASEPATH.'../upload/all_images/';

    $this->check_user_page_access();
	}



	public function index()

	{
		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('Social Service', 'admin/Social_service');



		$data['active']='social_service';

		$data['page_name']='Social Service';

		

		$data['list']=$this->General_model->view_all_data('social_service', 'id', 'asc');

		$content=$this->load->view('admin/social_service/social_service_list.tpl.php',$data, true);

		$this->render($content);

	}

	public function socialserviceAdd() {
        // add breadcrumbs
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Social Service', 'admin/Social_service');
        $this->breadcrumbs->push('Add Social Service', 'Social_service');
        $data['active']='social_service';
        $data['page_name']='Add Social Service';
        //fetch the category for showing into game add form
        // $data['cat_name']=$this->General_model->game_system_list();
        $content=$this->load->view('admin/social_service/social_service_add.tpl.php', $data, true);
        $this->render($content);
    }

	public function add_social_service(){
        if($this->input->post('name', TRUE) != ''){
            $arr = array(
				'name' => $_POST['name']
			);
		    if (isset($_FILES["image"]["name"]) && $_FILES["image"]["name"]!='') {
		    	$upimg = time().basename($_FILES["image"]["name"]);
		    	$ext = end((explode(".", $upimg)));
		    	$extarr = array('gif','jpeg','png','jpg');
		    	if (in_array(strtolower($ext), $extarr)) {
		    		$target_file = $this->target_dir.$upimg;
		    		(move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) ? $arr['image'] = $upimg : '';
	        }
	    }
		$r = $this->General_model->insert_data('social_service',$arr);
		($r) ? $this->session->set_userdata('message_succ', 'Successfully Added') : $this->session->set_userdata('message_err', 'Failure to Add');
		redirect(base_url().'admin/Social_service/');

        } else {
            header('location:'.base_url().'admin/Social_service');
        }
    }	

    public function socialserviceEdit($id) {
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Social Service', 'admin/Social_service');
        $this->breadcrumbs->push('Edit Social Service', 'admin/Social_service/socialserviceEdit');
        $data['active']='social_service';
        $data['page_name']='Edit Page';
        $data['data']=$this->General_model->view_data('social_service',array('id'=>$id));       
        $content=$this->load->view('admin/social_service/social_service_edit.tpl.php',$data,true);
        $this->render($content);
    }

    public function socialserviceUpdate() {
		$data['active']='social_service';
        $img=$this->input->post('old_image', TRUE);
        if(isset($_FILES["image"]["name"]) && $_FILES["image"]["name"]!='')
        {
            $img=time().basename($_FILES["image"]["name"]);
            $target_file = $this->target_dir.$img;
            if(move_uploaded_file($_FILES["image"]["tmp_name"], $target_file))
            {
                if(file_exists($this->target_dir.$this->input->post('old_image', TRUE)))
                {
                    unlink($this->target_dir.$this->input->post('old_image', TRUE));
                }
            }
        }
        $data=array(
            'name'=>$this->input->post('name', TRUE),
            'image'=>$img
            );
        $r=$this->General_model->update_data('social_service',$data,array('id'=>$this->input->post('id', TRUE)));
        if($r)
        {
            $this->session->set_userdata('message_succ', 'Update successfull');
            header('location:'.base_url().'admin/Social_service/socialserviceEdit/'.$this->input->post('id', TRUE));
        }
        else
        {
            $this->session->set_userdata('message_err', 'Failure to update');
            header('location:'.base_url().'admin/Social_service/socialserviceEdit/'.$this->input->post('id', TRUE));
        }
    }

    public function delete_social_service($id){
    	$r = $this->General_model->delete_data('social_service',array('id'=> $id));
    	$this->General_model->delete_data('users_social_link',array('social_id'=> $id));
		($r) ? $this->session->set_flashdata('message_succ', 'Delete Successfull') : $this->session->set_flashdata('message_err', 'Failure to delete');
		header('location:'.base_url().'admin/Social_service/');
    }	
}

