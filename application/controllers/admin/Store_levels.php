<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Store_levels extends My_Controller {
  	function __construct() {
        parent::__construct();
        $this->target_dir = BASEPATH.'../upload/all_images/';
        $this->check_user_page_access();
	}
	public function index() {
		// add breadcrumbs
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Store Levels', 'admin/Store_levels');
		$data['active']='store_levels';
		$data['page_name']='Store Levels';
		$data['global_store_fee'] = $this->General_model->view_single_row('site_settings','option_title','global_store_fee');
		$data['list'] = $this->General_model->view_all_data('store_levels', 'order_id', 'asc');

        $data['store_membership_status'] = $this->General_model->view_single_row('site_settings','option_title','store_membership_status');
		$content=$this->load->view('admin/store_levels/store_levels_list.tpl.php',$data, true);
		$this->render($content);
	}

	public function storelevelsAdd() {
        // add breadcrumbs
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Store Levels', 'admin/Store_levels');
        $this->breadcrumbs->push('Add Store Level', 'Store_levels');
        $data['active']='store_levels';
        $data['page_name']='Add Store Level';
        //fetch the category for showing into game add form
        // $data['cat_name']=$this->General_model->game_system_list();

        $data['max_level'] = $this->db->select_max('level')->from('store_levels')->get()->row();
        $content = $this->load->view('admin/store_levels/store_levels_add.tpl.php', $data, true);
        $this->render($content);
    }
    public function view_users_of_level($level_id) {
        // add breadcrumbs
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Store Levels', 'admin/Store_levels');
        $this->breadcrumbs->push('Level Player List', 'view_users_of_level');
        $data['active'] = 'store_levels';
        $data['page_name'] = 'Level Player Lists';

        $level_arr = array('level_id' => $level_id);
        $data['list'] = $this->Product_model->get_store_level_users($level_arr);
        $data['level_id'] = $level_id;
        $content = $this->load->view('admin/store_levels/view_users_of_level.tpl.php', $data, true);
        $this->render($content);   
    }
	public function add_store_levels(){
        if($this->input->post('level', TRUE) != '' && $this->input->post('item_sold', TRUE) != '' && $this->input->post('reward', TRUE) != ''){
            $max_level = $this->db->select_max('level')->from('store_levels')->get()->row()->level;
            $max_id = $this->db->select_max('id')->from('store_levels')->get()->row()->id;
            if ($max_level+1 > $this->input->post('level', TRUE)) {
                $this->session->set_userdata('message_err', 'Level must me Greater then or Equal to '.($max_level+1));
            } else {
                $arr = array(
    				'level' => $_POST['level'],
                    'item_sold' => $_POST['item_sold'],
                    'level_title' => $_POST['level_title'],
                    'reward' => $_POST['reward'],
                    'order_id' => $max_id+1,
                    'created_at' => date("Y-m-d H:i:s")
    			);
                if (!empty($_FILES['level_image']) && !empty($_FILES['level_image']['name'])) {
                    $img = time().basename($_FILES['level_image']["name"]);
                    $ext = strtolower(end((explode(".", $img))));
                    if (!in_array(strtolower($ext), array('gif','jpeg','jpg', 'png'))){
                        $this->session->set_flashdata('message_err', 'Please select valid image');
                        redirect($_SERVER['HTTP_REFERER']);
                    }
                    if (!move_uploaded_file($_FILES["level_image"]["tmp_name"], $this->target_dir.$img)) {
                        $this->session->set_flashdata('message_err', 'Something wrong');
                        redirect($_SERVER['HTTP_REFERER']);
                    }
                    $arr['level_img'] = $img;
                }
                $r = $this->General_model->insert_data('store_levels',$arr);
        		($r) ? $this->session->set_userdata('message_succ', 'Successfully Added') : $this->session->set_userdata('message_err', 'Failure to Add');
            }
        } else {
            $this->session->set_userdata('message_err', 'Failure to Add');
        }
    	redirect(base_url().'admin/Store_levels/');
    }
    function reset_level_players($level) {
        $r[] = $this->General_model->delete_data('user_level_items',array('level'=> $level));
        $r[] = $this->General_model->delete_data('user_level',array('current_level'=> $level));
        (!empty($r)) ? $this->session->set_userdata('message_succ', 'Successfully Reset') : $this->session->set_userdata('message_err', 'Failure to Reset');
        redirect(base_url().'admin/Store_levels/view_users_of_level/'.$level);
    }
    public function level_order_update() {
        if (!empty($_POST['old']) && !empty($_POST['new'])) {
            $old_ordr_arr = array_map(function($value){return trim(strip_tags($value));}, $_POST['old']);
            $new_ordr_arr = array_map(function($value){return trim(strip_tags($value));}, $_POST['new']);

            $this->db->select('id,order_id')->from('store_levels');
            $this->db->where_in('order_id',$old_ordr_arr)->order_by('order_id');
            $get_old_level_order = $this->db->get()->result_array();

            $get_old_level_order = array_combine(array_column($get_old_level_order, 'order_id'), $get_old_level_order);

            foreach ($old_ordr_arr as $key => $olda) {
                $this->General_model->update_data('store_levels', array('order_id' => $new_ordr_arr[$key]), array('id' => $get_old_level_order[$olda]['id']));
            }
            $data['updated'] = true;
            echo json_encode($data);
            exit();
        }
    }
    public function storelevelsEdit($id) {
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Store Levels', 'admin/Store_levels');
        $this->breadcrumbs->push('Edit Store Levels', 'admin/Store_levels/storelevelsEdit');
        $data['active']='store_levels';
        $data['page_name']='Edit Page';
        $data['data']=$this->General_model->view_data('store_levels',array('id'=>$id));       
        $content=$this->load->view('admin/store_levels/store_levels_edit.tpl.php',$data,true);
        $this->render($content);
    }

    public function storelevelsUpdate() {
		$data['active']='store_levels';
        $arr = array(
            'level' => $this->input->post('level',TRUE),
            'item_sold' => $this->input->post('item_sold',TRUE),
            'level_title' => $this->input->post('level_title',TRUE),
            'reward' => $this->input->post('reward',TRUE),
        );
        if (!empty($_FILES['level_image']) && !empty($_FILES['level_image']['name'])) {
            $img = time().basename($_FILES['level_image']["name"]);
            $ext = strtolower(end((explode(".", $img))));
            if (!in_array(strtolower($ext), array('gif','jpeg','jpg', 'png'))){
                $this->session->set_flashdata('message_err', 'Please select valid image');
                redirect($_SERVER['HTTP_REFERER']);
            }
            if (!move_uploaded_file($_FILES["level_image"]["tmp_name"], $this->target_dir.$img)) {
                $this->session->set_flashdata('message_err', 'Something wrong');
                redirect($_SERVER['HTTP_REFERER']);
            }
            $arr['level_img'] = $img;
        }
        $r=$this->General_model->update_data('store_levels',$arr,array('id'=>$this->input->post('id', TRUE)));
        ($r) ? $this->session->set_userdata('message_succ', 'Update successfull') :$this->session->set_userdata('message_err', 'Failure to update');
        header('location:'.base_url().'admin/Store_levels/storelevelsEdit/'.$this->input->post('id', TRUE));
    }

    public function delete_store_levels($level_id){
        $level_arr = array('level_id' => $level_id);
        $list = $this->Product_model->get_store_level_users($level_arr);
        if (!empty($list)) {
            $this->session->set_flashdata('message_err', "You can't delete this level, Users reached this level.");
        } else {
        	$r = $this->General_model->delete_data('store_levels',array('id'=> $id));
    		($r) ? $this->session->set_flashdata('message_succ', 'Delete Successfull') : $this->session->set_flashdata('message_err', 'Failure to delete');
        }
		redirect(base_url().'admin/Store_levels/');
    }	

    public function global_store_fee(){
        if($this->input->post('global_store_fee', TRUE) != ''){
            $r = $this->General_model->update_data('site_settings',array('option_value' => $_POST['global_store_fee']),array('option_title'=>'global_store_fee'));    
            ($r) ? $this->session->set_flashdata('message_succ', 'Global store fee set successfully') : $this->session->set_flashdata('message_err', 'Failure to set global store fee');
            header('location:'.base_url().'admin/Store_levels/');
        }else{
            $this->session->set_flashdata('message_err', 'Failure to set global store fee');
            header('location:'.base_url().'admin/Store_levels/');
        }
    }

    public function change_store_membership_status(){
        $r = $this->General_model->update_data('site_settings',array('option_value' => $_POST['status']),array('option_title'=>'store_membership_status'));    
        ($r) ? $this->session->set_flashdata('message_succ', 'Status updated') : $this->session->set_flashdata('message_err', 'Failure to update status');
        header('location:'.base_url().'admin/Store_levels/');
    }
}

