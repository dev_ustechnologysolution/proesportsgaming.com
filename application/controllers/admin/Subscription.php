<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';

class Subscription extends My_Controller {
	function __construct() {
        parent::__construct();
        $this->load->model('Admin_user_model');
        $this->target_dir=BASEPATH.'../upload/subscription/';
        $this->check_user_page_access();
    }
    public function index(){
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Subscription Plan List', 'admin/Subscription');

		$data['active']='pro_esports_subscribe';
		$data['page_name']='Subscription Plan List';
		$data['list']=$this->General_model->view_all_data('subscription_plans','id','desc');
		$content=$this->load->view('admin/subscribe/subscribe_plans_list.tpl.php',$data,true);
		$this->render($content);
    }
    public function add_subscription_plan() {
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Subscription Plan List', 'admin/subscription');
        $this->breadcrumbs->push('Add Subscription Plan', 'admin/subscription/add_subscription_plan');

		$data['active']='pro_esports_subscribe';
		$data['page_name']='Add Subscription Plan';

		$content=$this->load->view('admin/subscribe/subscribe_create.tpl.php',$data,true);
		$this->render($content);
    }
    public function insert_subscription_plan() {
    	$img = '';
		if (isset($_FILES["image"]["name"]) && $_FILES["image"]["name"]!='') {
			$img = time().basename($_FILES["image"]["name"]);
			$ext = end((explode(".", $img)));
			
			if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg') {
				$target_file = $this->target_dir.$img;
		        move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
		    } else {
		    	$this->session->set_flashdata('msg', 'This is not a image file');
				redirect(base_url().'admin/subscription/add_subscription_plan');
		    }
		}
		$data=array(
			'title'=>$this->input->post('title',TRUE),
			'status'=>'1',
			'description' => $this->input->post('description',TRUE),
			'amount' => $this->input->post('amount',TRUE),
			'fees' => $this->input->post('fees',TRUE),
			'image' => $img,
			'terms_condition' => $this->input->post('terms_condition',TRUE),
			'start_date' => date('Y-m-d h:i'),
			'expire_date' => date('Y-m-d h:i')
		 );
		$r = $this->General_model->insert_data('subscription_plans',$data);
		if($r) {
			$this->session->set_userdata('message_succ', 'Successfull Added');
			redirect(base_url().'admin/subscription');
		} else {
			$this->session->set_userdata('message_err', 'Failure to Create');
			redirect(base_url().'admin/subscription');
		}
    }
    public function subscription_plan_edit($id='-1'){
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Subscription Plan List', 'admin/subscription');
		$this->breadcrumbs->push('Edit Subscription Plan', 'admin/subscription/subscription_plan_edit');

		$data['active']='pro_esports_subscribe';
		$data['page_name']='Edit Subscription Plan';
		$data['subscription_plan']=$this->General_model->view_single_row('subscription_plans',array('id'=>$id),'*');

		$content=$this->load->view('admin/subscribe/subscription_plan_edit.tpl.php',$data,true);
		$this->render($content);
    }
    public function subscribe_plan_update(){	
		$data['active']='pro_esports_subscribe';
		$img = $this->input->post('image', TRUE);;
		if((isset($_FILES["image"]["name"])) && ($_FILES["image"]["name"] != '')) {
			$img=time().basename($_FILES["image"]["name"]);
			$ext = end((explode(".", $img)));
			if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg'){
				$target_file = $this->target_dir.$img;
		        if(move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
					if(file_exists($this->target_dir.$this->input->post('old_image', TRUE))) {
						unlink($this->target_dir.$this->input->post('old_image', TRUE));
					}
				}
			}
		}

		$data=array(
			'title'=>$this->input->post('title',TRUE),
			'status'=>'1',
			'description' => $this->input->post('description',TRUE),
			'amount' => $this->input->post('amount',TRUE),
			'fees' => $this->input->post('fees',TRUE),
			'terms_condition' => $this->input->post('terms_condition',TRUE),
			'image' => $img
		 );

		$r=$this->General_model->update_data('subscription_plans',$data,array('id'=>$this->input->post('id', TRUE)));
		if($r) {
			$this->session->set_userdata('message_succ', 'Update successfull');
			redirect(base_url().'admin/Subscription/subscription_plan_edit/'.$this->input->post('id', TRUE));
		} else {
			$this->session->set_userdata('message_err', 'Failure to update');
			redirect(base_url().'admin/Subscription/subscription_plan_edit/'.$this->input->post('id', TRUE));
		}	
	}
	public function delete_subscription_plan($id='-1'){
		$subscription_plan = $this->General_model->view_single_row('subscription_plans',array('id'=>$id),'*');
		$r=$this->General_model->delete_data('subscription_plans',array('id'=>$id));
		if($r) {
			if(file_exists($this->target_dir.$subscription_plan['image'])) {
				unlink($this->target_dir.$subscription_plan['image']);
			}
			$this->session->set_userdata('message_succ', 'Delete Successfull');	
			redirect(base_url().'admin/subscription');
		} else {
			$this->session->set_userdata('message_err', 'Failure to delete');
			redirect(base_url().'admin/subscription');
		}
	}
}