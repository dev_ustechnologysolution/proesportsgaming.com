<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include BASEPATH.'../application/controllers/admin/My_Controller.php';

class Support extends My_Controller 

{

  	function __construct()

    {

	parent::__construct();

    $this->target_dir=BASEPATH.'../upload/banner/';

	}



	public function index()

	{

		$this->check_user_page_access();

		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('Support List', 'admin/support');



		$data['active']='support_list';

		$data['page_name']='Support List';

		

		$data['list']=$this->General_model->view_all_data('support','id','asc');

		$content=$this->load->view('admin/support/support_list.tpl.php',$data,true);

		$this->render($content);

	}

	//load edit page

	public function supportEdit($id='-1')

	{

		$this->check_user_page_access();

		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('support List', 'admin/support');

		$this->breadcrumbs->push('Edit Support', 'admin/support/supportEdit');



		$data['active']='support_edit';

		$data['page_name']='Edit Page';



		$data['support_data']=$this->General_model->view_data('support',array('id'=>$id));

		$content=$this->load->view('admin/support/support_edit.tpl.php',$data,true);

		$this->render($content);

	}

	//edit 

	public function supportUpdate()

	{

		$this->check_user_page_access();

		//$data['active']='game_edit';

		$data=array(

			'content'=>$this->input->post('content', TRUE)
		 );

		 $r=$this->General_model->update_data('support',$data,array('id'=>$this->input->post('id', TRUE)));

		if($r)

		{

			$this->session->set_userdata('message_succ', 'Update successfull');	

			redirect(base_url().'admin/support/supportEdit/'.$this->input->post('id', TRUE));

		}

		else

		{

			$this->session->set_userdata('message_err', 'Failure to update');	

			redirect(base_url().'admin/support/supportEdit/'.$this->input->post('id', TRUE));

		}	

	}
	
}

