<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class User extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Admin_user_model');
		$this->load->model('Agent_user_model');
		$this->target_dir=BASEPATH.'../upload/profile_img/';
	}
	
	public function agentlogin() {
		$agent_id=$this->input->post('action_value', TRUE);
		$agent_password=md5($this->input->post('security_password', TRUE));
		$data_arr= $this->Agent_user_model->agent_authenticate($agent_id,$agent_password);
		if(isset($data_arr) && is_array($data_arr)) {
			$this->Agent_user_model->log_this_login($data_arr);
			header('location:'.base_url().'admin/chat/chat_with_cust');
			exit;
		} else {
			$data['err'] ='Invalid username or password';
			echo "<script> alert('Enter Valid Information');</script>";
			redirect(base_url().'admin/chat' ,'refresh');
		}
	}
	public function login() {
		$username = trim($this->input->post('username', TRUE));
		$password = md5($this->input->post('password', TRUE));
		$data_arr = $this->Admin_user_model->authenticate($username,$password);
		if (isset($data_arr) && is_array($data_arr)) {
			$this->Admin_user_model->log_this_login($data_arr);
			header('location:'.base_url().'admin/dashboard/');
			exit;
		} else {
			$data['err'] ='Invalid username or password';
			$this->load->view('admin/login/login',$data);
		}
	}
	//logout
	function logout() {
		$this->Admin_user_model->logout_this_login();
		header('location:'.base_url().'admin/');
	}
	//logout
	function agentlogout() {
		$this->Agent_user_model->logout_this_login();
		header('location:'.base_url().'admin/chat/');
	}
	//p	rofile view
	function profile() {
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
		$this->breadcrumbs->push('Profile', 'admin/user/profile');
		//v		iew single row with codition
		$data['user_detail']=$this->Admin_user_model->user_detail();
		$content=$this->load->view('admin/user/profile.tpl.php',$data,true);
		$this->render($content);
	}
	
	//p	rofile edit
	function profile_edit() {
		$this->check_user_page_access();
		if (isset($_FILES["image"]["name"]) && $_FILES["image"]["name"]!='') {
			$img = time().basename($_FILES["image"]["name"]);
			$target_file = $this->target_dir.$img;
			if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file))
				unlink($this->target_dir.$this->input->post('old_image', TRUE));
		} else {
			$img = $this->input->post('old_image', TRUE);
		}
		$admin_data	=	 array(
			'email'=>$this->input->post('email', TRUE),
		);
		$data = array(
			'name' => $this->input->post('name', TRUE),
			'address' => $this->input->post('address', TRUE),
			'timezone' => $this->input->post('timezone', TRUE),
			'phone' => $this->input->post('phone', TRUE),
			'img' => $img
		);
		$r = $this->General_model->update_data('cre_admin_user',$admin_data,array('id'=>$this->session->userdata('admin_user_id')));
		
		$r = $this->General_model->update_data('cre_admin_detail',$data,array('user_id'=>$this->session->userdata('admin_user_id')));


		$r = $this->General_model->update_data('cust_service_agent_cre',$admin_data,array('id'=>$this->session->userdata('admin_user_id')));
		
		$r = $this->General_model->update_data('cust_service_agent_detail',$data,array('user_id'=>$this->session->userdata('admin_user_id')));

		
		if($r) {
			$session_data = array(
				'user_name'=>$this->input->post('name', TRUE),
				'img'=>$img,
				'message_succ'=>'Update Successful'
			);
			$this->session->set_userdata($session_data);
			header('location:'.base_url().'admin/user/profile');
		}
	}
	//change password
	function change_password() {
		$this->check_user_page_access();
		//check new_password and confirm_password
		if ($this->input->post('new_password', TRUE)==$this->input->post('confirm_password', TRUE)) {
			//check current password
			$r = $this->Admin_user_model->check_password('cre_admin_user',array('id'=>$this->session->userdata('admin_user_id'),'password'=>md5($this->input->post('current_password', TRUE))));
			if ($r == 1) {
				$data = array(
					'password'=>md5($this->input->post('new_password', TRUE)),
				);
				$r = $this->General_model->update_data('cre_admin_user',$data,array('id'=>$this->session->userdata('admin_user_id')));
				$r = $this->General_model->update_data('cust_service_agent_cre',$data,array('id'=>$this->session->userdata('admin_user_id')));
				if ($r) {
					echo '<div class="box box-success">                <div class="box-header with-border">                  <h3 class="box-title">Password changed successfully</h3>                  <div class="box-tools pull-right">                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>                  </div>                </div>              </div>';
				}
			} else {
				echo '<div class="box box-danger">                <div class="box-header with-border">                  <h3 class="box-title">Current password is not correct</h3>                  <div class="box-tools pull-right">                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>                  </div>                </div>              </div>';
			}
		} else {
			echo '<div class="box box-danger">                <div class="box-header with-border">                  <h3 class="box-title">New password and confirm password not match</h3>                  <div class="box-tools pull-right">                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>                  </div>                </div>              </div>';
		}
	}
	// forgot password
	function forgot_password() {
		$view_data = array();
		if ($this->input->post('email', TRUE)!='') {
			$user = $this->General_model->view_data('cre_admin_user',array('email'=>$this->input->post('email', TRUE)));
			if (isset($user)&& count($user) > 0) {
				$user_id = $user[0]['id'];
				$user_name = $this->General_model->view_data('cre_admin_detail',array('user_id'=>$user_id));
				$user_name = $user_name[0]['name'];
				$token = md5('cre'.time());
				$email = $this->input->post('email', TRUE);
				$data = array('token'=>$token);
				if($this->General_model->update_data('cre_admin_user',$data,array('email'=>$this->input->post('email', TRUE))))
					$to =$email;
					$subject = "Recovery Password #Cresc Re";
					$message ='			<html>			<body>			<p> Hello '.$user_name.'</p>			<p>Please click on the below link to reset your password.</p>			<a href='.base_url().'admin/user/reset_password/'.$token.'>Click here</a>			<p>Thanks,</p>			<p>CreseRe Team</p>			</body>			</html>			';
					$headers = "MIME-Version: 1.0" . "\r\n";
					$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
					$headers .= 'From: <test@crescentek.net>' . "\r\n";
					// mail($to,$subject,$message,$headers);
					$user_name = 'User';
					$this->General_model->sendMail($to,$user_name,$subject,$message);
					$view_data['err']='<span style="color:green">please check your mail</span>';
				} else {
					$view_data['err']='<span style="color:red">Give proper email</span>';
				}
			}
			$this->load->view('admin/login/forgot_pass',$view_data);
		}
		//reset password
		function reset_password($token='') {
			$view_data = array();
			$user = $this->General_model->view_data('cre_admin_user',array('token'=>$token));
			if (isset($user)&& count($user)>0) {
				if($this->input->post('new_password', TRUE)!='') {
					if($this->input->post('new_password', TRUE)==$this->input->post('confirm_password', TRUE)) {
						$email= $user[0]['email'];
						$data=array('token'=>'','password'=>md5($this->input->post('new_password', TRUE)));
						if($this->General_model->update_data('cre_admin_user',$data,array('email'=>$email)))
							$view_data['err']='<span style="color:green">Password reset successfully</span>';
					} else {
						$view_data['err']='<span style="color:red">Passwords not matching </span>';
					}
				}
			} else {
				header('location:'.base_url());
			}
			$this->load->view('admin/login/reset_pass',$view_data);
		}
		public function delete_customer_for_chat($data){
			$this->db->where('id', $data);
		    $this->db->update('user', array('is_active_for_chat'=>'0'));
		}
	}


