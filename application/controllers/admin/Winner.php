<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Winner extends My_Controller 
{
  function __construct()
    {
	parent::__construct();
	$this->load->model('User_model');
    $this->target_dir=BASEPATH.'../upload/usr/';
	}

	public function index()
	{
		$this->check_user_page_access();
		// add breadcrumbs
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Winner List', 'winner_list');
		$data['active']='winner_list';
		$data['page_name']='Winner List';

		$data['list']=$this->User_model->winner_result();
		
		// echo "<pre>";
		// print_r($data['list']);exit;
		$content=$this->load->view('admin/result/winner_list.tpl.php',$data,true);
		$this->render($content);
	}
	
	//delete
	public function delete($id){
		$this->check_user_page_access();
		
		$data=array(
			'is_active'=>1
		);
		$r=$this->General_model->update_data('bet',$data,array('id'=>$id));
		if($r) {
			$this->session->set_userdata('message_succ', 'Delete Successfull');	
			redirect(base_url().'admin/Winner');
		} else {
			$this->session->set_userdata('message_err', 'Failure to delete');	
			redirect(base_url().'admin/Winner');
		}
	}
	
	function security_pass_bluk($id){
		echo '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="security_pass_bluk" name="security_pass_bluk" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Admin Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required >
								<input type="hidden" id="action_value" class="form-control" name="action_value" value="'.$id.'" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
	}
	
	function get_reset_data(){
		
		$security_password 		= $_REQUEST['security_password'];
		$action_value 			= $_REQUEST['action_value'];
		
		$security_password_db = $this->General_model->view_all_data('admin_security_password','id','asc');
		if($security_password == $security_password_db[0]['password']){
			$action_imp = explode(',',$action_value);
			if(!empty($action_imp)){
				foreach($action_imp as $act_imp){
					$data=array(
						'is_active'=>1
					);
					$r=$this->General_model->update_data('bet',$data,array('id'=>$act_imp));
				}
			}
			
			$data_value = 'success';			
		} else{
			$data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="security_pass" name="security_pass" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Admin Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
								<input type="hidden" id="action_value" class="form-control" name="action_value" value="'.$action_value.'" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
		}	
		echo $data_value;
	}
	
}
 