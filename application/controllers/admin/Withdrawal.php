<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Withdrawal extends My_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('User_model');
    }
    public function index() {
        // add breadcrumbs
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('game List', 'admin/game');
        $data['active'] = 'Withdrawal';
        $data['page_name'] = 'Withdrawal';
        //showing game list from function which is created in general_model
    }
    public function withdrawal_fee() {
        // add breadcrumbs
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Withdrawal Fee', 'admin/withdrawal_fee');
        $data['active'] = 'withdrawal_fee';
        $data['page_name'] = 'Withdrawal Fee';
        //showing game list from function which is created in general_model
        $fee_type = 'withdrawal_fee';
        $data['withdrawal_fee']=$this->General_model->view_data('admin_fee_master',array('fee_type'=>$fee_type));
        $content = $this->load->view('admin/withdrawal/withdrawal_fee.tpl.php',$data,true);
        $this->render($content);
    }
    
    // Update Point Percentage
    public function update_withdrawal_fee(){
    	$this->check_user_page_access();
    	if (!empty($_POST)) {
    		$fee_type = 'withdrawal_fee';
    		$fee_calc_type = $_POST['calculation_type'];
    		$fee_value = $_POST['fee_value'];

    		for ($i=0; $i < count($_POST['fee_value']); $i++) {
    			$fee_range = $_POST['min-range'][$i].'-'.$_POST['max-range'][$i];
    			$get_manual_purchase_fee = $this->General_model->view_single_row('admin_fee_master',array('id' => $_POST['id'][$i]),'*');
    			if ($get_manual_purchase_fee && count($get_manual_purchase_fee) != 0) {
    				$add_manual_purchase_fee = array(
    					'fee_type'=>$fee_type,
    					'fee_range'=>$fee_range,
    					'fee_calc_type'=>$fee_calc_type[$i],
    					'fee_value'=>$fee_value[$i],
			        );
					$r = $this->General_model->update_data('admin_fee_master',$add_manual_purchase_fee,array('id'=>$_POST['id'][$i]));
    			} else {
    				$add_manual_purchase_fee = array(
    					'fee_type'=>$fee_type,
    					'fee_range'=>$fee_range,
    					'fee_calc_type'=>$fee_calc_type[$i],
    					'fee_value'=>$fee_value[$i],
			        );
					$r = $this->General_model->insert_data('admin_fee_master',$add_manual_purchase_fee);
    			}
    		}
    		
    	}
        $data['active']='points_percentage';
        if ($r) {
            $this->session->set_userdata('message_succ', 'Update successfull');
            header('location:'.base_url().'admin/withdrawal/withdrawal_fee');
        } else {
            $this->session->set_userdata('message_err', 'Failure to update');
            header('location:'.base_url().'admin/withdrawal/withdrawal_fee');
        }
    }
}