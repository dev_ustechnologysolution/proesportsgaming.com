<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Attributes extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->target_dir=BASEPATH.'../upload/profile_img/';
	}
	 
	function get_attributes()
	{

			
		$options = array(
      'select' => 'at.title, at.type, at.image_upload, at.attr_order, paot.attr_id, paot.option_label, paot.option_id,paot.extra_amount,paot.opt_order',
      'table' => 'attributes_tbl at',
      'join' => array(
      	'product_attributes_opt_tbl paot' => 'paot.attr_id = at.id',
      ),
      'order' => array('at.attr_order' => 'asc', 'paot.opt_order' => 'asc'),
      'where' => array('at.status' => 1)
    );
    $attributes_list = $this->General_model->commonGet($options);
     $result = array();
	  
    foreach($attributes_list as $key => $attribute) {
    	
    	foreach ($attributes_list as $skey => $arrayAttribute) {
    		if($attribute->attr_id == $arrayAttribute->attr_id ){
				$result[$attribute->attr_id]['attr_order']=$attribute->attr_order;
				$result[$attribute->attr_id]['title']=$attribute->title;
				$result[$attribute->attr_id]['type']=$attribute->type;
				$result[$attribute->attr_id]['image_upload']=$attribute->image_upload;
    			$result[$attribute->attr_id]['option_list'][$attribute->option_id]['option_label'] = $attribute->option_label; 
    			$result[$attribute->attr_id]['option_list'][$attribute->option_id]['extra_amount'] = $attribute->extra_amount; 
    			$result[$attribute->attr_id]['option_list'][$attribute->option_id]['opt_order'] = $attribute->opt_order; 
    		}
    	}
    	$result[$attribute->attr_id]['option_list'] = array_values($result[$attribute->attr_id]['option_list']);
    	
    }
    
    $attributes_list = array_values($result);
   
		if(!empty($attributes_list))
		{
			$response=array('status' => TRUE,
							'status_code'=>200,
	                   		'attr_list'=>$attributes_list
	                );
	        echo json_encode($response,JSON_UNESCAPED_SLASHES);

		}
		else
		{
			$response=array('status' => FALSE,
							'status_code'=>400,
                			'message' => 'attribute list is Empty');
    			echo json_encode($response);
		}
		

	}
	
	
}
