<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Cart extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->target_dir = BASEPATH.'../upload/products/';
	}

	//cart list api 
	function get_all_cart_products($user_id="")
	{
		if($user_id)
		{

			$data = $this->Cart_model->cart_items($user_id);
			foreach ($data as $key => $value) {
					$data[$key]['sel_variation'] = $this->convertSelVariantToArray($data[$key]['sel_variation']);
					$data[$key]['sel_variation_amount'] = $this->convertSelVariantToArray($data[$key]['sel_variation_amount']);
		        	$data[$key]['product_image']= base_url()."upload/products/".$value['product_image'];
		        }
		  
			if(!empty($data))
			{
				$response=array('status' => TRUE,
								'status_code'=>200,
		                   		'cart_list'=>$data
		                );
		        echo json_encode($response,JSON_UNESCAPED_SLASHES);

			}
			else
			{
				$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'Your Cartlist is Empty');
	    		echo json_encode($response);
			}
		}
		else
		{
			$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'Please enter user id');
	    		echo json_encode($response);
		}
		

	}

	//cart product quantity update api
	function cart_product_qty_update()
	{
		$this->form_validation->set_error_delimiters('', '');
    	 $this->form_validation->set_rules("user_id", "user id", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("product_id", "product id", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("cart_item_id", "cart item id", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("prdct_qty", "quantity", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("extra_amount", "extra amount", "required", array('required' => 'Please enter %s.'));
    	   $array = array();
  		if($this->form_validation->run())
  		{
			  $cart_item = $this->Cart_model->cart_items();
		      $getqty_col = array_column($cart_item, 'qty', 'id');
		      $getprdct_col = array_column($cart_item, 'product_id', 'id');
		      $product_qty = 0;
		      foreach ($getprdct_col as $key => $gc) {
		       ($gc == $_POST['product_id'] && $key != $_POST['cart_item_id']) ? $product_qty += $getqty_col[$key] : '';
		      }
		      $arr = array( 'product_id' => $_POST['product_id'], 'get' => 'row');
		      $product_detail = $this->Product_model->productDetail($arr);
		      $data['item'] = $_POST;
		      if (($_POST['prdct_qty']+$product_qty) <= $product_detail->qty) {
		        if ($_POST['user_id'] !='') {
		          $cart_update = $this->General_model->update_data('cart_item', array('qty' => $_POST['prdct_qty']), array('id' => $_POST['cart_item_id']));
		        } else {
		          $_SESSION['cart_item_list'][$_POST['cart_item_id']]['qty'] = $_POST['prdct_qty'];
		        }
		        $data['data_update'] = 'Item Update successfully';
		        $prdcts_total_price = ($product_detail->fee + $product_detail->price + $_POST['extra_amount']) * $_POST['prdct_qty'];
		        $data['prdcts_total_price'] = number_format($prdcts_total_price,2,".","");
		        $response=array('status' => TRUE,
								'status_code'=>200,
		                   		'cart_list'=>$data
		                );
		        echo json_encode($response,JSON_UNESCAPED_SLASHES);

		      } else {
		        // $data['err'] = $exceed_prdct = "You can't add more items";
		        $response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => "You can't add more items");
	    		echo json_encode($response);
		      }
		}
  		else
		{
				$errors = $this->form_validation->error_array();
    			$fields = array_keys($errors);
    			$err_msg = $errors[$fields[0]];
			   	$array = array(
			   	
			    'error'    => true,
			    'status_code'=>400,
			    'error_message'=>$err_msg = $errors[$fields[0]],
			    
   			);
			echo json_encode($array);
		}
	}

	//delete product from cart api
	function cart_product_delete($user_id="",$id='')
	{

	 if($user_id)
	 {
		
			if(!empty($id))
			{
				
				$delete = $this->Product_model->remove_item_cart($id,'single_product');
				if($delete)
				{
					$response=array('status' => TRUE,
									'status_code'=>200,
			                   		'message'=>'Removed Item from cart'
			                );
			        echo json_encode($response,JSON_UNESCAPED_SLASHES);
				}
				else
				{

					 $response=array('status' => FALSE,
									'status_code'=>400,
		                			'message' => "Unable to remove");
		    		echo json_encode($response);
				}

			}
			else
			{
				
				$delete = $this->Product_model->clear_cart($user_id,'all_product');
				if($delete)
				{
					$response=array('status' => TRUE,
									'status_code'=>200,
			                   		'message'=>'Your Cartlist Cleared'
			                );
			        echo json_encode($response,JSON_UNESCAPED_SLASHES);
				}
				else
				{

					 $response=array('status' => FALSE,
									'status_code'=>400,
		                			'message' => "Unable to Clear");
		    		echo json_encode($response);
				}
	  

			}
		}
		else
		{
			 $response=array('status' => FALSE,
									'status_code'=>400,
		                			'message' => "Please enter user id");
		    		echo json_encode($response);
		}

	}
	
}
