<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Category extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->target_dir=BASEPATH.'../upload/profile_img/';
	}
	 
	//get all main categories
	function get_all_categories($user_id=0)
	{

		
		$data = $this->Product_model->get_all_Categories($user_id);
		  
		if(!empty($data))
		{
			foreach ($data as $k => $value) {
				$subcats = $this->Product_model->getCategoryByParent($value['id']);
				$data[$k]['subcategories'] = $subcats;
			}
			$response=array('status' => TRUE,
							'status_code'=>200,
	                   		'category_list'=>$data
	                );
	        echo json_encode($response,JSON_UNESCAPED_SLASHES);

		}
		else
		{
			$response=array('status' => FALSE,
							'status_code'=>400,
                			'message' => 'Category Not Available');
    			echo json_encode($response);
		}
		

	}

	//get sub categories
	function get_all_subcategories($parent_id="")
	{

	  if($parent_id)
	  {
			$data = $this->Product_model->getCategoryByParent($parent_id);
			  
			if(!empty($data))
			{
				$response=array('status' => TRUE,
								'status_code'=>200,
		                   		'subcategory_list'=>$data
		                );
		        echo json_encode($response,JSON_UNESCAPED_SLASHES);

			}
			else
			{
				$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'Subcategory Not Available');
	    			echo json_encode($response);
			}
	}
	else
	{
			$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'Please enter main category id');
	    			echo json_encode($response);
	}
		

	}	

	//add category api
	function add_category()
	{
		
		$this->form_validation->set_error_delimiters('', '');
    	 $this->form_validation->set_rules("user_id", "user id", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("name", "name", "required", array('required' => 'Please enter %s.'));
    	   $array = array();
  		if($this->form_validation->run())
  		{
				$query = $this->db->query("SELECT MAX(sort_num) as sort_num FROM product_categories_tbl");
				$row = $query->row();
				$data = array(
					'user_id' => (!empty($_POST['user_id'])) ? $_POST['user_id'] : 0,
					'name' => trim($_REQUEST['name']),
					'status' => 1,
					'sort_num' => $row->sort_num +1,
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				);
				
				
				$r = $this->General_model->insert_data('product_categories_tbl',$data);
				if(isset($r)) {
						$response=array('status' => TRUE,
										'status_code'=>200,
	                    				'message' => 'Category Insert successfully.');
	        			echo json_encode($response);
	        		}
	        		else {
							$response=array('status' => FALSE,
											'status_code'=>400,
	                    					'message' => 'Failure to insert');
	        			echo json_encode($response);
					}
	}
	else
		{
				$errors = $this->form_validation->error_array();
    			$fields = array_keys($errors);
    			$err_msg = $errors[$fields[0]];
			   	$array = array(
			   	
			    'error'    => true,
			    'status_code'=>400,
			    'error_message'=>$err_msg = $errors[$fields[0]],
			    
   			);
			echo json_encode($array);
		}
	
	
	}

	//add category api
	function add_subcategory()
	{
		
		 $this->form_validation->set_error_delimiters('', '');
    	 $this->form_validation->set_rules("user_id", "user id", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("name", "name", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("parent_id", "parent id", "required", array('required' => 'Please enter %s.'));
    	   $array = array();
  		if($this->form_validation->run())
  		{
				$query = $this->db->query("SELECT MAX(sort_num) as sort_num FROM product_categories_tbl");
				$row = $query->row();
				$data = array(
					'user_id' => (!empty($_POST['user_id'])) ? $_POST['user_id'] : 0,
					'parent_id' =>$_POST['parent_id'],
					'name' => trim($_REQUEST['name']),
					'status' => 1,
					'sort_num' => $row->sort_num +1,
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				);

				$r = $this->General_model->insert_data('product_categories_tbl',$data);
				if(isset($r)) {
						$response=array('status' => TRUE,
										'status_code'=>200,
	                    				'message' => 'Subcategory Insert successfully.');
	        			echo json_encode($response);
	        		}
	        		else {
							$response=array('status' => FALSE,
											'status_code'=>400,
	                    					'message' => 'Failure to insert');
	        			echo json_encode($response);
					}
	}
	else
	{
			$errors = $this->form_validation->error_array();
    			$fields = array_keys($errors);
    			$err_msg = $errors[$fields[0]];
			   	$array = array(
			   	
			    'error'    => true,
			    'status_code'=>400,
			    'error_message'=>$err_msg = $errors[$fields[0]],
			    
   			);
			echo json_encode($array);
	}
	
	
	}

	//update category api
	function edit_category()
	{
		
		 $this->form_validation->set_error_delimiters('', '');
    	 $this->form_validation->set_rules("name", "name", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("id", "id", "required", array('required' => 'Please enter %s.'));
    	   $array = array();
  		if($this->form_validation->run())
  		{
				$id = $this->input->post('id', TRUE);
				$data = array(
					'parent_id' => (!empty($_POST['parent_id'])) ? $_POST['parent_id'] : 0,
					'name' => trim($_POST['name']),
					'updated_at' => date('Y-m-d H:i:s')
				);
		
				$r = $this->General_model->update_data('product_categories_tbl',$data,array('id'=>$id));
				
				if(isset($r)) {
						$response=array('status' => TRUE,
										'status_code'=>200,
	                    				'message' => 'Update successfully.');
	        			echo json_encode($response);
	        		}
	        		else {
							$response=array('status' => FALSE,
											'status_code'=>400,
	                    					'message' => 'Failure to update');
	        			echo json_encode($response);
					}
	}
	else
	{
			$errors = $this->form_validation->error_array();
    			$fields = array_keys($errors);
    			$err_msg = $errors[$fields[0]];
			   	$array = array(
			   	
			    'error'    => true,
			    'status_code'=>400,
			    'error_message'=>$err_msg = $errors[$fields[0]],
			    
   			);
			echo json_encode($array);
	}
	
	
	}

	//delete category api
	 function delete_categories($id="") {

	 if($id)
	 {
		 	$get_sub_cat=$this->Product_model->getCategoryByParent($id);
		 	
		 	if(!empty($get_sub_cat))
		 	{

		 		foreach ($get_sub_cat as $key => $value) {
		 	
		 			 $this->General_model->delete_data('product_categories_tbl',array('id'=> $value['id']));
		 		}
				 
				 $r = $this->General_model->delete_data('product_categories_tbl',array('id'=> $id));
				 if($r) {
							$response=array('status' => TRUE,
											'status_code'=>200,
		                    				'message' => 'Delete successfully.');
		        			echo json_encode($response);
		        		}
		        		else {
								$response=array('status' => FALSE,
												'status_code'=>400,
		                    					'message' => 'Failure to delete');
		        			echo json_encode($response);
						}
				

		 	}
		 	else
		 	{
		 		$r = $this->General_model->delete_data('product_categories_tbl',array('id'=> $id));
		 		if($r) {
							$response=array('status' => TRUE,
											'status_code'=>200,
		                    				'message' => 'Delete successfully.');
		        			echo json_encode($response);
		        		}
		        		else {
								$response=array('status' => FALSE,
												'status_code'=>400,
		                    					'message' => 'Failure to delete');
		        			echo json_encode($response);
						}
			}
		}
		else
		{
					$response=array('status' => FALSE,
									'status_code'=>400,
		                    		 'message' => 'Please enter category id');
		        			echo json_encode($response);

		}
		
	}

	 function category_status_update()
	 {

	 	$this->form_validation->set_error_delimiters('', '');
    	$this->form_validation->set_rules("id", "id", "required", array('required' => 'Please enter %s.'));
    	$this->form_validation->set_rules("status", "status", "required", array('required' => 'Please enter %s.'));
    	   $array = array();
  		if($this->form_validation->run())
  		{	

		 	$get_sub_cat=$this->Product_model->getCategoryByParent($_POST['id']);
		 	
		 	if(!empty($get_sub_cat))
		 	{

		 		foreach ($get_sub_cat as $key => $value) {
		 	
		 			 
		 			  $data['status'] = $_POST['status'];
					  $r = $this->General_model->update_data('product_categories_tbl',$data,array('id'=>$value['id']));
		 		}
				 
				 $data['status'] = $_POST['status'];
				$r = $this->General_model->update_data('product_categories_tbl',$data,array('id'=>$_POST['id']));
				 if($r) {
							$response=array('status' => TRUE,
											'status_code'=>200,
		                    				'message' => 'Update successfully.');
		        			echo json_encode($response);
		        		}
		        		else {
								$response=array('status' => FALSE,
												'status_code'=>400,
		                    					'message' => 'Failure to Update');
		        			echo json_encode($response);
						}
				

		 	}
		 	else
		 	{
		 		$data['status'] = $_POST['status'];
				$r=$this->General_model->update_data('product_categories_tbl',$data,array('id'=>$_POST['id']));
		 		if($r) {
							$response=array('status' => TRUE,
											'status_code'=>200,
		                    				'message' => 'Update successfully.');
		        			echo json_encode($response);
		        		}
		        		else {
								$response=array('status' => FALSE,
												'status_code'=>400,
		                    					'message' => 'Failure to Update');
		        			echo json_encode($response);
						}
			}
		}
		else
		{
				$errors = $this->form_validation->error_array();
	    			$fields = array_keys($errors);
	    			$err_msg = $errors[$fields[0]];
				   	$array = array(
				   	
				    'error'    => true,
				    'status_code'=>400,
				    'error_message'=>$err_msg = $errors[$fields[0]],
				    
	   			);
				echo json_encode($array);
		}
	
				
	}
}
	