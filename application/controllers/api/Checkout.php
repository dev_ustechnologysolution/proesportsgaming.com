<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Checkout extends My_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('My_model');
        $this->load->model('Email_model');
        $this->load->model('Paypal_payment_model');
        $this->load->library("MyAuthorize");
        $this->load->library('user_agent');
        $this->load->library('form_validation');
    }
    
    public function paypal_request(){
            $this->form_validation->set_error_delimiters('', '');
            $this->form_validation->set_rules("user_id", "User id", "required", array('required' => 'Please enter %s.'));
            $this->form_validation->set_rules("name", "Name", "required", array('required' => 'Please enter %s.'));
            $this->form_validation->set_rules("email", "Email", "trim|required|valid_email", array('required' => 'Please enter %s.'));
            $this->form_validation->set_rules("phone", "Phone", "required", array('required' => 'Please enter %s.'));
            $this->form_validation->set_rules("address", "Address", "required", array('required' => 'Please enter %s.'));
            $this->form_validation->set_rules("country", "Country", "required", array('required' => 'Please enter %s.'));
            $this->form_validation->set_rules("countrysortname", "Country Sortname", "required", array('required' => 'Please enter %s.'));
            $this->form_validation->set_rules("state", "State", "required", array('required' => 'Please enter %s.'));
            $this->form_validation->set_rules("city", "City", "required", array('required' => 'Please enter %s.'));
            $this->form_validation->set_rules("zip_code", "Zipcode", "required", array('required' => 'Please enter %s.'));
            $this->form_validation->set_rules("shipping_location_type", "Shipping Location Type", "required", array('required' => 'Please enter %s.'));

            $ship_to_diffadd_check = $this->input->post('ship_to_diffadd_check');
            if(isset($ship_to_diffadd_check) && $ship_to_diffadd_check == "on"){
                $this->form_validation->set_rules("shippping_address", "Shippping Address", "required", array('required' => 'Please enter %s.'));
                $this->form_validation->set_rules("shipping_country", "Shippping Country", "required", array('required' => 'Please enter %s.'));
                $this->form_validation->set_rules("shipping_countrysortname", "Shippping Country Sortname", "required", array('required' => 'Please enter %s.'));
                $this->form_validation->set_rules("shipping_state", "Shippping State", "required", array('required' => 'Please enter %s.'));
                $this->form_validation->set_rules("shiptocity", "Shippping City", "required", array('required' => 'Please enter %s.'));
                $this->form_validation->set_rules("shiptozip", "Shippping Zipcode", "required", array('required' => 'Please enter %s.'));
            }

            if($this->form_validation->run())
            {
                $user_id = $this->input->post('user_id');
                $country = $this->input->post('country');
                $data['shipping_fee'] = $this->Cart_model->calc_shipping_mdl(array('country' => $country));
                $shipping_fee = explode('_', $data['shipping_fee']);
                $cart_item = $this->cart_items($user_id);
                $prdct_ref_sel = array_column($cart_item,'ref_seller_id');
                $prdct_prc = array_column($cart_item,'amount');
                $prdct_fee = array_column($cart_item,'product_fee');
                $prdct_qty = array_column($cart_item,'qty');
                $prdct_vrtn_arr = array_column($cart_item,'sel_variation_amount');
                $prdct_vrtn_amt = [];
                foreach ($prdct_vrtn_arr as $key => $pva) {
                    $sv = explode('&', $pva);
                    $prdct_vrtn_amt[] = end(explode('_', $sv[2]));
                }
                $SUBTOTAL = array_sum(array_map(function($w, $x, $y, $z) { return $w * ($x + $y + $z); }, $prdct_qty, $prdct_prc, $prdct_fee, $prdct_vrtn_amt));
                $total_variation_amount = array_sum(array_map(function($x, $y) { return $x * $y; }, $prdct_qty, $prdct_vrtn_amt));
                $total_fee_amount = array_sum(array_map(function($x, $y) { return $x * $y; }, $prdct_qty, $prdct_fee));
                $shipping_location_type = $this->input->post('shipping_location_type');
                $ship_data = 0;
                if(!empty($cart_item)){
                     foreach ($cart_item as $key => $ci) { 
                      $sv = explode('&', $ci['sel_variation_amount']);
                      $variation_amount = end(explode('_', $sv[2]));
                      $item_amount = $ci['amount'] + $variation_amount;
                      $ci['prdcts_total_price'] = number_format((float) $ci['qty'] * ($ci['amount']+$ci['product_fee']+$variation_amount), 2, '.', '');
                      ($ci['product_exist_id'] !='') ? $SUBTOTAL += $item_details['prdcts_total_price'] : '';

                      if ($ci['seller_id'] == 0) {
                        $shipping_admin = ($shipping_location_type == 'inside') ? 'inside_us_product_fees' : 'outside_us_product_fees';
                        $admin_item_name_arr[] = $ci['product_name'];
                      }

                      $vendor_shipping_fee_arr[$ci['seller_id']] = array(
                        'local_product_fee' => $ci['local_product_fee'],
                        'international_product_fee' => $ci['international_product_fee'],
                        'product_name' => array($ci['product_name']),
                        'sold_by_accno' => $ci['account_no'],
                        'sold_by' => ($ci['display_name'] != '' && $ci['display_name'] != null) ? $ci['display_name'] : $ci['username'],
                      );
                    }
                }
              
                // Admin products shipping calculation
                $admin_shipping_fee_arr = $this->Cart_model->calc_shipping_mdl('');
                if (!empty($admin_shipping_fee_arr) && isset($shipping_admin) && $shipping_admin !='') {
                  foreach ($admin_shipping_fee_arr as $key => $asfa) {
                    if ($asfa['option_title'] == $shipping_admin) {
                      $shipping_cal_type = explode('_', $asfa['option_value']); 
                      $shipping_val = ($shipping_cal_type[0] == 1) ? $shipping_cal_type[1]: $SUBTOTAL*$shipping_cal_type[1]/100;
                      $ship_data += $shipping_val;
                    }
                  }
                  $shipinfo[] = array(
                    'item_name' => $admin_item_name_arr,
                    'shipping_charge' => $shipping_val,
                    'sold_by_accno' => 0,
                    'sold_by' => 'Esports',
                  );
                }
                // vendor products shipping calculation
                if (!empty($vendor_shipping_fee_arr)) {
                  foreach ($vendor_shipping_fee_arr as $key => $vsfa) {
                    if ($key != ' ') {
                      $shipping_cal_type = ($shipping_location_type == 'inside') ? explode('_', $vsfa['local_product_fee']) : explode('_', $vsfa['international_product_fee']);
                      $ship_charge = ($shipping_cal_type[0] == 1) ? $shipping_cal_type[1] : $SUBTOTAL*$shipping_cal_type[1]/100;
                      $ship_data += $ship_charge;
                      $shipinfo[] = array(
                        'item_name' => $vsfa['product_name'],
                        'shipping_charge' => $ship_charge,
                        'sold_by_accno' => $vsfa['sold_by_accno'],
                        'sold_by' => $vsfa['sold_by'],
                      );
                    }
                  }
                }
                $SHIPPING = number_format((float)$ship_data, 2, '.', '');
                $total = number_format((float)($SHIPPING)+($SUBTOTAL), 2, '.', '');
                $order_calc = array(
                    'shipping_fee' => $SHIPPING,
                    'subtotal' => $SUBTOTAL,
                    'total' => $total
                );
                $pymnt_opt = array(
                    'user_id' => $user_id,
                    'shipping_fee' => $SHIPPING,
                    'cart_item' => $cart_item,
                    'int' => $int,
                    'prdct_prc' => $prdct_prc,
                    'SUBTOTAL' => $SUBTOTAL,
                    'total' => $total,
                    'ref_id' => $prdct_ref_sel[0],
                    'seller_id' => $cart_item[0]['seller_id'],
                );
                $pymnt_opt['payment_method'] = 'paypal';
                $pymnt_opt['fromwallet'] = 0;
                $pymnt_opt['total_variation_amount'] = $total_variation_amount;
                $pymnt_opt['total_fee_amount'] = $total_fee_amount;
                $this->paypal_checkout($pymnt_opt);
        }else{
            $errors = $this->form_validation->error_array();
            $fields = array_keys($errors);
            $err_msg = $errors[$fields[0]];
               $array = array(
                
                'error'    => true,
                'status_code'=>400,
                'error_message'=>$err_msg = $errors[$fields[0]],
                
            );
            echo json_encode($array);
        }
    }
    
    function paypal_checkout($paypl_opt) {
        $fromwallet = $paypl_opt['fromwallet'];
        $payment_method = $paypl_opt['payment_method'];
        $user_id = $paypl_opt['user_id'];
        $cart_item = $paypl_opt['cart_item'];
        $shipping_fee = $paypl_opt['shipping_fee'];
        $int = $paypl_opt['int'];
        $prdct_prc = $paypl_opt['prdct_prc'];
        $SUBTOTAL = $paypl_opt['SUBTOTAL'];
        $total = $paypl_opt['total'];

        $ship_to_diffadd_check = $this->input->post('ship_to_diffadd_check');
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['phone'] = $this->input->post('phone');

        $billing_details[] =  array(
            'address' => $this->input->post('address'),
            'zip' => $this->input->post('zip_code'),
            'country' => $this->input->post('country'),
            'countrycode' => $this->input->post('countrysortname'),
            'state' => $this->input->post('state'),
            'city' =>  $this->input->post('city'),
        );
        if($ship_to_diffadd_check == "on"){
            $shipping_details[] = array(
               'address' => $this->input->post('shippping_address'),
               'zip' => $this->input->post('shiptozip'),
               'country' => $this->input->post('shipping_country'),
               'countrycode' => $this->input->post('shipping_countrysortname'),
               'state' => $this->input->post('shipping_state'),
               'city' =>  $this->input->post('shiptocity'),
            );
        }
        if(!empty($cart_item)){
            foreach ($cart_item as $key => $ci) {
                $set_cartitem[] = array(
                    'name' => $ci['product_name'],
                    'price' => $ci['amount'],
                    'number' => $ci['product_id'],
                    'quantity' => $ci['qty'],
                    'currency' => "USD",
                    'itemurl' => base_url().'Store/view_product/'.$ci['product_id'],
                );
            }
        }
        if(!empty($shipping_details) && isset($ship_to_diffadd_check) && $ship_to_diffadd_check == "on"){
            $data['billing_details'] = $billing_details;
            $data['shipping_details'] = $shipping_details;
        }else{
             $data['shipping_details'] = $billing_details;
        }
    
        $data['items'] = $set_cartitem;
        $data['subscribe_amount'] = $total;        
        $data['shipping_charge'] = $shipping_fee;
        $data['returnurl'] = $data['cancelurl'] = base_url().'checkout/get_payment_data/';

        $custom_arr = array(
            'userid' => $user_id,
            'fromwallet' => $fromwallet,
            'payment_method' => $payment_method,
            'ref_id' => $paypl_opt['ref_id'],
            'seller_id' => $paypl_opt['seller_id'],
            'return_url' => base_url().'checkout/'.$_SESSION['shipping_location_type']
        );

        $data['custom'] = json_encode($custom_arr);
       
        $data['title'] = 'Pay Amount.';
        $data['notifyurl'] = '';
        $data['billingtype'] = 'MerchantInitiatedBilling';
        $data['ref_id'] = $paypl_opt['ref_id'];
        $data['seller_id'] = $paypl_opt['seller_id'];
        $data['total_variation_amount'] = $paypl_opt['total_variation_amount'];
        $data['total_fee_amount'] = $paypl_opt['total_fee_amount'];

        $response = array(
                    'status' => TRUE,
                    'status_code' => 200,
                    'paypal_data' => $data
                );
        echo json_encode($response ,JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
    }

    function cart_items($user_id) {
      $this->db->select('ci.*, c.user_id, pt.id as product_exist_id, pt.seller_id, ud.local_product_fee, ud.international_product_fee, ud.display_name, ud.name as username, ud.country, cn.sortname, cn.countryname, ud.display_name, ud.account_no');
      $this->db->from('cart_item ci');
      $this->db->join('cart c', 'c.id = ci.cart_id');
      $this->db->join('products_tbl pt', 'pt.id = ci.product_id', 'left outer');
      $this->db->join('user_detail ud', 'ud.user_id = pt.seller_id', 'left outer');
      $this->db->join('country cn', 'cn.country_id = ud.country', 'left outer');
      $this->db->where('c.user_id',$user_id);
      $this->db->order_by('ci.created_at');
      $getitems = $this->db->get()->result_array();
      return $getitems;
    }

    public function paypal_response() {
                $user_id = $this->input->post('user_id');
                $wallet_amount = $this->input->post('wallet_amount');
                $payment_method = $this->input->post('payment_method');
                $ref_id = $this->input->post('ref_id');
                $seller_id = $this->input->post('seller_id');
                $payerid = $this->input->post('payer_id');
                $transactionid =  $this->input->post('transaction_id');
                $plc_ckordr = array(
                    'paypal_deducted' => $this->input->post('paypal_deducted'),
                    'payment_method' => $payment_method,
                    'total' => $wallet_amount,
                    'transactionid' => $transactionid,
                    'payerid' => $payerid,
                    'ref_id' => $ref_id,
                    'user_id' => $user_id,
                    'seller_id' => $seller_id
                );
                $this->wallet_checkout($plc_ckordr);
    }

    function wallet_checkout($wallet_opt) {
        $user_id = $wallet_opt['user_id'];
        $grand_total = $wallet_opt['total'];
        $payment_method = $wallet_opt['payment_method'];
        $paypal_deducted = $wallet_opt['paypal_deducted'];
        $transactionid = (isset($wallet_opt['transactionid']) && $wallet_opt['transactionid'] != '') ? $wallet_opt['transactionid'] : '';
        $payerid = (isset($wallet_opt['payerid']) && $wallet_opt['payerid'] != '') ? $wallet_opt['payerid'] : '';
        if ($user_id != 0) {
            $rem_bal = $this->input->post('total_balance') - $grand_total;
            $update_data['total_balance'] = number_format((float) $rem_bal, 2, '.', '');
            $this->General_model->update_data('user_detail', $update_data, array('user_id' => $user_id));
        }
        
        $plc_ckordr = array(
            'user_id' => $user_id,
            'wallet_deducted' => $grand_total,
            'paypal_deducted' => $paypal_deducted,
            'payerid' => $payerid,
            'transactionid' => $transactionid,
            'payment_method' => 1,
            'ref_id' => $wallet_opt['ref_id'],
            'seller_id' => $wallet_opt['seller_id']
        );
        $this->place_ckout_order($plc_ckordr);
    }
  
    function place_ckout_order($plc_ckordr) {
        $card_data = $plc_ckordr['card_data'];
        $ref_seller_id = $plc_ckordr['ref_id'];
        $seller_id = $plc_ckordr['seller_id'];
        $wallet_deducted = number_format((float) $plc_ckordr['wallet_deducted'], 2, '.', '');
        $paypal_deducted = number_format((float) $plc_ckordr['paypal_deducted'], 2, '.', '');
        $card_deducted = number_format((float) $plc_ckordr['card_deducted'], 2, '.', '');
        $payment_method = $plc_ckordr['payment_method'];

        $payerid = $plc_ckordr['payerid'];
        $paypal_transaction_id = $plc_ckordr['transactionid'];
        $card_transaction_id = $plc_ckordr['transaction_id'];
        $wbsite_transaction_id = $this->db->select_max('ot.transaction_id')->from('orders_tbl ot')->get()->row()->transaction_id+1;
        $user_id = $plc_ckordr['user_id'];
        
        $shippaddress= array(
            'address' => $this->input->post('shipping_address'),
            'country' => $this->input->post('shipping_country'),
            'state' => $this->input->post('shipping_state'),
            'city' => $this->input->post('shipping_city'),
            'zip_code' => $this->input->post('shipping_phone'),
        );
        $billaddress= array(
            'address' => $this->input->post('billing_address'),
            'country' => $this->input->post('billing_country'),
            'state' => $this->input->post('billing_state'),
            'city' => $this->input->post('billing_city'),
            'zip_code' => $this->input->post('billing_phone'),
        );
        $cust_info = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone'),
        );
        $subtotal = $this->input->post('order_subtotal');
        $shipping_fee = $this->input->post('order_shipping_fee');
        $grand_total = $this->input->post('order_total');

        $insert_order = array(
            'transaction_id' => $wbsite_transaction_id,
            'paypal_payer_id' => $payerid,
            'paypal_transaction_id' => $paypal_transaction_id,
            'card_transaction_id' => $card_transaction_id,
            'sold_by' => $seller_id,
            'ref_seller_id' => $ref_seller_id,
            'user_id' => $user_id,
            'name' => $cust_info['name'],
            'email' => $cust_info['email'],
            'phone' => $cust_info['phone'],
            'shipping_address' => $shippaddress['address'],
            'shipping_country' => $shippaddress['country'],
            'shipping_state' => $shippaddress['state'],
            'shipping_city' => $shippaddress['city'],
            'shipping_zip_code' => $shippaddress['zip_code'],
            'billing_address' => $billaddress['address'],
            'billing_country' => $billaddress['country'],
            'billing_state' => $billaddress['state'],
            'billing_city' => $billaddress['city'],
            'billing_zip_code' => $billaddress['zip_code'],
            'sub_total' => $subtotal,
            'shipping_charge' => $shipping_fee,
            'grand_total' => $grand_total,
            'payment_method' => $payment_method,
            'wallet_deducted' => $wallet_deducted,
            'paypal_deducted' => $paypal_deducted,
            'card_deducted' => $card_deducted,
            'order_status' => 'placed',
            'card_data' => $card_data,
            'created_at' => date('Y-m-d H:i:s', time()),
        );
       
        $order = $this->General_model->insert_data('orders_tbl', $insert_order);
        if ($order) {
            $cart_item = $this->cart_items($user_id);
            $is_single_download = [];
            $items_arr = [];
            foreach ($cart_item as $key => $itmlst) {
                $is_single_download[] = $itmlst['file_name'];
                $sel_variation = !empty($itmlst['sel_variation']) ? explode(', ', $itmlst['sel_variation']) : '';
                $variation = [];
                if (isset($sel_variation) && $sel_variation != '') {
                    foreach ($sel_variation as $k => $sv) {
                        $sv = explode('&', $sv);
                        $var_title = end(explode('_', $sv[0]));
                        $var_opt = end(explode('_', $sv[1]));
                        $variation[] = $var_title.'_'.$var_opt;
                    }
                }
                $product_attributes = implode(',', $variation);

                $insert_order_items = $items_arr[] = array(
                    'order_id' => $order,
                    'product_id' => $itmlst['product_id'],
                    'sold_by' => $itmlst['seller_id'],
                    'ref_seller_id' => $itmlst['ref_seller_id'],
                    'product_name' => $itmlst['product_name'],
                    'product_image' => $itmlst['product_image'],
                    'qty' => $itmlst['qty'],
                    'amount' => $itmlst['amount'],
                    'product_fee' => $itmlst['product_fee'],
                    'file_name' => $itmlst['file_name'],
                    'product_attributes' => $product_attributes,
                    'sel_variation_amount' => $itmlst['sel_variation_amount'],
                );
                $insertitems = $this->General_model->insert_data('orders_items_tbl', $insert_order_items);
                if ($insertitems) {
                    $darr['product_id'] = $itmlst['product_id'];
                    $darr['get'] = 'row';
                    $get_product_detail = $this->Product_model->productDetail($darr);
                    $update_data['qty'] = $get_product_detail->qty - $itmlst['qty'];
                    $this->General_model->update_data('products_tbl', $update_data, array('id' => $itmlst['product_id']));
                    $this->Product_model->remove_item($itmlst['id'],'single_product');
                }
            }
            if (sizeof($is_single_download) == 1 && $is_single_download[0] !='') {
                $user_data['order_status'] = 'completed';
                $this->General_model->update_data('orders_tbl',$user_data,array('id'=>$order));
            }
            if ($insertitems) {
                $emailarr = array(
                    'email_title' => 'Thank You for order',
                    'email_address' => $insert_order['email'],
                    // 'email_address' => 'amarjoshi581@gmail.com',
                    'name' => $insert_order['name'],
                    'umessage' => 'Your order has been received is now being processed.',
                    'wbsite_transaction_id' => $wbsite_transaction_id,
                    'order_info' => $insert_order,
                    'items_arr' => $items_arr,
                    'subject' => 'Your order has been placed',
                    'order_created_date' => $insert_order['created_at'],
                );
                $email_to_cust = $this->Email_model->placed_order_mail($emailarr);
                $emailarr['email_title'] = 'Hey! you have got a new order';
                $emailarr['subject'] = 'You have got new order';
                $emailarr['email_address'] = 'storeadmin';
                $emailarr['umessage'] = 'You have received an order from '.$cust_info['name'].'. The order is as follows:';
                $email_to_admin = $this->Email_model->placed_order_mail($emailarr); 

                $this->General_model->sendMail($email,$name,$subject,$body);
                $this->Product_model->clear_cart($user_id,'all_product');

                $response=array('status' => TRUE,
                                        'status_code'=>200,
                                        'message' => 'Order placed successfully.');
                echo json_encode($response);
            } else {
                $response=array('status' => FALSE,
                            'status_code'=>400,
                            'message' => 'Unable to Place order');
                echo json_encode($response);
            }
        } else {
            $response=array('status' => FALSE,
                            'status_code'=>400,
                            'message' => 'Unable to Place order');
            echo json_encode($response);
        }
    }

}