<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Membership extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->target_dir=BASEPATH.'../upload/profile_img/';
	}
	//get memberlist api
	function get_membership_list()
	{

		 $data = $this->General_model->get_all_membership();
		 

		if(!empty($data))
		{
			foreach ($data as $key => $value) {
            	
            	$data[$key]['image']=base_url()."upload/membership/".$value['image'];
            	  
            }

			$response=array('status' => TRUE,
							'status_code'=>200,
	                   		'membership_list'=>$data
	                );
	        echo json_encode($response,JSON_UNESCAPED_SLASHES);

		}
		else
		{
			$response=array('status' => FALSE,
							'status_code'=>400,
                			'message' => 'Membership List Is Empty');
    			echo json_encode($response);
		}
		

	}

	//Get Membership info
	function get_membership_info($id=""){
		if($id)
		{
	        $data = $this->General_model->view_single_row('membership','id',$id);
	        
	        if(!empty($data))
			{
				$data['image']=base_url()."upload/membership/".$data['image'];
				$response=array('status' => TRUE,
								'status_code'=>200,
		                   		'membership_info'=>$data
		                );
		        echo json_encode($response,JSON_UNESCAPED_SLASHES);

			}
			else
			{
					$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'Data Not Available');
	    			echo json_encode($response);
			}
		}
		else
		{
					$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'Please enter membership id');
	    			echo json_encode($response);

		}
    }

    // get user mebmershi api
    function get_user_membership($user_id="")
    {
    	if($user_id)
    	{
    		 $options = array(
            'select' => 'pms.plan_id, pms.user_id, pms.payer_email, m.title, m.time_duration, pms.payment_date, pms.transaction_id',
            'table' => 'player_membership_subscriptions pms',
            'join' => array('membership m' => 'm.id = pms.plan_id'),
            'where' => array(
                'pms.user_id' => $user_id,
                'pms.payment_status' => 'Active'
            ),
            'order_by' => array('pms.id' => 'desc'),
            'single' => true
       		 );
	        $data['user_plan_data'] = $this->General_model->commonGet($options);
	        if(!empty($data['user_plan_data']))
			{
				$response=array('status' => TRUE,
								'status_code'=>200,
		                   		'user_plan_data'=>$data['user_plan_data']
		                );
		        echo json_encode($response,JSON_UNESCAPED_SLASHES);

			}
			else
			{
				$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => "You Have Not Membership Subscription");
	    			echo json_encode($response);
			}	
    	}
    	else
    	{
    			$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => "Please enter user id");
	    			echo json_encode($response);

    	}


    	
        
    }

    //subscribe_membership_api
    function subscribe_membership()
    {
    	$this->form_validation->set_error_delimiters('', '');
    	 $this->form_validation->set_rules("user_id", "user id", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("membership_subscribe_plan", "membership subscribe plan", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("title", "title", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("fees", "fees", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("time_duration", "time duration", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("membership_subscribe_amount", "membership subscribe amount", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("membership_subscription_opt", "membership subscription opt", "required", array('required' => 'Please enter %s.'));
    	   $array = array();
  		if($this->form_validation->run())
  		{ 	
    		$user_id=$_POST['user_id'];
    		
    	 	$data['subscribe_amount'] = number_format(($_POST['membership_subscribe_amount'] + $_POST['fees']),2,".","");
            $data['subscribe_plan'] = $_POST['membership_subscribe_plan'];
            $payer_detail = $this->User_model->user_detail();
            $data['payer_email'] = $payer_detail[0]['email'];
           	$data['returnurl'] = $data['cancelurl'] = base_url().'membership/GetPaymentDetails';
            $custom_arr = array(
              'subscripltion_plan' => $_POST['membership_subscribe_plan'],
              'title' => $_POST['title'],
              'time_duration' => $_POST['time_duration']
            );
            $data['custom'] = json_encode($custom_arr);
           	$data['title'] = $_POST['title'].' Membership Plan.';
           	$data['notifyurl'] = '';
            $data['billingtype'] = 'RecurringPayments';
           	// $response = $this->Paypal_payment_model->Set_express_checkout($data);

           	// $getdetails[]= $this->paypal_pro->GetExpressCheckoutDetails($_GET['token']); 
    	  	
	        // $implode_custom_data = explode('&', $getdetails[0]['CUSTOM']);
	        // $plan = $implode_custom_data[1];
	        
	        $player_subscriptions_data = array(
	            'token_id' => '',
	            'transaction_id' => '',
	            'user_id' => $user_id,
	            'plan_id' => $_POST['membership_subscribe_plan'],            
	            'validity' => '',
	            'valid_from' => '',
	            'valid_to' => '',
	            'amount' => $_POST['membership_subscribe_amount'],
	            'currency_code' => '',
	            'payer_email' => '',
	            'create_at' => date('Y-m-d H:i:s', time()),
	            'update_at' => date('Y-m-d H:i:s', time()),
	            'payment_status' => 'Active',
	            'payment_date' => '' 
	        );        
	        $r = $this->General_model->insert_data('player_membership_subscriptions', $player_subscriptions_data);
	       
	        if ($r) {
	            // $common_settings = json_decode($this->session->userdata('common_settings'), true);
	            $common_settings['free_membership'] = 'off';
	            $this->General_model->update_data('user_detail',array('common_settings' => json_encode($common_settings)),array('user_id'=>$user_id));

	            $response=array('status' => TRUE,
							'status_code'=>200,
	                   		'message'=>'You have successfully subscribed '.$_POST['title']. ' Membership Plan'
	                );
	        	echo json_encode($response);
	            // $data['getdetails'] = $getdetails;
	            // $data['plan'] = $implode_custom_data[3];            
	            // $data['subscribe_amount'] = $getdetails['REQUESTDATA']['AMT'];
	            
	        }
	        else
	        {
	        	$response=array('status' => FALSE,
							'status_code'=>400,
                			'message' => "Subscription Failure");
    			echo json_encode($response);

	        }
	    }
  		else
		{
				$errors = $this->form_validation->error_array();
    			$fields = array_keys($errors);
    			$err_msg = $errors[$fields[0]];
			   	$array = array(
			   	
			    'error'    => true,
			    'status_code'=>400,
			    'error_message'=>$err_msg = $errors[$fields[0]],
			    
   			);
			echo json_encode($array);
		}
    } 
}
