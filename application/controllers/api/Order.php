<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Order extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('User_model');
		$this->load->model('Paypal_payment_model');
        $this->load->model('Email_model');
		$this->load->library('form_validation');
		$this->target_dir=BASEPATH.'../upload/profile_img/';
	}
	
	//get user order list api 
	function get_my_order($user_id="")
	{

		if($user_id)
		{
			$arr = array( 'user_id' => $user_id, 'order_by' => 'true' );
	        $data = $this->Product_model->get_my_product($arr);
	        
			if(!empty($data))
			{
				$response=array('status' => TRUE,
								'status_code'=>200,
		                   		'order_list'=>$data
		                );
		        echo json_encode($response,JSON_UNESCAPED_SLASHES);

			}
			else
			{
				$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'Your orderlist is Empty');
	    			echo json_encode($response);
			}
		}
		else
		{
			$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'Please enter user id');
	    			echo json_encode($response);

		}
		

	}

	//get player order list api 
	function get_player_order($user_id="")
	{
		if($user_id)
		{
		
			$arr = array('ref_seller_id' =>$user_id, 'order_by' => 'true' );
	        $data= $this->Product_model->get_my_product($arr);
	        
			if(!empty($data))
			{
				$response=array('status' => TRUE,
								'status_code'=>200,
		                   		'order_list'=>$data
		                );
		        echo json_encode($response,JSON_UNESCAPED_SLASHES);

			}
			else
			{
				$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'Your orderlist is Empty');
	    			echo json_encode($response);
			}
		}
		else
		{

						$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'Please enter user id');
	    				echo json_encode($response);

		}
		

	}

	//order cancel api
	function cancel_my_order()
	{
		$this->form_validation->set_error_delimiters('', '');
    	$this->form_validation->set_rules("order_id", "order id", "required", array('required' => 'Please enter %s.'));
  		if($this->form_validation->run())
  		{
  		
		
			 	$arr['order_id'] = $_POST['order_id'];
	            $detail = $this->Cart_model->cancelorder($arr);
	            $this->Email_model->order_action_mail(array('order_id' => $arr['order_id'], 'mail_action' => 'canceled'));
	           $message=$detail['message_succ'];
	           $message=str_replace("<br>", "", $message);
	           
			if(!empty($message))
			{
				$response=array('status' => TRUE,
								'status_code'=>200,
		                   		'order_list'=>$message
		                );
		        echo json_encode($response,JSON_UNESCAPED_SLASHES);

			}
			else
			{
				$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'Unable to cancel Order');
	    			echo json_encode($response);
			}
		}
  		else
		{
				$errors = $this->form_validation->error_array();
    			$fields = array_keys($errors);
    			$err_msg = $errors[$fields[0]];
			   	$array = array(
			   	
			    'error'    => true,
			    'status_code'=>400,
			    'error_message'=>$err_msg = $errors[$fields[0]],
			    
   			);
			echo json_encode($array);
		}
		

	}
	//get order detail api
	function get_my_order_details($user_id="",$id="")
	{
		
		if($id)
		{
				 $arr = array(
	                'user_id' => $user_id,
	                'id' => $id
	            );
	            $order_items = $this->Product_model->get_my_product($arr);
	            $atrlbl = array();
	            $optlbl = array();
	            $combined = array();
	            foreach ($order_items as $key => $value) {
				    $variants = explode(",",$value->product_attributes);
				    foreach ($variants as $var_value) {
				    	$pair = explode("_", $var_value);
			    		if(count($pair)-1 > 1){
			                $part1 = substr("$value",0, strrpos($value,'_'));
			                $part2 = substr("$value", (strrpos($value,'_') + 1));
			                $atrlbl[] = $part1; 
			                $optlbl[] = $part2; 
			            }else{
			                $atrlbl[] = $pair[0]; 
			                $optlbl[] = $pair[1];
			            }
				    }
				    $combined = ['atrlbl'=>$atrlbl, 'optlbl'=>$optlbl];
				    $value->product_attributes = $combined;
	            	$value->sel_variation_amount = $this->convertSelVariantToArray($value->sel_variation_amount);
	            	$value->product_image=base_url()."upload/products/".$value->product_image;
	            }
	            if(!empty($order_items))
				{
					$response=array('status' => TRUE,
									'status_code'=>200,
			                   		'order_details'=>$order_items
			                );
			        echo json_encode($response,JSON_UNESCAPED_SLASHES);

				}
				else
				{
					$response=array('status' => FALSE,
									'status_code'=>400,
		                			'message' => 'Order detail not available');
		    			echo json_encode($response);
				}
		}
		else
		{
				$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'Please enter order id');
	    			echo json_encode($response);

		}
           
	}

	//complete order api	
	function complete_order()
	{
		$this->form_validation->set_error_delimiters('', '');
    	$this->form_validation->set_rules("id", "id", "required", array('required' => 'Please enter %s.'));
  		if($this->form_validation->run())
  		{
			$id = $_POST['id'];
            $fields['order_status'] = 'completed';
            $fields['updated_at'] = date('Y-m-d H:i:s');
            $update_data_where['id'] = $id;
            $updatedata = $this->General_model->update_data('orders_tbl', $fields, $update_data_where);
            $this->Email_model->order_action_mail(array('order_id' => $_GET['id'], 'mail_action' =>'completed'));
            if(isset($updatedata))
            {
				$response=array('status' => TRUE,
				'status_code'=>200,
				'message' => 'Order status has been completed.');
				echo json_encode($response);
            }
            else
            {
				$response=array('status' => FALSE,
				'status_code'=>400,
    			'message' => 'Unable to updated Order status');
				echo json_encode($response);	
            }
        }
  		else
		{
				$errors = $this->form_validation->error_array();
    			$fields = array_keys($errors);
    			$err_msg = $errors[$fields[0]];
			   	$array = array(
			   	
			    'error'    => true,
			    'status_code'=>400,
			    'error_message'=>$err_msg = $errors[$fields[0]],
			    
   			);
			echo json_encode($array);
		}
	}

	//reward calculation function
	 public function reward_calculation($total_sold_items, $user_id, $total_reward, $user_level){

        $level_detail = $this->General_model->view_single_row('store_levels','level',$user_level[0]['current_level']);

        if($total_sold_items >= $level_detail['item_sold']){
            if(($level_detail['item_sold'] - $user_level[0]['sold_items']) >= 0) {
                if(($level_detail['item_sold'] - $user_level[0]['sold_items']) != 0){
                    $lvl_items = $this->General_model->view_data('user_level_items',array('level' => $user_level[0]['current_level'], 'user_level_id'=>$user_level[0]['id']));
                    $item_data = array(
                        'user_level_id' => $user_level[0]['id'],
                        'user_id' => $user_id,
                        'sold_items' => (count($lvl_items) > 0) ? ($level_detail['item_sold'] - $user_level[0]['sold_items'] <= $total_sold_items) ? ($level_detail['item_sold'] - $user_level[0]['sold_items']) : ($total_sold_items - $user_level[0]['sold_items']) : $level_detail['item_sold'],
                        'level' => $user_level[0]['current_level'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    );
                    $this->General_model->insert_data('user_level_items',$item_data);
                    for ($i = $user_level[0]['current_level']; $i > 0 ; $i--) {
                        $reward_level_detail = $this->General_model->view_single_row('store_levels','level',$i);

                        $total_reward = $total_reward + ($level_detail['item_sold'] * $reward_level_detail['reward']);
                        // print_r($level_detail['item_sold'] * $reward_level_detail['reward']);
                    }
                } 
                if($user_level[0]['sold_items'] >= $level_detail['item_sold']){
                    $this->General_model->update_data('user_level', array('sold_items' => $level_detail['item_sold'], 'current_level' => $user_level[0]['current_level'] + 1), array('user_id' => $user_id));
                } else {
                    $this->General_model->update_data('user_level', array('sold_items' => 0,'current_level' => $user_level[0]['current_level'] + 1), array('user_id' => $user_id));
                }
            }
            $user_level = $this->General_model->view_data('user_level',array('user_id' => $user_id));
            $this->reward_calculation($total_sold_items - $level_detail['item_sold'], $user_id, $total_reward, $user_level);
            
            $this->db->where('user_id', $user_id);
            $this->db->set('total_balance', 'total_balance + '.$total_reward, FALSE);
            $this->db->update('user_detail');
         }else{
            $item_data = array(
                'user_level_id' => $user_level[0]['id'],
                'user_id' => $user_id,
                'sold_items' => ($user_level[0]['sold_items'] > $total_sold_items) ? $total_sold_items : $total_sold_items - $user_level[0]['sold_items'],
                'level' => $user_level[0]['current_level'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
            $this->General_model->insert_data('user_level_items',$item_data);

            $this->db->where('user_id', $user_id);
            $this->db->set('sold_items', $total_sold_items, FALSE);
            $this->db->update('user_level');

         }
    }

	//update tracking id api
	function update_tracking_info() {
        $this->form_validation->set_error_delimiters('', '');
    	$this->form_validation->set_rules("editid", "order id", "required", array('required' => 'Please enter %s.'));
    	$this->form_validation->set_rules("edit_traking_id", "traking id", "required", array('required' => 'Please enter %s.'));
    	$this->form_validation->set_rules("item_sold_by", "sheller id", "required", array('required' => 'Please enter %s.'));
    	$this->form_validation->set_rules("grand_total", "grand total", "required", array('required' => 'Please enter %s.'));
    	$this->form_validation->set_rules("ref_seller", "ref seller id", "required", array('required' => 'Please enter %s.'));
  		if($this->form_validation->run())
  		{    
	            $qty = 0;
	            if ($_POST['editid'] !='' && $_POST['edit_traking_id']) {
	                $odata = $this->General_model->view_single_row('orders_tbl','id',$_POST['editid']);
	                if(($odata['tracking_id'] == null || empty($odata['tracking_id'])) && ($_POST['item_sold_by'] !='' && $_POST['grand_total'] !='' && $_POST['ref_seller'] !='')){
	                    if ($_POST['item_sold_by'] == '0' && $_POST['ref_seller'] != '0'){

	                        //Users get reward based on sum of reward of level they reached like if users is on 2nd level then they will get reward of level one and level 2 both(level 1 0.01 + level 2 0.02 = 0.03)

	                        $items = $this->General_model->view_data('orders_items_tbl',array('order_id' => $_POST['editid']));

	                        
	                        foreach ($items as $item) {
	                            $qty += $item['qty'];
	                        }
	                        $user_level = $this->General_model->view_data('user_level',array('user_id' => $_POST['ref_seller']));
	                        
	                        if (count($user_level) > 0 && $user_level != null) {
	                            $total_sold_items = $user_level[0]['sold_items'] + $qty;
	                            $m = $this->reward_calculation($total_sold_items, $_POST['ref_seller'], $total_reward = 0, $user_level);

	                        } else {
	                            $data = array(
	                                'user_id' => $_POST['ref_seller'],
	                                'sold_items' => 0,
	                                'current_level' => 1,
	                                'created_at' => date('Y-m-d H:i:s'),
	                                'updated_at' => date('Y-m-d H:i:s')
	                            );
	                            $this->General_model->insert_data('user_level',$data);
	                            $user_level = $this->General_model->view_data('user_level',array('user_id' => $_POST['ref_seller']));
	                            $m = $this->reward_calculation($qty, $_POST['ref_seller'], $total_reward = 0, $r);
	                        }
	                    } else {
	                        $global_store_fee = $this->General_model->view_single_row('site_settings','option_title','global_store_fee');
	                        $this->db->where('user_id', $_POST['item_sold_by']);
	                        $this->db->set('total_balance', 'total_balance + '.($_POST['grand_total'] - $global_store_fee['option_value']), FALSE);
	                        $this->db->update('user_detail');
	                    }
	                }
	                $r = $this->General_model->update_data('orders_tbl', array('tracking_id' => $_POST['edit_traking_id']), array('id' => $_POST['editid']));
	                if(isset($r))
	            	{
						$response=array('status' => TRUE,
						'status_code'=>200,
						'message' => 'Tracking info updated.');
						echo json_encode($response);
	            	}
	            	else
	            	{
						$response=array('status' => FALSE,
						'status_code'=>400,
		    			'message' => 'Unable to updated Tracking info');
						echo json_encode($response);	
	            	}
	                
	            }
        }
  		else
		{
				$errors = $this->form_validation->error_array();
    			$fields = array_keys($errors);
    			$err_msg = $errors[$fields[0]];
			   	$array = array(
			   	
			    'error'    => true,
			    'status_code'=>400,
			    'error_message'=>$err_msg = $errors[$fields[0]],
			    
   			);
			echo json_encode($array);
		}    
        
    }
	
}
