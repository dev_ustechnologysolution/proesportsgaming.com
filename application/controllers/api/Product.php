<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Product extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->target_dir = BASEPATH.'../upload/products/';
		
	}
	
	
	//get all products 
	function get_all_products()
	{

		     $list_arr = array(
                'order_by' => 'product_order',
                'seller_id' => '0',
            );		
           $data = $this->Product_model->SearchProductArr($list_arr);
           foreach ($data as $key => $value) {
	        	 $data[$key]['image']= base_url()."upload/products/".$value['image'];
	        }
   
   		  
		if(!empty($data))
		{
			$response=array('status' => TRUE,
							'status_code'=>200,
	                   		'product_list'=>$data
	                );
	        echo json_encode($response,JSON_UNESCAPED_SLASHES);

		}
		else
		{
			$response=array('status' => FALSE,
							'status_code'=>400,
                			'error_message' => 'Products not availble');
    			echo json_encode($response);
		}
		

	}
	//view product detail with id
	function view_product($id="") {
		if ($id) {

			// $data['ref_id'] = $_SESSION['ref_id'];
	        // $data['category_list'] = $this->Product_model->getCategories();
	        $data['product_id'] = $id;
	        $arr = array('product_id' => $id);
	        $data['product_detail'] = $this->Product_model->productDetail($arr);
	        // $data['product_detail']=$data['product_detail'][0];
	        
	        // (empty($data['product_detail']))? redirect(base_url().'store'):'';
	        
	        $data['prdct_img_arr'] = array_unique(array_column($data['product_detail'],'image'));
	        $result = array();
	        foreach ($data['prdct_img_arr'] as $key => $value) {
	        	$result[$key]= base_url()."upload/products/".$value; 
	        }
	        $data['prdct_img_arr'] = $result;
		    $data['default_sel_img'] = $data['prdct_img_arr'][0];
		    $data['product_detail'] = $data['product_detail'][0];
		      
	        $arr = array('product_id' => $id);
	        $product_attribute_data = $this->Product_model->attributes_list($arr);

	        $result1 = array();
	        foreach ($product_attribute_data as $key => $pad) {
	        	$attr_arr = array();
	        	$option_list = array();
        		$option_list[] = array(
        			"attr_id" => $pad->attr_id,
    				"option_id" => $pad->option_id,
    				"option_label" => $pad->option_label,
    				"extra_amount" => $pad->extra_amount,
        		);
	        	if (!in_array($pad->attr_id, array_column($result1, 'attribute_id'))) {
		        	$attr_arr = array(
		        		'attribute_id' => $pad->attr_id,
		        		'title' => $pad->title,
		        		'type' => $pad->type,
		        		'option_list' => $option_list,
		        	);
	        		$result1[] = $attr_arr;
	        	} else {
	        		$attribute_id_key = array_search($pad->attr_id, array_column($result1, 'attribute_id'));
	        		$result1[$attribute_id_key]['option_list'] = array_merge($result1[$attribute_id_key]['option_list'],$option_list);
	        	}
	        }
		    $data['attribute_list'] = $result1;
	        $data['product_ads'] = $this->General_model->get_ads_detail('product_ads',1,'',$data['product_id'])[0];
	        if (!empty($data['product_detail'])) {
				$response = array(
					'status' => TRUE,
					'status_code' => 200,
					'product_data' => $data
				);
		        echo json_encode($response ,JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
			} else {
				$response = array(
					'status' => FALSE,
					'status_code' => 400,
					'error_message' => 'Products not availble'
				);
				echo json_encode($response);
			}
		} else {
			$response = array(
				'status' => FALSE,
				'status_code' => 400,
				'error_message' => 'Please enter product id'
			);
			echo json_encode($response);
		}
	}
	//get user products 
	function get_user_products_list($id="")
	{
		if($id)
		{
			    $list_arr = array(
	                'order_by' => 'product_order',
	                'seller_id' => $id,
	            );		
	           $data = $this->Product_model->SearchProductArr($list_arr);
	           foreach ($data as $key => $value) {
		        	 $data[$key]['image']= base_url()."upload/products/".$value['image'];
		        }
		       
	   		  
			if(!empty($data))
			{
				$response=array('status' => TRUE,
								'status_code'=>200,
		                   		'product_list'=>$data
		                );
		        echo json_encode($response,JSON_UNESCAPED_SLASHES);

			}
			else
			{
				$response=array('status' => FALSE,
								'status_code'=>400,
	                			'error_message' => 'Products not availble');
	    			echo json_encode($response);
			}
		}
		else
		{
			$response=array('status' => FALSE,
								'status_code'=>400,
	                			'error_message' => 'Please enter user id');
	    			echo json_encode($response);

		}
		

	}

	// search product api
	function get_product_list_by_search()
	{
		if(!empty($_POST['sub_category_id']) && empty($_POST['category_id']))
		{
			$response=array('status' => FALSE,
								'status_code'=>400,
	                			'error_message' => 'Please select main category');
	    			echo json_encode($response);
		}
		else
		{
		 if (!empty($_POST)) {
	            $main_cat = $_POST['category_id'];
	            $store_id = '0';
	            $main_sub_cat = $_POST['sub_category_id'];
	            $product_srch_term = $_POST['product_name'];
	            $last_item = (!empty($_POST['allcount']) && $_POST['allcount'] != 'NaN') ? $_POST['allcount'] : '';
	            if ($main_cat !='') {
	                $arr = array( 'main_cat' => $main_cat );
	                $all_sub_cat = $this->Product_model->getSubCarArr($arr);
	                $all_sub_cat_arr = explode(',', $all_sub_cat->cate_id_arr);
	            }
	            // $store_ads_list = $this->General_model->get_ads_detail('store_ads',100,'img','');
	            if($_POST['btn_id'] != null && $_POST['btn_id'] != ''){
	                $last_item = '';
	            }
	            $arr1 = array(
	                'all_sub_cat_arr' => $all_sub_cat_arr,
	                'main_cat' => $main_cat,
	                'main_sub_cat' => $main_sub_cat,
	                'product_srch_term' => $product_srch_term,
	                'last_id' => $last_item,
	                'order_by' => 'product_order',
	                'limit' => $this->countItem_loader,
	                'seller_id' => $store_id,
	            );


	            // if (isset($_POST['num_display_prdct']) && !empty($_POST['num_display_prdct'])) {
	            //     $arr1['limit'] = $_POST['num_display_prdct'];
	            // }
	            $res_arr = $this->Product_model->SearchProductArr($arr1);
	            
	            // $get_last_arr = array(
	            //     'all_sub_cat_arr' => $all_sub_cat_arr,
	            //     'main_cat' => $main_cat,
	            //     'main_sub_cat' => $main_sub_cat,
	            //     'product_srch_term' => $product_srch_term,
	            //     'get_last_id' => 'yes',
	            //     'seller_id' => $store_id,
	            //     'limit' => 1,
	            // );
	            // $data['get_last_id'] = $this->Product_model->SearchProductArr($get_last_arr)[0];
	            
	            // $data['ads'] = $store_ads_list;
	            $data = $res_arr;
	            foreach ($data as $key => $value) {
		        	 $data[$key]['image']= base_url()."upload/products/".$value['image'];
		        }
	            
	           if(!empty($data))
			   {
					$response=array('status' => TRUE,
								'status_code'=>200,
		                   		'product_list'=>$data
		                );
		       	 	echo json_encode($response,JSON_UNESCAPED_SLASHES);

			   }
			   else
			   {
					$response=array('status' => FALSE,
								'status_code'=>400,
	                			'error_message' => 'Products not availble');
	    			echo json_encode($response);
			   }
	        }
	    

		}
	}

	//check_custom_tag_name api
	function check_custom_tag_name($id="")
	{

	  	if($id)
	  	{
	        $data= $this->Product_model->check_tag($id);
	         if(!empty($data))
				   {
						$response=array('status' => TRUE,
									'status_code'=>200,
			                   		'custom_tag_data'=>$data
			                );
			       	 	echo json_encode($response,JSON_UNESCAPED_SLASHES);

				   }
				   else
				   {
						$response=array('status' => FALSE,
									'status_code'=>400,
		                			'error_message' => 'custom tag not availble');
		    			echo json_encode($response);
				   }
		}
		else
		{
				$response=array('status' => FALSE,
								'status_code'=>400,
	                			'error_message' => 'Please enter product id');
	    			echo json_encode($response);

		}
	}

	function get_attribute_image()
	{
		$this->form_validation->set_error_delimiters('', '');
    	 $this->form_validation->set_rules("product_id", "product_id", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("attribute_id", "attribute_id", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("option_id", "option_id", "required", array('required' => 'Please enter %s.'));
    	   $array = array();
  		if($this->form_validation->run())
  		{
  			 $attr = array(
                           'attribute_id' => $_POST['attribute_id'],
                           'option_id' => $_POST['option_id'],
                           'product_id' => $_POST['product_id'],
                           'fetch_data' => 'result',
                        );
  			  

                        $getdata = $this->Cart_model->get_attribute_img($attr);
                        $get_main_imgs = array_unique(array_column($getdata,'option_image'));
                        // $default_main_sel_img = $get_main_imgs[0];
                      

                        foreach($get_main_imgs as $key=>$value)
						{
						    if(is_null($value) || $value == '')
						       unset($get_main_imgs[$key]);
						  	
						}

						
        				foreach ($get_main_imgs as $key => $value) {
        	 				$get_main_imgs[$key]= base_url()."upload/products/".$value; 
        	
        				}
        										

        				if(!empty($get_main_imgs))
        				{
        						$response=array('status' => TRUE,
								'status_code'=>200,
		                   		'images'=>$get_main_imgs,
		                		);
		       	 			echo json_encode($response,JSON_UNESCAPED_SLASHES);	
        				}
        				else
        				{
        						$response=array('status' => FALSE,
								'status_code'=>400,
	                			'error_message' => 'images not availble');
	    					    echo json_encode($response);
        				}	
        				  
                        
                        
  		}
  		else
		{
				$errors = $this->form_validation->error_array();
    			$fields = array_keys($errors);
    			$err_msg = $errors[$fields[0]];
			   	$array = array(
			   	
			    'error'    => true,
			    'status_code'=>400,
			    'error_message'=>$err_msg = $errors[$fields[0]],
			    
   			);
			echo json_encode($array);
		}

	}
	function add_to_cart_product()
	{

	$this->form_validation->set_error_delimiters('', '');
	 $this->form_validation->set_rules("user_id", "user id", "required", array('required' => 'Please enter %s.'));
	 $this->form_validation->set_rules("ref_seller_id", "ref seller id", "required", array('required' => 'Please enter %s.'));
	 $this->form_validation->set_rules("qty", "qty", "required", array('required' => 'Please enter %s.'));
	 $this->form_validation->set_rules("product_image", "product image", "required", array('required' => 'Please enter %s.'));
	 $this->form_validation->set_rules("product_id", "product id", "required", array('required' => 'Please enter %s.'));
	 $this->form_validation->set_rules("amount", "amount", "required", array('required' => 'Please enter %s.'));
	 $this->form_validation->set_rules("product_fee", "product_fee", "required", array('required' => 'Please enter %s.'));
	 $this->form_validation->set_rules("int_fee", "int fee", "required", array('required' => 'Please enter %s.'));
	 $this->form_validation->set_rules("product_name", "product name", "required", array('required' => 'Please enter %s.'));
	 $this->form_validation->set_rules("sel_variation[]", "sel variation", "required", array('required' => 'Please enter %s.'));
	 $this->form_validation->set_rules("extra_amount[]", "extra amount", "required", array('required' => 'Please enter %s.'));
	 
	   $array = array();
		if($this->form_validation->run())
		{		

				if ($_POST['qty'] < 1) {
		        // $this->session->set_flashdata('message_err', 'Please select quantity more than '.$_POST['qty']);
		        // redirect($_SERVER['HTTP_REFERER']);
					$response=array('status' => FALSE,
										'status_code'=>400,
			                			'error_message' => 'Please select quantity more than '.$_POST['qty']);
			    					    echo json_encode($response);
		      }
		      else
		      {
			      // $cart_items = $this->Cart_model->cart_items();
			      $product_image =$_POST['product_image'];
			      $add_product = array(
			        'user_id' => $_POST['user_id'],
			        'product_id' => $_POST['product_id'],
			        'product_image' => $product_image,
			        'product_name' => $_POST['product_name'],
			        'qty' => $_POST['qty'],
			        'amount' => $_POST['amount'],
			        'product_fee' => $_POST['product_fee'],
			        'ref_seller_id' => $_POST['ref_seller_id'],
			      );
			      if (!empty($_POST['sel_variation'])) {
			        $sel_variation = $_POST['sel_variation'];
			        $sel_variation_extra_amnt = $_POST['extra_amount'];
			        
			        // Get Attribute list with option
			        $arr = array('product_id' => $_POST['product_id']);
			        $product_attribute_data = $this->Product_model->attributes_list($arr);
			        $option_arr = array();
			        if (!empty($product_attribute_data)) {
			          $title_arr = array_column($product_attribute_data, 'title');
			          $attr_arr = array_column($product_attribute_data, 'attr_id');
			          $attribute_type_list = array_combine($attr_arr, $title_arr);
			          foreach($product_attribute_data as $attribute) {
			            $option_arr[$attribute->attr_id][$attribute->option_id] = $attribute->option_label; 
			          }
			          $sel_variation_arr = $sel_variation_amont_arr = array();
			          foreach ($sel_variation as $key => $sv) {
			            foreach ($sv as $k => $sel_attr) {
			              $sel_variation_arr[] = 'atrlbl_'.$attribute_type_list[$key].'&optlbl_'.$option_arr[$key][$sel_attr];
			              $sel_variation_amont_arr[] = 'atr_'.$key.'&opt_'.$sel_attr.'&variation_amount_'.
			              $sel_variation_extra_amnt[$key][$sel_attr];
			              
			            }
			          }
			        }
			        if (!empty($_POST['Custom_Tag'])) {
			          $sel_variation_arr[] = 'atrlbl_Custom Tag&optlbl_'.trim($_POST['Custom_Tag']);
			        }
			        $add_product['sel_variation'] = $sel_variation_arr;
			        $add_product['sel_variation_amount'] = $sel_variation_amont_arr;

			      }
			      // 'sel_variation_amount' => (!empty($_POST['extra_amount'])) ? $_POST['extra_amount'] : '',
			      // 'sel_variation' => $_POST['sel_variation'],

			      
			        $insert_product = $this->Cart_model->add_item_cart($add_product);
			        if ($insert_product['added'] == true) {
			          // $msg = 'Item added into Cart';
			          // $this->session->set_flashdata('message_succ', $msg);
			          // redirect(base_url().'cart');
					        	$response=array('status' => TRUE,
									'status_code'=>200,
			                   		'message'=>'Item added into Cart',
			                		);
			       	 			echo json_encode($response);	
			        } else if ($insert_product['errmsg'] !='') {

			        			
								$response=array('status' => FALSE,
								'status_code'=>400,
			        			'error_message' =>$insert_product['errmsg']);
							    echo json_encode($response);
			        	// $this->session->set_flashdata('message_err', $insert_product['errmsg']);
			        	// redirect(base_url().'store/view_product/'.$insert_product['product_id']);
			        } else {
								$response=array('status' => FALSE,
								'status_code'=>400,
			        			'error_message' => 'Unable to add item');
							    echo json_encode($response);
			          // $this->session->set_flashdata('message_err', 'Unable to add item');
			          // redirect(base_url().'store/view_product/'.$_POST['product_id']);
			        }
			  }
		}
		else
		{
				$errors = $this->form_validation->error_array();
    			$fields = array_keys($errors);
    			$err_msg = $errors[$fields[0]];
			   	$array = array(
			   	
			    'error'    => true,
			    'status_code'=>400,
			    'error_message'=>$err_msg = $errors[$fields[0]],
			    
   			);
			echo json_encode($array);
		}      

	}

	//add to wishlist api
	function add_to_wishlist_product()
	{
		

	 $this->form_validation->set_error_delimiters('', '');
	 $this->form_validation->set_rules("user_id", "user id", "required", array('required' => 'Please enter %s.'));
	 $this->form_validation->set_rules("ref_seller_id", "ref seller id", "required", array('required' => 'Please enter %s.'));
	 $this->form_validation->set_rules("qty", "qty", "required", array('required' => 'Please enter %s.'));
	 $this->form_validation->set_rules("product_image", "product image", "required", array('required' => 'Please enter %s.'));
	 $this->form_validation->set_rules("product_id", "product id", "required", array('required' => 'Please enter %s.'));
	 $this->form_validation->set_rules("amount", "amount", "required", array('required' => 'Please enter %s.'));
	 $this->form_validation->set_rules("product_fee", "product_fee", "required", array('required' => 'Please enter %s.'));
	 $this->form_validation->set_rules("int_fee", "int fee", "required", array('required' => 'Please enter %s.'));
	 $this->form_validation->set_rules("product_name", "product name", "required", array('required' => 'Please enter %s.'));
	 $this->form_validation->set_rules("sel_variation[]", "sel variation", "required", array('required' => 'Please enter %s.'));
	 $this->form_validation->set_rules("extra_amount[]", "extra amount", "required", array('required' => 'Please enter %s.'));
	 
	   $array = array();
		if($this->form_validation->run())
		{		

				if ($_POST['qty'] < 1) {
		        // $this->session->set_flashdata('message_err', 'Please select quantity more than '.$_POST['qty']);
		        // redirect($_SERVER['HTTP_REFERER']);
					$response=array('status' => FALSE,
										'status_code'=>400,
			                			'error_message' => 'Please select quantity more than '.$_POST['qty']);
			    					    echo json_encode($response);
		      }
		      else
		      {
			      // $cart_items = $this->Cart_model->cart_items();
			      $product_image =$_POST['product_image'];
			      $add_product = array(
			        'user_id' => $_POST['user_id'],
			        'product_id' => $_POST['product_id'],
			        'product_image' => $product_image,
			        'product_name' => $_POST['product_name'],
			        'qty' => $_POST['qty'],
			        'amount' => $_POST['amount'],
			        'product_fee' => $_POST['product_fee'],
			        'ref_seller_id' => $_POST['ref_seller_id'],
			      );
			      if (!empty($_POST['sel_variation'])) {
			        $sel_variation = $_POST['sel_variation'];
			        $sel_variation_extra_amnt = $_POST['extra_amount'];
			        
			        // Get Attribute list with option
			        $arr = array('product_id' => $_POST['product_id']);
			        $product_attribute_data = $this->Product_model->attributes_list($arr);
			        $option_arr = array();
			        if (!empty($product_attribute_data)) {
			          $title_arr = array_column($product_attribute_data, 'title');
			          $attr_arr = array_column($product_attribute_data, 'attr_id');
			          $attribute_type_list = array_combine($attr_arr, $title_arr);
			          foreach($product_attribute_data as $attribute) {
			            $option_arr[$attribute->attr_id][$attribute->option_id] = $attribute->option_label; 
			          }
			          $sel_variation_arr = $sel_variation_amont_arr = array();
			          foreach ($sel_variation as $key => $sv) {
			            foreach ($sv as $k => $sel_attr) {
			              $sel_variation_arr[] = 'atrlbl_'.$attribute_type_list[$key].'&optlbl_'.$option_arr[$key][$sel_attr];
			              $sel_variation_amont_arr[] = 'atr_'.$key.'&opt_'.$sel_attr.'&variation_amount_'.
			              $sel_variation_extra_amnt[$key][$sel_attr];
			              
			            }
			          }
			        }
			        if (!empty($_POST['Custom_Tag'])) {
			          $sel_variation_arr[] = 'atrlbl_Custom Tag&optlbl_'.trim($_POST['Custom_Tag']);
			        }
			        $add_product['sel_variation'] = $sel_variation_arr;
			        $add_product['sel_variation_amount'] = $sel_variation_amont_arr;
			      

			      }
			      // 'sel_variation_amount' => (!empty($_POST['extra_amount'])) ? $_POST['extra_amount'] : '',
			      // 'sel_variation' => $_POST['sel_variation'],

			      
			        $insert_product = $this->Cart_model->add_item_wishlist($add_product);
       				

			        if ($insert_product) 
			        {
						$response=array('status' => TRUE,
						'status_code'=>200,
			             'message'=>'Item added into Wishlist',
			            );
			   	 		echo json_encode($response);
			          
			        } else 
			        {
						$response=array('status' => FALSE,
						'status_code'=>400,
		    			'error_message' => 'Unable to add item into Wishlist');
					    echo json_encode($response);

			        }
			       
			  }
		}
		else
		{
				$errors = $this->form_validation->error_array();
    			$fields = array_keys($errors);
    			$err_msg = $errors[$fields[0]];
			   	$array = array(
			   	
			    'error'    => true,
			    'status_code'=>400,
			    'error_message'=>$err_msg = $errors[$fields[0]],
			    
   			);
			echo json_encode($array);
		}      

	}

	//get_product_list_by_account api
	 function get_product_list_by_account($user_id="")
	 {
	 	if($user_id)
	 	{

		   		 $arr = array('seller_id' =>$user_id, 'order_by' => 'pt.product_order', 'group_by' => 'pt.id');
		    	 $data= $this->Product_model->productDetail($arr);
		    	  if (!empty($data)) 
		          {
						$response=array('status' => TRUE,
						'status_code'=>200,
			             'list'=>$data,
			            );
			   	 		echo json_encode($response);
		          
		        } else 
		        {
						$response=array('status' => FALSE,
						'status_code'=>400,
		    			'error_message' => 'Your Selling Products list is Empty');
					    echo json_encode($response);

		        }
		}
		else
		{
			$response=array('status' => FALSE,
						'status_code'=>400,
		    			'error_message' => 'Please enter user id');
					    echo json_encode($response);
		}
	    	 
  	 }

  	 //get local tax api
  	  function get_player_local_tax($user_id="")
	 {
	 	if($user_id)
	 	{

	    	 $tax_data= $this->Product_model->get_local_tax($user_id);
	    	 $local_product_fee = $tax_data->local_product_fee;
	    	 $local_product_fee = explode('_', $local_product_fee);
	    	 $data['type']=$local_product_fee[0];
	    	 $data['type'] = $data['type']==1 ? 'Fix Amount' : 'Percentage';
	    	 $data['value'] = $local_product_fee[1];  
	    	
	    	  if (!empty($data)) 
	          {
					$response=array('status' => TRUE,
					'status_code'=>200,
		             'data'=>$data,
		            );
		   	 		echo json_encode($response);
	          
	        } else 
	        {
					$response=array('status' => FALSE,
					'status_code'=>400,
	    			'error_message' => 'data not availble');
				    echo json_encode($response);

	        }
	    }
	    else
	    {
	    		$response=array('status' => FALSE,
					'status_code'=>400,
	    			'error_message' => 'Please enter user id');
				    echo json_encode($response);
	    }
	    	 
  	 }
  	  //get international tax api
  	  function get_player_international_tax($user_id="")
	 {
	 	if($user_id)
	 	{
	    	 $tax_data= $this->Product_model->get_international_tax($user_id);
	    	 $international_product_fee = $tax_data->international_product_fee;
	    	 $international_product_fee = explode('_', $international_product_fee);
	    	 $data['type']=$international_product_fee[0];
	    	 $data['type'] = $data['type']==1 ? 'Fix Amount' : 'Percentage';
	    	 $data['value'] = $international_product_fee[1];  
	    	
	    	  if (!empty($data)) 
	          {
					$response=array('status' => TRUE,
					'status_code'=>200,
		             'data'=>$data,
		            );
		   	 		echo json_encode($response);
	          
	        } else 
	        {
					$response=array('status' => FALSE,
					'status_code'=>400,
	    			'error_message' => 'data not availble');
				    echo json_encode($response);

	        }
	    }
	    else
	    {
	    			$response=array('status' => FALSE,
					'status_code'=>400,
	    			'error_message' => 'Please enter user id');
				    echo json_encode($response);

	    }
	    	 
  	 }
  	  //update local tax api
  	  function update_player_local_tax()
	 {
	 	$this->form_validation->set_error_delimiters('', '');
    	 $this->form_validation->set_rules("user_id", "user id", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("type", "type", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("value", "value", "required|numeric", array('required' => 'Please enter %s.'));
    	   $array = array();
  		if($this->form_validation->run())
  		{
	 		
	 		$user_id=$_POST['user_id'];
	 		$type=$_POST['type'];
	 		$value=$_POST['value'];
	 		$data=$type.'_'.$value;
	 		
	    	 $update_local_tax=$this->General_model->update_data('user_detail',array('local_product_fee'=>$data),array('user_id'=>$user_id)); 
	    	
	    	  if (isset($update_local_tax)) 
	          {
					$response=array('status' => TRUE,
					'status_code'=>200,
		             'message'=>'update successfully',
		            );
		   	 		echo json_encode($response);
	          
	        } else 
	        {
					$response=array('status' => FALSE,
					'status_code'=>400,
	    			'error_message' => 'Unable to update');
				    echo json_encode($response);

	        }
	    }
  		else
		{
				$errors = $this->form_validation->error_array();
    			$fields = array_keys($errors);
    			$err_msg = $errors[$fields[0]];
			   	$array = array(
			   	
			    'error'    => true,
			    'status_code'=>400,
			    'error_message'=>$err_msg = $errors[$fields[0]],
			    
   			);
			echo json_encode($array);
		}
	    	 
  	 }
  	 //update international tax api
  	  function update_player_international_tax()
	 {
	 	$this->form_validation->set_error_delimiters('', '');
    	 $this->form_validation->set_rules("user_id", "user id", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("type", "type", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("value", "value", "required|numeric", array('required' => 'Please enter %s.'));
    	   $array = array();
  		if($this->form_validation->run())
  		{
	 		
	 		$user_id=$_POST['user_id'];
	 		$type=$_POST['type'];
	 		$value=$_POST['value'];
	 		$data=$type.'_'.$value;
	 		
	    	 $update_local_tax=$this->General_model->update_data('user_detail',array('international_product_fee'=>$data),array('user_id'=>$user_id)); 
	    	
	    	  if (isset($update_local_tax)) 
	          {
					$response=array('status' => TRUE,
					'status_code'=>200,
		             'message'=>'update successfully',
		            );
		   	 		echo json_encode($response);
	          
	        } else 
	        {
					$response=array('status' => FALSE,
					'status_code'=>400,
	    			'error_message' => 'Unable to update');
				    echo json_encode($response);

	        }
	    }
  		else
		{
				$errors = $this->form_validation->error_array();
    			$fields = array_keys($errors);
    			$err_msg = $errors[$fields[0]];
			   	$array = array(
			   	
			    'error'    => true,
			    'status_code'=>400,
			    'error_message'=>$err_msg = $errors[$fields[0]],
			    
   			);
			echo json_encode($array);
		}
	    	 
  	 }

  	 //add product api
  	  function save_product() {
  		$this->form_validation->set_error_delimiters('', '');
    	$this->form_validation->set_rules("user_id", "user id", "required", array('required' => 'Please enter %s.'));
    	$this->form_validation->set_rules("product_name", "product name", "required", array('required' => 'Please enter %s.'));
    	$this->form_validation->set_rules("category_id", "category id", "required", array('required' => 'Please enter %s.'));
    	$this->form_validation->set_rules("description", "description", "required", array('required' => 'Please enter %s.'));
    	$this->form_validation->set_rules("price", "price", "required", array('required' => 'Please enter %s.'));
    	$this->form_validation->set_rules("qty", "quantity", "required", array('required' => 'Please enter %s.'));

		if ($_FILES['product_image'] && empty($_FILES['product_image']['name'][0])) {
			$this->form_validation->set_rules("product_image", "product image", "required", array('required' => 'Please upload %s.'));
		}

		// check validation for attributes and option 
		if (!empty($_POST['attr_id'])) {
			foreach ($_POST['attr_id'] as $key => $value) {
				$attr['attribute_id'] = $key;
				$attr_data = $this->Product_model->get_attribute_data($attr);
				if (empty($attr_data)) {
					$response = array(
						'status' => FALSE,
						'status_code' => 400,
						'error_message' => 'Failure to find attribute'
					);
				    echo json_encode($response);
				    exit();
				}
				foreach ($value as $k => $opt_val) {
					$attr_check['attribute_id'] = $key;
					$attr_check['option_id'] = $k;
					$attr_check_data = $this->Product_model->get_attribute_data($attr_check);
					if (empty($attr_check_data)) {
						$response = array(
							'status' => FALSE,
							'status_code' => 400,
							'error_message' => 'Failure to find attribute and option'
						);
					    echo json_encode($response);
					    exit();								
					}
				}
			}
		}

		$array = array();
  		if ($this->form_validation->run()) {

			$query = $this->db->query('SELECT max(product_order) as product_order FROM products_tbl');
			$current_product_order = $query->result();
			$product_order = $current_product_order[0]->product_order + 1;
			$data = array(
				'category_id' => $_POST['category_id'].','.$_POST['subcategory_id'],
				'name' => trim($_POST['product_name']),
				'description' => trim($_POST['description']),
				'price' => trim($_POST['price']),
				'final_price' => (isset($_POST['final_price']) && $_POST['final_price'] != '') ? trim($_POST['final_price']) : '0.00',
				'special_price' => ($_POST['special_price'] != '') ? trim($_POST['special_price']) : '0.00',
				'qty' => trim($_POST['qty']),
				'fee' => trim($_POST['fee']),
				'status' => 1,
				'seller_id' => $_POST['user_id'],
				'product_order' =>$product_order,
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			);
			$r = $this->General_model->insert_data('products_tbl',$data);

			if (!empty($_POST['product_image'])) {
				foreach ($_POST['product_image'] as $key => $value) {
					$arr = array(
						'file_dir' => $this->target_dir, 
						'extension' => 'jpg', 
						'base64_string' => $value
					);
					$img = $this->Product_model->base64_to_jpeg($arr);
					if (!$img) {
						$response = array(
							'status' => FALSE,
							'status_code' => 400,
		    				'error_message' => 'Failure to upload image'
		    			);
					    echo json_encode($response);
					    exit();
					} else {
						$main_img_data[] = array(
				        	'product_id' => (isset($r)) ? $r : 0,
				        	'user_id' => (isset($_POST['user_id'])) ? $_POST['user_id'] : 0,
				        	'image' => $img,
				        	'is_default' => '0',
				        	'created_at' => date('Y-m-d H:i:s'),
				        	'updated_at' => date('Y-m-d H:i:s')
				        );
					}
				}
				if (!empty($main_img_data)) {
					$this->db->insert_batch('product_images',$main_img_data);
				}
			}
			if ($r) {
				if (!empty($_POST['attr_id'])) {
					$insert_attr_data = array();
					foreach ($_POST['attr_id'] as $key => $value) {
						if(is_array($value)){
							foreach($value as $option_key => $optId){
								$newOptId = $option_key;
								$newAtrId = $key;

								$attribute_option_image_arr = $_POST['attr_image'][$newAtrId][$newOptId];
								if (isset($attribute_option_image_arr) && !empty($attribute_option_image_arr)) {
									foreach ($attribute_option_image_arr as $k => $aoa) {
										$arr = array(
											'file_dir' => $this->target_dir, 
											'extension' => 'jpg', 
											'base64_string' => $aoa
										);
										$optionimg = $this->Product_model->base64_to_jpeg($arr);
										if (!$optionimg) {
											$response = array(
												'status' => FALSE,
												'status_code' => 400,
							    				'error_message' => 'Failure to upload option image'
							    			);
										    echo json_encode($response);
										    exit();
										}
										$attr_option_arr = array(
											'product_id' => $r,
											'user_id' => (isset($_POST['user_id'])) ? $_POST['user_id'] : 0,
											'attribute_id' => $newAtrId,
											'option_id' => $newOptId,
											'option_image' => $optionimg,
											'created_at' => date('Y-m-d H:i:s'),
											'updated_at' => date('Y-m-d H:i:s')
										);
										$insert_attr_data[] = $attr_option_arr;
									}
								} else {
									$attr_option_arr = array(
										'product_id' => $r,
										'user_id' => (isset($_POST['user_id'])) ? $_POST['user_id'] : 0,
										'attribute_id' => $newAtrId,
										'option_id' => $newOptId,
										'created_at' => date('Y-m-d H:i:s'),
										'updated_at' => date('Y-m-d H:i:s')
									);
									$insert_attr_data[] = $attr_option_arr;
								}

							}
						}
					}
					if (!empty($insert_attr_data)) {
						$this->db->insert_batch('product_attributes_tbl',$insert_attr_data);
					}
				}
				$response = array(
					'status' => TRUE,
					'status_code' => 200,
			        'message' => 'Product Insert successfull',
			    );
			    echo json_encode($response);
			} else {
				$response = array(
					'status' => FALSE,
					'status_code'=>400,
		    		'error_message' => 'Failure to Insert'
		    	);
		    	echo json_encode($response);
			}
	   } else {
			$errors = $this->form_validation->error_array();
			$fields = array_keys($errors);
			$err_msg = $errors[$fields[0]];
		   	$array = array(
		   		'error'    => true,
		    	'status_code'=>400,
		    	'error_message'=>$err_msg = $errors[$fields[0]],
		    );
			echo json_encode($array);
		}
	}

	//update product
	function update_product() {
		$this->form_validation->set_error_delimiters('', '');
    	 $this->form_validation->set_rules("user_id", "user id", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("product_id", "product id", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("product_name", "product name", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("category_id", "category id", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("description", "description", "required", array('required' => 'Please enter %s.'));
    	 
    	 if ($_FILES['product_image'] && empty($_FILES['product_image']['name'][0]))
		{
		    
		    $this->form_validation->set_rules("product_image[]", "product image", "required", array('required' => 'Please upload %s.'));
		}
    	  
    	 $this->form_validation->set_rules("price", "price", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("qty", "quantity", "required", array('required' => 'Please enter %s.'));
    	 
    	   $array = array();
  		if($this->form_validation->run())
  		{
				$id = $this->input->post('product_id', TRUE);
				$data = array(
					'category_id' => $_POST['category_id'].','.$_POST['subcategory_id'],
					'name' => trim($_POST['product_name']),
					'description' => trim($_POST['description']),
					'price' => trim($_POST['price']),
					'final_price' => ($_POST['final_price'] != '') ? trim($_POST['final_price']) : '0.00',
					'special_price' => ($_POST['special_price'] != '') ? trim($_POST['special_price']) : '0.00',
					'qty' => trim($_POST['qty']),
					'fee' => trim($_POST['fee']),
					'status' => 1,
					'updated_at' => date('Y-m-d H:i:s')
				);
				$r = $this->General_model->update_data('products_tbl', $data, array('id'=>$id));

				 if($r) {
				 	//upload product images
				 	 //add product images
			if ($_FILES['product_image']['name'] != '') {
			$cpt = count($_FILES['product_image']['name']);//count for number of image files
	   			
		    for($i=0; $i<$cpt; $i++)
		    {           
					
					$main_img = time().$i.'_'.basename($_FILES['product_image']['name'][$i]);				
					
					$target_file = $this->target_dir.$main_img;
					 move_uploaded_file($_FILES["product_image"]["tmp_name"][$i], $target_file);
					
						$main_img_data = array(
		        	'product_id' => $id,
		        	'user_id' => (isset($_POST['user_id'])) ? $_POST['user_id'] : 1,
		        	'image' => $main_img,
		        	'is_default' => '0',
		        	'created_at' => date('Y-m-d H:i:s'),
		        	'updated_at' => date('Y-m-d H:i:s')
		        );
		        $this->General_model->insert_data('product_images',$main_img_data);
				}
				
			}
				// 	if ($_FILES['game_file']["name"] != '') {
				// 		$game_name = time().'_'.$r.basename($_FILES["game_file"]["name"]);
				// 		$target_file = $this->target_dir.$game_name;
				// 		move_uploaded_file($_FILES["game_file"]["tmp_name"], $target_file);
				// 		$this->General_model->update_data('products_tbl', array('game_name' => $game_name), array('id' => $id));
				// 	}

					// $product_img_data = $this->General_model->view_data('product_images',array('product_id' => $id,'is_default' => '0'));

					// $old_gallery_image = explode(',', $_POST['old_gallery_image']);
					// $t = array_diff(array_column($product_img_data,'id'), $old_gallery_image);
					// $product_attribute_data = $this->General_model->view_data('product_attributes_tbl',array('product_id'=>$id));
					// // if (count($_POST['attr_id']) > 0) {
					// 	foreach ($product_attribute_data as $v) {
					// 		if (!in_array($v['attribute_id'].'_'.$v['option_id'], array_keys($_POST['attr_id']))) {
					// 			$this->General_model->delete_data('product_attributes_tbl',array('id' => $v['id']));
					// 		} else {
					// 			$update_attr_data = array(
					// 				'attribute_id' => $v['attribute_id'],
					// 				'option_id' => $v['option_id'],
					// 				'updated_at' => date('Y-m-d H:i:s')
					// 			);
					// 			$this->General_model->update_data('product_attributes_tbl',$update_attr_data,array('product_id'=>$id,'option_id' => $arr_attr_data[1]));
					// 		}
					// 	}
					// 	$arr_option = array_column($product_attribute_data,'option_id');
					// 	foreach ($_POST['attr_id'] as $key => $value) {
					// 		$arr_attr_data = explode('_', $key);
					// 		if (!in_array($arr_attr_data[1], $arr_option)) {
					// 			$insert_attr_data = array(
					// 				'product_id' => $id,
					// 				'attribute_id' => $arr_attr_data[0],
					// 				'option_id' => $arr_attr_data[1],
					// 				'updated_at' => date('Y-m-d H:i:s'),
					// 				'created_at' => date('Y-m-d H:i:s')
					// 			);
					// 			$this->General_model->insert_data('product_attributes_tbl',$insert_attr_data);
					// 		}
					// 	}
					// }
								$response=array('status' => TRUE,
								'status_code'=>200,
					             'message'=>'Update successfull',
					            );
					   	 		echo json_encode($response);
					
				} else {
								$response=array('status' => FALSE,
								'status_code'=>400,
				    			'error_message' => 'Failure to update');
							    echo json_encode($response);
					
				}
		}
	   else
	   {
			$errors = $this->form_validation->error_array();
			$fields = array_keys($errors);
			$err_msg = $errors[$fields[0]];
		   	$array = array(
		   	
		    'error'    => true,
		    'status_code'=>400,
		    'error_message'=>$err_msg = $errors[$fields[0]],
		    
			);
			echo json_encode($array);
		}
	}
	//delete product image 
	function delete_product_image($id="") {
		

		if (isset($id) && $id != '') 
		{
			
			$r = $this->General_model->delete_data('product_images', array('id' => $id));
			if($r)
			{
						$response=array('status' => TRUE,
						'status_code'=>200,
			             'message'=>'Image deleted successfully',
			            );
			   	 		echo json_encode($response);
			} 
			else
			{
				$response=array('status' => FALSE,
						'status_code'=>400,
		    			'error_message' => 'Failure to delete');
					    echo json_encode($response);
			}
			
  		  }
  		else
  		{
  			$response=array('status' => FALSE,
						'status_code'=>400,
		    			'error_message' => 'Please enter product id');
					    echo json_encode($response);
  		}
	}
	//product delete
	function delete_product($id="") {
		
		if (isset($id) && $id != '') 
		{
			$r = $this->General_model->delete_data('products_tbl', array('id' => $id));
			if($r)
			{
						$response=array('status' => TRUE,
						'status_code'=>200,
			             'message'=>'Item has been Removed from your store',
			            );
			   	 		echo json_encode($response);
			} 
			else
			{
				$response=array('status' => FALSE,
						'status_code'=>400,
		    			'error_message' => 'Failure to delete');
					    echo json_encode($response);
			}
			
  		}
  		else
  		{
  			$response=array('status' => FALSE,
						'status_code'=>400,
		    			'error_message' => 'Please enter product id');
					    echo json_encode($response);
  		}
	}
	
}
