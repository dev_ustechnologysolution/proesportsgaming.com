<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Store extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->target_dir = BASEPATH.'../upload/all_images/';
	}
	 
	function get_store_overview($user_id='')
	{
		if($user_id)
		{
			$user_data = $this->General_model->view_data('user_detail',array('user_id'=>$user_id));
			$store_data = $this->Order_model->get_user_order_details($user_id);
			$data['total_gross_selling']=$store_data[0]['total_gross_selling'] ? $store_data[0]['total_gross_selling'] : '0';		  
			$data['total_items_sold']=$store_data[0]['total_items_sold'] ? $store_data[0]['total_items_sold'] : '0';		  
			$data['total_orders']=$store_data[0]['total_orders'] ? $store_data[0]['total_orders'] : '0';		  
			$data['store_banner']=$user_data[0]['store_banner'] ? base_url().'upload/all_images/'.$user_data[0]['store_banner'] : '';  
			$data['store_link']=$user_data[0]['store_link'] ? $user_data[0]['store_link'] : '0';		  
		
			if(!empty($data))
			{
				$response=array('status' => TRUE,
								'status_code'=>200,
		                   		'store_data'=>$data
		                );
		        echo json_encode($response,JSON_UNESCAPED_SLASHES);

			}
			else
			{
				$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'Data Not Available');
	    			echo json_encode($response);
			}
		}
		else
		{
			$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'Please enter user id');
	    			echo json_encode($response);

		}
		

	}

	//get level api
	function get_store_level($user_id="")
	{
		$data['levels'] = $this->General_model->view_all_data('store_levels','order_id','asc');
		foreach ($data['levels'] as $key => $value) {
            	
            	$data['levels'][$key]['level_img']=base_url()."upload/all_images/".$value['level_img'];
            	  
            }
           if(!empty($user_id))
           { 
          		$user_level['user_level'] = $this->General_model->view_single_row('user_level',array('user_id' => $user_id), '*');  

          		 $this->db->select('*');
		        $this->db->select('SUM(sold_items) AS items', FALSE);
		        $this->db->from('user_level_items'); 
		        $this->db->where('user_level_id', $user_level['user_level']['id']);       
		        $this->db->group_by('level');           
		        $sql = $this->db->get();
		        $data['user_level_data'] = $sql->result_array();

		        foreach ($data['levels'] as $key => $lvl) {
		        	$data['levels'][$key]['status']=(in_array($lvl['level'], array_column($data['user_level_data'], 'level')) && $user_level['user_level']['current_level'] != $lvl['level']) ? 'Reached' : '';
		            $levels_array[] = $lvl['level_title'];
		            $user_level_data_array[] = ($data['user_level_data'][$key]['items']) ? $data['user_level_data'][$key]['items'] : 0;
		            $user_level_data_array_remain[] = $lvl['item_sold'] - $data['user_level_data'][$key]['items'];
		        }
		      
		         $data['levels_array'] = $levels_array;
        		 $data['user_level_data_array'] = $user_level_data_array;
        		 $data['user_level_data_array_remain'] = $user_level_data_array_remain;

		        $current_key = array_search($user_level['user_level']['current_level'],array_column($data['levels'], 'level'));
		        $data['current_level'] = $data['levels'][$current_key]; 
           }
			if(!empty($data))
			{
				$response=array('status' => TRUE,
								'status_code'=>200,
		                   		'data'=>$data
		                );
		        echo json_encode($response,JSON_UNESCAPED_SLASHES);

			}
			else
			{
				$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'Data Not Available');
	    			echo json_encode($response);
		}
	}

	//banner update api
	function update_store_banner()
	{	
		 $this->form_validation->set_error_delimiters('', '');
		 $this->form_validation->set_rules("user_id", "user id", "required", array('required' => 'Please enter %s.'));
    	 	if (empty($_FILES['banner_image']['name']))
			{
		    
		    	$this->form_validation->set_rules("banner_image", "banner image", "required", array('required' => 'Please upload %s.'));
			}
		$array = array();
  		if($this->form_validation->run())
  		{

			$user_id=$_POST['user_id'];
			$img=$_FILES['banner_image']['name'];
	        if(isset($_FILES["banner_image"]["name"]) && $_FILES["banner_image"]["name"]!='')
	        {
	            $img=time().basename($_FILES["banner_image"]["name"]);
	            $target_file = $this->target_dir.$img;

	            if(move_uploaded_file($_FILES["banner_image"]["tmp_name"], $target_file))
	            {
	            	$old_image=$this->General_model->view_single_row('user_detail',array('user_id' => $user_id), 'store_banner');
	            
	                if(file_exists($this->target_dir.$old_image['store_banner']))
	                {
	                    unlink($this->target_dir.$old_image['store_banner']);
	                }
	            }
	        }
	        
	        $data=array(
	            'store_banner'=>$img
	            );
	        $r = $this->General_model->update_data('user_detail',$data,array('user_id'=>$user_id));
	        if($r)
				{
					$response=array('status' => TRUE,
									'status_code'=>200,
			                   		'data'=>'Update successfull'
			                );
			        echo json_encode($response,JSON_UNESCAPED_SLASHES);

				}
				else
				{
					$response=array('status' => FALSE,
									'status_code'=>400,
		                			'message' => 'Failure to update');
		    			echo json_encode($response);
			}
	   }
	   else
	   {
			$errors = $this->form_validation->error_array();
			$fields = array_keys($errors);
			$err_msg = $errors[$fields[0]];
		   	$array = array(
		   	
		    'error'    => true,
		    'status_code'=>400,
		    'error_message'=>$err_msg = $errors[$fields[0]],
		    
			);
			echo json_encode($array);
		}

        
	}

	//change storage link api
	 function update_store_link() 
	 {
	 	 $this->form_validation->set_error_delimiters('', '');
		 $this->form_validation->set_rules("user_id", "user id", "required", array('required' => 'Please enter %s.'));
    	 $this->form_validation->set_rules("store_link", "store link", "required|alpha_numeric_spaces", array('required' => 'Please enter %s.'));
			
		$array = array();
  		if($this->form_validation->run())
  		{	
            $store_link = trim($_POST['store_link']);
            $update_data['store_link'] = $store_link;
            $user_id=$_POST['user_id'];
            $update_data['store_link']=str_replace(" ","-",$update_data['store_link']);
           
            $checklink = array(
                'store_link' => $store_link,
                'user_id !=' => $user_id
            );
            
            $userdata = $this->General_model->view_single_row('user_detail',$checklink,'store_link');
            if(!empty($userdata))
            {
				$response=array('status' => FALSE,
							    'status_code'=>400,
	            			     'message' => 'Store is exist with this name. Please try another name..');
				echo json_encode($response);
            }
            elseif (strpos($store_link, 'account') !== false) 
            {
				$response=array('status' => FALSE,
							    'status_code'=>400,
	            			    'message' => "Can't use Account keyword. Please try another name..");
				echo json_encode($response);
            }
            else
            {
            	
            	$r = $this->General_model->update_data('user_detail', $update_data, array('user_id' =>$user_id));
            	if($r)
            	{
            		$response=array('status' => TRUE,
									'status_code'=>200,
			                   		'data'=>'Store link has been Updated.');
			        echo json_encode($response);
            	}
            	else
            	{

            		$response=array('status' => FALSE,
									'status_code'=>400,
		                			'message' => 'Failure to update store link');
		    		echo json_encode($response);
            	}
            }
        }
	   else
	   {
			$errors = $this->form_validation->error_array();
			$fields = array_keys($errors);
			$err_msg = $errors[$fields[0]];
		   	$array = array(
		   	
		    'error'    => true,
		    'status_code'=>400,
		    'error_message'=>$err_msg = $errors[$fields[0]],
		    
			);
			echo json_encode($array);
		} 
    }

    //delete store banner api
    public function delete_store_banner($user_id=""){
    	if($user_id)
    	{
	        $data=array(
	            'store_banner'=>''
	            );
	        $r = $this->General_model->update_data('user_detail',$data,array('user_id'=>$user_id));
	        if($r)
	    	{
	    		$response=array('status' => TRUE,
								'status_code'=>200,
		                   		'data'=>'Banner removed successfully');
		        echo json_encode($response);
	    	}
	    	else
	    	{

	    		$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'Fail to remove banner');
	    		echo json_encode($response);
	    	}
	    }
	    else
	    {
	    	$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'Please enter user id');
	    		echo json_encode($response);
	    }
    }

	
}
