<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class User extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->target_dir=BASEPATH.'../upload/profile_img/';
		
	}
	
	public function register() 
	{

		$user = array(
			'email'=>$this->input->post('email', TRUE),
			'password'=>md5($this->input->post('password', TRUE)),
		);
		// if(!empty($this->input->post('email', TRUE)) && !empty($this->input->post('password', TRUE)) && !empty($this->input->post('first_name', TRUE)) &&  !empty($this->input->post('last_name', TRUE)) &&  !empty($this->input->post('display_name', TRUE)))
		// {
			 $this->form_validation->set_error_delimiters('', '');
			  			  
			  $this->form_validation->set_rules("first_name", "First Name", "required", array('required' => 'Please Enter %s.'));
			  $this->form_validation->set_rules("last_name", "Last Name", "required", array('required' => 'Please Enter %s.'));
			  $this->form_validation->set_rules("email", "Email", "required|valid_email|is_unique[user.email]", array('required' => 'Please enter %s.'));
			  $this->form_validation->set_rules("password", "password", "required", array('required' => 'Please Enter %s.'));
			  $this->form_validation->set_rules("friends_mail", "Friends Mail", "required", array('required' => 'Please Enter %s.'));
			  $this->form_validation->set_rules("team_mail", "team Mail", "required", array('required' => 'Please Enter %s.'));
			  $this->form_validation->set_rules("challenge_mail", "challenge Mail", "required", array('required' => 'Please Enter %s.'));
    	   $array = array();
  if($this->form_validation->run())
  {
			if ($this->input->post('email', TRUE)!='') 
			{
				if($this->User_model->check_mail('user',array('email'=>$this->input->post('email', TRUE)))) {
					 
					 $response=array('status' => FALSE,
					 				'status_code'=>400,
	                    			'error_message' => 'Your email already exist');
	        		echo json_encode($response);
				}
				elseif($this->input->post('password', TRUE) != $this->input->post('re_type_password', TRUE))
				{
					$response=array('status' => FALSE,
									'status_code'=>400,
	                    			 'error_message' => 'password and re_type_password are not same');
	        		echo json_encode($response);
				}
				 else {
					//insert data in user table
					 $r = $this->General_model->insert_data('user',$user);

					$friends_mail = $this->input->post('friends_mail', TRUE); 
					$team_mail = $this->input->post('team_mail', TRUE);
					$challenge_mail = $this->input->post('challenge_mail', TRUE);
					
					// (isset($_POST['challenge_mail'])) ? 'yes' : 'no';

					$mail_cate_data = array(
						'friends_mail' => $friends_mail==1 ? 'yes' : 'no',
						'team_mail' => $team_mail==1 ? 'yes' : 'no',
						'challenge_mail' => $challenge_mail==1 ? 'yes' : 'no',
						'user_id' => $r
					 );
		
					//insert data in mail category
					$insert_mail = $this->General_model->insert_data('mail_categories',$mail_cate_data);

					$full_name= $this->input->post('first_name', TRUE)." ".$this->input->post('last_name', TRUE);
					$query = $this->db->query("SELECT MAX(account_no) as account_no FROM user_detail");

					$row = $query->row();
					$user_detail = array(
						'user_id' => $r,
						'name' => $full_name,
						'display_name' => $this->input->post('display_name', TRUE),
						'user_reference' => 2,
						'account_no' => $row->account_no + 1,
						'custom_name' => $row->account_no + 1,
						'store_link' => $row->account_no + 1,
					);
					//insert userdatail data
					$insert_user_detail=$this->General_model->insert_data('user_detail',$user_detail);
					
					if(isset($insert_user_detail) && isset($insert_mail) && isset($r)) {
						$response=array('status' => TRUE,
										'status_code'=>200,
	                    				'message' => 'The user has been register successfully.');
	        			echo json_encode($response);
	        		}
	        		else {
							$response=array('status' => FALSE,
											'status_code'=>400,
	                    					'message' => 'register unsuccessfull');
	        			echo json_encode($response);
					}
				
				}	
			}

		}
		
		 else
  		{

  			$errors = $this->form_validation->error_array();
    		$fields = array_keys($errors);
    		$err_msg = $errors[$fields[0]];
			   $array = array(
			   	
			    'error'    => true,
			    'status_code'=>400,
			    'error_message'=>$err_msg = $errors[$fields[0]],
			    
   			);
			echo json_encode($array);	   
  		}
 
  
			
	}
	public function login() 
	{
			// if(!empty($this->input->post('email', TRUE)) && !empty($this->input->post('password', TRUE)))
			// {

		;

			  $this->form_validation->set_error_delimiters('', '');
			  $this->form_validation->set_rules("email", "Email", "required|valid_email", array('required' => 'Please enter %s.'));
			  $this->form_validation->set_rules("password", "password", "required", array('required' => 'Please enter %s.'));
			  $array = array();
  			if($this->form_validation->run())
  			{
				// ($this->session->userdata('user_id') != '') ? redirect(base_url()) : '';
				$email =$this->input->post('email', TRUE);
				$password = md5($this->input->post('password', TRUE));
				$data_arr = $this->User_model->authenticate($email,$password);
				$changeactive_flag = $this->User_model->changeactive_flag($data_arr);
				if (!empty($data_arr) && $data_arr->ban_option == 0) {
					$this->User_model->log_this_login($data_arr);
				    $this->session->unset_userdata('login_att');

					$response=array('status' => TRUE,
									'status_code'=>200,
	                    'message' => 'login Successfully.',
	                    'user_data'=>$data_arr
	                    
	                );
	        		echo json_encode($response);
						
					
				} else if (!empty($data_arr) && $data_arr->ban_option > 0) {
					$data['err'] = "";
					if ($email != '') {
						$checkbanned = $this->User_model->checkbanned($data_arr);
						$response=array('status' => FALSE,
										'status_code'=>400,
	                    				'error_message' => $checkbanned);
	        							echo json_encode($response);
						// $data['err'] ='<h5 style="margin-left: 255px;color: red;">'.$checkbanned.'</h5>';
					}
				
				} else 
				{

					// $data['err'] = ($email != '') ? '<h5 style="margin-left: 255px;color: red;">Invalid username or password</h5>' : '';

						$response=array('status' => FALSE,
										'status_code'=>400,
	                    				'error_message' => 'Invalid email or password');
	        			echo json_encode($response);
					($this->session->userdata('login_att') == '') ? $this->session->set_userdata('login_att','1') : $this->session->set_userdata('login_att',$this->session->userdata['login_att'] + 1);
					if ($this->session->userdata('login_att') > 7) {
						// $data['err'] ='<h5 style="margin-left: 255px;color: red;">Your Account Has Been Blocked </h5>';
						$response=array('status' => FALSE,
										'status_code'=>400,
	                    				'error_message' => 'Your Account Has Been Blocked');
	        							echo json_encode($response);
						$user_data	=	array(
							'is_active'	=>	'1',
						);
						$this->General_model->update_data('user',$user_data,array('email'=>$email));
					}
					
				}
			}
			else
  		{
  			$errors = $this->form_validation->error_array();
    		$fields = array_keys($errors);
    		$err_msg = $errors[$fields[0]];
			   $array = array(
			   	
			    'error'    => true,
			    'status_code'=>400,
			    'error_message'=>$err_msg = $errors[$fields[0]],
			    
   			);
			echo json_encode($array);
		}
	}

	//forgot password api
	function forgot_password()
    {

  //   	if(!empty($this->input->post('email', TRUE)))
		// {
    	$this->form_validation->set_error_delimiters('', '');
    	  $this->form_validation->set_rules("email", "Email", "required|valid_email", array('required' => 'Please enter %s.'));
    	   $array = array();
  if($this->form_validation->run())
  {
				$view_data=array();
				if($this->input->post('email', TRUE)!='')
				{
					$user=$this->General_model->view_data('user',array('email'=>$this->input->post('email', TRUE)));
					if(isset($user)&& count($user)>0)
					{
						$user_id=$user[0]['id'];
						$user_name=$this->General_model->view_data('user_detail',array('user_id'=>$user_id));
						$user_name=$user_name[0]['name'];

						$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
						$password = substr(str_shuffle($chars), 0, 6);    		    		
						$email = $this->input->post('email', TRUE);
						$token = array('token'=>$password);

						$this->General_model->update_data('user',$token , array('email'=>$email));			
						$data = array('password'=>md5($password));
						if($this->General_model->update_data('user',$data,array('email'=>$this->input->post('email', TRUE))))
							$to =$email;
							$header = base_url().'/assets/frontend/images/email/header.png';
						$subject = "Recovery Password";
						$message ='<html><body><table cellpadding="0" cellspacing="0" broder="0" width="600">';
						$message .='<tr><td><img src="'.$header.'" style="width:100%;"></td></tr>';
						$message .='<tr><td><p> Hello '.$user_name.'</p><p>Please click on the below link to reset your password.</p><a href='.base_url().'user/reset_password/'.$password.'>Click here</a><p>Thanks,</p></td></tr>';
						$message .='<tr><td><img src="'.base_url().'/assets/frontend/images/email/footer.png" style="width:100%;"></td></tr>';
						$message .='</table></body></html>';

						$headers = "MIME-Version: 1.0" . "\r\n";
						$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
						$headers .= 'From: <noreply@ProEsportsGaming.com>' . "\r\n";
						if(mail($to,$subject,$message,$headers)){
							// $view_data['err']='<span style="color:green">please check your mail</span>';
							$response=array('status' => TRUE,
											'status_code'=>200,
		                					'message' => 'please check your mail');
		    			echo json_encode($response);

						} else {
							$re = $this->General_model->sendMail($to,$user_name,$subject,$message);
							if($re == 1){
								// $view_data['err']='<span style="color:green">please check your mail</span>';
								$response=array('status' => TRUE,
												'status_code'=>200,
		                						'message' => 'please check your mail');
		    						echo json_encode($response);
							} else {

								// $view_data['err']='<span style="color:red">mail not send </span>';
								$response=array('status' => FALSE,
												'status_code'=>400,
		                						'message' => 'mail not send');
		    									echo json_encode($response);
							}
						}
					} else {
						// $view_data['err']='<span style="color:red">Give proper email</span>';
							$response=array('status' => FALSE,
											'status_code'=>400,
		                					'message' => 'Give proper email');
		    								echo json_encode($response);
					}
				}
				// $content=$this->load->view('login/forgot_pass',$view_data,true);
				// $this->render($content);
		}
		else
  		{
   			$errors = $this->form_validation->error_array();
    		$fields = array_keys($errors);
    		$err_msg = $errors[$fields[0]];
			   $array = array(
			   	
			    'error'    => true,
			    'status_code'=>400,
			    'error_message'=>$err_msg = $errors[$fields[0]],
			    
   			);
			echo json_encode($array);	
  		}
  
  
	}

	//get user info 
	function Get_Profile_info($id="")
	{
		if($id)
		{ 
		
			$data_arr = $this->User_model->get_user_data($id);
			if(!empty($data_arr))
			{
				if($data_arr->image){
					$data_arr->image = base_url()."upload/profile_img/".$data_arr->image;
				}
				$response=array('status' => TRUE,
								'status_code'=>200,
		                   		'user_profile'=>$data_arr
		                );
		        echo json_encode($response);

			}
			else
			{
				$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'User does not exists');
	    			echo json_encode($response);
			}
		}
		else
		{
			$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'Please enter user id');
	    			echo json_encode($response);
		}
		

	}
	//update user data
	function update_profile_info()
	{
			
			//update email and password
		 $this->form_validation->set_error_delimiters('', '');
			  $this->form_validation->set_rules("user_id", "user id", "required", array('required' => 'Please enter %s.'));
			  $this->form_validation->set_rules("first_name", "First Name", "required", array('required' => 'Please enter %s.'));
			  $this->form_validation->set_rules("last_name", "Last Name", "required", array('required' => 'Please enter %s.'));
			  $this->form_validation->set_rules("email", "Email", "required|valid_email", array('required' => 'Please enter %s.'));
			  $this->form_validation->set_rules("password", "password", "required", array('required' => 'Please enter %s.'));
			  $this->form_validation->set_rules("friends_mail", "Friends Mail", "required", array('required' => 'Please enter %s.'));
			  $this->form_validation->set_rules("team_mail", "team Mail", "required", array('required' => 'Please enter %s.'));
			  $this->form_validation->set_rules("challenge_mail", "challenge Mail", "required", array('required' => 'Please enter %s.'));
    	   $array = array();
  if($this->form_validation->run())
  {
					
				
				if($this->input->post('password', TRUE) != $this->input->post('re_type_password', TRUE))
				{
					$response=array('status' => FALSE,
			            'message' => 'password and re_type_password are not same');
					echo json_encode($response);
				}
				else
				{
					$id = $this->input->post('user_id',TRUE);
					$email = array('email'=>$this->input->post('email', TRUE));
					$this->General_model->update_data('user',$email,array('id'=>$id));
					if($this->input->post('password', TRUE) != '') {
					$password=array('password'=>md5($this->input->post('password', TRUE)));
					$r=$this->General_model->update_data('user',$password,array('id'=>$id));

					$friends_mail = $this->input->post('friends_mail', TRUE); 
					$team_mail = $this->input->post('team_mail', TRUE);
					$challenge_mail = $this->input->post('challenge_mail', TRUE);
					
					// (isset($_POST['challenge_mail'])) ? 'yes' : 'no';

					$mail_cate_data = array(
						'friends_mail' => $friends_mail==1 ? 'yes' : 'no',
						'team_mail' => $team_mail==1 ? 'yes' : 'no',
						'challenge_mail' => $challenge_mail==1 ? 'yes' : 'no',
						
					 );

					//update data in mail category
					$update_mail =  $this->General_model->update_data('mail_categories',$mail_cate_data,array('user_id' => $id));

					$full_name= $this->input->post('first_name', TRUE)." ".$this->input->post('last_name', TRUE);

					$user_detail = array(
						
						'name' => $full_name,
						'display_name' => $this->input->post('display_name', TRUE),
						'user_reference' => 2
					);

					//update userdatail data
					$update_user_detail=$this->General_model->update_data('user_detail',$user_detail,array('user_id'=>$id));
					// echo "<pre>";
					// print_r($update_user_detail);
					// exit();
					if(isset($update_user_detail) && isset($update_mail) && isset($r)) 
					{
						$response=array('status' => TRUE,
										'status_code'=>200,
			            				'message' => 'update successfully');
						echo json_encode($response);
					}
					else 
					{
							$response=array('status' => TRUE,
											'status_code'=>400,
			            					'message' => 'Unable to update');
						echo json_encode($response);
					}
				
				}	
			}
		}
		 else
  		{
			   	$errors = $this->form_validation->error_array();
    			$fields = array_keys($errors);
    			$err_msg = $errors[$fields[0]];
			   	$array = array(
			   	
			    'error'    => true,
			    'status_code'=>400,
			    'error_message'=>$err_msg = $errors[$fields[0]],
			    
   			);
			echo json_encode($array);
  		}

	}
	//get user info 
	function get_all_players() {
		$data_arr = $this->User_model->get_app_user();
		if (!empty($data_arr)) {
			$response = array('status' => TRUE,
				'status_code'=>200,
				'userslist'=>$data_arr
			);
			echo json_encode($response, JSON_PRETTY_PRINT);
		} else {
			$response = array('status' => FALSE,
				'status_code'=>400,
				'error_message' => 'User does not exists'
			);
			echo json_encode($response);
		}
	}

	//get profile image api
	function get_profile_img($user_id)
	{
		$data_arr = $this->User_model->get_user_data($user_id);
		if ($data_arr->image != '' && file_exists(BASEPATH.'../upload/profile_img/'.$data_arr->image)) {
    			$image = $data_arr->image;
    		}
		if(!empty($image))
		{
			$image=base_url()."upload/profile_img/".$image;
			$response=array('status' => TRUE,
							'status_code'=>200,
	                   		'profile_img'=>$image
	                );
	        echo json_encode($response,JSON_UNESCAPED_SLASHES);

		}
		else
		{
			$response=array('status' => FALSE,
							'status_code'=>400,
                			'error_message' => 'user profile image does not exists');
    			echo json_encode($response);
		}
		
	}

	//update profile image api
	function update_profile_img()
	{
		 $this->form_validation->set_error_delimiters('', '');
		 $this->form_validation->set_rules("user_id", "user id", "required", array('required' => 'Please enter %s.'));
    	 	if (empty($_FILES['image']['name']))
			{
		    
		    	$this->form_validation->set_rules("image", "image", "required", array('required' => 'Please upload %s.'));
			}
		$array = array();
  		if($this->form_validation->run())
  		{

			$user_id=$_POST['user_id'];
			if (isset($_FILES["image"]["name"]) && $_FILES["image"]["name"]!='') {
				$img=time().basename($_FILES["image"]["name"]);
				$ext = end((explode(".", $img)));
				$ext =strtolower($ext);
			
				if ($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg') {
					
					$target_file = $this->target_dir.$img;
					if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
						$old_image=$this->General_model->view_single_row('user_detail',array('user_id' => $user_id), 'image');
						if (file_exists($this->target_dir.$old_image['image'])) {
							unlink($this->target_dir.$old_image['image']);
						}
					}
					$data=array(
		            'image'=>$img
		            );
		        	$r = $this->General_model->update_data('user_detail',$data,array('user_id'=>$user_id));
			        if($r)
					{
						$response=array('status' => TRUE,
											'status_code'=>200,
					                   		'data'=>'Profile Image Update Successfully'
					                );
					        echo json_encode($response,JSON_UNESCAPED_SLASHES);

						}
						else
						{
							$response=array('status' => FALSE,
											'status_code'=>400,
				                			'message' => 'Failure to update');
				    			echo json_encode($response);
					}
				} 
				else 
				{
					
					$response=array('status' => FALSE,
								'status_code'=>400,
	                			'error_message' => 'This is not a image file');
	    			echo json_encode($response);
					
				}
				
			}
		}
	   else
	   {
			$errors = $this->form_validation->error_array();
			$fields = array_keys($errors);
			$err_msg = $errors[$fields[0]];
		   	$array = array(
		   	
		    'error'    => true,
		    'status_code'=>400,
		    'error_message'=>$err_msg = $errors[$fields[0]],
		    
			);
			echo json_encode($array);
		}	
	}
}
