<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Wishlist extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->target_dir=BASEPATH.'../upload/profile_img/';
	}
	 
	function get_products($user_id="")
	{
		if($user_id)
		{

			$data = $this->Cart_model->get_wishlist_items($user_id);
			
			foreach ($data as $key => $value) {
		        	$data[$key]['sel_variation'] = $this->convertSelVariantToArray($data[$key]['sel_variation']);
					$data[$key]['sel_variation_amount'] = $this->convertSelVariantToArray($data[$key]['sel_variation_amount']);
		        	$data[$key]['product_image']= base_url()."upload/products/".$value['product_image'];
		        }
			  
			
			if(!empty($data))
			{
				$response=array('status' => TRUE,
								'status_code'=>200,
		                   		'wish_list'=>$data
		                );
		        echo json_encode($response,JSON_UNESCAPED_SLASHES);

			}
			else
			{
				$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'Your Wishlist is Empty');
	    			echo json_encode($response);
			}
		}
		else
		{
			$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'Please enter user id');
	    			echo json_encode($response);

		}
		

	}

	// delete wishlist product api
	function delete_wishlist_product($id="")
	{
	 if ($id)
	 {		
		  $wishlist_item_id = $id;
		  $r = $this->General_model->delete_data('wishlist_item',array('id'=>$wishlist_item_id));
		
		  if($r)
		  {
		  				$response=array('status' => TRUE,
								'status_code'=>200,
		                   		'message'=>'Item has been Removed from wishlist successfully'
		                );
		        		echo json_encode($response,JSON_UNESCAPED_SLASHES);

		  }
		  else
		  {
		  			$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'Failure to delete');
	    			echo json_encode($response);

		  }
	  }
	  else
	  {
	  		$response=array('status' => FALSE,
								'status_code'=>400,
	                			'message' => 'Please enter wishlist item id');
	    			echo json_encode($response);
	  }

	}

	//add to cart from wishlist api
	function add_to_cart_from_wishlist_product($id='') {
    if (!empty($id)) 
    {	
    	
	      $getitem = $this->db->get_where('wishlist_item',array('id' => $id))->row();
	      $insert_data = array(
	        'action' => 'from_wishlist',
	        'user_id' => $getitem->user_id,
	        'product_id' => $getitem->product_id,
	        'product_image' => $getitem->product_image,
	        'product_name' => $getitem->product_name,
	        'qty' => $getitem->qty,
	        'amount' => $getitem->amount,
	        'product_fee' => $getitem->product_fee,
	        'sel_variation' => $getitem->sel_variation,
	        'sel_variation_amount' => $getitem->sel_variation_amount,
	        'ref_seller_id' => $getitem->ref_seller_id
	      );
	      
	      $insert_product = $this->Cart_model->add_item_cart($insert_data);
	      if ($insert_product['added'] == true) {
	        	$this->General_model->delete_data('wishlist_item',array('id'=>$id));
	       	 	$response=array('status' => TRUE,
								'status_code'=>200,
				                 'message'=>'Item added into Cart',
				                		);
				   echo json_encode($response);
	        
	  
	      } else if ($insert_product['errmsg'] !='') {
	      		$response=array('status' => FALSE,
								'status_code'=>400,
			        			'error_message' =>$insert_product['errmsg']);
							 echo json_encode($response);
	     
	      } else {
	      		$response=array('status' => FALSE,
									'status_code'=>400,
				        			'error_message' => 'Unable to add item');
								    echo json_encode($response);
	        	
	      }
	}
    else 
    {
        		$response=array('status' => FALSE,
								'status_code'=>400,
			        			'error_message' => 'Unable to add item');
							    echo json_encode($response);
     }
  }
}
