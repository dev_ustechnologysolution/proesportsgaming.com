<?php
// date_default_timezone_set('UTC');
// $paypal_url='https://www.sandbox.paypal.com/cgi-bin/webscr';
$paypal_url='https://www.paypal.com/cgi-bin/webscr';
function getSQLLimit($toshow=10,$page=0) {
    $start=0;
    $sql='';
    if(isset($page) && $page>0)
    {
        $start = ($page-1)*$toshow;
    }
    if($toshow!=-1)
        $sql = " limit $start, $toshow";
    return $sql;
}
function makeOption($value = array(),$id = '')
{
    $option = '';
    if($value)
    {
        if($id=='')
            $id =-1;
        foreach ($value as $key=>$txt)
        {
            $select = '';
            if($key == $id)
                $select = " selected ";
            $option     .="<option $select value='$key'>$txt</option>";
        }
    }
    return $option;
}
function paggingInitialization($obj,$dataArr=array())
{
    $obj->load->library('pagination');
    $config['base_url'] = $dataArr['base_url'];
    $config['total_rows'] = $dataArr['total_row'];
    $config['per_page'] = $dataArr['per_page'];
    $config['uri_segment'] = $dataArr['uri_segment'];
    $config['next_link'] = $dataArr['next_link'];
    $config['prev_link'] = $dataArr['prev_link'];
    $config['page_query_string']=false;

    $obj->pagination->initialize($config);
}
function add_date($date='',$time='86400',$return='')
{
    if($date)
    {
        if(is_numeric($date))
            $date	= $date + $time;
        else
            $date	= strtotime($date)+$time;
    }
    else
        $date	= time()+$time;
    if($return)
        return $date;
    else
        return date('Y-m-d',$date);
}
function sendGIF()
{
    // open the file in a binary mode
    $name = BASEPATH."../images/front/company_logo.png";
//        echo $this->public_path;
    //Open and read in binary
    $fp = fopen($name, 'rb');
    // send the right headers
    header("Pragma: no-cache");
    header("Cache-Control: no-cache, must-revalidate");
    header("Content-Type: image/gif");
    header("Content-Length: " . filesize($name));
    // dump the picture and stop the script
    fpassthru($fp);
}
######################################### following functions created by iman #####################################
function upload_file($obj, $arr , $filename)
{
    $obj->load->library('upload');
    $obj->upload->initialize($arr);
    /*$config['upload_path'] = $arr['upload_path'];
    $config['file_name'] = $arr['file_name'];
    $config['allowed_types'] = $arr['allowed_types'];
    $config['max_size']	= $arr['max_size'];
    $config['max_width']  = $arr['max_width'];
    $config['max_height']  = $arr['max_height'];
    $obj->load->library('upload', $config);*/
    $upload = $obj->upload->do_upload($filename);
//	$obj->upload->clear();
    $retArr='';
    if($upload)
        $retArr=$obj->upload->data();
    else
        $retArr=$obj->upload->display_errors('','|');
    return $retArr;
}
function create_thumb($obj, $arr)
{
    $obj->load->library('image_lib');
    $obj->image_lib->initialize($arr);
    $return = $obj->image_lib->resize();
    $obj->image_lib->clear();
    return 	$return;
}
function getExtension($filename)
{
    preg_match('/(^.*)\.([^\.]*)$/', $filename, $matches);
    return $matches;
}
function get_date_hr($sec){
    $days = floor($sec/86400);
    $remind = (int)($sec%86400);
    $hr = (int)($remind/3600);
    if($hr<0 || $days<0)
        return '--';
    return  $days." d ".$hr." hr";
}
function dbOut($str) {
    return stripslashes($str);
}

function time_elapsed_string($ptime) {
    $etime = time() - $ptime;

    if ($etime < 1)
    {
        return '0 seconds';
    }

    $a = array( 365 * 24 * 60 * 60  =>  'year',
                 30 * 24 * 60 * 60  =>  'month',
                      24 * 60 * 60  =>  'day',
                           60 * 60  =>  'hour',
                                60  =>  'minute',
                                 1  =>  'second'
                );
    $a_plural = array( 'year'   => 'years',
                       'month'  => 'months',
                       'day'    => 'days',
                       'hour'   => 'hours',
                       'minute' => 'minutes',
                       'second' => 'seconds'
                );

    foreach ($a as $secs => $str)
    {
        $d = $etime / $secs;
        if ($d >= 1)
        {
            $r = round($d);
            return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
        }
    }
}
?>