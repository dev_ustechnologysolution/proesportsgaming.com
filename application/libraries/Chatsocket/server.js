// config
'use strict';
var ishttps = true;
var fs = require("fs");
var socket = require( 'socket.io' );
var express = require( 'express' );
if (ishttps == true) {
    var hskey = fs.readFileSync('/var/www/html/proesportsgaming.com/ssl-cert/new/proesportsgaming_private_latest.key', 'utf8');
    var hscert = fs.readFileSync('/var/www/html/proesportsgaming.com/ssl-cert/new/proesportsgaming_latest.crt', 'utf8');
    var options = {
        key: hskey,
        cert: hscert,
        passphrase: '6*4ZfY77R90d'
    };
    var https = require( 'https' );
    var app = express();
    var server = https.createServer(options,app);
} else {
    var http = require( 'http' );
    var app = express();
    var server = http.createServer(app);
}
var io = socket.listen(server);

var socketstorage = {};
var users = {};

const NodeMediaServer = require('node-media-server');
const config = {
  rtmp: {
    port: 1935,
    chunk_size: 60000,
    gop_cache: true,
    ping: 30,
    ping_timeout: 60
  },
  http: {
    port: 8000,
    allow_origin: '*'
  }
};
if (ishttps == true) {
  var fs = require("fs");
  var hskey = '/var/www/html/proesportsgaming.com/ssl-cert/new/proesportsgaming_key_latest.pem';
  var hscert = '/var/www/html/proesportsgaming.com/ssl-cert/new/proesportsgaming_chain_latest.pem';
  config.https = {
      port:8100,
      key: hskey,
      cert: hscert,
      passwordphrase: '6*4ZfY77R90d'
    }
}

function arraySearch(arr,val) {
    for (var i=0; i<arr.length; i++)
        if (arr[i] === val)
            return i;
    return false;
}

/*
Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

*/

/**
 * Get the key by value
 * 
 * @param  {String} value [Value]
 * @return {Number}       [Key]
 */
// Object.prototype.getKeyByValue = function( value ) {
//     for( var prop in this ) {
//         if( this.hasOwnProperty( prop ) ) {
//              if( this[ prop ] === value )
//                  return prop;
//         }
//     }
// };

// DB connection
const mysql = require('mysql2/promise');
var dbconfig = {
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'esportst_db'
};
(ishttps == true) ? dbconfig.password = 'SG2@18' : '';
const msconnection = mysql.createPool(dbconfig);

var nms = new NodeMediaServer(config);
nms.run();
nms.on('preConnect', (id, args) => {
  console.log('[NodeEvent on preConnect]', `id=${id} args=${JSON.stringify(args)}`);
  // let session = nms.getSession(id);
  // session.reject();
});
 
nms.on('postConnect', (id, args) => {
  console.log('[NodeEvent on postConnect]', `id=${id} args=${JSON.stringify(args)}`);
});
 
nms.on('doneConnect', (id, args) => {
  console.log('[NodeEvent on doneConnect]', `id=${id} args=${JSON.stringify(args)}`);
});
 
nms.on('prePublish', (id, StreamPath, args) => {
  console.log('[NodeEvent on prePublish]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
  // let session = nms.getSession(id);
  // session.reject();
});
 
nms.on('postPublish', (id, StreamPath, args) => {
  console.log('[NodeEvent on postPublish]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
  var key = StreamPath.replace('/live/', '');
  myfunction(key);
});
 
nms.on('donePublish', (id, StreamPath, args) => {
  console.log('[NodeEvent on donePublish]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
  var key = StreamPath.replace('/live/', '');
  stopStream(key);
});
 
nms.on('prePlay', (id, StreamPath, args) => {
  console.log('[NodeEvent on prePlay]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
  // let session = nms.getSession(id);
  // session.reject();
});
 
nms.on('postPlay', (id, StreamPath, args) => {
  console.log('[NodeEvent on postPlay]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
});
 
nms.on('donePlay', (id, StreamPath, args) => {
  console.log('[NodeEvent on donePlay]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
});

const stopStream = async function endStream(key){
    try{
        var stream_arr = [];
        var followcnt;
        var rows1 = await msconnection.query('SELECT * FROM stream_settings WHERE key_option="default_obs_stream" AND default_stream_channel="'+key+'"');
            var streamsetting = Object.values(JSON.parse(JSON.stringify(rows1)));
            var rows4 = await msconnection.query('UPDATE lobby_fans SET stream_status="disable" where stream_type=2 AND obs_stream_channel="'+key+'" AND user_id='+streamsetting[0][0]['user_id']);
            var valueToPush = { };
            valueToPush['key'] = key;
            valueToPush['is_creator'] = streamsetting[0][0]['user_id'];                                    
            valueToPush['on'] = "endStreamRemoveBanner";
            io.sockets.emit('getrealtimedata', {data:valueToPush} );
    }catch(e){
        console.log(e);
    }
}

const getfollowers = async function followers_detail(user_id){
    const followers = await msconnection.query('SELECT COUNT(fu.id) as followercount, GROUP_CONCAT(fu.followers_id SEPARATOR ",") as followers_arr FROM follower_users fu WHERE fu.following_id='+user_id);
    var fl = Object.values(JSON.parse(JSON.stringify(followers)));
    return fl[0][0];
}

const getsubscribedlist = async function subscribers_detail(user_id){
    const subscribers = await msconnection.query('SELECT ps.*, ud.*, psh.payment_id FROM paypal_subscriptions ps JOIN user_detail ud ON ud.user_id = ps.user_to LEFT OUTER JOIN paypal_subscriptions_history psh ON psh.payment_id = ps.transaction_id WHERE ps.user_from='+user_id+' AND ps.subscribe_status=1 AND ps.is_history=0');
    var subscribedlist = Object.values(JSON.parse(JSON.stringify(subscribers)));
    return subscribedlist[0];
}

const myfunction = async function updateStreamStatus(key){
    try{
        var lobby_ids = [];
        var stream_arr = [];
        var followcnt;
        var rows1 = await msconnection.query('SELECT * FROM stream_settings WHERE key_option="default_obs_stream" AND default_stream_channel="'+key+'"');
            var streamsetting = Object.values(JSON.parse(JSON.stringify(rows1)));
            console.log('user_id',streamsetting[0][0]['user_id']);
                var rows2 = await msconnection.query('SELECT * FROM stream_settings WHERE user_id="'+streamsetting[0][0]['user_id']+'"');
                var rows2_val = Object.values(JSON.parse(JSON.stringify(rows2)));
                // if (rows2_val[0][0]['default_value'] == 'off') {
                //     console.log("can't change status because its manual from stream setting tab")
                // } else {
                    // var rows2 = await msconnection.query('SELECT l.id as lobby_id, l.user_id as creator, l.custom_name as lname, gb.id as group_bet_id, ud.user_id as user_id, ud.image, ud.name, lg.table_cat FROM lobby l JOIN group_bet gb ON l.id = gb.lobby_id JOIN lobby_group lg ON gb.id=lg.group_bet_id JOIN user_detail ud ON ud.user_id=lg.user_id WHERE lg.user_id='+streamsetting[0][0]['user_id']+' AND is_archive=0 AND l.status=1 AND gb.bet_status=1');
                    var rows2 = await msconnection.query('SELECT l.id as lobby_id, l.user_id as creator, l.custom_name as lname, gb.id as group_bet_id, ud.user_id as user_id, ud.image, ud.name, lg.table_cat, lf.stream_type as stream_type, lf.fan_tag, lf.stream_status, lf.waiting_table FROM lobby l JOIN group_bet gb ON l.id = gb.lobby_id JOIN lobby_fans lf ON lf.lobby_id = l.id LEFT OUTER JOIN lobby_group lg ON gb.id = lg.group_bet_id AND lg.user_id = lf.user_id JOIN user_detail ud ON ud.user_id = lf.user_id WHERE lf.user_id = "'+streamsetting[0][0]['user_id']+'" AND l.is_archive = 0 AND lf.stream_type = 2 AND l.status = 1 AND gb.bet_status = 1 AND ((l.user_id = "'+streamsetting[0][0]['user_id']+'" AND (lg.table_cat IS NOT NULL OR lf.waiting_table IS NOT NULL)) OR (l.user_id != "'+streamsetting[0][0]['user_id']+'" AND lg.table_cat IS NOT NULL AND lf.waiting_table IS NULL))');
                    var ldata = Object.values(JSON.parse(JSON.stringify(rows2)));
                    var lobbybetdata = ldata[0];
                    for (const v of lobbybetdata) {
                        var valueToPush = { };
                        lobby_ids.push(v.lobby_id);
                        // var rows3 = await msconnection.query('SELECT * FROM lobby_fans WHERE user_id='+streamsetting[0][0]['user_id']+' AND lobby_id='+v.lobby_id);
                        // var lobbyfandata = Object.values(JSON.parse(JSON.stringify(rows3)));
                        var followersdata = await getfollowers(v.user_id);
                        var subscribersdata = await getsubscribedlist(v.user_id);
                        
                        valueToPush['fan_tag'] = v.fan_tag;
                        valueToPush['lobby_id'] = v.lname;
                        valueToPush['group_bet_id'] = v.group_bet_id;
                        valueToPush['is_creator'] = v.creator;                                    
                        valueToPush['on'] = "change_stream_status_data";                                    
                        valueToPush['stream_type'] = v.stream_type;                                    
                        valueToPush['streamer_channel'] = key; 
                        valueToPush['streamer_id'] = v.user_id; 
                        valueToPush['streamer_img'] = v.image; 
                        valueToPush['streamer_name'] = v.name; 
                        valueToPush['table_cat'] = v.table_cat; 
                        valueToPush['updated_data'] = "enable";                                    
                        valueToPush['follower_count'] = followersdata.followercount;
                        valueToPush['is_followd'] = 'follow';
                        valueToPush['is_subscribed'] = 'no';
                        valueToPush['subscribedlist'] = subscribersdata;
                        // var lobby_fan_data = await msconnection.query('SELECT * FROM lobby_fans WHERE user_id='+streamsetting[0][0]['user_id']+' AND lobby_id='+v.lobby_id);
                        console.log(v.stream_status+' == enable && '+v.stream_type+' == 2');
                        if(v.stream_status == 'enable' && v.stream_type == 2){
                            console.log('lfdata',v.stream_status);
                        } else {
                            console.log('lobbybetdata',v);
                            var rows4 = await msconnection.query('UPDATE lobby_fans SET stream_status="enable",stream_type=2,obs_stream_channel="'+key+'" WHERE user_id='+v.user_id+' AND lobby_id='+v.lobby_id);
                        }
                        io.sockets.emit('getrealtimedata', {data:valueToPush} );
                    // }
                }
    }catch(e){
        console.log(e);
    }
}
// Start connection
io.sockets.on( 'connection', function( client ) {


    console.log('new connection');


    /**
     * Store socket ID with user ID for the online users
     * 
     * @param  {Object} data
     * @return {Null}
     */
    client.on( 'socket id', function(data){
        try{
            console.log( "New User : " + data.emit_from  +" : "+ client.id);
            socketstorage[data.user] = client.id;
            users[data.user] = data.user;
            io.sockets.emit('update ol users', {users: users} );
        } catch(err){
            err = new Error('The data passed is not recognized');
            throw err;
        }

    });


    /**
     * Process disconnect when the user lost
     * 
     * @param  {Object} data
     * @return {Null}
     */
    client.on('disconnect', function(data) {
        try{
            var i = client.id;
            // var disconnected_user = socketstorage.getKeyByValue( i );
            var disconnected_user = 1;
            console.log('user disconnected: '+disconnected_user+' = '+i);
            delete users[disconnected_user];
            io.sockets.emit('disconnect user', {user: disconnected_user} );
        } catch(err){
            err = new Error('The user disconnected is not recognized');
            throw err;
        }
        
    });


    /**
     * Send message to the users involve in the conversation
     * 
     * @param  {Object} data
     * @return {Null}
     */
    client.on('send-message', function(data) {
        try{
            var emit_users = data.emit_to;
            var users_count = emit_users.length;

            for (var i = 0; i < users_count; i++) {
                var socketid = socketstorage[emit_users[i]];
                if(socketid!= undefined && data.emit_from != emit_users[i]){
                    console.log('Send message to user: '+emit_users[i]+', socket id: '+ socketid);
                    io.to(socketid).emit('recieved-message', {emit_from: data.emit_from, emit_to: emit_users[i], message_id: data.message_id, ci: data.ci} );
                }
                
            }
        } catch(err){
            err = new Error('An error occurred while emitting the data');
            throw err;
        }
    });

    client.on('send-message-to-pvp-player', function(data) {
        try{
            var emit_users = data.emit_to;
            var users_count = emit_users.length;
            var emit_user_name = data.emit_user_name;
            for (var i = 0; i < users_count; i++) {
                var socketid = socketstorage[emit_users[i]];
                if(socketid!= undefined && data.emit_from != emit_users[i]){
                    console.log('Send message to Pvp player : '+emit_users[i]+', socket id: '+ socketid);
                    io.to(socketid).emit('recieved-message', {emit_user_name:emit_user_name, emit_from: data.emit_from, emit_to: emit_users[i], message_id: data.message_id, ci: data.ci} );
                }
                
            }
        } catch(err){
            err = new Error('An error occurred while emitting the data');
            throw err;
        }
    });

    client.on('send-message-to-pvp-player-from-admin', function(data) {
        try{
            var emit_users = data.emit_to;
            var users_count = emit_users.length;
            var emit_user_name = data.emit_user_name;
            for (var i = 0; i < users_count; i++) {
                var socketid = socketstorage[emit_users[i]];
                if(socketid!= undefined && data.emit_from != emit_users[i]){
                    console.log('Send message to Pvp player From Admin : '+emit_users[i]+', socket id: '+ socketid);
                    io.to(socketid).emit('recieved-message', {emit_user_name:emit_user_name, emit_from: data.emit_from, emit_to: emit_users[i], message_id: data.message_id, ci: data.ci} );
                }
                
            }
        } catch(err){
            err = new Error('An error occurred while emitting the data');
            throw err;
        }
    });

    client.on('send-message-to-customer', function(data) {
        try{
            var emit_users = data.emit_to;
            var users_count = emit_users.length;
            var emit_user_name = data.emit_user_name;
            for (var i = 0; i < users_count; i++) {
                var socketid = socketstorage[emit_users[i]];
                if(socketid!= undefined && data.emit_from != emit_users[i]){
                    console.log('Send message to Customer : '+emit_users[i]+', socket id: '+ socketid);
                    io.to(socketid).emit('recieved-message', {emit_user_name:emit_user_name, emit_from: data.emit_from, emit_to: emit_users[i], message_id: data.message_id, ci: data.ci} );
                }
                
            }
        } catch(err){
            err = new Error('An error occurred while emitting the data');
            throw err;
        }
    });

    client.on('send-message-to-friend', function(data) {
        try{
            var emit_users = data.emit_to;
            var users_count = emit_users.length;
            var emit_user_name = data.emit_user_name;
            for (var i = 0; i < users_count; i++) {
                var socketid = socketstorage[emit_users[i]];
                if(socketid!= undefined && data.emit_from != emit_users[i]){
                    console.log('Send message to Friend : '+emit_users[i]+', socket id: '+ socketid);
                    io.to(socketid).emit('recieved-message', {emit_user_name:emit_user_name, emit_from: data.emit_from, emit_to: emit_users[i], message_id: data.message_id, ci: data.ci} );
                }
                
            }
        } catch(err){
            err = new Error('An error occurred while emitting the data');
            throw err;
        }
    });

    client.on('send-message-to-admin', function(data) {
        try{
            var emit_users = data.emit_to;
            var users_count = emit_users.length;
            var emit_user_name = data.emit_user_name;
            for (var i = 0; i < users_count; i++) {
                var socketid = socketstorage[emit_users[i]];
                if(socketid!= undefined && data.emit_from != emit_users[i]){
                    console.log('Send message to Admin : '+emit_users[i]+', socket id: '+ socketid);
                    io.to(socketid).emit('recieved-message', {emit_user_name:emit_user_name, emit_from: data.emit_from, emit_to: emit_users[i], message_id: data.message_id, ci: data.ci} );
                }
            }
        } catch(err){
            err = new Error('An error occurred while emitting the data');
            throw err;
        }
    });

    client.on('send_report_group_data', function(data) {
        try{
            var lobby_id = data.lobby_id;
            var group_bet_id = data.group_bet_id;
            var user_id = data.user_id;
            var table_cat = data.table_cat;
            var win_lose_status = data.win_lose_status;
            var ref_id = data.ref_id;
            for (var i = 0; i < 1; i++) {
                var socketid = user_id[i];
                if(socketid!= undefined){
                    console.log('report updated id : '+user_id[i]+', socket id: '+ socketid);
                    io.local.emit('get_report_updated-data', {lobby_id:lobby_id,group_bet_id:group_bet_id,table_cat:table_cat,user_id:user_id,win_lose_status:win_lose_status,ref_id:ref_id} );
                }
            }
        } catch(err){
            err = new Error('An error occurred while emitting the data');
            throw err;
        }
    });

    client.on('get_tip_data', function(data) {
        try{
            var from_total_balance, to_total_balance, user_from, user_to;
            from_total_balance = data.from_total_balance;
            to_total_balance = data.to_total_balance;
            user_from = data.user_from;
            user_to = data.user_to;
            for (var i = 0; i < 1; i++) {
                var socketid = user_to[i];
                if(socketid!= undefined){
                    io.local.emit('get_tip_data', {from_total_balance:from_total_balance,to_total_balance:to_total_balance,user_from:user_from,user_to:user_to} );
                }
            }
        } catch(err){
            err = new Error('An error occurred while emitting the data');
            throw err;
        }
    });

    client.on('change_lobby_size', function(data) {
        try{
            var lobby_id = data.lobby_id;
            for (var i = 0; i < 1; i++) {
                var socketid = lobby_id[i];
                if(socketid!= undefined){
                    io.local.emit('get_changed_lobby_size', {lobby_id:lobby_id, group_bet_id:data.group_bet_id, refresh:data.refresh } );
                }
            }
        } catch(err){
            err = new Error('An error occurred while emitting the data');
            throw err;
        }
    });
    
    client.on('sendrealtimedata', function(data) {
        try{
            for (var i = 0; i < 1; i++) {
                var socketid = 1;
                if(socketid!= undefined){
                    io.local.emit('getrealtimedata', {data:data} );
                }
            }
        } catch(err){
            err = new Error('An error occurred while emitting the data');
            throw err;
        }
    });

    /**
     * Group conversation option changes
     * It will emit to the users involve in the group conversation
     * 
     * @param  {Object} data
     * @return {Null}
     */
    client.on('update-group-conversation', function(data) {
        try{
            var emit_users = data.emit_to;
            var users_count = emit_users.length;

            for (var i = 0; i < users_count; i++) {
                var socketid = socketstorage[emit_users[i].user_id];
                if(socketid!= undefined && data.emit_from != emit_users[i].user_id){
                    console.log('Update Group Conversation: '+emit_users[i].user_id+', socket id: '+ socketid);
                    io.to(socketid).emit('update-group-conversation', {emit_from: data.emit_from, emit_to: emit_users[i].user_id, cn_id: data.cn_id} );
                }
                
            }

        } catch(err){
            err = new Error('An error occurred while emitting the data');
            throw err;
        }
    });

});


process.on('uncaughtException', function (err) {
  console.error((new Date).toUTCString() + ' uncaughtException:', err.message);
  console.error(err.stack);
  //process.exit(1);
});


server.listen( 10000 );

