<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Gameimage {
	### function to get image name ###
	public function getImageList($game_id="") {
		$ci =& get_instance();
		$ci->load->database();
		
		$condition = "game_id=".$game_id;
		$ci->db->where($condition);
		$query = $ci->db->get('game_image');
		$result = $query->result();
		return $result[0]->game_image;
	}
	### function to get winner id ###
	public function getWinner($bet_id="") {
		$ci =& get_instance();
		$ci->load->database();
		
		$condition = "id=".$bet_id;
		$ci->db->where($condition);
		$query = $ci->db->get('bet');
		$result = $query->result();
		return $result[0]->winner_id;
	}
	### function to get loser id ###
	public function getLoser($bet_id="") {
		$ci =& get_instance();
		$ci->load->database();
		
		$condition = "id=".$bet_id;
		$ci->db->where($condition);
		$query = $ci->db->get('bet');
		$result = $query->result();
		return $result[0]->loser_id;
	}
}