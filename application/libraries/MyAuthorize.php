<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of MyAuthorize
 *
 * @author wahyu widodo
 */
 
include("./vendor/autoload.php"); 
 
class MyAuthorize {
	
	public $merchanAuthentication;
	public $refId;

	public function __construct(){	

		
		$this->merchanAuthentication = new net\authorize\api\contract\v1\MerchantAuthenticationType();

		$host_split = explode('.',$_SERVER['HTTP_HOST']);
		if($host_split[0] == 'localhost' || $host_split[0] == 'esportstesting')
		{	 
			//test credentials
			 $this->merchanAuthentication->setName("76Lr36zXG6jW");
			 $this->merchanAuthentication->setTransactionKey("9E5z3d3Br53TFEn6");
			
		}
		else
		{
			//live credentirals
			 $this->merchanAuthentication->setName("2Qx5q4PQ942");
			 $this->merchanAuthentication->setTransactionKey("86k2dUA52Wt7Tb7u");
		}
		
		$this->refId = 'ref'.time();
	}

	public function chargerCreditCard($detCus){	

		$creditCard = new net\authorize\api\contract\v1\CreditCardType();
		$creditCard->setCardNumber($detCus['cnumber']);
		$creditCard->setExpirationDate($detCus['cexpdate']);					 	
		$creditCard->setCardCode($detCus['ccode']);

		$paymentOne = new net\authorize\api\contract\v1\PaymentType();

		$paymentOne->setCreditCard($creditCard);

		$order = new net\authorize\api\contract\v1\OrderType();
		$order->setDescription($detCus['cdesc']);


		// Preparin customer information object
		$billto = new net\authorize\api\contract\v1\CustomerAddressType();
		$billto->setFirstName($detCus['fname']);
		$billto->setLastName($detCus['lname']);
		$billto->setAddress($detCus['address']);
		$billto->setCity($detCus['city']);
		$billto->setState($detCus['state']);
		$billto->setCountry($detCus['country']);
		$billto->setZip($detCus['zip']);
		$billto->setPhoneNumber($detCus['phone']);
		$billto->setEmail($detCus['email']);



		// create transaction 
		$transactionRequestType = new net\authorize\api\contract\v1\TransactionRequestType();
		$transactionRequestType->setTransactionType("authCaptureTransaction");
		$transactionRequestType->setAmount($detCus['amount']); 
		$transactionRequestType->setOrder($order);
		$transactionRequestType->setPayment($paymentOne);
		$transactionRequestType->setBillTo($billto);



		$request = new net\authorize\api\contract\v1\CreateTransactionRequest();
		$request->setMerchantAuthentication($this->merchanAuthentication);

		$request->setRefId($this->refId); 

		$request->setTransactionRequest($transactionRequestType);

		$controllerx = new net\authorize\api\controller\CreateTransactionController($request);

		$host_split = explode('.',$_SERVER['HTTP_HOST']);
		if($host_split[0] == 'localhost' || $host_split[0] == 'esportstesting')
		{
			//for sandbox
			$response = $controllerx->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
		}
		else
		{	
			//for live
			$response = $controllerx->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);
		}

// echo"<pre>";
// 		print_r($response);
// 		exit();


		if ($response != null){
		    $tresponse = $response->getTransactionResponse();
			// return $tresponse;
		 	 $this->authorize_fallback_data(json_encode($tresponse), 'Payment Responce');
		    if (($tresponse != null) && ($tresponse->getResponseCode()=="1") ) {
		    	return $transaction_id=$tresponse->getTransId();
		    	 
		    	 // return redirect('/authorizesecond/pushPayment');
		      // echo "Charge Credit Card AUTH CODE : " . $tresponse->getAuthCode() . "\n";
		      // echo "Charge Credit Card TRANS ID  : " . $tresponse->getTransId() . "\n";

		    }else{
		    	 redirect('/authorizesecond/failure','refresh');
		         // echo  "Charge Credit Card ERROR :  Invalid response\n";
		    }

		} else{
		      // echo  "Charge Credit card Null response returned";
		    redirect('/authorizesecond/failure','refresh');

		}
	}
	   //authorize data
    public function authorize_fallback_data($value, $call) {
    	// $this->load->library('user_agent');
    	// echo "<pre>";
    	// print_r($call);
    	// exit();
    	$path = BASEPATH.'../card_data/';
    	$today_date = date('m-d-Y', time());
		$date1 = str_replace('-', '/', $today_date);
		$tomorrow = date('m-d-Y',strtotime($date1 . "-1 days"));
		
		$tomorrow_file = $path.$tomorrow.'_card.php';
    	$today_file = $path.$today_date.'_card.php';
		
		// if (file_exists($tomorrow_file)) {
  //   		unlink($tomorrow_file);
		// }

    	if (file_exists($today_file)) {
    		// write_file($today_file, "\n\nUsers Browser\n\n".$this->agent->browser()."\n\n", 'a');
    		write_file($today_file, "\n\n".$call, 'a');
    		write_file($today_file, $value, 'a');
    		write_file($today_file, "\n\n", 'a');
    	} else {
    		// write_file($today_file, "\n\nUsers Browser\n\n".$this->agent->browser()."\n\n", 'a');
    		write_file($today_file, $call, 'a');
			write_file($today_file, $value, 'a');
			write_file($today_file, "\n\n", 'a');
    	}
    }
}