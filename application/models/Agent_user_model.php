<?php
class Agent_user_model extends CI_Model
{
    public function __construct()
    {
	      parent::__construct();
    }

     function agent_authenticate($agent_id, $agent_password)
    {
    
        $sql = "SELECT * FROM cust_service_agent_cre WHERE
                id = '".$agent_id."' AND password = '".$agent_password."' ";
        $query= $this->db->query($sql);
        $result_arr= $query->result_array();
        if( isset($result_arr[0]) )
            return $result_arr[0];
        else
            return false;
    }
      function log_this_login($data_arr)
    {
        $sql= "SELECT * FROM cust_service_agent_cre WHERE id= '{$data_arr['id']}'";
        $query=$this->db->query($sql);
        $row = $query->row();
        $id = $row->id;
        $password = $row->password;
        $this->db->update('cust_service_agent_cre', array('last_login'=>date('Y-m-d H:i:s'),'logged_in'=>'1'),array('id'=>$data_arr['id']));

         $this->db->insert('agent_login_attempts', array('login_time'=>date('Y-m-d H:i:s'),'agent_id'=>$id,'is_online'=>'1'));
        $_SESSION["timeout"] = time()+ (24 * 60 * 60 * 5);
        $_SESSION['admin_user_agent_id'] = $id;
        $_SESSION['admin_user_agent_password'] = $password;
    }
    //logout
   function logout_this_login()
    {
        $this->db->update('cust_service_agent_cre', array('last_logout'=>date('Y-m-d H:i:s'),'logged_in'=>'0'),array('id'=>$this->session->userdata('admin_user_agent_id')));

        $this->db->select('a.*');
        $this->db->from('agent_login_attempts a');
        $this->db->where('a.agent_id', $this->session->userdata('admin_user_agent_id'));
        $this->db->where('a.is_online', '1');
        $query_row = $this->db->get();
        $query_result = $query_row->result();
        $login_time = $query_result[0]->login_time;
        $total_ol_time = strtotime(date('Y-m-d H:i:s')) - strtotime($login_time);

        $days = floor($total_ol_time / 86400 % 7);
        $hours = floor($total_ol_time / 3600 % 24);
        $mins = floor($total_ol_time/ 60 % 60);
        $secs = floor($total_ol_time % 60);
        if($days == '00'){

          if ($hours == '00') {

              if ($mins == '00') {
                   $total_time = sprintf('%02d sec', $secs);
                   }
              else 
                  {
                    $total_time = sprintf('%02d mins %02d sec', $mins, $secs);
                  }
                }
              else
                 {
                     $total_time = sprintf('%02d hrs %02d mins %02d sec', $hours, $mins, $secs);
                 }
             }
           else
             {
                 $total_time = sprintf('%02d days %02d hrs %02d mins %02d sec', $days, $hours, $mins, $secs);
             }
        
        $this->db->update('agent_login_attempts', array('logout_time'=>date('Y-m-d H:i:s'),'is_online'=>'0','total_time'=>$total_time),array('agent_id'=>$this->session->userdata('admin_user_agent_id'),'is_online'=>'1'));
        unset($_SESSION["timeout"]);
        unset($_SESSION['admin_user_agent_id']);
        unset($_SESSION['admin_user_agent_password']);
    }

  }
?>
