<?php
class Cart_model extends CI_Model {
	public function __construct() {
		parent::__construct();
    $this->load->model('Paypal_payment_model');
	}
	// My Cart Item
  function cart_item() {
      $cart_item = $this->General_model->view_data('products_cart', array(
          'user_id' => $_SESSION['user_id']
      ));
      return $cart_item;
  }
  function get_attribute_img($attr='') {
      $this->db->select('pat.product_id, pat.attribute_id, pat.option_id, pat.option_image');
      $this->db->from('product_attributes_tbl pat');
      (isset($attr['attribute_id']) && $attr['attribute_id'] != '') ? $this->db->where('pat.attribute_id',$attr['attribute_id']) : '';
      (isset($attr['option_id']) && $attr['option_id'] != '') ? $this->db->where('pat.option_id',$attr['option_id']) : '';
      (isset($attr['product_id']) && $attr['product_id'] != '') ? $this->db->where('pat.product_id',$attr['product_id']) : '';
      $res = (isset($attr['fetch_data']) && $attr['fetch_data'] == 'result') ? $this->db->get()->result(): $this->db->get()->row();
      return $res;
  }
  function calc_shipping_mdl($arr) {
    $this->db->select('ss.*');
    $this->db->from('site_settings ss');
    $this->db->where('ss.option_title','inside_us_product_fees');
    $this->db->or_where('ss.option_title','outside_us_product_fees');
    $shipping_fee = $this->db->get()->result_array();
    return $shipping_fee;
  }
  function cart_items($user_id='') {
   $user_id=(!empty($user_id)) ? $user_id : $this->session->userdata('user_id');
    if ($user_id !='') {
      $this->db->select('ci.*, c.user_id, pt.id as product_exist_id, pt.seller_id, ud.local_product_fee, ud.international_product_fee, ud.display_name, ud.name as username, ud.country, cn.sortname, cn.countryname, ud.display_name, ud.account_no');
      $this->db->from('cart_item ci');
      $this->db->join('cart c', 'c.id = ci.cart_id');
      $this->db->join('products_tbl pt', 'pt.id = ci.product_id', 'left outer');
      $this->db->join('user_detail ud', 'ud.user_id = pt.seller_id', 'left outer');
      $this->db->join('country cn', 'cn.country_id = ud.country', 'left outer');
      $this->db->where('c.user_id',$user_id);
      $this->db->order_by('ci.created_at');
      $getitems = $this->db->get()->result_array();
    } else {
      $getitems = $this->session->userdata('cart_item_list');
      $cart_item_list = [];
      if (!empty($getitems)) {
        foreach ($getitems as $key => $gi) {
          $getproductexist = $this->General_model->view_single_row('products_tbl', array('id' => $gi['product_id']),'*');
          if (!empty($getproductexist)) {
            $gi['product_exist_id'] = $getproductexist['id'];
            $gi['seller_id'] = $getproductexist['seller_id'];
            $cart_item_list[] = $gi;
          }
        }
      }
      $getitems = $_SESSION['cart_item_list'] = $cart_item_list;
    }
    return $getitems;
  }
  // add to cart common function
  function add_item_cart($arr) {
    $user_id=(!empty($arr['user_id'])) ? $arr['user_id'] : $this->session->userdata('user_id');
    
    $data['product_id'] = $arr['product_id'];
    if ($arr['action'] == 'from_wishlist') {
    } else {
      $arr['sel_variation'] = array_flip($arr['sel_variation']);
      ksort($arr['sel_variation']);
      $arr['sel_variation'] = array_flip($arr['sel_variation']);
      $arr['sel_variation'] = implode(', ', $arr['sel_variation']);

      $arr['sel_variation_amount'] = array_flip($arr['sel_variation_amount']);
      ksort($arr['sel_variation_amount']);
      $arr['sel_variation_amount'] = array_flip($arr['sel_variation_amount']);
      $arr['sel_variation_amount'] = implode(', ', $arr['sel_variation_amount']);
    }
    
    $arr1 = array( 'product_id' => $data['product_id'], 'get' => 'row');
    $product_detail = $this->Product_model->productDetail($arr1);

    $arr['amount'] = number_format((float) $arr['amount'], 2, '.', '');
    $arr['product_fee'] = number_format((float) $arr['product_fee'], 2, '.', '');
     
     $cart_item_list = $this->cart_items($user_id);
 
    if (empty($cart_item_list) || in_array($product_detail->seller_id, array_column($cart_item_list,'seller_id'))) {

      $pq = 0; $product_already_exist = $data['errmsg'] = '';
      foreach ($cart_item_list as $key => $cil) {
        if ($cil['product_id'] == $data['product_id']) {
          $pq += $cil['qty'];

        }
        if ($cil['product_id'] == $data['product_id'] && $cil['sel_variation'] == $arr['sel_variation']) {
          $product_already_exist = ($arr['action'] == 'from_wishlist') ? ($cil['qty'] + $arr['qty']) : $key.'_'.($cil['qty'] + $arr['qty']);
        }
      } 
      if ($pq != 0 && ($pq + $arr['qty']) > $product_detail->qty) {
        $data['errmsg'] = ($product_detail->qty - $pq != 0) ? 'You can add '.($product_detail->qty - $pq).' quantity of this Product' : 'You have exceeded to add maximum quantity of this Product';
        return $data;
        exit();
      }
      if ($user_id != '') {
          $cart_id = $this->db->get_where('cart',array('user_id' => $user_id))->row()->id;
          if (!$cart_id) {
              $cart_id = $this->General_model->insert_data('cart',array('user_id' => $user_id));
          }
        
          if ($cart_id) {
              if ($product_already_exist != '') {
                if ($arr['action'] == 'from_wishlist') {
                  $qty = $product_already_exist;
                } else {
                  $already_exist = explode('_', $product_already_exist);
                  $key = $already_exist[0];
                  $qty = $already_exist[1];
                }
                $update_data['qty'] = $qty;
                $this->General_model->update_data('cart_item', $update_data, array('sel_variation' => $arr['sel_variation'],'cart_id'=>$cart_id, 'sel_variation_amount' => $arr['sel_variation_amount'],'product_id' => $arr['product_id']));
                $data['added'] = true;
              } else {
                  $insert_arr = array(
                    'cart_id' => $cart_id,
                    'product_id' => $arr['product_id'],
                    'product_image' => $arr['product_image'],
                    'product_name' => $arr['product_name'],
                    'qty' => $arr['qty'],
                    'amount' => $arr['amount'],
                    'product_fee' => $arr['product_fee'],
                    'sel_variation' => $arr['sel_variation'],
                    'sel_variation_amount' => $arr['sel_variation_amount'],
                    'file_name' => $product_detail->game_name,
                    'ref_seller_id' => $arr['ref_seller_id'],
                  );
                  $this->General_model->insert_data('cart_item',$insert_arr);
                  $data['added'] = true;
              }
          }
      } else {
          if ($product_already_exist) {
              $already_exist = explode('_', $product_already_exist);
              $key = $already_exist[0];
              $qty = $already_exist[1];
              $_SESSION['cart_item_list'][$key]['qty'] = $qty;
          } else {
              $sessionarr = array(
                  'qty' => $arr['qty'],
                  'product_image' => $arr['product_image'],
                  'product_id' => $arr['product_id'],
                  'amount' => $arr['amount'],
                  'product_fee' => $arr['product_fee'],
                  'product_name' => $arr['product_name'],
                  'sel_variation' => $arr['sel_variation'],
                  'sel_variation_amount' => $arr['sel_variation_amount'],
                  'file_name' => $product_detail->game_name,
              );
              $_SESSION['cart_item_list'][] = $sessionarr;
          }
          $data['added'] = true;
      }
    } else {
       $data['errmsg'] = 'You can order this item after checkout your current cart <br> OR <br> You can clear your current cart to buy this item. <br><br> <a href="'.base_url().'cart" class="cmn_btn">Go to cart</a> <br><br>';
    }
    return $data;
  }
  // Whishlist
  function add_item_wishlist($arr) {
    
    $user_id=(!empty($arr['user_id'])) ? $arr['user_id'] : $this->session->userdata('user_id');
   
    $data['product_id'] = $arr['product_id'];
    $arr['sel_variation'] = array_flip($arr['sel_variation']);
    ksort($arr['sel_variation']);
    $arr['sel_variation'] = array_flip($arr['sel_variation']);
    $arr['sel_variation'] = implode(', ', $arr['sel_variation']);

    $arr['sel_variation_amount'] = array_flip($arr['sel_variation_amount']);
    ksort($arr['sel_variation_amount']);
    $arr['sel_variation_amount'] = array_flip($arr['sel_variation_amount']);
    $arr['sel_variation_amount'] = implode(', ', $arr['sel_variation_amount']);

    $arr1 = array('product_id' => $data['product_id'], 'get' => 'row');
   
    $product_detail = $this->Product_model->productDetail($arr1);

    $arr['amount'] = number_format((float) $arr['amount'], 2, '.', '');
    $arr['product_fee'] = number_format((float) $arr['product_fee'], 2, '.', '');        
    $wishlist_id = $this->db->get_where('wishlist',array('user_id' => $user_id))->row()->id;

    $wishlist_id = ($wishlist_id == '') ? $this->General_model->insert_data('wishlist',array('user_id' => $user_id)) : $wishlist_id;

    if ($wishlist_id) {

      $item_isexist = $this->db->get_where('wishlist_item',array('wishlist_id' => $wishlist_id,'product_id' => $arr['product_id'], 'sel_variation' => $arr['sel_variation']))->row();

      if(empty($item_isexist)) {
        
        $arr['wishlist_id'] = $wishlist_id;
        $arr['file_name'] = $product_detail->game_name;
        unset($arr['action']);
     
         
        $wishlist_add = $this->General_model->insert_data('wishlist_item',$arr);

      } else {
        $arr['qty'] = $arr['qty'] + $item_isexist->qty;
        $this->General_model->update_data('wishlist_item', array('qty' => $arr['qty']), array('wishlist_id' => $wishlist_id,'product_id' => $arr['product_id'], 'sel_variation' => $arr['sel_variation'], 'sel_variation_amount' => $arr['sel_variation_amount']));
          $wishlist_add = true;
      }
      return $wishlist_add;
      exit();
    }
  }
  function wishlist_items() {
    $this->db->select('wi.*,w.user_id');
    $this->db->from('wishlist_item wi');
    $this->db->join('wishlist w', 'w.id = wi.wishlist_id');
    $this->db->where('w.user_id',$this->session->userdata('user_id'));
    $this->db->order_by('wi.created_at');
    $sql = $this->db->get();
    $getitems = $sql->result_array();
    return $getitems;
  }
  // api wishlist 
   function get_wishlist_items($user_id) {
    $this->db->select('wi.*,w.user_id');
    $this->db->from('wishlist_item wi');
    $this->db->join('wishlist w', 'w.id = wi.wishlist_id');
    $this->db->where('w.user_id',$user_id);
    $this->db->order_by('wi.created_at');
    $sql = $this->db->get();
    $getitems = $sql->result_array();
    return $getitems;
  }
  function cancelorder($arr) {
    
    if (count($arr) > 0) {
      $this->load->model('Paypal_payment_model');
      $view_order = $this->General_model->view_single_row('orders_tbl', array('id' => $arr['order_id']),'*');
      $data['updatedetail'] = '';
      $data['message_succ'] = 'Order canceled successfully <br>';
      if ($view_order['paypal_deducted'] > 0) {
          $arrdata['payerid'] = $view_order['paypal_payer_id'];
          $arrdata['refundtype'] = 'Full';
          $arrdata['transactionid'] = $view_order['paypal_transaction_id'];
          $arrdata['amt'] = $view_order['paypal_deducted'];
          $refund_transaction = $this->Paypal_payment_model->refund_transaction($arrdata);
          if (strtolower($refund_transaction['data']['ACK']) == 'success') {
              $data['updatedetail'] = true;
              $data['message_succ'] .= '$ '.number_format((float)$view_order['paypal_deducted'], 2, '.', '').' amount will be credited in seven business days. <br>';
          }
      }
      $wallet_deducted = $view_order['wallet_deducted'];
      $wallet_deducted += $view_order['card_deducted'];
      if ($wallet_deducted > 0) {
          $get_user_detail = $this->User_model->get_user_detail($view_order['user_id']);
          $update_balance['total_balance'] = number_format((float) $get_user_detail->total_balance + $wallet_deducted, 2, '.', '');
          $updated_data = $this->General_model->update_data('user_detail', $update_balance, array('user_id' => $view_order['user_id']));
          if ($updated_data) {
              $data['updatedetail'] = true;
              $data['message_succ'] .= '$ '.number_format((float)$wallet_deducted, 2, '.', '').' amount have been credited in your wallet.';
          }
      }
      if ($data['updatedetail'] == true) {
        $data['order_status'] = $updatedt['order_status'] = 'canceled';
        $updatedt['updated_at'] = date('Y-m-d H:i:s');
        $ordr_cancel = $this->General_model->update_data('orders_tbl', $updatedt, array('user_id' => $view_order['user_id'],'id' => $arr['order_id']));
        return $data;
      }
    }
  }
}