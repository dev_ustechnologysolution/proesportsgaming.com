<?php
class Email_model extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->model('Product_model');
		$this->load->model('Order_model');
        $this->store_admin_mail = (in_array($_SERVER['REMOTE_ADDR'], array( '127.0.0.1', '::1'))) ? 'vaghelamehul250@gmail.com' : 'Eric@ProFantasyGaming.com';
	}

    

	function placed_order_mail($arr) {
		$maildataar = array(
			'email_title' => $arr['email_title'],
			'umessage' => $arr['umessage'],
			'email_address' => $arr['email_address'],
			'name' => $arr['name'],
			'order_info' => $arr['order_info'],
			'items_arr' => $arr['items_arr'],
			'wbsite_transaction_id' => $arr['wbsite_transaction_id'],
			'subject' => $arr['subject'],
			'order_created_date' => $arr['order_created_date'],
			'action' => 'cancel',
    	);
    	$sndmail = $this->email_template_send($maildataar);
    }
    function order_action_mail($arr){
    	$order_id = $arr['order_id'];
    	$getar = array(
    		'id' => $order_id,
    		'get_items' => 'yes'
    	);
    	$get_myorder = $this->Order_model->getorderitems($getar);
    	$order_info = array(
			'sub_total' => $get_myorder[0]['sub_total'],
			'shipping_charge' => $get_myorder[0]['shipping_charge'],
			'grand_total' => $get_myorder[0]['grand_total'],
			'email' => $get_myorder[0]['email'],
			'phone' => $get_myorder[0]['phone'],
			'shipping_address' => $get_myorder[0]['shipping_address'],
			'shipping_country' => $get_myorder[0]['shipping_country'],
			'shipping_state' => $get_myorder[0]['shipping_state'],
			'shipping_city' => $get_myorder[0]['shipping_city'],
			'shipping_zip_code' => $get_myorder[0]['shipping_zip_code'],
			'billing_address' => $get_myorder[0]['billing_address'],
			'billing_country' => $get_myorder[0]['billing_country'],
			'billing_state' => $get_myorder[0]['billing_state'],
			'billing_city' => $get_myorder[0]['billing_city'],
			'billing_zip_code' => $get_myorder[0]['billing_zip_code'],
    	); 
    	$maildataar = array(
			'email_title' => 'Your order has been '.$arr['mail_action'],
			'umessage' => 'Your order has been successfully '.$arr['mail_action'].', Order details is shown below:',
			'email_address' => $get_myorder[0]['email'],
			'name' => $get_myorder[0]['name'],
			'order_info' => $order_info,
			'items_arr' => $get_myorder,
			'wbsite_transaction_id' => $get_myorder[0]['transaction_id'],
			'subject' => 'Order '.$arr['mail_action'],
			'order_created_date' => $get_myorder[0]['created_at'],
			'order_updated_date' => $get_myorder[0]['updated_at'],
			'action' => $arr['mail_action'],
    	);
    	$sndmail = $this->email_template_send($maildataar);
    	$maildataar['email_title'] = 'You have '.$arr['mail_action'].' order';
        $maildataar['umessage'] = 'You have '.$arr['mail_action'].' an order for '.$get_myorder[0]['name'].'. The order is as follows:';
        $maildataar['subject'] = 'Order '.$arr['mail_action'];
        $maildataar['email_address'] = 'storeadmin';
    	$sndmail = $this->email_template_send($maildataar);
    }

    function email_template_send($arr) {
    	$email_title = $arr['email_title'];
        $umessage = $arr['umessage'];
        $email = ($arr['email_address'] == 'storeadmin') ? $this->store_admin_mail : $arr['email_address'];
        $name = $arr['name'];
        $order_info = $arr['order_info'];
        $items_arr = $arr['items_arr'];
        $order_no = $arr['wbsite_transaction_id'];
        $subject = $arr['subject'];

        $order_placed_date = ($arr['order_created_date'] != '') ? '<h4 style="text-align: left; margin: 10px 0;">Order placed date: '.date('m/d/Y H:i:s A', strtotime($arr['order_created_date'])).'</h4>' : '';
        if ($arr['action'] != '') {
        	$order_aciton_date = ($arr['order_updated_date'] != '') ? '<h4 style="text-align: left; margin: 10px 0;">Order '.$arr['action'].' date: '.date('m/d/Y H:i:s A', strtotime($arr['order_updated_date'])).'</h4>' : '';
        } 
        $body ='<html>
            <body>
                <div style="width: 100%; text-align: center;">
                    <div style="width: 500px; border: 1px solid #ff5000; display: inline-block; padding: 0 15px;">
                        <div style="background-color: #ff5000; color: #fff;">
                            <h2 style="text-align: center;">'.$email_title.'</h2>
                        </div>
                        <div>
                            <p style="text-align: left;">'.$umessage.'</p>
                        </div>
                        <div>
                            <h2 style="text-align: left;">Order:#'.$order_no.'</h2>'.$order_placed_date.$order_aciton_date.'
                        </div>
                        <div>
                            <table cellpadding="0" cellspacing="0" border="1" width="600" style="width: 100%;">
                                <tr>
                                    <th style="width: 50%; text-align: left; padding: 10px;">Item</th>
                                    <th style="width: 10%; text-align: left; padding: 10px;">Qty</th>
                                    <th style="width: 10%; text-align: left; padding: 10px;"> Price</th>
                                    <th style="width: 10%; text-align: left; padding: 10px;"> Fee</th>
                                    <th style="width: 20%; text-align: left; padding: 10px;"> Total</th>
                                </tr>';
                                if (!empty($items_arr)) {
                                    foreach ($items_arr as $key => $ia) {
                                        $sel_variation = !empty($ia['product_attributes']) ? explode(',', $ia['product_attributes']) : '';
                                        if (isset($sel_variation) && $sel_variation != '') {
                                            $selvar = '';
                                            foreach ($sel_variation as $k => $sv) {
                                                $att = explode('_', $sv);
                                                $selvar .= '<h6 style="margin: 5px 0px;">
                                                        <span class="var_title">'.ucwords($att[0]).'</span> : <span> '.$att[1].'</span>
                                                    </h6>';
                                            }
                                        }
                                        $amt_total = number_format((float)($ia['amount']+$ia['product_fee']) * $ia['qty'], 2, '.', '');
                                        $body .='<tr>
                                            <td style="padding: 10px; text-align: left;"><div>'.$ia['product_name'].'</div>'.$selvar.'</td>
                                            <td style="padding: 10px; text-align: right;">'.$ia['qty'].'</td>
                                            <td style="padding: 10px; text-align: right;">$'.$ia['amount'].'</td>
                                            <td style="padding: 10px; text-align: right;">$'.$ia['product_fee'].'</td>
                                            <td style="padding: 10px; text-align: right;">$'.$amt_total.'</td>
                                        </tr>';
                                    }
                                }
                                $body .= '
                                <tr>
                                    <th colspan="4" style="padding: 10px; text-align: left;">Total</th>
                                    <td style="padding: 10px; text-align: right;"><b>$'.number_format((float)$order_info['sub_total'], 2, '.', '').'</b></td>
                                </tr>
                                <tr>
                                    <th colspan="4" style="padding: 10px; text-align: left;">Shipping</th>
                                    <td style="padding: 10px; text-align: right;"><b>$'.number_format((float)$order_info['shipping_charge'], 2, '.', '').'</b></td>
                                </tr>
                                <tr>
                                    <th colspan="4" style="padding: 10px; text-align: left;">Grand Total</th>
                                    <td style="padding: 10px; text-align: right;"><b>$'.number_format((float)$order_info['grand_total'], 2, '.', '').'</b></td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <h2 style="text-align: left;">Customer details</h2>
                            <h4 style="text-align: left;"><b>Email : </b> '.$order_info['email'].'</h4>
                            <h4 style="text-align: left;"><b>Phone : </b> '.$order_info['phone'].'</h4>
                        </div>
                        <div style="width: 50%; float: left;">
                            <h2 style="text-align: left;">Shipping address</h2>
                            <h4 style="text-align: left; font-weight: 500;"><b> Address: </b>'.$order_info['shipping_address'].'<br> <b> Country: </b>'.$order_info['shipping_country'].'<br> <b> State: </b>'.$order_info['shipping_state'].'<br> <b> City: </b>'.$order_info['shipping_city'].'<br> <b> Zip code: </b>'.$order_info['shipping_zip_code'].'</h4>
                        </div>
                        <div style="width: 50%; float: left;">
                            <h2 style="text-align: left;">Billing address</h2>
                            <h4 style="text-align: left; font-weight: 500;"><b> Address: </b>'.$order_info['billing_address'].'<br><b> Country: </b>'.$order_info['billing_country'].'<br><b> State: </b>'.$order_info['billing_state'].'<br><b> City: </b>'.$order_info['billing_city'].'<br> <b> Zip code: </b>'.$order_info['billing_zip_code'].'</h4>
                        </div>
                        <div style="width: 100%; background-color: #ff5000; color: #fff; display: inline-block;">
                            <h4 style="text-align: center;"><a href="'.base_url().'" style="color: #fff; text-decoration: none;">'.base_url().'</a></h4>
                        </div>
                    </div>
                </div>
            </body>
        </html>';
        $mail = $this->General_model->sendMail($email,$name,$subject,$body);
        return $mail;
    }
}
