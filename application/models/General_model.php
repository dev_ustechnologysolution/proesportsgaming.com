<?php
class General_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->model('User_model');
		$this->load->helper('url');
		$this->load->library('user_agent');
		$config = array(
            // 'Sandbox' => 'true',            // Sandbox / testing mode option.
            'APIUsername' => $this->paypal_lib->apiusername,    // PayPal API username of the API caller
            'APIPassword' => $this->paypal_lib->apipassword,    // PayPal API password of the API caller
            'APISignature' => $this->paypal_lib->apisignature,  // PayPal API signature of the API caller
            'APISubject' => '',                                     // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
            'APIVersion' => $this->paypal_lib->apiversion       // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
        );
        $this->load->library('paypal_pro', $config);
	}
	//insert data
	public function insert_data($table_name,$data){
		$this->db->insert($table_name,$data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}


	public function insert_multi_data($data_insert_cre,$data_insert_detail) {
		$this->db->insert('cust_service_agent_cre',$data_insert_cre);
		$this->db->insert('cre_admin_user',$data_insert_cre);
		$data_insert_detail['user_id'] = $this->db->insert_id();
		$this->db->insert('cust_service_agent_detail',$data_insert_detail);
		$this->db->insert('cre_admin_detail',$data_insert_detail);
	}


	//update data
	public function update_data($table_name,$data,$condition){
		return $this->db->update($table_name, $data,$condition);
	}

	//delete data
	public function delete_data($table_name,$condition){
		return $this->db->delete($table_name,$condition);
	}

	//view all data
	public function view_all_data($table_name,$column,$order){
		$this->db->order_by($column,$order);
		$query = $this->db->get($table_name);
		return $query->result_array();
	}
	
	public function commonGet($options) {
    $select = false;
    $table = false;
    $join = false;
    $order = false;
    $limit = false;
    $offset = false;
    $where = false;
    $or_where = false;
    $single = false;
    $where_not_in = false;
    $like = false;

    extract($options);	
    if ($select != false)
        $this->db->select($select);

    if ($table != false)
        $this->db->from($table);

    if ($where != false)
        $this->db->where($where);

    if ($where_not_in != false) {
        foreach ($where_not_in as $key => $value) {
            if (count($value) > 0)
                $this->db->where_not_in($key, $value);
        }
    }

    if ($like != false) {
        $this->db->like($like);
    }

    if ($or_where != false)
        $this->db->or_where($or_where);

    if ($limit != false) {
        if (!is_array($limit)) {
            $this->db->limit($limit);
        } else {
            foreach ($limit as $limitval => $offset) {
                $this->db->limit($limitval, $offset);
            }
        }
    }
    if ($order != false) {
        foreach ($order as $key => $value) {
            if (is_array($value)) {
                foreach ($order as $orderby => $orderval) {
                    $this->db->order_by($orderby, $orderval);
                }
            } else {
                $this->db->order_by($key, $value);
            }
        }
    }
    if ($join != false) {
        foreach ($join as $key => $value) {
            if (is_array($value)) {
                if (count($value) == 3) {
                    $this->db->join($value[0], $value[1], $value[2]);
                } else {
                    foreach ($value as $key1 => $value1) {
                        $this->db->join($key1, $value1);
                    }
                }
            } else {
                $this->db->join($key, $value);
            }
        }
    }
    $query = $this->db->get();
    if ($single) {
        return $query->row();
    }
    return $query->result();
	}

	public function view_single_row($table_name,$where,$field)	{		
		$this->db->where($where,$field);
		$query = $this->db->get($table_name);
		return $query->row_array();
	}
	public function view_single_row_single_column($table_name,$where,$column) {
		$this->db->select($table_name.".".$column);
		$this->db->where($where);
		$query = $this->db->get($table_name);
		return $query->row_array();
	}	

	public function view_limit_data($table_name,$col,$order,$limit)	{
		$this->db->order_by($col,$order);
		$this->db->limit($limit);
		$query = $this->db->get($table_name);
		return $query->result_array();
	}

	//view data
	public function view_data($table_name,$condition)	{
		$this->db->where($condition);
		$query = $this->db->get($table_name);
		return $query->result_array();
	}
	//view data by order
	public function view_data_by_order($table_name,$condition,$column,$order)	{
		$this->db->order_by($column,$order);
		$this->db->where($condition);
		$query = $this->db->get($table_name);
		return $query->result_array();
	}

	public function get_num_rows($table_name,$condition){
		$this->db->where($condition);
		$query = $this->db->get($table_name);
		return $query->num_rows();
	}

	public function array_merge_custom($first,$second,$key='') {
	    $result = array();
	    foreach($first as $value) {
	    	if ($key != '') {
	    		$result[$value->$key] = $value;
	    	} else {
	    		$result[] = $value;
	    	}
	    }
	    foreach($second as $value) {
	        if ($key != '') {
	    		$result[$value->$key] = $value;
	    	} else {
	    		$result[] = $value;
	    	}
	    }
	    return $result;
	}

	public function replace_url_to_anchor_tag($message)	{
		// $message = htmlspecialchars_decode($message);
		$reg_exUrl = "/((https?:\/\/)|(www))[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,256}(\/\S*)?/";
		if (preg_match_all($reg_exUrl,$message,$matches)) {
        	foreach ($matches[0] as $key => $match) {
        		$linkurl = preg_replace("/^((https?:\/\/)|(www.))/i", "//", $match);
				if ($getmessage) {
					$getmessage = str_replace(
					    $match,
					    '<a href="'.$linkurl.'" target="_blank" class="site_link_msgbox">'.$match.'</a>',
					    $getmessage
					);					
				} else {
					$getmessage = str_replace(
					    $match,
					    '<a href="'.$linkurl.'" target="_blank" class="site_link_msgbox">'.$match.'</a>',
					    $message
					);
				}
			}
        } else {
            $getmessage = $message;
        }
        return $getmessage;
	}
	public function backend_game_list()	{
		$this->db->select('ag.id,ag.game_name,ag.game_description,ag.created_at,GROUP_CONCAT(c.category_name SEPARATOR " | ") as category_name');
		$this->db->from('admin_game ag');
		$this->db->join('admin_game_category c','c.id=ag.game_category_id');
		$this->db->where('ag.custom',null);		
		$this->db->group_by('ag.game_name');
		$this->db->order_by('ag.created_at','desc');
		$query=$this->db->get();
		return $query->result_array();
	}
	public function backend_game_cat_list()	{
		$this->db->select('agc.*');
		$this->db->from('admin_game_category agc');
		$this->db->where('agc.custom',null);
		$this->db->where('agc.is_deleted',0);
		$query=$this->db->get();
		return $query->result_array();
	}
	public function backend_subgame_list()	{
		$this->db->select('sg.id,ag.game_name,ag.game_category_id,gc.category_name,sg.sub_game_name');
		$this->db->from('admin_game ag');
		$this->db->join('admin_sub_game sg','sg.game_id=ag.id');		
		$this->db->where('sg.custom',null);
		$this->db->join('admin_game_category gc','gc.id=ag.game_category_id');
		$query=$this->db->get();
		return $query->result_array();
	}

	public function open_challenge_game_list() {
		
		$this->db->select('g.*');

		$this->db->from('game g');

		$update_query=$this->db->get();
			
		foreach ($update_query->result() as $key => $val) {

			if ($val->game_timer != "0000-00-00 00:00:00" && strtotime($val->game_timer) < time() && $val->status == '1' && $val->payment_status == '1') {

					$get_price_game = $this->view_data('game',array('id'=>$val->id));
					$points = $this->view_data('user_detail',array('user_id'=>$val->user_id));
					$percentage = $this->view_all_data('admin_points_percentage','id','asc');                        
					$lessthan5_total 		= $percentage[0]['lessthan5'];	
					$morethan5_25_total 	= $percentage[0]['morethan5_25'];	
					$morethan25_50_total 	= $percentage[0]['morethan25_50'];	
					$morethan50_total		= $percentage[0]['morethan50'];	
					
					
					$price_total = $get_price_game[0]['price'];
					if($price_total < 5){
						$price_add = $price_total + $lessthan5_total;
					}else if($price_total >= 5 && $price_total < 25){
						$price_add = $price_total + $morethan5_25_total;
					} else if($price_total >= 25 && $price_total < 50){
						$price_add = $price_total + $morethan25_50_total;
					} else if($price_total >= 50){
						$price_add = $price_total + $morethan50_total;
					}
					$price_add_decimal = number_format((float)$price_add, 2, '.', '');
					$update_balance	=	$points[0]['total_balance'] + $price_add_decimal ;
					$data_Where = array(
						'total_balance'	=>	$update_balance,
					);

					if ($_SESSION['user_id'] == $val->user_id) {
						$_SESSION['total_balance'] = $update_balance;
					}
					$this->update_data('user_detail',$data_Where,array('user_id'=>$val->user_id));
					$data=array(
						'updated_at' => date('Y-m-d'),
						'status'=> 0
					);
					$this->update_data('game',$data,array('id'=>$val->id));
			}
		}

		$this->db->select('g.id,g.price,g.user_id,g.game_timer,g.description,g.status,g.created_at,g.payment_status,gi.game_image,ag.game_name');		
		$this->db->from('game g');
		$this->db->join('bet b','b.challenger_id=g.user_id');
		$this->db->join('admin_game ag','ag.id=g.game_id');
		$this->db->join('game_image gi','gi.id=g.image_id');
		// $this->db->join('admin_game ag','ag.id=g.game_id');
		$this->db->where('g.payment_status',1);
		$this->db->where('g.status',1);
		$this->db->where('g.user_id',$this->session->userdata('user_id'));
		$this->db->group_by('g.id');
		$query=$this->db->get();
	

		return $query->result_array();

	}

	public function friend_challange_list() {
		
		$this->db->select('asg.sub_game_name,g.game_password, g.id,g.game_category_id,g.price,g.game_timer,g.description,g.device_id,g.game_type,g.created_at,g.game_id,gi.game_image,ag.game_name,ud.name,g.sub_game_id');

		$this->db->from('game g');

		$this->db->join('user_detail ud','g.user_id=ud.user_id');

		$this->db->join('game_image gi','gi.id=g.image_id');

		$this->db->join('admin_game ag','ag.id=g.game_id');

		$this->db->join('admin_sub_game asg','g.sub_game_id = asg.id');

		$this->db->where("g.payment_status='1' AND g.status='1' AND g.personal_challenge ='yes' AND g.challenge_friend_id= '".$_SESSION['user_id']."'");

		// $this->db->where("g.personal_challenge ='yes' AND g.challenge_friend_id= '".$_SESSION['user_id']."'");

		$this->db->limit(0,5);

		$query=$this->db->get();
		
		return $query->result_array();

	}

	public function pending_game_review() {
		$this->db->select('g.id,g.price,g.user_id,g.description,g.status,g.game_id,g.created_at,g.payment_status,gi.game_image,ag.game_name,b.challenger_id,b.accepter_id,g.device_id,g.accepter_device_id');
		$this->db->from('game g');
		$this->db->join('game_image gi','gi.id=g.image_id');
		$this->db->join('admin_game ag','ag.id=g.game_id');
		$this->db->join('bet b','b.id=g.bet_id');
		//$this->db->where("accepter_id",$this->session->userdata("user_id"));
		$where = "g.status='2' AND b.flag='0' AND `b.challenger_id`='".$this->session->userdata('user_id')."'";
		//$this->db->where("accepter_id",$this->session->userdata('user_id') OR "challenger_id",$this->session->userdata('user_id'));
		//$this->db->where('challenger_id',$this->session->userdata('user_id'));
		//$where = "status='2' AND payment_status='1'";
		$this->db->where($where);
		$this->db->group_by('g.id');
		$query=$this->db->get();
		return $query->result_array();
	}

	public function playing_game_list() {
		$this->db->select('g.id,g.price,g.user_id,g.description,g.status,g.game_id,g.created_at,g.payment_status,gi.game_image,ag.game_name,b.challenger_id,b.accepter_id,g.device_id,g.accepter_device_id');
		$this->db->from('game g');
		$this->db->join('game_image gi','gi.id=g.image_id');
		$this->db->join('admin_game ag','ag.id=g.game_id');
		$this->db->join('bet b','b.id=g.bet_id');
		//$this->db->where("accepter_id",$this->session->userdata("user_id"));
		$where = "1 AND (g.status='2') AND b.flag='0' AND (`b.accepter_id`='".$this->session->userdata('user_id')."' OR `b.challenger_id`='".$this->session->userdata('user_id')."')";
		//$this->db->where("accepter_id",$this->session->userdata('user_id') OR "challenger_id",$this->session->userdata('user_id'));
		//$this->db->where('challenger_id',$this->session->userdata('user_id'));
		//$where = "status='2' AND payment_status='1'";
		$this->db->where($where);
		$this->db->group_by('g.id');
		$res = $this->db->get()->result_array();
		return $res;
	}

	public function xbox_game_list() {
		$this->db->select('g.*');
		$this->db->from('game g');
		$update_query=$this->db->get();
		foreach ($update_query->result() as $key => $val) {
			if ($val->game_timer != "0000-00-00 00:00:00" && strtotime($val->game_timer) < time() && $val->status == '1' && $val->payment_status == '1') {
				$get_price_game = $this->view_data('game',array('id'=>$val->id));
				$points = $this->view_data('user_detail',array('user_id'=>$val->user_id));
				$percentage = $this->view_all_data('admin_points_percentage','id','asc');                        
				$lessthan5_total 		= $percentage[0]['lessthan5'];	
				$morethan5_25_total 	= $percentage[0]['morethan5_25'];	
				$morethan25_50_total 	= $percentage[0]['morethan25_50'];	
				$morethan50_total		= $percentage[0]['morethan50'];	

				$price_total = $get_price_game[0]['price'];
				if($price_total < 5){
					$price_add = $price_total + $lessthan5_total;
				} else if ($price_total >= 5 && $price_total < 25) {
					$price_add = $price_total + $morethan5_25_total;
				} else if ($price_total >= 25 && $price_total < 50) {
					$price_add = $price_total + $morethan25_50_total;
				} else if ($price_total >= 50) {
					$price_add = $price_total + $morethan50_total;
				}
				$price_add_decimal = number_format((float)$price_add, 2, '.', '');

				$update_balance	=	$points[0]['total_balance'] + $price_add_decimal ;
				$data_Where = array(
					'total_balance'	=>	$update_balance,
				);
				if ($_SESSION['user_id'] == $val->user_id) {
					$_SESSION['total_balance'] = $update_balance;
				}
				$this->update_data('user_detail',$data_Where,array('user_id'=>$val->user_id));

				$data=array(
					'updated_at' => date('Y-m-d'),
					'status'=> 0
				);
				$this->update_data('game',$data,array('id'=>$val->id));
			}
		}
		
		$this->db->select('asg.sub_game_name,g.id,g.best_of,g.game_category_id,g.price,g.game_timer,g.description,g.device_id,g.game_type,g.created_at,g.game_id,g.game_password,gi.game_image,ag.game_name,ud.name,g.sub_game_id,agc.cat_slug, agc.cat_img');

		$this->db->from('game g');

		$this->db->join('user_detail ud','g.user_id=ud.user_id');

		$this->db->join('game_image gi','gi.id=g.image_id');

		$this->db->join('admin_game ag','ag.id=g.game_id');
		$this->db->join('admin_game_category agc','agc.id=g.game_category_id');
		$this->db->join('admin_sub_game asg','g.sub_game_id = asg.id');

		$this->db->where("g.payment_status='1' AND g.status='1' AND g.personal_challenge ='no'");

		$this->db->limit(0,5);

		$query=$this->db->get();

		return $query->result_array();

	}

	public function lobby_list($arr='') {
		$this->db->select('l.id, l.user_id as lobby_crtor, l.id, l.game_category_id, l.price, l.description, l.device_id, l.game_type, l.created_at, l.game_id, l.sub_game_id, l.event_start_date, l.event_end_date, l.event_price, l.registration_status, l.event_description, l.is_event, l.is_spectator, l.spectate_price, l.is_archive, ud.name, ud.common_settings, ud.timezone as creator_timezone, gi.game_image, asg.sub_game_name, ag.game_name, ag.game_category_id as game, agc.cat_img, agc.cat_slug, lf.stream_status, lf.stream_type, lf.obs_stream_channel, lf.stream_channel, agc.category_name');
		$this->db->from('lobby l');
		$this->db->join('user_detail ud','l.user_id = ud.user_id');
		$this->db->join('game_image gi','gi.id = l.image_id');
		$this->db->join('admin_game ag','ag.id = l.game_id');
		$this->db->join('admin_sub_game asg','l.sub_game_id = asg.id');
		$this->db->join('group_bet gb','gb.lobby_id = l.id');
		$this->db->join('lobby_fans lf','lf.lobby_id = l.id AND lf.user_id = l.user_id');
		$this->db->join('lobby_group lg','lg.group_bet_id = gb.id AND lg.user_id = l.user_id', 'LEFT OUTER');
		$this->db->join('admin_game_category agc','agc.id = l.game_category_id');
		$this->db->where('NOT ((lf.stream_channel IS NULL OR lf.stream_channel = "") AND (lf.obs_stream_channel IS NULL OR lf.obs_stream_channel = ""))');
		(!empty($arr['system'])) ? $this->db->where("agc.id",$arr['system']) : '';
		(!empty($arr['game_name'])) ? $this->db->where("asg.game_id",$arr['game_name']) : '';
		(!empty($arr['sub_game_name'])) ? $this->db->where("asg.id",$arr['sub_game_name']) : '';
		(!empty($arr['game_size_var'])) ? $this->db->where("l.game_type",$arr['game_size_var']) : '';
		(!empty($arr['srch_term'])) ? $this->db->where("(l.device_id LIKE '%".$arr['srch_term']."%' OR ag.game_name LIKE '%".$arr['srch_term']."%')") : '';
		if (!empty($arr['is_event'])) {
			$this->db->where("l.is_event", $arr['is_event']);
			$this->db->order_by('l.event_start_date','desc');
			$this->db->order_by('l.registration_status','desc');
		} else {
			$this->db->where("l.is_event", 0);
			$this->db->order_by("l.lobby_order","asc");
		}
		// $this->db->join('lobby_group lg','lg.group_bet_id = gb.id');
		$this->db->where("l.status", 1);
		$this->db->where("gb.bet_status", 1);
		$this->db->where("l.is_archive", 0);
		$this->db->where("lf.stream_status", "enable");
		// $this->db->where("lg.is_creator", 'yes');
		$this->db->limit(0,5);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function created_lobbys($user_id) {
		$this->db->select('asg.sub_game_name, l.user_id as creator, l.is_event, l.id as lobby_id, l.game_category_id, l.price, l.description, l.device_id, l.game_type, l.created_at, l.game_id, gi.game_image, ag.game_name, ud.name, ud.is_admin, l.sub_game_id, gb.id as group_bet_id, l.custom_name,l.is_archive, l.event_start_date');
		$this->db->from('lobby l');
		$this->db->join('user_detail ud','l.user_id=ud.user_id');
		$this->db->join('game_image gi','gi.id=l.image_id');
		$this->db->join('admin_game ag','ag.id=l.game_id');
		$this->db->join('admin_sub_game asg','l.sub_game_id = asg.id');
		$this->db->join('group_bet gb','gb.lobby_id = l.id');
		$this->db->where("l.payment_status='1' AND l.status='1' AND l.user_id='".$user_id."' AND l.is_archive = '0'");
		$this->db->where("gb.bet_status", 1);
		$this->db->where("l.is_archive", 0);
		$data = $this->db->get()->result_array();
		return $data;
	}

	public function get_my_lobby_list($user_id = '',$waiting_include) {
		$this->db->select('asg.sub_game_name,l.user_id as creator,l.is_event,l.id as lobby_id,l.game_category_id,l.price,l.description,l.device_id,l.game_type,l.created_at,l.game_id,gi.game_image,ag.game_name,ud.name,l.sub_game_id,gb.id as group_bet_id, lg.is_creator, lg.is_controller, lg.user_id, "no" as from_waitinglist,l.is_archive,l.event_start_date');
		$this->db->from('lobby l');
		$this->db->join('game_image gi','gi.id = l.image_id');
		$this->db->join('admin_game ag','ag.id = l.game_id');
		$this->db->join('admin_sub_game asg','l.sub_game_id = asg.id');
		$this->db->join('group_bet gb','gb.lobby_id = l.id');
		$this->db->join('lobby_group lg','lg.group_bet_id = gb.id');
		$this->db->join('user_detail ud','ud.user_id = lg.user_id');
		$this->db->where("l.payment_status='1' AND l.status='1' AND l.is_archive = '0'");

		if ($user_id !='') {
			$this->db->where("lg.user_id", $user_id);
			$this->db->where("l.user_id !=".$user_id);
		} else {
			$this->db->where("lg.user_id", $_SESSION['user_id']);
			$this->db->where("l.user_id !=".$_SESSION['user_id']);
		}
		$this->db->where("gb.bet_status", 1);
		$this->db->where("l.is_event", 0);
		// $this->db->limit(0,5);
		$res_data = $this->db->get()->result_array();
		
		// if user is in waiting list of any lobby in fans table 
		if ($waiting_include == 'yes') {
			$this->db->select('asg.sub_game_name,l.user_id as creator,l.is_event,l.id as lobby_id,l.game_category_id,l.price,l.description,l.device_id,l.game_type,l.created_at,l.game_id,gi.game_image,ag.game_name,ud.name,l.sub_game_id,gb.id as group_bet_id, "yes" as from_waitinglist, lf.user_id');
			$this->db->from('lobby l');
			$this->db->join('game_image gi','gi.id=l.image_id');
			$this->db->join('admin_game ag','ag.id=l.game_id');
			$this->db->join('admin_sub_game asg','l.sub_game_id = asg.id');
			$this->db->join('group_bet gb','gb.lobby_id = l.id');
			$this->db->join('lobby_fans lf','lf.lobby_id = l.id');
			$this->db->join('user_detail ud','ud.user_id = lf.user_id');
			$this->db->where("l.payment_status='1' AND l.status='1'");
			$this->db->where("lf.is_waiting",1);
			$this->db->where("l.is_archive", 0);
			$this->db->where("l.is_event", 0);
			if ($user_id !='') {
				$this->db->where("lf.user_id", $user_id);
				$this->db->where("l.user_id !=".$user_id);
			} else {
				$this->db->where("lf.user_id", $_SESSION['user_id']);
				$this->db->where("l.user_id !=".$_SESSION['user_id']);
			}
			$this->db->where("gb.bet_status", 1);
			// $this->db->limit(0,5);
			$res_data_fans = $this->db->get()->result_array();
			$res_data = array_merge($res_data,$res_data_fans);
		}
		return $res_data;
	}

	public function game_system_list() {
		$this->db->select('agc.*');
		$this->db->from('admin_game_category agc');
		$this->db->where('agc.custom',null);
		$this->db->where('agc.is_deleted',0);
		$this->db->order_by('display_order','asc');
		$query = $this->db->get();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
		    foreach ($query->result_array() as $row) {
		        $data[] = $row;
		    }
		}
		return $data;
	}

	public function playstore_game_list() {

		$this->db->select('asg.sub_game_name,g.id,g.price,g.game_category_id, g.description,g.device_id,g.game_timer,g.game_type,g.created_at,g.game_id,gi.game_image,ag.game_name,ud.name');

		$this->db->from('game g');

		$this->db->join('user_detail ud','g.user_id=ud.user_id');

		$this->db->join('game_image gi','gi.id=g.image_id');

		$this->db->join('admin_game ag','ag.id=g.game_id');

		$this->db->join('admin_sub_game asg','g.sub_game_id = asg.id');

		$this->db->where("g.payment_status='1' AND g.status='1' AND g.game_category_id='2'");

		$this->db->limit(0,5);

		$query=$this->db->get();

		return $query->result_array();

	}

	public function windows_game_list() {

		$this->db->select('g.id,g.price,g.description,g.created_at,g.game_id,gi.game_image,ag.game_name,ud.name');

		$this->db->from('game g');

		$this->db->join('user_detail ud','g.user_id=ud.user_id');

		$this->db->join('game_image gi','gi.id=g.image_id');

		$this->db->join('admin_game ag','ag.id=g.game_id');

		$this->db->where("g.payment_status='1' AND g.status='1' AND g.game_category_id='3'");

		$this->db->limit(0,5);

		$query=$this->db->get();

		return $query->result_array();

	}

	public function admin_xbox_game_list() 
	{

		$this->db->group_by('game_id');
		$this->db->select('ag.id,ag.game_name,ag.game_description,ag.created_at,gi.game_image');

		$this->db->from('admin_game ag');

		$this->db->join('game_image gi','gi.game_id=ag.id');
		

		$this->db->where("ag.play_status='0' AND ag.game_category_id='1'");

		$this->db->limit(0,5);

		$query=$this->db->get();
		return $query->result_array();

	}

	public function admin_playstore_game_list() {

		$this->db->group_by('game_id');

		$this->db->select('ag.id,ag.game_name,ag.game_description,ag.created_at,gi.game_image');

		$this->db->from('admin_game ag');

		$this->db->join('game_image gi','gi.game_id=ag.id');

		$this->db->where("ag.play_status='0' AND ag.game_category_id='2'");

		$this->db->limit(0,5);

		$query=$this->db->get();

		return $query->result_array();

	}

	public function admin_windows_game_list($condition='') {

		$this->db->group_by('game_id');
		$this->db->select('ag.id,ag.game_name,ag.game_description,ag.created_at,gi.game_image');
		$this->db->from('admin_game ag');
		$this->db->join('game_image gi','gi.game_id=ag.id');
		$this->db->where($condition);
		/*$this->db->limit(0,5);*/
		$query=$this->db->get();
		return $query->result_array();
	}
	public function winning_game_list() {

		// $this->db->select('ag.game_name,ag.game_description,ud.name,ud.user_id,aph.amount,aph.payment_date');

		$this->db->select('ag.game_name, g.price, b.winner_id');

		// $this->db->select('b.*, ag.*, ud.*, g.*');

		$this->db->from('bet b');

		$this->db->join('admin_game ag','ag.id = b.game_id');

		$this->db->join('game g','g.game_id = b.game_id');

		// $this->db->join('payment p','p.game_id = g.game_id');

		// $this->db->join('admin_payment_history aph','aph.bet_id = b.id');

		$this->db->join('user_detail ud','ud.user_id=b.winner_id');

		$this->db->where('b.winner_id',$this->session->userdata('user_id'));

		// $this->db->where('aph.payment_status','completed');

		// $this->db->order_by('b.id', 'DESC');
		$this->db->group_by('b.id');
		
		$this->db->order_by('b.id', 'DESC');

		$this->db->limit('5');

		$query=$this->db->get();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
		    foreach ($query->result_array() as $row) {
		        $data[] = $row;
		    }
		}

		return $data;

		// return $query->result_array();


	}

	public function played_game_list() {

		$this->db->distinct();

		$this->db->select('*');

		$this->db->from('bet b');

		$this->db->join('admin_game ag','ag.id=b.game_id');		

		$this->db->where('b.loser_id',$this->session->userdata('user_id'));		

		$this->db->group_by('b.id');
		
		$this->db->order_by('b.id', 'DESC');
		
		$this->db->limit('5');

		$query=$this->db->get();

		return $query->result_array();
	}

	public function lost_games() {

		$this->db->distinct();

		$this->db->select('*');

		$this->db->from('bet b');

		$this->db->join('admin_game ag','ag.id=b.game_id');

		$this->db->join('game g','g.game_id=b.game_id');

		$this->db->join('user_detail ud','ud.user_id=b.loser_id');

		$this->db->join('game_image gi','gi.game_id = b.game_id');

		$this->db->where('b.loser_id',$this->session->userdata('user_id'));

		$this->db->where('g.game_id', $this->session->userdata('game_id'));

		// $this->db->order_by('p.payment_date', 'DESC');

		$this->db->group_by('g.game_id');

		$this->db->limit('5');

		$query=$this->db->get();

		return $query->result_array();

	}

	public function frontend_gamevideo_section() {

		$this->db->distinct();

		$this->db->select('v.video_url,v.add_time,ag.game_name,ag.game_description,g.device_id,g.accepter_device_id');

		$this->db->from('video v');

		$this->db->join('bet b','b.winner_id=v.user_id');		

		$this->db->join('admin_game ag','ag.id=v.game_id');

		$this->db->join('game g','g.game_id=v.game_id');

		//$this->db->join('user_detail ud','ud.user_id=v.user_id');

		$this->db->where('b.game_id=v.game_id');
		$this->db->where('v.video_url !="" ');
		$this->db->where('g.video_id !=""  AND g.video_id !="0"');
		$this->db->group_by('g.video_id');
		$this->db->order_by('g.id','desc'); 

		$this->db->limit(8);

		$query=$this->db->get();

		// print_r($query->result_array()); exit(0);

		return $query->result_array();
	}

	public function challenger_game_details($id) {
		$this->db->select('asg.sub_game_name,g.id,g.price,g.game_password,g.description,g.sub_game_id,g.created_at,g.device_id,g.game_type,g.game_id,gi.game_image,ag.game_name,ud.name,ud.image');
		$this->db->from('game g');
		$this->db->join('user_detail ud','g.user_id=ud.user_id');
		$this->db->join('game_image gi','gi.id=g.image_id');
		$this->db->join('admin_game ag','ag.id=g.game_id');
		$this->db->join('admin_sub_game asg','asg.id=g.sub_game_id');
		$this->db->where('g.id',$id);
		$query=$this->db->get();
		return $query->result_array();
	}

	public function lobby_details($id) {
		$this->db->select('asg.sub_game_name,l.is_event_image,l.id,l.user_id as creator,l.user_id,l.event_image,l.spectate_price,l.is_spectator,l.is_event,l.event_price,l.price,l.description,l.sub_game_id,l.created_at,l.device_id,l.game_type,l.game_id,l.registration_status,gi.game_image,ag.game_name,ud.name,ud.image,l.is_archive,l.custom_name');
		$this->db->from('lobby l');
		$this->db->join('user_detail ud','l.user_id=ud.user_id');
		$this->db->join('game_image gi','gi.id=l.image_id');
		$this->db->join('admin_game ag','ag.id=l.game_id');
		$this->db->join('admin_sub_game asg','asg.id=l.sub_game_id');
		$this->db->where('l.id',$id);
		$query=$this->db->get();
		return $query->result_array();
	}

	public function single_lobby_detail($id) {
		$this->db->select('asg.sub_game_name,l.id,l.price,l.description,l.sub_game_id,l.created_at,l.device_id,l.game_type,l.game_id,gi.game_image,ag.game_name,ud.name,ud.image');
		$this->db->from('lobby l');
		$this->db->join('user_detail ud','l.user_id=ud.user_id');
		$this->db->join('game_image gi','gi.id=l.image_id');
		$this->db->join('admin_game ag','ag.id=l.game_id');
		$this->db->join('admin_sub_game asg','asg.id=l.sub_game_id');
		$this->db->where('l.id',$id);
		$query=$this->db->get();
		return $query->result_array();
	}

	public function lobby_bet_detail($id){
		$this->db->select('gb.*, asg.sub_game_name, l.user_id as crtr, l.is_event, l.is_spectator, l.event_price, l.event_image, l.spectate_price, l.game_type, l.price, l.custom_name, l.event_fee, l.spectator_fee, l.lobby_password, gi.game_image as lobby_image, ag.game_name as lobby_name, ag.game_category_id, ag.game_description, ag.game_name, asg.sub_game_name, asg.id as sub_game_id, gi.game_image, l.image_id as game_image_id, ag.custom as is_custom, l.event_title, l.event_key, l.event_description, l.event_start_date, l.event_end_date, l.is_new_lobby, l.is_event_start, l.is_event_end, l.for_18_plus, l.is_override_prize, l.override_prize_amt');
		$this->db->from('group_bet gb');
		$this->db->join('lobby l','l.id = gb.lobby_id');
		$this->db->join('admin_game ag','ag.id = l.game_id');
		$this->db->join('admin_sub_game asg','asg.game_id = l.game_id');
		$this->db->join('game_image gi','gi.id = l.image_id');
		$this->db->where('gb.lobby_id',$id);
		$this->db->where('gb.is_active',1);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function lobby_game_imgs($id){
		$this->db->select('gi.game_image, gi.id');
		$this->db->from('game_image gi');
		$this->db->join('lobby l','l.game_id = gi.game_id');
		$this->db->join('group_bet gb','gb.lobby_id = l.id');
		$this->db->where('gb.lobby_id',$id);
		$this->db->where('gb.is_active',1);
		$query=$this->db->get();
		return $query->result_array();
	}
	public function stream_chat_expire($left,$right){
		$main_arr = array_merge($left,$right);
		$is_exist = 0;
		foreach ($main_arr as $key => $ar) {
			if ($ar['user_id'] == $_SESSION['streamer_chat_id_set']) {
				$is_exist = 1;
			}
		}
		if ($is_exist == 0) {
			unset($_SESSION['lobby_id']);
            unset($_SESSION['streamer_name_set']);
            unset($_SESSION['streamer_chat_id_set']);
            unset($_SESSION['streamer_channel_set']);   	
		}
		$this->db->select('gb.*,l.game_type,l.price,l.lobby_password,gi.game_image as lobby_image,ag.game_name as lobby_name');
		$this->db->from('group_bet gb');
		$this->db->join('lobby l','l.id=gb.lobby_id');
		$this->db->join('admin_game ag','ag.id=l.game_id');
		$this->db->join('game_image gi','gi.game_id=ag.id');
		$this->db->where('gb.lobby_id',$id);
		$this->db->where('gb.is_active',1);
		$query=$this->db->get();
		return $query->result_array();
	}
	public function get_play_status_lobby_bet($id){
		$this->db->select('lg.*, gb.*');
		$this->db->from('lobby_group lg');
		$this->db->join('group_bet gb','gb.id=lg.group_bet_id');
		$this->db->where('gb.lobby_id',$id);
		$this->db->where('gb.is_active',1);
		$this->db->where('lg.play_status',1);
		$this->db->where('lg.status',1);
		$query=$this->db->get();
		return $query->result_array();
	}
	public function get_lefttop_played_id($group_bet_id,$lobby_id){
		$this->db->select('lg.user_id');
		$this->db->from('lobby_group lg');
		$this->db->join('group_bet gb','gb.id=lg.group_bet_id');
		$this->db->where('gb.lobby_id',$lobby_id);
		$this->db->where('lg.group_bet_id',$group_bet_id);
		$this->db->where('lg.table_cat','LEFT');
		$this->db->where('lg.play_status',1);
		$this->db->where('lg.status',1);
		$query=$this->db->get();
		return $query->row();
	}	
	public function get_righttop_played_id($group_bet_id,$lobby_id){
		$this->db->select('lg.user_id');
		$this->db->from('lobby_group lg');
		$this->db->join('group_bet gb','gb.id=lg.group_bet_id');
		$this->db->where('gb.lobby_id',$lobby_id);
		$this->db->where('lg.group_bet_id',$group_bet_id);
		$this->db->where('lg.table_cat','RIGHT');
		$this->db->where('lg.play_status',1);
		$this->db->where('lg.status',1);
		$query=$this->db->get();
		return $query->row();
	}
	public function lobby_head_tags($lobby_id,$group_bet_id='',$table_cat=''){
		$this->db->select('lf.fan_tag');
		$this->db->from('lobby_group lg');
		$this->db->join('lobby_fans lf','lf.user_id=lg.user_id');
		$this->db->join('group_bet gb','gb.id=lg.group_bet_id');
		if ($group_bet_id != '') {
			$this->db->where('gb.id',$group_bet_id);
		}
		if ($table_cat !='') {
			$this->db->where('lg.table_cat',$table_cat);
		}
		$this->db->where('lf.lobby_id',$lobby_id);
		$this->db->where('lg.play_status',1);
		$this->db->where('gb.bet_status',2);
		$this->db->where('lg.status',1);
		$query=$this->db->get();
		return $query->row()->fan_tag;
	}
	public function get_my_fan_tag($lobby_id,$user_id){
		$this->db->select('lf.id, lf.user_id, lf.fan_tag, lf.status as fan_status, lf.mute_time, lf.stream_status, lf.stream_channel, lf.waiting_table, lf.is_waiting, lf.team_name, ud.is_18, ud.image, lf.is_second_banner, lf.stream_type, lf.stream_channel, lf.obs_stream_channel');
		$this->db->from('lobby_fans lf');
		$this->db->join('user_detail ud','ud.user_id = lf.user_id');
		$this->db->where('lf.lobby_id',$lobby_id);
		$this->db->where('lf.user_id',$user_id);
		$query=$this->db->get();
		return $query->row();
	}
	public function get_first_report_dtime($id){
		$this->db->select('lg.updated_at, lg.user_id, gb.*, ud.timezone');
		$this->db->from('lobby_group lg');
		$this->db->join('user_detail ud','ud.user_id = lg.user_id');
		$this->db->join('group_bet gb','gb.id=lg.group_bet_id');
		$this->db->where('gb.lobby_id',$id);
		$this->db->where('gb.is_active',1);
		$this->db->where('lg.play_status',1);
		$this->db->where('lg.status',1);
		$this->db->order_by('lg.updated_at',"ASC");
        $this->db->where('lg.updated_at !=','0000-00-00 00:00:00');
		$query=$this->db->get();
		return $query->row();
	}
	public function get_grp_for_me($id,$user_id='',$table_cat='',$numrow=''){
		$this->db->select('lg.group_bet_id, lg.table_cat, lg.user_id, lg.reported, lg.play_status, lg.win_lose_status, lg.is_creator, lg.is_controller, lg.is_banner, gb.*, ud.name, ud.user_id, ud.team_name, ud.image, ud.display_name, ud.display_name_status, lf.stream_status, lf.stream_type, lf.stream_channel, lf.obs_stream_channel');
		// , COUNT(fu.id) as followercount, GROUP_CONCAT(fu.followers_id SEPARATOR ",") as followers_arr
		$this->db->from('lobby_group lg');
		$this->db->join('group_bet gb','gb.id = lg.group_bet_id');
		$this->db->join('user_detail ud','ud.user_id = lg.user_id');
		$this->db->join('lobby_fans lf','lf.lobby_id = gb.lobby_id AND lf.user_id = lg.user_id');
		// $this->db->join('follower_users fu','fu.following_id = lg.user_id', 'left outer');
		$this->db->where('gb.is_active',1);
		$this->db->where('gb.lobby_id',$id);
		$this->db->where('lg.status',1);
		$this->db->where('gb.bet_status',1);
		($table_cat !='') ? $this->db->where('lg.table_cat',$table_cat) : '';
		($user_id !='') ? $this->db->where('lg.user_id',$user_id) : $this->db->where('lg.user_id',$this->session->userdata('user_id'));
		$query = $this->db->get();
		$res = ($numrow=='single') ? $query->row() : $query->result();
		return $res;
	}
	public function fanscount($id) {
		$this->db->select('lf.*');
		$this->db->from('lobby_fans lf');
		$this->db->where('lf.lobby_id',$id);
		$query=$this->db->get();
		return $query->result_array();
	}
	public function check_bet_reported_for_me($id) {
		$this->db->select('lg.*');
		$this->db->from('lobby_group lg');
		$this->db->join('group_bet gb','gb.id=lg.group_bet_id');
		$this->db->where('gb.lobby_id',$id);
		$this->db->where('gb.bet_status',1);
		$this->db->where('lg.user_id',$_SESSION['user_id']);
		$this->db->where('lg.status',1);
		$query=$this->db->get();
		return $query->result_array();
	}
	public function check_bet_reported($id) {
		$this->db->select('lg.*');
		$this->db->from('lobby_group lg');
		$this->db->join('group_bet gb','gb.id=lg.group_bet_id');
		$this->db->where('gb.lobby_id',$id);
		$this->db->where('gb.bet_status',1);
		$this->db->where('lg.status',1);
		$this->db->where('lg.reported',1);
		$query=$this->db->get();
		return $query->result_array();
	}
	public function lobby_game_bet_left_grp($id) {
		$this->db->select('lg.group_bet_id, lg.table_cat, lg.user_id, lg.reported, lg.play_status, lg.win_lose_status, lg.is_creator, lg.is_controller, lg.is_banner, gb.*, ud.name, ud.user_id, ud.team_name, ud.image, ud.display_name, ud.display_name_status, lf.stream_status, lf.stream_type, lf.stream_channel, lf.obs_stream_channel');
		$this->db->from('lobby_group lg');
		$this->db->join('group_bet gb','gb.id=lg.group_bet_id');
		$this->db->join('lobby_fans lf','lf.lobby_id = gb.lobby_id AND lf.user_id = lg.user_id');
		$this->db->join('user_detail ud','ud.user_id=lg.user_id');
		$this->db->where('gb.lobby_id',$id);
		$this->db->where('lg.table_cat','LEFT');
		$this->db->where('gb.bet_status',1);
		$query=$this->db->get();
		return $query->result_array();
	}
	public function get_defult_lobby_chat($id){
		$this->db->select('lf.*, ud.name, ud.user_id, ud.team_name, ud.image');
		$this->db->from('lobby l');
		$this->db->join('lobby_fans lf','lf.user_id = l.user_id AND lf.lobby_id = l.id');
		$this->db->join('user_detail ud','ud.user_id = l.user_id');
		$this->db->where('l.id', $id);
		$this->db->where('l.is_archive', 0);
		$creator_info = $this->db->get()->row();
		// if ($creator_info->is_waiting == 0) {
		// 	$this->db->select('lg.*,gb.lobby_id,ud.name, ud.user_id, ud.team_name, ud.image, lf.id as fan_id, lf.fan_tag');
		// 	$this->db->from('lobby_group lg');
		// 	$this->db->join('group_bet gb','gb.id=lg.group_bet_id');
		// 	$this->db->join('lobby l','l.id=gb.lobby_id');
		// 	$this->db->join('user_detail ud','ud.user_id=l.user_id');
		// 	$this->db->join('lobby_fans lf','lf.user_id=l.user_id AND lf.lobby_id=l.id');
		// 	$this->db->where('gb.lobby_id',$id);
		// 	$this->db->where('lg.is_creator','yes');
		// 	$this->db->where('gb.bet_status',1);
		// 	$creator_info = $this->db->get()->row();
		// }
		return $creator_info;
	}
	public function lobby_game_bet_right_grp($id) {
		$this->db->select('lg.group_bet_id, lg.table_cat, lg.user_id, lg.reported, lg.play_status, lg.win_lose_status, lg.is_creator, lg.is_controller, lg.is_banner, gb.*, ud.name, ud.user_id, ud.team_name, ud.image, ud.display_name, ud.display_name_status, lf.stream_status, lf.stream_type, lf.stream_channel, lf.obs_stream_channel');
		$this->db->from('lobby_group lg');
		$this->db->join('group_bet gb','gb.id = lg.group_bet_id');
		$this->db->join('user_detail ud','ud.user_id = lg.user_id');
		$this->db->join('lobby_fans lf','lf.lobby_id = gb.lobby_id AND lf.user_id = lg.user_id');
		$this->db->where('gb.lobby_id',$id);
		$this->db->where('lg.table_cat','RIGHT');
		$this->db->where('gb.bet_status',1);
		$query=$this->db->get();
		return $query->result_array();
	}
	public function get_stream_channels($id) {
		// get the top two default chat of lobby
		$this->db->select('l.id as lobby_id, lf.*, ud.name, ud.user_id, ud.team_name, ud.image');
		$this->db->from('lobby_fans lf');
		$this->db->join('lobby l','l.id = lf.lobby_id AND lf.user_id = l.user_id');
		$this->db->join('user_detail ud','ud.user_id = lf.user_id');
		$this->db->where('l.id',$id);
		// $this->db->where('lf.is_waiting',1);
		$this->db->where('lf.stream_status','enable');
		$fansinfo = $this->db->get()->row();
		$res = $fansinfo;
		// $lmt = (count($fansinfo) >= 2) ? 0 : ((count($fansinfo) == 1) ? 1 : 2);
		// if (count($fansinfo) < 2) {
		// 	$this->db->select('lg.*, ud.name, ud.user_id, ud.team_name, ud.image, gb.lobby_id');
		// 	$this->db->from('lobby_group lg');
		// 	$this->db->join('group_bet gb','gb.id = lg.group_bet_id');
		// 	$this->db->join('user_detail ud','ud.user_id = lg.user_id');
		// 	$this->db->where('lg.stream_status','enable');
		// 	$this->db->where('gb.lobby_id',$id);
		// 	$this->db->where('gb.bet_status',1);
		// 	$this->db->order_by('lg.id');
		// 	// $this->db->limit($lmt);
		// 	$query_res = $this->db->get()->result();
		// 	$res = array_merge($fansinfo,$query_res);
		// }
		// unset($_SESSION['stream_store']);
		// unset($_SESSION['default_stream_enable']);
		// unset($_SESSION['defult_chat_arr']);
		if (!in_array($id, $_SESSION['default_stream_enable']) && !empty($res)) {
			$_SESSION['default_stream_enable'][] = $id;
			// for ($i = 0; $i < count($res); $i++) {
				// if ($i < 4) {
				// if ($i == 0) {
				$defult_chat_arr[] = array(
                    'lobby_id' => $res->lobby_id,
                    'streamer_name_set' => $res->name,
                    'streamer_chat_id_set' => $res->user_id,
                    'streamer_channel_set' => $res->stream_channel,
                    'stream_type' => $res->stream_type,
                    'defult_creator_name' => '',
                );
            	$_SESSION['stream_store'] = (!empty($_SESSION['stream_store'])) ? array_merge($_SESSION['stream_store'], $defult_chat_arr) : $defult_chat_arr;
				// }
			// }
		}
		return $res;
	}	
	public function lobby_creator($id) {
		$this->db->select('l.*');
		$this->db->from('lobby l');
		$this->db->where('l.id',$id);
		$query=$this->db->get();
		return $query->row();
	}
	public function duplicate_lobby_insert($lobby_id,$group_bet_id,$user_id,$table_cat) {
		$dt = array(
		    'bet_status' => 3,
		    'video_id' => '',
		    'is_active' => 0
		);
		$group_bt_update = $this->update_data('group_bet', $dt, array(
		    'id' => $group_bet_id
		));
		if ($group_bt_update) {
			$this->db->select('gb.*');
	        $this->db->from('group_bet gb');
	        $this->db->where('gb.id', $group_bet_id);
	        $this->db->where('gb.lobby_id', $lobby_id);
	        $this->db->where('gb.bet_status', 3);
	        $this->db->where('gb.is_active', 0);
	        $query              = $this->db->get();
	        $get_duplicate_data = $query->row();
	        
	        $get_data            = array(
	            'lobby_id' => $get_duplicate_data->lobby_id,
	            'game_id' => $get_duplicate_data->game_id,
	            'game_sub_id' => $get_duplicate_data->game_sub_id,
	            'challenger_table' => $get_duplicate_data->challenger_table,
	            'accepter_table' => $get_duplicate_data->accepter_table,
	            'winner_table' => '',
	            'loser_table' => '',
	            'bet_status' => 1,
	            'challenge_date' => $get_duplicate_data->challenge_date,
	            'accepte_date' => '0000-00-00 00:00:00',
	            'video_id' => '',
	            'is_active' => 1
	        );
	        $group_bet_insert_id = $this->insert_data('group_bet', $get_data);

	        $options = array(
                'select' => 'lg.*,ud.total_balance',
                'table' => 'user_detail ud',
                'join' => array(
                    'lobby_group lg' => 'lg.user_id = ud.user_id'
                ),
                'where' => array(
                    'lg.group_bet_id' => $group_bet_id,
                    'lg.status' => 1
                )
            );
            $lobby_bet_users     = $this->commonGet($options);

            if ($lobby_bet_users) {
	            foreach ($lobby_bet_users as $key => $lbusers) {
	                if ($lbusers->total_balance >= $lobby_bet[0]['price'] && $lbusers->total_balance != '0.00' && $lbusers->total_balance != '0') {
	                	if ($user_id != $lbusers->user_id) {
		                    $join_lobby_data = array(
		                        'group_bet_id' => $group_bet_insert_id,
		                        'table_cat' => $lbusers->table_cat,
		                        'user_id' => $lbusers->user_id,
		                        'is_creator' => $lbusers->is_creator,
		                        'is_controller' => $lbusers->is_controller,
		                        'stream_status' => $lbusers->stream_status,
		                        'stream_channel' => $lbusers->stream_channel,
		                        'status' => 1,
		                        'created_at' => date('Y-m-d H:i:s', time())
		                    );
		                    $this->General_model->insert_data('lobby_group', $join_lobby_data);
		                }
	                }
	            }
	        }   
		}
	}	
	public function get_join_detail($group_bet_id,$table_cat='',$user_id = ''){
		$user_id = ($user_id != '') ? $user_id : $this->session->userdata('user_id');

		$this->db->select('lg.*, ud.name, ud.user_id, ud.image, ud.display_name, ud.display_name_status, lf.stream_status, lf.stream_type, lf.stream_channel, lf.obs_stream_channel');
		$this->db->from('lobby_group lg');
		$this->db->join('user_detail ud','ud.user_id = lg.user_id');
		$this->db->join('group_bet gb','gb.id = lg.group_bet_id');
		$this->db->join('lobby_fans lf','lf.lobby_id = gb.lobby_id AND lf.user_id = lg.user_id');
		$this->db->where('lg.group_bet_id',$group_bet_id);
		$this->db->where('lg.user_id',$user_id);
		($table_cat != '') ? $this->db->where('lg.table_cat',$table_cat) : '';
		$this->db->where('gb.bet_status',1);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function get_join_group_data($lobby_id,$group_bet_id,$table_cat = ''){
		$this->db->select('lg.*, ud.name, ud.user_id, ud.total_balance, lf.fan_tag, lf.team_name');
		$this->db->from('lobby_group lg');
		$this->db->join('user_detail ud','ud.user_id=lg.user_id');
		$this->db->join('group_bet gb','gb.id=lg.group_bet_id');
		$this->db->join('lobby_fans lf','lf.user_id = ud.user_id AND lf.lobby_id = gb.lobby_id');
		$this->db->where('gb.lobby_id',$lobby_id);
		$this->db->where('lg.group_bet_id',$group_bet_id);
		(isset($table_cat) && $table_cat !='') ? $this->db->where('lg.table_cat',$table_cat) : '';
		$this->db->where('gb.bet_status',1);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function accepter_game_list() {
		$this->db->select('g.id,g.game_id,g.description,g.price,g.image_id,g.created_at,b.game_id,b.accepter_id,b.bet_status');
		$this->db->from('game g');
		$this->db->join('bet b','g.id=b.game_id');
		$this->db->where('b.accepter_id',$this->session->userdata('user_id'));
		$this->db->where('g.status',2);
		$query=$this->db->get();
		return $query->result_array();
	}

	public function accepter_old_game_list() {
		$this->db->select('g.id,g.game_id,g.description,g.price,g.created_at,g.image_id,ud.name,ud.user_id');
		$this->db->from('bet b');
		$this->db->join('game g','g.id=b.game_id');
		$this->db->join('user_detail ud','ud.user_id=b.accepter_id');
		$this->db->where('b.accepter_id',$this->session->userdata('user_id'));
		$this->db->where('g.status',3);
		$query=$this->db->get();
		return $query->result_array();
	}
	public function ordinal($number) {
	    $ends = array('th','st','nd','rd','th','th','th','th','th','th');
	    if ((($number % 100) >= 11) && (($number%100) <= 13))
	        return $number. '<lable class="ordinal_ch">th</lable>';
	    else
	        return $number.'<lable class="ordinal_ch">'.$ends[$number % 10].'</lable>';
	}

	public function watch_game_info($list_game_imp) {

		$this->db->select('g.id as game_tbl_id,g.status as game_status,g.price,g.sub_game_id, g.device_id,g.accepter_device_id,ag.id,ag.game_name,g.bet_id, b.challenger_id,b.accepter_id,g.status');

		$this->db->from('game g');

		$this->db->join('admin_game ag','ag.id=g.game_id');

		$this->db->join('bet b','b.id=g.bet_id');

		//$this->db->group_by('v.game_tbl_id');
		if($list_game_imp){
			$list_game_exp =explode(',',$list_game_imp);
			$this->db->where_not_in('g.id',$list_game_exp);
		}

		$this->db->order_by('g.created_at');

		$this->db->where('g.status',2);

		$query=$this->db->get();

		return $query->result_array();
	
	}
	public function watch_lobby_bet_info($list_game_imp) {

		$this->db->select('gb.id as group_bet_id,gb.status as group_bet_status,l.price,gb.sub_game_id,ag.id,ag.game_name,gb.lobby_id, gb.challenger_table,gb.accepter_table');

		$this->db->from('group_bet gb');

		$this->db->join('admin_game ag','ag.id=g.game_id');

		$this->db->join('bet b','b.id=g.bet_id');

		//$this->db->group_by('v.game_tbl_id');
		if($list_game_imp){
			$list_game_exp =explode(',',$list_game_imp);
			$this->db->where_not_in('g.id',$list_game_exp);
		}

		$this->db->order_by('g.created_at');

		$this->db->where('g.status',2);

		$query=$this->db->get();

		return $query->result_array();
	
	}

	public function url_info() {

		$this->db->select('v.game_tbl_id,g.status as game_status,g.price,g.device_id,g.accepter_device_id,GROUP_CONCAT(v.video_url SEPARATOR ",") as video_urls,v.game_sub_id,ud.user_id,ud.name,ag.id,ag.game_name,g.bet_id, GROUP_CONCAT(v.user_id SEPARATOR ", ") AS bet_user_id, GROUP_CONCAT(v.status SEPARATOR ", ") AS v_status, GROUP_CONCAT(v.detail_admin SEPARATOR "|") AS v_detail_admin,  GROUP_CONCAT(v.id SEPARATOR ", ") AS v_video_id, b.challenger_id,b.accepter_id');

		$this->db->from('video v');

		$this->db->join('user_detail ud','ud.user_id=v.user_id');

		$this->db->join('admin_game ag','ag.id=v.game_id');

		$this->db->join('game g','g.id=v.game_tbl_id');
		
		$this->db->join('bet b','b.id=g.bet_id');

		$this->db->group_by('v.game_tbl_id');

		$this->db->order_by('g.created_at');

		$this->db->where('g.status',2);

		$query=$this->db->get();

		
		return $query->result_array();
	
	}

	public function lobby_groupbeturl_info() {
		$this->db->select('l.is_archive,gb.lobby_id, lv.grp_bet_id, gb.bet_status as group_bet_status,l.price,gb.game_sub_id,gb.game_id,GROUP_CONCAT(DISTINCT(lv.video_url) SEPARATOR ",") as video_urls,ud.user_id,ud.name,ag.id,ag.game_name,gb.id, GROUP_CONCAT(DISTINCT(lv.user_id) SEPARATOR ", ") AS bet_user_id, GROUP_CONCAT(DISTINCT(lv.status) SEPARATOR ", ") AS lv_status, GROUP_CONCAT(DISTINCT(lv.detail_admin) SEPARATOR "|") AS lv_detail_admin,  GROUP_CONCAT(DISTINCT(lv.id) SEPARATOR ", ") AS lv_video_id, gb.challenger_table, gb.accepter_table, GROUP_CONCAT(DISTINCT(lg.win_lose_status) SEPARATOR ",") AS tables_win_lose_status, GROUP_CONCAT(DISTINCT(lg.reported) SEPARATOR ",") AS tables_reported, GROUP_CONCAT(DISTINCT(lg.table_cat) SEPARATOR ",") AS tables_cat');
		$this->db->from('group_bet gb');
		$this->db->join('lobby_video lv','lv.grp_bet_id = gb.id');
		$this->db->join('user_detail ud','ud.user_id = lv.user_id');
		$this->db->join('lobby_group lg','lg.user_id = ud.user_id');
		$this->db->join('lobby l','l.id = gb.lobby_id');
		$this->db->join('admin_game ag','ag.id = l.game_id');
		$this->db->group_by('gb.id');
		$this->db->order_by('lg.table_cat','ASC');
		$this->db->where('gb.bet_status',2);
		$this->db->where('l.is_archive',0);
		$query=$this->db->get();
		return $query->result_array();
	}

	public function lobby_report_status($id,$group_bet_id='') {
		$this->db->select('GROUP_CONCAT(DISTINCT(lg.reported) SEPARATOR ",") AS tables_reported, GROUP_CONCAT(DISTINCT(lg.table_cat) SEPARATOR ",") AS tables_cat, lg.id, lg.user_id, lg.win_lose_status');
		$this->db->from('group_bet gb');
		$this->db->join('lobby_video lv','lv.grp_bet_id = gb.id');
		$this->db->join('user_detail ud','ud.user_id=lv.user_id');
		$this->db->join('lobby_group lg','lg.user_id=ud.user_id');
		$this->db->join('lobby l','l.id=gb.lobby_id');
		$this->db->join('admin_game ag','ag.id=l.game_id');
		if ($group_bet_id !='') {
			$this->db->where('gb.id',$group_bet_id);
		}
		$this->db->where('gb.bet_status',2);
		$this->db->where('lg.group_bet_id',$id);
		$this->db->group_by('lg.id');
		$this->db->order_by('gb.challenge_date');
		$query=$this->db->get();
		return $query->result_array();
	}
	
	public function url_info_admin($id) {
		$this->db->select('GROUP_CONCAT(v.user_id SEPARATOR ", ") AS bet_user_id, GROUP_CONCAT(v.id SEPARATOR ", ") AS v_video_id');
		$this->db->from('video v');
		$this->db->where('v.game_tbl_id',$id);
		$query=$this->db->get();
		return $query->row();
	}
	
	public function lobby_report_timer() {
		$this->db->select('llrt.*');
		$this->db->from('live_lobby_report_timer llrt');
		$query=$this->db->get();
		return $query->row();
	}

	public function check_lobby_group_user_won_lose($group_bet_id) {
		$this->db->select('lg.*, lv.id as video_id');
		$this->db->from('group_bet gb');
		$this->db->join('lobby_group lg','lg.group_bet_id=gb.id');
		$this->db->join('lobby_video lv','lv.grp_bet_id=gb.id');
		$this->db->where('gb.id',$group_bet_id);
		$this->db->where('lg.win_lose_status !=',null);
		$this->db->where('gb.bet_status',1);
		$this->db->where('lg.user_id !=',$_SESSION['user_id']);
		$query=$this->db->get()->row();
		return $query;
	}
	
	public function check_user_won_lose_info($game_id) {

		$this->db->select('v.game_tbl_id,g.status as game_status,g.price,g.device_id,GROUP_CONCAT(v.video_url SEPARATOR ",") as video_urls,v.game_sub_id,ud.user_id,ud.name,ag.id,ag.game_name,g.bet_id, GROUP_CONCAT(v.user_id SEPARATOR ", ") AS bet_user_id, GROUP_CONCAT(v.status SEPARATOR ", ") AS v_status,  GROUP_CONCAT(v.id SEPARATOR ", ") AS v_video_id');

		$this->db->from('video v');

		$this->db->join('user_detail ud','ud.user_id=v.user_id');

		$this->db->join('admin_game ag','ag.id=v.game_id');

		$this->db->join('game g','g.id=v.game_tbl_id');

		$this->db->group_by('v.game_tbl_id');

		$this->db->order_by('g.created_at');

		$this->db->where('g.id',$game_id);
		
		$this->db->where('g.status',2);

		$query=$this->db->get();

		return $query->row();
	
	}

	public function withdraw_requests() {
		$this->db->select('u.email as user_email,w.paid_to_email,w.id,w.points,w.amount_requested,w.amount_paid,w.req_date,w.paid_date,w.status,ud.name,ud.total_balance,ud.user_id,ud.number,ud.account_no');
		$this->db->from('withdraw_request w');
		$this->db->join('user_detail ud','ud.user_id=w.user_id');
		$this->db->join('user u','u.id=ud.user_id');
		$this->db->where('w.status','0');
		$this->db->order_by('w.req_date',DESC);
		$query=$this->db->get();
		return $query->result_array();
	}

	public function withdraw_paid_requests() {
		$this->db->select('u.email as user_email,w.id,w.points,w.paid_to_email,w.collected_fees,w.amount_requested,w.amount_paid,w.req_date,w.paid_date,w.status,ud.name,ud.total_balance,ud.user_id,ud.number,ud.account_no');
		$this->db->from('withdraw_request w');
		$this->db->join('user_detail ud','w.user_id=ud.user_id');
		$this->db->join('user u','u.id=ud.user_id');
		$this->db->where('w.status','1');
		$this->db->where('w.reset','1');
		$this->db->order_by('w.paid_date',DESC);
		$query=$this->db->get();
		return $query->result_array();
	}

	public function paid_deposit() {

		$this->db->select('p.id,p.points,p.collected_fees,p.payment_date as paid_date,p.package_id,ud.name,ud.total_balance,ud.user_id');	
		$this->db->from('payment p');

		$this->db->join('user_detail ud','ud.user_id=p.user_id');
		
		$this->db->where('p.payment_status','completed');

		$this->db->where('p.reset','1');

		$query=$this->db->get();

		return $query->result_array();
	}	

	public function challange_not_accepted() {
		$this->db->select('b.id,ag.game_name,g.price,ud.name,b.is_accept');

		$this->db->from('bet b');

		$this->db->join('game g','g.bet_id=b.id');

		$this->db->join('admin_game ag','ag.id=b.game_id');

		$this->db->join('user_detail ud','ud.user_id=b.challenger_id');

		$this->db->where('b.is_accept',0);

		$this->db->order_by('ag.game_name', 'ASC');

		$query=$this->db->get();

		return $query->result_array();
	}

	public function video_not_uploaded() {
		$this->db->select('b.id,ag.game_name,ud.name,g.video_id');

		$this->db->from('bet b');

		$this->db->join('game g','g.bet_id=b.id');

		$this->db->join('admin_game ag','ag.id=b.game_id');

		$this->db->join('user_detail ud','ud.user_id=b.challenger_id');

		$this->db->where('g.video_id',0);

		$query=$this->db->get();

		return $query->result_array();
	}

	public function uploaded_videos() {

		$this->db->select('`g.bet_id`,,`v.game_tbl_id`,`g.status` as `game_status`,`g.device_id`,v.video_url as video_urls, v.id as `video_id`, `v.game_sub_id`,`v.lock`,`ag.game_name`,`ud.user_id`,`ud.name`');
	
		//$this->db->select('`g.bet_id`,`g.status` as `game_status`,`g.device_id`,GROUP_CONCAT(v.video_url SEPARATOR ",") as video_urls, GROUP_CONCAT(v.id SEPARATOR ",") as `video_id`, `v.game_sub_id`,`ud.user_id`,`ud.name`,`ag.game_name`');

		$this->db->from('video v');

		$this->db->join('bet b','b.winner_id=v.user_id');
		
		$this->db->join('user_detail ud','ud.user_id=v.user_id');

		$this->db->join('admin_game ag','ag.id=v.game_id');

		$this->db->join('game g','g.id=v.game_tbl_id');

		$this->db->where('g.video_id = v.id');
		
		$this->db->group_by('g.bet_id');

		$this->db->where('v.video_url !=""');
		
		$this->db->where('v.lock','1');
		
		$this->db->order_by('g.created_at');
		
		$this->db->limit('8');

		$query=$this->db->get();

		return $query->result_array();

	}
	
	
	public function unlock_uploaded_videos($id='') {

		$this->db->select('`g.bet_id`,,`v.game_tbl_id`,`g.status` as `game_status`,`g.device_id`,v.video_url as video_urls, v.id as `video_id`, `v.game_sub_id`,`v.lock`,v.add_time,`ag.game_name`,ag.game_description,`ud.user_id`,`ud.name`,g.accepter_device_id');
	
		//$this->db->select('`g.bet_id`,`g.status` as `game_status`,`g.device_id`,GROUP_CONCAT(v.video_url SEPARATOR ",") as video_urls, GROUP_CONCAT(v.id SEPARATOR ",") as `video_id`, `v.game_sub_id`,`ud.user_id`,`ud.name`,`ag.game_name`');

		$this->db->from('video v');
		
		$this->db->join('bet b','b.winner_id=v.user_id');

		$this->db->join('user_detail ud','ud.user_id=v.user_id');

		$this->db->join('admin_game ag','ag.id=v.game_id');

		$this->db->join('game g','g.id=v.game_tbl_id');
		
		$this->db->where('g.video_id = v.id');

		$this->db->group_by('g.bet_id');

		$this->db->where('v.video_url !="" ');
		
		$this->db->where('v.lock','0');
		
		$this->db->where('g.status',3);
		
		$this->db->order_by('g.created_at');
		if ($id != '') {
			$this->db->limit($id);
		}
		$query=$this->db->get();

		return $query->result_array();

	}
	
	
	public function uploaded_videos_details($id) {

		$this->db->select('`g.bet_id`,`v.game_tbl_id`,`g.status` as `game_status`,`g.device_id`,v.video_url as video_urls, v.id as `video_id`, `v.game_sub_id`,`ag.game_name`,`ud.user_id`,`ud.name`');
	
		//$this->db->select('`g.bet_id`,`g.status` as `game_status`,`g.device_id`,GROUP_CONCAT(v.video_url SEPARATOR ",") as video_urls, GROUP_CONCAT(v.id SEPARATOR ",") as `video_id`, `v.game_sub_id`,`ud.user_id`,`ud.name`,`ag.game_name`');

		$this->db->from('video v');

		$this->db->join('user_detail ud','ud.user_id=v.user_id');

		$this->db->join('admin_game ag','ag.id=v.game_id');

		$this->db->join('game g','g.id=v.game_tbl_id');

		//$this->db->group_by('v.user_id','g.id');

		$this->db->where('v.id',$id);
		
		$this->db->where('g.status',3);
		
		$this->db->where('v.video_url !="" ');
		
		$this->db->order_by('g.created_at');
		
		//$this->db->limit('10');

		$query=$this->db->get();

		return $query->row();

	}

	function video_detail() {
		$this->db->select('v.id,v.game_id,v.video_url,g.name');
		$this->db->from('video v');
		$this->db->join('game g', 'v.game_id=g.id');
		$this->db->where('v.user_id', $this->session->userdata('user_id'));
		$query = $this->db->get()->result();
		return $query;
	}

	function getvideos($id='',$is_display='',$is_home='') {
		$this->db->select('v.*');
		$this->db->from('videos v');
		($id != '') ? $this->db->where('v.id',$id) : '';
		($is_display != '') ? $this->db->where('v.is_display',$is_display) : '';
		($is_home !='') ? $this->db->where('v.is_home',$is_home) : '';
		$this->db->order_by('v.order_by');
		$query = $this->db->get()->result();
		return $query;		
	}
	function get_username($id){
		$this->db->select('*');
		$this->db->from('user_detail');
		$this->db->where('user_id',$id);
		$query=$this->db->get();
		$result =  $query->row();
		if(!empty($result)){
			return $result->name;
		} else {
			return '';
		}
	}
	
	function get_past_withdrawal_data() {
		$options             = array(
            'select' => 'wr.req_date',
            'table' => 'withdraw_request wr',
            'where' => array(
            	'wr.user_id' => $_SESSION['user_id'],
                'wr.status' => 0
            ),
            'order' => array('wr.req_date' => 'desc'),
            'single' => true
        );
        $my_lobby_table = $this->commonGet($options)->req_date;
        return $my_lobby_table;
	}

	public function search_game_view_data($table_name,$condition)	{
		$this->db->where($condition);
		$this->db->order_by('game_name','asc');
		$query = $this->db->get($table_name);
		return $query->result_array();
	}

	public function get_game_view_data($table_name,$condition)	{
		$this->db->where($condition);
		$this->db->order_by('game_name','asc');
		$query = $this->db->get($table_name);
		return $query->result_array();
	}
	
	public function get_admin_data($id){
		$this->db->select('*');
		$this->db->from('video');
		$this->db->where('id',$id);
		$query=$this->db->get();
		$result =  $query->row();
		if(!empty($result)){
			return $result->detail_admin;
		} else {
			return '';
		}
	}

	public function get_lobby_admin_data($id){
		$this->db->select('*');
		$this->db->from('lobby_video');
		$this->db->where('id',$id);
		$query=$this->db->get();
		$result =  $query->row();
		if(!empty($result)){
			return $result->detail_admin;
		} else {
			return '';
		}
	}
	public function get_my_lobby_table($lobby_id,$group_bet_id,$user_id){
		$options             = array(
            'select' => 'lg.table_cat',
            'table' => 'lobby_group lg',
            'join' => array(
                'group_bet gb' => 'gb.id = lg.group_bet_id'
            ),
            'where' => array(
            	'gb.lobby_id' => $lobby_id,
            	'lg.user_id' => $user_id,
                'lg.group_bet_id' => $group_bet_id,
                'lg.status' => 1
            ),
            'single' => true,
        );
        $my_lobby_table = $this->commonGet($options)->table_cat;
        return $my_lobby_table;
	}
	public function get_lobby_vids_data($cat,$lobby_id,$grp_bet_id){
		$this->db->select('lv.video_url, lv.user_id, lv.id, lv.detail_admin, lv.table_cat');
		$this->db->from('lobby_video lv');
		$this->db->join('lobby_group lg','lg.group_bet_id = lv.grp_bet_id');
		$this->db->join('group_bet gb','gb.id = lv.grp_bet_id');
		$this->db->where('lv.lobby_id',$lobby_id);
		$this->db->where('lv.table_cat',$cat);
		$this->db->where('gb.bet_status',2);
		$this->db->where('lv.grp_bet_id',$grp_bet_id);
		$this->db->group_by('lg.table_cat');
		$query_data=$this->db->get()->row();
		return $query_data;
	}
	public function get_report_tables($lobby_id,$grp_bet_id){
		$this->db->select('lg.table_cat');
		$this->db->from('lobby_video lv');
		$this->db->join('lobby_group lg','lg.group_bet_id = lv.grp_bet_id');
		$this->db->join('group_bet gb','gb.id = lv.grp_bet_id');
		$this->db->where('lv.lobby_id',$lobby_id);
		$this->db->where('lg.reported',1);
		$this->db->where('gb.bet_status',2);
		$this->db->where('lv.grp_bet_id',$grp_bet_id);
		$this->db->group_by('lg.table_cat');
		$query_data = $this->db->get()->result();
		return $query_data;
	}
	public function lobby_url_info_admin($id){
		$this->db->select('GROUP_CONCAT(lg.table_cat SEPARATOR ", ") AS bet_user_table, GROUP_CONCAT(lv.user_id SEPARATOR ", ") AS bet_user_id, GROUP_CONCAT(lv.id SEPARATOR ", ") AS lv_video_id');
		$this->db->from('lobby_video lv');
		$this->db->join('lobby_group lg','lg.user_id=lv.user_id');
		$this->db->where('lv.grp_bet_id',$id);
		$query=$this->db->get();
		return $query->row();
	}
	public function getYoutubeEmebedUrl($url,$autoplay) {
		$embedeurl['embeded_iframe'] = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe src=\"//www.youtube.com/embed/$1?rel=0&mute=1&autoplay=".$autoplay."&enablejsapi=1&showinfo=0&loop=0&playlist=$1\" width=\"100%\" height=\"550\" frameborder=\"0\"></iframe>",$url);
		$embedeurl['url'] = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","https://www.youtube.com/embed/$1",$url);
		return $embedeurl;
	}

	// MATCHES TAB
	public function matches_system_list()	{
		$this->db->select('acg.*');
		$this->db->from('admin_game_category acg');
		$this->db->where('acg.custom',null);
		$this->db->where('acg.is_deleted',0);
		$this->db->order_by('display_order','asc');
		$query = $this->db->get();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
		    foreach ($query->result_array() as $row) {
		        $data[] = $row;
		    }
		}
		return $data;
	}

	public function matches_custsyslist_list()	{
		$this->db->select('acg.*');
		$this->db->from('admin_game_category acg');
		$this->db->where('acg.custom','custom_lobby');
		$this->db->where('acg.is_deleted',0);
		$query=$this->db->get();
		// return $query->result_array();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
		    foreach ($query->result_array() as $row) {
		        $data[] = $row;
		    }
		}

		return $data;
	}	

	public function matches_game_name_list($game_category_id='')	{
		$this->db->select('ag.id,ag.game_name,ag.game_description,ag.created_at');
		$this->db->from('admin_game ag');
		$this->db->join('admin_game_category c','c.id=ag.game_category_id');
		if ($game_category_id !='') {
			$this->db->where('ag.game_category_id',$game_category_id);
		}
		$this->db->where('ag.custom',null);
		$this->db->group_by('ag.game_name');
		$query=$this->db->get();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
		    foreach ($query->result_array() as $row) {
		        $data[] = $row;
		    }
		}

		return $data;
		// return $query->result_array();
	}
	public function matches_sub_game_name_list($admin_game_id='')	{
		$this->db->select('asg.id as asgid, asg.*');
		$this->db->from('admin_sub_game asg');
		$this->db->where('asg.custom',null);
		if ($admin_game_id !='') {
			$this->db->where('asg.game_id',$admin_game_id);
		}
		$this->db->group_by('asg.sub_game_name');
		$query=$this->db->get();
		// return $query->result_array();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
		    foreach ($query->result_array() as $row) {
		        $data[] = $row;
		    }
		}

		return $data;
	}
	public function matches_game_size()	{
		$this->db->select('g.*');
		$this->db->from('game g'); 
		$this->db->group_by('g.game_type');
		$query=$this->db->get();
		// return $query->result_array();
		$data = array();
		if($query !== FALSE && $query->num_rows() > 0){
		    foreach ($query->result_array() as $row) {
		        $data[] = $row;
		    }
		}

		return $data;
	}
	public function banner_order() {
  	$this->db->select('b.*');
  	$this->db->from('banner b');
		$this->db->order_by('banner_order','asc');
		$this->db->where('banner_order !=', 0, FALSE);
		$this->db->limit(10);
		$query = $this->db->get();
		$data = array();
		if ($query !== FALSE && $query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}
	public function tab_banner() {
    $this->db->select('b.*');
    $this->db->from('tab_banner b');
		$this->db->order_by('banner_order','asc');
		$this->db->where('banner_order !=', 0, FALSE);
		$this->db->limit(6);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function chat_banner() {
    $this->db->select('b.*');
    $this->db->from('chat_banner b');
		$this->db->order_by('banner_order','asc');
		$this->db->where('banner_order !=', 0, FALSE);
		$this->db->limit(6);
    $query = $this->db->get();
    return $query->result_array();
  }
  public function friendlist($user_id,$srch_plyr='') {
    $this->db->select('u.email, ud.*, fl.*');
    $this->db->from('user u');
    $this->db->join('user_detail ud','ud.user_id=u.id');
    $this->db->join('friend_list fl','fl.friend_id=ud.user_id');
    $this->db->where('fl.request','0');
    $this->db->where('fl.user_id',$user_id);
    $this->db->where_not_in('fl.friend_id',$_SESSION['user_id']);
    if ($srch_plyr != '') {
    	(is_numeric($srch_plyr)) ? $this->db->where("ud.account_no LIKE '".$srch_plyr."'") : $this->db->where("(ud.name LIKE '%".$srch_plyr."%' OR ud.display_name LIKE '%".$srch_plyr."%')");
		}
    $query = $this->db->get();
    return $query->result_array();
  }
  public function challengelist($user_id) {
	  $this->db->select('u.*, ud.*, fl.*');
	  $this->db->from('user u');
	  $this->db->join('user_detail ud','ud.user_id=u.id');
	  $this->db->join('friend_list fl','fl.friend_id=ud.user_id');
	  $this->db->where('fl.request','0');
	  $this->db->where('fl.user_id',$user_id);
	  $query = $this->db->get();
	  return $query->result_array();
	}
  public function count_unread_msg($user_id) {
	  $this->db->select('mc.*');
	  $this->db->from('mail_data md');
	  $this->db->join('mail_conversation mc','mc.message_id=md.id');
	  $this->db->where('mc.user_id',$user_id);
	  $this->db->where('mc.user_to',$_SESSION['user_id']);
	  $this->db->where('mc.direction','OUT');	
	  $this->db->where('mc.mail_type','friends_mail');
	  $query=$this->db->get();
	  return $query->result_array();
	}
	public function count_challenge_unread_msg($user_id) {
	  $this->db->select('mc.*');
	  $this->db->from('mail_data md');
	  $this->db->join('mail_conversation mc','mc.message_id=md.id');
	  $this->db->where('mc.user_id',$user_id);
	  $this->db->where('mc.user_to',$_SESSION['user_id']);
	  $this->db->where('mc.direction','OUT');
	  $this->db->where('mc.mail_type','challenge_mail');
	  $query=$this->db->get();
	  return $query->result_array();
	}
	public function find_friendlist($user_id,$srch_plyr='') {
		$this->db->select('ud.user_id,ud.name,ud.image,ud.team_name,ud.display_name_status,ud.display_name,CONCAT("0","_","3") as friend_id');
		$this->db->from("user_detail ud");
		$this->db->where('ud.user_id !=',$user_id);
		if ($srch_plyr != '') {
			(is_numeric($srch_plyr)) ? $this->db->where("ud.account_no LIKE '".$srch_plyr."'") : $this->db->where("(ud.name LIKE '%".$srch_plyr."%' OR ud.display_name LIKE '%".$srch_plyr."%')");
		}
		$query1 = $this->db->get_compiled_select();
    $this->db->select("0 as user_id,'' as name,'' as image,'' as team_name,'' as display_name_status,'' as display_name,CONCAT(fl.user_id,'_',fl.request) as friend_id");
    $this->db->from('friend_list fl');
    $this->db->where('fl.friend_id',$user_id);
    $query2 = $this->db->get_compiled_select();

    $query = $this->db->query($query1." UNION ALL ".$query2);
    return $query->result();
  }
  function remove_element($array,$value) {
  	foreach (array_keys($array, $value) as $key) {
			unset($array[$key]);
		}  
		return $array;
	}
    public function pendingrequest_friendlist($user_id,$srch_pending_plyr='') {
    	$this->db->select('ud.*, fl.*');
			$this->db->from('friend_list fl');
			$this->db->join('user_detail ud','ud.user_id = fl.friend_id');
			$this->db->where('fl.request','1');
			$this->db->where('fl.user_id',$user_id);
			$this->db->where('ud.user_id !=',$user_id);
			if ($srch_pending_plyr != '') {
				(is_numeric($srch_pending_plyr)) ? $this->db->where("ud.account_no LIKE '".$srch_pending_plyr."'") : $this->db->where("(ud.name LIKE '%".$srch_pending_plyr."%' OR ud.display_name LIKE '%".$srch_pending_plyr."%')");
			}
			$query = $this->db->get();
			return $query->result_array();
    }
    public function friend_profile($user_id) {
      $this->db->select('u.email, u.ban_time, u.ban_option, u.id, ud.*');
      $this->db->from('user u');
      $this->db->join('user_detail ud','ud.user_id=u.id');
      $this->db->where('u.id',$user_id);
      $query = $this->db->get()->row();
      return $query;
    }        
    public function get_all_unread_mails($user_id) {
    	$this->db->select('*');
        $this->db->from('mail_data md');
        $this->db->join('mail_conversation mc','mc.message_id=md.id');
        $this->db->join('user_detail ud','ud.user_id=mc.user_to');
        $this->db->where('mc.user_id',$user_id);
        $this->db->where('md.status', 1);
        $this->db->where('mc.direction','IN');
        $this->db->order_by('md.id','desc');
        $rows = $this->db->get()->result_array();
        return $rows;
    }
 	public function get_mails($user_id){
 		$this->db->select('*');
        $this->db->from('mail_data md');
        $this->db->join('mail_conversation mc','mc.message_id=md.id');
        $this->db->where('mc.user_id',$user_id);
        $this->db->where('mc.user_to',$_SESSION['user_id']);
        $this->db->where('mc.direction','OUT');
        $this->db->where('mc.mail_type','friends_mail'); 
        $this->db->where('mc.user_from',$user_id);
        $rows=$this->db->get()->result_array();
        return $rows;
    }  
    public function online_friendlist($user_id){
    	$this->db->select('fl.friend_id, u.*, ud.*');
    	$this->db->from('friend_list fl');
    	$this->db->join('user u','u.id=fl.friend_id');
    	$this->db->join('user_detail ud','ud.user_id=u.id');
    	$this->db->where('fl.user_id =',$user_id);
    	$this->db->where('u.logged_in =','1');
    	$online_friend_array = $this->db->get()->result_array();
    	return $online_friend_array;
    }
    public function get_ads_detail($ads_cat='',$limit='',$ads_type='',$product_id=''){
    	$this->db->select('at.*');
    	$this->db->from('ads_tbl at');
        ($ads_cat != '') ? $this->db->where('at.ads_type =',$ads_cat) : '';
        if ($ads_type == 'vid') {
        	$this->db->where('(at.custom_commercial IS NOT NULL AND at.custom_commercial !="") OR (at.youtube_link IS NOT NULL AND at.youtube_link !="")');
        } else if ($ads_type == 'img') {
        	$this->db->where('at.upload_sponsered_by_logo IS NOT NULL');
        	$this->db->where('at.upload_sponsered_by_logo !=','');
        } else if ($product_id !='') {
        	$this->db->where('FIND_IN_SET('.$product_id.', at.product_id)');
        }
    	$this->db->order_by('id','random');
        ($limit != '') ? $this->db->limit($limit) : $this->db->limit(1);
    	$res = $this->db->get()->result();
    	return $res;
    }
    public function lobby_chat($id){
    	$options = array(
        'select' => 'lgp.*,xm.message,xm.attachment,xm.message_type,ud.name,ud.image,ud.is_admin,lf.status as fan_status, lf.mute_time',
        'table' => 'lobby_group_conversation lgp',
        'join' => array('xwb_messages xm' => 'xm.id = lgp.message_id','user_detail ud' => 'ud.user_id = lgp.user_id', 'lobby_fans lf' => 'lf.user_id = lgp.user_id'),
        'where' => array('lgp.lobby_id'=>$id,'lf.lobby_id'=>$id,'lgp.status' => 1,'lf.status' => 1),
        'order' => array('lgp.date' => 'asc')
        );
        return $this->commonGet($options);
    }

    public function paypal_fallback_data($value, $call) {
    	$path = BASEPATH.'../paypal_data/';
    	$today_date = date('m-d-Y', time());
		$date1 = str_replace('-', '/', $today_date);
		$tomorrow = date('m-d-Y',strtotime($date1 . "-1 days"));
		
		$tomorrow_file = $path.$tomorrow.'_paypal.php';
    	$today_file = $path.$today_date.'_paypal.php';
		
		// if (file_exists($tomorrow_file)) {
  //   		unlink($tomorrow_file);
		// }

    	if (file_exists($today_file)) {
    		write_file($today_file, "\n\nUsers Browser\n\n".$this->agent->browser()."\n\n", 'a');
    		write_file($today_file, "\n\n".$call, 'a');
    		write_file($today_file, $value, 'a');
    		write_file($today_file, "\n\n", 'a');
    	} else {
    		write_file($today_file, "\n\nUsers Browser\n\n".$this->agent->browser()."\n\n", 'a');
    		write_file($today_file, $call, 'a');
			write_file($today_file, $value, 'a');
			write_file($today_file, "\n\n", 'a');
    	}
    }


	function sendMail($to,$name,$subject,$message) {
		$post_data = array(
			'from' => array('noreply@ProEsportsGaming.com','Pro Esports Gaming'),
			'to' => array($to => $name),
			"cc" => array("mehul@aspireedge.com" => "Mehul Aspireedge"),
			'subject' => $subject,
			'html' => $message
		);

		$post_data = json_encode($post_data);
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, 'https://api.sendinblue.com/v2.0/email');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = 'Api-Key: a1wdYKGbI0pSUscA';
		$headers[] = 'Content-Type: application/x-www-form-urlencoded';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
			
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);
		$final_result = json_decode($result);
		
		if($final_result->code == 'success'){
			return 1;
		} else {
			return 0;
		}
	}
	public function getsubscribedlist($data) {
        $user_from = $data['user_from'];
        $user_to = $data['user_to'];
        $single = $data['single'];
        $is_history = $data['is_history'];
        $subscribe_status = $data['subscribe_status'];
        $transaction_id = $data['transaction_id'];
        $query = $this->db->select('ps.*, ud.*');
    	$query += $this->db->from('paypal_subscriptions ps');
    	$query += $this->db->join('user_detail ud','ud.user_id = ps.user_to');
    	$query += ($user_from) ? $this->db->where('ps.user_from',$user_from) : '';
    	$query += ($user_to) ? $this->db->where('ps.user_to',$user_to) : '' ;
    	$query += ($subscribe_status == 1) ? $this->db->where('ps.subscribe_status',1) : $this->db->where('ps.subscribe_status',0);
    	$query += ($is_history == 1) ? $this->db->where('ps.is_history', 1) : $this->db->where('ps.is_history', 0);
    	$query += ($transaction_id != NULL) ? $this->db->where('ps.transaction_id', $transaction_id) : '';
    	if($single)
    		$data = $this->db->get()->row();
    	else
    		$data = $this->db->get()->result();
    	return $data;
	}
	public function subscribedlist($user_id) {
		$this->db->select('ps.*, ud.*, psh.payment_id');
		$this->db->from('paypal_subscriptions ps');
		$this->db->join('user_detail ud','ud.user_id = ps.user_to');
		$this->db->join('paypal_subscriptions_history psh','psh.payment_id = ps.transaction_id','left outer');
		($user_id != '') ? $this->db->where('ps.user_from',$user_id) : '';
		$this->db->where('ps.subscribe_status',1);
		$this->db->where('ps.is_history',0);
	  	$query = $this->db->get();
	  	$result = $query->result();
	    return $result;
  	}
  	public function waitinglist($lobby_id='',$table='',$user_id='',$is_waiting='',$numrow='') {
		$this->db->select('lf.id as fan_id, lf.user_id, lf.lobby_id, lf.fan_tag, lf.is_waiting, lf.waiting_table as table_cat, lf.obs_stream_channel, lf.stream_type, lf.stream_status, lf.stream_channel, ud.*, lf.team_name, ud.id as user_detail_id, l.id as lobby_id, l.user_id as creator_id');
		// , COUNT(fu.id) as followercount
		$this->db->from('lobby_fans lf');
		$this->db->join('user_detail ud','ud.user_id = lf.user_id');
		// $this->db->join('follower_users fu','fu.following_id = lf.user_id', 'right');
		$this->db->join('lobby l','l.id = lf.lobby_id');
		($lobby_id != '') ? $this->db->where('lf.lobby_id',$lobby_id) : '';
		($table != '') ? $this->db->where('lf.waiting_table',$table) : '';
		($is_waiting != '') ? $this->db->where('lf.is_waiting',$is_waiting) : '';
		($user_id !='') ? $this->db->where('lf.user_id',$user_id) : '';
			$this->db->where('l.status',1);
	  	$query = $this->db->get();
	  	$result = ($numrow == 'single') ? $query->row() : $query->result();
	  	return $result;
	}
	public function subscribedbylist($user_id) {
	  	$options = array(
	  		'select' => 'ps.*, ud.*',
	      	'table' => 'paypal_subscriptions ps',
	      	'join' => array('user_detail ud' => 'ud.user_id = ps.user_to', 'user u' => 'u.id = ud.user_id'),
	      	'where' => array('ps.user_to'=>$user_id),
	    );
	  	return $this->commonGet($options);
	}
	function Manage_recurring_payments_profile_status($payment_id,$subscription_opt) {
        $MRPPSFields = array(
                        'profileid' => $payment_id, // Required. Recurring payments profile ID returned from CreateRecurring...
                        'action' => $subscription_opt, // Required. The action to be performed.  Mest be: Cancel, Suspend, Reactivate
                        'note' => '' // The reason for the change in status.  For express checkout the message will be included in email to buyers.  Can also be seen in both accounts in the status history.,
                        );
                        
        $PayPalRequestData = array('MRPPSFields' => $MRPPSFields);
        
        $PayPalResult = $this->paypal_pro->ManageRecurringPaymentsProfileStatus($PayPalRequestData);
        if (!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK'])) {
            $errors = array('Errors'=>$PayPalResult['ERRORS']);
        }
    }
    function get_recurring_payments_profile_details($payment_id='') {
        if (!empty($_GET['payment_id'])) {
            $payment_id = $_GET['payment_id'];
        }
        $GRPPDFields = array(
            'profileid' => $payment_id
        );
        $PayPalRequestData = array('GRPPDFields' => $GRPPDFields);
        
        $PayPalResult = $this->paypal_pro->GetRecurringPaymentsProfileDetails($PayPalRequestData);
        
        if(!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK'])) {
            $errors = array(
            	'Errors' => $PayPalResult['ERRORS']
            );
        } else {
            return $PayPalResult;
        }
    }
    function get_subscriptionlist_adminside($profileid = '') {
    	if ($profileid !='') {
    		$this->db->select('psb.*,psbh.*');
    		$this->db->from('paypal_subscriptions_history psbh');
    		$this->db->join('paypal_subscriptions psb','psb.payment_id=psbh.payment_id');
	    	$this->db->where('psbh.payment_id =',$profileid);
    	} else {
	    	$this->db->select('psb.*');
	    	$this->db->from('paypal_subscriptions psb');
    	}
    	$res = $this->db->get();
    	return $res->result_array();
    }

    function get_mytradelist($user_id='',$option='',$order_by='') {
			$this->db->select('th.*, ud.name, ud.image, ud.display_name, ud.team_name, ud.account_no');
			$this->db->from('trade_history th');
			if ($option == 'sent') {
				$this->db->join('user_detail ud','ud.user_id = th.trade_id');
	    	$this->db->where('th.user_id',$user_id);
	  	} 
	  	if ($option == 'receive') {
	  		$this->db->join('user_detail ud','ud.user_id = th.user_id');
	  		$this->db->where('th.trade_id',$user_id);
	  	}
	  	if ($order_by != '') {
	  		$this->db->order_by($order_by, 'DESC');
	  	}
	  	$res = $this->db->get();
	  	return $res->result();
    }
    function get_tip_history($user_id = '',$where,$join_field){
    	$this->db->select('th.*, ud.name, ud.image, ud.display_name, ud.team_name, ud.account_no');
			$this->db->from('tip_history th');
			$this->db->join('user_detail ud','ud.user_id = '.$join_field);
	    $this->db->where($where,$user_id);
    	$this->db->order_by('th.created', 'DESC');
    	
    	$res = $this->db->get();
    	return $res->result();
    }
    function get_stream_setting($user_id='',$order_by='',$key_option='') {
		$this->db->select('ss.*, ud.name, ud.image, ud.display_name, ud.team_name, ud.account_no');
		$this->db->from('stream_settings ss');
		$this->db->join('user_detail ud','ud.user_id = ss.user_id');
		$this->db->where('ss.user_id',$user_id);
		($order_by != '') ? $this->db->order_by($order_by, 'DESC') : '';
		($key_option !='') ? $this->db->where('ss.key_option',$key_option) : '';
	  	$res = $this->db->get();
	  	return $res->result();
    }
    function get_user_img($user_id='') {
    	$img = 'default_profile_img.png';
    	if ($user_id != '') {
    		$data = $this->User_model->get_user_detail($user_id);
    		if ($data->image != '' && file_exists(BASEPATH.'../upload/profile_img/'.$data->image)) {
    			$img = $data->image;
    		}
    	}
		return $img;
    }

    function getmytipicon($user_id='',$order_by='') {
		$this->db->select('ss.*, ud.name, ud.image, ud.display_name, ud.team_name, ud.account_no');
		$this->db->from('stream_settings ss');
		$this->db->join('user_detail ud','ud.user_id = ss.user_id');
    	$this->db->where('ss.user_id',$user_id);
    	if ($order_by != '') {
    		$this->db->order_by($order_by, 'DESC');
    	}
    	$res = $this->db->get()->row()->tip_selicon_img;
    	if (count($res) == 0) {
    		$res = 'tip.png';
    	}
    	return $res;
    }
    public function security_pw_auth($security_password='',$dataid=''){
    	$security_password_db = $this->General_model->view_all_data('admin_security_password','id','asc');
    	if($security_password == $security_password_db[0]['password']){
        	$result['data_id'] = $dataid;
        } else {
        $data_value = '<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form action="#" id="security_id" name="security_id" method="POST">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Admin Password</label>
                                <input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
                                <input type="hidden" id="userid" class="form-control" name="userid" value="'.$dataid.'" required>
                            </div>
                        </div>
                        <div class="box-footer">
                            <input class="btn btn-primary" type="submit" value="Submit" >
                        </div>
                    </form>
                </div>
            </div>          
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>      
            </div>';
        $result['data_value'] = $data_value;
        }
        return $result;
    }
    public function get_chat_file($arr) {
    	$this->db->select('ss.*, ud.name, ud.image, ud.display_name, ud.team_name, ud.account_no');
		$this->db->from('stream_settings ss');
		$this->db->join('user_detail ud','ud.user_id = ss.user_id');
		if ($arr['user_id']) {
    		$this->db->where('ss.user_id',$arr['user_id']);
		}
    	if ($arr['file_name']) {
    		if ($arr['key_type'] == 'tip_icon') {
    			$this->db->where("ss.tip_icon_imgs LIKE '%".$arr['file_name']."%'");
    		} else if ($arr['key_type'] == 'tip_sound' || $arr['key_type'] == 'sub_sound') {
    			$this->db->where("ss.sound_audios LIKE '%".$arr['file_name']."%'");
    		}
    	}
    	$res = $this->db->get()->result();
    	return $res;
    }
    public function get_ticket_detail($arr) {
    	$this->db->select('eu.type as ticket_type, eu.lobby_id as ticket_lby_id, eu.user_id as ticket_user_id, eu.is_gift as ticket_is_gift, eu.gift_to as ticket_is_gift_to, ud.name, ud.display_name, ud.team_name, ud.account_no, l.is_event as lobby_is_event, l.is_spectator as lobby_is_spectator, l.event_price, l.event_image, l.spectate_price');
		$this->db->from('events_users eu');
		$this->db->join('user_detail ud','ud.user_id = eu.user_id');
		$this->db->join('lobby l','l.id = eu.lobby_id');
		(!empty($arr['is_remove'])) ? $this->db->where('eu.is_remove',$arr['is_remove']): $this->db->where('eu.is_remove',0);
		(!empty($arr['user_id'])) ? $this->db->where('eu.user_id',$arr['user_id']):'';
		(!empty($arr['lobby_id'])) ? $this->db->where('eu.lobby_id',$arr['lobby_id']):'';
		(!empty($arr['type'])) ? $this->db->where('eu.type',$arr['type']):'';
		(!empty($arr['is_gift'])) ? $this->db->where('eu.is_gift',$arr['is_gift']):'';
		$res = $this->db->get()->result();
    	return $res;
    }
    //get member_list api
    //get all user of web
     function get_all_membership() {
        $this->db->select('id,display_order,title,time_duration,image,amount,fees,status,start_date,expire_date');
        $this->db->from('membership');
         $query = $this->db->get();
        $res_arr = $query->result_array();
        return $res_arr;
    }
}


 