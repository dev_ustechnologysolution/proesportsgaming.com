<?php 
class Giveaway_model extends CI_Model {
	public function __construct(){
		parent::__construct();
	}
	function checkgiveawayuser($giveaway_id, $user_id) {
    $gaarr = array(
      'giveaway_id' => $giveaway_id,
      'user_id' => $user_id
    );
    $view_data = $this->General_model->view_data('giveaway_users', $gaarr);
    echo "<pre>";
    print_r($view_data);
    exit();
  }
  public function get_ga_data($arr='') {
    $this->db->select('ga.title, ga.amount as gw_amount, ga.entrieslimit as gw_entrieslimit, ga.description as gw_description, ga.is_multiple_entries as gw_is_multiple_entries, ga.img as gw_img, ga.reset as gw_reset, ga.date_time_of_giveaway as gw_date_time_of_giveaway, ga.id as gw_id, ga.date_time_registration_close, ga.fireworks_duration_time, ga.lobby_id, ga.created_at, ga.fireworks_audio, ga.default_stream, ga.default_stream_showstatus, ga.use_custom_fireworks_audio, ga.user_id as ga_crtr, l.user_id as crtr, ud.account_no as creator_acc, ud.total_balance as ga_crtr_balance, ud.display_name, ud.name, ud.image, ag.game_name, ud.timezone, ud.common_settings');
    $this->db->from('giveaway ga');
    $this->db->join('lobby l','l.id = ga.lobby_id');
    $this->db->join('admin_game ag','ag.id = l.game_id');
    $this->db->join('user_detail ud','ud.user_id = l.user_id');
    $this->db->where('ga.reset',0);
    (!empty($arr['order_by'])) ? $this->db->order_by($arr['order_by'],'desc') : $this->db->order_by('ga.created_at','desc');
    (!empty($arr['id'])) ? $this->db->where('ga.lobby_id',$arr['id']): '';
    $query = $this->db->get();
    return $query->result_array();
  }
  public function get_ga_winner($arr='') {
    $this->db->select('gw.*, gw.id as giveaway_winner_id, ud.account_no, ud.display_name, ud.name');
    $this->db->from('giveaway_winners gw');
    $this->db->join('user_detail ud','ud.user_id = gw.user_id');
    (!empty($arr['lobby_id'])) ? $this->db->where('gw.lobby_id',$arr['lobby_id']): '';
    (!empty($arr['giveaway_id'])) ? $this->db->where('gw.giveaway_id',$arr['giveaway_id']): '';
    $this->db->order_by('gw.created_at','desc');
    $query = $this->db->get();
    return $query->row_array();
  }
  public function get_ga_chat($arr='') {
    $this->db->select('ggc.*, xm.message, xm.attachment, xm.message_type, ud.name, ud.image, ud.is_admin, lf.status as fan_status, lf.mute_time, ud.is_admin, ud.display_name, lf.fan_tag');
    $this->db->from('giveaway_group_conversation ggc');
    $this->db->join('xwb_messages xm','xm.id = ggc.message_id');
    $this->db->join('user_detail ud','ud.user_id = ggc.user_id');
    $this->db->join('lobby_fans lf','lf.user_id = ggc.user_id AND lf.lobby_id = ggc.lobby_id');
    $this->db->join('giveaway ga','ga.lobby_id = ggc.lobby_id AND ga.id = ggc.giveaway_id');
    (!empty($arr['lobby_id'])) ? $this->db->where('ggc.lobby_id',$arr['lobby_id']) : '';
    (!empty($arr['id'])) ? $this->db->where('ggc.id',$arr['id']) : '';
    $this->db->where('ggc.status',1);
    $this->db->where('lf.status',1);
    $this->db->order_by('ggc.message_id','asc');
    $query = $this->db->get();
    return $query->result_array();
  }
  public function giveaway_all_users($arr) {
    $this->db->select('gu.*, gu.id as gu_id, ud.*, ga.amount as gw_amount');
    $this->db->from('giveaway_users gu');
    $this->db->join('user_detail ud','ud.user_id = gu.user_id');
    $this->db->join('giveaway ga','ga.id = gu.giveaway_id');
    (!empty($arr['id'])) ? $this->db->where('gu.giveaway_id',$arr['id']) : '';
    (!empty($arr['lobby_id'])) ? $this->db->where('gu.lobby_id',$arr['lobby_id']) : '';
    $this->db->where('ga.reset', 0);
    $query = $this->db->get();
    return $query->result_array(); 
  }
} 