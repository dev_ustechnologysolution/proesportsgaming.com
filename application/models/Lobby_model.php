<?php 
class Lobby_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->model('User_model');
		$this->load->helper('url');
	}
	public function check_spectator_and_event($arr) {
		$this->db->select('count(id) as total');
		$this->db->from('events_users eu');
		$this->db->where('eu.is_remove',0);
		// $this->db->where('eu.is_refund',0);
		($arr['user_id'] != '') ? $this->db->where('((user_id = '.$arr['user_id'].' and is_gift = 0) OR (is_gift = 1 and gift_to = '.$arr['user_id'].')) AND (type = 1 OR type = 2)') : '';
		($arr['lobby_id'] != '') ? $this->db->where('lobby_id',$arr['lobby_id']) : '';
		$query = $this->db->get();
		return $query->result_array();
	}
	public function event_price_and_fees_info($arr = '') {
		$this->db->select('eu.*');
		$this->db->from('events_users eu');
		$this->db->where('eu.is_remove',0);
		// $this->db->where('eu.is_refund',0);
		($arr['lobby_id'] != '') ? $this->db->where('lobby_id', $arr['lobby_id']) : '';
		$query = $this->db->get();
		return $query->result_array();
	}
	public function get_lobby_banner_admin() {
		$this->db->select('l.lobby_banner_order, l.device_id, l.event_start_date, lg.user_id, lg.is_creator, l.event_image, l.is_spectator, l.is_event,GROUP_CONCAT(lg.stream_channel, "*_*+", lf.is_banner) as channel, l.game_id, gb.lobby_id, ag.game_name, ag.game_description,l.status');
		$this->db->from('lobby_group lg');
		$this->db->join('group_bet gb','gb.id = lg.group_bet_id');
		$this->db->join('lobby l','l.id = gb.lobby_id');
		$this->db->join('admin_game ag','ag.id = l.game_id');
		$this->db->join('lobby_fans lf','lf.lobby_id = l.id AND lf.user_id = lg.user_id');
		// $this->db->where('lg.stream_channel !=', '');
		// $this->db->where('lg.stream_status', 'enable');
		$this->db->where('gb.bet_status', 1);
		$this->db->where('l.status', 1);
		$this->db->where('l.is_archive', 0);
		$this->db->group_by('gb.lobby_id');
		$this->db->order_by('l.event_start_date','asc');
		$query = $this->db->get();
		// echo "<pre>";
		// print_r($query->result());
		// exit;
		return $query->result();
	}
	public function get_second_banner() {
		// $this->db->select('lf.id, lg.stream_channel, lg.stream_status, lf.obs_stream_channel, lf.stream_type, lf.stream_channel as fan_stream_channel, lf.stream_status as fan_stream_status');
		$this->db->select('lf.id, lf.obs_stream_channel, lf.stream_type, lf.stream_channel, lf.stream_status, lf.lobby_id, gb.id as group_bet_id, lf.user_id, ag.game_name');
		$this->db->from('lobby_fans lf');
		$this->db->join('group_bet gb','gb.lobby_id = lf.lobby_id');
		$this->db->join('lobby l','l.id = lf.lobby_id AND l.user_id = lf.user_id');
		$this->db->join('admin_game ag','ag.id = l.game_id');
		// $this->db->join('lobby_group lg','lg.group_bet_id = gb.id', 'left outer');
		// $this->db->where('NOT ((lf.stream_channel IS NULL OR lf.stream_channel = "") AND (lg.stream_channel IS NULL OR lg.stream_channel = ""))');
		$this->db->where('NOT ((lf.obs_stream_channel IS NULL OR lf.obs_stream_channel = ""))');
		$this->db->where('gb.bet_status', 1);
		$this->db->where('l.status', 1);
		$this->db->where('l.is_archive', 0);
		$this->db->where('lf.is_second_banner', 1);
		$this->db->where('lf.stream_status', 'enable');
		// $this->db->group_by('lf.user_id');
		$this->db->order_by('l.created_at','asc');
		$query = $this->db->get();
		return $query->result();
	}	
	//archive data
	public function get_archive_data() {
		$this->db->select('l.lobby_banner_order, l.device_id, l.event_start_date, lg.user_id, lg.is_creator, l.event_image, l.is_spectator, l.is_event,GROUP_CONCAT(lg.stream_channel, "*_*+", lg.is_banner) as channel, l.game_id, gb.lobby_id, ag.game_name, ag.game_description,l.status');
		$this->db->from('lobby_group lg');
		$this->db->join('group_bet gb','gb.id = lg.group_bet_id');
		$this->db->join('lobby l','l.id = gb.lobby_id');
		$this->db->join('admin_game ag','ag.id = l.game_id');
		// $this->db->where('lg.stream_channel !=', '');
		// $this->db->where('lg.stream_status', 'enable');
		$this->db->where('gb.bet_status', 1);
		$this->db->where('l.status', 1);
		$this->db->where('l.is_archive', 1);
		$this->db->group_by('gb.lobby_id');
		$this->db->order_by('l.lobby_banner_order','asc');
		$query = $this->db->get();
		// echo "<pre>";
		// print_r($query->result());
		// exit;
		return $query->result();
	}
	public function stream_banner_data() {
		// $this->db->select('GROUP_CONCAT(lg.stream_channel) as channel,gb.lobby_id');
		// $this->db->from('lobby_group lg');
		// $this->db->join('group_bet gb','gb.id = lg.group_bet_id');
		// $this->db->join('lobby l','l.id = gb.lobby_id');
		// $this->db->join('admin_game ag','ag.id = l.game_id');
		// $this->db->where('l.is_game_active', 1);
		// $this->db->where('l.status', 1);
		// $this->db->where('lg.is_banner', 1);
		// $this->db->where('lg.stream_status', 'enable');
		// $this->db->where('gb.bet_status', 1);
		// $this->db->group_by('gb.lobby_id');
		// $this->db->order_by('l.lobby_banner_order','asc');
		// $query = $this->db->get();
		// $result = $query->result();
		// echo "<pre>";
		// print_r($result);
		// echo "</pre>";

		$this->db->select('lf.id, lf.obs_stream_channel, lf.stream_type, lf.stream_channel, lf.stream_status, lf.lobby_id, lf.user_id');
		$this->db->from('lobby_fans lf');
		$this->db->join('lobby l','l.id = lf.lobby_id');
		$this->db->where('NOT ((lf.stream_channel IS NULL OR lf.stream_channel = "") AND (lf.obs_stream_channel IS NULL OR lf.obs_stream_channel = ""))');
		$this->db->where('l.is_game_active', 1);
		$this->db->where('l.is_archive', 0);
		$this->db->where('l.status', 1);
		$this->db->where('lf.is_banner', 1);
		$this->db->where('lf.stream_status', 'enable');
		$this->db->order_by('l.lobby_banner_order','asc');
		$query = $this->db->get();
		$result = $query->result();
		$result_new = [];
		if (!empty($result)) {
			foreach ($result as $key => $value) {
				(count($result_new[$value->lobby_id]) < 2) ? $result_new[$value->lobby_id][] = $value : '';
			}
		}
		return $result_new;
	}
	public function get_lobby_stream_banner_data($arr =' ') {
		$this->db->select('lf.id, lf.is_banner, lf.waiting_table, lf.obs_stream_channel, lf.stream_type, lf.stream_channel, lf.stream_status, lf.lobby_id, lf.user_id, ud.display_name, ud.name, lg.table_cat');
		$this->db->from('lobby_fans lf');
		$this->db->join('lobby l','l.id = lf.lobby_id');
		$this->db->join('user_detail ud','ud.user_id = lf.user_id');
		$this->db->join('group_bet gb','gb.lobby_id = lf.lobby_id');
		$this->db->join('lobby_group lg','lg.user_id = lf.user_id AND lg.group_bet_id = gb.id', 'LEFT OUTER');
		
		$this->db->where('NOT ((lf.stream_channel IS NULL OR lf.stream_channel = "") AND (lf.obs_stream_channel IS NULL OR lf.obs_stream_channel = ""))');
		$this->db->where('NOT ((lf.waiting_table IS NULL OR lf.waiting_table = "") AND (lg.table_cat IS NULL OR lg.table_cat = ""))');

		$this->db->where('l.is_game_active', 1);
		$this->db->where('l.is_archive', 0);
		$this->db->where('l.status', 1);
		// (!empty($arr['is_banner'])) ? $this->db->where('lf.is_banner', $arr['is_banner']) : '';
		(!empty($arr['stream_status'])) ? $this->db->where('lf.stream_status', $arr['stream_status']) : '';
		(!empty($arr['lobby_id'])) ? $this->db->where('lf.lobby_id', $arr['lobby_id']) : '';
		// $this->db->where('lf.stream_status', 'enable');
		$this->db->order_by('l.lobby_banner_order','asc');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	public function check_event_registration_is_off($arr) {
		if (!empty($arr)) {
			$lobby_detail = $arr['lobby_data'];
			$id = $lobby_detail[0]['id'];
			$lobbyarr = array(
				'lobby_id' => $id,
				'user_id' => $this->session->userdata('user_id')
			);
			$check_spnev = $this->check_spectator_and_event($lobbyarr);
			if ($lobby_detail[0]['is_event'] == 1 && $lobby_detail[0]['is_spectator'] == 0 && $lobby_detail[0]['user_id'] != $this->session->userdata('user_id') && $check_spnev[0]['total'] == 0 && $lobby_detail[0]['registration_status'] != 1) {
			  $this->session->set_flashdata('message_succ', 'Sorry registration is closed for this event.<br>Please check Calendar for future events.');
			  redirect(base_url());
			}
		}
	}
	public function check_only_eve($arr) {
		if (!empty($arr)) {
			$lobby_detail = $arr['lobby_data'];
      		$id = $lobby_detail[0]['id'];
			$lobbyarr = array(
				'lobby_id' => $id,
				'user_id' => $this->session->userdata('user_id')
			);
      		$check_spnev = $this->check_spectator_and_event($lobbyarr);
      		if ($lobby_detail[0]['is_event'] == 1 && $lobby_detail[0]['user_id'] != $this->session->userdata('user_id') && $check_spnev[0]['total'] == 0 && $lobby_detail[0]['registration_status'] != 1) {
      			$this->session->set_flashdata('message_succ', 'Sorry registration is closed for this event.<br>Please check Calendar for future events.');
      			redirect(base_url());
      		}
      	}	
	}
	public function check_created_lobbies($user_id) {
		$this->db->select('asg.sub_game_name, l.user_id as creator, l.is_event, l.id as lobby_id, l.game_category_id, l.price, l.description, l.device_id, l.game_type, l.created_at, l.game_id, gi.game_image, ag.game_name, ud.name, ud.is_admin, ud.common_settings, l.sub_game_id, gb.id as group_bet_id, l.custom_name,l.is_archive, l.event_start_date');
		$this->db->from('lobby l');
		$this->db->join('user_detail ud','l.user_id=ud.user_id');
		$this->db->join('game_image gi','gi.id=l.image_id');
		$this->db->join('admin_game ag','ag.id=l.game_id');
		$this->db->join('admin_sub_game asg','l.sub_game_id = asg.id');
		$this->db->join('group_bet gb','gb.lobby_id = l.id');
		$this->db->where("l.payment_status='1' AND l.status='1' AND l.user_id='".$user_id."' AND l.is_archive = '0'");
		$this->db->where("gb.bet_status", 1);
		// $this->db->where("l.is_event", 0);
		$data['created_lobbies'] = $this->db->get()->result_array();
		$get_user_data = $this->User_model->get_user_detail($user_id);
		$common_settings = json_decode($get_user_data->common_settings);
		$get_activated_membership = $this->User_model->get_activated_membership($user_id);
		$data['error'] = $data['redirect_url'] = '';
		$data['reached_limit'] = 'no';
		$data['membership'] = 'yes';
		
		$get_stream_setting = $this->General_model->view_data('site_settings', array('option_title' => 'default_lobby_creation_limit'))[0];
		
		if (empty($get_activated_membership) && $get_user_data->is_admin == 0 && (empty($common_settings->free_membership) || $common_settings->free_membership == 'off') && count($data['created_lobbies']) >= $get_stream_setting['option_value']) {
			$data['membership'] = 'no';
			$data['error'] = 'Please subscribe Membership to Create lobby';
			$data['redirect_url'] = base_url() . 'membership/getMembershipPlan';
		}
		if (!empty($get_activated_membership) && count($data['created_lobbies']) >= $get_activated_membership->lobby_creation_limit && $get_user_data->is_admin == 0 && (empty($common_settings->free_membership) || $common_settings->free_membership == 'off')) {
			$data['reached_limit'] = 'yes';
			$data['error'] = 'Already Reached the Limit of creating A lobby <br> Please remove one of your created Live Lobby challenges from My Sreams tab. Than Create a New Challenge.';
			$data['redirect_url'] = base_url() . 'dashboard';
		}
		return $data;
	}
	public function check_joined_lobbies($user_id) {
		$data['joined_lobbies'] = $this->General_model->get_my_lobby_list($user_id,'no');
		$data['get_activated_membership'] = $this->User_model->get_activated_membership($user_id);
		$get_user_data = $this->User_model->get_user_detail($user_id);
		$common_settings = json_decode($get_user_data->common_settings);
		$data['error'] = $data['redirect_url'] = '';
		$data['reached_limit'] = 'no';
		$data['membership'] = 'yes';

		$get_stream_setting = $this->General_model->view_data('site_settings', array('option_title' => 'default_lobby_join_limit'))[0];

		if (empty($data['get_activated_membership']) && $get_user_data->is_admin == 0 && (empty($common_settings->free_membership) || $common_settings->free_membership == 'off') && count($data['joined_lobbies']) >= $get_stream_setting['option_value']) {
			$data['error'] = 'Please subscribe Membership to Join lobby';
			$data['redirect_url'] = base_url() . 'membership/getMembershipPlan';
			$data['membership'] = 'no';
		}
		if (!empty($data['get_activated_membership']) && count($data['joined_lobbies']) >= $data['get_activated_membership']->lobby_joining_limit && $get_user_data->is_admin == 0 && (empty($common_settings->free_membership) || $common_settings->free_membership == 'off')) {
			$data['reached_limit'] = 'yes';
			$data['error'] = 'Already Reached the Limit of joining A Lobby list <br> Please Leave your Existing game in My Lobbies Tab. Than Join a New Game.';
			$data['redirect_url'] = base_url() . 'dashboard';
		}
		return $data;
	}
}