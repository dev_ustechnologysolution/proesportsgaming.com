<?php
class My_model extends CI_Model
{
    public function __construct()
    {
	parent::__construct();
    }
    
    function generate_combo($table,$name,$value,$cond='',$selected=-1,$name_prefix='',$order_by='id',$order_type='ASC')
    {
        $sql    = "SELECT * FROM {$this->db->dbprefix}$table $cond ORDER BY $order_by $order_type";
        $query = $this->db->query($sql);
	$result_arr = $query->result_array();
        $html   = '';
        foreach ($result_arr as $k=>$v)
        {
            $select = '';
            if($selected == $v[$value])
                $select = ' selected ';
            $html   .= "<option value='{$v[$value]}' $select>$name_prefix{$v[$name]}</option>";
        }
        return $html;
    }

    function generate_combo_malti($table,$name,$value,$cond='',$selected='',$name_prefix='',$order_by='id',$order_type='ASC')
    {
        $sql    = "SELECT * FROM {$this->db->dbprefix}$table $cond ORDER BY $order_by $order_type";
        $query = $this->db->query($sql);
	$result_arr = $query->result_array();
        $html   = '';
        $ckArr  = array();
        if($selected!='')
            $ckArr  = explode(',', $selected);
        foreach ($result_arr as $k=>$v)
        {
            $select = '';
            if(in_array($v[$value], $ckArr))
                $select = ' selected ';
            $html   .= "<option value='{$v[$value]}' $select>$name_prefix{$v[$name]}</option>";
        }
        return $html;
    }

   
	
	
    function upload_image($arr=array())
    {
        $this->load->helper('my_image_helper');
        if( isset($_FILES[$arr['file']]['name']) && $_FILES[$arr['file']]['name']!='')
        {
            $img_details = upload_file($this,
                                    array('upload_path' => $arr['directory'],
                                        'file_name' => (isset($arr['new_name'])?$arr['new_name']:time()),
                                        'allowed_types' => (isset($arr['allowed_types'])?$arr['allowed_types']:'gif|jpg|png|jpeg'),
                                        'max_size' => (isset($arr['max_size'])?$arr['max_size']:'1000'),
                                        'max_width' => (isset($arr['max_width'])?$arr['max_width']:'0'),
                                        'max_height' => (isset($arr['max_height'])?$arr['max_height']:'0'),
                                        'file_type_variation' => (isset($arr['file_type_variation'])?$arr['file_type_variation']:''),
                                        ), $arr['file']
                                    );
								
            if(is_array($img_details))
            {
                $imgPath= $img_details['orig_name'];
                if(isset($arr['create_thumb']))
                {
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $img_details['full_path'];
                    $config['create_thumb'] = TRUE;
                    $config['maintain_ratio'] = TRUE;
                    $config['thumb_marker'] = '';
					$config['width'] = (isset($arr['width'])?$arr['width']:'150');
                    $config['height'] = (isset($arr['height'])?$arr['height']:'200');
                    $config['new_image'] = $arr['thumb_path'].$imgPath;
                    $this->load->library('image_lib');
                    $this->image_lib->initialize($config);
                    $this->image_lib->resize();
                }
                return array('message'=>$imgPath,'message_type'=>'succ');
            }
            else
            {
                $err=explode('|',$img_details);
                var_dump($err);
                return array('message'=>$err[0],'message_type'=>'err');
            }
        }
    }
	
	
	
    function manage_hites($table,$field_name,$id)
    {
        $sql    = "UPDATE {$this->db->dbprefix}$table SET $field_name=$field_name+1 WHERE id=$id";
        $this->db->query($sql);
        $data   = $this->_get($table, 1, 0, array('id'=>$id));
        return $data[0][$field_name];
    }

    function set_insert($table,$data=array())
    {
        if( count($data)==0 || !$table ||  $table=='')
            return false;
        if($this->db->insert($table, $data))
            return $this->db->insert_id();
        else
            return false;
    }
    
    function set_update($table,$data=array(),$cond=array())
    {
        if( count($data)==0 || !$table ||  $table=='')
            return false;
        if(!$this->is_valid_data($table,$cond))
            return false;
        if($this->db->update($table, $data, $cond))
            return true;
        else
            return false;
    }

    function set_delete($table,$cond=array())
    {
        if( !$table ||  $table=='')
            return false;
        if(!$this->is_valid_data($table,$cond))
            return false;
        if($this->db->delete($table,  $cond))
            return true;
        else
            return false;
    }

    function is_valid_data($table,$cond=array())
    {
        $sub_cond   = ' WHERE 1 ';
        if(count($cond)>0)
        {
            foreach ($cond as $k=>$v)
                $sub_cond   .= " AND $k='$v' ";
        }
        $sql    = "SELECT * FROM {$this->db->dbprefix}$table $sub_cond ";
        $query  = $this->db->query($sql);
        if($query->num_rows()==0)
            return false;
        else
            return true;
    }

    function _get($table,$toshow=-1,$page=0,$cond=array(),$order_name='id',$order_type='ASC')
    {
        $sub_cond   = ' WHERE 1 ';
        $limit  = '';
        if(count($cond)>0)
        {
            foreach ($cond as $k=>$v)
            {
                if($k=='ext_cnd')
                    $sub_cond   .= " AND $v";
                elseif($v!='' && $v!=-1)
                        $sub_cond   .= " AND $k='$v' ";
            }
        }
        $sql    = "SELECT * FROM {$this->db->dbprefix}$table $sub_cond ";
        if($toshow>0)
            $limit	= ' limit '.$page.','.$toshow;
        $sql    .= ' order by '.$order_name.' '.$order_type.' '.$limit;
        $query = $this->db->query($sql);
        $result_arr = $query->result_array();
        return $result_arr;
    }
    
    function _get_count($table,$cond=array())
    {
        $sub_cond   = ' WHERE 1 ';
        if(count($cond)>0)
        {
            foreach ($cond as $k=>$v)
            {
                if($k=='ext_cnd')
                    $sub_cond   .= " AND $v";
                elseif($v!='' && $v!=-1)
                        $sub_cond   .= " AND $k='$v' ";
            }
        }
        $sql    = "SELECT * FROM {$this->db->dbprefix}$table $sub_cond ";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
	
	function get_issue()
    {
		$sql= "SELECT i.*,a.name,aa.name solved_name ,o.req_bill_to_forename,o.req_bill_to_surname,o.req_bill_to_email FROM {$this->db->dbprefix}issue i
				inner join {$this->db->dbprefix}admin a on i.create_agent_id=a.id
				inner join {$this->db->dbprefix}order_info o on i.customer_id=o.id
				left join {$this->db->dbprefix}admin aa on i.solved_agent_id=aa.id
				
				
				";
		
		$query          = $this->db->query($sql);
		$result_arr     = $query->result_array();
        return $result_arr;
		
    }
			function lead_current_status_get($status_id='-1')	{		
			$ret_arr=array(
				'1'=>'Callback:Did not Pick Up',
				'2'=>'Callback:Switched Off/Network Error',
				'3'=>'Callback:Driving',
				'4'=>'Callback:Busy',
				'5'=>'Not Interested: Satisfied with Ola/TFS',
				'6'=>'Not Interested: Does not like Uber Service',
				'7'=>'Not Interested: Does not like Uber incentives',
				'8'=>'Not Interested: Others',
				'9'=>'Interested: Has Attached his car with Uber',
				'10'=>'Interested: Has already received all information',
				'11'=>'Interested: Information Given, wants more time to think',
				'12'=>'Interested: Information Given, will attach car soon',
				'13'=>'Interested: Will attach next car with uber',
				'14'=>'Interested: Wants more information about Vehicle Financing/Incentives/Others',
				'15'=>'Interested: Others',
				'16'=>'Warned and promised to improve',
				'17'=>'Warned but did not behave properly',
				'18'=>'Wrong Number',
				);
				
		if(isset($ret_arr[$status_id]))			
		{	
			return 	$ret_arr[$status_id];	
		}
		else
		{
			return 	'No Status';	
		}			}

    function getPcTabGame($toshow=-1,$page=0,$category_id='',$condition='')
    {
        /*
        $this->db->group_by('game_id');
        $this->db->select('ag.id,ag.game_name,ag.game_description,ag.created_at,gi.game_image');
        $this->db->from('admin_game ag');
        $this->db->join('game_image gi','gi.game_id=ag.id');
        $this->db->where($condition);
        $query=$this->db->get();
        return $query->result_array();
        */

        $sql="select ag.id,ag.game_name,ag.game_description,ag.created_at,gi.game_image,asg.sub_game_name, g.game_timer from admin_game ag
        LEFT JOIN game_image gi ON gi.game_id=ag.id
        LEFT JOIN game g ON g.game_id=ag.id
        LEFT JOIN admin_sub_game asg ON g.sub_game_id=asg.id        
        WHERE ag.play_status='0'";
        if($condition!='')
        {
            $sql .= $condition;
        }
        $sql .= "group by ag.id";
        $limit='';
        if($toshow>0)
        {
            $limit	= ' limit '.$page.','.$toshow;
        }

        $query = $this->db->query($sql);
        $result_arr['tot'] = $query->num_rows();

        $sql    .= $limit;
        $query = $this->db->query($sql);
        $result_arr['data'] = $query->result_array();

        return $result_arr;

    }

	function getPcTabGameChallange($toshow=-1,$page=0,$category_id='',$condition='')
    {
        /*
        $sql='select DISTINCT l.*,la.status lead_current_status from leads l
		left join leads_assign la on la.lead_id=l.id
		';
        */
        $sql="select asg.sub_game_name,g.id,g.price,g.description,g.game_timer,g.created_at,g.game_id,g.game_type,g.device_id,gi.game_image,ag.game_name,ud.name from game g
        LEFT JOIN user_detail ud ON g.user_id=ud.user_id
        LEFT JOIN game_image gi ON gi.id=g.image_id
        LEFT JOIN admin_game ag ON ag.id=g.game_id
        LEFT JOIN admin_sub_game asg ON asg.id=g.sub_game_id
        WHERE g.payment_status='1' AND g.status='1' AND g.game_category_id='".$category_id."'";

        if($condition!='')
        {
            $sql .= $condition;
        }
        $limit='';
        if($toshow>0)
        {
            $limit	= ' limit '.$page.','.$toshow;
        }

        $query = $this->db->query($sql);
        $result_arr['tot'] = $query->num_rows();

        $sql    .= $limit;
        $query = $this->db->query($sql);
        $result_arr['data'] = $query->result_array();

        return $result_arr;
    }
    function getMatchesTabGameChallange($toshow=-1,$page=0,$category_id='',$condition='')
    {
        /*
        $sql='select DISTINCT l.*,la.status lead_current_status from leads l
        left join leads_assign la on la.lead_id=l.id
        ';
        */
        $sql="select asg.sub_game_name, ag.game_category_id,gc.cat_img,gc.category_name,g.best_of,g.id,g.price,g.game_password,g.description,g.game_timer,g.created_at,g.game_id,g.game_type,g.device_id,gi.game_image,ag.game_name,gc.cat_slug,ud.name from game g
        LEFT JOIN user_detail ud ON g.user_id=ud.user_id
        LEFT JOIN game_image gi ON gi.id=g.image_id
        LEFT JOIN admin_game ag ON ag.id=g.game_id
        LEFT JOIN admin_sub_game asg ON asg.id=g.sub_game_id
        LEFT JOIN admin_game_category gc ON gc.id=ag.game_category_id
        WHERE g.payment_status='1' AND g.status='1'";
        if($condition!='')
        {
            $sql .= $condition;
        }
        $limit='';
        if($toshow>0)
        {
            $limit  = ' limit '.$page.','.$toshow;
        }

        $query = $this->db->query($sql);
        $result_arr['tot'] = $query->num_rows();

        $sql    .= $limit;
        $query = $this->db->query($sql);
        $result_arr['data'] = $query->result_array();

        return $result_arr;
    }
    function chatads_data() {
        $this->db->select('c.*');
        $this->db->from('chatads c');
        $query = $this->db->get();
        return $query->result_array();
    }
    function chatads_single_data($id) {

        $this->db->select('c.*');
        $this->db->from('chatads c');
        $this->db->where('c.id', $id);
        $query = $this->db->get()->row();
        return $query;
    }
    public function get_alltimezone() {
        $timezoneArray = timezone_identifiers_list();
        $select = '<select name="timezone" class="form-control" id="form-control" required>';
        $select .= '<option value="" selected >Select Time zone</option>';
        $timezoneArray = preg_grep("/America/", $timezoneArray, PREG_GREP_INVERT);
        array_push($timezoneArray, "HST", "America/Anchorage", "America/Los_Angeles", "MST", "America/Chicago", "EST");
        foreach ($timezoneArray as $key => $ta) {
            $newta = str_replace(["HST", "America-Anchorage", "America-Los_Angeles", "MST", "America-Chicago", "EST"],["U.S-Hawaii-Aleutian", "U.S-Alaska", "U.S-Pacific", "U.S-Mountain", "U.S-Central", "U.S-Eastern"],str_replace('/','-',$ta));
            $select .='<option value="'.$ta.'"';
            $select .= (!empty($_COOKIE['time_zne']) && ($ta == timezone_name_from_abbr('', $_COOKIE['time_zne'] * 60, false))) ? ' selected' : '';
            $select .= '>'.$newta.'</option>';
        }
        $select .='</select>';
        return $select;
    }
}
