<?php
class Order_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}
	function getorderitems($arr) {
    $select = 'ot.tracking_id, ot.email, ot.phone, ot.name, ot.user_id, ot.sub_total, ot.shipping_address, ot.shipping_city, ot.ref_seller_id, ot.shipping_country, ot.shipping_state, ot.shipping_zip_code, ot.billing_address, ot.billing_city, ot.billing_country, ot.billing_state, ot.billing_zip_code, ot.payment_method, ot.paypal_deducted, ot.wallet_deducted, ot.id as order_id, ot.transaction_id, ot.shipping_charge, ot.grand_total, ot.payment_method, ot.order_status, ot.created_at, ot.updated_at';
    $select .= ($arr['get_items'] != '') ? ', oit.sold_by, oit.id as order_item_id, oit.product_fee, oit.file_name, oit.product_id, oit.product_name, oit.product_image, oit.qty, oit.amount, oit.product_attributes' : '';
    $this->db->select($select);
    $this->db->from('orders_tbl ot');
    if ($arr['get_items'] != '') {
    	$this->db->join('orders_items_tbl oit', 'oit.order_id = ot.id');
    	($arr['id'] != '') ? $this->db->where('oit.order_id',$arr['id']) : '';
    	($arr['get_items_by_order'] !='') ? $this->db->order_by('oit.created_at','desc'):'';
      ($arr['sold_by'] != '') ? (($arr['sold_by'] == 'players') ? $this->db->where('oit.sold_by !=', 0) : $this->db->where('oit.sold_by', $arr['sold_by'])) : '';
    }
    ($arr['order_status'] != '') ? $this->db->where_in('ot.order_status',$arr['order_status']) : '';
    ($arr['user_id'] != '') ? $this->db->where('ot.user_id',$arr['user_id']) : '';
    // if(isset($arr['sold_by']) && $arr['sold_by'] != '') {
    //   if (isset($arr['type']) && $arr['type'] != '' && $arr['type'] == 'player') {
    //     $this->db->where('ot.sold_by !=', $arr['sold_by']);
    //   } else {  
    //     $this->db->where('ot.sold_by', $arr['sold_by']);
    //   }
    // }
    (!empty($arr['order_by'])) ? $this->db->order_by($arr['order_by'][0],$arr['order_by'][1]) : '';
    $res = $this->db->get()->result_array();
    return $res;
	}

  function get_user_order_details($sold_by){
    $this->db->select('SUM(ord.grand_total) as total_gross_selling, COUNT(ord.id) as total_orders, SUM(ord_itm.qty) as total_items_sold');
    $this->db->from('orders_tbl ord');
    $this->db->join('orders_items_tbl ord_itm', 'ord_itm.order_id = ord.id');
    $this->db->where('ord.tracking_id !=', null);
    $this->db->where('ord.ref_seller_id', $sold_by);
    $res = $this->db->get()->result_array();
    return $res;
  }
}