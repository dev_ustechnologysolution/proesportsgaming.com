<?php 
class Payment_history_model extends CI_Model {
	public function __construct(){
		parent::__construct();
	}
	function user_history($arr) {
		$data['user_detail'] = $this->General_model->view_data('user_detail', array( 'user_id' => $arr['user_id']))[0];
		$data['purchase_history'] = $this->purchase_history($arr);
		$data['withdrawal_history'] = $this->withdrawal_history($arr);
		$data['membership_history'] = $this->membership_history($arr);
		$data['subscription_history'] = $this->subscription_history($arr);
		$data['matches_history'] = $this->matches_history($arr);
		$data['lobby_history'] = $this->lobby_history($arr);
		$data['tournament_history'] = $this->tournament_history($arr);
		$data['trading_history'] = $this->trading_history($arr);
		$data['tips_history'] = $this->tips_history($arr);
		$data['giveaway_history'] = $this->giveaway_history($arr);
		$data['user_balance_update'] = $this->user_balance_update($arr);
		$array = array_merge($data['purchase_history'], $data['withdrawal_history'], $data['subscription_history'], $data['matches_history'], $data['membership_history'], $data['lobby_history'], $data['tournament_history'], $data['trading_history'], $data['tips_history'], $data['giveaway_history'], $data['user_balance_update']);

	    $data['paypal_arr'] = array_filter(array_map(function ($value) { return ($value['payment_effect'] == 'paypal') ? $value : ''; }, $array));

	    $data['wallet_arr'] = array_filter(array_map(function ($value1) { return ($value1['payment_effect'] == 'wallet') ? $value1 : ''; }, $array));
		return $data;
	}
	function purchase_history($arr) {
		$this->db->select('p.*, ap.id as packageid, ap.point_title as package_title, ap.amount as package_amount');
		$this->db->from('payment p');
		$this->db->join('admin_packages ap', 'p.package_id = ap.id','left outer');
		$this->db->where('p.user_id', $arr['user_id']);
		$this->db->where('p.payment_status', 'completed');
		(!empty($arr['start_date']) && !empty($arr['end_date'])) ? $this->db->where('p.payment_date >= "'.$arr['start_date'].'" AND p.payment_date <= "'.$arr['end_date'].'"') : '';
		$result_array = $this->db->get()->result_array();
		$history = [];
		if (!empty($result_array)) {
			foreach ($result_array as $key => $h) {
				$transaction_title = 'Bought "'.((!empty($h['packageid']) && $h['package_title'] != '' && $h['package_title'] != null) ? $h['package_title'] : 'Individual') .'" Package';
				$history[] = array(
					'id' => $h['id'],
					'user_id' => $arr['user_id'],
					'transaction_title' => $transaction_title,
					'module' => 'purchase_funds',
					'amount' => number_format((float)$h['amount']-$h['collected_fees'],2,".",""),
					'fee' => number_format((float)$h['collected_fees'],2,".",""),
					'payment_effect' => 'wallet',
					'transaction_type' => 'credit',
					'created_at' => date('Y-m-d H:i:s', strtotime($h['payment_date'])),
					'custom_data' => ''
				);
			}
		}
		return $history;
	}
	function withdrawal_history($arr) {
		$this->db->select('wr.*');
		$this->db->from('withdraw_request wr');
		$this->db->where('wr.user_id', $arr['user_id']);
		$this->db->where('wr.status', 1);
		(!empty($arr['start_date']) && !empty($arr['end_date'])) ? $this->db->where('wr.paid_date >= "'.$arr['start_date'].'" AND wr.paid_date <= "'.$arr['end_date'].'"') : '';
		$result_array = $this->db->get()->result_array();
		$history = [];
		if (!empty($result_array)) {
			foreach ($result_array as $key => $h) {
				$amnt = ($h['collected_fees'] !='' && $h['collected_fees'] != null) ? number_format((float) $h['amount_paid'] - $h['collected_fees'],2,".","") : number_format((float) $h['amount_paid'],2,".","");
				// $transaction_title = 'Withdraw of '.$amnt.' amount';
				$transaction_title = 'Amount Withdrawal';
				$history[] = array(
					'id' => $h['id'],
					'user_id' => $arr['user_id'],
					'transaction_title' => $transaction_title,
					'module' => 'withdraw_funds',
					'amount' => $amnt,
					'fee' => number_format((float)$h['collected_fees'],2,".",""),
					'payment_effect' => 'wallet',
					'transaction_type' => 'debit',
					'created_at' => date('Y-m-d H:i:s', strtotime($h['paid_date'])),
					'custom_data' => ''
				);
			}
		}
		return $history;
	}
	function membership_history($arr) {
		$this->db->select('pms.*, m.title');
		$this->db->from('player_membership_subscriptions pms');
		$this->db->join('membership m', 'm.id = pms.plan_id');
		$this->db->where('pms.user_id', $arr['user_id']);
		(!empty($arr['start_date']) && !empty($arr['end_date'])) ? $this->db->where('pms.create_at >= "'.$arr['start_date'].'" AND pms.create_at <= "'.$arr['end_date'].'"') : '';
		$result_array = $this->db->get()->result_array();
		$history = [];
		if (!empty($result_array)) {
			foreach ($result_array as $key => $h) {
				$amnt = number_format((float) $h['amount'],2,".","");
				$transaction_title = (!$updated_plan) ? 'Active plan "'.$h['title'].'"' : 'Update plan "'.$updated_plan.'" To "'.$h['title'].'"';
				$history[] = array(
					'id' => $h['id'],
					'user_id' => $arr['user_id'],
					'transaction_title' => $transaction_title,
					'module' => 'membership',
					'amount' => $amnt,
					'fee' => '',
					'payment_effect' => 'paypal',
					'transaction_type' => 'debit',
					'created_at' => date('Y-m-d H:i:s', strtotime($h['create_at'])),
					'custom_data' => ''
				);
				$updated_plan = $h['title'];
			}
		}
		return $history;
	}
	function subscription_history($arr) {
		$this->db->select('ps.*');
		$this->db->from('paypal_subscriptions ps');
		$this->db->where('ps.payment_status','Completed');
		$this->db->where("(ps.user_to = '".$arr['user_id']."' OR ps.user_from = '".$arr['user_id']."')");
		(!empty($arr['start_date']) && !empty($arr['end_date'])) ? $this->db->where('ps.created_at >= "'.$arr['start_date'].'" AND ps.created_at <= "'.$arr['end_date'].'"') : '';
		$result_array = $this->db->get()->result_array();
		$history = [];
		if (!empty($result_array)) {
			foreach ($result_array as $key => $h) {
				$getdetail['user_id'] = ($h['user_from'] == $arr['user_id']) ? $h['user_to'] : $h['user_from'];
				$user_details = $this->General_model->view_data('user_detail', $getdetail)[0];
				$name = ($user_details['display_name_status'] == 1 && !empty($user_details['display_name'])) ? $user_details['display_name'] : $user_details['name'];
				
				$transaction_title = ($h['user_from'] == $arr['user_id']) ? "Sent subscription to '".$name."'" : "Received subscription from '".$name."'";

				$amnt = number_format((float) $h['amount'] - $h['fee'],2,".","");
				if ($h['user_from'] != $arr['user_id']) {
					$history[] = array(
						'id' => $h['id'],
						'user_id' => $arr['user_id'],
						'transaction_title' => $transaction_title,
						'module' => 'subscription',
						'amount' => $amnt,
						'fee' => $h['fee'],
						'payment_effect' => 'wallet',
						'transaction_type' => 'credit',
						'created_at' => date('Y-m-d H:i:s', strtotime($h['created_at'])),
						'custom_data' => ''
					);
				} else {
					$history[] = array(
						'id' => $h['id'],
						'user_id' => $arr['user_id'],
						'transaction_title' => $transaction_title,
						'module' => 'subscription',
						'amount' => $amnt,
						'fee' => $h['fee'],
						'payment_effect' => 'paypal',
						'transaction_type' => 'debit',
						'created_at' => date('Y-m-d H:i:s', strtotime($h['created_at'])),
						'custom_data' => ''
					);
				}
			}
		}
		return $history;
	}
	function matches_history($arr) {
		$this->db->select('b.*, ag.game_name, g.price, g.created_at as game_created_at, g.status as game_status');
		$this->db->from('bet b');
		$this->db->join('admin_game ag','ag.id = b.game_id');
		$this->db->join('game g','g.bet_id = b.id');
		$this->db->where("(b.challenger_id = '".$arr['user_id']."' OR b.accepter_id = '".$arr['user_id']."')");
		(!empty($arr['start_date']) && !empty($arr['end_date'])) ? $this->db->where('g.created_at >= "'.$arr['start_date'].'" AND g.created_at <= "'.$arr['end_date'].'"') : '';
		$this->db->group_by('b.id');
		$result_array = $this->db->get()->result_array();
		$history = [];
		if (!empty($result_array)) {
			$percentage = $this->General_model->view_all_data('admin_points_percentage','id','asc');
			foreach ($result_array as $key => $h) {
				if ($h['price'] < 5)
					$fee = $percentage[0]['lessthan5'];
				else if($h['price'] >= 5 && $h['price'] < 25)
					$fee = $percentage[0]['morethan5_25'];
				else if($h['price'] >= 25 && $h['price'] < 50)
					$fee = $percentage[0]['morethan25_50'];
				else if($h['price'] >= 50)
					$fee = $percentage[0]['morethan50'];

				$transaction_title = "Challenge ".(($h['challenger_id'] == $arr['user_id']) ? 'placed' : 'accepted')." for '".$h['game_name']."'";
				$created_at = ($h['challenger_id'] == $arr['user_id']) ? $h['challenge_date'] : $h['accepte_date'];
				$amnt = number_format((float) $h['price'] + $fee,2,".","");
				$history[] = array(
					'id' => $h['id'],
					'user_id' => $arr['user_id'],
					'transaction_title' => $transaction_title,
					'module' => 'match_challenge',
					'amount' => $amnt,
					'fee' => number_format((float) $fee,2,".",""),
					'transaction_type' => 'debit',
					'payment_effect' => 'wallet',
					'created_at' => ($created_at != null && $created_at !='' && $created_at != "0000-00-00" && $created_at != "0000-00-00 00:00:00") ? date('Y-m-d H:i:s', strtotime($created_at)) : date('Y-m-d H:i:s', strtotime($h['game_created_at'])),
					'custom_data' => ''
				);
				if ($h['winner_id'] == $arr['user_id']) {
					$created_at = $h['win_lose_deta'];
					$transaction_title = "Win Challenge for '".$h['game_name']."'";
					$history[] = array(
						'id' => $h['id'],
						'user_id' => $arr['user_id'],
						'transaction_title' => $transaction_title,
						'module' => 'match_challenge',
						'amount' => $h['price']*2,
						'fee' => number_format((float) $fee,2,".",""),
						'transaction_type' => 'credit',
						'payment_effect' => 'wallet',
						'created_at' => ($created_at != null && $created_at !='' && $created_at != "0000-00-00" && $created_at != "0000-00-00 00:00:00") ? date('Y-m-d H:i:s', strtotime($created_at)) : date('Y-m-d H:i:s', strtotime($h['game_created_at'])),
						'custom_data' => ''
					);
				}
				if (($h['accepter_id'] == $arr['user_id'] || $h['challenger_id'] == $arr['user_id']) && $h['bet_status'] == '0' && $h['game_status'] == '0' && $h['winner_id'] == '0') {
					$transaction_title = "Challenge refund for '".$h['game_name']."'";
					$created_at = ($h['challenger_id'] == $arr['user_id']) ? $h['challenge_date'] : $h['accepte_date'];
					$amnt = number_format((float) $h['price'] + $fee,2,".","");
					$history[] = array(
						'id' => $h['id'],
						'user_id' => $arr['user_id'],
						'transaction_title' => $transaction_title,
						'module' => 'match_challenge',
						'amount' => $amnt,
						'fee' => number_format((float) $fee,2,".",""),
						'transaction_type' => 'credit',
						'payment_effect' => 'wallet',
						'created_at' => ($created_at != null && $created_at !='' && $created_at != "0000-00-00" && $created_at != "0000-00-00 00:00:00") ? date('Y-m-d H:i:s', strtotime($created_at)) : date('Y-m-d H:i:s', strtotime($h['game_created_at'])),
						'custom_data' => ''
					);
				}
			}
		}
		return $history;
	}
	function lobby_history($arr) {
		$this->db->select('lg.user_id, lg.created_at, lg.updated_at, l.price, ag.game_name, lg.id, lg.win_lose_status, lg.play_status, lg.group_bet_id, lg.table_cat as join_table, gb.winner_table, gb.winner_table as group_winner_table');
		$this->db->from('lobby_group lg');
		$this->db->join('group_bet gb','gb.id = lg.group_bet_id');
		$this->db->join('lobby l','l.id = gb.lobby_id');
		$this->db->join('admin_game ag','ag.id = l.game_id');
		(!empty($arr['start_date']) && !empty($arr['end_date'])) ? $this->db->where('lg.created_at >= "'.$arr['start_date'].'" AND lg.created_at <= "'.$arr['end_date'].'"') : '';
		$this->db->where("lg.user_id",$arr['user_id']);
		$this->db->where("lg.play_status",1);
		$result_array = $this->db->get()->result_array();
		
		$history = [];
		if (!empty($result_array)) {
			$lobby_fee = $this->General_model->view_all_data('admin_lobby_fee', 'id', 'asc');
      		$fee_per_game = $lobby_fee[0]['fee_per_game'];
      		$join_fee = number_format((float)$fee_per_game, 2, '.', '');

			foreach ($result_array as $key => $h) {
		        $fee = $h['price'] * $join_fee/100;
		        $amnt = $h['price'] + $fee;

		        $transaction_title = "Ready Up Lobby '".$h['game_name']."'";
				$history[] = array(
					'id' => $h['id'],
					'user_id' => $arr['user_id'],
					'transaction_title' => $transaction_title,
					'module' => 'lobby_challenge',
					'amount' => number_format((float) $amnt,2,".",""),
					'fee' => number_format((float) $fee,2,".",""),
					'transaction_type' => 'debit',
					'payment_effect' => 'wallet',
					'created_at' => date('Y-m-d H:i:s', strtotime($h['created_at'])),
					'custom_data' => ''
				);
				if ($h['group_winner_table'] == $h['join_table']) {
					$transaction_title = "Win Lobby for '".$h['game_name']."'";
					$history[] = array(
						'id' => $h['id'],
						'user_id' => $arr['user_id'],
						'transaction_title' => $transaction_title,
						'module' => 'lobby_challenge',
						'amount' => $h['price']*2,
						'fee' => number_format((float) $fee,2,".",""),
						'transaction_type' => 'credit',
						'payment_effect' => 'wallet',
						'created_at' => date('Y-m-d H:i:s', strtotime($h['updated_at'])),
						'custom_data' => ''
					);
				}
			}
		}
		return $history;
	}
	function tournament_history($arr) {
		$this->db->select('eu.user_id, eu.id, eu.lobby_id, eu.created, ag.game_name, l.price, l.event_creator, l.event_fee_collector, l.event_price, l.spectate_price, l.event_fee_creator, l.spectator_fee, l.event_fee,eu.is_refund');
		$this->db->from('events_users eu');
		$this->db->join('lobby l', 'l.id = eu.lobby_id');
		$this->db->join('admin_game ag', 'ag.id = l.game_id');
		$this->db->where("(eu.user_id = '".$arr['user_id']."' OR l.event_fee_collector = '".$arr['user_id']."' OR l.event_fee_creator = '".$arr['user_id']."')");
		(!empty($arr['start_date']) && !empty($arr['end_date'])) ? $this->db->where('eu.created >= "'.$arr['start_date'].'" AND eu.created <= "'.$arr['end_date'].'"') : '';
		$result_array = $this->db->get()->result_array();
		
		$history = [];
		if (!empty($result_array)) {
			foreach ($result_array as $key => $h) {
				$transaction_title = ($h['is_refund'] == 0) ? "Tournament ticket bought for '".$h['game_name']."'" : "Get  refund Tournament ticket for '".$h['game_name']."'";


				if ($h['user_id'] == $arr['user_id']) {
					if ($h['event_price'] > 0 && $h['spectate_price'] == 0) {
						$amnt = number_format((float)$h['event_price'],2,".","");
						$fee = number_format((float)$h['event_fee'],2,".","");
					} else if ($h['event_price'] == 0 && $h['spectate_price'] >= 0) {
						$amnt = number_format((float)$h['spectate_price'],2,".","");
						$fee = number_format((float)$h['spectator_fee'],2,".","");
					}
					// $transaction_title = "Tournament ticket bought for '".$h['game_name']."'";
					$history[] = array(
						'id' => $h['id'],
						'user_id' => $arr['user_id'],
						'transaction_title' => $transaction_title,
						'module' => 'tournament',
						'amount' => $amnt,
						'fee' => $fee,
						'payment_effect' => 'wallet',
						'transaction_type' => ($h['is_refund'] == 0) ? 'debit' : 'credit',
						'created_at' => date('Y-m-d H:i:s', strtotime($h['created'])),
						'custom_data' => ''
					);
				} else if ($h['event_fee_creator'] == $arr['user_id']) {
					// $transaction_title = "Get Tournament ticket fee for '".$h['game_name']."'";
					if ($h['event_price'] > 0 && $h['spectate_price'] == 0) {
						$amnt = number_format((float)$h['event_price'] - $h['event_fee'],2,".","");
						$fee = '';
					} else if ($h['event_price'] == 0 && $h['spectate_price'] >= 0) {
						$amnt = number_format((float)$h['spectate_price'] - $h['spectator_fee'],2,".","");
						$fee = '';
					}
					$amnt = number_format((float)$h['event_price']-$h['event_fee'],2,".","");
					$history[] = array(
						'id' => $h['id'],
						'user_id' => $arr['user_id'],
						'transaction_title' => $transaction_title,
						'module' => 'tournament',
						'amount' => $amnt,
						'fee' => $fee,
						'payment_effect' => 'wallet',
						// 'transaction_type' => 'credit',
						'transaction_type' =>($h['is_refund'] == 0) ? 'credit' : 'debit',
						'created_at' => date('Y-m-d H:i:s', strtotime($h['created'])),
						'custom_data' => ''
					);
				} else if ($h['event_fee_collector'] == $arr['user_id']) {
					// $transaction_title = "Get Tournament ticket fee for '".$h['game_name']."'";
					$amnt = ($h['event_fee'] > 0) ? $h['event_fee'] : (($h['spectator_fee'] > 0) ? $h['spectator_fee'] : '');
					($amnt != '') ? $amnt = number_format((float)$amnt,2,".","") : '';
					$history[] = array(
						'id' => $h['id'],
						'user_id' => $arr['user_id'],
						'transaction_title' => $transaction_title,
						'module' => 'tournament',
						'amount' => $amnt,
						'fee' => $fee,
						'payment_effect' => 'wallet',
						// 'transaction_type' => 'credit',
						'transaction_type' =>($h['is_refund'] == 0) ? 'credit' : 'debit',
						'created_at' => date('Y-m-d H:i:s', strtotime($h['created'])),
						'custom_data' => ''
					);
				}
				// //refund
				//  if ($h['is_refund'] == 1) {
				// 	$transaction_title = "Get Tournament refund for '".$h['game_name']."'";
				// 	if ($h['event_price'] > 0 && $h['spectate_price'] == 0) {
				// 		$amnt = number_format((float)$h['event_price'],2,".","");
				// 		$fee = number_format((float)$h['event_fee'],2,".","");
				// 	} else if ($h['event_price'] == 0 && $h['spectate_price'] >= 0) {
				// 		$amnt = number_format((float)$h['spectate_price'],2,".","");
				// 		$fee = number_format((float)$h['spectator_fee'],2,".","");
				// 	}
				// 	$history[] = array(
				// 		'id' => $h['id'],
				// 		'user_id' => $arr['user_id'],
				// 		'transaction_title' => $transaction_title,
				// 		'module' => 'tournament',
				// 		'amount' => $amnt,
				// 		'fee' => $fee,
				// 		'payment_effect' => 'wallet',
				// 		'transaction_type' => 'credit',
				// 		'created_at' => date('Y-m-d H:i:s', strtotime($h['created'])),
				// 		'custom_data' => ''
				// 	);
				// }
			}
		}
		// echo"<pre>";
		// print_r($history);
		// exit;
		return $history;

	}
	function trading_history($arr) {
		$this->db->select('th.*');
		$this->db->from('trade_history th');
		$this->db->where("(th.user_id = '".$arr['user_id']."' OR th.trade_id = '".$arr['user_id']."')");
		(!empty($arr['start_date']) && !empty($arr['end_date'])) ? $this->db->where('th.created >= "'.$arr['start_date'].'" AND th.created <= "'.$arr['end_date'].'"') : '';
		$result_array = $this->db->get()->result_array();
		$history = [];
		if (!empty($result_array)) {
			foreach ($result_array as $key => $h) {
				$getdetail['user_id'] = ($h['user_id'] == $arr['user_id']) ? $h['trade_id'] : $h['user_id'];
				$user_details = $this->General_model->view_data('user_detail', $getdetail)[0];
				$name = ($user_details['display_name_status'] == 1 && !empty($user_details['display_name'])) ? $user_details['display_name'] : $user_details['name'];
				
				$transaction_title = ($h['user_id'] == $arr['user_id']) ? "Sent money to '".$name."'" : "Received money from '".$name."'";
				$transaction_type = ($h['user_id'] == $arr['user_id']) ? "debit" : "credit";
				$amnt = number_format((float) $h['transfer_amt'],2,".","");
				$history[] = array(
					'id' => $h['id'],
					'user_id' => $arr['user_id'],
					'transaction_title' => $transaction_title,
					'module' => 'trading',
					'amount' => $amnt,
					'fee' => '',
					'payment_effect' => 'wallet',
					'transaction_type' => $transaction_type,
					'created_at' => date('Y-m-d H:i:s', strtotime($h['created'])),
					'custom_data' => ''
				);
			}
		}	
		return $history;
	}
	function tips_history($arr) {
		$this->db->select('th.*');
		$this->db->from('tip_history th');
		$this->db->where("(th.user_from = '".$arr['user_id']."' OR th.user_to = '".$arr['user_id']."')");
		(!empty($arr['start_date']) && !empty($arr['end_date'])) ? $this->db->where('th.created >= "'.$arr['start_date'].'" AND th.created <= "'.$arr['end_date'].'"') : '';
		$result_array = $this->db->get()->result_array();
		$history = [];
		if (!empty($result_array)) {
			$lobby_fee = $this->General_model->view_all_data('admin_lobby_fee', 'id', 'asc');
	    	$tip_fees = number_format((float)$lobby_fee[0]['tip_fees'], 2, '.', '');
			foreach ($result_array as $key => $h) {
				$fee = ($h['tip_amt'] * $tip_fees) / 100;

				$getdetail['user_id'] = ($h['user_from'] == $arr['user_id']) ? $h['user_to'] : $h['user_from'];
				$user_details = $this->General_model->view_data('user_detail', $getdetail)[0];
				$name = ($user_details['display_name_status'] == 1 && !empty($user_details['display_name'])) ? $user_details['display_name'] : $user_details['name'];
				
				$transaction_title = ($h['user_from'] == $arr['user_id']) ? "Sent money to '".$name."'" : "Received money from '".$name."'";
				$transaction_type = ($h['user_from'] == $arr['user_id']) ? "debit" : "credit";
				$amnt = number_format((float) $h['tip_amt']+$fee,2,".","");
				$history[] = array(
					'id' => $h['id'],
					'user_id' => $arr['user_id'],
					'transaction_title' => $transaction_title,
					'module' => 'tips',
					'amount' => $amnt,
					'fee' => $fee,
					'payment_effect' => 'wallet',
					'transaction_type' => $transaction_type,
					'created_at' => date('Y-m-d H:i:s', strtotime($h['created'])),
					'custom_data' => ''
				);
			}
		}	
		return $history;
	}
	function giveaway_history($arr) {
		$this->db->select('gu.*, ag.game_name, g.amount, g.user_id as giveaway_creator');
		$this->db->from('giveaway_users gu');
		$this->db->join('giveaway g','g.id = gu.giveaway_id');
		$this->db->join('lobby l','l.id = gu.lobby_id');
		$this->db->join('admin_game ag','ag.id = l.game_id');
		$this->db->where("(gu.user_id = '".$arr['user_id']."' OR g.user_id = '".$arr['user_id']."')");
		(!empty($arr['start_date']) && !empty($arr['end_date'])) ? $this->db->where('gu.created_at >= "'.$arr['start_date'].'" AND gu.created_at <= "'.$arr['end_date'].'"') : '';
		
		$result_array = $this->db->get()->result_array();
		$history = [];
		if (!empty($result_array)) {
			$global_gafees = $this->General_model->view_data('site_settings', array('option_title' => 'global_giveaway_fees'))[0];
      		$global_gafees = explode('_', $global_gafees['option_value']);
	    
			foreach ($result_array as $key => $h) {
				if ($h['giveaway_creator'] == $arr['user_id']) {
					$ga_fee_collector_cr_amnt = ($global_gafees[0] == 1) ? number_format((float) $global_gafees[1], 2, '.', '') : number_format((float) $h['amount']*$global_gafees[1]/100, 2, '.', '');
					$fee = $ga_fee_collector_cr_amnt;
					$transaction_title = "Earn giveaway tickets for '".$h['game_name']."'";
					$transaction_type = "credit";
					$amnt = number_format((float) $h['amount']-$fee,2,".","");
					$history[] = array(
						'id' => $h['id'],
						'user_id' => $arr['user_id'],
						'transaction_title' => $transaction_title,
						'module' => 'giveaway',
						'amount' => $amnt,
						'fee' => number_format((float)$fee,2,".",""),
						'payment_effect' => 'wallet',
						'transaction_type' => $transaction_type,
						'created_at' => date('Y-m-d H:i:s', strtotime($h['created_at'])),
						'custom_data' => ''
					);
				} 
				if ($h['user_id'] == $arr['user_id']) {
	        		$ga_fee_collector_cr_amnt = ($global_gafees[0] == 1) ? number_format((float) $global_gafees[1], 2, '.', '') : number_format((float) $h['amount']*$global_gafees[1]/100, 2, '.', '');
					$fee = $ga_fee_collector_cr_amnt;
					$transaction_title = "Join giveaway for '".$h['game_name']."'";
					$transaction_type = "debit";
					$amnt = number_format((float) $h['amount'],2,".","");
					$history[] = array(
						'id' => $h['id'],
						'user_id' => $arr['user_id'],
						'transaction_title' => $transaction_title,
						'module' => 'giveaway',
						'amount' => $amnt,
						'fee' => number_format((float)$fee,2,".",""),
						'payment_effect' => 'wallet',
						'transaction_type' => $transaction_type,
						'created_at' => date('Y-m-d H:i:s', strtotime($h['created_at'])),
						'custom_data' => ''
					);
				}
			}
		}	
		return $history;
	}
	function user_balance_update($arr) {
		$this->db->select('bu.*');
		$this->db->from('balance_update bu');
		$this->db->where("bu.user_id",$arr['user_id']);
		(!empty($arr['start_date']) && !empty($arr['end_date'])) ? $this->db->where('bu.created_at >= "'.$arr['start_date'].'" AND bu.created_at <= "'.$arr['end_date'].'"') : '';
		$result_array = $this->db->get()->result_array();
		$history = [];
		if (!empty($result_array)) {
			foreach ($result_array as $key => $h) {
				$transaction_title = "Balace Update From Admin side";
				$transaction_type = ($h['affect'] == 0)? "debit" : "credit";
				$amnt = number_format((float) $h['amount'],2,".","");
				$history[] = array(
					'id' => $h['id'],
					'user_id' => $arr['user_id'],
					'transaction_title' => $transaction_title,
					'module' => 'BalanceUpdate',
					'amount' => $amnt,
					'fee' => '',
					'payment_effect' => 'wallet',
					'transaction_type' => $transaction_type,
					'created_at' => date('Y-m-d H:i:s', strtotime($h['created_at'])),
					'custom_data' => ''
				);
			}
		}	
		return $history;
	}
	function store_history($arr) {
	}
}