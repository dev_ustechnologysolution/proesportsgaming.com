<?php
class Product_model extends CI_Model 
{
	public function __construct(){
		parent::__construct();
        $this->load->model('General_model');  
	}
    function getCategories(){
        $results = $this->db
                ->select('id,parent_id,name,sort_num')
                ->order_by('sort_num','asc')
                ->where('user_id',0)
                ->get('product_categories_tbl')
                ->result_array();
        return $results;
    }
    //api get main categories
     function get_all_Categories($user_id){
        $results = $this->db
                ->select('id,parent_id,name,sort_num,status')
                ->where('user_id',$user_id)
                ->where('parent_id',0)
                ->order_by('sort_num','asc')
                ->get('product_categories_tbl')
                ->result_array();
              
        return $results;
    }
    //api for subcategory
    function getCategoryByParent($parent_id){
        $results = $this->db
                ->select('id,parent_id,name,sort_num,status')
                ->where('parent_id',$parent_id)
                ->order_by('sort_num','asc')
                ->get('product_categories_tbl')
                ->result_array();
        return $results;
    }
    function getCategoryTree($idField='id', $parentIdField='parent_id', $childrenField='children') {
        $results = $this->getCategories();
        $hierarchy = array(); 
        $itemReferences = array(); 

        foreach ( $results as $item ) {
            $id       = $item[$idField];
            $parentId = $item[$parentIdField];
            if (isset($itemReferences[$parentId])) { 
                $itemReferences[$parentId][$childrenField][$id] = $item; 
                $itemReferences[$id] =& $itemReferences[$parentId][$childrenField][$id]; 
            } elseif (!$parentId || !isset($hierarchy[$parentId])) { 
                $hierarchy[$id] = $item;
                $itemReferences[$id] =& $hierarchy[$id];
            }
        }
        unset($results, $item, $id, $parentId);
        
        foreach ( $hierarchy as $id => &$item ) {
            $parentId = $item[$parentIdField];
            if ( isset($itemReferences[$parentId] ) ) { 
                $itemReferences[$parentId][$childrenField][$id] = $item; 
                unset($hierarchy[$id]); 
            }
        }
        unset($itemReferences, $id, $item, $parentId);
        return $hierarchy;
    }
    function getSubCarArr($arr) {
        $this->db->select('GROUP_CONCAT(pct.name SEPARATOR ", ") AS subcate_name_arr, GROUP_CONCAT(pct.id SEPARATOR ", ") AS cate_id_arr');
        $this->db->from('product_categories_tbl pct');
        ($arr['main_cat'] != '') ? $this->db->where('pct.parent_id',$arr['main_cat']) : '';
        $this->db->order_by('pct.sort_num','asc');
        $this->db->where('pct.parent_id !=',0);
        return $this->db->get()->row();
    }
    function SearchProductArr($arr) {
        ($arr['get_last_id'] == 'yes') ? $this->db->select('MAX(pt.id) as last_id, COUNT(pt.id) as count') : $this->db->select('pt.*, pi.image');
        $this->db->from('products_tbl pt');
        ($arr['get_last_id'] != 'yes') ? $this->db->join('product_images pi', 'pt.id = pi.product_id','left outer') : '';
        ($arr['main_cat'] != '') ? $where = "FIND_IN_SET('".$arr['main_cat']."', pt.category_id)" : '';
        ($arr['main_sub_cat'] != '') ? $where .= " AND FIND_IN_SET('".$arr['main_sub_cat']."', pt.category_id)":'';
        (isset($where) && $where !='') ? $this->db->where($where) : '';
        ($arr['product_srch_term'] !='') ? $this->db->where("pt.name LIKE '%".$arr['product_srch_term']."%'") : '';
        ($arr['product_id'] !='') ? $this->db->where('pt.id',$arr['product_id']): '';
        ($arr['last_id'] != '') ? $this->db->where('pt.product_order >'.$arr['last_id']) : '';
        ($arr['seller_id'] != '') ? $this->db->where('pt.seller_id',$arr['seller_id']) : '';
        ($arr['limit'] != 0) ? $this->db->limit($arr['limit']) : '';
        ($arr['get_last_id'] != 'yes') ? $this->db->group_by('pi.product_id') : '';

        ($arr['order_by'] != '') ? $this->db->order_by($arr['order_by']) : $this->db->order_by('id','asc');
        $query = $this->db->get();
        $res_arr = $query->result_array();
        // echo "<pre>a";
        // print_r($res_arr);
        // exit();
        return $res_arr;
    }
    function productDetail($arr) {
        $this->db->select('pt.*, pi.is_default, pi.image, pi.id as image_id, ud.local_product_fee, ud.international_product_fee, ud.display_name, ud.name as username, ud.country, cn.sortname, cn.countryname');
        $this->db->from('products_tbl pt');
        $this->db->join('product_images pi', 'pi.product_id = pt.id', 'left outer');
        $this->db->join('user_detail ud', 'ud.user_id = pt.seller_id', 'left outer');
        $this->db->join('country cn', 'cn.country_id = ud.country', 'left outer');
        (isset($arr['product_id']) && $arr['product_id'] !='') ? $this->db->where('pt.id',$arr['product_id']) : '';
        (isset($arr['is_default']) && $arr['is_default'] !='') ? $this->db->where('pi.is_default',$arr['is_default']) : '';
        (isset($arr['seller_id']) && $arr['seller_id'] !='') ? $this->db->where('pt.seller_id',$arr['seller_id']) : '';
        (isset($arr['order_by']) && $arr['order_by'] !='') ? $this->db->order_by($arr['order_by']) : '';
        (isset($arr['group_by']) && $arr['group_by'] !='') ? $this->db->group_by($arr['group_by']) : '';
        // seller_id
        $prdct_dtl = (isset($arr['get']) && $arr['get'] == 'row') ? $this->db->get()->row() : $this->db->get()->result();
        // ($prdct_dtl->seller_id == 0) ? $prdct_dtl->country = 231 : '';
        return $prdct_dtl;
    }

     //get_local_tax api
    function get_local_tax($user_id)
    {
        $this->db->select('local_product_fee');
        $this->db->from('user_detail');
        $this->db->where('user_id',$user_id);
        $row = $this->db->get()->row();
        return $row;
    }

    //check custom tag api
    function get_international_tax($user_id)
    {
        $this->db->select('international_product_fee');
        $this->db->from('user_detail');
        $this->db->where('user_id',$user_id);
        $row = $this->db->get()->row();
        return $row;
    }

    //check custom tag api
    function check_tag($id) {
        $this->db->select('id,customizable_tag,customizable_tag_description');
        $this->db->from('products_tbl');
        $this->db->where('id',$id);
        $this->db->where('is_customizable',1);
        $this->db->where('customizable_tag !=','');
        $row = $this->db->get()->row();
        return $row;
    }
    function attributes_list($arr='') {
        $this->db->select('at.title, at.type, paot.extra_amount, paot.attr_id, paot.option_label, paot.option_id');
        $this->db->from('product_attributes_tbl pat');
        $this->db->join('attributes_tbl at', 'at.id = pat.attribute_id');
        $this->db->join('product_attributes_opt_tbl paot', 'paot.option_id = pat.option_id');
        $this->db->where('at.status',1);
        ($arr['product_id'] != '') ? $this->db->where('pat.product_id',$arr['product_id']) : '';
        $this->db->group_by('pat.option_id');
        $this->db->order_by('at.attr_order','asc');
        $this->db->order_by('paot.opt_order','asc');
        $attributes_list = $this->db->get()->result();
        return $attributes_list;
    }
    function get_attribute_data($arr = '') {
        $this->db->select('at.title, at.type, paot.extra_amount, paot.attr_id, paot.option_label, paot.option_id');
        $this->db->from('product_attributes_opt_tbl paot');
        $this->db->join('attributes_tbl at', 'at.id = paot.attr_id');
        ($arr['attribute_id'] != '') ? $this->db->where('paot.attr_id',$arr['attribute_id']) : '';
        ($arr['option_id'] != '') ? $this->db->where('paot.option_id',$arr['option_id']) : '';
        $this->db->group_by('paot.option_id');
        $this->db->order_by('at.attr_order','asc');
        $this->db->order_by('paot.opt_order','asc');
        $data = $this->db->get()->result();
        return $data;
    }
    function get_my_product($arr=''){
        $select = 'ot.paypal_deducted,ot.card_deducted,ot.wallet_deducted, ot.id, ot.transaction_id, ot.sold_by, ot.ref_seller_id, ot.tracking_id, ot.user_id, ot.shipping_address, ot.billing_address, ot.sub_total, ot.shipping_charge, ot.grand_total, ot.payment_method, ot.order_status, ot.created_at';
        $select .= ($arr['id'] != '') ? ', oit.product_fee, oit.file_name, oit.order_id as order_id, oit.product_id, oit.product_name, oit.product_image, oit.qty, oit.amount, oit.product_attributes, oit.sel_variation_amount' : ', COUNT(oit.order_id) as count_order';
        $this->db->select($select);
        $this->db->from('orders_tbl ot');
        $this->db->join('orders_items_tbl oit', 'oit.order_id = ot.id');
        // $this->db->join('products_tbl pt', 'pt.id = oit.product_id');
        ($arr['id'] != '') ? $this->db->where('oit.order_id',$arr['id']) : $this->db->group_by('oit.order_id');
        ($arr['user_id'] !='') ? $this->db->where('ot.user_id',$arr['user_id']) : '';
        ($arr['ref_seller_id'] !='') ? $this->db->where('ot.ref_seller_id',$arr['ref_seller_id']) : '';
        ($arr['order_by'] !='') ? $this->db->order_by('oit.created_at','desc'):'';
        $res = $this->db->get()->result();
        return $res;
    }
    // Category List
    function category_list($arr='') {
        $this->db->select('id,parent_id,name,sort_num,status');
        $this->db->order_by('id','asc');
        ($arr['parent_id'] !='')? $this->db->where('parent_id',$arr['parent_id']):'';
        ($arr['user_id'] !='')? $this->db->where('user_id',$arr['user_id']):'';
        $this->db->order_by('sort_num','asc');
        $category_list = $this->db->get('product_categories_tbl')->result_array();
        return $category_list;
    }
    // Remove items from cart
    function remove_item($id, $type) {
        if ($type == 'single_product') {
            if ($this->session->userdata('user_id') !='')
                $this->General_model->delete_data('cart_item', array('id' => $id));
            else
                unset($_SESSION['cart_item_list'][$id]);
            
            $delprdct_fromcart = true;
        } else {
            if ($this->session->userdata('user_id') !='')
                $this->General_model->delete_data('cart', array('user_id' => $this->session->userdata('user_id')));
            else
                unset($_SESSION['cart_item_list']);

            $delprdct_fromcart = true;
        }
        return $delprdct_fromcart;
    }
    //for remove item from cart  api
    function remove_item_cart($id, $type) {
      
        if ($type == 'single_product')
        {
            $this->General_model->delete_data('cart_item', array('id' => $id));
            $delprdct_fromcart = true;
        } 
       
        return $delprdct_fromcart;
    }

    //for clear cart api  
    function clear_cart($user_id, $type) {
      
        if ($type == 'all_product')
        {
            $this->General_model->delete_data('cart', array('user_id' =>$user_id));
            $delprdct_fromcart = true;
        } 
       
        return $delprdct_fromcart;
    }
    function store_levels($arr='') {
        $this->db->select('*');
        $this->db->from('store_levels');
        // (!empty($arr['where_in_column'])) ? $this->db->where_in($arr['where_in_column'],$arr['where_in_value']) : '';
        // $this->db->order_by('order_id');
        $res = $this->db->get()->result_array(); 
        return $res;
    }
    function get_store_level_users($arr = '') {
        $this->db->select('uli.*, sl.item_sold as tg_item_sold, ud.display_name, ud.name, ud.total_balance, ud.user_id, ud.number, ud.account_no');
        $this->db->from('user_level_items uli');
        $this->db->join('store_levels sl','uli.level = sl.level');
        $this->db->join('user_detail ud','ud.user_id = uli.user_id');
        (!empty($arr['level_id'])) ? $this->db->where('uli.level', $arr['level_id']) : '';
        $this->db->group_by('uli.level');
        $list = $this->db->get()->result_array();
        return $list;
    }
    function base64_to_jpeg($arr) {
        if (!empty($arr['file_dir']) && !empty($arr['extension']) && !empty($arr['base64_string'])) {
            $ran_string = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(8/strlen($x)) )),1,16);
            $img = time().'_uniquestr-'.$ran_string.'.'.$arr['extension'];
            $ifp = fopen($arr['file_dir'].$img, 'wb');
            $data = explode(',', $arr['base64_string']);
            $w = fwrite($ifp, base64_decode($data[1]));
            if ($w) {
                fclose($ifp);
                return $img;
            }
        }
    }
}