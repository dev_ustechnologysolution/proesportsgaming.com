<?php 
class Tournaments_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}
	function get_tournaments_event() {
		$this->db->select('asg.sub_game_name, l.id as lobby_id, l.bg_color, l.registration_status, l.user_id as lobby_creator, l.lobby_order, l.device_id, l.event_image, ag.game_category_id as game, l.is_spectator, l.game_type, l.price, l.event_key, l.is_event, l.event_image, l.game_id, gi.game_image, ag.game_name, ag.game_description, l.created_at, l.event_title as title, l.event_description as description, l.event_bg_img, l.event_start_date as start, l.event_end_date as end, l.registration_status, ud.common_settings, ud.is_admin as proplayer, ud.image as pro_img, ud.common_settings, ek.key_title, ek.key_image, agc.cat_img, ud.timezone as creator_timezone,l.is_archive');
		$this->db->from('lobby l');
		$this->db->join('user_detail ud','l.user_id = ud.user_id');
		$this->db->join('game_image gi','gi.id = l.image_id');
		$this->db->join('admin_game ag', 'ag.id = l.game_id');
		$this->db->join('admin_game_category agc','agc.id = ag.game_category_id');
		$this->db->join('admin_sub_game asg','asg.id = l.sub_game_id');
		$this->db->join('group_bet gb','gb.lobby_id = l.id');
		$this->db->join('event_keys ek','ek.id = l.event_key', 'left');
		$this->db->where("l.payment_status='1' AND l.status='1'");
		$this->db->where("gb.bet_status", 1);
		$this->db->where('l.is_event', 1);
		// $this->db->where('l.event_title !=', NULL);
		$this->db->order_by('l.event_start_date','desc');
		$this->db->order_by('l.registration_status','desc');
		// $this->db->order_by('l.lobby_order','asc');
		$getitems = $this->db->get()->result();
		return $getitems;
	}
	function archive_get_tournaments_event() {
		$this->db->select('asg.sub_game_name, l.id, l.bg_color, l.registration_status, l.user_id as lobby_creator, l.lobby_order, l.device_id, l.event_image, ag.game_category_id as game, l.is_event, l.is_spectator, l.is_archive, l.event_price, l.spectate_price, l.game_type, l.price, l.event_key, l.event_image, l.game_id, gi.game_image, ag.game_name, ag.game_description, l.created_at, l.event_title as title, l.event_description as description, l.event_bg_img, l.event_start_date as start, l.event_end_date as end, l.registration_status, ud.common_settings, ud.is_admin as proplayer, ud.image as pro_img, ud.common_settings, ek.key_title, ek.key_image, agc.cat_img, ud.timezone as creator_timezone, lf.stream_status, lf.stream_type, lf.obs_stream_channel, lf.stream_channel, "lobby_event" as event_type');
		$this->db->from('lobby l');
		$this->db->join('user_detail ud','l.user_id = ud.user_id');
		$this->db->join('game_image gi','gi.id = l.image_id');
		$this->db->join('admin_game ag', 'ag.id = l.game_id');
		$this->db->join('admin_game_category agc','agc.id = ag.game_category_id');
		$this->db->join('admin_sub_game asg','asg.id = l.sub_game_id');
		$this->db->join('lobby_fans lf','lf.lobby_id = l.id AND lf.user_id = l.user_id');
		$this->db->join('group_bet gb','gb.lobby_id = l.id');
		$this->db->join('event_keys ek','ek.id = l.event_key', 'left');
		$this->db->where("l.payment_status='1' AND l.status='1'");
		$this->db->where("gb.bet_status", 1);
		$this->db->where('l.is_event', 1);
		// $this->db->where('l.is_archive', 0);
		// $this->db->where('l.event_title !=', NULL);
		$this->db->order_by('l.event_start_date','asc');
		$this->db->order_by('l.registration_status','desc');
		// $this->db->order_by('l.lobby_order','asc');
		$getitems = $this->db->get()->result();
		return $getitems;
	}
	function get_custom_events() {
		$this->db->select('ce.id, ce.title, ce.event_bg_img, ce.start, ce.description, ce.url, ce.bg_color, ce.end, ce.event_key, ek.key_title, ek.key_image, cad.timezone as creator_timezone, "custom_event" as event_type ');
		$this->db->from('custom_events ce');
		$this->db->join('event_keys ek','ek.id = ce.event_key', 'left');
		$this->db->join('cre_admin_detail cad','cad.user_id = ce.admin_user_id');
		$getitems = $this->db->get()->result();
		return $getitems;
	}
	function event_start_end_date($arr='') {
		if ($arr['event_type'] == 'lobby_event') {
			$this->db->select('l.event_start_date, l.event_end_date, ud.timezone, ud.common_settings');
			$this->db->from('lobby l');
			$this->db->join('user_detail ud','ud.user_id = l.user_id');
			$this->db->where('l.id',$arr['id']);
		} else {
			$this->db->select('ce.start, ce.end, cad.timezone');
			$this->db->from('custom_events ce');
			$this->db->join('cre_admin_detail cad','cad.user_id = ce.admin_user_id');
			$this->db->where('ce.id',$arr['id']);
		}
		$result = $this->db->get()->result();
		return $result;	
	}
	function get_key_list($arr) {
		$this->db->select('ek.*');
		$this->db->from('event_keys ek');
		($arr['id'] != '') ? $this->db->where('ek.id',$arr['id']) : '';
		$this->db->order_by('ek.order');
		$sql = $this->db->get();
		$getitems = $sql->result();
		return $getitems;
	}
	function getdefault_keylist($arr) {
		$this->db->select('dkc.*');
		$this->db->from('default_key_selected dkc');
		($arr['id'] != '') ? $this->db->where('dkc.id',$arr['id']) : '';
		($arr['user_id'] != '') ? $this->db->where('dkc.user_id',$arr['user_id']) : '';
		$sql = $this->db->get();
		$getitems = $sql->result();
		return $getitems;
	}
	function get_tournament_users($arr = '') {
		$this->db->select('eu.id, l.event_price, l.event_fee, l.spectator_fee, l.spectate_price, l.event_fee_creator, l.is_new_lobby, l.is_event_start, ud.account_no, ud.name, ud.display_name_status, ag.game_name as lobby_name, ud.display_name, ud.account_no, ud.image, u.email, ud.team_name, eu.user_id, eu.is_gift, eu.gift_to, eu.type, eu.created, eu.lobby_id, eu.deducted_event_price, eu.deducted_event_fee, eu.applied_discount_on_tickets, lf.team_name as lobby_team_name, lf.fan_tag as lobby_fan_tag');
		$this->db->from('events_users eu');
		$this->db->join('user_detail ud','ud.user_id = eu.user_id');
		$this->db->join('user u','u.id = ud.user_id');
		$this->db->join('lobby l', 'l.id = eu.lobby_id');
		$this->db->join('lobby_fans lf', 'lf.user_id = eu.user_id AND lf.lobby_id = eu.lobby_id', 'LEFT OUTER');
		$this->db->join('admin_game ag','ag.id = l.game_id');
		(!empty($arr['is_remove'])) ? $this->db->where("eu.is_remove",$arr['is_remove']) : $this->db->where("eu.is_remove",0);
		(!empty($arr['lobby_id'])) ? $this->db->where("eu.lobby_id",$arr['lobby_id']) : '';
		(!empty($arr['type'])) ? $this->db->where("eu.type",$arr['type']) : '';
		(!empty($arr['order'])) ? $this->db->order_by($arr['order'],'asc') : $this->db->order_by('eu.created','desc');
		(!empty($arr['start_date']) && !empty($arr['end_date'])) ? $this->db->where('eu.created >= "'.$arr['start_date'].'" AND eu.created <= "'.$arr['end_date'].'"') : '';
		$getitems = $this->db->get()->result();
		return $getitems;
	}
}
