<?php

class Video_model extends CI_Model {

	public function __construct()

    {

	  parent::__construct();

	}

  	function video_detail() {

	$this->db->select('v.id,v.game_id,v.video_url,g.name');

    $this->db->from('video v');

    $this->db->join('game g', 'v.game_id=g.id');

	// $this->db->where('v.user_id', $this->session->userdata('user_id'));

    $query = $this->db->get();

	return $query->result_array();

	}

}

?>