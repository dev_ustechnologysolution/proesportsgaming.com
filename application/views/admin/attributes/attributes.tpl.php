<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php $this->load->view('admin/common/show_message'); ?>
            <form  action="<?php echo base_url(); ?>admin/attributes/attributeInsert" method="post" enctype="multipart/form-data" class="admin_game_edit_form">
                <div class="box box-primary" id="hidden_div" style="padding: 30px;"> 
                    <div class="box-header with-border optionheader">
                        <h4><b><?php echo 'Add '.ucfirst($attributes_data[0]['title']); ?></b></h4>
                    </div>
                    <input type="hidden" name="attr_id" value="<?php echo $attr_id; ?>">
                    <div class="sortable_div">
                        <div class="form-group row optdata_div is_draggable">
                            <label class="col-sm-2 text-right" for="name">Value </label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control optlbl" name="option_label[]" value="" required />
                            </div>
                            <label class="col-sm-1 text-left padd0" for="examount">Extra amount</label>
                            <div class="col-sm-2"><input type="number" class="form-control extraamnt" name="value_amount[]" value="0" required/></div>
                            <div class="col-sm-2">
                                <button class="btn btn-primary add-row" type="button">Add more</button>
                            </div>
                        </div>
                    </div>
                    <div class="input_fields_container"></div>
                    <div class="form-group row pb20" style="padding: 30px 0;">
                        <div class="col-sm-12" align="center">
                            <button class="btn btn-primary" type="submit" id="edit_game_update">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<script src="<?php echo base_url(); ?>assets/plugins/jQueryUI/jquery-ui.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".input_fields_container").sortable();
        // $(".input_fields_container").sortable({
        //     update  : function(event, ui) {
        //         var optordr = new Array();
        //         if (optordr.length > 0) {
        //             $(this).find('.optdata_div').each(function(index, value){
        //                 $(this).find('.opt_order').val(optordr[index].opt_order);
        //             });
        //         }
        //     }
        // });

        var x = 1;
        max_fields_limit = 10;
        $(".add-row").click(function(e){
            var val = $(this).parent('div').siblings().find('input.optlbl').val();
            val = val.replace(/\s+/g, '');
            $(this).parent('div').siblings().find('input.optlbl').val(val);
            var extraamnt = $(this).parent('div').siblings().find('input.extraamnt').val();
            extraamnt = extraamnt.replace(/\s+/g, '');
            $(this).parent('div').siblings().find('input.extraamnt').val(extraamnt);
            if (val.length > 0 && val !='' && extraamnt.length > 0 && extraamnt !='') {
                e.preventDefault();            
                if(x < max_fields_limit) {
                    x++; 
                    var nwrw = '<div class="form-group row optdata_div is_draggable remove_'+x+'" data-seq="remove_'+x+'">';
                    nwrw += '<label class="col-sm-1 text-center drag" data-toggle="tooltip" data-original-title="Drag to Change display order"><i class="fa fa-bars"></i></label>';
                    nwrw += '<label class="col-sm-1 text-right">Value</label>';
                    nwrw += '<div class="col-sm-5 "><input type="text" required class="form-control" name="option_label[]" value="'+val+'"/></div>';
                    nwrw += '<label class="col-sm-1 text-left padd0" for="examount">Extra amount</label>';
                    nwrw += '<div class="col-sm-2"><input type="number" class="form-control extraamnt" name="value_amount[]" value="'+extraamnt+'" required/></div>';
                    nwrw += '<div class="col-sm-1 "><button class="btn btn-danger remove_field" type="button">Remove</button></div></div>';

                    $('.input_fields_container').prepend(nwrw); 
                    $(this).parent('div').siblings().find('input.optlbl').val('');
                    $(this).parent('div').siblings().find('input.extraamnt').val(0);
                } else {
                    $('.add-row').attr('disabled','true');
                }
            } else {
                $(this).closest('form').find('[type="submit"]').click();
            }
        });
        $('.input_fields_container').on("click",".remove_field", function(e){ 
            e.preventDefault();
            data = $(this).closest('[data-seq]').data('seq'); 
            $('.'+data).remove(); 
            x--;
        })        
    }); 
    function showDiv(divId, element) {
        document.getElementById(divId).style.display = element.value == 3 ? 'block' : 'none';
    }   
</script>
<style type="text/css">
    .optionheader { margin: 30px 6px; }
    #hidden_div { padding-bottom: 20px; }
</style>