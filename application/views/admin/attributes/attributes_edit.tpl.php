<link rel="stylesheet" href="<?php echo base_url().'assets/plugins/jQueryUI/jquery-ui.min.css'; ?>">
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php $this->load->view('admin/common/show_message') ?>
            <form action="<?php echo base_url(); ?>admin/attributes/attributeUpdate" method="post" enctype="multipart/form-data" class="admin_game_edit_form">
                <input type="hidden" name="id" value="<?php echo $attributes_data[0]['id']; ?>">
                <div class="box box-primary" id="hidden_div" style="padding: 30px;"> 
                    <div class="box-header with-border optionheader">
                        <h4>
                            <b><?php echo "Update ".ucfirst($attributes_data[0]['title']); ?></b>
                            <a class="btn btn-primary pull-right" href="<?php echo base_url().'admin/attributes/attributeAdd?id='.$attributes_data[0]['id']; ?>"> Add new <?php echo ucfirst($attributes_data[0]['title']); ?></a>
                        </h4>
                    </div>
                    <div class="box-body">
                        <div class="form-group row">
                            <label class="col-sm-1 text-right padd0" for="title">Attribute Title </label>
                            <div class="col-sm-4">
                                <input class="form-control" type="text" name="title" value="<?php echo $attributes_data[0]['title']; ?>">
                            </div>
                            <label class="col-sm-1 text-right padd0" for="attr_type">Attribute type</label>
                            <div class="col-sm-4">
                                <select class="form-control" name="select_attr_type" required="">
                                  <option value="">Please select attribute type</option>
                                  <option value="1" <?php echo ($attributes_data[0]['type'] == 1)? 'selected':''; ?>>Checkbox</option>
                                  <option value="2" <?php echo ($attributes_data[0]['type'] == 2)? 'selected':''; ?>>Radio button</option>
                                  <option value="3" <?php echo ($attributes_data[0]['type'] == 3)? 'selected':''; ?>>Dropdown</option>
                                </select>
                            </div>
                            <div class="col-sm-1"><label>Image Upload</label></div>
                            <div class="col-sm-1"><label class="switch common_switch"><input type="checkbox" name="upload_img_opt" <?php echo (!empty($attributes_data[0]['image_upload']) && $attributes_data[0]['image_upload'] == 1) ? 'checked' : ''; ?> class=""><span class="slider round"></span></label></div>
                        </div>
                        <hr class="text-center" style="width: 90%;">
                        <div class="sortable_div">
                            <?php $k = 1; foreach($option_data as $key => $v) { ?>
                                <div class="form-group clearfix remove_<?php echo $k; ?> optdata_div is_draggable" data-seq="remove_<?php echo $k; ?>" ordr="<?php echo $v['opt_order']; ?>">
                                    <label class="col-sm-1 text-center drag" data-placement="bottom" data-toggle="tooltip" data-original-title="Drag to Change display order"><i class="fa fa-bars"></i></label>
                                    <label class="col-sm-1 text-right" for="name">Value </label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control <?php echo ($key == 0) ? 'optlbl' : ''; ?>" name="option_label[]" value="<?php echo $v['option_label']; ?>" required/>
                                        <input type="hidden" class="form-control" name="option_data[]" value="<?php echo $v['option_id']; ?>"/>
                                        <input type="hidden" class="opt_order" name="opt_order[]" value="<?php echo $v['opt_order']; ?>"/>
                                    </div>
                                    <label class="col-sm-1 text-left padd0" for="examount">Extra amount</label>
                                    <div class="col-sm-2"><input type="number" class="form-control extraamnt" name="value_amount[]" value="<?php echo $v['extra_amount']; ?>" required/></div>
                                    <div class="col-sm-2">
                                        <button class="btn btn-danger remove_field" type="button">Remove</button>
                                    </div>
                                </div>
                                <?php $k++;
                            } ?>
                        </div>
                        <div class="input_fields_container"></div>
                        <div class="form-group row">
                            <div class="col-sm-12 text-center" align="center">
                                <button class="btn btn-primary" type="submit" id="edit_game_update">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        var optordr = <?php echo json_encode($option_data); ?>;
        $(".sortable_div").sortable({
            update  : function(event, ui) {
                if (optordr.length > 0) {
                    $(this).find('.optdata_div').each(function(index, value){
                        $(this).find('.opt_order').val(optordr[index].opt_order);
                    });
                }
            }
        });
        // $(".sortable_div").disableSelection();

        // $('form .optdata_div.is_draggable').draggable({cursor: "pointer", revert: true});
        // var x = '<?php // echo $k;?>';
        // var max_fields_limit = 10;
        // $(".add-row").click(function(e) {
        //     e.preventDefault();
        //     var val = $(this).parent('div').siblings().find('input.optlbl').val();
        //     if (x < max_fields_limit) {
        //         x++; 
        //         var nwrw = '<div class="form-group row remove_'+x+'" data-seq="remove_'+x+'"><label class="col-sm-2 text-right">Main Title</label><div class="col-sm-4 "><input type="text" required class="form-control" name="option_label[]" value="'+val+'"/></div><div class="col-sm-1 "><button class="btn btn-danger remove_field" type="button">Remove</button></div></div>';
        //         $('.input_fields_container').prepend(nwrw); 
        //         $(this).parent('div').siblings().find('input.optlbl').val('');
        //     } else {
        //         $('.add-row').attr('disabled','true');
        //     }
        // });
        $(".remove_field").click(function(e){
            e.preventDefault();
            data = $(this).closest('[data-seq]').data('seq');
            $('.'+data).remove();
        });
    });
    function showDiv(divId, element) {
        document.getElementById(divId).style.display = element.value == 3 || element.value == 2 ? 'block' : 'none';
    }   
</script>
<style type="text/css">
    .optionheader { margin: 10px 6px 30px; }
    .pb20 { padding-bottom: 20px; }
    #hidden_div { padding-bottom: 20px; }
    .block { display: block; }
    .none { display: none; }
</style>
