<section class="content">
  <div class="row">
    <div class="col-md-12">
			<?php $this->load->view('admin/common/show_message') ?>
        <div class="box box-primary">
          <form enctype="multipart/form-data">
            <div class="box-body">
              <div class="form-group">
                  <label for="name">Banner Name</label>
                  <input type="text" placeholder="Banner Name" id="name" class="form-control" name="bname">
              </div>
              <div class="form-group">
                  <label for="description">Banner Content</label>
                  <textarea class="form-control" id="description" placeholder="Banner Content" name="content"></textarea>
              </div>
              <div class="form-group" style="display:none;">
                  <label for="site_url">Banner Link</label>
                  <input type="url" placeholder="Banner Link" id="site_url" class="form-control" name="site_url">
              </div>
              <div class="form-group col-sm-12" style="padding-left: 0;">
                <div class="col-md-6" style="padding-left: 0;">
                  <label>Button1 Title</label>
                  <input type="text" class="form-control" placeholder="Banner Title" name="button1_title">
                </div>
                <div class="col-md-6">
                  <label>Button1 Link</label><input type="url" class="form-control" placeholder="Banner Link" name="button1_link">
                </div>
              </div> 
              <div class="form-group col-sm-12" style="padding-left: 0;">
                <div class="col-md-6" style="padding-left: 0;">
                  <label>Button2 Title</label>
                  <input type="text" class="form-control" placeholder="Banner Title" name="button2_title">
                </div>
                <div class="col-md-6">
                  <label>Button2 Link</label><input type="url" class="form-control" placeholder="Banner Link" name="button2_link">
                </div>
              </div> 
                
              <div class="form-group col-sm-4" style="padding-left: 0;">
                <div class="col-md-1">
                  <input type="radio" name="selected_banner" id="selected_banner_image" value="upload_image_banner" checked>
                </div>
                <div class="col-md-11">
                  <label>Banner Image</label>
                  <input type="file" class="form-control image_banner" id="file" placeholder="Image" name="image_banner" accept="image/*">
                </div>
              </div>
              <div class="form-group col-sm-4" style="padding-left: 0;">
                <div class="col-md-1">
                  <input type="radio" name="selected_banner" id="selected_banner_youtube_link" value="youtube_link">
                </div>
                <div class="col-md-11">
                  <label>Banner YouTube Link</label>
                  <input type="url" class="form-control banner_youtube_link_input" id="banner_youtube_link" placeholder="YouTube Link" name="banner_youtube_link" style="display: none;">
                  <input type="number" class="form-control banner_youtube_link_duration" id="banner_youtube_link_duration" placeholder="Youtube Video Duration" name="banner_youtube_link_duration" style="display: none; margin-top: 5px;">
                </div>
              </div>
              <div class="form-group col-sm-4" style="padding-right: 0;">
                <div class="col-md-1">
                  <input type="radio" name="selected_banner" id="selected_banner_video" value="upload_video_banner">
                </div>
                <div class="col-md-11">
                  <label>Banner Video</label>
                  <input type="file" class="form-control video_banner" id="file" placeholder="Video" name="video_banner" accept="video/mp4,video/3gp,video/ogg" style="display: none;">
                  <input type="number" class="form-control video_duration" id="video_duration" placeholder="Video Duration" name="video_duration" style="display: none; margin-top: 5px;">
                </div>
              </div>
            </div><!-- /.box-body -->
            <div class="col-sm-12">
              <div class="upload_progress_bar" style="display: none;">
                <div class="progress progress-sm active" style="display: none;">
                  <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="">
                    <span class="sr-only"></span>
                  </div>
                </div>
              </div>              
            </div>
            <div class="box-footer">
              <button class="btn btn-primary insert_banner-btn" type="button">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$('.insert_banner-btn').click(function(){
  var fileuploaddata = new FormData();
  fileuploaddata.append('bname', $('input[name="bname"]').val());
  fileuploaddata.append('content', $('textarea[name="content"]').val());
  fileuploaddata.append('site_url', $('input[name="site_url"]').val());
  fileuploaddata.append('banner_order', $('input[name="banner_order"]').val());
  fileuploaddata.append('selected_banner', $('input[name="selected_banner"]:checked').val());
  fileuploaddata.append('image_banner', $('input[name="image_banner"]').prop('files')[0]);
  fileuploaddata.append('video_duration', $('input[name="video_duration"]').val());
  fileuploaddata.append('video_banner', $('input[name="video_banner"]').prop('files')[0]);
  fileuploaddata.append('banner_youtube_link', $('input[name="banner_youtube_link"]').val());
  fileuploaddata.append('banner_youtube_link_duration', $('input[name="banner_youtube_link_duration"]').val());
  fileuploaddata.append('button2_title', $('input[name="button2_title"]').val());
  fileuploaddata.append('button1_link', $('input[name="button1_link"]').val());
  fileuploaddata.append('button2_link', $('input[name="button2_link"]').val());
  fileuploaddata.append('button1_title', $('input[name="button1_title"]').val());

  xhr = new XMLHttpRequest();
  xhr.open( 'POST', base_url +'admin/banner/bannerInsert', true );
  xhr.responseType = 'json';
  xhr.upload.onprogress = update_progress;
  xhr.onreadystatechange = function (srcData) {
    srcData = this.response;
    if (srcData.url.length != 0) {
      window.location.href = srcData.url;
    }
  };
  function update_progress(e) {
    if (e.lengthComputable) {
      $('.progress.progress-sm, .upload_progress_bar').show().addClass('active');
      var percentage = Math.round((e.loaded/e.total)*100);
      $('.progress-sm .progress-bar').css('width',percentage+"%");
    }
  }
  xhr.send(fileuploaddata);    
});
</script>