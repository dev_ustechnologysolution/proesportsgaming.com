<section class="content">
  <div class="row">
    <div class="col-md-12">
			<?php $this->load->view('admin/common/show_message'); ?>
        <div class="box box-primary">
          <form enctype="multipart/form-data">
            <div class="box-body">
              <div class="form-group">
                  <label for="name">Banner Name</label>
                  <input type="text" id="name" class="form-control" name="bname" value="<?php if(isset($banner_data[0]['banner_name']))echo $banner_data[0]['banner_name'];?>">
              </div>
              <div class="form-group">
                  <label for="description">Banner Content</label>
                  <textarea class="form-control" name="content"><?php if(isset($banner_data[0]['banner_content']))echo $banner_data[0]['banner_content'];?></textarea>
              </div>
              <div class="form-group" style="display:none;">
                  <label for="site_url">Banner Link</label>
                  <input type="url" id="site_url" class="form-control" name="site_url" value="<?php if(isset($banner_data[0]['site_url']))echo $banner_data[0]['site_url'];?>">
              </div>
              <div class="form-group col-sm-12" style="padding-left: 0;">
                <div class="col-md-6" style="padding-left: 0;">
                  <label>Button1 Title</label>
                  <input type="text" class="form-control" placeholder="Banner Title" name="button1_title" value="<?php if(isset($banner_data[0]['button1_title']))echo $banner_data[0]['button1_title'];?>">
                </div>
                <div class="col-md-6">
                  <label>Button1 Link</label>
                  <input type="url" class="form-control" value="<?php if(isset($banner_data[0]['button1_link']))echo $banner_data[0]['button1_link'];?>" placeholder="Banner Link" name="button1_link">
                </div>
              </div> 
              <div class="form-group col-sm-12" style="padding-left: 0;">
                <div class="col-md-6" style="padding-left: 0;">
                  <label>Button2 Title</label>
                  <input value="<?php if(isset($banner_data[0]['button2_title']))echo $banner_data[0]['button2_title'];?>" type="text" class="form-control" placeholder="Banner Title" name="button2_title">
                </div>
                <div class="col-md-6">
                  <label>Button2 Link</label>
                  <input value="<?php if(isset($banner_data[0]['button2_link']))echo $banner_data[0]['button2_link'];?>" type="url" class="form-control" placeholder="Banner Link" name="button2_link">
                </div>
              </div> 
              <div class="form-group upload-div col-sm-4 <?php if($banner_data[0]['banner_image'] != '') echo 'active' ?>" style="padding-left: 0;">
                <div class="col-md-1"><input type="radio" name="selected_banner" id="edit_banner_image" value="edit_banner_image" <?php if ($banner_data[0]['banner_image'] != '') echo 'checked class="checked"';?>></div>
                <div class="col-md-11"><label>Banner Image</label><a style="float: right; cursor: pointer; display: none;" class="browse_new_image">Change Image</a>
                <input type="file" class="form-control image_banner" id="file" placeholder="Image" name="image_banner" accept="image/*" style="display: none;">
                <input type="hidden" id="old_image_banner" name="old_image_banner" value="<?php echo $banner_data[0]['banner_image'];?>"></div>
                <div class="col-md-1"></div><div class="col-md-11" style="margin-top: 5px; "><img src="<?php echo base_url().'upload/banner/'.$banner_data[0]['banner_image'];?>"  class="old_image_preview" height="150" width="150" style="display: none;">
                </div>
              </div>
              <div class="form-group col-sm-4" <?php if($banner_data[0]['banner_youtube_link'] != '') echo 'active' ?> style="padding-left: 0;">
                <div class="col-md-1">
                  <input type="radio" name="selected_banner" id="selected_banner_youtube_link" value="youtube_link" <?php echo ($banner_data[0]['banner_youtube_link'] != '') ? 'checked class="checked"' : '';?>>
                </div>
                <div class="col-md-11">
                  <label>Banner YouTube Link</label>
                  <input type="url" class="form-control banner_youtube_link_input" id="banner_youtube_link" placeholder="YouTube Link" name="banner_youtube_link" value="<?php echo $banner_data[0]['banner_youtube_link'];?>">
                  <input type="number" class="form-control banner_youtube_link_duration" id="banner_youtube_link_duration" placeholder="Youtube Video Duration" name="banner_youtube_link_duration" value="<?php echo $banner_data[0]['banner_youtube_link_duration'];?>" style="display: none; margin-top: 5px;">
                </div>
              </div>              
              <?php 
              $banner_video_duration = ($banner_data[0]['banner_video_duration'] == '0')? '': $banner_data[0]['banner_video_duration'];
              ?>
              <div class="form-group upload-div col-sm-4 <?php if($banner_data[0]['banner_video'] != '') echo 'active' ?>" style="padding-right: 0;">
                <div class="col-md-1">
                  <input type="radio" name="selected_banner" id="edit_banner_video" value="edit_banner_video" <?php if($banner_data[0]['banner_video'] != '') echo 'checked class="checked"';?>>
                </div>
                <div class="col-md-11">
                  <label>Banner Video</label>
                  <a style="float: right; cursor: pointer; display: none;" class="browse_new_video">Change Video</a>
                  <input type="file" class="form-control video_banner" id="file" placeholder="Video" name="video_banner" accept="video/mp4,video/3gp,video/ogg" style="display: none;">
                  <input type="number" class="form-control video_duration" id="old_video_duration" name="old_video_duration" placeholder="Video Duration" style="display: none; margin-top: 5px;" value="<?php echo $banner_video_duration;?>">
                  <input type="hidden" class="form-control new_video_banner" name="old_video_banner" value="<?php echo $banner_data[0]['banner_video'];?>">
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-11" style="margin-top: 5px; ">
                  <span class="old_video_preview" style="display: none;"><b>Video :</b> <?php echo $banner_data[0]['banner_video']; ?></span>
                </div>
              </div>

            </div><!-- /.box-body -->
            <div class="col-sm-12">
              <div class="upload_progress_bar" style="display: none;">
                <div class="progress progress-sm active" style="display: none;">
                  <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="">
                    <span class="sr-only"></span>
                  </div>
                </div>
              </div>              
            </div>
            <div class="box-footer">
              <input type="hidden" class="form-control" name="banner_order" value="<?php echo $banner_data[0]['banner_order'];?>">
              <input type="hidden" name="id" value="<?php if(isset($banner_data[0]['id'])) echo $banner_data[0]['id'];?>">
              <button class="btn btn-primary upload_bannerform-btn" type="button">Update</button>
            </div>
          </form>
        </div>
      </div>
    </div>
</section>
<script type="text/javascript">
$('.upload_bannerform-btn').click(function(){
  var fileuploaddata = new FormData();    
  fileuploaddata.append('file', $('input[name="video_banner"]').prop('files')[0]);
  fileuploaddata.append('image_banner', $('input[name="image_banner"]').prop('files')[0]);
  fileuploaddata.append('bname', $('input[name="bname"]').val());
  fileuploaddata.append('content', $('textarea[name="content"]').val());
  fileuploaddata.append('site_url', $('input[name="site_url"]').val());
  fileuploaddata.append('selected_banner', $('input[name="selected_banner"]:checked').val());
  fileuploaddata.append('old_image_banner', $('input[name="old_image_banner"]').val());
  fileuploaddata.append('old_video_duration', $('input[name="old_video_duration"]').val());
  fileuploaddata.append('old_youtube_link', $('input[name="old_youtube_link"]').val());
  fileuploaddata.append('old_video_banner', $('input[name="old_video_banner"]').val());
  fileuploaddata.append('banner_order', $('input[name="banner_order"]').val());
  fileuploaddata.append('button1_title', $('input[name="button1_title"]').val());
  fileuploaddata.append('banner_youtube_link', $('input[name="banner_youtube_link"]').val());
  fileuploaddata.append('banner_youtube_link_duration', $('input[name="banner_youtube_link_duration"]').val());
  fileuploaddata.append('id', $('input[name="id"]').val());
  fileuploaddata.append('button2_title', $('input[name="button2_title"]').val());
  fileuploaddata.append('button1_link', $('input[name="button1_link"]').val());
  fileuploaddata.append('button2_link', $('input[name="button2_link"]').val());
  fileuploaddata.append('button1_title', $('input[name="button1_title"]').val());

  xhr = new XMLHttpRequest();
  xhr.open( 'POST', base_url +'admin/banner/bannerUpdate', true );
  xhr.responseType = 'json';
  xhr.upload.onprogress = update_progress;
  xhr.onreadystatechange = function (srcData) {
    srcData = this.response;
    if (srcData.url.length != 0 && srcData.url != undefined && srcData.url != null) {
      window.location.href = srcData.url;
    }
  };
  function update_progress(e) {
    if (e.lengthComputable) {
      $('.progress.progress-sm, .upload_progress_bar').show().addClass('active');
      var percentage = Math.round((e.loaded/e.total)*100);
      $('.progress-sm .progress-bar').css('width',percentage+"%");
    }
  }
  xhr.send(fileuploaddata);
});
</script>