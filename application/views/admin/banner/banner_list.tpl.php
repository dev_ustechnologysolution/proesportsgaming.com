<style>
.none { display: none; }
.block { display: block; }
</style>
<link rel="stylesheet" href="<?php echo base_url().'assets/plugins/datatables/rowReorder.dataTables.min.css'; ?>">
<div class="pull-left status_button col-sm-12 banner_page">
  <b>Live Lobby Banner</b> &nbsp;&nbsp;<label class="switch"><input type="checkbox" name="lobby_status" value="lobby_status" <?php echo ($lobby_status['option_value'] == 'on') ? 'checked' : ''; ?> class="lobby_status"><span class="slider round"></span></label>
</div>
<input type="hidden" id="lobby_st" value="<?php echo $lobby_status['option_value']; ?>"> 
<section class="content banner_tab_main_section" style="margin-top:30px;">
  <?php $this->load->view('admin/common/show_message'); ?>
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs" role="tablist">
      <li class="<?php echo ($active_tab == 'lobby_event') ? 'active' : '';?>"><a href="#eventtab" role="tab" data-toggle="tab">Event List</a></li>
      <li class="<?php echo ($active_tab == 'lobby') ? 'active' : '';?>"><a href="#live_streambanner" role="tab" data-toggle="tab">Live Stream Banners</a></li>
      <li class="<?php echo ($active_tab == 'banner') ? 'active' : '';?>"><a href="#banner_tab" role="tab" data-toggle="tab">Banner List</a></li>
      <li class="<?php echo ($active_tab == 'off_streamer_event') ? 'active' : '';?>"><a href="#off_streamer_event" role="tab" data-toggle="tab">Off Stream Lobbies</a></li>
       <li class="<?php echo ($active_tab == 'archive_event') ? 'active' : '';?>"><a href="#archive_event" role="tab" data-toggle="tab">Archive Event</a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane <?php echo ($active_tab == 'lobby_event') ? 'active' : '';?>" id="eventtab">
        <div class="banner_page2">
          <div class="box-header">
            <div class="pull-right">
              <button class="btn btn-primary btn-xl modal-live-stream_opt">Global Banner OFF</button>
              <button class="btn btn-primary btn-xl edit-data" action="edit_glbl_evefee">Global Event Fee</button>
              <a href="<?php echo base_url().'admin/banner/allevent_calendar'; ?>" class="btn btn-primary btn-xl" title="View Spectate Users"> <i class="fa fa-calendar"></i>&nbsp;&nbsp;Daywise events </a>
            </div>
          </div>
          <div class="box-body">
            <table class="table table-bordered table-striped table-responsive custom_datatable" table_data="event_list" order_column="4">
              <thead>
                <tr>
                  <!-- <th>Lobby Strem Order</th> -->
                  <th>Lobby ID</th>
                  <th>Lobby Title</th>
                  <th>Event Date</th>
                  <th>Creator</th>
                  <th>Tag</th>
                  <th>Link</th>
                  <th>Is Banner</th>
                  <th>Is Event</th>
                  <th>Event Image</th>
                  <th>Action</th>
                  <th>Is Spectator</th>
                  <th>Spectator</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($event_lobby_data as $k => $lobby) {
                  $creator_arr = $this->General_model->view_single_row('user_detail','user_id',$lobby->user_id);
                  ($lobby->is_event == 1) ? $is_event_arr[] =   $lobby->user_id : '';
                  $arr = explode(',',$lobby->channel);
                  $count = 0;
                  
                  foreach ($arr as $a) {
                    $sub_arr = explode('*_*+',$a);
                    if ($sub_arr[1] == 1) {
                      $count = 1;
                    }
                  }
                  $is_banner_class = ($count == 0) ? 'no_banner' : $is_banner_class = 'yes_banner'; ?>
                  <tr>
                    <!-- <td title="Drag & Drop for change Order"><i class="fa fa-bars" style="padding-right: 10px;"></i><?php //echo ($lobby->lobby_banner_order == 0) ? '' : $lobby->lobby_banner_order; ?> --><!-- <a href="#" class="lobby_view" id="<?php //echo $lobby->lobby_id; ?>" style="margin-left: 5px;"><i class="fa fa-pencil fa-fw"></i></a> --><!-- </td> -->
                    <td><?php echo $lobby->lobby_id; ?></td>
                    <td><?php echo $lobby->game_name; ?></td>
                    <td><?php echo $this->User_model->custom_date(array('date' => $lobby->event_start_date,'format' => 'Y-m-d')); ?></td>
                    <td><?php echo ucwords($creator_arr['name']); ?></td>
                    <td><?php echo $lobby->device_id?></td>
                    <td><a target="_blank" class="btn btn-primary" href="<?php echo base_url('livelobby/watchLobby/'.$lobby->lobby_id); ?>">Live Lobby</td> 
                    <td><p class="<?php echo $is_banner_class; ?>"><span class="hide"><?php echo ($count == 0) ? 'ON' : 'OFF'; ?></span></p></td>
                    <td><span class="hide"><?php echo ($lobby->is_event == 1) ? 'ON' :'OFF'; ?></span><label class="myCheckbox"><input <?php echo ($lobby->is_event == 1) ? 'checked' :''; ?> id="is_event<?php echo $k;?>" class="single-checkbox is_event custom-cb" type="checkbox" name="progress" value="<?php echo $lobby->lobby_id;?>"><span></span></label></td>
                    <td>
                      <a style="cursor: pointer;" title="Update Event Image" onclick="return changeEventprice('<?php echo $lobby->lobby_id; ?>','event_price');"><?php ($lobby->event_image == '') ? $img = 'no-image.png' : $img = $lobby->event_image;?><img style="border: 1px solid gray;border-radius: 12px;" src="<?php echo base_url().'upload/banner/'.$img; ?>" width="125"></a>
                    </td>
                    <td style="min-width: 200px;">
                      <a class="btn btn-primary actn_btn" title="Edit Banner" href="<?php echo base_url().'admin/banner/lobbyBannerEdit?id='.$lobby->lobby_id.'&name='.$lobby->game_name;?>"><i class="fa fa-pencil fa-fw"></i></a>  <a class="btn btn-primary actn_btn" title="Update Event Ticket Amount" onclick="return changeEventprice('<?php echo $lobby->lobby_id; ?>','event_price');"><i class="fa fa-money fa-fw"></i></a>  <a class="btn btn-primary actn_btn" title="List Event Users" href="<?php echo base_url().'admin/banner/getEventsUsers?id='.$lobby->lobby_id;?>"><i class="fa fa-address-book fa-fw"></i></a> <a class="btn btn-primary delete_lobby delete-data actn_btn" id="<?php echo $lobby->lobby_id; ?>" custom-type="delete" action="delete_live_lobby_banner" title="Delete Lobby" winre_loc="<?php echo base_url().'admin/banner/delete_lobby/'.$lobby->lobby_id; ?>"><i class="fa fa-trash fa-fw"></i></a>
                      <a class="btn btn-primary delete_lobby delete-data actn_btn" id="<?php echo $lobby->lobby_id; ?>"   title="Archive" winre_loc="<?php echo base_url().'admin/banner/archive_lobby/'.$lobby->lobby_id; ?>"><i class="fa fa-archive fa-fw"></i></a>
                    </td>
                    <td>
                      <div class="pull-left col-sm-12 banner_page">
                        <label class="switch">
                          <input type="checkbox" name="is_" value="<?php echo $lobby->lobby_id; ?>" <?php echo ($lobby->is_spectator == 1) ? 'checked' : ''; ?> class="is_spectator"><span class="slider round"></span>
                        </label>
                      </div>
                    </td>
                    <td style="min-width: 120px;">
                      <a class="btn btn-primary actn_btn" title="Update Spectate Ticket Amount" onclick="return changeEventprice('<?php echo $lobby->lobby_id; ?>','spectate_price');"><i class="fa fa-money fa-fw"></i></a>  <a class="btn btn-primary actn_btn" title="List Spectate Users" href="<?php echo base_url().'admin/banner/getSpectateUsers?id='.$lobby->lobby_id;?>"><i class="fa fa-address-book fa-fw"></i></a> 
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="tab-pane <?php echo ($active_tab == 'lobby') ? 'active' : '';?>" id="live_streambanner">
        <div class="banner_page2">
          <div class="box-header">
            <div class="pull-right">
              <button class="btn btn-primary btn-xl modal-live-stream_opt">Global Banner OFF</button>
              <button class="btn btn-primary btn-xl edit-data" action="edit_glbl_evefee">Global Event Fee</button>
              <a href="<?php echo base_url().'admin/banner/allevent_calendar'; ?>" class="btn btn-primary btn-xl" title="View Spectate Users"> <i class="fa fa-calendar"></i>&nbsp;&nbsp;Daywise events </a>
            </div>
          </div>
          <div class="box-body">
            <table class="table table-bordered table-striped table-responsive example_row_reorder">
              <thead>
                <tr>
                  <th>Lobby Strem Order</th>
                  <th>Lobby ID</th>
                  <th>Lobby Title</th>
                  <th>Event Date</th>
                  <th>Creator</th>
                  <th>Tag</th>
                  <th>Link</th>
                  <th>Is Banner</th>
                  <th>Is Event</th>
                  <th>Event Image</th>
                  <th>Action</th>
                  <th>Is Spectator</th>
                  <th>Spectator</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($live_lobby_data as $k => $lobby) {
                  $creator_arr = $this->General_model->view_single_row('user_detail','user_id',$lobby->user_id);
                  ($lobby->is_event == 1) ? $is_event_arr[] =   $lobby->user_id : '';
                  $arr = explode(',',$lobby->channel);
                  $count = 0;
                  foreach($arr as $a) {
                    $sub_arr = explode('*_*+',$a);
                    if ($sub_arr[1] == 1) {
                      $count = 1;
                    }
                  }
                  $is_banner_class = ($count == 0) ? 'no_banner' : $is_banner_class = 'yes_banner'; ?>          
                  <tr>

                    <td title="Drag & Drop for change Order"><?php echo $lobby->lobby_banner_order; ?><i class="fa fa-bars" style="padding-left: 10px;"></i></td>
                    <!-- <td><?php //echo ($lobby->lobby_banner_order == 0) ? '' : $lobby->lobby_banner_order; ?><a href="#" class="lobby_view" id="<?php // echo $lobby->lobby_id; ?>" style="margin-left: 5px;"><i class="fa fa-pencil fa-fw"></i></a></td> -->
                    <td><?php echo $lobby->lobby_id; ?></td>
                    <td><?php echo $lobby->game_name; ?></td>
                    <td><?php echo $this->User_model->custom_date(array('date' => $lobby->event_start_date,'format' => 'Y-m-d')); ?></td>
                    <td><?php echo ucwords($creator_arr['name']); ?></td>
                    <td><?php echo $lobby->device_id?></td>
                    <td><a target="_blank" class="btn btn-primary" href="<?php echo base_url('livelobby/watchLobby/'.$lobby->lobby_id); ?>">Live Lobby</td> 
                    <td><p class="<?php echo $is_banner_class; ?>"><span class="hide"><?php echo ($count == 0) ? 'ON' : 'OFF'; ?></span></p></td>
                    <td><span class="hide"><?php echo ($lobby->is_event == 1) ? 'ON' :'OFF'; ?></span><label class="myCheckbox"><input <?php echo ($lobby->is_event == 1) ? 'checked' :''; ?> id="is_event<?php echo $k;?>" class="single-checkbox is_event custom-cb" type="checkbox" name="progress" value="<?php echo $lobby->lobby_id;?>"><span></span></label></td>
                    <td>
                      <a style="cursor: pointer;" title="Update Event Image" onclick="return changeEventprice('<?php echo $lobby->lobby_id; ?>','event_price');"><?php ($lobby->event_image == '') ? $img = 'no-image.png' : $img = $lobby->event_image;?><img style="border: 1px solid gray;border-radius: 12px;" src="<?php echo base_url().'upload/banner/'.$img; ?>" width="125"></a>
                    </td>
                    <td style="min-width: 200px;">
                      <a class="btn btn-primary actn_btn" title="Edit Banner" href="<?php echo base_url().'admin/banner/lobbyBannerEdit?id='.$lobby->lobby_id.'&name='.$lobby->game_name;?>"><i class="fa fa-pencil fa-fw"></i></a>  <a class="btn btn-primary actn_btn" title="Update Event Ticket Amount" onclick="return changeEventprice('<?php echo $lobby->lobby_id; ?>','event_price');"><i class="fa fa-money fa-fw"></i></a>  <a class="btn btn-primary actn_btn" title="List Event Users" href="<?php echo base_url().'admin/banner/getEventsUsers?id='.$lobby->lobby_id;?>"><i class="fa fa-address-book fa-fw"></i></a> <a class="btn btn-primary delete_lobby delete-data actn_btn" id="<?php echo $lobby->lobby_id; ?>" custom-type="delete" action="delete_live_lobby_banner" title="Delete Lobby" winre_loc="<?php echo base_url().'admin/banner/delete_lobby/'.$lobby->lobby_id; ?>"><i class="fa fa-trash fa-fw"></i></a>
                    
                    </td>
                    <td>
                      <div class="pull-left col-sm-12 banner_page">
                        <label class="switch">
                          <input type="checkbox" name="is_" value="<?php echo $lobby->lobby_id; ?>" <?php echo ($lobby->is_spectator == 1) ? 'checked' : ''; ?> class="is_spectator"><span class="slider round"></span>
                        </label>
                      </div>
                    </td>
                    <td style="min-width: 120px;">
                      <a class="btn btn-primary actn_btn" title="Update Spectate Ticket Amount" onclick="return changeEventprice('<?php echo $lobby->lobby_id; ?>','spectate_price');"><i class="fa fa-money fa-fw"></i></a>  <a class="btn btn-primary actn_btn" title="List Spectate Users" href="<?php echo base_url().'admin/banner/getSpectateUsers?id='.$lobby->lobby_id;?>"><i class="fa fa-address-book fa-fw"></i></a>
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="tab-pane <?php echo ($active_tab == 'banner') ? 'active' : '';?>" id="banner_tab">
        <div class="banner_page1">
          <div class="box-header">
            <div>
              <a href="<?php echo base_url().'admin/banner/bannerAdd'; ?>"><button class="btn pull-right btn-primary btn-xl">Add Banner</button></a>
            </div>
          </div>
          <div class="box-body">
            <table class="table table-bordered table-striped table-responsive example_row_reorder">
              <thead>
                <tr>
                  <th>Banner Order</th>
                  <th>Change Banner Order</th>
                  <th>Banner Name</th>
                  <th>Banner Content</th>
                  <th>Banner Youtube Link</th>
                  <th>Banner Image</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($list as $value) {
                  $banner_order = $value['banner_order'];
                  $banner_img = $value['banner_image'] != '' ? $value['banner_image'] : 'placeholder.png';
                  ?>        
                  <tr>
                    <td title="Drag & Drop for change Order"><?php echo $value['banner_order']; ?><i class="fa fa-bars" style="padding-left: 10px;"></i></td>

                    <td><?php // echo $value['banner_order']; ?><a class="view" id="<?php echo $value['id']?>" style="margin-left: 5px;"><i class="fa fa-pencil fa-fw"></i></a></td>
                    <td><?php echo $value['banner_name']?></td>
                    <td><?php echo $value['banner_content']?></td>
                    <td><?php echo $value['banner_youtube_link']?></td>
                    <td><img src="<?php echo base_url().'upload/banner/'. $banner_img; ?>" width="100" height="100"></td>
                    <td style="min-width: 100px;"><a class="btn btn-primary actn_btn" title="Edit Banner" href="<?php echo base_url().'admin/banner/bannerEdit/'.$value['id'];?>"><i class="fa fa-pencil fa-fw"></i></a>  <a class="btn btn-primary actn_btn" title="Delete Lobby" href="<?php echo base_url().'admin/banner/delete/'.$value['id'];?>" onclick="return confirm('Are you sure delete ?')"><i class="fa fa-trash-o fa-fw"></i></a></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="tab-pane <?php echo ($active_tab == 'off_streamer_event') ? 'active' : '';?>" id="off_streamer_event">
        <div class="banner_page2">
          <div class="box-header">
            <div class="pull-right">
              <button class="btn btn-primary btn-xl modal-live-stream_opt">Global Banner OFF</button>
              <button class="btn btn-primary btn-xl edit-data" action="edit_glbl_evefee">Global Event Fee</button>
              <a href="<?php echo base_url().'admin/banner/allevent_calendar'; ?>" class="btn btn-primary btn-xl" title="View Spectate Users"> <i class="fa fa-calendar"></i>&nbsp;&nbsp;Daywise events </a>
            </div>
          </div>
          <div class="box-body">
            <table class="table table-bordered table-striped table-responsive custom_datatable">
              <thead>
                <tr>
                  <!-- <th>Lobby Strem Order</th> -->
                  <th>Lobby ID</th>
                  <th>Lobby Title</th>
                  <th>Event Date</th>
                  <th>Creator</th>
                  <th>Tag</th>
                  <th>Link</th>
                  <th>Is Banner</th>
                  <th>Is Event</th>
                  <th>Event Image</th>
                  <th>Action</th>
                  <th>Is Spectator</th>
                  <th>Spectator</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($streamer_off_lobby_data as $k => $lobby) {
                  $creator_arr = $this->General_model->view_single_row('user_detail','user_id',$lobby->user_id);
                  ($lobby->is_event == 1) ? $is_event_arr[] =   $lobby->user_id : '';
                  $arr = explode(',',$lobby->channel);
                  $count = 0;
                  foreach ($arr as $a) {
                    $sub_arr = explode('*_*+',$a);
                    if ($sub_arr[1] == 1) {
                      $count = 1;
                    }
                  }
                  $is_banner_class = ($count == 0) ? 'no_banner' : $is_banner_class = 'yes_banner'; ?>
                  <tr>

                    <!-- <td title="Drag & Drop for change Order"><i class="fa fa-bars" style="padding-right: 10px;"></i><?php //echo ($lobby->lobby_banner_order == 0) ? '' : $lobby->lobby_banner_order; ?></td> -->
                    <!-- <td><?php // echo ($lobby->lobby_banner_order == 0) ? '' : $lobby->lobby_banner_order; ?><a href="#" class="lobby_view" id="<?php // echo $lobby->lobby_id; ?>" style="margin-left: 5px;"><i class="fa fa-pencil fa-fw"></i></a> -->
                    <td><?php echo $lobby->lobby_id; ?></td>
                    <td><?php echo $lobby->game_name; ?></td>
                    <td><?php echo  $this->User_model->custom_date(array('date' => $lobby->event_start_date,'format' => 'Y-m-d')); ?></td>
                    <td><?php echo ucwords($creator_arr['name']); ?></td>
                    <td><?php echo $lobby->device_id?></td>
                    <td><a target="_blank" class="btn btn-primary" href="<?php echo base_url('livelobby/watchLobby/'.$lobby->lobby_id); ?>">Live Lobby</td> 
                    <td><p class="<?php echo $is_banner_class; ?>"><span class="hide"><?php echo ($count == 0) ? 'ON' : 'OFF'; ?></span></p></td>
                    <td><span class="hide"><?php echo ($lobby->is_event == 1) ? 'ON' :'OFF'; ?></span><label class="myCheckbox"><input <?php echo ($lobby->is_event == 1) ? 'checked' :''; ?> id="is_event<?php echo $k;?>" class="single-checkbox is_event custom-cb" type="checkbox" name="progress" value="<?php echo $lobby->lobby_id;?>"><span></span></label></td>
                    <td>
                      <a style="cursor: pointer;" title="Update Event Image" onclick="return changeEventprice('<?php echo $lobby->lobby_id; ?>','event_price');"><?php ($lobby->event_image == '') ? $img = 'no-image.png' : $img = $lobby->event_image;?><img style="border: 1px solid gray;border-radius: 12px;" src="<?php echo base_url().'upload/banner/'.$img; ?>" width="125"></a>
                    </td>
                    <td style="min-width: 200px;">
                      <a class="btn btn-primary actn_btn" title="Edit Banner" href="<?php echo base_url().'admin/banner/lobbyBannerEdit?id='.$lobby->lobby_id.'&name='.$lobby->game_name;?>"><i class="fa fa-pencil fa-fw"></i></a>  <a class="btn btn-primary actn_btn" title="Update Event Ticket Amount" onclick="return changeEventprice('<?php echo $lobby->lobby_id; ?>','event_price');"><i class="fa fa-money fa-fw"></i></a>  <a class="btn btn-primary actn_btn" title="List Event Users" href="<?php echo base_url().'admin/banner/getEventsUsers?id='.$lobby->lobby_id;?>"><i class="fa fa-address-book fa-fw"></i></a> <a class="btn btn-primary delete_lobby delete-data actn_btn" id="<?php echo $lobby->lobby_id; ?>" custom-type="delete" action="delete_live_lobby_banner" title="Delete Lobby" winre_loc="<?php echo base_url().'admin/banner/delete_lobby/'.$lobby->lobby_id; ?>"><i class="fa fa-trash fa-fw"></i></a>
                        <a class="btn btn-primary delete_lobby delete-data actn_btn" id="<?php echo $lobby->lobby_id; ?>"   title="Archive" winre_loc="<?php echo base_url().'admin/banner/archive_lobby/'.$lobby->lobby_id; ?>"><i class="fa fa-archive fa-fw"></i></a>
                    </td>
                    <td>
                      <div class="pull-left col-sm-12 banner_page">
                        <label class="switch">
                          <input type="checkbox" name="is_" value="<?php echo $lobby->lobby_id; ?>" <?php echo ($lobby->is_spectator == 1) ? 'checked' : ''; ?> class="is_spectator"><span class="slider round"></span>
                        </label>
                      </div>
                    </td>
                    <td style="min-width: 120px;">
                      <a class="btn btn-primary actn_btn" title="Update Spectate Ticket Amount" onclick="return changeEventprice('<?php echo $lobby->lobby_id; ?>','spectate_price');"><i class="fa fa-money fa-fw"></i></a>  <a class="btn btn-primary actn_btn" title="List Spectate Users" href="<?php echo base_url().'admin/banner/getSpectateUsers?id='.$lobby->lobby_id;?>"><i class="fa fa-address-book fa-fw"></i></a>
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
       <div class="tab-pane <?php echo ($active_tab == 'archive_event') ? 'active' : '';?>" id="archive_event">
        <div class="banner_page2">
          <div class="box-header">
            <div class="pull-right">
              <button class="btn btn-primary btn-xl modal-live-stream_opt">Global Banner OFF</button>
              <button class="btn btn-primary btn-xl edit-data" action="edit_glbl_evefee">Global Event Fee</button>
              <a href="<?php echo base_url().'admin/banner/allevent_calendar'; ?>" class="btn btn-primary btn-xl" title="View Spectate Users"> <i class="fa fa-calendar"></i>&nbsp;&nbsp;Daywise events </a>
            </div>
          </div>
          <div class="box-body">
            <table class="table table-bordered table-striped table-responsive custom_datatable" table_data="event_list" order_column="4">
              <thead>
                <tr>
                  <!-- <th>Lobby Strem Order</th> -->
                  <th>Lobby ID</th>
                  <th>Lobby Title</th>
                  <th>Event Date</th>
                  <th>Creator</th>
                  <th>Tag</th>
                  <th>Link</th>
                  <th>Is Banner</th>
                  <th>Is Event</th>
                  <th>Event Image</th>
                  <th>Action</th>
                  <th>Is Spectator</th>
                  <th>Spectator</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($get_archive_data as $k => $lobby) {
                  $creator_arr = $this->General_model->view_single_row('user_detail','user_id',$lobby->user_id);
                  ($lobby->is_event == 1) ? $is_event_arr[] =   $lobby->user_id : '';
                  $arr = explode(',',$lobby->channel);
                  $count = 0;
                  foreach ($arr as $a) {
                    $sub_arr = explode('*_*+',$a);
                    if ($sub_arr[1] == 1) {
                      $count = 1;
                    }
                  }
                  $is_banner_class = ($count == 0) ? 'no_banner' : $is_banner_class = 'yes_banner'; ?>
                  <tr>
                    <!-- <td title="Drag & Drop for change Order"><i class="fa fa-bars" style="padding-right: 10px;"></i><?php //echo ($lobby->lobby_banner_order == 0) ? '' : $lobby->lobby_banner_order; ?> --><!-- <a href="#" class="lobby_view" id="<?php //echo $lobby->lobby_id; ?>" style="margin-left: 5px;"><i class="fa fa-pencil fa-fw"></i></a> --><!-- </td> -->
                    <td><?php echo $lobby->lobby_id; ?></td>
                    <td><?php echo $lobby->game_name; ?></td>
                    <td><?php echo $this->User_model->custom_date(array('date' => $lobby->event_start_date,'format' => 'Y-m-d')); ?></td>
                    <td><?php echo ucwords($creator_arr['name']); ?></td>
                    <td><?php echo $lobby->device_id?></td>
                    <td><a target="_blank" class="btn btn-primary" href="<?php echo base_url('livelobby/watchLobby/'.$lobby->lobby_id); ?>">Live Lobby</td> 
                    <td><p class="<?php echo $is_banner_class; ?>"><span class="hide"><?php echo ($count == 0) ? 'ON' : 'OFF'; ?></span></p></td>
                    <td><span class="hide"><?php echo ($lobby->is_event == 1) ? 'ON' :'OFF'; ?></span><label class="myCheckbox"><input <?php echo ($lobby->is_event == 1) ? 'checked' :''; ?> id="is_event<?php echo $k;?>" class="single-checkbox is_event custom-cb" type="checkbox" name="progress" value="<?php echo $lobby->lobby_id;?>"><span></span></label></td>
                    <td>
                      <a style="cursor: pointer;" title="Update Event Image" onclick="return changeEventprice('<?php echo $lobby->lobby_id; ?>','event_price');"><?php ($lobby->event_image == '') ? $img = 'no-image.png' : $img = $lobby->event_image;?><img style="border: 1px solid gray;border-radius: 12px;" src="<?php echo base_url().'upload/banner/'.$img; ?>" width="125"></a>
                    </td>
                    <td style="min-width: 200px;">
                      <a class="btn btn-primary actn_btn" title="Edit Banner" href="<?php echo base_url().'admin/banner/lobbyBannerEdit?id='.$lobby->lobby_id.'&name='.$lobby->game_name;?>"><i class="fa fa-pencil fa-fw"></i></a>  <a class="btn btn-primary actn_btn" title="Update Event Ticket Amount" onclick="return changeEventprice('<?php echo $lobby->lobby_id; ?>','event_price');"><i class="fa fa-money fa-fw"></i></a>  <a class="btn btn-primary actn_btn" title="List Event Users" href="<?php echo base_url().'admin/banner/getEventsUsers?id='.$lobby->lobby_id;?>"><i class="fa fa-address-book fa-fw"></i></a> <a class="btn btn-primary delete_lobby delete-data actn_btn" id="<?php echo $lobby->lobby_id; ?>" custom-type="delete" action="delete_live_lobby_banner" title="Delete Lobby" winre_loc="<?php echo base_url().'admin/banner/delete_lobby/'.$lobby->lobby_id; ?>"><i class="fa fa-trash fa-fw"></i></a>
                      <a class="btn btn-primary delete_lobby delete-data actn_btn" id="<?php echo $lobby->lobby_id; ?>"   title="un-Archive" winre_loc="<?php echo base_url().'admin/banner/un_archive_lobby/'.$lobby->lobby_id; ?>"><i class="fa fa-unlock fa-fw"></i></a>
                    </td>
                    <td>
                      <div class="pull-left col-sm-12 banner_page">
                        <label class="switch">
                          <input type="checkbox" name="is_" value="<?php echo $lobby->lobby_id; ?>" <?php echo ($lobby->is_spectator == 1) ? 'checked' : ''; ?> class="is_spectator"><span class="slider round"></span>
                        </label>
                      </div>
                    </td>
                    <td style="min-width: 120px;">
                      <a class="btn btn-primary actn_btn" title="Update Spectate Ticket Amount" onclick="return changeEventprice('<?php echo $lobby->lobby_id; ?>','spectate_price');"><i class="fa fa-money fa-fw"></i></a>  <a class="btn btn-primary actn_btn" title="List Spectate Users" href="<?php echo base_url().'admin/banner/getSpectateUsers?id='.$lobby->lobby_id;?>"><i class="fa fa-address-book fa-fw"></i></a> 
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<style>
  .no_banner{ width:15px; height:15px; background:red; border-radius:4px; }
  .yes_banner{ width:15px; height:15px; background:green; border-radius:4px; }
  .myCheckbox input { visibility:hidden; position: absolute; z-index: -9999; }
  .myCheckbox span { width: 15px; height: 15px; display: block; background: red; border-radius:4px; }
  .myCheckbox input:checked + span { background: green; }
  .banner_tab_main_section .actn_btn { font-size: 18px; padding: 2px 4px; }
</style>
<div class="modal fade  custom-width" id="modal-1">
  <div class="modal-dialog" style="width: 60%;">
    <div class="modal-content">      
    </div>
  </div>
</div>
<div class="modal fade custom-width" id="modal-live-stream_opt">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">All Live Stream Banners</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <form action="<?php echo base_url(); ?>admin/banner/is_banner_flag_all/" id="changebalance_id" name="changebalance_id" method="POST">
            <div class="box-body">
              <div class="form-group">
                <label for="name">
                  <button type="submit" name="submit" value="off" class="btn btn-danger">ALL OFF</button>
                </label>
              </div>
            </div>
            <input type="hidden" name="is_event_arr" value='<?php echo (isset($is_event_arr) && !empty($is_event_arr)) ? json_encode($is_event_arr) : '' ?>'>
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url().'assets/plugins/datatables/dataTables.rowReorder.min.js'; ?>"></script>
<script src="<?php echo base_url().'assets/backend/js/row_reorder_custom.js'; ?>"></script>
<script type="text/javascript">
  $('.custom_datatable').DataTable({
     aLengthMenu: [[25, 50, 75, -1], [25, 50, 75, "All"]],
    iDisplayLength: 25,
    stateSave: true,
    "order": [[2, "asc"]]
  });
  var table = $('.example_row_reorder').DataTable({
    rowReorder: true,
    // scrollY: 700,
    aLengthMenu: [[25, 50, 75, -1], [25, 50, 75, "All"]],
    iDisplayLength: 25,
    stateSave: true,
    order: [[1, "desc"]]
  });
  table.on('row-reorder', function(e, diff, edit) {
    var o = [], n = [];
    for (var i = 0, ien = diff.length ; i < ien ; i++) {
      o.push(diff[i].oldData), n.push(diff[i].newData); 
    }
    var type = $(this).closest('.tab-pane').attr('id');
  
    try {
      $.ajax({
        type: "POST",
        url: base_url + "admin/banner/UpdateBannerOrder",
        data: {'old':o,'new':n, 'type' : type},
        beforeSend: function(){ admin_loader_show(); },
        success: function (data){ admin_loader_hide(); }
      });
    } catch(err) { alert(err); }
  });


  function changeEventFee() {
    $.ajax({
      url : '<?php echo site_url(); ?>admin/Banner/getEventFee/',
      success: function(result) {
        (result) ? $('#modal-basic .modal-content').html(result).parent('#modal-basic').modal('show') : '';
        $(".err_msg").html('');
      }
    });
  }
  function change_all_isbanner() {
    $.ajax({
      url : '<?php echo site_url(); ?>admin/Banner/change_all_isbanner/',
      success: function(result) {
        (result) ? $('#modal-basic .modal-content').html(result).parent('#modal-basic').modal('show') : '';
        $(".err_msg").html('');
      }
    });
  }

  function changeEventprice(id,type) {
    $.ajax({
      url : '<?php echo site_url(); ?>admin/Banner/getEventPrice?id='+id+'&type='+type,
      success: function(result) {
        if (result) {
          $('.modal-content').html(result);
          $('#modal-1').modal('show');
          $(".err_msg").html('');
          $("#event_price").blur(function() {
            var type = $("#price_type").val();

            var calctype = parseInt($(".calctype").val());
            var calcval = parseInt($(".calcval").val()); 
            var current_price = parseInt($("#event_price").val());
            var event_price = parseInt($("#old_event_price").val());
            var spectator_price = parseInt($("#old_spectator_price").val());

            if (type == 'event_price') {
              if((free_event == 'free' && current_price >= spectator_price) || current_price > spectator_price){
                $(".err_msg").html(''); 
                var evefee = calcval;
                if (calctype == 1) {
                  evefee = calcval;
                } else if (calctype == 2) {
                  evefee = current_price * calcval /100;
                }
                $(".event_fee").val(evefee);
                $('input[name="acc_price_credited"]').val(parseFloat($("#event_price").val() - $(".event_fee").val()).toFixed(2));
              } else {
                $(".err_msg").html("Specatator Ticket price is "+spectator_price+" you have to enter price is more than spectator price");
                $("#event_price").val('');
              }
            } else {
              if((current_price == 0 && current_price == event_price) || current_price < event_price){
                $(".err_msg").html('');
              } else {
                $(".err_msg").html("Event Ticket price is "+event_price+" you have to enter price is less than event price");
                $("#event_price").val('');
              }
            }
          });
          $(".event_fee").blur(function(){ 
            var event_price = $("#event_price").val();
            var event_fee = $(".event_fee").val(); 
            $('input[name="acc_price_credited"]').val(parseFloat($("#event_price").val() - $(".event_fee").val()).toFixed(2));
            if ((free_event == 'free' && parseInt(event_price) >= parseInt(event_fee)) || parseInt(event_price) > parseInt(event_fee)){
              $(".fee_err_msg").html('');
            } else {
              $(".fee_err_msg").html("Please Enter fee less than price.");
              $(".event_fee").val('');
            }
          });
          if ($('.eve_datetimepicker').length) {
            $('.eve_datetimepicker').datetimepicker({format: 'YYYY-MM-DD HH:mm:ss'});
          }
          commonchecked();
        }
      }
    });
  }
  function commonchecked() {
    $('.onlydate').click(function () {
      if ($(this).is(':checked') == true) {
        ($('input[name="event_start_date"]').val() != '') ? $('input[name="event_start_date"]').val(moment($('input[name="event_start_date"]').val()).format("YYYY-MM-DD")) : '';
        ($('input[name="event_end_date"]').val() != '') ? $('input[name="event_end_date"]').val(moment($('input[name="event_end_date"]').val()).format("YYYY-MM-DD")) : '';
      } else {
        ($('input[name="event_start_date"]').val() != '') ? $('input[name="event_start_date"]').val(moment( $('input[name="event_start_date"]').val()).format("YYYY-MM-DD HH:mm:ss")) : '';
        ($('input[name="event_end_date"]').val() != '') ? $('input[name="event_end_date"]').val(moment($('input[name="event_end_date"]').val()).format("YYYY-MM-DD HH:mm:ss")) : '';
      }
    });

    $('.eve_img_dlt').click(function() {
      var eveid = $(this).attr('data-id');
      if (eveid != undefined && eveid != '') {
        $.ajax({
          url : '<?php echo site_url(); ?>admin/Banner/remove_event_img/'+eveid,
          beforeSend: function() { admin_loader_show(); },
          success: function(result) {
            admin_loader_hide();
            $('.img_pre_div').html('<label class="alert alert-danger">Removed</label>');
            return false;
          }
        });    
      }
    });

  }
  $(".lobby_status").change(function() {
    var type = (this.checked) ? 'on' : 'off';
    $.ajax({
      url : '<?php echo site_url(); ?>admin/Banner/updateSiteSettings?type='+type,
      success: function(result) { }
    });
  });
  $(".modal-live-stream_opt").click(function () { $('#modal-live-stream_opt').modal('show'); });
  $(document).ready(function() {
    // $('.example_row_reorder').dataTable({
    //     aLengthMenu: [[25, 50, 75, -1], [25, 50, 75, "All"]],
    //     iDisplayLength: 25,
    //     stateSave: true,
    // });
    $('.is_spectator').click(function() {
      var checked_val = $(this).prop("value");
      var type = ($(this).prop("checked") == true) ? 1 : 0;
      $.ajax({
        url : '<?php echo site_url(); ?>admin/Banner/updateIsSpectator?id='+checked_val+'&type='+type+'&update_type=spectator',
        success: function(result) {
          window.location.href = "<?php echo site_url(); ?>admin/Banner/";
        }
      });
    });
    $('.is_event').click(function() { 
      var checked_val = $(this).prop("value");
      var type = ($(this).prop("checked") == true) ? 1 : 0;
      $.ajax({
        url : '<?php echo site_url(); ?>admin/Banner/updateIsSpectator?id='+checked_val+'&type='+type+'update_type=event',
        success: function(result) {
          window.location.href = "<?php echo site_url(); ?>admin/Banner/";
        }
      });
    });
  });
  $(document).on('click','.view',function(){
    var id = $(this).attr('id');
    if(id){
      $.ajax({
        url : '<?php echo site_url(); ?>admin/Banner/ChangeBannerOrder/?id='+id,
        success: function(result) {
          if(result){
            $('.modal-content').html(result);
            $('#modal-1').modal('show');
          }
        }
      });
    }
  });
  $(document).on('click','.lobby_view',function(){
    var id = $(this).attr('id');
    if(id){
      $.ajax({
        url : '<?php echo site_url(); ?>admin/Banner/ChangeLobbyOrder/?id='+id,
        success: function(result) {
          if(result){
            $('.modal-content').html(result);
            $('#modal-1').modal('show');
          }
        }
      });
    }
  });
  $(document).on('submit','#change_banner_order',function(e){
    e.preventDefault();
    var data = $(this).serialize();
    console.log(data);
    if(data){
      $.ajax({
        url: "<?php echo site_url(); ?>admin/Banner/UpdateBannerOrder",
        data: data,
        type: 'POST',
        success: function(result) {
          window.location.href = "<?php echo site_url(); ?>admin/Banner?type=banner";       
        }
      });
    }             
  });
  $(document).on('submit','#change_lobby_order',function(e){
    e.preventDefault();
    var data = $(this).serialize();
    if(data){
      $.ajax({
        url: "<?php echo site_url(); ?>admin/Banner/UpdateLobbyOrder",
        data: data,
        type: 'POST',
        success: function(result) {
          window.location.href = "<?php echo site_url(); ?>admin/Banner/";       
        }
      });
    }             
  });
</script>