<section class="content">
  <div class="row">
    <div class="col-md-12">
		<?php $this->load->view('admin/common/show_message') ?>
        <div class="box box-primary"> 
            <a onclick="history.back();" class="pull-right mt20 btn btn-primary"> <span class="glyphicon glyphicon-chevron-left"></span>Back</a>
            <div class="box-body">
                <h1 class="text-center"><?php echo $live_lobby_name.' Live Stream List';?></h1>
                <br>
                <?php
                $left_table_arr = $right_table_arr = [];

                $left_table_arr = array_map(function($value) {
                    if (strtolower($value->table_cat) == 'left' || strtolower($value->waiting_table) == 'left') {
                        return $value;
                    }
                }, $live_lobby_stream_data);

                $right_table_arr = array_map(function($value) {
                    if (strtolower($value->table_cat) == 'right' || strtolower($value->waiting_table) == 'right') {
                        return $value;
                    }
                }, $live_lobby_stream_data);
                $left_table_arr = array_values(array_filter($left_table_arr));
                $right_table_arr = array_values(array_filter($right_table_arr));
                // echo "<pre>";
                // echo "<br> left_table_arr:";
                // print_r($live_lobby_stream_data);
                // echo "<br> right_table_arr:";
                // print_r($right_table_arr);
                // exit();
                ?>
                <form id="form1" class="row" method=post action=check.php>
                    <?php if (!empty($left_table_arr)) { ?>
                        <div class="col-sm-6">
                            <table class="table table-hover table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th class="border-right">Left Player Name</th>
                                        <th>Left Stream Type</th>
                                        <th>Left Stream channel</th>
                                        <th>Is Banner</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($left_table_arr as $key => $lta) { ?>
                                    <tr>
                                        <td><?php echo ($lta->display_name == '') ? $lta->name : $lta->display_name;?></td> 
                                        <td class="border-right"><?php echo ($lta->stream_type == '1') ? 'Twitch' : 'OBS' ;?></td>
                                        <td class="border-right"><?php echo ($lta->stream_type == '1') ? $lta->stream_channel : $lta->obs_stream_channel ;?></td>
                                        <td><label class="myCheckbox"><input <?php echo ($lta->is_banner == 1) ? 'checked' :'';?> id="is_banner<?php echo $k;?>" class="single-checkbox custom-cb" type="checkbox" name="progress" value="<?php echo $lta->id;?>"><span></span></label></td> 
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    <?php } ?>
                    <?php if (!empty($right_table_arr)) { ?>
                        <div class="col-sm-6">
                            <table class="table table-hover table-bordered table-striped col-sm-6">
                                <thead>
                                    <tr>
                                        <th class="border-right">Right Player Name</th>
                                        <th>Right Stream Type</th>
                                        <th>Right Stream channel</th>
                                        <th>Is Banner</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($right_table_arr as $key => $rta) { ?>
                                    <tr>
                                        <td><?php echo ($rta->display_name == '') ? $rta->name :$rta->display_name;?></td> 
                                        <td class="border-right"><?php echo ($rta->stream_type == '1') ? 'Twitch' : 'OBS' ;?></td>
                                        <td class="border-right"><?php echo ($rta->stream_type == '1') ? $rta->stream_channel : $rta->obs_stream_channel ;?></td>
                                        <td valign="top"><label class="myCheckbox"><input <?php echo ($rta->is_banner == 1) ? 'checked' :'';?> id="is_banner<?php echo $k;?>" class="single-checkbox custom-cb" type="checkbox" name="progress" value="<?php echo $rta->id;?>"><span></span></label></td> 
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    <?php } ?>

                       <!--  <tr>
                            <th>Left Stream channel</th>
                            <th class="border-right">Left Player Name</th>
                            <th>Is Banner</th>
                            <th>Right Stream channel</th>
                            <th class="border-right">Right Player Name</th>
                            <th>Is Banner</th>
                            <th>Link</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($live_lobby_stream_data)) {
                                foreach($live_lobby_stream_data as $k => $stream) { ?>
                                    <tr>
                                        <?php if ($stream->table_cat == 'LEFT') { ?>
                                            <td><?php echo ($stream->display_name == '') ? $stream->name :$stream->display_name;?></td> 
                                            <td class="border-right"><?php echo $stream->stream_channel;?></td>
                                            <td><label class="myCheckbox"><input <?php echo ($stream->is_banner == 1) ? 'checked' :'';?> id="is_banner<?php echo $k;?>" class="single-checkbox custom-cb" type="checkbox" name="progress" value="<?php echo $stream->id;?>"><span></span></label></td> 
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        <?php } else { ?>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><?php echo ($stream->display_name == '') ? $stream->name :$stream->display_name;?></td> 
                                            <td class="border-right"><?php echo $stream->stream_channel;?></td>
                                            <td valign="top"><label class="myCheckbox"><input <?php echo ($stream->is_banner == 1) ? 'checked' :'';?> id="is_banner<?php echo $k;?>" class="single-checkbox custom-cb" type="checkbox" name="progress" value="<?php echo $stream->id;?>"><span></span></label></td> 
                                        <?php } ?>
                                        <td>
                                            <a target="_blank" class="btn btn-primary" href="<?php echo base_url('livelobby/watchLobby/'.$stream->lobby_id); ?>">Live Lobby</a>
                                        </td>
                                    </tr>
                                <?php }
                            } ?> 
                        </tbody> -->
                    <!-- </table> -->
                </form>
            </div>
        </div>
    </div>
   </div>
</section>
<style>
    .mt20{
        margin-top: 20px;
    }
</style>
<script>
$(document).ready(function(){
    var $checkboxes = $('#form1 td input[type="checkbox"]');
    $('input[type="checkbox"]').click(function() {
        var countCheckedCheckboxes = $checkboxes.filter(':checked').length;
        var checked_val = $(this).prop("value");
        if ($(this).prop("checked") == true) {
            if(countCheckedCheckboxes > 2) {
                alert("Please Select only Two");
                $(this).prop("checked") == false;
                return false;
            } else {
                var type = 'check';
            }
        }
        else if ($(this).prop("checked") == false) {
            var type = 'uncheck';
        }
        $.ajax({
            url : '<?php echo site_url(); ?>admin/Banner/updateIsBanner?type='+type+'&id='+checked_val,
            success: function(result) {
                // window.location = window.location;
            }
        });
    });
});
</script>