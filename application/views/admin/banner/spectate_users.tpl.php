
<section class="content">
  <div class="row">
    <div class="col-md-12">
		<?php $this->load->view('admin/common/show_message') ?>
        <div class="box box-primary">
            <div class="box-header">
                <!-- <button onclick="myFunction('<?php echo $lobby_id; ?>')" class="mt20 btn btn-primary"> <span class="glyphicon glyphicon-refresh"></span> Reset</button>  -->
                <a href="<?php echo base_url().'admin/banner/eventUserDelete?type=2&lobby_id='.$lobby_id ?>" class="mt20 btn btn-primary"> <span class="glyphicon glyphicon-refresh"></span> Reset</a>
                <a href="javascript:void(0);" class="mt20 btn btn-primary reset_spec_btn"> <span class="glyphicon glyphicon-refresh"></span> Reset Selected Users</a>
                <a href="javascript:void(0);" class="mt20 btn btn-primary refund_spec_btn"> <span class="glyphicon glyphicon-usd"></span> Refund Selected Users</a>
                <a onclick="history.back();" class="pull-right mt20 btn btn-primary"> <span class="glyphicon glyphicon-chevron-left"></span>Back</a>
            </div>
            <div class="box-body">
                <span style="color: red" id="reset_spec_error"></span>
                <p>Spectate Users list</p>                
                <table id="dttble_example_entries_increase" class="table table-hover table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Reset/Refund <br> Select All &nbsp;&nbsp; <input type="checkbox" name="select_all" class="select_all" ></th>
                            <th>Reg #</th>
                            <th>Date</th>
                            <th>Acct.#</th>
                            <th>User Name</th>
                            <th>Display Name</th>
                            <th>Team Name</th>
                            <th>Email</th>
                            <th>Total Cost</th>
                            <!-- <th>Pro Fees Collected</th> -->
                            <th>Fee</th>
                            <th>Prize</th>
                            <th>Discount</th>
                            <!-- <th>Amount Of Ticket($)</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $i = 1;
                        foreach($events_users as  $user) {
                            $tkt_total_price = $user->spectate_price;
                            $tkt_spectator_fee = $user->spectator_fee;
                            $tkt_deducted_price = $user->spectate_price - $user->spectator_fee;
                            $tkt_applied_discount_on_tickets = $user->applied_discount_on_tickets;
                            if ($user->is_new_lobby == '1') {
                                $tkt_spectator_fee = $user->deducted_event_fee;
                                $tkt_deducted_price = $user->deducted_event_price;
                            }

                            // $fee = $user->event_price * $user->event_fee/100;
                            $event_fee_creator = $this->General_model->view_single_row('user_detail','user_id',$user->event_fee_creator);
                            // echo "<pre>"; print_r($user);
                            if ($user->is_gift == 1) {
                                $is_gift = 'Yes';
                                $gift_user = $this->General_model->view_single_row('user_detail','user_id',$user->gift_to); 
                                $gift_user_name = ucwords($gift_user['name']);
                                $gift_user_acc = $gift_user['account_no'];
                            } else {
                                $is_gift = 'No';
                                $gift_to = '-';
                                $gift_user_name = '';
                                $gift_user_acc = '';
                            } ?>                            
                            <tr>
                                <td><?php echo '<input type="checkbox" name="reset_spec[]" value="'.$user->user_id.'">'; ?></td>
                                <td><?php echo $i++; ?></td> 
                                <td><?php echo date('Y-m-d H:i', strtotime($user->created));?></td>
                                <td><?php echo $user->account_no; ?></td> 
                                <td><?php echo $user->name;?></td>
                                <td><?php echo $user->lobby_fan_tag;?></td> 
                                <td><?php echo $user->lobby_team_name;?></td>
                                <td><?php echo $user->email;?></td>   
                                <!-- <td><?php //echo $user->spectate_price; ?></td> -->
                                <td><?php echo number_format($tkt_total_price, 2, ".", ""); ?></td>
                                <td><?php echo number_format($tkt_spectator_fee, 2, ".", ""); ?></td>
                                <td><?php echo number_format($tkt_deducted_price, 2, ".", ""); ?></td>
                                <td><?php echo number_format($tkt_applied_discount_on_tickets, 2, ".", ""); ?></td>                            
                            </tr>
                        <?php  }?> 
                    </tbody>
                </table>
                <form role="form" action="<?php echo base_url().'admin/banner/reset_selected_spec?lobby_id='.$lobby_id; ?>" class="reset_spec_form" method="POST">
                    <input type="hidden" name="selected_spec" id="selected_spec">
                </form>

                <form role="form" action="<?php echo base_url().'admin/banner/refund_selected_spec?lobby_id='.$lobby_id; ?>" class="refund_spec_form" method="POST">
                    <input type="hidden" name="selected_spec_rf" id="selected_spec_rf">
                </form>
            </div>
        </div>
    </div>
   </div>
</section>
<script type="text/javascript">
    // $('#dttble_example_entries_increase').DataTable( {
    //         "lengthMenu": [[-1, 25, 50, 100, 200], ["All", 25, 50, 100, 200]]
    //     });
    $(document).ready(function(){
        $('#dttble_example_entries_increase').DataTable( {
            "lengthMenu": [[-1, 25, 50, 100, 200], ["All", 25, 50, 100, 200]]
        });

        $('.reset_spec_btn').on('click',function(){
            var selected_spec = [];
            $('input[name="reset_spec[]"]:checked').each(function(){
                selected_spec.push($(this).val());
            });
            $('#selected_spec').val(selected_spec);
            if($('#selected_spec').val() == null || $('#selected_spec').val() == ''){
                $('#reset_spec_error').html('Please select users from list to reset.');
            }else{
                $('.reset_spec_form').submit();
            }            
        })

        $('.refund_spec_btn').on('click',function(){
            var selected_spec_rf = [];
            $('input[name="reset_spec[]"]:checked').each(function(){
                selected_spec_rf.push($(this).val());
            });
            $('#selected_spec_rf').val(selected_spec_rf);
            if($('#selected_spec_rf').val() == null || $('#selected_spec_rf').val() == ''){
                $('#reset_spec_error').html('Please select users from list to refund.');
            }else{
                $('.refund_spec_form').submit();
            }            
        })       

        $('input[name="reset_spec[]"]').on('change',function(){
            $('#reset_spec_error').html('');
        })

        $('.select_all').on('click', function(){
            if($(this).is(":checked")){
                $('input[name="reset_spec[]"]').prop('checked', true);
            }else{
                $('input[name="reset_spec[]"]').prop('checked', false);
            }
        })
    })
</script>
<style>
    .mt20{
        margin-top: 20px;
    }
</style>


