<section class="content">

  <div class="row">

    <div class="col-md-12">

			<?php $this->load->view('admin/common/show_message') ?>

        <div class="box box-primary">

          <form  action="<?php echo base_url(); ?>admin/banner/tab_bannerInsert" method="post" enctype="multipart/form-data">

            <div class="box-body">

              <div class="form-group">

                  <label for="name">Banner Name</label>

                  <input type="text" placeholder="Banner Name" id="name" class="form-control" name="bname">

              </div>

              <div class="form-group">

                  <label for="description">Banner Content</label>

                  <textarea class="form-control" id="description" placeholder="Banner Content" name="content"></textarea>

              </div>

              <div class="form-group">

                  <label for="site_url">Banner Link</label>

                  <input type="url" placeholder="Banner Link" id="site_url" class="form-control" name="site_url">

              </div>

                <div class="form-group">
                <label for="file">Banner Order</label>
                <input type="number" class="form-control" id="banner_order" placeholder="Banner Order" name="banner_order">
                </div>

                <div class="form-group col-sm-6" style="padding-left: 0;">
                <div class="col-md-1"><input type="radio" name="selected_banner" id="selected_banner_image" value="upload_image_banner" checked></div>
                <div class="col-md-11"><label>Banner Image</label>
                <input type="file" class="form-control image_banner" id="file" placeholder="Image" name="image_banner" accept="image/*"></div></div>
                <div class="form-group col-sm-6" style="padding-right: 0;">
                <div class="col-md-1"><input type="radio" name="selected_banner" id="selected_banner_video" value="upload_video_banner"></div>
                <div class="col-md-11"><label>Banner Video</label>
                <input type="file" class="form-control video_banner" id="file" placeholder="Video" name="video_banner" accept="video/*" style="display: none;">
                <input type="text" class="form-control video_duration" id="video_duration" placeholder="Video Duration" name="video_duration" style="display: none; margin-top: 5px;"></div></div>


              </div><!-- /.box-body -->

              <div class="box-footer">

                <button class="btn btn-primary" type="submit">Submit</button>

              </div>

          </form>

        </div>

      </div>

    </div>

  </div>   

</section>