<section class="content">

  <div class="row">

    <div class="col-md-12">

			<?php $this->load->view('admin/common/show_message') ?>

        <div class="box box-primary">

          <form  action="<?php echo base_url(); ?>admin/banner/tab_bannerUpdate" method="post" enctype="multipart/form-data">

            <div class="box-body">

              <div class="form-group">

                  <label for="name">Banner Name</label>

                  <input type="text" id="name" class="form-control" name="bname" value="<?php if(isset($banner_data[0]['banner_name']))echo $banner_data[0]['banner_name'];?>">

              </div>

              <div class="form-group">

                  <label for="description">Banner Content</label>

                  <textarea class="form-control" name="content"><?php if(isset($banner_data[0]['banner_content']))echo $banner_data[0]['banner_content'];?></textarea>

              </div>

              <div class="form-group">

                  <label for="site_url">Banner Link</label>

                  <input type="url" id="site_url" class="form-control" name="site_url" value="<?php if(isset($banner_data[0]['site_url']))echo $banner_data[0]['site_url'];?>">

              </div>
                <div class="form-group upload-div col-sm-6 <?php if($banner_data[0]['banner_image'] != '') echo 'active' ?>" style="padding-left: 0;">
                <div class="col-md-1"><input type="radio" name="selected_banner" id="edit_banner_image" value="edit_banner_image" <?php if($banner_data[0]['banner_image'] != '') echo 'checked class="checked"';?>></div>
                <div class="col-md-11"><label>Banner Image</label><a style="float: right; cursor: pointer; display: none;" class="browse_new_image">Change Image</a>
                <input type="file" class="form-control image_banner" id="file" placeholder="Image" name="image_banner" accept="image/*" style="display: none;">
                <input type="hidden" id="old_image_banner" name="old_image_banner" value="<?php echo $banner_data[0]['banner_image'];?>"></div>
                <div class="col-md-1"></div><div class="col-md-11" style="margin-top: 5px; "><img src="<?php echo base_url().'upload/banner/'.$banner_data[0]['banner_image'];?>"  class="old_image_preview" height="150" width="150" style="display: none;">
                </div>
              </div>
              <?php if ($banner_data[0]['banner_video_duration']=='0')
              {
                $banner_video_duration = '';
              }
              else
              {
                $banner_video_duration = $banner_data[0]['banner_video_duration'];
              }
              ?>
                <div class="form-group upload-div col-sm-6 <?php if($banner_data[0]['banner_video'] != '') echo 'active' ?>" style="padding-right: 0;">
                <div class="col-md-1"><input type="radio" name="selected_banner" id="edit_banner_video" value="edit_banner_video" <?php if($banner_data[0]['banner_video'] != '') echo 'checked class="checked"';?>></div>
                <div class="col-md-11"><label>Banner Video</label> <a style="float: right; cursor: pointer; display: none;" class="browse_new_video">Change Video</a>
                <input type="file" class="form-control video_banner" id="file" placeholder="Video" name="video_banner" accept="video/*" style="display: none;">
                <input type="text" class="form-control video_duration" id="old_video_duration" name="old_video_duration" placeholder="Video Duration" style="display: none; margin-top: 5px;" value="<?php echo $banner_video_duration;?>">
                <input type="hidden" class="form-control new_video_banner" name="old_video_banner" value="<?php echo $banner_data[0]['banner_video'];?>">
              </div>
                 <div class="col-md-1"></div><div class="col-md-11" style="margin-top: 5px; "><span class="old_video_preview" style="display: none;"><b>Video :</b> <?php echo $banner_data[0]['banner_video']; ?></span></video>
                </div>
              </div>


              </div><!-- /.box-body -->

              <div class="box-footer">

                <input type="hidden" class="form-control" name="banner_order" value="<?php echo $banner_data[0]['banner_order'];?>">

                <input type="hidden" name="id" value="<?php if(isset($banner_data[0]['id'])) echo $banner_data[0]['id'];?>">

                <button class="btn btn-primary" type="submit">Update</button>

              </div>

          </form>

        </div>

      </div>

    </div>

</section>