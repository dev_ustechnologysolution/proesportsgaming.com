<section class="content">

  <div class="box">

    <div class="box-header">

      <a href="<?php echo base_url().'admin/banner/'; ?>"><button class="btn pull-left btn-primary btn-xl">Main Sliders</button></a><a href="<?php echo base_url().'admin/banner/tab_banner'; ?>"><button class="btn pull-left btn-primary btn-xl">Tab Sliders</button></a>
      <a href="<?php echo base_url().'admin/banner/chat_banner'; ?>"><button class="btn pull-left btn-primary btn-xl">Chat Sliders</button></a>
            
      <a href="<?php echo base_url().'admin/banner/tab_bannerAdd'; ?>"><button class="btn pull-right btn-primary btn-xl">Add Banner</button></a>

    </div>

    <?php $this->load->view('admin/common/show_message') ?>

    <div class="box-body">

      <table id="example1" class="table table-bordered table-striped">

        <thead>

          <tr>

            <th>Sl No</th>

            <th>Banner Order</th>

            <th>Banner Name</th>

            <th>Banner Content</th>

            <th>Banner Image</th>

            <th>Action</th>

          </tr>

        </thead>

        <tbody>

          <?php $i=1; foreach($list as $value){?>
          <?php if ($value['banner_order']=='0')
              {
                $banner_order = '';
              }
              else
              {
                $banner_order = $value['banner_order'];
              }

              if ($value['banner_image'] == '') {
                $value['banner_image'] = 'placeholder.png';
              }
          ?>
          
          <tr>

            <td><?php echo $i++; ?></td>

            <td><?php echo $banner_order; ?>&nbsp;&nbsp;<a href="#" class="view" id="<?php echo $value['id']?>"><i class="fa fa-pencil fa-fw"></i></a></td>

            <td><?php echo $value['banner_name']?></td>

            <td><?php echo $value['banner_content']?></td>

            <td><img src="<?php echo base_url().'upload/banner/'. $value['banner_image'] ?>" width="100" height="100"></td>

            <td><a href="<?php echo base_url().'admin/banner/tab_bannerEdit/'.$value['id'];?>"><i class="fa fa-pencil fa-fw"></i></a>|<a href="<?php echo base_url().'admin/banner/tab_bannerdelete/'.$value['id'];?>" onclick="return confirm('Are you sure delete ?')"><i class="fa fa-trash-o fa-fw"></i></a></td>

           </tr>

          <?php }?>

        </tbody>

      </table>

    </div>

  </div>

</sec tion>
<div class="modal fade  custom-width" id="modal-1">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">
      
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).on('click','.view',function(){
  var id = $(this).attr('id');
  if(id){
    $.ajax({
      url : '<?php echo site_url(); ?>admin/Banner/ChangeTabBannerOrder/?id='+id,
      success: function(result) {
        if(result){
          $('.modal-content').html(result);
          $('#modal-1').modal('show');                              

        }
      }
    });
  }
});

$(document).on('submit','#change_banner_order',function(e){
  e.preventDefault();
  var data = $(this).serialize();
  if(data){
    $.ajax({
      url: "<?php echo site_url(); ?>admin/Banner/UpdateTabBannerOrder",
      data: data,
      type: 'POST',
      success: function(result) {
        window.location.href = "<?php echo site_url(); ?>admin/Banner/tab_banner";       
      }
    });
  }             
});
</script>