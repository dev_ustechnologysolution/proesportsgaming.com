<section class="content">
    <div class="row">
        <div class="col-md-8">
            <?php $this->load->view('admin/common/show_message') ?>
            <div class="box box-primary">             
                <form  action="<?php echo base_url(); ?>admin/categories/<?php echo (isset($category_data)) ? 'categoriesUpdate' : 'categoriesInsert' ?>" method="post" enctype="multipart/form-data" class="admin_game_edit_form">
                    <input type="hidden" value="<?php echo (isset($category_data)) ? $category_data[0]['id'] : ''; ?>" name="id">
                    <div class="box-body"> 
                    <?php if($type == 0){ ?>                       
                         <div class="form-group">
                            <label for="name">Select Main Category</label>
                            <select class="form-control" name="parent_id">
                                <option value="0">Please Select Category</option>
                                <?php 
                                foreach ($category_list as $key => $value) { 
                                    if ($value['parent_id'] == 0) { ?>
                                    <option <?php echo ((isset($category_data)) && ($category_data[0]['parent_id'] == $value['id'])) ? 'selected' : ''; ?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php }
                                } ?>
                            </select>
                        </div>
                    <?php } ?>
                        <div class="form-group">
                            <label for="name"><?php echo ($type == 0) ? 'Sub Category Name' : 'Category Name'; ?></label>
                            <input type="text" required id="name" class="form-control" name="name" value="<?php echo (isset($category_data)) ? $category_data[0]['name'] : ''; ?>"/>
                        </div>
                        <div class="input_fields_container"></div>
                    </div>

                    <div class="box-footer">                                                         
                        <button class="btn btn-primary" type="submit" id="edit_game_update">Submit</button>
                    </div>
                </form>                
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        var x = 1;
        max_fields_limit = 10;
        $(".add_cat").click(function(e){
            e.preventDefault();            
            if(x < max_fields_limit){ 
                x++; 
                $('.input_fields_container').append('<div class="form-group row remove_'+x+'" data-seq="remove_'+x+'"><div class="col-sm-10 "><input type="text" required class="form-control" name="name[]" value=""/></div><div class="col-sm-1 "><button class="btn btn-danger remove_field" type="button">Remove</button></div></div>'); 
            } else {
                $('.add_cat').attr('disabled','true');
            }
        });
        
        
        $('.input_fields_container').on("click",".remove_field", function(e){ 
            e.preventDefault();
            data = $(this).closest('[data-seq]').data('seq'); 
            $('.'+data).remove(); 
            x--;
        })        
    }); 
    function showDiv(divId, element) {
        document.getElementById(divId).style.display = element.value == 3 ? 'block' : 'none';
    }   
</script>
