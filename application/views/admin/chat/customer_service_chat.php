<section class="content background-white">
   <div class="row">
      <div class="col-md-12">
         <div class="row">
            <div class="agent_navigation pull-right">

               <a href="<?php echo base_url();?>admin/chat/create_new_agent">ADD NEW</a><a href="<?php echo base_url();?>admin/chat/cust_chat_history">HISTORY</a><a href="" class="" onclick="location.reload()">Refresh</a>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 xwb-main-contact-conversation">
               <div class="table-responsive agent_table">
                  <table class="table table-hover" id="example1">
                     <thead>
                        <tr class="active">
                           <th>Sl No.</th>
                           <th>Customer Service Online</th>
                           <th>Agent Type</th>
                           <th>Notes</th>
                           <th style="text-align: center;">Open Chat</th>
                           <th>Online Time</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           $CI =& get_instance();
                           $result = $CI->chatsocket->getConversationAdminside();
                           if ($result['status'] == true) 
                           {
                               $conversation = $result['users_conversation'];
                               foreach ($conversation as $key => $value) 
                               {
                                   $user_to = $value['user_to'];
                                   $offline = 'offline';
                                   $last_logout = $value['last_logout'];
                                   $last_login = $value['last_login'];

                                  
                                       if (strtotime($last_logout) < strtotime($last_login)) {
                                                                             
                                        $newtime = strtotime(date("Y-m-d H:i:s"))-strtotime($last_login);
                                        $days = floor($newtime / 86400 % 7);
                                        $hours = floor($newtime / 3600 % 24);
                                        $mins = floor($newtime/ 60 % 60);
                                        $secs = floor($newtime % 60);
                                      if($days == '00'){

                                      if ($hours == '00') {

                                          if ($mins == '00') {
                                               $loggin_time = "<span class='color_orange'>";
                                               $loggin_time .= sprintf('%02d sec', $secs);
                                               $loggin_time .= "</span>";
                                             }
                                                else
                                                {
                                                $loggin_time = "<span class='color_orange'>";
                                                $loggin_time .= sprintf('%02d mins %02d sec', $mins, $secs);
                                                $loggin_time .= "</span>";
                                                }
                                             }
                                             else
                                             {  $loggin_time = "<span class='color_orange'>";
                                                 $loggin_time .= sprintf('%02d hrs %02d mins %02d sec', $hours, $mins, $secs);
                                                 $loggin_time .= "</span>";
                                             }
                                         }
                                          else
                                         {  
                                              $loggin_time = "<span class='color_orange'>";
                                              $loggin_time .= sprintf('%02d days %02d hrs %02d mins %02d sec', $days, $hours, $mins, $secs);
                                              $loggin_time .= "</span>";
                                         }
                                       } 
                                        else
                                        {
                                            $loggin_time = "<span class='color_blue'>offline</span>";
                                        }
                                    
                                   if ($_SESSION['admin_user_agent_id'] == $value['user_to']) {
                                    $chat_link = '<a class="open_cust_list_for_chat" href="'.base_url().'admin/chat/chat_with_cust"><i class="fa fa-comments color_orange" aria-hidden="true"></i></a>'; 
                                    }
                                    else
                                    {
                                  $chat_link = '<a class="agent_login_for_chat" id="'.$value['user_to'].'"><i class="fa fa-comments" aria-hidden="true"></i></a>';  
                                     }
                                     
                                     
                                   $message_page .= '<tr>
                                           <td>' . $value['id'] . '</td>
                                           <td>' . $value['display_name'] . '</td>
                                           <td>' . $value['agent_type'] . '</td>
                                           <td>' . $value['notes'] . '</td>
                                           <td align="center">'.$chat_link.'</td>
                                           <td>'.$loggin_time.'</td>
                                           <td><a class="delete_agent" id="'.$value['user_to'].'"><i class="fa fa-trash-o fa-fw"></i></a></td>
                                         </tr>';

                               }
                               // $message_page .= csListUsers();
                               if ($return) {
                                   return $message_page;
                               } else {
                                   echo $message_page;
                               }
                           } 
                           else {
                               $list = $result['error'];
                           }
                           ?>
                     </tbody>
                  </table>
               </div>
            </div>
           <div class="modal fade custom-width" id="modal-1">
            <div class="modal-dialog" style="width: 50%;">
              <div class="modal-content">
                
              </div>
            </div>
          </div>
         <script> 
            $(document).on('click','.agent_login_for_chat',function(){
              var id = $(this).attr('id');
              if(id){
                $.ajax({
                  url : '<?php echo site_url(); ?>admin/FrontUser/chat_security_pass/'+id,
                  success: function(result) {
                    if(result){
                      $('.modal-content').html(result);
                      $('#modal-1').modal('show');                              
                    }
                  }
                })
              }
            })
            $(document).on('click','.delete_agent',function(){
             var id = $(this).attr('id');
              if(id){
                $.ajax({
                  url : '<?php echo site_url(); ?>admin/chat/security_password_data/'+id,
                  success: function(result) {
                    if(result){
                      $('.modal-content').html(result);
                      $('#modal-1').modal('show');                              
                    }
                  }
                })
              }
            })
             $(document).on('submit','#security_id',function(e){
            e.preventDefault();
            var data = $(this).serialize();
              $.ajax({
                url: "<?php echo site_url(); ?>admin/chat/delete_agent_data",
                data: data,
                type: 'POST',
                success: function(data_value) {
                if(data_value == '1')
                {
                  window.location.href = "<?php echo site_url(); ?>admin/chat/";
                }
                else
                {
                  alert("Password Error");
                }
              }
              });
          });
          </script> 
         </div>
      </div>
   </div>
</section>


