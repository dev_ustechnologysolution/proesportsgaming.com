
<section class="content background-white box box-primary">

  <div class="row ">

    <div class="col-md-12">
    	<form action="<?php echo base_url();?>admin/chatads/create_pvpads/" method="POST" enctype='multipart/form-data' class="add_ads"  onsubmit="return adsimage_size_validation()">
    		  <input type="hidden" class="form-control" id="ads_category" name="ads_category" value="<?php echo $ads_cat; ?>">
    		  <div class="row">
    		  <div class="form-group col-sm-6">
    		  	<div class="col-sm-1">
    		  		<input type="radio" name="selected_commercial" id="selected_youtube_ads" value="selected_youtube" checked>
    		  	</div>
    		  	<div class="col-sm-11">
		    		<div>
					    <label>Youtube Link :</label>
					    <input type="url" class="form-control" id="youtube_link" name="youtube_link" placeholder="Enter Youtube Link" required="required">			
					    <label id="upload_youtube_lable">Youtube Commercial Link Title:</label>
					    <input type="text" class="form-control" id="youtube_link_title" name="youtube_link_title" placeholder="Enter Youtube Link Title" required="required">
					  </div>
					</div>
			</div>
			<div class="form-group col-sm-6">
    		  	<div class="col-sm-1">
    		  		<input type="radio" name="selected_commercial" id="selected_commercial_ads" value="selected_commercial">
    		  	</div>
    		  	<div class="col-sm-11">
				   <div>
				    <label>Upload Commercial:</label>
				    <input type="file" class="form-control" id="upload_commercial" name="upload_commercial" placeholder="Upload Commercial" accept="video/mp4,video/3gp,video/ogg" style="display: none;">
				    <label style="display: none;" id="upload_commercial_lable">Commercial Title:</label>
				    <input type="text" class="form-control" id="commercial_title" name="commercial_title" placeholder="Enter Commercial Title" style="display: none;">
				  </div>
				</div>
			  </div>
			  </div>
			   <div class="form-group">
			    <label>Upload Banner Title:</label>
			    <input type="text" class="form-control" id="upload_banner_title" name="upload_banner_title" placeholder="Enter Upload Banner Title" required>
			    <label>Upload Banner:</label>
			    <label id="banner_size" style="display:none"></label>
			    <input type="file" class="form-control" id="upload_banner" name="upload_banner" placeholder="Upload Banner" required accept="image/*">			    
			  </div>

			   <div class="form-group">
			    <label>Banner Link Title:</label>
			    <input type="text" class="form-control" id="banner_link_title" name="banner_link_title" placeholder="Enter Banner Link Title" required>
			    <label>Banner Link:</label>
			    <input type="text" class="form-control" id="banner_link" name="banner_link" placeholder="Enter Banner Link" required>			    
			  </div>			  

			   <div class="form-group">
			    <label>Sponsered By Logo Title:</label>
			    <input type="text" class="form-control" id="sponsered_by_logo_title" name="sponsered_by_logo_title" placeholder="Enter Sponsered By Logo Title" required>
			    <label>Sponsered By Logo Link:</label>
			    <input type="text" class="form-control" id="sponsered_by_logo_link" name="sponsered_by_logo_link" placeholder="Enter Sponsered By Logo Link" required>
			    <label>Upload Sponsered By Logo:(120px-80px)</label>
    			<label id="sponsered_by_logo_size" style="display:none"></label>
			    <input type="file" class="form-control" id="upload_sponsered_by_logo" name="upload_sponsered_by_logo" placeholder="Upload Sponsered By Logo" required accept="image/*">			    
			  </div>
			   <div class="form-group">
			    <label>10 Sec Intro Title:</label>
			    <input type="text" class="form-control" id="ten_sec_intro_title" name="ten_sec_intro_title" placeholder="Enter 10 Sec Intro Title" required>
			    <label>Upload 10 Sec Intro:</label>
			    <input type="file" class="form-control" id="upload_ten_sec_intro" name="upload_ten_sec_intro" placeholder="Upload 10 Sec Intro" require  accept="video/mp4,video/3gp,video/ogg">
			  </div>

			    <div class="form-group">
			    <label>Autoskipp Time:</label> <input type="number" class="form-control" placeholder="Enter Autoskipp Second" id="autoskipp_time" name="autoskipp_time" required>
			  </div>
			  
				</div>
			  </div>
			  <input type="submit" name="submit" class="btn btn-default" value="Submit">
			</form>

    </div>
  </div>

</section>