<?php if ($content) { ?>
	<div class="content-wrapper">
		<section class="content-header">
			<h1><?php if(isset($page_name)) echo $page_name; ?></h1>
			<?php echo $this->breadcrumbs->show(); ?>  
		</section>
		<section class="content"> 
			<?php echo $content;?>
		</section>
	</div>
<?php } else { ?>    
	<div class="content-wrapper" style="background:#000; height:720px; ">
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-sm-3"> </div>
			<div class="col-sm-6" style="margin:20px 0px;">
				<img src="<?php echo base_url().'assets/frontend/images/logo.png'; ?>" width="100%" />		
			</div>
			<div class="col-sm-3"> </div>
		</div>
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-3 searchcountry">
				<div class="input-group add-on form-control add-on-for-search">
					<input placeholder="Search ID/Country" name="searchcountry" id="searchcountry" type="text" onkeyup="searchcountry()">
					<span class="glyphicon glyphicon-search form-control-feedback"></span>
				</div>
			</div>
			<div class="col-sm-2"></div>
			<div class="col-sm-3 searchstate">
				<div class="input-group add-on form-control add-on-for-search">
					<input placeholder="Search ID/State of US" name="searchstate" id="searchstate" type="text" onkeyup="searchstate()">
					<span class="glyphicon glyphicon-search form-control-feedback"></span>
				</div>
			</div>
			<div class="col-sm-2"></div>
	</div>
	<div class="row searchcountry_div">
		<div class="col-sm-4 searchcountry_list">
			<ul class="country_list">				
			</ul>
		</div>
	</div>
	<div class="row searchstate_div">
		<div class="searchstate_list">
			<ul class="state_list">				
			</ul>
		</div>
	</div>
	<div class="row" style="margin:20px 0px;" oncontextmenu="return false;">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
			<video preload="auto" class="dashboard_vid" autoplay style="width: 100%;" loop src="<?php echo base_url();?>upload/admin_dashboard/<?php echo $admin_map;?>"> 
			  <source src="/" type="video/mp4">
			</video>
		</div>
		<div class="col-sm-2"></div>
	</div>
	<div class="row totalall" style="margin:20px 0px;">
		<div class="col-sm-4">
			<div>Active Players : <?php echo $activeplayers; ?></div>
		</div>
		<div class="col-sm-4"> 
			<div>Balance: $<?php echo number_format((float) $totalbalance, 2, '.', ''); ?> <a class="edittotalbalance" id="<?php echo $totalbalance; ?>"><i class="fa fa-pencil fa-fw"></i></a></div>
		</div>
		<div class="col-sm-4">
			<div>Fees Collected: $<?php echo number_format((float) $totalfees, 2, '.', ''); ?> <a class="edittotalfees" id="<?php echo $totalfees; ?>"><i class="fa fa-pencil fa-fw"></i></a></div>
		</div>
	</div>
</div>
<?php } ?>
<!-- Modal 1 (Basic)-->
<div class="modal fade  custom-width" id="modal-basic">
	<div class="modal-dialog" style="width: 50%;">
		<div class="modal-content">
		</div>
	</div>
</div>
<script type="text/javascript">
// edit total balance 
$(document).on('click','.edittotalbalance',function(){
	var id = $('.edittotalfees').attr('id');
	if(id){
		$.ajax({
			url : '<?php echo base_url();?>admin/Payment/edittotalbalance_security_data/'+id,
			success: function(result) {
				if(result){
					$('.modal-content').html(result);
					$('#modal-basic').modal('show');															
				}
			}
		});
	}
});
 $(document).on('submit','#balance_id',function(e){
	e.preventDefault();
    var data = $(this).serialize();
	 $.ajax({
     url: "<?php echo base_url();?>admin/Payment/edit_balance/",
     data: data,
     type: 'POST',
 	 success: function(result) {
			$('.modal-content').html(result);
			$('#modal-1').modal('show');				
	 }
	});
});
$(document).on('submit','#editbalance_id',function(e){
	e.preventDefault();
	var data = $(this).serialize();
	if(data){
		$.ajax({
			url: "<?php echo base_url();?>admin/Payment/update_balance",
			data: data,
			type: 'POST',
			success: function(result) {
				window.location.href = "<?php echo base_url();?>admin/Dashboard/";				
			}
		});
	}							
});


// edit total fees
$(document).on('click','.edittotalfees',function(){
	var id = $('.edittotalbalance').attr('id');
	if(id){
		$.ajax({
			url : '<?php echo base_url();?>admin/Payment/edittotalfees_security_data/'+id,
			success: function(result) {
				if(result){
					$('.modal-content').html(result);
					$('#modal-basic').modal('show');															
				}
			}
		});
	}
});
 $(document).on('submit','#fees_id',function(e){
	e.preventDefault();
    var data = $(this).serialize();
	 $.ajax({
     url: "<?php echo base_url();?>admin/Payment/edit_fees/",
     data: data,
     type: 'POST',
 	 success: function(result) {
			$('.modal-content').html(result);
			$('#modal-1').modal('show');				
	 }
	});
});
$(document).on('submit','#editfees_id',function(e){
	e.preventDefault();
	var data = $(this).serialize();
	if(data){
		$.ajax({
			url: "<?php echo base_url();?>admin/Payment/update_fees",
			data: data,
			type: 'POST',
			success: function(result) {
				window.location.href = "<?php echo base_url();?>admin/dashboard/";				
			}
		});
	}							
});

// active and decative
$(document).on('click','input[name=country_status]',function(){
	var countryid = $(this).attr('id');
	if ($('#' + countryid).is(":checked")) {
		$.ajax({
			url : '<?php echo base_url();?>admin/My_Controller/country_status_active/'+countryid,
			success: function(result) { }
		});
	} else {
		$.ajax({
			url : '<?php echo base_url();?>admin/My_Controller/country_status_dective/'+countryid,
			success: function(result) { }
		});
	}
});
// active and decative
$(document).on('click','input[name=state_status]',function(){
	var stateid = $(this).attr('id');
	if ($('#' + stateid).is(":checked")) {
		$.ajax({
			url : '<?php echo base_url();?>admin/My_Controller/state_status_active/'+stateid,
			success: function(result) {
			}
		});
	} else {
		$.ajax({
			url : '<?php echo base_url();?>admin/My_Controller/state_status_dective/'+stateid,
			success: function(result) {
			}
		});
	}
});
// search countries //
function searchcountry() {
	var searchcountry = $('#searchcountry').val();
	var postData = { 'searchcountry': searchcountry };
  $.ajax({
    type: "post",
    url: base_url+"admin/My_Controller/searchcountry",
    data: postData,
    success: function (res) {
      $('.country_list li').remove();
      var jdata = $.parseJSON(res);
      $('.country_list').append(jdata.country);
    }
  });
}

// search states of US//
function searchstate() {
	var searchstate = $('#searchstate').val();
	var postData = { 'searchstate': searchstate };
  $.ajax({
    type: "post",
    url: base_url+"admin/My_Controller/searchstate",
    data: postData,
    success: function (res) {
      $('.state_list li').remove();
      var jdata = $.parseJSON(res);
      $('.state_list').append(jdata.country);
    }
  });
}
</script>