<script src="<?php echo base_url().'assets/backend/';?>js/bootstrap-formhelpers-selectbox.js"></script>
<script src="<?php echo base_url().'assets/backend/';?>js/bootstrap-formhelpers-countries.en_US.js"></script>
<script src="<?php echo base_url().'assets/backend/';?>js/bootstrap-formhelpers-countries.js"></script>
<script src="<?php echo base_url().'assets/backend/';?>js/bootstrap-formhelpers-states.en_US.js"></script>
<script src="<?php echo base_url().'assets/backend/';?>js/bootstrap-formhelpers-states.js"></script>
<script src="<?php echo base_url().'assets/'; ?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="<?php echo base_url().'assets/'; ?>plugins/select2/select2.full.min.js"></script>
<script type="text/javascript">base_url='<?php echo base_url(); ?>';</script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script> $.widget.bridge('uibutton', $.ui.button); </script>
<script src="<?php echo base_url().'assets/backend/'; ?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url().'assets/'; ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url().'assets/'; ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url().'assets/'; ?>plugins/morris/morris.min.js"></script>
<script src="<?php echo base_url().'assets/'; ?>plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url().'assets/'; ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url().'assets/'; ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="<?php echo base_url().'assets/'; ?>plugins/knob/jquery.knob.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="<?php echo base_url().'assets/'; ?>plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url().'assets/'; ?>plugins/datepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url().'assets/'; ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url().'assets/'; ?>plugins/fastclick/fastclick.min.js"></script>
<script src="<?php echo base_url().'assets/backend/'; ?>dist/js/app.min.js"></script>
<script src="<?php echo base_url().'assets/backend/'; ?>dist/js/demo.js"></script>
<script src="<?php echo base_url().'assets/backend/'; ?>/js/ajax.js"></script>
<script src="<?php echo base_url().'assets/backend/'; ?>/js/form_validation.js"></script>
<!-- <script src="<?php //echo base_url().'assets/backend/'; ?>js/jquery.datetimepicker.full.js"></script> -->
<script src="<?php echo base_url().'assets/'; ?>plugins/iCheck/icheck.min.js"></script>

<script>
  $(function () {
    $('#example1').dataTable({
    aLengthMenu: [
      [25, 50, 100, 200, -1],
      [25, 50, 100, 200, "All"]
    ],
    iDisplayLength:25,
    });

    $('#example2').dataTable({
    aLengthMenu: [
      [25, 50, 100, 200, -1],
      [25, 50, 100, 200, "All"]
    ],
    iDisplayLength:25,
    columnDefs: [ { orderable: false, targets: [0]}]
    });
  });
</script>



	<script src="<?php echo base_url(); ?>assets/backend/js/jquery.knob.js"></script>



		<!-- jQuery File Upload Dependencies -->

		<script src="<?php echo base_url(); ?>assets/backend/js/jquery.ui.widget.js"></script>

		<script src="<?php echo base_url(); ?>assets/backend/js/jquery.iframe-transport.js"></script>

		<script src="<?php echo base_url(); ?>assets/backend/js/jquery.fileupload.js"></script>

		<script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>ckeditor/adapters/jquery.js"></script>
	 
<script>
  try { CKEDITOR.replace('editor1'); } catch {}
// <![CDATA[
/* if(CKEDITOR) {
  CKEDITOR.replace('editor', { */
    /* 'extraPlugins': 'doksoft_backup,doksoft_stat,zoom',
    'filebrowserImageBrowseUrl': 'http://localhost/play_box/ckeditor/plugins/imgbrowse/imgbrowse.html',
    'filebrowserImageUploadUrl': 'http://localhost/play_box/ckeditor/plugins/iaupload.php' */
//    extraAllowedContent: 'audio[*]{*}',
  /* });
  CKEDITOR.config.extraAllowedContent = 'audio[*]{*}';
} */
// ]]>
</script>
		<script type="text/javascript">
    var ul = $('#upload ul');
    $('#drop a').click(function(){
      $(this).parent().find('input').click();
    });
    $('#upload').fileupload({
      dropZone: $('#drop'),
      add: function (e, data) {
        var tpl = $('<li class="working"><input type="text" value="0" data-width="48" data-height="48"'+' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><p></p><span></span></li>');
        tpl.find('p').text(data.files[0].name).append('<i>' + formatFileSize(data.files[0].size) + '</i>'); 
        $('#addbimage').val($('#addbimage').val()+'/'+data.files[0].name)
        data.context = tpl.appendTo(ul);
        tpl.find('input').knob();
        tpl.find('span').click(function(){
          if (tpl.hasClass('working')){
            jqXHR.abort();
          }
          tpl.fadeOut(function() {
            tpl.remove();
          });
        });
        var jqXHR = data.submit();
      },
      progress: function(e, data){
        var progress = parseInt(data.loaded / data.total * 100, 10);
        data.context.find('input').val(progress).change();
        if (progress == 100) {
          data.context.removeClass('working');
        }
      },
      fail:function(e, data){
        data.context.addClass('error');
      }
    });
    $(document).on('drop dragover', function (e) {
      e.preventDefault();
    });
    function formatFileSize(bytes) {
      if (typeof bytes !== 'number') {
        return '';
      }
      if (bytes >= 1000000000) {
        return (bytes / 1000000000).toFixed(2) + ' GB';
      }
      if (bytes >= 1000000) {
        return (bytes / 1000000).toFixed(2) + ' MB';
      }
      return (bytes / 1000).toFixed(2) + ' KB';
    }

    $('.building').click(function(){
      $('.building').removeClass('active');
      var id = $(this).attr('val');
      $('#building').val(id);
      $('#buildid'+id).addClass('active');
    });
 </script>

<script>
  $('#expiry_date_time').datetimepicker({
    dayOfWeekStart : 1,
    lang:'en',
    disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
    startDate:	'2016/03/05'
  });
  function senddraft() {
    var tomail=$('#tomail').val();
    var subject=$('#subject').val();
    var message=$('#compose-textarea').val();
    var user_email=$('#user_email').val();
    var site_url="<?php echo base_url(); ?>";
    alert('senddraft');
    $.ajax({
      url:site_url+"mail/save_draft",
      type:"POST",
      data:{user_email:user_email},
      success:function(returnData){
        if(returnData=='success') {
          alert('sdfsdfdsf');
        }
      }
    });
  }
</script>

	 <script>

      $(function () {

        //Enable iCheck plugin for checkboxes

        //iCheck for checkbox and radio inputs

        $('.mailbox-messages input[type="checkbox"]').iCheck({

          checkboxClass: 'icheckbox_flat-blue',

          radioClass: 'iradio_flat-blue'

        });



        //Enable check and uncheck all functionality

        $(".checkbox-toggle").click(function () {

          var clicks = $(this).data('clicks');

          if (clicks) {

            //Uncheck all checkboxes

            $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");

            $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');

          } else {

            //Check all checkboxes

            $(".mailbox-messages input[type='checkbox']").iCheck("check");

            $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');

          }

          $(this).data("clicks", !clicks);

        });



        //Handle starring for glyphicon and font awesome

        $(".mailbox-star").click(function (e) {

          e.preventDefault();

          //detect type

          var $this = $(this).find("a > i");

          var glyph = $this.hasClass("glyphicon");

          var fa = $this.hasClass("fa");



          //Switch states

          if (glyph) {

            $this.toggleClass("glyphicon-star");

            $this.toggleClass("glyphicon-star-empty");

          }



          if (fa) {

            $this.toggleClass("fa-star");

            $this.toggleClass("fa-star-o");

          }

        });

      });

	  

	  

	  

	   //Date range picker

        $('#reservation').daterangepicker();

        //Date range picker with time picker

        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});

        //Date range as a button

        $('#daterange-btn').daterangepicker(

            {

              ranges: {

                'Today': [moment(), moment()],

                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

                'Last 7 Days': [moment().subtract(6, 'days'), moment()],

                'Last 30 Days': [moment().subtract(29, 'days'), moment()],

                'This Month': [moment().startOf('month'), moment().endOf('month')],

                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

              },

              startDate: moment().subtract(29, 'days'),

              endDate: moment()

            },

        function (start, end) {

          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

        }

        );

    </script>

    <script>

	var _URL = window.URL || window.webkitURL;

$("#file").change(function (e) {

    var file, img;

    if ((file = this.files[0])) {

        img = new Image();

		 img.onload = function () {

           $("#image_size").html(this.width + "-" + this.height);

        };

        img.src = _URL.createObjectURL(file);

    }

});



function image_size_validation()

{



	var v=$("#image_size").html();

	if(v!='')

	{

	var r=v.split('-');

	if(r[0]<=190 && r[1]<=112)

	{

		return true;

	}

	else

	{

		alert('your image size '+r[0]+'px - '+r[1]+'px');

		return false;

	}

	}

}

	

	</script>

    <script src="<?php echo site_url();?>/xwb_assets/js/chatsocket.js"></script>


  </body>

</html>

