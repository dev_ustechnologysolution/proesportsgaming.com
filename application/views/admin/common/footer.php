<footer class="main-footer">
  <div class="pull-right hidden-xs"></div>
</footer>
</div><!-- ./wrapper -->
<div class="adminloader" id="adminloader">
  <div class="loader_sub_div">
    <img src="<?php echo base_url(); ?>/assets/frontend/images/ajax-loader.gif">
  </div>
</div>
<?php
$CI = & get_instance();
$socketUser = ($CI->chatsocket->current_user != null) ? $CI->chatsocket->current_user : $_SESSION['admin_user_id'];
$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
?>
<script src="<?php echo $protocol.addhttp($CI->chatsocket->domain_name.':'.$CI->chatsocket->socket_port.'/socket.io/socket.io.js');?>"></script> 
<link rel="stylesheet" href="<?php echo base_url('xwb_assets/js/prettyPhoto-3.1.6/css/prettyPhoto.css'); ?>" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<script src="<?php echo base_url('xwb_assets/js/prettyPhoto-3.1.6/js/jquery.prettyPhoto.js'); ?>" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo base_url('xwb_assets/js/bootbox.min.js');?>"></script>
<script type="text/javascript">
  if (typeof io != 'undefined') {
    var socket = io.connect( "<?php echo addhttp($CI->chatsocket->domain_name.':'.$CI->chatsocket->socket_port);?>",{secure: true});
  }
  var varSCPath = "<?php echo base_url('xwb_assets'); ?>";
  var socketUser = "<?php echo $socketUser; ?>";
  window.formKey = "<?php echo csFormKey(); ?>";
  if (typeof socket != 'undefined') {
    socket.emit( "socket id", { "user": socketUser } );
  }
</script>
<script language="javascript" type="text/javascript">
  function OpenPopupCenter(pageURL) {
    var targetWin = window.open(pageURL);
    targetWin.location = pageURL;
  }
</script>