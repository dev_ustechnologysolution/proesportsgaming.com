<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo ADMIN_TITLE ;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/backend/'; ?>bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/chat.css">

  <!-- Font Awesome -->

  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/fontwesome/css/font-awesome.min.css'); ?>">

  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url() ?>assets/frontend/images/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url() ?>assets/frontend/images/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url() ?>assets/frontend/images/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>assets/frontend/images/favicon/favicon-16x16.png">
  <!-- <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script> -->
  <!-- Ionicons -->

  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

     <!-- DataTables -->

  <link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>plugins/datatables/dataTables.bootstrap.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/backend/'; ?>dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>plugins/select2/select2.css" type="text/css" charset="utf-8" />
  <link rel="stylesheet" href="<?php echo base_url().'assets/backend/'; ?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>plugins/iCheck/flat/blue.css">
  <link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>plugins/morris/morris.css">
  <link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>plugins/datepicker/datepicker3.css">
  <link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>plugins/daterangepicker/daterangepicker-bs3.css">

  <link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="<?php echo base_url().'assets/backend/'; ?>bootstrap/css/custom.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/bootstrap/css/'; ?>jquery.datetimepicker.css"/>
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
  <script src="<?php echo base_url().'assets/backend/'; ?>js/jquery-1.11.3.min.js"></script>
  <link href="<?php echo base_url(); ?>assets/plugins/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
  <script src="<?php echo base_url().'assets/plugins/datepicker/js/';?>moment-with-locales.min.js"></script>
  <script src="<?php echo base_url().'assets/plugins/datepicker/js/';?>bootstrap-datetimepicker.min.js"></script>
  <script type="text/javascript">
    var freeevent = '<?php echo free_event; ?>';
    var free_event = (freeevent != '') ? 'free': 'paid'; 
    var time_zne = new Date().getTimezoneOffset() == 0 ? 0 : -new Date().getTimezoneOffset();
    document.cookie = 'time_zne = '+time_zne;
  </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <header class="main-header">
      <a href="<?php echo base_url().'admin/dashboard/'; ?>" class="logo">
        <span class="logo-mini"><b><?php echo substr(ADMIN_TITLE,0,3) ;?></b></span>
        <span class="logo-lg"><b><?php echo ADMIN_TITLE ;?></b></span>
      </a>
      <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo base_url().'upload/profile_img/'.$this->session->userdata('img'); ?>" class="user-image" alt="User Image">
                <span class="hidden-xs"><?php echo $this->session->userdata('admin_user_name'); ?></span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="<?php echo base_url().'upload/profile_img/'.$this->session->userdata('img'); ?>" class="img-circle" alt="User Image">
                  <p>
                    <?php echo $this->session->userdata('admin_user_name'); ?>
                  </p>
                </li>
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="<?php echo base_url().'admin/user/profile'; ?>" class="btn btn-default btn-flat">Profile</a>
                  </div>
                  <div class="pull-right">
                    <a href="<?php echo base_url().'admin/user/logout'; ?>" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>