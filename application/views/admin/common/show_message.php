<!-- message -->
<?php
if($this->session->userdata('message_succ')!='') {
?>
<div class="box box-success">
  <div class="box-header with-border">
    <h3 class="box-title"><?php echo $this->session->userdata('message_succ');?></h3>
    <div class="box-tools pull-right">
      <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
    </div><!-- /.box-tools -->
  </div><!-- /.box-header -->
</div><!-- /.box -->
<!-- message -->
<?php
} else if($this->session->userdata('message_err')!='') {
?>
<div class="box box-danger">
  <div class="box-header with-border">
    <h3 class="box-title"><?php echo $this->session->userdata('message_err');?></h3>
    <div class="box-tools pull-right">
      <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
    </div><!-- /.box-tools -->
  </div><!-- /.box-header -->
</div><!-- /.box -->
<!-- message -->
<?php
} else if($this->session->flashdata('message_err')!='') {
?>
<div class="box box-danger">
  <div class="box-header with-border">
    <h3 class="box-title"><?php echo $this->session->userdata('message_err');?></h3>
    <div class="box-tools pull-right">
      <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
    </div><!-- /.box-tools -->
  </div><!-- /.box-header -->
</div><!-- /.box -->
<!-- message -->
<?php }
$this->session->unset_userdata('message_succ'); 
$this->session->unset_userdata('message_err');
?>