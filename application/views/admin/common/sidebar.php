<aside class="main-sidebar">
  <section class="sidebar">
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url().'upload/profile_img/'.$this->session->userdata('img')?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $this->session->userdata('admin_user_name')?></p>
        <p><?php echo $this->session->userdata('admin_user_role')?></p>
      </div>
    </div>
    <ul class="sidebar-menu">
      <li class="treeview">
        <a href="<?php echo base_url().'admin/dashboard/'; ?>">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
			<?php if ($_SESSION['admin_user_role_id'] == 1) { ?>
        <li class="<?php if(isset($active) && ($active=='game' || $active=='game_list' || $active=='game_edit' || $active=='game_cat_list')) echo 'active' ;?> treeview">
          <a>
            <i class="fa fa-gamepad"></i> <span>Game</span> <i class="fa fa-angle-right pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if(isset($active) && ($active=='game' || $active=='game_list' || $active=='game_edit')) echo 'active' ;?> treeview">
              <a href="<?php echo base_url().'admin/game'; ?>">
                <i class="fa fa-list"></i> <span>Game List</span>
              </a>
            </li>
            <li class="<?php if(isset($active) && ($active=='game' || $active=='game_cat_list')) echo 'active' ;?> treeview">
              <a href="<?php echo base_url().'admin/game/game_cat_list'; ?>">
                <i class="fa fa-list"></i> <span>Game Category List</span>
              </a>
            </li>
          </ul>
        </li>
			<?php } ?>
      <?php if (MATCH_MODULE == 'on') { ?>
  			<li class="<?php if(isset($active) && ($active=='game_url')) echo 'active' ;?> treeview">
  				<a href="<?php echo base_url().'admin/game/gameUrl'; ?>">
  					<i class="fa fa-external-link"></i> <span>Watch Game</span>
  				</a>
        </li>
      <?php } ?>
      <li class="<?php if(isset($active) && ($active=='lvlby_bet_url' || $active=='live_lobby_list' || $active=='lobby_report_timer' || $active=='giveaway')) echo 'active' ;?> treeview">
        <a>
          <i class="fa fa-braille"></i> <span>Live Lobby</span> <i class="fa fa-angle-right pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="<?php if(isset($active) && ($active=='lvlby_bet_url')) echo 'active' ;?> treeview">
            <a href="<?php echo base_url().'admin/live_lobby/group_bet_url'; ?>">
              <i class="fa fa-external-link"></i> <span>Watch Live Lobby</span>
            </a>
          </li>
          <li class="<?php if(isset($active) && ($active=='live_lobby_list')) echo 'active' ;?> treeview">
            <a href="<?php echo base_url().'admin/live_lobby/live_lobby_list'; ?>">
            <i class="fa fa-list"></i> <span>Live Lobby List</span>
            </a>
          </li>
          <li class="<?php if(isset($active) && ($active=='lobby_report_timer')) echo 'active' ;?>">
            <a href="<?php echo base_url().'admin/live_lobby/lobby_report_timer'; ?>">
              <i class="fa fa-clock-o" aria-hidden="true"></i><span>Live Lobby Report Timer</span>
            </a>
          </li>
          <li class="<?php if(isset($active) && ($active=='giveaway')) echo 'active' ;?>">
            <a href="<?php echo base_url().'admin/giveaway'; ?>">
              <i class="fa fa-ticket" aria-hidden="true"></i><span>Giveaway</span>
            </a>
          </li>
        </ul>
      </li>
			<li class="<?php if(isset($active) && ($active=='uploaded_videos')) echo 'active' ;?> treeview">
				<a href="<?php echo base_url().'admin/game/uploaded_videos'; ?>">
					<i class="fa fa-video-camera"></i> <span>Latest Streams</span>
				</a>
      </li>
      <?php if (MATCH_MODULE == 'on') { ?>
        <li class="<?php if(isset($active) && ($active=='winner_list')) echo 'active' ;?> treeview">
          <a href="<?php echo base_url().'admin/winner'; ?>">
            <i class="fa fa-smile-o"></i> <span>Winner List</span>
          </a>
        </li>
        <li class="<?php if(isset($active) && ($active=='loser_list')) echo 'active' ;?> treeview">
          <a href="<?php echo base_url().'admin/loser'; ?>">
            <i class="fa fa-frown-o"></i> <span>Loser List</span>
          </a>
        </li>
      <?php } ?>
			<?php if ($_SESSION['admin_user_role_id'] == 1) { ?>
        <li class="<?php if(isset($active) && ($active == 'user_point_list' || $active=='package_list' || $active=='points_percentage' || $active== 'fee_per_game' || $active=='fee_per_lobby')) echo 'active' ;?> treeview">
          <a >
              <i class="fa fa-get-pocket" aria-hidden="true"></i> <span>Admin Packages List</span> <i class="fa fa-angle-right pull-right"></i>
          </a>
          <ul class="treeview-menu">
    				<li class="<?php if(isset($active) && ($active=='user_point_list')) echo 'active' ;?> treeview"><a href="<?php echo base_url().'admin/points/user_point_list'; ?>"> <i class="fa fa-bank"></i> <span>Players Purchases</span></a></li>
            <li class="<?php if(isset($active) && ($active=='package_list')) echo 'active' ;?>"><a href="<?php echo base_url().'admin/points'; ?>"><i class="fa fa-file-powerpoint-o"></i>Packages</a></li>
            <li class="<?php if(isset($active) && ($active=='points_percentage')) echo 'active' ;?>"><a href="<?php echo base_url().'admin/points/pointPercentage'; ?>"><i class="fa fa-percent" aria-hidden="true"></i></i>Manuel Credit Fee</a></li>
		        <li class="<?php if(isset($active) && ($active=='fee_per_game')) echo 'active' ;?>"><a href="<?php echo base_url().'admin/points/fee_per_game'; ?>"><i class="fa fa-percent" aria-hidden="true"></i></i>Fee Per Game</a></li>
            <li class="<?php if(isset($active) && ($active=='fee_per_lobby')) echo 'active' ;?>"><a href="<?php echo base_url().'admin/points/fee_per_lobby'; ?>"><i class="fa fa-percent" aria-hidden="true"></i></i>Live Lobby Fee</a></li>
          </ul>
        </li>
        <?php } ?>
        <li class="<?php if(isset($active) && ($active == 'trade_history' || $active=='pro_esports_membership' || $active=='subscriber_history' || $active == 'players_membership' || $active == 'tip_history' || $active == 'pro_esports_subscribe')) echo 'active' ;?> treeview">
          <a>
              <img src="<?php echo base_url().'assets/backend/'; ?>images/trade.png" width="18" /> &nbsp;<span>Trades & Subs</span> <i class="fa fa-angle-right pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if(isset($active) && ($active=='trade_history')) echo 'active' ;?> treeview"><a href="<?php echo base_url().'admin/trade'; ?>"> <img src="<?php echo base_url().'assets/backend/'; ?>images/trade_history.png" width="18" /> &nbsp; <span>Trade History</span></a>
            </li>

            <li class="<?php if(isset($active) && ($active=='tip_history')) echo 'active' ;?> treeview"><a href="<?php echo base_url().'admin/trade/tip_history'; ?>"><i class="fa fa-money"></i>  &nbsp; <span>Tip History</span></a>
            </li>
            
            <li class="membership_li <?php if(isset($active) && ($active=='pro_esports_membership')) echo 'active' ;?> treeview"><a style="padding:4px;" href="<?php echo base_url().'admin/membership'; ?>"> <img width="40" height ="30" class="customer_service_icon" <?php echo ($active=='pro_esports_membership')?'src="'.base_url().'assets/frontend/images/gaming_white.png"':'src="'.base_url().'assets/frontend/images/gaming.png"';?> >&nbsp; <span>Pro Esports Membership</span></a>
            </li>

            <li class="<?php if(isset($active) && ($active=='pro_esports_subscribe')) echo 'active' ;?> treeview"><a href="<?php echo base_url().'admin/Subscription'; ?>" style="padding:4px;"> <img  width="40" height ="30" src="<?php echo base_url().'assets/backend/'; ?>images/subs_player_plan.png" />&nbsp; <span>Players Subscribe Plans</span></a></li> 

            <li class="<?php if(isset($active) && ($active=='subscriber_history')) echo 'active' ;?> treeview"><a href="<?php echo base_url().'admin/trade/subscription'; ?>"> <img src="<?php echo base_url().'assets/backend/'; ?>images/subscriber_history.png" width="18" /> &nbsp; <span>Players Subscribers List</span></a>
            </li>
          </ul>
        </li>
        <li class="<?php if(isset($active) && ($active=='withdrawal' || $active=='withdrawal_paid_list' || $active == 'withdrawal_fee')) echo 'active' ;?> treeview">
          <a>
            <img src="<?php echo base_url().'assets/backend/'; ?>images/atm-icon.png" width="20" /> <span>Withdrawal Management </span><i class="fa fa-angle-right pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if(isset($active) && ($active=='withdrawal')) echo 'active' ;?>"><a href="<?php echo base_url().'admin/game/withdrawal'; ?>"><i class="fa fa-list"></i>  Withdrawal Request List</a></li>
            <li class="<?php if(isset($active) && ($active=='withdrawal_paid_list')) echo 'active' ;?>"><a href="<?php echo base_url().'admin/game/paid_withdrawal'; ?>"><i class="fa fa-list"></i>  Withdrawal Paid List</a></li>
           <!--  <li class="<?php if(isset($active) && ($active=='withdrawal_fee')) echo 'active' ;?>"><a href="<?php echo base_url().'admin/withdrawal/withdrawal_fee'; ?>"><i class="fa fa fa-percent"></i>  Withdrawal Fee</a></li> -->
          </ul>
        </li>
        <li class="<?php if(isset($active) && ($active=='usr' || $active=='usr_list' || $active=='usr_edit')) echo 'active' ;?> treeview">
          <a href="<?php echo base_url().'admin/FrontUser'; ?>"><i class="fa fa-user"></i> <span>Frontend User</span></i></a>
        </li>
        <li class="<?php if(isset($active) && ($active=='chat_banner' || $active=='chat_banner_list' || $active=='chat_banner_edit' || $active=='tab_banner' || $active=='tab_banner_list' || $active=='tab_banner_edit' || $active=='banner' || $active=='banner_list' || $active=='banner_edit')) echo 'active' ;?>"><a href="<?php echo base_url().'admin/banner'; ?>"><i class="fa fa-flag-o"></i>Banner List</a></li>
        <li class="<?php if(isset($active) && ($active == 'event_list')) echo 'active' ;?> treeview">
          <a href="<?php echo base_url().'admin/events'; ?>"><i class="fa fa-calendar"></i> <span>Event Management</span></a>
        </li>
        <?php if($_SESSION['admin_user_role_id'] == 1) { ?>
          <li class="<?php if(isset($active) && ($active=='categories_list' || $active=='store_levels' || $active=='attributes_list' || $active=='products_list' || $active=='product_ads')) echo 'active' ;?> treeview">
            <a>
              <img  width="28" height ="26" src="<?php echo base_url().'assets/backend/'; ?>images/tshirt.png" />&nbsp; <span>Pro Esports Store</span> <i class="fa fa-angle-right pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li class="<?php if(isset($active) && ($active=='store_levels')) echo 'active'; ?>"><a href="<?php echo base_url().'admin/Store_levels'; ?>">&nbsp; <i class = "fa fa-bar-chart"></i>&nbsp;Store Levels</a></li>
              <li class="<?php if(isset($active) && ($active=='categories_list')) echo 'active'; ?>"><a href="<?php echo base_url().'admin/categories'; ?>"> <img  width="24" height ="24" src="<?php echo base_url().'assets/backend/'; ?>images/node.png" />&nbsp;Category Master</a></li>
              <li class="<?php if(isset($active) && ($active=='attributes_list')) echo 'active'; ?>"><a href="<?php echo base_url().'admin/attributes'; ?>"> <span class="glyphicon glyphicon-sort-by-attributes"></span>&nbsp;Attributes Master</a></li>
              <li class="<?php if(isset($active) && ($active=='products_list')) echo 'active'; ?>"><a href="<?php echo base_url().'admin/products'; ?>"> <span class="fa fa-get-pocket"></span>&nbsp;Products Master</a></li>
              <li class="<?php if(isset($active) && ($active=='product_ads')) echo 'active'; ?>">
                <a href="<?php echo base_url().'admin/products/product_ads'; ?>">
                  <span class="fa fa-audio-description"></span>&nbsp;Product Ads
                </a>
              </li>
            </ul>
          </li>
          <li class="<?php if (isset($active) && ($active == 'canceledorders' || $active == 'completedOrders' || $active == 'orders_list' || $active == 'player_pro_orders_list')) echo 'active' ;?> treeview">
            <a>
              <img  width="25" height ="22" src="<?php echo base_url().'assets/backend/'; ?>images/orders.png" />&nbsp; <span>Pro Esports Orders</span> <i class="fa fa-angle-right pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li class="<?php if(isset($active) && ($active == 'orders_list')) echo 'active' ;?> treeview">
                <a href="<?php echo base_url().'admin/orders'; ?>">
                  <img  width="22" height ="22" src="<?php echo base_url().'assets/backend/'; ?>images/truck.png" />&nbsp; <span> Pro New Orders</span> 
                </a>              
              </li>
              <li class="<?php if(isset($active) && ($active == 'player_pro_orders_list')) echo 'active' ;?> treeview">
                <a href="<?php echo base_url().'admin/orders/player_pro_orders_list'; ?>">
                  <img  width="22" height ="22" src="<?php echo base_url().'assets/backend/'; ?>images/truck.png" />&nbsp; <span> Player Pro Orders</span> 
                </a>              
              </li>
              <li class="<?php if(isset($active) && ($active == 'completedOrders')) echo 'active' ;?> treeview">
                <a href="<?php echo base_url().'admin/orders/completedOrders'; ?>"><img width="22" height ="22" src="<?php echo base_url().'assets/backend/'; ?>images/truck_smily.png" />&nbsp; <span> Completed Orders</span> </a>
              </li>
              <li class="<?php if(isset($active) && ($active == 'canceledorders')) echo 'active' ;?> treeview">
                <a href="<?php echo base_url().'admin/orders/canceledorders'; ?>"><img width="22" height ="22" src="<?php echo base_url().'assets/backend/'; ?>images/truck_sad.png" />&nbsp; <span> Canceled Orders</span> </a>
              </li>
            </ul>
          </li>

          <li class="<?php if (isset($active) && ($active == 'player_canceledorders' || $active == 'player_completedOrders' || $active == 'player_orders_list')) echo 'active' ;?> treeview">
            <a>
              <img  width="25" height ="22" src="<?php echo base_url().'assets/backend/'; ?>images/orders.png" />&nbsp; <span>Players Orders</span> <i class="fa fa-angle-right pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li class="<?php if(isset($active) && ($active == 'player_orders_list')) echo 'active' ;?> treeview">
                <a href="<?php echo base_url().'admin/orders/player_orders_list'; ?>">
                  <img  width="22" height ="22" src="<?php echo base_url().'assets/backend/'; ?>images/truck.png" />&nbsp; <span> Player New Orders</span> 
                </a>              
              </li>
              <li class="<?php if(isset($active) && ($active == 'player_completedOrders')) echo 'active' ;?> treeview">
                <a href="<?php echo base_url().'admin/orders/player_completedOrders'; ?>"><img width="22" height ="22" src="<?php echo base_url().'assets/backend/'; ?>images/truck_smily.png" />&nbsp; <span> Player Completed Orders</span> </a>
              </li>
              <li class="<?php if(isset($active) && ($active == 'player_canceledorders')) echo 'active' ;?> treeview">
                <a href="<?php echo base_url().'admin/orders/player_canceledorders'; ?>"><img width="22" height ="22" src="<?php echo base_url().'assets/backend/'; ?>images/truck_sad.png" />&nbsp; <span> Player Canceled Orders</span> </a>
              </li>
            </ul>
          </li>

        <?php } ?>
          
      <?php if($_SESSION['admin_user_role_id'] == 1) { ?>
            <li class="<?php if(isset($active) && ($active=='send_email' || $active=='email_template' || $active == 'email_report')) echo 'active' ;?> treeview">
              <a>
                <i class="fa fa-envelope"></i> <span>Email Management</span> <i class="fa fa-angle-right pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php if(isset($active) && $active=='email_template') echo 'active' ;?>"><a href="<?php echo base_url().'admin/Sendmail/email_template'; ?>"><i class="fa fa-file-code-o"></i>Email Template</a></li>
                <li class="<?php if(isset($active) && $active=='send_email') echo 'active' ;?>"><a href="<?php echo base_url().'admin/Sendmail/sendBulkEmail'; ?>"><i class="fa fa-cloud-upload"></i>Send Email</a></li>
               
                <li class="<?php if(isset($active) && $active=='email_report') echo 'active' ;?>"><a href="<?php echo base_url().'admin/Sendmail/'; ?>"><i class="fa fa-list-alt"></i>Email Report</a></li>
              </ul>
            </li>
      <?php } ?>
      <?php if ($_SESSION['admin_user_role_id'] == 1) { ?>
        <li class="<?php if(isset($active) && ($active=='menu' || $active=='menu_list' || $active=='menu_edit')) echo 'active' ;?> treeview">
          <a href="<?php echo base_url().'admin/menu'; ?>"><i class="fa fa-list"></i>Menu Management</a></li>
        </li>
        <li class="<?php if(isset($active) && ($active=='ads_list')) echo 'active' ;?> treeview">
          <a href="<?php echo base_url().'admin/ads/ads_list'; ?>"><i class="fa fa-audio-description"></i> <span>Ads Management</span></a>
        </li>
      <?php } ?>
      <?php if($_SESSION['admin_user_role_id'] == 1) { ?>
        <li class="<?php if(isset($active) && ($active=='work_list' || $active=='work_edit' || $active=='content_list' || $active=='content_edit' || $active=='heading_list' || $active=='heading_edit' || $active=='privacy_list' || $active=='privacy_edit' || $active=='support_list' || $active=='support_edit' || $active=='terms_list' || $active=='terms_edit' || $active=='logo_list' || $active=='logo_edit' || $active=='link_list' || $active=='link_edit' || $active=='screenshot' || $active=='admin_map'|| $active=='cover_password' || $active=='screenshot_list' || $active=='customer_service_page' || $active=='screenshot_edit' || $active=='site_backup' || $active == 'default_settings' || $active == 'dst_option' ||  $active == 'usr_security' || $active =='social_service')) echo 'active' ;?> treeview">
          <a><i class="fa fa-cog fa-fw"></i> <span>Site Settings</span> <i class="fa fa-angle-right pull-right"></i></a>
          <ul class="treeview-menu">
            <li class="<?php if(isset($active) && ($active=='default_settings')) echo 'active' ;?>"><a href="<?php echo base_url().'admin/Default_Settings'; ?>"><i class="fa fa-gear"></i>Default Settings</a></li>

            <li class="<?php if(isset($active) && ($active=='dst_option')) echo 'active' ;?>"><a href="<?php echo base_url().'admin/daylight_saving'; ?>"><i class="fa fa-clock-o"></i>DST settings</a></li>

            <li class="<?php if(isset($active) && ($active=='heading_list' || $active=='heading_edit')) echo 'active' ;?>"><a href="<?php echo base_url().'admin/heading'; ?>"><i class="fa fa-header"></i>Heading Management</a></li>
            <!-- <li class="<?php if(isset($active) && ($active=='screenshot' || $active=='screenshot_list' || $active=='screenshot_edit')) echo 'active' ;?> treeview">
              <a href="<?php echo base_url().'admin/Screenshot'; ?>"><i class="fa fa-desktop"></i> <span>Screenshot</span> </a>
            </li> -->
            <li class="<?php if(isset($active) && ($active=='work_list' || $active=='work_edit')) echo 'active' ;?>"><a href="<?php echo base_url().'admin/prize'; ?>"><i class="fa fa-arrow-right"></i>Flow Of Works</a></li>
            <li class="<?php if(isset($active) && ($active=='content_list' || $active=='content_edit')) echo 'active' ;?>"><a href="<?php echo base_url().'admin/Content'; ?>"><i class="fa fa-file-text"></i>Footer Content</a></li>
            <li class="<?php if(isset($active) && ($active=='logo_list' || $active=='logo_edit')) echo 'active' ;?>"><a href="<?php echo base_url().'admin/logo'; ?>"><i class="fa fa-picture-o"></i>Footer Logo</a></li>
            <li class="<?php if(isset($active) && ($active=='link_list' || $active=='link_edit')) echo 'active' ;?>"><a href="<?php echo base_url().'admin/link'; ?>"><i class="fa fa-external-link"></i>Footer Social Link</a></li>
            <li class="<?php if(isset($active) && $active=='customer_service_page') echo 'active';?>"><a href="<?php echo base_url().'admin/customer_service'; ?>"><i class="fa fa-chain-broken"></i>Footer Customer Service</a></li>
            <li class="<?php if(isset($active) && $active=='admin_map') echo 'active' ;?>"><a href="<?php echo base_url().'admin/Content/admin_map_video'; ?>"><i class="fa fa-globe"></i>Admin Map</a></li>
            <li class="<?php if(isset($active) && $active=='cover_password') echo 'active' ;?>"><a href="<?php echo base_url().'admin/Content/cover_password'; ?>"><i class="fa fa-lock"></i>Cover Page</a></li>
            <li class="<?php if(isset($active) && $active=='site_backup') echo 'active' ;?>"><a href="<?php echo base_url().'admin/Backup/'; ?>"><i class="fa fa-hdd-o"></i>Site Backup</a></li>
            <li class="<?php if(isset($active) && $active=='social_service') echo 'active';?>"><a href="<?php echo base_url().'admin/Social_service'; ?>"><i class="fa fa-facebook"></i>Social Service</a></li>
            <?php if($this->session->userdata('admin_user_id') == 500000) {  ?>
              <li class="<?php if(isset($active) && ($active=='usr_security' )) echo 'active' ;?>"><a href="<?php echo base_url().'admin/FrontUser/security'; ?>"><i class="fa fa-lock"></i>Security Password</a></li>
            <?php } ?>
          </ul>
        </li>
      <?php } ?>
      <li class="<?php if(isset($active) && ($active=='customer_service_agent_list' || $active=='pvp_chat' || $active=='chat_ads')) echo 'active'; ?> treeview">
        <a><img src="<?php echo site_url();?>assets/backend/images/customer_services.png" class="customer_service_icon" height="13px" width="14px"> <span>&nbsp;&nbsp;&nbsp;Customer Service</span> <i class="fa fa-angle-right pull-right"></i></a>
        <ul class="treeview-menu">
          <li class="<?php if(isset($active) && ($active=='customer_service_agent_list')) echo 'active' ;?>"><a href="<?php echo site_url();?>admin/chat"><i class="fa fa-user"></i> Customer services chat</a></li>
          <li class="<?php if(isset($active) && ($active=='pvp_chat')) echo 'active' ;?>"><a href="<?php echo site_url();?>admin/pvpchat"><i class="fa fa-group"></i> PVP Chat</a></li>
          <li class="<?php if(isset($active) && ($active=='chat_ads')) echo 'active' ;?>"><a href="<?php echo site_url();?>admin/chatads"><i class="fa fa-audio-description"></i> Chat Ads</a></li>
        </ul>
      </li>

			<!--

			<li class="<?php if(isset($active) && ($active=='banner' || $active=='banner_list' || $active=='banner_edit' )) echo 'active' ;?> treeview">

				<a >

					<i class="fa fa-user"></i> <span>Admin User</span> <i class="fa fa-angle-right pull-right"></i>

				</a>

				<ul class="treeview-menu">

					<li class="<?php if(isset($active) && ($active=='banner' || $active=='banner_list' || $active=='banner_edit')) echo 'active' ;?>"><a href="<?php echo base_url().'admin/banner'; ?>"><i class="fa fa-flag-o"></i>Admin</a></li>           

				</ul>

            </li> -->

			

          </ul>

        </section>

        <!-- /.sidebar -->

      </aside>

