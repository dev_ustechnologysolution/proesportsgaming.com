<section class="content">
<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Contacts list</h3>
                  <a href="<?php echo base_url().'admin/contact'; ?>"><button class="btn pull-right btn-primary btn-xl">Add</button></a>
                </div><!-- /.box-header -->
                <?php $this->load->view('admin/common/show_message') ?>
                <div class="box-body">
                  <table id="contact_list" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($list as $value){?>
                      <tr>
                        <td><?php echo $value['name']?></td>
                        <td><?php echo $value['email']?></td>
                        <td><?php echo $value['phone']?></td>
                        <td><a href="<?php echo base_url().'admin/contact/contact_edit/'.$value['id'];?>">Edit</a>|<a href="<?php echo base_url().'admin/contact/delete/'.$value['id'];?>" onclick="return confirm('are you sure delete')">Delete</a></td>
                       </tr>
                      <?php }?>
                      
                      </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div>
 </section>