<div class="col-sm-12 cover_page_top_div">
  <div class="pull-left status_button">
    <label class="switch"><input type="checkbox" name="cover_page_status" value="cover_page_status" <?php if($list['status'] == '1') { echo 'checked'; } ?> class="cover_page_status"><span class="slider round"></span></label>
  </div>
  <input type="hidden" class="enable_id" value="<?php echo $list['status']; ?>">
  <div class="pull-right">
    <a class="btn btn-info" onclick="location.reload()"> Refresh </a>
  </div>
</div>
<div class="clearfix"></div>
<section class="content">
  <?php $this->load->view('admin/common/show_message') ?>
  <div class="box box-primary custom_map_video" <?php if($list['status'] == '0') { echo 'style="display: none;"'; } ?>>
    <table id="" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th class="admin_map_th">Cover Password</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><?php echo $list['password']; ?></td>
          <td><a id="<?php echo $list['id'];?>" class="edit_pw"><i class="fa fa-pencil fa-fw"></i></a></td>
        </tr>
      </tbody>
    </table>
  </div>
</section>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script> $.validate();</script>
<!-- Modal 1 (Basic)-->
<div class="modal fade  custom-width" id="modal-pw">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">
      
    </div>
  </div>
</div>
<script>
$(document).on('click','.edit_pw',function(e){ 
  e.preventDefault();
  var id = $(this).attr('id');
  if(id){
    $.ajax({
      url : '<?php echo site_url(); ?>admin/content/cover_password_edit/'+id,
      success: function(result) {
        if(result){
          $('.modal-content').html(result);
          $('#modal-pw').modal('show');                              
        }
      }
    })
  }
})

$(document).on('submit','#cover_pw_id',function(e){
  e.preventDefault();
  var data = $(this).serialize();
  // var action_value = $('#action_value').val();
  if(data){
    $.ajax({
      url: "<?php echo site_url(); ?>admin/content/cover_password_change",
      data: data,
     type: 'POST',
     success: function(result) {
        $('.modal-content').html(result);
        $('#modal-1').modal('show');        
     }
    });
  }             
})

$(document).on('submit','#editpw_id',function(e){
  e.preventDefault();
  var data = $(this).serialize();
  if(data){
    $.ajax({
      url: "<?php echo base_url();?>admin/content/update_cover_pw",
      data: data,
      type: 'POST',
      success: function(result) {
        window.location.reload();        
      }
    });
  }
});
$('.cover_page_top_div .switch input.cover_page_status').click(function() {
  var enable_id = $('.enable_id').val();
  if (this.checked) {
    $('.custom_map_video').show();
    $.ajax({
      url: base_url + '/admin/Content/changeCoverStatus/enable',
      beforeSend:function() { admin_loader_show(); },
      success: function(result) { admin_loader_hide(); }
    });
  } else {
    $('.custom_map_video').hide();
    $.ajax({
      url: base_url + '/admin/Content/changeCoverStatus/disable',
      beforeSend:function() { admin_loader_show(); },
      success: function(result) { admin_loader_hide(); }
    });
  }
});
</script>