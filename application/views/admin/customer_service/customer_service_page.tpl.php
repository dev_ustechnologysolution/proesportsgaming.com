<section class="content">

  <div class="box">

    <?php $this->load->view('admin/common/show_message') ?>

    <div class="box-body">

      <table id="customer_service" class="table table-bordered table-striped">

        <thead>

          <tr>

            <th>Sl No</th>

            <th>Column Details</th>

            <th>Status</th>

            <th>Action</th>

          </tr>

        </thead>

        <tbody>

          <?php $i=1; foreach($list as $value){?>

          <tr>

            <td><?php echo $i++; ?></td>

            <td><?php echo $value['column_details']; ?></td>

            <td id="cs_status_td_<?php echo $value['id']; ?>" >

              <?php 
              if($value['status']=='active')
              { 
              ?>
                <span class="label label-success">Active</span>
              <?php
              }
              else
              {
              ?>  
                <span class="label label-danger">Deactive</span>
              <?php
              }
              ?>

            </td>

            <td id="cs_action_td_<?php echo $value['id']; ?>" >
              <?php 
              if($value['status']=='active')
              { 
              ?>  
                <button class="btn btn-block btn-danger btn-xs deactiveCsStatus" id="<?php echo $value['id']; ?>">Deactive</button>
              <?php
              }
              else 
              {
              ?>
                <button class="btn btn-block btn-success btn-xs activeCsStatus" id="<?php echo $value['id']; ?>">Active</button>
              <?php
              }
              ?>
            </td>

           </tr>

          <?php }?>

        </tbody>

      </table>

    </div>

  </div>

</section>