<section class="content">

  <div class="row">

    <div class="col-md-12">

			<?php $this->load->view('admin/common/show_message'); ?>

        <div class="box box-primary">

          <form id="f3"  class="form" action="" method="post">

            <div class="box-body">

              <div class="form-group row required">

                  <label class ="col-sm-1" for="title">Title</label>
                  <div class="col-sm-6">
	                  <input required type="text" class="form-control" name="title" value="">
	              </div>
              </div>

              <div class="form-group row required">

                  <label class ="col-sm-1" for="description">Subject</label>
                   <div class="col-sm-6">
                  		<input required type="text" class="form-control" name="subject" value="">
                  	</div>
              </div>
             
              <div class="form-group row required" id="content">

                  <label class ="col-sm-1" for="editor1">Content</label>
                   <div class="col-sm-6">
                  		<textarea required style="visibility: hidden; display: none;" id="editor1" name="editor1" class="ckeditor" rows="30" cols="80"></textarea>
                  	</div>
              </div>

            </div>

            <div class="box-footer col-md-8" align="center">

              <button class="btn btn-primary" type="submit">ADD</button>

            </div>

          </form>

        </div>

      </div>

    </div>

</section>
<style>
.form-group.required .control-label:after {
  content:"*";
  color:red;
}
</style>

<script>  
$("form").submit( function(e) {
    var messageLength = CKEDITOR.instances['editor1'].getData().replace(/<[^>]*>/gi, '').length;      
    if(!messageLength ) {
          alert( 'Please enter a Content' );  
          e.preventDefault();          
    } else {
       e.preventDefault();
        var body = CKEDITOR.instances.editor1.getData(); 
       var formData = {
          'title'              : $('input[name=title]').val(),
          'subject'             : $('input[name=subject]').val(),
          'editor1'    : body
      };
        $.ajax({
          url : '<?php echo site_url(); ?>admin/Sendmail/createEmailTemplate',
          type        : 'POST',
          data        : formData,
          success: function(data) {
            console.log(data);
            if(data == 'true'){
              window.location.href = "<?php echo base_url();?>admin/Sendmail/email_template"; 
            
            }
            
          }
       })           
    }
 });
</script>
