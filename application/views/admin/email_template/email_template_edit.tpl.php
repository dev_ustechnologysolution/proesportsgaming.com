<section class="content">

  <div class="row">

    <div class="col-md-12">

			<?php $this->load->view('admin/common/show_message'); ?>

        <div class="box box-primary">

          <form id="f3" action="" method="post">

            <div class="box-body">

              <div class="form-group row required">

                  <label class ="col-sm-1" for="title">Title</label>

                  <div class="col-sm-6">
                    <input  required type="text" class="form-control" name="title" value="<?php if(isset($email_template_data[0]['title'])) echo $email_template_data[0]['title'];?>">
                  </div>
              </div>

              <div class="form-group row  required">

                  <label class ="col-sm-1"  for="subject">Subject</label>

                  <div class="col-sm-6">
                    <input required type="text" class="form-control" name="subject" value="<?php if(isset($email_template_data[0]['subject'])) echo $email_template_data[0]['subject'];?>">
                  </div>
              </div>
             
              <div class="form-group row required" id="content">

                  <label class ="col-sm-1" for="editor1">Content</label>

                  <div class="col-sm-6">
                    <textarea required id="editor1" name="editor1" class="ckeditor" rows="10" cols="80"><?php if(isset($email_template_data[0]['body'])) echo $email_template_data[0]['body'];?>                    
                    </textarea>
                  </div>
              </div>

            </div>

            <div class="box-footer" align="center">

              <input type="hidden" name="id" id="template_id" value="<?php if(isset($email_template_data[0]['id'])) echo $email_template_data[0]['id'];?>">
              <div class="col-md-7">
              <button class="btn btn-primary" type="submit">Update</button>
            </div>
            </div>

          </form>

        </div>

      </div>

    </div>

</section>
<script>  
$("form").submit( function(e) {
    var messageLength = CKEDITOR.instances['editor1'].getData().replace(/<[^>]*>/gi, '').length;      
    if(!messageLength ) {
          alert( 'Please enter a Content' );  
          e.preventDefault();          
    } else {
      var body = CKEDITOR.instances.editor1.getData();
       e.preventDefault(); 
       var formData = {
          'title'              : $('input[name=title]').val(),
          'subject'             : $('input[name=subject]').val(),
          'editor1'    : body,
          'id':$('input[name=id]').val()
      };
        $.ajax({
          url : '<?php echo site_url(); ?>admin/Sendmail/emailTemplateUpdate',
          type        : 'POST',
          data        : formData,
          success: function(data) {
            console.log(data);
            if(data == 'true'){
              location.reload();             
            }
            
          }
       })           
    }
 });
</script>