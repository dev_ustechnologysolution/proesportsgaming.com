<section class="content">

  <div class="box">

    <div class="box-header">
  
      <a href="<?php echo base_url().'admin/Sendmail/addEmailTemplate'; ?>"><button class="btn pull-right btn-primary btn-xl">Add Email Template</button></a>
    </div>

    <?php $this->load->view('admin/common/show_message') ?>

    <div class="box-body">

      <table id="example1" class="table table-bordered table-striped">

        <thead>

          <tr>

            <th>Sl No</th>

            <th>Title</th>

            <th>Subject</th>

            <th>Action</th>

          </tr>

        </thead>

        <tbody>

          <?php $i=1; foreach($list as $value){ ?>

          <tr>

            <td><?php echo $i++; ?></td>

            <td><?php echo $value['title']?></td>
            
            <td><?php echo $value['subject']?></td>
            
            <td>

             
              <a title="Edit Template" href="<?php echo base_url().'admin/Sendmail/emailTemplateEdit/'.$value['id'];?>"> <i class="fa fa-pencil"></i></a>              
              <a title="Remove Template" class="delete" id=" <?php echo $value['id']; ?>"><i class="fa fa-trash-o"></i></a>

            </td>

           </tr>

          <?php } ?>

        </tbody>

      </table>

    </div>

  </div>

</section>
<div class="modal fade  custom-width" id="modal-1">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">
      
    </div>
  </div>
</div>
<script>
$(document).on('click','.delete',function(e){ 
  e.preventDefault();
  var delete_id = $(this).attr('id');
  $.ajax({
      url : '<?php echo site_url(); ?>admin/Sendmail/delete_data',
      data: {id:delete_id},
      type: 'POST',
      success: function(result) {
        if(result){
          $('.modal-content').html(result);
          $('#modal-1').modal('show');                              
        }
      }
    })
})

$(document).on('submit','#security_password_id',function(e){
  e.preventDefault();
  var data = $(this).serialize();
  if(data){
    $.ajax({
      url: "<?php echo site_url(); ?>admin/Sendmail/delete",
      data: data,
      type: 'POST',
      success: function(result) {
        console.log(result);
        if(result == true){
          window.location.href = "<?php echo site_url(); ?>admin/Sendmail/email_template";
        } else {
          $('#security_password').val('');
          alert('Password Error');
          return false;
        }
              
      }
    });
  }             
})
</script>