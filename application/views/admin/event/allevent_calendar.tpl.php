<link href='<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar.css' rel='stylesheet' />
<script src='<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar.min.js'></script>
<section class="body-middle innerpage">
  <?php $this->load->view('admin/common/show_message'); ?>
	<div class="row">
		<div class="col-sm-12">
				<div class="clearfix text-center">
          <a class="btn btn-primary pull-right edit-data" action="exp_plyr_history" winre_loc="<?php echo base_url().'admin/Banner/export_tournament_history/'; ?>" style="margin: 0 auto 10px 10px;"><i class="fa fa-print"></i> Export all History</a>
          <a class="btn btn-primary pull-right" href="<?php echo base_url().'admin/Banner/globalHistory?alldata=yes';?>" style="margin: 0 auto 10px;"><i class="fa fa-history"></i> Global History</a>
        </div>
	    	<div class="box box-primary">
	    		<div class="box-body no-padding admins-calendar admin_eventcalendar">
	    			<div id="calendar"></div>
	        	</div>
	    	</div>
	    </div>
	</div>	
</section>

<div class="modal custom-width" id="calendar_mdl" style="display: none; padding-right: 17px; overflow: auto !important">
  <div class="modal-dialog" style="width: 70%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiEvent Detailss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;"></h4>
      </div>
      <div class="modal-body"></div>
    </div>
  </div>
</div>
<link href="<?php echo base_url(); ?>assets/plugins/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<script src="<?php echo base_url().'assets/plugins/datepicker/js/moment-with-locales.min.js'; ?>"></script>
<script type="text/javascript">
  var date = new Date();
  var d = date.getDate(), m = date.getMonth(), y = date.getFullYear();
  var events_arr = <?php echo $events; ?>;
  $('#calendar').fullCalendar({
    header    : {
      left  : 'prev,next today',
      center: 'title',
      right : 'month,agendaWeek,agendaDay'
    },
    buttonText: {
      today: 'today',
      month: 'month',
      week : 'week',
      day  : 'day'
    },
    selectable: false,
    events: events_arr,
    editable: false,
    eventLimit: true,
    // eventConstraint: {
    //   start: moment().format('YYYY-MM-DD HH:mm:ss'),
    //   end: '2200-01-01'
    // },
    eventClick: function(event) {
      event.action = 'edit_event';
      event_clickfun(event);
      return false;
    },

    eventRender: function(event, element, view) {
      if (event.color != '') {
        element.css({'background':event.css_backgcolor});
        // ,'z-index':9999
        element.find('.fc-title, .fc-content').css({'z-index':9});
      }
      (event.is_archive == 1) ? element.css('color','black') : ''; 
    },
    eventMouseover: function(event, element, view) {
      if (event.is_proplayre != '') {
        $("body").append('<div class="tooltipevent tooltip_eve_calendar"><div class="eve_cal_title">' + event.is_proplayre + '</div>');
        var bg_color = (event.bg_color != '' && event.bg_color != null) ? ((event.bg_color.indexOf('_') != -1) ? 'linear-gradient(200deg, '+event.bg_color.split('_')[0]+', '+event.bg_color.split('_')[1]+') 0% 0% / 200% 200%' : event.bg_color) : '#ff5000';
        $('.tooltipevent').css({"z-index": 10000, "background": bg_color, "color": "#fff", "border": "1px solid #fff" });
        $(this).mousemove(function(e) { $('.tooltipevent').css('top', e.pageY + 10).css('left', e.pageX + 20); });
      }
    },
    eventMouseout: function(calEvent, jsEvent) {
      $(this).css('z-index', 8);
      $('.tooltipevent').remove();
      // $('.key_shw_div').html('').hide();
    },
  });

  function event_clickfun(event) {
    if (event.event_type != undefined && event.event_type == 'lobby_event' && event.id != undefined) {
      var postData = { 'lobby_id' : event.id, 'evetitle' : event.title };
      $.ajax({
        url : base_url + 'admin/banner/getall_registered_player',
        data: postData,
        type: 'POST',
        beforeSend: function() { admin_loader_show(); },
        success: function(res) {
          admin_loader_hide();
          var res = $.parseJSON(res);
          calendar_mdl_show(res);
        }
      })
    }
  }

  function calendar_mdl_show(args) {
    var evetitle = '<div class="box-body"><h2 class="text-center">'+args.evetitle+' Tournament Players</h2></div>';
    var senddata = "<div class='row'>";
    senddata += '<div class="box-body">';
    senddata += '<div class="col-sm-6"><a href="javascript:void(0);" class="mt20 btn btn-primary reset_users_btn"> <span class="glyphicon glyphicon-refresh"></span> Reset Selected Users</a><a href="javascript:void(0);" class="mt20 btn btn-primary refund_users_btn" style="margin-left: 5px;"> <span class="glyphicon glyphicon-usd"></span> Refund Selected Users</a></div><div class="col-sm-6 text-right" style="padding-bottom: 10px;"><a class="btn btn-primary" href="'+base_url+'admin/Banner/tournament_history/'+args.lobby_id+'">Export History</a></div><span style="color: red" id="reset_users_error"></span>';
    senddata += '<table class="table table-bordered table-striped table-responsive" id="example_1"><thead><tr>';
    senddata += '<th>Reset/Refund <br> Select All &nbsp;&nbsp; <input type="checkbox" name="select_all" class="select_all" ></th>';
    senddata += '<th>Date time</th>';
    senddata += '<th>Acc#</th>';
    senddata += '<th>Name</th>';
    senddata += '<th>Money Deducted</th>';
    senddata += '<th>Ticket Type</th>';
    senddata += '</tr></thead>';
    senddata += '<tbody';
    if (args.players_data != undefined && args.players_data.length > 0) {
      for (var i = args.players_data.length - 1; i >= 0; i--) {
        senddata += '<tr>';
        senddata += '<td><input type="checkbox" name="reset_users[]" value="'+args.players_data[i].user_id+'"></td>'
        senddata += '<td>'+args.players_data[i].created+'</td>';
        senddata += '<td>'+args.players_data[i].account_no+'</td>';
        senddata += '<td>'+ ((args.players_data[i].display_name_status == '1') ? args.players_data[i].display_name : args.players_data[i].name) +'</td>';
        senddata += '<td>'+parseFloat(args.players_data[i].event_price - args.players_data[i].event_fee).toFixed(2)+'</td>';
        senddata += '<td>'+ ((args.players_data[i].type == '1') ? 'Event' : 'spectator')+'</td>';
        senddata += '</tr>';
      }
    }
    senddata += '</tbody></table><form role="form" action="'+base_url+'admin/banner/reset_selected_users?lobby_id='+args.lobby_id+'" class="reset_users_form" method="POST"><input type="hidden" name="selected_users" id="selected_users"></form><form role="form" action="'+base_url+'admin/banner/refund_selected_users?lobby_id='+args.lobby_id+'" class="refund_users_form" method="POST"><input type="hidden" name="selected_users_rf" id="selected_users_rf"></form></div>';
    $('#calendar_mdl .modal-title').html(evetitle);
    $('#calendar_mdl .modal-body').html(senddata);
    $('#example_1').DataTable();
    $('#calendar_mdl').show();

    $('.reset_users_btn').on('click',function(){
            var selected_users = [];
            $('input[name="reset_users[]"]:checked').each(function(){
                selected_users.push($(this).val());
            });
            $('#selected_users').val(selected_users);
            if($('#selected_users').val() == null || $('#selected_users').val() == ''){
                $('#reset_users_error').html('Please select users from list to reset.');
            }else{
                $('.reset_users_form').submit();
            }            
        })

        $('.refund_users_btn').on('click',function(){
            var selected_users_rf = [];
            $('input[name="reset_users[]"]:checked').each(function(){
                selected_users_rf.push($(this).val());
            });
            $('#selected_users_rf').val(selected_users_rf);
            if($('#selected_users_rf').val() == null || $('#selected_users_rf').val() == ''){
                $('#reset_users_error').html('Please select users from list to refund.');
            }else{
                $('.refund_users_form').submit();
            }            
        });

        $('input[name="reset_users[]"]').on('change',function(){
            $('#reset_users_error').html('');
        })

        $('.select_all').on('change', function(){
            if($(this).is(":checked")){
                $('input[name="reset_users[]"]').prop('checked', true);
            }else{
                $('input[name="reset_users[]"]').prop('checked', false);
            }
        })
  }  
</script>