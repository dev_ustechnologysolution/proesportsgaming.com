<link href='<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar.css' rel='stylesheet' />
<script src='<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar.min.js'></script>
<section class="body-middle innerpage">
    <?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
	<div class="row">
		<div class="col-sm-12">
			<?php if (!empty(trim($lobby_name))) { ?>
				<div class="clearfix text-center">
					<h2><?php echo $lobby_name." Calendar"; ?></h2>
					<a class="btn btn-primary pull-right edit-data" id="13" winre_loc="<?php echo base_url().'admin/Banner/export_tournament_history/'.$lobby_id.'/'.$type; ?>" style="margin: 0 auto 10px;"><i class="fa fa-print"></i> Export all History</a>
				</div>
			<?php } ?>
	    	<div class="box box-primary">
	    		<div class="box-body no-padding admins-calendar admin_eventcalendar">
	    			<div id="calendar"></div>
	        	</div>
	    	</div>
	    </div>
	</div>	
</section>
<link href="<?php echo base_url(); ?>assets/plugins/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<script src="<?php echo base_url().'assets/plugins/datepicker/js/moment-with-locales.min.js'; ?>"></script>
<script type="text/javascript">
  var date = new Date();
  var d = date.getDate(), m = date.getMonth(), y = date.getFullYear();
  var events_arr = <?php echo $evelist; ?>;
  $('#calendar').fullCalendar({
    header    : {
      left  : 'prev,next today',
      center: 'title',
      right : 'month,agendaWeek,agendaDay'
    },
    buttonText: {
      today: 'today',
      month: 'month',
      week : 'week',
      day  : 'day'
    },
    selectable: true,
    events: events_arr,
    editable: true,
    eventLimit: true,
    // eventConstraint: {
    //   start: moment().format('YYYY-MM-DD HH:mm:ss'),
    //   end: '2200-01-01'
    // },
    eventClick: function(event) {
      event.action = 'edit_event';
      event_clickfun(event);
      return false;
    },
    eventDrop : function(event, delta, revertFunc) {
      updateevent(event, delta, revertFunc);
    },
    eventResize: function(event, delta, revertFunc) {
      updateevent(event, delta, revertFunc);
    },
    eventRender: function(event, element, view) {
      if (event.color != '') {
        element.css({'background':event.css_backgcolor});
        // ,'z-index':9999
        element.find('.fc-title, .fc-content').css({'z-index':9});
      };
      document.oncontextmenu = function() {return false;};
      $(element).mousedown(function(e){ 
        if( e.button == 2 ) { 
          var contextmenu = '<div class="dropdown clearfix contextMenu" style="display: block; width: auto; position: absolute;"><ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu" style="display:block;"><li><a class="clone_event">Copy Event</a></li>';
          // contextmenu += '<li><a href="" class="delete_event">Delete Event</a></li>';
          contextmenu += '</ul></div>';

        //    $('#calendar_mdl form').attr({'action':base_url+'admin/events/edit_events/?cust_event_id='+event_id,'event_id':event_id}).find('.event_url').show().closest('form').find('.delete_eve').attr('href',base_url+'admin/events/delete_events/'+event_id).show();
        // (args.event_is_expired == 'yes') ? $('#calendar_mdl form .box-footer').find('input, .delete_eve').hide().siblings('.clone_event').show() : $('#calendar_mdl form .box-footer').find('input, .delete_eve').show().siblings('.clone_event').hide();
        

          $('.contextMenu').remove();
          $(contextmenu).insertAfter($(this));
          $(".contextMenu").hide().css({display: "block",width: 'auto',position: 'absolute'}).find('ul').css({'background-color':'#000','padding': '0','min-width': 'auto'}).find('a').css({'color':'#fff'});
          $(".contextMenu a").mouseover(function () { $(this).css('color','#000'); }).mouseout(function () { $(this).css('color','#fff'); });
          $(".contextMenu .clone_event").click(function () {
            event.action = 'add_event';
            event_clickfun(event);
          });
        } 
        return true; 
      }); 
      $('html').click(function() {
        $('.contextMenu').remove();
      });
      $("form .clone_event").click(function () {
        var event_id = $(this).closest('.cal_event_form').attr('event_id');
        $(event).map(function() {
          if (this.id == event_id) {
            event.action = 'add_event';
            event_clickfun(this);
          }
        });
      });
    },
    eventMouseover: function(event, element, view) {
      if (event.is_proplayre != '') {
        $("body").append('<div class="tooltipevent tooltip_eve_calendar"><div class="ticket_info"><div class="profile_img"><img src="'+event.profile_img+'" class="img img-responsive" alt="smile"></div><div class="acc_info"><label>Acc No# '+event.acc_no+' </label></div></div>');
        var bg_color = (event.bg_color != '' && event.bg_color != null) ? ((event.bg_color.indexOf('_') != -1) ? 'linear-gradient(200deg, '+event.bg_color.split('_')[0]+', '+event.bg_color.split('_')[1]+') 0% 0% / 200% 200%' : event.bg_color) : '#ff5000';
        $('.tooltipevent').css({"z-index": 10000, "background": bg_color, "color": "#fff", "border": "1px solid #ff5000", "border-radious": "4px" });
        $(this).mousemove(function(e) { $('.tooltipevent').css('top', e.pageY + 10).css('left', e.pageX + 20); });
      }
    },
    eventMouseout: function(calEvent, jsEvent) {
      $(this).css('z-index', 8);
      $('.tooltipevent').remove();
      // $('.key_shw_div').html('').hide();
    },
  });

</script>