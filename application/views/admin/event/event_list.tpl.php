<link href="<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css" />
<script src="<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar.min.js"></script>
<!-- <script src="<?php //echo base_url(); ?>assets/plugins/jQueryUI/jquery-ui.min.js"></script> -->
<!-- <script src="<?php //echo base_url(); ?>assets/plugins/colorpicker/bootstrap.bundle.min.js"></script> -->
<script src="<?php echo base_url(); ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<section>
  <?php $this->load->view('admin/common/show_message'); ?>
  <div class="box-header">
    <a class="btn btn-primary add_default_key" action="add_default_key" keyopt="fr_proplayer_assign" style="float:right;margin-right: 7px;">Pro-Players Default Key</a>
    <a class="btn btn-primary add_default_key" action="add_default_key" keyopt="fr_admin_assign" style="float:right;margin-right: 7px;">Admin Default Key</a>
    <a class="btn btn-primary add_events_key" action="add_key" style="float:right;margin-right: 7px;">Key</a>
    <a class="btn btn-primary add_event_incalendar" action="add_event" style="float:right;margin-right: 7px;">Add Event</a>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-body no-padding admins-calendar admin_eventcalendar">
          <div id="calendar"></div>
        </div>
      </div>
      <div class="box box-primary">
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Key Order</th>
                <th>Key Title</th>
                <th>Key Image</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if (!empty($keylist)) {
                foreach($keylist as $kl) { ?>
                  <tr>
                    <td><label><?php echo $kl->order; ?><a class="event_key_order_change" id="<?php echo $kl->id; ?>" event_order="<?php echo $kl->order; ?>" style="margin-left: 5px;"><i class="fa fa-pencil fa-fw"></i></a></label></td>
                    <td><?php echo $kl->key_title; ?></td>
                    <td>
                      <div><img src="<?php echo base_url().'upload/event_key_img/'.$kl->key_image; ?>" alt="<?php echo $kl->key_image; ?>" height="40px" width="40px"></div>
                    </td>
                    <td>
                      <a title="Edit Key" class="edit_events_key" key_title="<?php echo $kl->key_title; ?>" key_image="<?php echo $kl->key_image; ?>" edit_id="<?php echo $kl->id; ?>"> <i class="fa fa-pencil"></i></a> | <a title="Remove Key" href="<?php echo base_url().'admin/events/delete_key/'.$kl->id; ?>"><i class="fa fa-trash-o"></i></a>
                    </td>
                  </tr>
                <?php }
              } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal custom-width" id="order_events_key" style="padding-right: 17px; overflow: auto !important">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title keyorder" style="color:#111; font-size:19px; font-weight:bold;"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <form action="<?php echo base_url(); ?>admin/events/change_key_order" id="change_lobby_order" name="change_lobby_order" method="POST">
            <div class="box-body">
              <div class="form-group">
                <label>Change Key Order </label>
                <input type="hidden"class="form-control" name="key_id" value="">
                <input type="hidden"class="form-control" name="old_key_order" value="">
                <input type="number" id="new_order" class="form-control" name="change_key_order" value="" required="">
              </div>
            </div>
            <div class="box-footer">
              <input class="btn btn-primary" type="submit" value="Submit">
              <button type="button" class="btn btn-info pull-right" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal custom-width" id="addevents_key" style="display: none; padding-right: 17px; overflow: auto !important">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Event Key</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <form method="POST" style="padding: 0 7px;" action="" class="cal_event_key_form" enctype="multipart/form-data">
            <div class="box-body">
              <div class="form-group">
                <label for="eve_key_title">Key title </label>
                <input type="text" name="eve_key_title" id="eve_key_title" class="form-control" value=""  placeholder="Enter Key Title" required>
              </div>
              <div class="form-group">
                <label for="eve_key_image">Key image </label>
                <div class="oldimg" style="display: none;">
                  <img src="" alt="Img" width="100px" height="auto" style="margin: 0px 0 10px 0px;">
                </div>
                <div>
                  <input type="file" name="eve_key_image" id="eve_key_image" class="form-control" value=""  placeholder="Choose Key Image" accept="image/*">
                </div>
              </div>
            </div>
            <div class="box-footer">
              <input class="btn btn-primary" type="submit" value="Submit">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal custom-width" id="add_default_key" style="display: none; padding-right: 17px; overflow: auto !important">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Default Event Key</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <form method="POST" style="padding: 0 7px;" action="" class="cal_default_event_key_form" enctype="multipart/form-data">
            <div class="box-body">
              <!-- for assign to Admin -->
              <div class="fr_admin_assign" style="display: none;">
                <div class="form-group">
                  <label for="default_eve_key_title">Assign Key</label>
                  <select class="form-control select_key">
                    <option value="">Choose Key</option>
                    <?php if (!empty($keylist)) {
                      foreach ($keylist as $key => $kl) {
                         $select1 = ($getdefault_keylist[0]->key_id == $kl->id) ? 'selected' : '';
                        ?>
                        <option value='<?php echo $kl->id; ?>' <?php echo $select1; ?>><?php echo $kl->key_title; ?></option>
                      <?php }
                    } ?>
                  </select>
                </div>
              </div>
              <!-- for assign pro player assign -->
              <div class="fr_proplayer_assign" style="display: none;">
                <table id="example2" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Acc#</th>
                      <th>Pro Player name</th>
                      <th>Assign key</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if (!empty($proplayers)) {
                      foreach ($proplayers as $key => $ppl) { ?>
                        <tr>
                          <td><?php echo $ppl['account_no']; ?></td>
                          <td><?php echo (!empty($ppl['display_name'])) ? $ppl['display_name'] : $ppl['name']; ?></td>
                          <td>
                            <select class="form-control select_key" proplayer_user_id="<?php echo $ppl['user_id']; ?>" style="display: block; width: 100%;">
                              <option value="">Choose Key</option>
                              <?php if (!empty($keylist)) {
                                foreach ($keylist as $key => $kl) {
                                  $select = ($getdefault_keylist[$ppl['user_id']]->key_id == $kl->id) ? 'selected' : '';
                                  ?>
                                  <option value='<?php echo $ppl['user_id']; ?>_<?php echo $kl->id; ?>' <?php echo $select; ?>><?php echo $kl->key_title; ?></option>
                                <?php }
                              } ?>
                            </select>                          
                          </td>
                        </tr>
                      <?php }
                    } ?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="box-footer">
              <input class="btn btn-primary" type="submit" value="Submit">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal custom-width" id="calendar_mdl" style="display: none; padding-right: 17px; overflow: auto !important">
  <div class="modal-dialog" style="width: 70%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiEvent Detailss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Event Detail <span class="lbytype"></span></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <form method="POST" style="padding: 0 7px;" class="cal_event_form" enctype="multipart/form-data">
            <div class="box-body">
              <div class="form-group">
                <label for="eve_title">Title</label>
                <input type="text" id="eve_title" class="form-control" name="eve_title" value="" data-validation="required" required="" placeholder="Enter Title">
                <input type="hidden" name="id">
                <input type="hidden" name="event_type">
              </div>
              <div class="form-group">
                <label for="description">Description <b>(RULES)</b></label>
                <textarea id="description" class="form-control" name="description" value="" data-validation="required" required="" placeholder="Enter Description"></textarea>
              </div>
              <div class="form-group clearfix select_only_date">
                <div class="col-md-6 padd0">
                  <label for="start_date">Start Date</label> 
                  <input type="text" id="start_date" class="form-control eve_datetimepicker start_date" value="" required="" placeholder="Enter Start Date" >
                </div>
                <div class="col-md-6 padd0">
                  <label for="end_date">End Date</label>
                  <input type="text" id="end_date" class="form-control eve_datetimepicker end_date" value="" required="" placeholder="Enter End Date">
                </div>
              </div>              
              <div class="form-group clearfix select_date_time">
                <div class="col-md-6 padd0">
                  <label for="start_date">Start Date</label> 
                  <input type="text" id="start_date" class="form-control eve_datetimepicker start_date" value="" required="" placeholder="Enter Start Date Time" >
                </div>
                <div class="col-md-6 padd0">
                  <label for="end_date">End Date</label>
                  <input type="text" id="end_date" class="form-control eve_datetimepicker end_date" value="" required="" placeholder="Enter End Date Time">
                </div>
              </div>
              <div class="form-group clearfix">
                <div class="col-md-12 padd0" style="margin-bottom: 5px;">
                  <span>
                    <label style="margin-top: 10px;"> <span class="pull-left"><label class="cntnr"><input type="checkbox" class="onlydate" id="onlydate" name="onlydate"><span class="chckmrk"></span></label></span>Only date</label>
                  </span>
                </div>
              </div>
              <div class="form-group event_url">
                <label for="eve_url">Url</label>
                <input type="url" id="eve_url" class="form-control" name="eve_url" value="" placeholder="Enter Event Url">
              </div>
              <div class="form-group">
                <label for="eve_bg_color">Background Color </label>
                <input type="text" id="eve_bg_color" class="form-control" name="eve_bg_color" value=""  placeholder="Enter Background color">
              </div>
              <div class="form-group ev_gradient_color">
                <label for="gradient_color_is_check" class="eve_bg_color"> <span class="pull-left"><label class="cntnr"><input type="checkbox" class="gradient_color_is_check" id="gradient_color_is_check" value="143"><span class="chckmrk"></span></label></span>Animetd Gradient Color</label>
                <input type="text" name="gradient_color" id="gradient_color" class="form-control" value=""  placeholder="Enter Gradient Color" style="display: none;">
              </div>
              <div class="form-group">
                <label for="eve_bg_color">Background Image </label>
                <input type="file" name="event_bg_img" class="form-control" accept="image/*">
                <div class="img_div" style="display: none;"></div>
              </div>
              <div class="form-group">
                <label for="eve_key_title">Select Key </label>
                <select class="form-control" name="choose_key">
                  <option value="">Choose Key</option>
                  <?php if (!empty($keylist)) {
                    foreach ($keylist as $key => $kl) { ?>
                      <option value='<?php echo $kl->id; ?>' <?php echo ($getdefault_keylist[0]->key_id == $kl->id) ? 'selected' : ''; ?>><?php echo $kl->key_title; ?></option>
                    <?php }
                  } ?>
                </select>
              </div>
            </div>
            <div class="box-footer">
              <input class="btn btn-primary" type="submit" value="Submit">
              <a class="btn btn-info clone_event" style="display: none;"> Copy Event </a>
              <a class="btn btn-danger pull-right delete_eve"> Delete Event </a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<link href="<?php echo base_url(); ?>assets/plugins/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<script src="<?php echo base_url().'assets/plugins/datepicker/js/'; ?>moment-with-locales.min.js"></script>
<script type="text/javascript">

  $('.select_only_date .eve_datetimepicker').datetimepicker({viewMode: 'days', format: 'YYYY-MM-DD', defaultDate: null, minDate: new Date}).keydown(false).val("");
  $(".start_date").focus(); 
  
  $('.select_date_time .eve_datetimepicker').datetimepicker({viewMode: 'days', format: 'YYYY-MM-DD HH:mm', defaultDate: null, minDate: new Date}).keydown(false).val("");

  $('.cal_event_form .onlydate').click(function () {
    if ($(this).is(':checked') == true) {
      if ($(".select_only_date .start_date").val() != '') {
        $(".select_only_date .start_date").val(moment($(".select_only_date .start_date").val()).format("YYYY-MM-DD"));
      }
      if ($(".select_only_date .end_date").val() != '') {
        $(".select_only_date .end_date").val(moment($(".select_only_date .end_date").val()).format("YYYY-MM-DD"));
      }
    } else {
      if ($(".select_date_time .start_date").val() != '') {
        $(".select_date_time .start_date").val(moment($(".select_date_time s.start_date").val()).format("YYYY-MM-DD HH:mm"));
      }
      if ($(".select_date_time .end_date").val() != '') {
        $(".select_date_time .end_date").val(moment($(".select_date_time .end_date").val()).format("YYYY-MM-DD HH:mm"));
      }
    }
  });
  function only_date_select() {
    if ($('.cal_event_form .onlydate').is(':checked')) {
      $('.select_date_time').hide().find('input').removeAttr('required name');
      $('.select_only_date').show().find('input').attr('required','');
      $('.select_only_date .start_date').attr('name','start_date');
      $('.select_only_date .end_date').attr('name','end_date');
    } else {
      $('.select_only_date').hide().find('input').removeAttr('required name');
      $('.select_date_time').show().find('input').attr('required','');
      $('.select_date_time .start_date').attr('name','start_date');
      $('.select_date_time .end_date').attr('name','end_date');
    }
  }
  function calendar_mdl_show(args) {
    var action = args.action,
    title = (args.title != undefined && args.title != '') ? args.title : '',
    description = (args.description != undefined && args.description != '') ? args.description : '',
    start_date = (args.start_date != undefined && args.start_date != '') ? args.start_date : '',
    end_date = (args.end_date != undefined && args.end_date != '') ? args.end_date : '',
    color = (args.color != undefined && args.color != '') ? args.color : '',
    gradient_color = (args.gradient_color != undefined && args.gradient_color != '') ? args.gradient_color : '',
    url = (args.url != undefined && args.url != '') ? args.url : '',
    event_id = (args.event_id != undefined && args.event_id != '') ? args.event_id : '',
    evetype = (args.action == 'edit_event') ? ((args.event_type == 'lobby_event') ? ': Lobby Event' : ((args.event_type == 'custom_event') ? ': Custom Event' : '')) : '';

    $('#calendar_mdl .modal-title .lbytype').html(evetype);
    
    (args.event_key != undefined && args.event_key != '' && args.event_key != 0) ? $('select[name="choose_key"] option[value="'+args.event_key+'"]').prop('selected', true) : '';
    (args.event_bg_img != undefined && args.event_bg_img != '') ? $('.cal_event_form .img_div').html('<img src="'+base_url+'upload/event_key_img/'+args.event_bg_img+'" class="img img-responsive"><input type="hidden" name="event_bg_img" value="'+args.event_bg_img+'">').show() : $('.cal_event_form .img_div').html('').hide();

    $('#calendar_mdl').find('input[name="eve_title"]').val(title).closest('#calendar_mdl').find('textarea[name="description"]').val(description).closest('#calendar_mdl').find('.start_date').val(start_date).closest('#calendar_mdl').find('.end_date').val(end_date).closest('#calendar_mdl').find('input[name="eve_bg_color"]').val(color).closest('#calendar_mdl').find('input[name="eve_url"]').val(url);

    (gradient_color != '') ? $('.ev_gradient_color .cntnr .gradient_color_is_check').prop('checked',true).closest('.ev_gradient_color').find('input[name="gradient_color"]').val(gradient_color).show() : $('.ev_gradient_color .cntnr .gradient_color_is_check').prop('checked',false).closest('.ev_gradient_color').find('#gradient_color').val('').hide();

    if (action == 'add_event') {
      $('#calendar_mdl form').removeAttr('event_id').attr('action',base_url+'admin/events/add_events').find('.delete_eve').hide();
      $('#calendar_mdl form').find('input[name="id"], input[name="event_type"]').val('');
    } else {
      $('#calendar_mdl form').find('input[name="id"]').val(event_id).siblings('input[name="event_type"]').val(args.event_type);
      if (args.event_type == 'lobby_event') {
        $('#calendar_mdl form').attr({'action':base_url+'admin/events/edit_events/?lobby_event_id='+event_id,'event_id':event_id}).find('.event_url').hide().closest('form').find('.delete_eve').attr('href',base_url+'admin/events/delete_events/lobby_event/'+event_id).show();
        (args.event_is_expired == 'yes') ? $('#calendar_mdl form .box-footer').find('input').hide().siblings('.clone_event').show() : $('#calendar_mdl form .box-footer').find('input').show().siblings('.clone_event').hide();
      } else {
        $('#calendar_mdl form').attr({'action':base_url+'admin/events/edit_events/?cust_event_id='+event_id,'event_id':event_id}).find('.event_url').show().closest('form').find('.delete_eve').attr('href',base_url+'admin/events/delete_events/custom_event/'+event_id).show();
        (args.event_is_expired == 'yes') ? $('#calendar_mdl form .box-footer').find('input, .delete_eve').hide().siblings('.clone_event').show() : $('#calendar_mdl form .box-footer').find('input, .delete_eve').show().siblings('.clone_event').hide();
      }
    }
    only_date_select();
    $('.cal_event_form .onlydate').click(function () {
      only_date_select();
    });

    // (args.event_is_expired == 'yes') ? $('#calendar_mdl form box-footer').find('.btn, .delete_eve').hide().siblings('.copybtn').show() : $('#calendar_mdl form box-footer').find('.btn, .delete_eve').show().siblings('.copybtn').hide(); 

    $('#calendar_mdl').show();
  }
  $('.add_event_incalendar').click(function () {
    var args = {
      'action' : $(this).attr('action'),
    }
    calendar_mdl_show(args);
  });

  $('.add_events_key, .edit_events_key').click(function () { 
    if ($(this).hasClass('add_events_key')) {
      $('#addevents_key').find('form').attr('action','<?php echo base_url(); ?>admin/events/add_event_key').find('input[name="eve_key_image"]').attr('required','').closest('form').find('.oldimg').hide();
    } 
    if ($(this).hasClass('edit_events_key')) {
      var id = $(this).attr('edit_id'), key_title = $(this).attr('key_title'), key_image = $(this).attr('key_image');
      $('#addevents_key').find('form').attr('action','<?php echo base_url(); ?>admin/events/edit_event_key/'+id).find('input[name="eve_key_title"]').val(key_title);
      $('#addevents_key').find('input[name="eve_key_image"]').removeAttr('required').closest('form').find('.oldimg img').attr('src','<?php echo base_url(); ?>upload/event_key_img/'+key_image).closest('.oldimg').show();
    }
    $('#addevents_key').modal('show');
  });

  $('.add_default_key').click(function () { 
    if ($(this).hasClass('add_default_key')) {
      var keyopt = $(this).attr('keyopt'), option = 'proplayers';
      (keyopt == 'fr_admin_assign') ? option = 'admin' : '';
      $('.'+keyopt).show().siblings().hide();
      $('#add_default_key').find('form').attr('action','<?php echo base_url(); ?>admin/events/add_default_key?key='+option).find('select').removeAttr('name');

      (keyopt == 'fr_admin_assign') ? $('.'+keyopt+' select.select_key').attr('name','choose_default_eve_key') : $('.'+keyopt+' select.select_key').attr('name',"choose_default_eve_key[]");
    }
    $('#add_default_key').modal('show');
  });

  $('#calendar_mdl').find('input[name="eve_bg_color"], .ev_gradient_color #gradient_color').colorpicker().on('colorpickerChange', function(event) {
    $('.jumbotron').css('background-color', event.color.toString());
  });
  $('#calendar_mdl').find('.ev_gradient_color #gradient_color_is_check').click(function () {
    ($(this).is(':checked')) ? $(this).closest('.ev_gradient_color').find('#gradient_color').show() : $(this).closest('.ev_gradient_color').find('#gradient_color').val('').hide();
  });

  var date = new Date();
  var d = date.getDate(), m = date.getMonth(), y = date.getFullYear();
  var events_arr = <?php echo $evelist; ?>;
  $('#calendar').fullCalendar({
    header    : {
      left  : 'prev,next today',
      center: 'title',
      right : 'month,agendaWeek,agendaDay'
    },
    buttonText: {
      today: 'today',
      month: 'month',
      week : 'week',
      day  : 'day'
    },
    selectable: true,
    events: events_arr,
    editable: true,
    eventLimit: true,
    // eventConstraint: {
    //   start: moment().format('YYYY-MM-DD HH:mm'),
    //   end: '2200-01-01'
    // },
    eventClick: function(event) {
      event.action = 'edit_event';
      event_clickfun(event);
      return false;
    },
    eventDrop : function(event, delta, revertFunc) {
      updateevent(event, delta, revertFunc);
    },
    eventResize: function(event, delta, revertFunc) {
      updateevent(event, delta, revertFunc);
    },
    eventRender: function(event, element, view) {
      if (event.color != '') {
        element.css({'background':event.css_backgcolor});
        // ,'z-index':9999
        element.find('.fc-title, .fc-content').css({'z-index':9});
      };
      (event.is_archive == 1) ? element.css('color','black') : ''; 
      document.oncontextmenu = function() {
        console.log('test');
        return false;
      };
      $(element).mousedown(function(e){ 
        if( e.button == 2 ) { 
          var contextmenu = '<div class="dropdown clearfix contextMenu" style="display: block; width: auto; position: absolute;"><ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu" style="display:block;"><li><a class="clone_event">Copy Event</a></li>';
          // contextmenu += '<li><a href="" class="delete_event">Delete Event</a></li>';
          contextmenu += '</ul></div>';

        //    $('#calendar_mdl form').attr({'action':base_url+'admin/events/edit_events/?cust_event_id='+event_id,'event_id':event_id}).find('.event_url').show().closest('form').find('.delete_eve').attr('href',base_url+'admin/events/delete_events/'+event_id).show();
        // (args.event_is_expired == 'yes') ? $('#calendar_mdl form .box-footer').find('input, .delete_eve').hide().siblings('.clone_event').show() : $('#calendar_mdl form .box-footer').find('input, .delete_eve').show().siblings('.clone_event').hide();
        

          $('.contextMenu').remove();
          $(contextmenu).insertAfter($(this));
          $(".contextMenu").hide().css({display: "block",width: 'auto',position: 'absolute'}).find('ul').css({'background-color':'#000','padding': '0','min-width': 'auto'}).find('a').css({'color':'#fff'});
          $(".contextMenu a").mouseover(function () { $(this).css('color','#000'); }).mouseout(function () { $(this).css('color','#fff'); });
          $(".contextMenu .clone_event").click(function () {
            event.action = 'add_event';
            event_clickfun(event);
          });
        } 
        return true; 
      }); 
      $('html').click(function() {
        $('.contextMenu').remove();
      });
      $("form .clone_event").click(function () {
        var event_id = $(this).closest('.cal_event_form').attr('event_id');
        $(event).map(function() {
          if (this.id == event_id) {
            event.action = 'add_event';
            event_clickfun(this);
          }
        });
      });
    },
    eventMouseover: function(event, element, view) {
      if (event.is_proplayre != '') {
        $("body").append('<div class="tooltipevent tooltip_eve_calendar"><div class="eve_cal_title">' + event.is_proplayre + '</div>');
        var bg_color = (event.bg_color != '' && event.bg_color != null) ? ((event.bg_color.indexOf('_') != -1) ? 'linear-gradient(200deg, '+event.bg_color.split('_')[0]+', '+event.bg_color.split('_')[1]+') 0% 0% / 200% 200%' : event.bg_color) : '#ff5000';
        $('.tooltipevent').css({"z-index": 10000, "background": bg_color, "color": "#fff", "border": "1px solid #fff" });
        $(this).mousemove(function(e) { $('.tooltipevent').css('top', e.pageY + 10).css('left', e.pageX + 20); });
      }
    },
    eventMouseout: function(calEvent, jsEvent) {
      $(this).css('z-index', 8);
      $('.tooltipevent').remove();
      // $('.key_shw_div').html('').hide();
    },
  });
  function updateevent(event, delta, revertFunc) {
    if (moment().format('YYYY-MM-DD HH:mm') > event.start._i && moment().format('YYYY-MM-DD HH:mm') < event.end._i) {
      alert("Can't change running event");
      location.reload();
      return false;
    }

    var postdata = {
      'onlyDate' : event.onlyDate,
      'event_type' : event.event_type,
      'id' : event.id,
      'update_start' : event.start.format('YYYY-MM-DD HH:mm'),
      'update_end' : event.end.format('YYYY-MM-DD HH:mm'),
    };
    $.ajax({
      url : base_url +'admin/events/edit_events_drag_resize',
      type: "post",
      data: postdata,
      success: function(res) {
        window.location.reload();
      }
    });
  }
  function event_clickfun(event) {
    (event.action == "add_event") ? $('#calendar_mdl form .box-footer').find('input').show().siblings('.clone_event').hide() : '';
    var startDate = moment(event.start).format("YYYY-MM-DD HH:mm");
    var endDate = moment(event.end).format("YYYY-MM-DD HH:mm");
    $('.onlydate').prop( "checked", false );
    if (event.onlyDate == true) {
      $('.onlydate').prop( "checked", true );
      startDate = moment(event.start).format("YYYY-MM-DD");
      endDate = moment(event.end).format("YYYY-MM-DD");
    }

    var bg_color = main_bg_color = event.color, gradient_color = '';
    var bg_color = (event.bg_color != '' && event.bg_color != null) ? ((event.bg_color.indexOf('_') != -1) ? 'linear-gradient(200deg, '+event.bg_color.split('_')[0]+', '+event.bg_color.split('_')[1]+') 0% 0% / 200% 200%' : event.bg_color) : '#ff5000';
    if (event.bg_color != '' && event.bg_color != null && event.bg_color.indexOf('_') != -1) {
      var evcolor = event.bg_color.split('_');
      gradient_color = evcolor[1];
      main_bg_color = evcolor[0];
      bg_color = 'linear-gradient(200deg, '+evcolor[0]+', '+evcolor[1]+') 0% 0% / 200% 200%';
    }

    var args = {
      'event_id' : (event.id != null && event.id !='') ? event.id : '',
      'title' : (event.title != null && event.title !='') ? event.title : '',
      'description' : (event.description != null && event.description !='') ? event.description : '',
      'color' : main_bg_color,
      'gradient_color' : gradient_color,
      'start_date' : (event.start != null && event.start !='') ? startDate : '',
      'end_date' : (event.end != null && event.end !='') ? endDate : '',
      'url' : (event.url != null && event.url !='') ? event.url : '',
      'event_key' : event.event_key,
      'event_type' : event.event_type,
      'event_bg_img' : (event.event_bg_img != undefined && event.event_bg_img !='') ? event.event_bg_img : '',
      'event_is_expired' : (event.event_is_expired != null && event.event_is_expired != '') ? event.event_is_expired : '',
      'action' : event.action,
    }
    calendar_mdl_show(args);
    return false;
  }
  $('.event_key_order_change').click(function() {
    var id = $(this).attr('id');
    var old_event_order = $(this).attr('event_order');
    $('#order_events_key').find('.modal-title.keyorder').html('Key Order : '+old_event_order);
    $('#order_events_key').find('input[name="key_id"]').val(id).siblings('input[name="old_key_order"]').val(old_event_order);
    $('#order_events_key').modal('show');
  });
</script>