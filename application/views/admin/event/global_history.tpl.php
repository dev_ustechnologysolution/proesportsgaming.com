<section class="content">
  <div class="row">
    <div class="col-md-12">
		<?php $this->load->view('admin/common/show_message') ?>
        <div class="box box-primary">
        	<form method="GET">
        		<div class="box-body mt20">
        			<div class="form-group row">
        				<label class="col-sm-2 text-right">Min date </label>
        				<div class="col-sm-2">
        					<input type="text" class="form-control custom_datetimepicker" dt_format="YYYY-MM-DD" name="min_date" placeholder="Min Date" required>
        				</div>
        				<label class="col-sm-2 text-right">Max date</label>
        				<div class="col-sm-2">
        					<input type="text" class="form-control custom_datetimepicker" dt_format="YYYY-MM-DD" name="max_date" placeholder="Max Date" required>
        				</div>
        				<div class="col-sm-4">
        					<div class="col-sm-6 text-right">
	        					<button type="submit" value="submit" class="btn btn-primary">Submit</button>
	        				</div>
	        				<div class="col-sm-6">
	        					<a class="btn btn-primary" href="<?php echo base_url().'admin/Banner/globalHistory';?>">All Data</a>
	        				</div>        					
        				</div>
        			</div>
        		</div>
        	</form>
            <div class="box-header">
                <a href="javascript:void(0);" class="mt20 btn btn-primary reset_users_btn"> <span class="glyphicon glyphicon-refresh"></span> Reset Selected Users</a>
                <a href="javascript:void(0);" class="mt20 btn btn-primary refund_users_btn"> <span class="glyphicon glyphicon-usd"></span> Refund Selected Users</a>
                <a onclick="history.back();" class="pull-right mt20 btn btn-primary"> <span class="glyphicon glyphicon-chevron-left"></span>Back</a>
            </div>
            <div class="box-body">
                <span style="color: red" id="reset_users_error"></span>
                <p>All Events Users list</p>                
                <table id="example2" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Reset/Refund <br> Select All &nbsp;&nbsp; <input type="checkbox" name="select_all" class="select_all" ></th>
                            <th>Reg #</th>
                            <th>Lobby/Event Name</th>
                            <th>Player Acct.#</th>
                            <!-- <th>Amount Of Ticket($)</th> -->
                            <th>Total Cost</th>
                            <!-- <th>Pro Fees Collected</th> -->
                            <th>Fee</th>
                            <th>Prize</th>
                            <th>Spectate Collected</th>
                            <th>Prize Collected Acct.#</th>
                            <th>User Name</th>
                            <th>Display Name</th>
                            <th>Team Name</th>
                            <th>Email</th>
                            <th>Is Gifted</th>
                            <th>Gifted User</th>
                            <th>Gifted User Acct.#</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $i = 1;
                        foreach($users_data as  $user) { 
                          // $fee = $user->event_price * $user->event_fee/100;
                          $pay_amount = $user->event_price - $user->event_fee;
                          $event_fee_creator = $this->General_model->view_single_row('user_detail','user_id',$user->event_fee_creator);
                          // echo "<pre>"; print_r($user);
                            if($user->is_gift == 1){
                                $is_gift = 'Yes';
                                $gift_user = $this->General_model->view_single_row('user_detail','user_id',$user->gift_to); 
                                $gift_user_name = ucwords($gift_user['name']);
                                $gift_user_acc = $gift_user['account_no'];
                            } else {
                                $is_gift = 'No';
                                $gift_to = '-';
                                $gift_user_name = '';
                                $gift_user_acc = '';
                            }
                           ?>                            
                         <tr>
                            <td><?php echo '<input type="checkbox" name="reset_users[]" value="'.$user->user_id.'" data-lobby_id="'.$user->lobby_id.'">'; ?></td>
                            <td><?php echo $i++; ?></td> 
                            <td><?php echo $user->lobby_name; ?></td> 
                            <td><?php echo $user->account_no; ?></td> 
                            <td><?php echo number_format($user->event_price,2,".",""); ?></td>
                            <td><?php echo number_format($user->event_fee,2,".",""); ?></td>
                            <td><?php echo number_format($pay_amount,2,".",""); ?></td>
                            <td><?php echo $user->spectator_fee;?></td>
                            <td><?php echo $user->event_fee_creator;?></td>
                            <td><?php echo $user->name;?></td>
                            <td><?php echo $user->display_name;?></td> 
                            <td><?php echo $user->team_name;?></td>
                            <td><?php echo $user->email;?></td>
                            <td><?php echo ($user->is_gift == 1) ? 'Yes' : 'No';?></td>
                            <td><?php echo $gift_user_name; ?></td>
                            <td><?php echo $gift_user_acc; ?></td>
                            <td><?php $date_arr = explode(' ',$user->created); echo $date_arr[0];?></td>                            
                        </tr>
                        <?php  }?> 
                    </tbody>
                </table>
                <form role="form" action="<?php echo base_url().'admin/banner/global_history_reset_users'; ?>" class="reset_users_form" method="POST">
                    <input type="hidden" name="selected_users" id="selected_users">
                </form>

                <form role="form" action="<?php echo base_url().'admin/banner/global_history_refund_users'; ?>" class="refund_users_form" method="POST">
                    <input type="hidden" name="selected_users_rf" id="selected_users_rf">
                </form>
            </div>
        </div>
    </div>
   </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        $('.reset_users_btn').on('click',function(){
            var selected_users = [];
            $('input[name="reset_users[]"]:checked').each(function(){
                selected_users.push({'user':$(this).val(),'lobby':$(this).attr('data-lobby_id')});
            });
            $('#selected_users').val(JSON.stringify(selected_users));
            if($('input[name="reset_users[]"]:checked').length == 0){
                $('#reset_users_error').html('Please select users from list to reset.');
            }else{
                $('.reset_users_form').submit();
            }            
        })

        $('.refund_users_btn').on('click',function(){
            var selected_users_rf = [];
            $('input[name="reset_users[]"]:checked').each(function(){
                selected_users_rf.push({'user':$(this).val(),'lobby':$(this).attr('data-lobby_id')});
            });
            $('#selected_users_rf').val(JSON.stringify(selected_users_rf));
            if($('input[name="reset_users[]"]:checked').length == 0){
                $('#reset_users_error').html('Please select users from list to refund.');
            }else{
                $('.refund_users_form').submit();
            }            
        });

        $('input[name="reset_users[]"]').on('change',function(){
            $('#reset_users_error').html('');
        })

        $('.select_all').on('change', function(){
            if($(this).is(":checked")){
                $('input[name="reset_users[]"]').prop('checked', true);
            }else{
                $('input[name="reset_users[]"]').prop('checked', false);
            }
        })

    })
</script>
<style>
    .mt20{
        margin-top: 20px;
    }
</style>


