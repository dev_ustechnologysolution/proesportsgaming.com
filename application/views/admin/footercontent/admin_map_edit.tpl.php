<section class="content">

  <div class="row">

    <div class="col-md-12">

			<?php $this->load->view('admin/common/show_message') ?>

        <div class="box box-primary">

          <form action="<?php echo base_url(); ?>admin/content/adminmapUpdate" method="post" enctype="multipart/form-data">

            <div class="box-body">

              <div class="form-group">

                  <label for="description">Edit Admin Map Video</label>


                  <input type="file" name="admin_map_video" class="form-control">

                 <!--  <textarea class="form-control" name="content" rows="15" cols="8"><?php //if(isset($content_data[0]['content']))echo $content_data[0]['content'];?></textarea> -->

              </div>

              </div><!-- /.box-body -->

              <div class="box-footer">

                <input type="hidden" name="id" value="<?php if(isset($content_data[0]['id'])) echo $content_data[0]['id'];?>">

                <input type="hidden" name="old_admin_map_video" value="<?php if(isset($content_data[0]['content']))echo $content_data[0]['content'];?>">

                <button class="btn btn-primary" type="submit">Update</button>

              </div>

          </form>

        </div>

      </div>

    </div>

</section>