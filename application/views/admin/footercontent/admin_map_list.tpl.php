    <div class="col-sm-12">
      <div class="pull-right" >
        <a href="" onclick="location.reload()"> Refresh </a> &nbsp;|&nbsp; 
        <a href="<?php echo base_url(); ?>admin/game/get_reset_withdrawal" id="get_url_1" class="reset_get"> Reset Export Excel </a> &nbsp;|&nbsp; 
        <a href="<?php echo base_url(); ?>admin/game/withdrawalPaidExcel" id="get_url_2" class="export_get"><i class="fa fa-print"></i> Export Excel</a>  
      </div>
    </div>

<div class="clearfix"></div>
<section class="content">

  <div class="box">

    <?php $this->load->view('admin/common/show_message') ?>



    <div class="box-body">
      <table id="" class="table table-bordered table-striped">

        <thead>

          <tr>

            <th>Sl No</th>
            <th class="admin_map_th">Admin Map Video <label class="switch"><input type="checkbox" name="map_status" value="map_status" <?php if($list[1]['status'] == '1') { echo 'checked'; } ?> class="map_status"><span class="slider round"></span></label></th>

            <!-- <th>Action</th> -->

          </tr>

        </thead>
        <input type="hidden" class="default_id" value="<?php echo $list[1]['id']; ?>">
        <input type="hidden" class="custom_id" value="<?php echo $list[2]['id']; ?>">
        <tbody>

<!--           <?php $i=1; foreach($list as $value){?>

          <tr>

            <td><?php echo $i++; ?></td>

            <td><?php echo $value['content']?></td>

            <td><a href="<?php echo base_url().'admin/content/contentEdit/'.$value['id'];?>"><i class="fa fa-pencil fa-fw"></i></a></td>

           </tr>

          <?php }?> -->

          
           <tr>

            <td <?php if($list[2]['status'] == '1') { echo 'style="display: none;"'; } ?>><?php echo $list[0]['id']; ?></td>
            <td <?php if($list[2]['status'] == '1') { echo 'style="display: none;"'; } ?> align="center" class="admin_map_video">
              <video controls autoplay width="60%">
                <source src="<?php echo base_url(); ?>upload/admin_dashboard/<?php echo $list[1]['content']?>">
               </video>
              </td>
          </tr>

        </tbody>

      </table>

    </div>

  </div>

  <div class="box box-primary custom_map_video" <?php if($list[1]['status'] == '1') { echo 'style="display: none;"'; } ?>>
<!-- http://localhost/profantasygamingnew/admin/content/adminmapUpdate -->
        <table id="" class="table table-bordered table-striped">

      <thead>

        <tr>

          <th>Sl No</th>

          <th class="admin_map_th">Custom Map Video </th>

          <th>Action</th>

        </tr>

      </thead>

      <tbody>
        
         <tr>

          <td><?php echo $list[1]['id']; ?></td>
          <td align="center" class="admin_map_video">
            <video controls autoplay width="60%">
              <source src="<?php echo base_url(); ?>upload/admin_dashboard/<?php echo $list[2]['content']?>">
             </video>
            </td>
          <td><a href="<?php echo base_url().'admin/content/adminmapEdit/'.$list[2]['id'];?>"><i class="fa fa-pencil fa-fw"></i></a></td>
        </tr>

      </tbody>

    </table>

        </div>

</section>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script> $.validate();</script>
<!-- Modal 1 (Basic)-->
<div class="modal fade  custom-width" id="modal-1">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">
      
    </div>
  </div>
</div>
<script>
$(document).on('click','.export_get',function(e){
  e.preventDefault();
  var id = $(this).attr('id');
  //var id = $(this).attr('href');
  if(id){
    $.ajax({
      url : '<?php echo site_url(); ?>admin/FrontUser/security_pass/'+id,
      success: function(result) {
        if(result){
          $('.modal-content').html(result);
          $('#modal-1').modal('show');                              
        }
      }
    })
  }
})

$(document).on('click','.reset_get',function(e){
  e.preventDefault();
  var id = $(this).attr('id');
  //var id = $(this).attr('href');
  if(id){
    $.ajax({
      url : '<?php echo site_url(); ?>admin/FrontUser/security_pass/'+id,
      success: function(result) {
        if(result){
          $('.modal-content').html(result);
          $('#modal-1').modal('show');                              
        }
      }
    })
  }
})


$(document).on('submit','#security_pass',function(e){
  e.preventDefault();
  var data = $(this).serialize();
  var action_value = $('#action_value').val();
  if(data){
    $.ajax({
      url: "<?php echo site_url(); ?>admin/FrontUser/get_reset_data",
      data: data,
      type: 'POST',
      success: function(result) {
        if(result == 'success'){
          var action_url = $('#'+action_value).attr('href');
          window.location.href = action_url;
          $('#modal-1').modal('hide');
        } else {
          $('.modal-content').html(result);
          $('#modal-1').modal('show');        
        }
      }
    });
  }             
})
</script>