<section class="content">
  <div class="box">
    <?php $this->load->view('admin/common/show_message') ?>
	<div class="box-header"><a href="<?php echo base_url().'admin/game/subgameAdd' ?>"><button class="btn btn-primary" style="float:right;">Add Sub Game</button></a></div>
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th style="display:none"></th>
            <th>Sl No</th>
            <th>Game Name</th>
            <th>Sub Game Name</th>
			<th>Game Category</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; foreach($list as $value){?>
          <tr>
            <td style="display:none"></td>
            <td><?php echo $i++; ?></td>
            <td><?php echo $value['game_name']?></td>
            <td><?php echo $value['sub_game_name'];?></td>
            <td><?php echo $value['category_name'];?></td>
            <td><a href="<?php echo base_url().'admin/game/subgameEdit/'.$value['id'];?>"><i class="fa fa-pencil fa-fw"></i></a>|<a href="<?php echo base_url().'admin/game/subgamedelete/'.$value['id'];?>" onclick="return confirm('are you sure delete')"><i class="fa fa-trash-o fa-fw"></i></a></td>
           </tr>
          <?php }?>
        </tbody>
      </table>
    </div>
  </div>
</section>