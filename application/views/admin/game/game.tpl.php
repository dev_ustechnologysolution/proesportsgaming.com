<section class="content">
	<div class="row">
		<div class="col-md-12">
			<?php $this->load->view('admin/common/show_message') ?>
			<div class="box box-primary">
				<form  action="<?php echo base_url(); ?>admin/game/add_game" method="post" enctype="multipart/form-data">
					<div class="box-body">
						<div class="form-group">
							<label for="name">Game Name</label>
							<input type="text" placeholder="Game Name" id="name" class="form-control game-name" name="name" required="required" onkeyup="validate_game()" onkeydown="validate_game()">
							<label class="alert alert-danger already_exist" style="display: none;"></label>
						</div>
						<div class="form-group">
							<label for="description">Game Description</label>
							<textarea class="form-control" id="description" placeholder="Game Description" name="description" rows="8" cols="8">Build Your Esports Team and Take on the World &#13;&#10;Special Instructions Here!</textarea>
						</div>
						<div class="form-group">
							<label for="file">Image (190px-112px)</label>
							<label id="image_size" style="display:none"></label>
							<input type="file" class="form-control" id="file" placeholder="Image" name="image[]"></br>
							<a href="#" class="add_field_button">+ Add More Images</a>
							<div class="input_fields_wrap"/>
						</div></br>
						<div class="form-group">
							<label for="price">Console</label><br/>
							<?php foreach ($cat_name as $value) { ?>
							<b class="col-sm-2"><?php echo $value['category_name']; ?> :</b> <span class="col-sm-2"><input type="checkbox" name="category[]" value="<?php echo $value['id']; ?>" ></span><div class="clearfix"></div>
							<?php } ?>							
						</div>
						<div class="form-group">
							<label for="price">Game Type</label><br/>
							<?php 
							 	$game_type = 16;
							 	echo '<div>';
							 	for ($x = 1; $x <= $game_type; $x++) { 
									if ($x % 4 == 1){
										echo '</div><div class="col-sm-3">';
									}
									echo '<b>'.$x.'V'.$x.' :</b> <input type="checkbox" name="game_type[]" value="'.$x.'" ><br/>';
								} 
								echo '</div>';
							?>
							<div class="clearfix"></div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button class="btn btn-primary" id="game_add" type="submit">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>