<section class="content">
	<div class="row">
		<div class="col-md-12">
			<?php $this->load->view('admin/common/show_message') ?>
			<div class="box box-primary">
				<form action="<?php echo base_url(); ?>admin/game/add_game_cat" method="post" enctype="multipart/form-data" class="game_category_name">
					<div class="box-body">
						<div class="form-group">
							<label for="Catname">Game Category Name</label>
							<input type="text" placeholder="Game Category Name" id="category_name" class="form-control" name="category_name" required="required" pattern="[A-Za-z0-9-_ ]+" title="Allow letters, numbers, hyphens, underscores and space Only">
							<label class="alert alert-danger already_exist" style="display: none;"></label>
						</div>
						<div class="form-group">
							<label for="category_img">Game Category Image</label>
							<input type="file" id="category_img" class="form-control" name="category_img" required="required"  title="Allow .png, .jpg, .gif only" accept="image/*">
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button class="btn btn-primary" id="game_add" type="submit">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>