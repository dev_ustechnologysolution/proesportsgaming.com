<section class="content">

  <div class="box">

    <div class="box-body">

      <table id="example1" class="table table-bordered table-striped">

        <thead>

          <tr>

            <th style="display:none"></th>

            <th>Sl No</th>

            <th>Game Name</th>

           <!--  <th>Game Sub Name</th> -->

            <th>Total Bet Amount</th>

            <th>Admin's Pay Amount</th>

            <th>Admin's Gain Amount</th>

          </tr>

        </thead>

        <tbody>

          <?php $i=1; foreach($total_amnt as $value){?>

          <tr>

            <td style="display:none"></td>

            <td><?php echo $i++; ?></td>

            <td><?php echo $value['game_name']?></td>
            
            <!--<td><?php echo $value['sub_game_name']?></td>-->

            <td><?php echo $value['amount'].$value['currency']?></td>

            <td><?php echo $value['amnt'].$value['currency']?></td>

            <td><?php echo ( $value['amount'] - $value['amnt'] ).$value['currency'] ?></td>

           </tr>

          <?php }?>

        </tbody>

      </table>

    </div>

  </div>

</section>