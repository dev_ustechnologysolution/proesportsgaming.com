<section class="content">
<?php $this->load->view('admin/common/show_message'); ?>
<div class="box">
	<div class="box-header">
		<a href="">
			<a class="btn btn-primary add_upvideo" style="float:right;margin-right: 7px;">Add Stream</a>
		</a>
	</div>
	<div class="box-body uploaded_video_list">
		<table id="example1" class="table table-bordered table-striped">
	        <thead>
	          <tr>
	            <th>Order</th>
	            <th>Tag</th>
	            <th>Video URL #1</th>
	            <th>Twitch Username</th>
	            <th>Display</th>
	            <th>Action</th>
	          </tr>
	        </thead>
	        <tbody>
	        	<?php foreach($video as $vd) { ?>
	        	<tr>
					<td><?php echo $vd->order_by; ?> <a class="change_order" id="<?php echo $vd->id; ?>"><i class="fa fa-pencil fa-fw"></i></a></td>
	        		<td><?php echo $vd->tag; ?></td>
					<td><a href="<?php echo $vd->video_url; ?>" target="_blank"><?php echo $vd->video_url; ?></a></td>
					<td><?php echo $vd->twitch_username; ?></td>
					<th>
						<label class="myCheckbox"><input class="single-checkbox" type="checkbox" name="display" id="<?php echo $vd->id; ?>" <?php echo ($vd->is_display == '1')? 'checked' : ''; ?>><span></span></label>
					</th>
		            <td><a class="view" id="<?php echo $vd->id; ?>"><i class="fa fa-pencil fa-fw"></i></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php echo base_url().'admin/game/deleteVideo/'.$vd->id;?>" onclick="return confirm('are you sure delete')"><i class="fa fa-trash fa-fw"></i></a></td>
	        	</tr>
	        	<?php } ?>
	        </tbody>
		</table>
	</div>
</div>
</section>
<div class="modal fade  custom-width" id="modal-1">
	<div class="modal-dialog" style="width: 50%;">
		<div class="modal-content">
			
		</div>
	</div>
</div>
<script>
$(document).on('submit','#change_vids.edit_vids',function(e){
	e.preventDefault();
	var data = $(this).serialize();
	if(data){
		$.ajax({
			url: "<?php echo site_url(); ?>admin/game/uploaded_videos_details_update/",
			data: data,
			type: 'POST',
			success: function(result) {
				window.location.href = "<?php echo site_url(); ?>admin/game/uploaded_videos/";	
			}
		});
	}							
})
$(document).on('submit','#change_vids.insert_vids',function(e){
	e.preventDefault();
	var data = $(this).serialize();
	if(data){
		$.ajax({
			url: "<?php echo site_url(); ?>admin/game/insert_new_upvideo/",
			data: data,
			type: 'POST',
			success: function(result) {
				window.location.href = "<?php echo site_url(); ?>admin/game/uploaded_videos/";
			}
		});
	}							
})
$(document).on('click','.view',function(){
	var id = $(this).attr('id');
	if(id){
		$.ajax({
			url : '<?php echo site_url(); ?>admin/game/uploaded_videos_details/'+id,
			success: function(result) {
				if (result) {
					$('.modal-content').html(result);
					$('#modal-1').modal('show');
				}
				$(".modal_datatable").DataTable();
				twich_click_fun();
			}
		})
	}
})
$(document).on('click','.add_upvideo',function(){
	$.ajax({
		url : '<?php echo site_url(); ?>admin/game/add_uploaded_videos/',
		success: function(result) {
			if (result) {
				$('.modal-content').html(result);
				$('#modal-1').modal('show');
			}
			$(".modal_datatable").DataTable();
			twich_click_fun();
		}
	});
});
function twich_click_fun() {
	$('.lobbylistbox .lobby_slct').click(function () {
		if ($(this).is(":checked")) {
			$('.lobbylistbox .lobby_slct').not(this).prop('checked', false);
		}
	})
	$('.switch.common_switch input[name="twitch_enable"]').change(function(){
	    if (this.checked) {
	    	if($('.switch.common_switch input[name="obs_enable"]').is(':checked')){
	    		$('.switch.common_switch input[name="obs_enable"]').prop('checked',false).change();
	    	}
	        $('#change_vids .twitch_username, #change_vids .lobbylistbox').show();
	        $('#change_vids .video_url').hide();
	        $('#change_vids input[name="twitchusername"]').attr('required','');
	        $('#change_vids input[name="videourl"]').removeAttr('required');
	        $('#change_vids input[name="videourl"]').val('');
	    } else {
	        $('#change_vids .twitch_username, #change_vids .lobbylistbox').hide();
	        $('#change_vids .video_url').show();
	        $('#change_vids input[name="videourl"]').attr('required','');
	        $('#change_vids input[name="twitchusername"]').removeAttr('required');
	        $('#change_vids input[name="twitchusername"]').val('');
	    }
	});
	$('.switch.common_switch input[name="obs_enable"]').change(function(){
	    if (this.checked) {
	    	if($('.switch.common_switch input[name="twitch_enable"]').is(':checked')){
	    		$('.switch.common_switch input[name="twitch_enable"]').prop('checked',false).change();
	    	}
	    	$('#change_vids .lobbylistbox').show();
	        $('#change_vids .video_url').hide();
	        $('#change_vids input[name="videourl"]').removeAttr('required');
	        $('#change_vids input[name="videourl"]').val('');
	    } else {
	    	$('#change_vids .lobbylistbox').hide();
	        $('#change_vids .video_url').show();
	        $('#change_vids input[name="videourl"]').attr('required','');
	    }
	});
}
$(document).on('click','.change_order',function(){
	var id = $(this).attr('id');
	if(id){
		$.ajax({
			url : '<?php echo site_url(); ?>admin/game/uploaded_videos_order/'+id,
			success: function(result) {
				if(result){
					$('.modal-content').html(result);
					$('#modal-1').modal('show');				
				}
			}
		})
	}
})
$(document).on('click','.uploaded_video_list input[name="display"]',function(){
	var id = $(this).attr('id');
	if (this.checked) {
		$.ajax({
			url : '<?php echo site_url(); ?>admin/game/change_videodisplay_set/'+id+'/1',
			success: function(result) {
			}
		})
	} else {
		$.ajax({
			url : '<?php echo site_url(); ?>admin/game/change_videodisplay_set/'+id+'/0',
			success: function(result) {
			}
		})
	}
})
$(document).on('click','.view_lock',function(){
	var id = $(this).attr('id');
	if(id){
		$.ajax({
			url : '<?php echo site_url(); ?>admin/game/videos_details_lock/'+id,
			success: function(result) {
				if(result){
					window.location.href = "<?php echo site_url(); ?>admin/game/uploaded_videos/";
				}
			}
		})
	}
})
</script>
