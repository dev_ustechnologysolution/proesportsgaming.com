<div class="col-sm-12">
	<div class="pull-right" >
<!-- 		<a href="" onclick="location.reload()"> Refresh </a> &nbsp;|&nbsp; 
		<a href="<?php // echo base_url(); ?>admin/game/get_reset_withdrawal" id="get_url_1" class="reset_get"> Reset All </a> &nbsp;|&nbsp;  -->
		<a href="<?php echo base_url(); ?>admin/game/withdrawalPaidExcel" id="get_url_2" class="export_get"><i class="fa fa-print"></i> Export Excel</a>  
	</div>
</div>
<div class="clearfix"></div>
<section class="content">
  <div class="box">
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Reg #</th>
            <th>User Name</th> 
            <th>Acct #</th>
            <th>Paid To Email</th>
            <th>Phone</th>
            <th>Current Balance($)</th>
            <th>Amount Requested($)</th>
            <th>Amount Paid($)</th>
            <th>Request Date</th>
            <th>Paid Date</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i = 1;
          foreach($withdraw as $value) { ?>
          <tr>
            <td><?php echo $i++;?></td>
            <td><?php echo $value['name'];?></td>
            <td><?php echo $value['account_no'];?></td>
            <td><?php echo $value['paid_to_email'];?></td>
            <td><?php echo $value['number'];?></td>
            <td><?php echo number_format((float)$value['total_balance'], 2, '.', '');?></td>
            <td><?php echo number_format((float)$value['amount_requested'], 2, '.', '');?></td>
            <td><?php echo number_format((float)$value['amount_paid'], 2, '.', '');?></td>
            <td><?php echo $value['req_date'];?></td>
            <td><?php if($value['status']==0) { echo '<span style="margin-left: 50px;">-</span>'; } else { echo $value['paid_date'];} ?></td>
            <td><a href="javascript:void(0)" class="delete" id="<?php echo $value['id'];?>"><i class="fa fa-trash-o fa-fw"></i>
          </tr>

          <?php }?>
        </tbody>
      </table>
    </div>
  </div>
</section>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script> $.validate();</script>
<!-- Modal 1 (Basic)-->
<div class="modal fade  custom-width" id="modal-1">
	<div class="modal-dialog" style="width: 50%;">
		<div class="modal-content">
			
		</div>
	</div>
</div>

<script>
$(document).on('click','.export_get',function(e){
	e.preventDefault();
	var id = $(this).attr('id');
	//var id = $(this).attr('href');
	if(id){
		$.ajax({
			url : '<?php echo site_url(); ?>admin/FrontUser/security_pass/'+id,
			success: function(result) {
				if(result){
					$('.modal-content').html(result);
					$('#modal-1').modal('show');															
				}
			}
		})
	}
})

$(document).on('click','.reset_get',function(e){
	e.preventDefault();
	var id = $(this).attr('id');
	//var id = $(this).attr('href');
	if(id){
		$.ajax({
			url : '<?php echo site_url(); ?>admin/FrontUser/security_pass/'+id,
			success: function(result) {
				if(result){
					$('.modal-content').html(result);
					$('#modal-1').modal('show');															
				}
			}
		})
	}
})


$(document).on('submit','#security_pass',function(e){
	e.preventDefault();
	var data = $(this).serialize();
	var action_value = $('#action_value').val();
	if(data){
		$.ajax({
			url: "<?php echo site_url(); ?>admin/FrontUser/get_reset_data",
			data: data,
			type: 'POST',
			success: function(result) {
				if(result == 'success'){
					var action_url = $('#'+action_value).attr('href');
					window.location.href = action_url;
					$('#modal-1').modal('hide');
				} else {
					$('.modal-content').html(result);
					$('#modal-1').modal('show');				
				}
			}
		});
	}							
})

$(document).on('click','.delete',function(e){
	e.preventDefault();
	var id = $(this).attr('id');
	//var id = $(this).attr('href');
	if(id){
		$.ajax({
			url : '<?php echo site_url(); ?>admin/Game/security_pass/'+id,
			success: function(result) {
				if(result){
					$('.modal-content').html(result);
					$('#modal-1').modal('show');
				}
			}
		})
	}
})
$(document).on('submit','#security_pass_with',function(e){
  e.preventDefault();
  var data = $(this).serialize();
  if(data){
    $.ajax({
      url: "<?php echo base_url();?>admin/Game/remove_withdrawal",
      data: data,
      type: 'POST',
      success: function(result) {
      	if(result == 'success'){
      		window.location.href = "<?php echo base_url();?>admin/game/paid_withdrawal";
      	}  else {
      		$('.modal-content').html(result);
        	$('#modal-1').modal('show'); 
        }
      }
    });
  }             
});

</script>
