<section class="content">
  	<div class="box">
  		<div class="col-md-12 msg">
  			<?php $this->load->view('admin/common/show_message'); ?>
  		</div>
  		<div class="box-body">
  			<div class="content">
  				<?php if (!empty($giveaway_detail)) {
  					echo $giveaway_detail['gw_description'];
  				} ?>
  			</div>
	    </div>
    </div>
  </div>
</section>
