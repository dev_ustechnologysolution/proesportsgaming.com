<section class="content">
  	<div class="box clone_data">
  		<div class="col-md-12 msg">
  			<?php $this->load->view('admin/common/show_message'); ?>
  		</div>
  		<div class="box-header">
  			<div class="pull-right">
  				<button class="btn btn-primary btn-xl edit-data" id="<?php echo !empty($global_gafees) ? $global_gafees['option_value'] : '2'; ?>" action="edit_glbl_gafee">Global Giveaway Fee</button>
        </div>
      </div>
  		<div class="box-body">
	      <table id="example1" class="table table-bordered table-striped">
	        <thead>
	          <tr>
	          	<th>Sr No.</th>
	            <th>Lobby ID</th>
	            <th>Giveaway Title</th>
	            <th>Creator Acct#</th>
	            <th>Creator Tag</th>
	            <th>Lobby Name</th>
	            <th>Amount</th>
	            <th>Limit Total Entries</th>
	            <th>Description of Prize & Rules</th>
	            <th>Created Date</th>
	            <th>Date and Time of Giveaway</th>
	            <th>Live Link</th>
	            <th>Action</th>
	          </tr>
	        </thead>
	        <tbody>
	        	<?php 
	        	if (!empty($get_all_giveaway)) { 
	        		$i = 1;
	        		foreach ($get_all_giveaway as $key => $gag) { ?>
	        			<tr>
	        				<td><?php echo $i++; ?></td>
	        				<td><?php echo $gag['title']; ?></td>
		        			<td><?php echo $gag['lobby_id']; ?></td>
		        			<td><?php echo $gag['creator_acc']; ?></td>
		        			<td><?php echo (!empty($gag['display_name'])) ? $gag['display_name'] : $gag['name']; ?></td>
		        			<td><?php echo $gag['game_name']; ?></td>
		        			<td><?php echo number_format((float)$gag['gw_amount'], 2, '.', ''); ?></td>
		        			<td><?php echo $gag['gw_entrieslimit']; ?></td>
		        			<td>
		        				<a href="<?php echo base_url().'admin/giveaway/description/'.$gag['lobby_id']; ?>" target="_blank" class="btn btn-primary" data-placement="bottom" data-toggle="tooltip" data-original-title="View Description of Prize & Rules"><i class="fa fa-file-text"></i></a>
		        			</td>
		        			<td><?php echo date('m/d/Y H:i:s', strtotime($gag['created_at'])); ?></td>
		        			<td><?php echo date('m/d/Y H:i:s', strtotime($gag['gw_date_time_of_giveaway'])); ?></td>
		        			<td><a target="_blank" class="btn btn-primary" href="<?php echo base_url().'livelobby/watchLobby/'.$gag["lobby_id"]; ?>">Live Lobby</a></td>
		        			<td style="min-width: 130px;">
		        				<a class="btn btn-primary" data-placement="bottom" data-toggle="tooltip" data-original-title="Giveaway Users" href="<?php echo base_url().'admin/giveaway/giveaway_users/'.$gag['lobby_id']; ?>"><i class="fa fa-user"></i></a>  <a class="btn btn-primary common_modal_show" data-placement="bottom" data-toggle="tooltip" data-original-title="View Winner" action="show_ga_winner" tg_id="<?php echo 'giveawayid_'.$gag['gw_id'].'&lobbyid_'.$gag['lobby_id']; ?>" url="<?php echo base_url(); ?>admin/giveaway/ga_winners"><i class="fa fa-trophy"></i></a>  <a class="btn btn-primary delete-data" data-placement="bottom" data-toggle="tooltip" data-original-title="Delete Giveaway" action="reset_giveaway" id="<?php echo $gag['lobby_id']; ?>"><i class="fa fa-trash"></i></a></td>
		        		</tr>
	        		<?php }
	        	} ?>
	        </tbody>
	      </table>
	    </div>
    </div>
  </div>
  <input type="hidden" name="ga_fee_collector" value="<?php echo (!empty($ga_fee_collector)) ? $ga_fee_collector['option_value'] : 0; ?>">
</section>
