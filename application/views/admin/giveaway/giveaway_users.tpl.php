<section class="content">
		<div class="clearfix msg">
			<?php $this->load->view('admin/common/show_message'); ?>
		</div>
  	<div class="box clone_data">
  		<div class="box-header">
  			<a href="<?php echo base_url().'admin/giveaway/reset_all_player/'.$giveaway_id; ?>" class="mt20 btn btn-primary"> <span class="glyphicon glyphicon-refresh"></span> Reset</a> 
  			<a class="mt20 btn btn-primary ga_usr_act_btn" action="reset_users"> <span class="glyphicon glyphicon-refresh"></span> Reset Selected Users</a>
  			<a class="mt20 btn btn-primary ga_usr_act_btn" action="refund_users"> <span class="glyphicon glyphicon-usd"></span> Refund Selected Users</a>
  			<a onclick="history.back();" class="pull-right mt20 btn btn-primary"> <span class="glyphicon glyphicon-chevron-left"></span>Back</a>
	    </div>
  		<div class="box-body">
	      <table id="customize_tbl" class="table table-bordered table-striped" account_search_clm="1">
	        <thead>
	          	<tr>
		          	<th>Reset/Refund <br> Select All  <input type="checkbox" name="select_all" class="select_all" ></th>
		          	<th>Player Acct#</th>
		          	<th>Player Name</th>
		          	<th>Giveaway Tag</th>
		          	<th>Deducted amount</th>
		            <th>Date and Time</th>
	          	</tr>
        		<tr>
					<td colspan="6"><input type="text" id="search-acc" placeholder="Search Account" class="form-control" style=" width: 100%; "></td>
				</tr>
	        </thead>
	        <tbody>
	        	<?php 
	        	if (!empty($giveaway_all_users)) { 
	        		foreach ($giveaway_all_users as $key => $gag) { ?>
	        			<tr>
	        				<td><?php echo '<input type="checkbox" name="reset_users[]" value="'.$gag['gu_id'].'_'.$gag['user_id'].'">'; ?></td>
		        			<td><?php echo $gag['account_no']; ?></td>
		        			<td><?php echo ($gag['display_name_status'] == 1 && !empty($gag['display_name'])) ? $gag['display_name'] : $gag['name']; ?></td>
		        			<td><?php echo $gag['giveaway_tag']; ?></td>
		        			<td><?php echo (!empty($gag['deducted_amount']) && $gag['deducted_amount'] != null) ? $gag['deducted_amount'] : number_format((float)$gag['gw_amount'], 2, '.', ''); ?></td>
		        			<td><?php echo date('m/d/Y H:i:s', strtotime($gag['created_at'])); ?></td>
		        		</tr>
	        		<?php }
	        	} ?>
	        </tbody>
	      </table>
	      <form role="form" action="<?php echo base_url().'admin/giveaway/remove_giveaway_users'; ?>" class="ga_users_form" method="POST">
	      	<input type="hidden" name="lobby_id" value="<?php echo $lobby_id; ?>">
	      	<input type="hidden" name="selected_guids" id="selected_guids">
	      	<input type="hidden" name="selected_user_ids" id="selected_user_ids">
	      	<input type="hidden" name="type" id="reset_users">
	      </form>
	    </div>
    </div>
  </div>
</section>
<script type="text/javascript">
	$('.select_all').on('change', function(){
		($(this).is(":checked")) ? $('input[name="reset_users[]"]').prop('checked', true) : $('input[name="reset_users[]"]').prop('checked', false);
  });
  $('.ga_usr_act_btn').on('click',function(){
  	var action = $(this).attr('action'), selected_guids = [], selected_user_ids = [];
    $('input[name="reset_users[]"]:checked').each(function(){
    	var arr = $(this).val().split('_');
    	selected_guids.push(arr[0]);
    	selected_user_ids.push(arr[1]);
    });
    $('#selected_guids').val(selected_guids);
    $('#selected_user_ids').val(selected_user_ids);
    $('.ga_users_form input[name="type"]').val(action);

    if (selected_guids.length > 0) {
    	$('.ga_users_form').submit();
    } else {
    	var act_var = (action == 'reset_users') ? 'Reset' : ((action == 'refund_users') ? 'Refund' : '');
    	alert('Please select users from list to '+act_var);
    }
  });
</script>
