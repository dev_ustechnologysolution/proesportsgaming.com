<section class="content">

  <div class="box">

    <?php $this->load->view('admin/common/show_message') ?>

    <div class="box-body">

      <table id="" class="table table-bordered table-striped">

        <thead>

          <tr>

            <th>Sl No</th>

            <th>Heading1</th>
            <th>Heading2</th>
            <th>Heading3</th>
            <th>Heading4</th>

            <th>Action</th>

          </tr>

        </thead>

        <tbody>

          <?php $i=1; foreach($list as $value){?>

          <tr>

            <td><?php echo $i++; ?></td>

            <td><?php echo $value['heading1']?></td>

            <td><?php echo $value['heading2']?></td>

            <td><?php echo $value['heading3']?></td>

            <td><?php echo $value['heading4']?></td>

            <td><a href="<?php echo base_url().'admin/heading/headingEdit/'.$value['id'];?>"><i class="fa fa-pencil fa-fw"></i></a></td>

           </tr>

          <?php }?>

        </tbody>

      </table>

    </div>

  </div>

</section>