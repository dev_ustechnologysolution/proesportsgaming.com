<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('admin/common/header');
$this->load->view('admin/common/sidebar');
$this->load->view('admin/common/container');
$this->load->view('admin/common/footer');
$this->load->view('admin/common/down_footer');
?>