<section class="content">

  <div class="box">

    <?php $this->load->view('admin/common/show_message') ?>

    <div class="box-body">

      <table id="" class="table table-bordered table-striped">

        <thead>

          <tr>

            <th>Sl No</th>

            <th>Link Class</th>

            <th>Link Url</th>

            <th>Action</th>

          </tr>

        </thead>

        <tbody>

          <?php $i=1; foreach($list as $value){?>

          <tr>

            <td><?php echo $i++; ?></td>

            <td><?php echo $value['class']?></td>

            <td><?php echo $value['link']?></td>

            <td><a href="<?php echo base_url().'admin/link/linkEdit/'.$value['id'];?>"><i class="fa fa-pencil fa-fw"></i></a></td>

           </tr>

          <?php }?>

        </tbody>

      </table>

    </div>

  </div>

</section>