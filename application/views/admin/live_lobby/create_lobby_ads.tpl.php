<section class="content background-white box box-primary">
  <div class="row ">
    <div class="col-md-12">
    	<form action="<?php echo base_url();?>admin/live_lobby/insert_live_lobby_ad/" method="POST" enctype='multipart/form-data' class="add_ads"  onsubmit="return lobbyadsimage_size_validation()">
			   <div class="form-group">
			    <label>Sponsered By Logo Title:</label>
			    <input type="text" class="form-control" id="sponsered_by_logo_title" name="sponsered_by_logo_title" placeholder="Enter Sponsered By Logo Title" required>
			    <label>Upload Sponsered By Logo: (400px * 300px)</label>
    			<label id="sponsered_by_logo_size" style="display:none"></label>
			    <input type="file" class="form-control" id="upload_sponsered_by_logo" name="upload_sponsered_by_logo" placeholder="Upload Sponsered By Logo" required accept="image/*">			    
			    <label>Sponsered By Logo Description:</label>
			    <textarea class="form-control" id="sponsered_by_logo_description" name="sponsered_by_logo_description" placeholder="Enter Sponsered By Logo Description" required></textarea>
			    <label>Sponsered By Logo Link:</label>
			    <input type="text" class="form-control" id="sponsered_by_logo_link" name="sponsered_by_logo_link" placeholder="Enter Sponsered By Logo Link" required>
			  	</div>
				</div>
			  </div>
			  <input type="submit" name="submit" class="btn btn-default" value="Submit">
			</form>
    </div>
  </div>
</section>