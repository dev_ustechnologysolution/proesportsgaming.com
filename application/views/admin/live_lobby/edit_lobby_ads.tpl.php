<section class="content background-white box box-primary">
  <div class="row">
    <div class="col-md-12"> 
<form action="<?php echo base_url();?>admin/live_lobby/update_single_data" method="POST" enctype='multipart/form-data' class="add_ads" onsubmit="return lobbyadsimage_size_validation()">
   <div class="form-group">
    <input type="hidden" name="id" value="<?php echo $edit_id; ?>">
    <label>Sponsered By Logo Title:</label>
    <input type="text" class="form-control" id="sponsered_by_logo_title" name="sponsered_by_logo_title" value="<?php echo $ads_single_row[0]->sponsered_by_logo_title; ?>" required pattern="[A-Za-z0-9-_ ]+" title="Allow letters, numbers, hyphens, underscores and space Only">
    <label>Upload Sponsered By Logo: (400px * 300px)</label>
    <label id="sponsered_by_logo_size" style="display:none"></label>
    <input type="file" class="form-control" id="upload_sponsered_by_logo" name="upload_sponsered_by_logo" value="" accept="image/*">		
    <img src="<?php echo base_url().'upload/ads_sponsered_by_logo/'.$ads_single_row[0]->upload_sponsered_by_logo ?>" height="auto" width="150" alt="Sponsered By Logo">
    <input type="hidden" class="form-control" name="upload_sponsered_by_logo_image" value="<?php echo $ads_single_row[0]->upload_sponsered_by_logo; ?>"> 	    
    <br>
    <label>Sponsered By Logo Description:</label>
    <textarea class="form-control" id="sponsered_by_logo_description" name="sponsered_by_logo_description" placeholder="Enter Sponsered By Logo Description" required><?php echo $ads_single_row[0]->sponsered_by_logo_description; ?></textarea>
    <label>Sponsered By Logo Link:</label>
    <input type="url" class="form-control" id="sponsered_by_logo_link" name="sponsered_by_logo_link" value="<?php echo $ads_single_row[0]->sponsered_by_logo_link; ?>" required>
  </div>
</div>
  </div>
  <input type="submit" name="submit" class="btn btn-default" value="Submit">
</form>

    </div>

  </div>

</section>