<section class="content box">
   <div class="row">
      <div class="col-md-12">
         <div class="row">
           <div class="agent_navigation pull-right">
           	   <a href="<?php echo base_url(); ?>admin/live_lobby/create_live_lobby_ad/">Add Pic Ad</a>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 xwb-main-contact-conversation">
               <div class="table-responsive agent_table">
                  <table id="example1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th>Sl. No.</th>
                           <th>Sponserd By</th>
                           <th>Sponserd Description</th>
                           <th>Sponserd Link</th>
                           <th>Date</th>
                           <th>Option</th>
                        </tr>
                     </thead>
                     <tbody>
                      <?php 
                      $i = 1;
                      if(isset($ads_list)){
                      foreach ($ads_list as $value) {
                        $ads_list_row = '<tr>
                        <td>'.$i.'</td>
                        <td>'.$value->sponsered_by_logo_title.'</td>
                        <td>'.$value->sponsered_by_logo_description.'</td>
                        <td>'.$value->sponsered_by_logo_link.'</td>
                        <td>'.$value->created_date.'</td>
                        <td><a href="'.base_url().'admin/live_lobby/edit_live_lobby_ad/?id='.$value->id.'"><i class="fa fa-pencil" id="'.$value->id.'"></i></a> &nbsp;&nbsp;&nbsp;&nbsp;<a class="delete_ads" id="'.$value->id.'"><i class="fa fa-trash-o" id="'.$value->id.'"></i></a></td>
                      </tr>'; 
                       echo $ads_list_row;
                       $i++;
                       } 
                     }
                       ?>
                     </tbody>
                  </table>
               </div>
            </div>
           <div class="modal fade  custom-width" id="modal-1">
            <div class="modal-dialog" style="width: 50%;">
              <div class="modal-content">
                
              </div>
            </div>
          </div>
         <script> 
            $(document).on('click','.delete_ads',function(){
             var id = $(this).attr('id');
              if(id){
                $.ajax({
                  url : '<?php echo site_url(); ?>admin/chat/security_password_data/'+id,
                  success: function(result) {
                    if(result){
                      $('.modal-content').html(result);
                      $('#modal-1').modal('show');                              
                    }
                  }
                })
              }
            })
             $(document).on('submit','#security_id',function(e){
            e.preventDefault();
            var data = $(this).serialize();
              $.ajax({
                url: "<?php echo site_url(); ?>admin/live_lobby/delete_ads_data",
                data: data,
                type: 'POST',
                success: function(data_value) {
                if(data_value == '1')
                {
                  window.location.href = "<?php echo site_url(); ?>admin/live_lobby/live_lobby_ads_list/";
                }
                else
                {
                  alert("Password Error");
                }
              }
              });
          });
          </script> 
         </div>
      </div>
   </div>
</section>


