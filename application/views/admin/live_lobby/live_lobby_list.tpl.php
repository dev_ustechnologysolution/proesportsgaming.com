<section class="content">
  <div class="row">
    <?php $this->load->view('admin/common/show_message'); ?>
    <div class="box nav-tabs-custom">
      <ul class="nav nav-tabs" role="tablist">
        <li class="<?php echo ($active == 'live_lobby_list') ? 'active' : '';?>"><a href="#javatab" role="tab" data-toggle="tab">Live Lobby List</a></li>
        <li class="<?php echo ($active == 'event_lobby') ? 'active' : '';?>"><a href="#eventtab" role="tab" data-toggle="tab">Event Lobby List</a></li>
      </ul>
      <div class="tab-content">
      <div class="tab-pane <?php echo ($active == 'live_lobby_list') ? 'active' : '';?>" id="javatab">
        <div class="banner_page2">
          <div class="box-body">
            <div class="col-md-12">
              <div class="table-responsive agent_table">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Display Order</th>
                      <th>Lobby ID</th> 
                      <th>Creator Acct#</th>
                      <th>Creator Tag</th>
                      <th>Lobby Name</th>
                      <th>Price</th>
                      <th>Lobby Size</th>
                      <th>Fans</th>
                      <th>Status</th>
                      <th>Created Date</th>
                      <th>Option</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $i = 1;
                    if (isset($live_lobby_list) && $live_lobby_list !='') {
                      foreach ($live_lobby_list as $value) {
                        if($value->is_event == 0){
                          $stream_data = $this->General_model->get_defult_lobby_chat($value->id);
                          $arr_stream_data = array($stream_data);
                          $creator_tag = $this->General_model->get_my_fan_tag($value->id,$value->user_id)->fan_tag;
                          if(count($arr_stream_data) > 0 ) {
                              if($arr_stream_data[0]->stream_status == 'enable'){
                                $stream_img = base_url().'assets/frontend/images/live_stream_off_red.png';
                              } else {
                                $stream_img = base_url().'assets/frontend/images/live_stream_off_admin.png';
                              }
                          } else {
                              $stream_img = base_url().'assets/frontend/images/live_stream_off_admin.png';;
                          }
                          $fans_count = count($this->General_model->fanscount($value->id));
                          $status = 'active';
                          $checked = 'checked';
                          if ($value->status == '0') {
                            $status = 'deactive';
                            $checked = '';
                          }
                          $list_row = '<tr>
                            <td>'.$value->lobby_order.'<a class="lobby_view" id="'.$value->id.'" >   <i class="fa fa-pencil fa-fw" id="'.$value->id.'"></i></a></td>
                            <td>'.$value->id.'</td>
                            <td>'.$value->creator_accno.'</td>
                            <td>'.$creator_tag.'</td>
                            <td>'.$value->game_name.'</td>
                            <td>'.$value->price.'</td>
                            <td>'.$value->game_type.'v'.$value->game_type.'</td>
                            <td>'.$fans_count.'</td>';
                            // <td><label class="myCheckbox"><input '.$checked.' id="'.$value->id.'" class="single-checkbox custom-cb edit_lobby" custom_type="'.$value->status.'" type="checkbox" name="progress" value="'.$status.'"><span></span></label></td>
                            $list_row .= '<td><img width="34" src='.$stream_img.'></td>
                            <td>'.$value->created_at.'</td>
                            <td><a class="delete_lobby delete-data" id="'.$value->id.'" custom-type="delete" action="delete_live_lobby"><i class="fa fa-trash-o" id="'.$value->id.'"></i></a></td>
                          </tr>';
                          echo $list_row;
                          $i++;
                        }
                      } 
                    } ?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="modal fade  custom-width" id="modal-1">
              <div class="modal-dialog" style="width: 50%;">
                <div class="modal-content">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane <?php echo ($active == 'event_lobby') ? 'active' : '';?>" id="eventtab">
        <div class="banner_page2">
          <div class="box-body">
            <div class="col-md-12">
              <div class="table-responsive agent_table">
                <table id="example2" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Display Order</th>
                      <th>Lobby ID</th> 
                      <th>Creator Acct#</th>
                      <th>Creator Tag</th>
                      <th>Lobby Name</th>
                      <th>Price</th>
                      <th>Lobby Size</th>
                      <th>Fans</th>
                      <th>Status</th>
                      <th>Created Date</th>
                      <th>Option</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $i = 1;
                    if (isset($live_lobby_list) && $live_lobby_list !='') {
                      foreach ($live_lobby_list as $value) {
                        if($value->is_event == 1){
                          $stream_data = $this->General_model->get_defult_lobby_chat($value->id);
                          $arr_stream_data = array($stream_data);
                          $creator_tag = $this->General_model->get_my_fan_tag($value->id,$value->user_id)->fan_tag;
                          if(count($arr_stream_data) > 0 ) {
                              if($arr_stream_data[0]->stream_status == 'enable'){
                                $stream_img = base_url().'assets/frontend/images/live_stream_off_red.png';
                              } else {
                                $stream_img = base_url().'assets/frontend/images/live_stream_off_admin.png';
                              }
                          } else {
                              $stream_img = base_url().'assets/frontend/images/live_stream_off_admin.png';;
                          }
                          $fans_count = count($this->General_model->fanscount($value->id));
                          $status = 'active';
                          $checked = 'checked';
                          if ($value->status == '0') {
                            $status = 'deactive';
                            $checked = '';
                          }
                          $list_row = '<tr>
                            <td>'.$value->lobby_order.'<a class="lobby_view" id="'.$value->id.'" >   <i class="fa fa-pencil fa-fw" id="'.$value->id.'"></i></a></td>
                            <td>'.$value->id.'</td>
                            <td>'.$value->creator_accno.'</td>
                            <td>'.$creator_tag.'</td>
                            <td>'.$value->game_name.'</td>
                            <td>'.$value->price.'</td>
                            <td>'.$value->game_type.'v'.$value->game_type.'</td>
                            <td>'.$fans_count.'</td>';
                            // <td><label class="myCheckbox"><input '.$checked.' id="'.$value->id.'" class="single-checkbox custom-cb edit_lobby" custom_type="'.$value->status.'" type="checkbox" name="progress" value="'.$status.'"><span></span></label></td>
                            $list_row .= '<td><img width="34" src='.$stream_img.'></td>
                            <td>'.$value->created_at.'</td>
                            <td><a class="delete_lobby delete-data" id="'.$value->id.'" custom-type="delete" action="delete_live_lobby"><i class="fa fa-trash-o" id="'.$value->id.'"></i></a></td>
                          </tr>';
                          echo $list_row;
                          $i++;
                        }
                      } 
                    } ?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="modal fade  custom-width" id="modal-1">
              <div class="modal-dialog" style="width: 50%;">
                <div class="modal-content">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
</section>
<script> 
  $(document).on('click','.edit_lobby',function(){
    var id = $(this).attr('id');
    var custom_type = $(this).attr('custom_type');
    if(custom_type == 0){ var type = 1;} else { type = 0;}
   
    if(id){
      $.ajax({
        url : '<?php echo site_url(); ?>admin/live_lobby/updateStatusPasswordRequire/',
        data: {id:id,type:type},
         type: 'POST',
        success: function(result) {
          if(result){
            $('.modal-content').html(result);
            $('#modal-1').modal('show');                              
          }
        }
      })
    }
  });
  $(document).on('submit','#status_update',function(e){
    e.preventDefault();
    var data = $(this).serialize();
    $.ajax({
      url: "<?php echo site_url(); ?>admin/live_lobby/updateLiveLobbyStatus",
      data: data,
      type: 'POST',
      success: function(data_value) {
        if(data_value == '1') {
          window.location.href = "<?php echo site_url(); ?>admin/live_lobby/live_lobby_list/";
        } else {
          alert("Password Error");
        }
      }
    });
  });
  $(document).on('click','.lobby_view',function(){
    var id = $(this).attr('id');
    if (id) {
      $.ajax({
        url : '<?php echo site_url(); ?>admin/live_lobby/ChangeLobbyListOrder/?id='+id,
        success: function(result) {
          if(result){
            $('.modal-content').html(result);
            $('#modal-1').modal('show');                              
          }
        }
      });
    }
  });
  $(document).on('submit','#change_lobby_order',function(e){
    e.preventDefault();
    var data = $(this).serialize();
    if (data) {
      $.ajax({
        url: "<?php echo site_url(); ?>admin/live_lobby/UpdateLobbyListOrder",
        data: data,
        type: 'POST',
        success: function(result) {
          window.location.href = "<?php echo site_url(); ?>admin/live_lobby/live_lobby_list";       
        }
      });
    }
  });
</script> 
