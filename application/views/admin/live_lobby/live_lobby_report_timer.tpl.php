<section class="content">
	<section class="content">
    	<div class="row">
			<div class="col-md-12">
				<?php $this->load->view('admin/common/show_message') ?>
				<div class="box box-primary">					
					<form  action="<?php echo base_url(); ?>admin/live_lobby/lobby_report_timer_update/" method="post" enctype="multipart/form-data" class="live_lobby_fees_point">
						<div><?php if($this->session->flashdata('status_msg')) { 
							echo $this->session->flashdata('status_msg'); 
							unset($_SESSION['status_msg']);
						} ?></div>
						<div class="form-group">
		                  <label>Hour</label>
		                  <select class="form-control" name="hours">
		                    <?php
		                    $n=24;
		                    for($i=0;$i<=$n;$i++){
		                    	$option = '<option value="'.$i.'"';
		                    	if ($i == $data->hours) {
		                    		$option .= 'selected';
		                    	}
		                    	$option .= '>'.$i.'</option>';	
		                    	echo $option;
		                     }
		                    ?>
		                  </select>
		              </div>
		              <div class="form-group">
		              	  <label>Minutes</label>
		                  <select class="form-control" name="minutes">
		                    <?php
		                    $n=60;
		                    for($i=0;$i<=$n;$i++){
		                    	$option = '<option value="'.$i.'"';
		                    	if ($i == $data->minutes) {
		                    		$option .= 'selected';
		                    	}
		                    	$option .= '>'.$i.'</option>';	
		                    	echo $option;
		                     }
		                    ?>
		                  </select>
		                </div>
		                <div class="form-group">
		                  <button class="btn btn-primary" id="update_report_timer" type="submit">Update</button>
		                </div>				
					</form>
				</div>
			</div>
		</div>
	</section>
</section>