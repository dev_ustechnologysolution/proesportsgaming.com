<br>
<?php $this->load->view('admin/common/show_message'); ?>
<section class="content"> 
  <div class="box">
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Sl. No.</th>
            <th>Acct#</th>
            <th>Trascation Id</th>
            <th>Player Name</th>          
            <th>Player Email</th>
            <th>Amount</th>
            <th>Created Date/Time</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $i = 1;
          foreach($plan_activated_users as $value) { ?>
            <tr>
              <td><?php echo $i++; ?></td>
              <td><?php echo $value['account_no']; ?></td>
              <td><?php echo $value['transaction_id']; ?></td>
              <td><?php echo ucwords($value['name']);?></td>
              <td><?php echo $value['payer_email']; ?></td>
              <td><?php echo $value['amount']; ?>
              <td><?php echo $value['create_at'];?></td>  
           </tr>
          <?php }?>
        </tbody>
      </table>
    </div>
  </div>
</section>