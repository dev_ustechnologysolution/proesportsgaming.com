<section class="content">
  <div class="row">   
    <div class="col-md-10">
      <?php $this->load->view('admin/common/show_message') ?>      
      <div class="box box-primary">
        <form enctype="multipart/form-data" action="<?php echo base_url().'admin/membership/create'; ?>" method="post" style="padding: 10px;">
          <div class="box-body">
            <div class="form-group row">
              <label for="title" class="col-sm-2">Title</label>
              <div class="col-sm-10">
                <input required type="text" class="form-control" name="title">
              </div>
            </div>

            <div class="form-group row">
              <label for="description" class="col-sm-2">Description</label>
              <div class="col-sm-10">
                <textarea required name="description" class="form-control" rows="10" cols="80"></textarea>
              </div>
            </div>

            <div class="form-group row">
              <label for="time_duration" class="col-sm-2">Time Duration</label>
              <div class="col-sm-10">
                <select name="time_duration" class="form-control">
                  <option value="Day">Day</option>
                  <option value="Week">Week</option>
                  <option value="SemiMonth">SemiMonth</option>
                  <option value="Month" selected>Month</option>
                  <option value="Year">Year</option>                    
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label for="image" class="col-sm-2">Image</label>
              <div class="col-sm-10">
                <input required type="file" class="form-control" name="image" accept="image/*">
              </div>
            </div>

            <div class="form-group row">
              <label for="image" class="col-sm-2">Lobby Creation limit</label>
              <div class="col-sm-10">
                <input required type="number" class="form-control" name="creation_limit" min="<?php echo(!empty($max_lobby_creation_limit)) ? $max_lobby_creation_limit : '1'; ?>" value="<?php echo(!empty($max_lobby_creation_limit)) ? $max_lobby_creation_limit : '1'; ?>">
              </div>
            </div>

            <div class="form-group row">
              <label for="image" class="col-sm-2">Lobby Joining limit</label>
              <div class="col-sm-10">
                <input required type="number" class="form-control" name="joining_limit" min="<?php echo(!empty($max_lobby_joining_limit)) ? $max_lobby_joining_limit : '1'; ?>" value="<?php echo(!empty($max_lobby_joining_limit)) ? $max_lobby_joining_limit : '1'; ?>">
              </div>
            </div>

            <div class="form-group row">
              <label for="image" class="col-sm-2">Stream Record limit</label>
              <div class="col-sm-10">
                <input required type="number" class="form-control" name="stream_record_limit" min="0" value="0">
              </div>
            </div>

            <div class="form-group row">
              <label for="image" class="col-sm-2">Tournament Ticket Discount</label>
              <div class="col-sm-10">
                <label class="switch common_switch">
                  <input type="checkbox" name="set_discount" onclick="(($(this).is(':checked')) ? $('.tkt_discount_div').show('slow') : $('.tkt_discount_div').hide('slow'))"><span class="slider round"></span>
                </label>
              </div>
            </div>
            <div class="clearfix tkt_discount_div" style="display: none; border: 1px solid rgb(210, 214, 222); padding: 14px 14px 0px; margin: 0px auto 18px;">
              <div class="form-group row">
                <label for="image" class="col-sm-2">Free Ticket</label>
                <div class="col-sm-10">
                  <label class="switch common_switch">
                    <input type="checkbox" name="free_ticket" checked onclick="(($(this).is(':checked')) ? $('.touenament_dic_div').hide('slow') : $('.touenament_dic_div').show('slow'))"><span class="slider round"></span>
                  </label>
                </div>
              </div>
              <div class="touenament_dic_div" style="display: none;">
                <div class="form-group row">
                  <label for="image" class="col-sm-2">Ticket Discount type</label>
                  <div class="col-sm-10">
                    <select name="discnt_type" class="form-control" required>
                       <option selected="" value="1">Percentage</option>
                       <option value="2">Fix Amount</option>
                       </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="image" class="col-sm-2">Ticket Discount amount</label>
                  <div class="col-sm-10">
                    <input required type="number" class="form-control" min="0" name="discout_amount" value="0" required>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label for="amount" class="col-sm-2">Amount ($)</label>
              <div class="col-sm-10">
                <input required type="number" class="form-control" min="1" name="amount">
              </div> 
            </div>

            <div class="form-group row">
              <label for="fees" class="col-sm-2">Fees ($)</label>
              <div class="col-sm-10">
                <input required type="number" class="form-control" min="0" name="fees">
               </div> 
            </div>

            <div class="form-group row">
              <label for="terms_condition" class="col-sm-2">Terms & Policy</label>
              <div class="col-sm-10">
                <textarea name="terms_condition" class="form-control" rows="10" cols="80"></textarea>
              </div>
            </div>
            <div class="box-footer col-sm-12" align="center"> 
              <div class="col-sm-10">             
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>