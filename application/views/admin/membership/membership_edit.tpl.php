<section class="content">
  <div class="row">   
    <div class="col-md-10">
      <?php $this->load->view('admin/common/show_message') ?>      
      <div class="box box-primary">
        <form  enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/membership/membershipUpdate" method="post" style="padding: 10px;">
          <div class="box-body">
            <div class="form-group row">
              <label for="title" class="col-sm-2">Title</label>
              <div class="col-sm-10">
                <input required type="text" value="<?php if(isset($membership_data['title'])) echo $membership_data['title'];?>" class="form-control" name="title">
              </div>
            </div>
            <div class="form-group row">
              <label for="description" class="col-sm-2">Description</label>
              <div class="col-sm-10">
                <textarea required name="description" class="form-control" rows="10" cols="80"><?php if (isset($membership_data['description'])) echo $membership_data['description'];?></textarea>
              </div>
            </div>
            <div class="form-group row">
              <label for="time_duration" class="col-sm-2">Time Duration</label>
              <div class="col-sm-10">
                <?php 
                if (!empty($plan_activated_users) && count($plan_activated_users) > 0) { ?>
                  <select name="time_duration" class="form-control">
                    <?php echo ($membership_data['time_duration'] == "Day") ? '<option value="Day" selected>Day</option>' : ''; ?>
                    <?php echo ($membership_data['time_duration'] == "Week") ? '<option value="Week" selected>Week</option>' : ''; ?>
                    <?php echo ($membership_data['time_duration'] == "SemiMonth") ? '<option value="SemiMonth" selected>SemiMonth</option>' : ''; ?>
                    <?php echo ($membership_data['time_duration'] == "Month") ? '<option value="Month" selected>Month</option>' : ''; ?>
                    <?php echo ($membership_data['time_duration'] == "Year") ? '<option value="Year" selected>Year</option' : ''; ?>>
                    </select>
                    <label class="alert alert-danger">Currently <?php echo count($plan_activated_users); ?> players have activated this membership plan so its duration cannot be changed.  <a class="btn btn-info btn-sm" href="<?php echo base_url().'admin/membership/membership_activated_player_list/'.$membership_data['id']; ?>">Click to see the player list</a></label>
                <?php } else { ?>
                  <select name="time_duration" class="form-control">
                    <option value="Day" <?php echo ($membership_data['time_duration'] == "Day") ? 'selected' : ''; ?>>Day</option>
                    <option value="Week" <?php echo ($membership_data['time_duration'] == "Week") ? 'selected' : ''; ?>>Week</option>
                    <option value="SemiMonth" <?php echo ($membership_data['time_duration'] == "SemiMonth") ? 'selected' : ''; ?>>SemiMonth</option>
                    <option value="Month" <?php echo ($membership_data['time_duration'] == "Month") ? 'selected' : ''; ?>>Month</option>
                    <option value="Year" <?php echo ($membership_data['time_duration'] == "Year") ? 'selected' : ''; ?> >Year</option>
                  </select>
                <?php } ?>
              </div>
            </div>
            <div class="form-group row upload-div  <?php if($membership_data['image'] != '') echo 'active' ?>" style="padding-left: 0;">
              <label class="col-sm-2">Image</label>
              <div class="col-sm-10">
                <input type="hidden" name="selected_banner" id="edit_banner_image" value="edit_banner_image" <?php if($membership_data['image'] != '') echo 'checked class="checked"';?>>
                <a style="float: right; cursor: pointer; display: none;" class="col-sm-10 browse_new_image">Change Image</a>
                <input type="file" class="form-control image_banner" id="file" placeholder="Image" name="image" accept="image/*" style="display: none;">
                <input type="hidden" id="old_image" name="image" value="<?php echo $membership_data['image'];?>">
                <div  style="margin-top: 5px; ">
                  <img src="<?php echo base_url().'upload/membership/'.$membership_data['image'];?>"  class="old_image_preview" height="150" width="150" style="display: none;">
                </div>
              </div>
            </div>
            <?php
            $set_discount = ($membership_data['tournament_ticket_discount_type'] == '1' && $membership_data['tournament_ticket_discount_amount'] == '0') ? false : true;
            $free_ticket = ($membership_data['tournament_ticket_discount_type'] == '3' && $membership_data['tournament_ticket_discount_amount'] == '0') ? true : false;

            // $membership_data['tournament_ticket_discount_type'] == '1'
            // $membership_data['tournament_ticket_discount_amount'] == '0';
            ?>
            <div class="form-group row">
              <label for="image" class="col-sm-2">Tournament Ticket Discount</label>
              <div class="col-sm-10">
                <label class="switch common_switch">
                  <input type="checkbox" name="set_discount" <?php echo ($set_discount == true) ? 'checked' : ''; ?> onclick="(($(this).is(':checked')) ? $('.tkt_discount_div').show('slow') : $('.tkt_discount_div').hide('slow'))"><span class="slider round"></span>
                </label>
              </div>
            </div>
            <div class="clearfix tkt_discount_div" style="<?php echo ($set_discount != true) ? 'display: none;' : ''; ?> border: 1px solid rgb(210, 214, 222); padding: 14px 14px 0px; margin: 0px auto 18px;">
              <div class="form-group row">
                <label for="image" class="col-sm-2">Free Ticket</label>
                <div class="col-sm-10">
                  <label class="switch common_switch">
                    <input type="checkbox" name="free_ticket" <?php echo ($free_ticket == true) ? 'checked' : ''; ?> onclick="(($(this).is(':checked')) ? $('.touenament_dic_div').hide('slow') : $('.touenament_dic_div').show('slow'))"><span class="slider round"></span>
                  </label>
                </div>
              </div>
              <div class="touenament_dic_div" style="<?php echo ($free_ticket == true) ? 'display: none;' : ''; ?>">
                <div class="form-group row">
                  <label for="image" class="col-sm-2">Ticket Discount type</label>
                  <div class="col-sm-10">
                    <select name="discnt_type" class="form-control" required>
                       <option <?php echo ($membership_data['tournament_ticket_discount_type'] == '1') ? 'selected' : ''; ?> value="1">Percentage</option>
                       <option <?php echo ($membership_data['tournament_ticket_discount_type'] == '2') ? 'selected' : ''; ?> value="2">Fix Amount</option>
                       </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="image" class="col-sm-2">Ticket Discount amount</label>
                  <div class="col-sm-10">
                    <input required type="number" class="form-control" min="0" name="discout_amount" value="<?php echo (!empty($membership_data['tournament_ticket_discount_amount'])) ? $membership_data['tournament_ticket_discount_amount'] : '0'; ?>" required>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label for="image" class="col-sm-2">Lobby Creation limit</label>
              <div class="col-sm-10">
                <input required type="number" class="form-control" name="creation_limit" min="<?php echo(!empty($membership_data['lobby_creation_limit'])) ? $membership_data['lobby_creation_limit'] : '1'; ?>" value="<?php echo(!empty($membership_data['lobby_creation_limit'])) ? $membership_data['lobby_creation_limit'] : '1'; ?>">
              </div>
            </div>

            <div class="form-group row">
              <label for="image" class="col-sm-2">Lobby Joining limit</label>
              <div class="col-sm-10">
                <input required type="number" class="form-control" name="joining_limit" min="<?php echo(!empty($membership_data['lobby_joining_limit'])) ? $membership_data['lobby_joining_limit'] : '1'; ?>" value="<?php echo(!empty($membership_data['lobby_joining_limit'])) ? $membership_data['lobby_joining_limit'] : '1'; ?>">
              </div>
            </div>

            <div class="form-group row">
              <label for="image" class="col-sm-2">Stream Record limit</label>
              <div class="col-sm-10">
                <input required type="number" class="form-control" name="stream_record_limit" min="<?php echo(!empty($membership_data['stream_record_limit'])) ? $membership_data['stream_record_limit'] : '0'; ?>" value="<?php echo(!empty($membership_data['stream_record_limit'])) ? $membership_data['stream_record_limit'] : '0'; ?>">
              </div>
            </div>

            <div class="form-group row">
              <label for="amount" class="col-sm-2">Amount ($)</label>
              <div class="col-sm-10">
                <input required value="<?php if(isset($membership_data['amount'])) echo $membership_data['amount'];?>" type="number" class="form-control" min="1" name="amount">
              </div> 
            </div>

            <div class="form-group row">
              <label for="fees" class="col-sm-2">Fees ($)</label>
              <div class="col-sm-10">
                <input required type="number" value="<?php if(isset($membership_data['fees'])) echo $membership_data['fees'];?>" class="form-control" min="0" name="fees">
               </div> 
            </div>

            <div class="form-group row">
              <label for="terms_condition" class="col-sm-2">Terms & Policy</label>
              <div class="col-sm-10">
                <textarea name="terms_condition" class="form-control" rows="10" cols="80"><?php if(isset($membership_data['terms_condition'])) echo $membership_data['terms_condition'];?></textarea>
              </div>
            </div>

            <div class="box-footer col-sm-12" align="center"> 
              <div class="col-sm-10">  
               <input type="hidden" name="id" value="<?php if(isset($membership_data['id'])) echo $membership_data['id'];?>">           
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>