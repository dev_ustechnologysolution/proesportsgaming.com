<br>
<?php $this->load->view('admin/common/show_message'); ?>
<div class="nav-tabs-custom">
  <ul class="nav nav-tabs" role="tablist">
    <li class="active"><a href="#membership_all_plans" role="tab" data-toggle="tab">Membership Plan</a></li>
    <li><a href="#membership_players_plans" role="tab" data-toggle="tab">Membership Players</a></li>
    <li><a href="#cancelled_membership_plans" role="tab" data-toggle="tab">Cancelled membership</a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="membership_all_plans">
      <div class="box">
        <div class="box-header">  
          <a href="<?php echo base_url().'admin/membership/addMembership'; ?>">
            <button class="btn pull-right btn-primary btn-xl">Add Membership</button>
          </a>
        </div>
        <div class="box-body">
          <table  class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Display Order</th>
                <th>Image</th>
                <th>Title</th>  
                <th>Description</th>
                <th>Amount($)</th>
                <th>Fees($)</th>
                <th>Status</th>
                <th>Created Date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $i=1; foreach($list as $value){ ?>
              <tr>
                <td><a class="edit-data icn_act_ordr" id="<?php echo $value['id'].'_'.$value['display_order']; ?>" action="edit_membership_order_data"><?php echo $value['display_order']; ?><i class="fa fa-pencil fa-fw"></i></a></td>
                <td><img src="<?php echo base_url().'upload/membership/'.$value['image'];?>" width="85" height="50"></td>
                <td><?php echo $value['title']; ?></td>
                <td><?php echo $value['description']; ?></td>
                <td><?php echo $value['amount']; ?></td>
                <td><?php echo $value['fees']; ?></td>
                <td><?php echo ($value['status'] == 1) ? 'Active' : 'Inactive'; ?></td>
                <td><?php echo $value['start_date'];?></td>
                <td>
                  <a href="<?php echo base_url().'admin/membership/membershipEdit/'.$value['id'];?>"> <i class="fa fa-pencil fa-fw"></i></a>|
                  <a href="#" class="delete" onclick="myFunction('<?php echo $value['id']; ?>')"><i class="fa fa-trash-o fa-fw"></i></a>              
                </td>
               </tr>
              <?php }?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="tab-pane" id="membership_players_plans">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <table  id="example_5" class="table table-bordered table-striped">
            <thead>
              <tr>
                <td colspan="9"><input type="text" id="search-acc" placeholder="Search Account" class="form-control" style=" width: 100%; "></td>
              </tr>
              <tr>
                <th>Sl. No.</th>
                <th>Acct#</th>
                <th>Trascation Id</th>
                <th>Plan</th>
                <th>Player Name</th>
                <th>Player Email</th>
                <th>Amount</th>
                <th>Created Date/Time</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $i = 1;
              foreach($plyer_list as $value) {
                $planDetails = $this->General_model->view_single_row('membership','id',$value['plan_id']); 
                ?>
              <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $value['account_no']; ?></td>
                <td><?php echo $value['transaction_id']; ?></td>
                <td><?php echo $planDetails['title']; ?></td>
                <td><?php echo ucwords($value['name']);?></td>
                <td><?php echo $value['payer_email']; ?></td>
                <td><?php echo $value['amount']; ?>
                  <!-- <a class="edit-data icn_act_ordr" id="<?php  //echo $value['id'].'_'.$value['display_order']; ?>" action="edit_player_membership_amount" winre_loc="<?php  //echo base_url().'admin/membership/change_player_membership/'.$value['transaction_id']; ?>"><i class="fa fa-pencil fa-fw"></i> <?php  //echo $value['amount']; ?></a> -->
                </td>
                <td><?php echo $value['create_at'];?></td>
                <td><a class="btn btn-primary delete-data" action="unsubscribe_membership_plan" id="<?php echo $value['transaction_id']; ?>" winre_loc="<?php echo base_url().'admin/membership/cancel_player_membership/'.$value['transaction_id']; ?>">Unsubscribe</a> | <a href="<?php echo base_url(); ?>admin/membership/membershipHistory/?user_id=<?php echo $value['user_id']; ?>">History</a></td>
               </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="tab-pane" id="cancelled_membership_plans">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <table  class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Sl. No.</th>
                <th>Trascation Id</th>
                <th>Plan</th>
                <th>Bought Date</th>
                <th>Player Name</th>
                <th>Player Email</th>
                <th>Acct#</th>
                <th>Amount</th>
                <th>Cancelled Date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $i=1; foreach($cancelled_plyer_list as $value) {
                $planDetails = $this->General_model->view_single_row('membership','id',$value['plan_id']); 
                $subscribe_toname = $this->General_model->view_single_row('user_detail','user_id',$value['user_id']); 
                ?>
              <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $value['transaction_id']; ?></td>
                <td><?php echo $planDetails['title']; ?></td>
                <td><?php echo $value['valid_from']; ?></td>
                <td><?php echo ucwords($subscribe_toname['name']);?></td>
                <td><?php echo $value['payer_email']; ?></td>
                <td><?php echo $subscribe_toname['account_no'];?></td>
                <td><?php echo $value['amount'];?></td>
                <td><?php echo $value['payment_date'];?></td>
                <td>
                  <a href="<?php echo base_url(); ?>admin/membership/membershipHistory/?user_id=<?php echo $value['user_id']; ?>">History</a>
                </td>
               </tr>
              <?php }?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade  custom-width" id="modal-1">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">
      
    </div>
  </div>
</div>
<script>
function myFunction(id) {
  $.ajax({
    url : '<?php echo site_url(); ?>admin/membership/delete_data/'+id,
    success: function(result) {
      if (result) {
        $('.modal-content').html(result);
        $('#modal-1').modal('show');
      }
    }
  });
}
$(document).on('submit','#security_password_id',function(e){
  e.preventDefault();
  var data = $(this).serialize();
  if(data){
    $.ajax({
      url: "<?php echo site_url(); ?>admin/membership/delete",
      data: data,
      type: 'POST',
      success: function(result) {
        if(result == true){
          window.location.href = "<?php echo site_url(); ?>admin/membership/";
        } else {
          $('#security_password').val('');
          alert('Password Error');
          return false;
        }
              
      }
    });
  }             
})
$(document).ready(function() {
  $('.table-bordered').not('#example_5').dataTable({
    aLengthMenu: [[25, 50, 75, -1], [25, 50, 75, "All"]],
    iDisplayLength: 25,
    stateSave: true,
  });
  var table = $('#example_5').DataTable();
  $('#search-acc').on('keypress keyup', function() {
    table.column(1).search(this.value).draw();
  });
 });
</script>