<section class="content">
  <div class="box">
    <div class="box-header"></div>
    <?php $this->load->view('admin/common/show_message'); ?>
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Sl. No.</th>
            <th>Plan Details</th>
            <th>Amount</th>
            <th>Payment Status</th>
            <th>Payment Date</th>
            <!-- <th>Created Date/Time</th> -->
            <!-- <th>Action</th> -->
          </tr>
        </thead>
        <tbody>
          <?php $i=1; foreach($list as $value) {
             $plan_arr = $this->General_model->view_single_row('membership','id',$value['plan_id']); 
            ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><p>Title : <?php echo $plan_arr['title']; ?></p><p>Amount : <?php echo $plan_arr['amount']; ?> | Fees : <?php echo $plan_arr['fees']; ?></p></td>
            <td><?php echo $value['amount']; ?></td>
            <td><lable class="common_text <?php echo ($value['payment_status'] == 'Active' || $value['payment_status'] == 'Completed') ? 'green' : 'red' ?>"><?php echo $value['payment_status']; ?></lable></td>
            <td><?php echo $value['payment_date']; ?></td>
            <!-- <td><?php echo $value['create_at']; ?></td> -->
            <!-- <td><a class="delete_subscription" id="<?php echo $value['payment_id']; ?>"><i class="fa fa-trash-o" id="<?php echo $value['payment_id']; ?>"></i></a></td>
           </tr> -->
          <?php $i++; }?>
        </tbody>
      </table>
    </div>
    <div class="modal fade custom-width" id="security_modal">
      <div class="modal-dialog" style="width: 50%;">
        <div class="modal-content">
          
        </div>
      </div>
    </div>
    <script> 
      $(document).on('click','.delete_subscription',function(){
       var id = $(this).attr('id');
        if(id){
          $.ajax({
            url : '<?php echo site_url(); ?>admin/chat/security_password_data/'+id,
            success: function(result) {
              if(result){
                $('.modal-content').html(result);
                $('#security_modal').modal('show');
              }
            }
          })
        }
      })
      $(document).on('submit','#security_id',function(e){
      e.preventDefault();
      var data = $(this).serialize();
      $.ajax({
        url: "<?php echo site_url(); ?>admin/trade/delete_subscription_history",
        data: data,
        type: 'POST',
        success: function(data) {
          data = $.parseJSON(data);
          if (data.data.data_value != undefined) {
            alert('Password Error');
            return false;
          } else {
              window.location.href = "<?php echo site_url(); ?>admin/trade/subscription_history/?profile_id="+data.data.data_id;
          }
        }
      });
    });
    </script>
  </div>
</section>
<style type="text/css">
  .common_text{
    color: #fff;
    padding: 6px 12px;
    text-align: center;
    vertical-align: middle
  }
  .red {
    background-color: red;
  }
  .green {
    background-color: green;
  }
</style>