<section class="content">
  <div class="box">
  <div class="box-header">
    </div>
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Sl. No.</th>
            <th>Trascation Id</th>
            <th>Plan</th>
            <th>Player Name</th>
            <th>Amount</th>
            <th>Created Date/Time</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; foreach($list as $value) {
            $planDetails = $this->General_model->view_single_row('membership','id',$value['plan_id']); 
            $subscribe_toname = $this->General_model->view_single_row('user_detail','user_id',$value['user_id']); 
            ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $value['transaction_id']; ?></td>
            <td><?php echo $planDetails['title']; ?></td>
            <td><?php echo ucwords($subscribe_toname['name']);?></td>
            <td><?php echo $value['amount'];?></td>
            <td><?php echo $value['create_at'];?></td>
            <td><a href="<?php echo base_url(); ?>admin/membership/membershipHistory/?user_id=<?php echo $value['user_id']; ?>">History</a></td>
           </tr>
          <?php }?>
        </tbody>
      </table>
    </div>
    <div class="modal fade custom-width" id="security_modal">
      <div class="modal-dialog" style="width: 50%;">
        <div class="modal-content">
          
        </div>
      </div>
    </div>
    <script> 
      $(document).on('click','.plan_unsubscribe', function(){
       var id = $(this).attr('id');
        if(id){
          $.ajax({
            url : '<?php echo site_url(); ?>admin/chat/security_password_data/'+id,
            success: function(result) {
              if(result){
                $('.modal-content').html(result);
                $('#security_modal').modal('show');
              }
            }
          })
        }
      })
      $(document).on('submit','#security_id',function(e){
      e.preventDefault();
      var data = $(this).serialize();
      $.ajax({
        // url: "<?php echo site_url(); ?>admin/trade/unsubscribe",
        // data: data,
        // type: 'POST',
        // success: function(data) {
        //   data = $.parseJSON(data);
        //   if (data.data.data_value != undefined) {
        //     alert('Password Error');
        //     return false;
        //   } else {
        //       window.location.href = "<?php echo site_url(); ?>admin/trade/subscription";
        //   }
        // }
      });
    });
    </script>
  </div>
</section>