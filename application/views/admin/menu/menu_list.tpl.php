<section class="content">

  <div class="box">

    <div class="box-header">
  
      <a href="<?php echo base_url().'admin/menu/addMenu'; ?>"><button class="btn pull-right btn-primary btn-xl">Add Menu</button></a>
    </div>

    <?php $this->load->view('admin/common/show_message') ?>

    <div class="box-body">

      <table id="example1" class="table table-bordered table-striped">

        <thead>

          <tr>

            <th>Sl No</th>

            <th>Menu Title</th>

            <th>Menu Type</th>

            <th>Status</th>

            <th>Action</th>

          </tr>

        </thead>

        <tbody>

          <?php $i=1; foreach($list as $value){ ?>

          <tr>

            <td><?php echo $i++; ?></td>

            <td><?php echo $value['title']?></td>
            <td><?php echo ($value['position'] == 1) ? 'Header Menu' : 'Footer Menu'; ?></td>

            <td id="menu_status_td_<?php echo $value['id']; ?>" >
              <?php 
              if($value['status']=='active')
              { 
              ?>
                <span class="label label-success">Active</span>
              <?php
              }
              else
              {
              ?>  
                <span class="label label-danger">Deactive</span>
              <?php
              }
              ?>
            </td>
            
            <td id="menu_action_td_<?php echo $value['id']; ?>">

              <?php 
              if($value['status']=='active')
              { 
              ?>  
                <button class="btn btn-block btn-danger btn-xs deactiveMenuStatus" id="<?php echo $value['id']; ?>" >Deactive</button>
              <?php
              }
              else 
              {
              ?>
                <button class="btn btn-block btn-success btn-xs activeMenuStatus" id="<?php echo $value['id']; ?>"  >Active</button>
              <?php
              }
              ?>
              <a href="<?php echo base_url().'admin/menu/menuEdit/'.$value['id'];?>"> <button class="btn btn-block btn-primary btn-xs" id="<?php echo $value['id']; ?>" >Edit</button></a>
              <button onclick="myFunction('<?php echo $value['id']; ?>')"  class="btn btn-block btn-xs btn-danger delete">Remove</button>

            </td>

           </tr>

          <?php }?>

        </tbody>

      </table>

    </div>

  </div>

</section>
<div class="modal fade  custom-width" id="modal-1">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">
      
    </div>
  </div>
</div>
<script>
function myFunction(id){
  $.ajax({
      url : '<?php echo site_url(); ?>admin/menu/delete_data/'+id,
      success: function(result) {
        if(result){
          $('.modal-content').html(result);
          $('#modal-1').modal('show');                              
        }
      }
    })
}

$(document).on('submit','#security_password_id',function(e){
  e.preventDefault();
  var data = $(this).serialize();
  if(data){
    $.ajax({
      url: "<?php echo site_url(); ?>admin/menu/delete",
      data: data,
      type: 'POST',
      success: function(result) {
        console.log(result);
        if(result == true){
          window.location.href = "<?php echo site_url(); ?>admin/menu/";
        } else {
          $('#security_password').val('');
          alert('Password Error');
          return false;
        }
              
      }
    });
  }             
})
</script>