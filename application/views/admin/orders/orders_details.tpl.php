<section class="content" style="margin-top:30px;">
   
  <?php $this->load->view('admin/common/show_message') ?> 
 
  <div class="box">
    <div class="box-header">      
      <a href="<?php echo base_url().'admin/orders'; ?>"><button class="btn pull-right btn-primary btn-xl">Back</button></a>
       
    </div>
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Item</th>
            <th>Product Name</th>
            <th>Qty</th>
            <th>Price ($)</th>
            <th>Processing ($)</th>
            <th>Gross ($)</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; foreach($list as $k => $order) {
            $sv = explode('&', $order['sel_variation_amount']);
            $variation_amount = end(explode('_', $sv[2]));
            $item_amnt = $order['amount'] + $variation_amount;
            $gross_total = $order['qty'] * ($order['product_fee'] + $item_amnt);
            ?>
            <tr>
              <td><img onclick="viewProduct('<?php echo $order['id']; ?>');" style="cursor:pointer" src="<?php echo base_url().'upload/products/'.$order['product_image']; ?>" width="50" height="50"></td>
              <td><?php echo $order['product_name']; ?></td>
              <td><?php echo $order['qty']; ?></td> 
              <td><?php echo $item_amnt; ?></td> 
              <td><?php echo $order['product_fee']; ?></td> 
              <td><?php echo number_format($gross_total, 2, '.', ''); ?></td> 
            </tr>
          <?php }?>
        </tbody>
      </table>
    </div>
   
  </div>
</section>
<style>
.no_banner{
  width:15px;
  height:15px;
  background:red;
  border-radius:4px;
}
.yes_banner{
  width:15px;
  height:15px;
  background:green;
  border-radius:4px;
}
.myCheckbox input {
   visibility:hidden;
    position: absolute;
    z-index: -9999;
}
.myCheckbox span {
    width: 15px;
    height: 15px;
    display: block;
    background: red;
    border-radius:4px;
}
.myCheckbox input:checked + span {
    background: green;
}
</style>
<div class="modal fade  custom-width" id="modal-1">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">      
    </div>
  </div>
</div>

<script type="text/javascript">
  function saveData(e,id,title) {
  if(e.keyCode === 13){
    if (confirm('Are you sure you want to save this thing into the database?')) {
      e.preventDefault();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url('admin/orders/savedata')?>",
        data: {  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
                'id': id,
                'title' :title,
              },
        success: function(response){ 
          alert(response);
        }
      });
    }  
 }  
}
function viewProduct(pid){
  $.ajax({
    url : '<?php echo site_url(); ?>admin/orders/orderItemDetail?id='+pid,
    success: function(result) {
      if (result) {
        $('.modal-content').html(result);
        $('#modal-1').modal('show'); 
      }
    }
  });
}
   $(document).on('click','.lobby_view',function(){
  var id = $(this).attr('id');
  if(id){
    $.ajax({
      url : '<?php echo site_url(); ?>admin/Categories/ChangeCategoryOrder/?id='+id,
      success: function(result) {
        if(result){
          $('.modal-content').html(result);
          $('#modal-1').modal('show');                              

        }
      }
    });
  }
});
$(document).on('submit','#change_lobby_order',function(e){
  e.preventDefault();
  var data = $(this).serialize();
  if(data){
    $.ajax({
      url: "<?php echo site_url(); ?>admin/Categories/UpdateCatOrder",
      data: data,
      type: 'POST',
      success: function(result) {
        window.location.href = "<?php echo site_url(); ?>admin/Categories/";       
      }
    });
  }             
});
</script>
<script>
$(document).ready(function(){
    
    var $checkboxes = $('td input[type="checkbox"]');
    
        $('input[type="checkbox"]').click(function()
        {            
            var countCheckedCheckboxes = $checkboxes.filter(':checked').length;            
            var checked_val = $(this).prop("value");
            
            if($(this).prop("checked") == true){             
                
                
                    var type = 'check';
            }
            else if($(this).prop("checked") == false){
                var type = 'uncheck';
            }
            $.ajax({
                url : '<?php echo site_url(); ?>admin/categories/updateIsStatus?type='+type+'&id='+checked_val,
                success: function(result) {                   
                    // window.location = window.location;                    
                   
                }
            });
            
        });
    });


</script>