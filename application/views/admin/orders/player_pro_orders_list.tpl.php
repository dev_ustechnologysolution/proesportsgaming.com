<section class="content" style="margin-top:30px;"> 
  <?php $this->load->view('admin/common/show_message') ?> 
  <div class="box">
    <div class="box-body ordrs">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Sold By</th>
            <th>Order No.</th>
            <th>Acct#</th>
            <th>Name</th>
            <th>Shipping <br> Address</th>
            <th>Shipping <br> City</th>
            <th>Shipping <br> State</th>
            <th>Shipping <br> Country</th>
            <th>Shipping <br> ZipCode</th>
            <th>Email</th>
            <th>Phone#</th>
            <th>Processing</th>
            <th>Shipping</th>
            <th>Grand Total</th>
            <th>Payment <br> Done</th>
            <th>Date</th>
            <th>Tracking</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; foreach($list as $k => $order) {
            $sender_arr = $this->General_model->view_single_row('user_detail','user_id',$order['sold_by']);
            $receiver_arr = $this->General_model->view_single_row('user_detail','user_id',$order['user_id']);
            $wallet = ($order['wallet_deducted'] > 0) ? $order['wallet_deducted'] : '';
            $paymentdone = ($order['wallet_deducted'] > 0) ? 'Wallet : '.number_format((float)$order['wallet_deducted'], 2, '.', '').' <br>' : '';
            $paymentdone .= ($order['paypal_deducted'] > 0) ? 'Paypal : '.number_format((float)$order['paypal_deducted'], 2, '.', '') : '';
            ?>          
          <tr>
            <td style="text-align: center;"><img src="<?php echo ($order['sold_by'] == 2) ? base_url().'upload/admin_dashboard/Chevron_3_Twitch_72x72.png' : base_url().'upload/profile_img/'.$sender_arr['image']?>" width="30" height="25"></td>
            <td><?php echo $order['order_id']; ?></td>
            <td><?php echo $receiver_arr['account_no']; ?></td> 
            <td><?php echo $order['name']; ?></td> 
            <td><?php echo $order['shipping_address']; ?></td> 
            <td><?php echo $order['shipping_city']; ?></td> 
            <td><?php echo $order['shipping_state']; ?></td> 
            <td><?php echo $order['shipping_country']; ?></td> 
            <td><?php echo $order['shipping_zip_code']; ?></td> 
            <td><?php echo $order['email']; ?></td> 
            <td><?php echo $order['phone']; ?></td> 
            <td><?php echo $order['sub_total']; ?></td> 
            <td><?php echo $order['shipping_charge']; ?></td> 
            <td><?php echo $order['grand_total']; ?></td> 
            <td style="min-width: 100px;"><?php echo $paymentdone; ?></td>
            <td><?php echo date('Y-m-d', strtotime($order['created_at'])); ?></td>
            <td class="icn">
              <a class="edit-data icn_act_ordr" id="<?php echo $order['order_id'].'_'.$order['sold_by'].'_'.$order['grand_total'].'_'.$order['ref_seller_id']; ?>" action="edit_tracking_data" title="Edit Tracking ID" data-toggle="tooltip" data-placement="bottom"><i class="glyphicon glyphicon-pencil"></i><?php echo $order['tracking_id']; ?></a>
            </td> 
            <td class="icn" style="min-width: 100px;">
              <a class="btn btn-primary icn_act_ordr" title="View Order" href="<?php echo base_url().'admin/orders/orderdetails?id='.$order['order_id']; ?>" data-toggle="tooltip" data-placement="bottom"> <i class="glyphicon glyphicon-eye-open"></i></a> &nbsp;
              <?php if ($order['order_status'] == 'placed') { ?>
                <a class="btn btn-primary icn_act_ordr edit-data <?php echo ($order['tracking_id'] == null || empty($order['tracking_id'])) ? 'disabled':''; ?>" title="Order Complete" id="<?php echo $order['order_id']; ?>" data-toggle="tooltip" data-placement="bottom" action="edit_order_action_cmplt"><i class="glyphicon glyphicon-ok"></i></a> &nbsp;
                <a class="btn btn-primary icn_act_ordr edit-data" title="Order Cancel" id="<?php echo $order['order_id']; ?>" data-toggle="tooltip" data-placement="bottom" action="edit_order_action_cancel"><i class="glyphicon glyphicon-remove"></i></a>
              <?php } else { echo 'Canceled'; } ?>
            </td> 
          </tr>
          <?php }?>
        </tbody>
      </table>
    </div>
   
  </div>
</section>
<style>
.no_banner{
  width:15px;
  height:15px;
  background:red;
  border-radius:4px;
}
.yes_banner{
  width:15px;
  height:15px;
  background:green;
  border-radius:4px;
}
.myCheckbox input {
   visibility:hidden;
    position: absolute;
    z-index: -9999;
}
.myCheckbox span {
    width: 15px;
    height: 15px;
    display: block;
    background: red;
    border-radius:4px;
}
.myCheckbox input:checked + span {
    background: green;
}
</style>
<div class="modal fade  custom-width" id="modal-1">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">      
    </div>
  </div>
</div>

<script type="text/javascript">
  function saveData(e,id,title) {
  if(e.keyCode === 13){
    if (confirm('Are you sure you want to save this thing into the database?')) {
      e.preventDefault();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url('admin/orders/savedata')?>",
        data: {  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
                'id': id,
                'title' :title,
                'type':1
              },
        success: function(response){ 
          alert(response);
        }
      });
    }  
 }  
}
function viewProduct(pid){
  $.ajax({
    url : '<?php echo site_url(); ?>admin/orders/orderItemDetail?id='+pid,
    success: function(result) {
      if(result){
        $('.modal-content').html(result);
        $('#modal-1').modal('show'); 
      }
    }
  });
}
   $(document).on('click','.lobby_view',function(){
  var id = $(this).attr('id');
  if(id){
    $.ajax({
      url : '<?php echo site_url(); ?>admin/Categories/ChangeCategoryOrder/?id='+id,
      success: function(result) {
        if(result){
          $('.modal-content').html(result);
          $('#modal-1').modal('show');                              

        }
      }
    });
  }
});
$(document).on('submit','#change_lobby_order',function(e){
  e.preventDefault();
  var data = $(this).serialize();
  if(data){
    $.ajax({
      url: "<?php echo site_url(); ?>admin/Categories/UpdateCatOrder",
      data: data,
      type: 'POST',
      success: function(result) {
        window.location.href = "<?php echo site_url(); ?>admin/Categories/";       
      }
    });
  }             
});
</script>
<script>
$(document).ready(function(){
  var $checkboxes = $('td input[type="checkbox"]');
  $('input[type="checkbox"]').click(function() {
    var countCheckedCheckboxes = $checkboxes.filter(':checked').length;
    var checked_val = $(this).prop("value");
    if ($(this).prop("checked") == true) {
      var type = 'check';
    } else if ($(this).prop("checked") == false) {
      var type = 'uncheck';
    }
    $.ajax({
      url : '<?php echo site_url(); ?>admin/categories/updateIsStatus?type='+type+'&id='+checked_val,
      success: function(result) {
      }
    });
  });
});


</script>