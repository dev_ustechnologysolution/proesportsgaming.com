<section class="content">
	<section class="content">
    	<div class="row">
			<div class="col-md-12">
				<?php $this->load->view('admin/common/show_message') ?>
				<div class="box box-primary">					
					<form  action="<?php echo base_url(); ?>admin/points/update_fee_per_percentage" method="post" enctype="multipart/form-data">
						
						<table cellspacing="3" cellpadding="3">
							<tr>
								<td style="width:120px;"></td>
								<td><h4> Enter Fee Amt</h4></td>								
							</tr>
							<tr>
								<td style="width:120px;"><label for="title">Less than $5 </label></td>
								<td><input type="text" placeholder="Less Than 5" id="lessthan5" class="form-control" name="lessthan5" required="required" value="<?php if(isset($points[0]['lessthan5'])) echo $points[0]['lessthan5'];?>"></td>								
							</tr>
							<tr>
								<td><label for="title">More than $5</label></td>
								<td><input type="text" placeholder="More than 5" id="morethan5_25" class="form-control" name="morethan5_25" required="required" value="<?php if(isset($points[0]['morethan5_25'])) echo $points[0]['morethan5_25'];?>"></td>
							</tr>
							<tr>
								<td><label for="title">More than $25</label></td>
								<td><input type="text" placeholder="More than 25" id="morethan25_50" class="form-control" name="morethan25_50" required="required" value="<?php if(isset($points[0]['morethan25_50'])) echo $points[0]['morethan25_50'];?>"></td>
							</tr>
							<tr>
								<td><label for="title">More than $50</label></td>
								<td><input type="text" placeholder="More than 50" id="morethan50" class="form-control" name="morethan50" required="required" value="<?php if(isset($points[0]['morethan50'])) echo $points[0]['morethan50'];?>"></td>
							</tr>
							<tr>
								<td></td>
								<td><input type="hidden" name="id" value="<?php if(isset($points[0]['id'])) echo $points[0]['id'];?>">
								<button class="btn btn-primary" id="update_point" type="submit">Submit</button></td>								
							</tr>
						</table>						
					</form>
				</div>
			</div>
		</div>
	</section>
</section>