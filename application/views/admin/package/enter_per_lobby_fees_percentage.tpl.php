<section class="content">
	<section class="content">
    	<div class="row">
			<div class="col-md-12">
				<?php $this->load->view('admin/common/show_message') ?>
				<div class="box box-primary livelobbyfeebox">
					<form  action="<?php echo base_url(); ?>admin/points/update_fee_per_lobby_percentage" method="post" enctype="multipart/form-data" class="live_lobby_fees_point">
						<table cellspacing="3" cellpadding="3">
							<tr>
								<td style="width:320px;"><label for="title">Fee for creating game ($)</label></td>
								<td><input type="text" placeholder="Fee for creating game" class="form-control" name="fee_for_creating_game" required="required" value="<?php echo($lobby_fee[0]['fee_for_creating_game'] !='') ? $lobby_fee[0]['fee_for_creating_game'] : 0; ?>"></td>
							</tr>
							<tr>
								<td style="width:320px;"><label for="title">Fee per game (%) </label></td>
								<td><input type="text" placeholder="Fee per game" name="fee_per_game" class="form-control" name="lessthan5" required="required"  value="<?php echo($lobby_fee[0]['fee_per_game'] != '') ? $lobby_fee[0]['fee_per_game'] : 0 ; ?>"></td>
							</tr>
							<tr>
								<td style="width:320px;"><label for="tip_fees">Tip Fees (%) </label></td>
								<td><input type="text" placeholder="Tip Fees" name="tip_fees" class="form-control" required="required" value="<?php echo($lobby_fee[0]['tip_fees'] != '') ? $lobby_fee[0]['tip_fees'] : 0 ; ?>"></td>
							</tr>
							<tr>
								<td style="width:320px;"></td>
								<td>
									<input type="hidden" name="id" value="<?php if(isset($lobby_fee[0]['id'])) echo $lobby_fee[0]['id'];?>">
									<button class="btn btn-primary" id="update_point" type="submit">Submit</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
				<div class="box subsfeedivbox box-primary">
					<div class="box-header">
						<h3 class="box-title">Subscription</h3>
		            </div>
		            <div class="box-body">
			            <form  action="<?php echo base_url(); ?>admin/points/update_fee_per_lobby_percentage" method="post" enctype="multipart/form-data" class="live_lobby_fees_point">
			            	<table cellspacing="3" cellpadding="3" class="table table-striped">
				              	<tbody>
				              		<tr>
					                  <th style="width: 10px">#</th>
					                  <th>Plan</th>
					                  <th>Amount($)</th>
					                  <th>Subscription Fees($)</th>
					                </tr>
					                <tr>
					                  <td>1.</td>
					                  <td>Basic</td>
					                  <td>
					                  	<input type="text" class="form-control" name="basic_plan_amount" placeholder="Basic plan amount" value="<?php echo($lobby_fee[0]['basic_subscription_amount']) ? $lobby_fee[0]['basic_subscription_amount'] : '' ; ?>">
					                  </td>
					                  <td>
					                  	<input type="text" class="form-control" name="basic_plan_fee" placeholder="Basic plan fee" value="<?php echo($lobby_fee[0]['basic_subscription_fee']) ? $lobby_fee[0]['basic_subscription_fee'] : '' ; ?>">
					                  </td>
					                </tr>
					                <tr>
					                  <td>2.</td>
					                  <td>Epic</td>
					                  <td>
					                  	<input type="text" class="form-control" name="epic_plan_amount" placeholder="Epic plan amount" value="<?php echo($lobby_fee[0]['epic_subscription_amount']) ? $lobby_fee[0]['epic_subscription_amount'] : '' ; ?>">
					                  </td>
					                  <td>
					                  	<input type="text" class="form-control" name="epic_plan_fee" placeholder="Epic plan fee" value="<?php echo($lobby_fee[0]['epic_subscription_fee']) ? $lobby_fee[0]['epic_subscription_fee'] : '' ; ?>">
					                  </td>
					                </tr>
					                <tr>
					                  <td>3.</td>
					                  <td>Legendary</td>
					                  <td>
					                  	<input type="text" class="form-control" name="legendary_plan_amount" placeholder="Legendary plan amount" value="<?php echo($lobby_fee[0]['legendary_subscription_amount']) ? $lobby_fee[0]['legendary_subscription_amount'] : '' ; ?>">
					                  </td>
					                  <td>
					                  	<input type="text" class="form-control" name="legendary_plan_fee" placeholder="Legendary plan fee" value="<?php echo($lobby_fee[0]['legendary_subscription_fee']) ? $lobby_fee[0]['legendary_subscription_fee'] : '' ; ?>">
					                  </td>
					                </tr>
					                <tr>
					                	<td colspan="4" align="center">
					                		<input type="hidden" name="id" value="<?php if(isset($lobby_fee[0]['id'])) echo $lobby_fee[0]['id'];?>">
					                		<button class="btn btn-primary" id="update_point" type="submit">Submit</button>
											</td>
					                </tr>
					            </tbody>
				          	</table>
			          	</form>
		            </div>
		        </div>
			</div>
		</div>
	</section>
</section>