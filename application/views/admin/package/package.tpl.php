<section class="content">
  <div class="box">
	<div class="box-header">
    <a href="<?php echo base_url().'admin/points/addPackage' ?>"><button class="btn btn-primary" style="float:right;margin-right: 7px;">Add Packages</button></a></div>
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Sl No</th>
            <th>Title</th>
            <th>Description</th>
			<th>Amount</th>
            <!--<th>Points</th>-->
            <th>Fee </th>
			<th>Created Date</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; foreach($package_data as $value) {
            // $fee = $value['amount'] * ($value['percentage']/100);
          ?>
          <tr>
            <td><?php echo $i++; ?></td>
            <td style="width: 20%"><?php echo $value['point_title']?></td>
            <td><?php
              if (strlen($value['point_description']) < 100) {
                echo $value['point_description'];
              } else {
                echo substr($value['point_description'],0,100).'....';
              } ?></td>
            <td><?php echo $value['amount'];?></td>
            <td><?php echo '$ '.$value['percentage'];?></td>
			      <td><?php echo $value['created_at'];?></td>
            <td><a href="<?php echo base_url().'admin/points/packageEdit/'.$value['id'];?>"><i class="fa fa-pencil fa-fw"></i></a>|<a href="<?php echo base_url().'admin/points/delete/'.$value['id'];?>" onclick="return confirm('Are you sure want to delete?')"><i class="fa fa-trash-o fa-fw"></i></a></td>
           </tr>
          <?php }?>
        </tbody>
      </table>
    </div>
  </div>
</section>