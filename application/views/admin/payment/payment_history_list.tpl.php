<section class="content">

  <div class="box">

    <div class="box-body">

      <table id="example1" class="table table-bordered table-striped">

        <thead>

          <tr>

            <th style="display:none"></th>

            <th>Sl No</th>

            <th>Winner Name</th>

            <th>Game Name</th>

            <th>Winner Payment Email</th>

            <th>Wining Price</th>

            <th>Status</th>

            <th>Payment Date</th>

          </tr>

        </thead>

        <tbody>

          <?php $i=1; foreach($payment_history_data as $value){?>

          <tr>

            <td style="display:none"></td>

            <td><?php echo $i++; ?></td>

            <td><?php echo $value['name']?></td>

            <td><?php echo $value['game_name']?></td>

            <td><?php echo $value['receiver_email']?></td>

            <td><?php echo $value['amount'].$value['currency']?></td>

            <td><?php echo $value['payment_status']?></td>

            <td><?php echo $value['payment_date']?></td>

           </tr>

          <?php }?>

        </tbody>

      </table>

    </div>

  </div>

</section>