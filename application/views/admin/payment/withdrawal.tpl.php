<div class="adm-dashbord">
	<div class="container padd0">
		<div class="col-lg-11">
			<div class="myprofile-edit">
				<h2>Withdrawal Information</h2>
				<div class="col-lg-12">
					<?php
					$paypal_url = $this->paypal_lib->paypal_url;
					$paypal_id = $withdrawPayment['email'];
					// $paypal_url='https://www.sandbox.paypal.com/cgi-bin/webscr';
					// $paypal_id= 'vaghelamehul35-facilitator@yahoo.com';
					// $paypal_id = 'YouVsTheWorld@ProFantasyGaming.com';
					// $paypal_url='https://www.paypal.com/cgi-bin/webscr';
					// $paypal_id= 'YouVsTheWorld@ProFantasyGaming.com';
					if ($withdrawPayment['total_balance'] >= $withdrawPayment['amount_requested']) { ?>
						<form action="<?php echo site_url() ?>admin/payment/payment_paypal" name="frmPaypal" method="post" id="frmPaypal">
							<div class="form-group">
			        			<input type="hidden" name="cmd" value="_xclick">
			        		</div>
					        <div class="form-group">
				            	<input type="hidden" name="business" value="<?php echo $paypal_id ?>">
					            <input type="hidden" name="invoice" value="<?php echo $info ?>">
					            <input type="hidden" name="paypal_url" value="<?php echo $paypal_url ?>">
					            <input type="hidden" name="receiver_id" value="<?php echo $withdrawPayment['user_id']; ?>">
					        </div>
					        <div class="form-group">
					        	<label class="col-lg-3">User Name:</label>
					        	<div class="input-group col-lg-9">
					        		<label><?php echo $withdrawPayment['name']; ?></label>
					        	</div>
					        	<input type="hidden" name="item_name" value="<?php echo 'Withdrawal payment'; ?>">
					        </div>
					        <div class="form-group">
					        	<label class="col-lg-3">User Email:</label>
					        	<div class="input-group col-lg-9">
					        		<label><?php echo $withdrawPayment['email']; ?></label>
					        	</div>
					        </div>
					        <div class="form-group">
					        	<label class="col-lg-3">Amount Requested:</label>
					        	<div class="input-group col-lg-9">
					        		<label><?php echo $withdrawPayment['amount_requested']; ?></label>
					        	</div>
					        </div>
					        <div class="form-group">
					        	<label for="name" class="col-lg-3">Choose Fees Type</label>
					        		<div class="input-group col-lg-9">
                            			<select class="form-control" name="fees_type" id="fees_type">
                                    		<option id="percentage" value="percentage">10%</option>	
                                    		<option id="doller" value="doller">$5</option>	
                                    		<option id="custom" value="custom">Custom</option>	
                            			</select>
                        			</div>
                        	</div>
					        <div class="form-group custom" style="display: none">
					        	<label class="col-lg-3">Fees Percentage :</label>
					        	<div class="input-group col-lg-9">
					        		<input type="hidden" id="withdrawamount" name="withdrawamount" value="<?php echo $withdrawPayment['amount_requested']; ?>">
					        		<input type="number" min="0" step="any" class="form-control" name="withdrawpercentage" id="withdrawpercentage"  value="0" onkeypress="return (event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57))">
					        		<!-- onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46" -->
					        		<!-- e.which != 8 && e.which != 0 -->
					        	</div>
					        </div>
					        <div class="form-group">
					        	<label class="col-lg-3">Pay :</label>
					        	<div class="input-group col-lg-9">
					        		<?php $points = $this->General_model->view_data('admin_points_percentage',array('id'=>'1')); ?>
					        		<?php $pay_amount = $withdrawPayment['amount_requested']-($withdrawPayment['amount_requested'] * 10/100); ?>
					        		<input type="number" step="any" min="0" class="form-control" name="amount" id="amount" value="<?php echo $pay_amount ?>" required onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46">
					        	</div>
					        </div>
					        <div class="form-group">
					        	<input type="hidden" name="currency_code" value="USD">
					        </div>
					        <div class="form-group">
					        	<input type="hidden" name="cancel_return" value="<?php echo base_url('admin/payment/failure'); ?>">
								<input type="hidden" name="no_shipping" value="1">
					        </div>
					        <div class="form-group">
					        	<input type="hidden" name="return" value="<?php echo base_url().'admin/payment/success_withrawal?receiver_id='.$withdrawPayment['user_id']; ?>" />
					        </div>
					        <input type="hidden" name="notify_url" value="" />
					        <input type="hidden" name="custom" value='<?php echo $this->session->userdata('admin_user_id'); ?>'>
					        <div class="form-group">
					        	<p>To complete this payment, Please pay with Paypal</p>
					        </div>
					        <div class="form-group" style="width:115px; margin:0 auto;">
					        	<input type="image" name="submit" src="https://www.paypalobjects.com/webstatic/en_US/btn/btn_pponly_142x27.png" alt="PayPal - The safer, easier way to pay online">
					        </div>
					    </form>
					<?php } else { ?>
						<div>
				    		<div class="form-group">
				    			<label class="col-lg-3">User Name:</label>
				    			<div class="input-group col-lg-9">
				    				<label><?php echo $withdrawPayment['name']; ?></label>
				    			</div>
				    			<input type="hidden" name="item_name" value="<?php echo 'Withdrawal payment'; ?>">
				    		</div>
				    		<div class="form-group">
				    			<label class="col-lg-3">User Email:</label>
				             	<div class="input-group col-lg-9">
					            	<label><?php echo $withdrawPayment['email']; ?></label>
					            </div>
					        </div>
					        <div class="form-group">
			          			<label class="col-lg-3">Amount Requested:</label>
					          	<div class="input-group col-lg-9">
					          		<label><?php echo $withdrawPayment['amount_requested']; ?></label>
					          	</div>
					        </div>
			          		<div class="form-group">
			          			<p style="font-size: 18px;color: red;">Insufficient Balance. Account you try to transaction from does not have enough balance. <br>Required <b><?php echo $withdrawPayment['amount_requested']; ?></b> and Got: <b><?php echo $withdrawPayment['total_balance']; ?></b></p>
			          			<a href="<?php echo base_url('admin/game/withdrawal');?>"><button type="button" class="btn btn-primary">Back</button></a>
			          		</div>
			          	</div>
			        <?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	// $('#withdrawpercentage').keypress(function(e){
	// 	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
 //        //display error message			
	// 		alert('Enter only Digits ');
	// 		return false;
	// 	} else {
			
	// 	}
	// })
	
	// $('#amount').focus(function(e){
	// 	final_amount();
	// });
	$('form[name="frmPaypal"]').submit(function () {
		var withdrawpercentage = parseFloat($('#withdrawpercentage').val());
		if (withdrawpercentage > 100) {
			alert('Please enter valid Fees Percentage');
			$('#amount').val('');
			return false;
		}
	})
	$('select').on('change', function()
	{
    	var fees_type=this.value;
    	if(fees_type=="percentage")
    	{
    		$('.custom').hide();
    		$('#withdrawpercentage').attr('required', false);
    		var withdrawamount = parseFloat($('#withdrawamount').val()).toFixed(2);
			var final_amount =  parseFloat(withdrawamount)-(withdrawamount * 10/100).toFixed(2);
			$('#amount').val(final_amount);

    	}
    	else if(fees_type=="doller")
    	{
    		$('.custom').hide();
    		$('#withdrawpercentage').attr('required', false);
    		var withdrawamount = parseFloat($('#withdrawamount').val()).toFixed(2);
			var final_amount =  parseFloat(withdrawamount)-(5).toFixed(2);
			$('#amount').val(final_amount);

    	}
    	else
    	{
    		$('.custom').show();
    		$('#withdrawpercentage').attr('required', true); 
    		var withdrawamount = parseFloat($('#withdrawamount').val()).toFixed(2);
    		$('#amount').val(withdrawamount);
    		
    	}
	});
	$('#withdrawpercentage').focusout(function(e){
		var withdrawpercentage = parseFloat($('#withdrawpercentage').val());
		var withdrawamount = parseFloat($('#withdrawamount').val()).toFixed(2);
		var final_amount =  parseFloat(withdrawamount)-(withdrawamount * withdrawpercentage/100).toFixed(2);
		$('#amount').val(final_amount);
	});
</script>