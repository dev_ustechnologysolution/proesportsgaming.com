<section class="content">
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('admin/common/show_message') ?>
      <form action="<?php echo base_url(); ?>admin/products/productUpdate" method="post" enctype="multipart/form-data" class="admin_game_edit_form" formaction="update">
        <div class="box box-primary"> 
          <h4 class="ml20">Product Basic Details</h4>
          <input type="hidden" id="product_id" name="id" value="<?php echo $product_data[0]['id']; ?>">
           <input type="hidden" id="addproduct" value="0">
          <div class="box-body">
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="name">Title</label>
                <input required type="text" required id="name" class="form-control" name="name" placeholder="Please enter product name" value="<?php echo $product_data[0]['name']; ?>"/>
              </div>
              <div class="form-group col-md-3">
                <label for="name">Select Main Category</label>
                <select required class="form-control" name="category_id" id="category_id">
                  <option value="">Please Select Category</option>
                  <?php foreach ($category_list as $key => $value) {
                    $arr_category = explode(",", $product_data[0]['category_id']);
                    if ($value['parent_id'] == 0) { ?>
                      <option <?php echo ($arr_category[0] == $value['id']) ? 'selected' : '';?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                    <?php }
                  } ?>
                </select>
              </div>  
              <div class="form-group col-md-3">
                <label for="name">Select Sub Category</label>
                <select required class="form-control" name="subcategory_id" id="sub_cat">
                  <option value="">Please Select Category</option>
                  <?php foreach ($category_list as $key => $value) {
                    $arr_category = explode(",", $product_data[0]['category_id']);
                    if ($value['parent_id'] == $arr_category[0]) { ?>
                      <option <?php echo ($arr_category[1] == $value['id']) ? 'selected' : '';?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                    <?php }
                  } ?>
                </select>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="description">Description</label>
                <textarea required placeholder="Please enter product Description" class="form-control" rows="6" name="description"><?php echo $product_data[0]['description']; ?> </textarea> 
              </div>
            </div>
          </div>
        </div> 
        <div class="box box-primary"> 
          <h4 class="ml20">Product Price Details</h4>
          <div class="box-body">   
            <div class="form-row">
              <div class="form-group col-md-4">
                <label>Price</label>
                <input required onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46" type="text" name="price" class="form-control" value="<?php echo $product_data[0]['price']; ?>" placeholder="Please Enter Price">
              </div>
              <div class="form-group col-md-4">
                <label>Qty</label>
                <input required type="number" name="qty" class="form-control" placeholder="Please Enter Qty" value="<?php echo $product_data[0]['qty']; ?>">
              </div>
              <div class="form-group col-md-4">
                <label>Tax Amount</label>
                <input onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46" name="fee" required type="text" class="form-control" placeholder="Please Enter Fee" value="<?php echo $product_data[0]['fee']; ?>">
              </div>
              <!--  <div class="form-group col-md-3">
              <label>Int Fee</label>
              <input onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46" name="int_fee" required type="text" class="form-control" placeholder="Please Enter Int Fee" value="<?php // echo $product_data[0]['int_fee']; ?>"></div>  -->
            </div>
          </div>
        </div> 
        <div class="box box-primary"> 
          <h4 class="ml20">Product Attributes Details</h4>
          <div class="box-body"> 
            <div class="form-row">
              <div class="form-group col-md-4">
                <label>Is 18 YRS</label>
                <select name="is_18" class="form-control">
                  <option <?php echo ($product_data[0]['is_18'] == 0) ? 'selected' : '';?> value="0">No</option>
                  <option <?php echo ($product_data[0]['is_18'] == 1) ? 'selected' : '';?> value="1">Yes</option>
                </select>
              </div>
              <div class="form-group col-md-4">
                <?php if ($product_data[0]['type'] != 1) { ?>
                  <label>Upload Game/Music</label><small>[ Upload only .zip,.rar,.7zip file ]</small>
                  <input type="file" class="form-control" accept=".zip,.rar,.7zip" name="game_file"/>
                <?php } ?>
              </div>
              <div class="col-md-4"></div>
            </div>
            <div class="form-row">
              <div class="col-md-12 product_customize_adminside">
                <div class="product_is_customizable">
                  <label class="switch common_switch"> Is Customizable  <input type="checkbox" name="is_customizable" <?php echo ($product_data[0]['is_customizable'] == 1) ? 'checked' : ''; ?>><span class="slider round"></span> </label>
                </div>
                <div class="product_is_customizable_opt_div" style="<?php echo ($product_data[0]['is_customizable'] == 1) ? 'display: block' : 'display: none'; ?>">
                  <div class="clearfix form-group">
                    <label>Title</label>
                    <input  type="text" name="customizable_tag" class="form-control" value="<?php echo (!empty($product_data[0]['customizable_tag'])) ? $product_data[0]['customizable_tag'] : 'ENTER TAG'; ?>" placeholder="Please Enter Tag" required="">
                  </div>
                  <div class="clearfix form-group">
                    <label>Description</label>
                    <input type="text" name="customizable_tag_description" class="form-control" value="<?php echo (!empty($product_data[0]['customizable_tag_description'])) ? $product_data[0]['customizable_tag_description'] : 'ENTER TAG DESCRIPTION'; ?>" placeholder="Please Enter Tag" required="">
                  </div>
                </div>
              </div>
            </div>
            <?php if ($product_data[0]['type'] == 1) { ?>
              <div class="form-row">
                <?php if (!empty($attribute_list)) {
                  $arr_selected_options = array_column($product_attribute_data, 'option_id');
                  foreach($attribute_list as $key => $v) { $arr_attr = explode('_',$key); ?> 
                    <div class="form-group col-md-12">
                      <label style="display: block;"><?php echo $arr_attr[0]; ?> </label>
                      <?php foreach($v as $v1) {
                        if ($arr_attr[0] == $v1->title) {
                          if ($v1->type == 2) { ?>
                            <input cust-name="<?php echo $arr_attr[0];?>" name="attr_id[<?php echo $v1->attr_id.'_'.$v1->option_id;?>]" <?php echo (in_array($v1->option_id, $arr_selected_options)) ? 'checked' : '';  ?> type="radio" id='<?php echo $v1->attr_id.'_'.$v1->option_id; ?>' value="<?php echo $v1->option_label; ?>" image_upload="<?php echo ($v1->image_upload == 1) ? 'true' : 'false'; ?>" onclick="$(this).siblings().prop('checked', false);" />
                            <label for='<?php echo $v1->attr_id.'_'.$v1->option_id; ?>' style="font-size: 20px; text-transform: uppercase;"><?php echo $v1->option_label; ?>  </label>
                          <?php } else { ?>
                            <input cust-name="<?php echo $arr_attr[0];?>" name="attr_id[<?php echo $v1->attr_id.'_'.$v1->option_id;?>]" <?php echo (in_array($v1->option_id, $arr_selected_options)) ? 'checked' : '';  ?> type="checkbox" id='<?php echo $v1->attr_id.'_'.$v1->option_id; ?>' class='chk-btn' image_upload="<?php echo ($v1->image_upload == 1) ? 'true' : 'false'; ?>" value="<?php echo $v1->option_label; ?>"/>
                            <label for='<?php echo $v1->attr_id.'_'.$v1->option_id; ?>'><?php echo $v1->option_label; ?></label>
                          <?php } ?>
                        <?php } ?>
                      <?php } ?>
                    </div>
                  <?php } ?>
                <?php } ?>
              </div>
            <?php } ?>
            <div class="form-row" id="dynamic_attr">
              <?php if (count($attribute_list) > 0) {
                $arr_selected_options = array_column($product_attribute_data, 'option_id');
                foreach($attribute_list as $key => $v) {
                  $arr_attr = explode('_',$key); ?> 
                  <?php foreach($v as $v1) {
                    // if (strtolower($v1->title) == 'color' && (in_array($v1->option_id, $arr_selected_options))) {
                    if ($v1->image_upload == 1 && in_array($v1->option_id, $arr_selected_options)) { ?>
                      <div class="form-group col-md-4" id="remove<?php echo $v1->attr_id.'_'.$v1->option_id; ?>">
                        <label for='<?php echo $v1->attr_id.'_'.$v1->option_id; ?>'><?php echo $v1->option_label; ?></label> 
                        <input  multiple type="file" id='<?php echo 'img_'.$v1->attr_id.'_'.$v1->option_id; ?>' name="attr_image[<?php echo $v1->attr_id.'_'.$v1->option_id;?>][]" class='form-control' onchange="preview_image1('<?php echo 'img_'.$v1->attr_id.'_'.$v1->option_id; ?>',1,'image_<?php echo $v1->option_label; ?>');"/>
                        <div id="image_<?php echo $v1->option_label; ?>" class="image_<?php echo $v1->option_label; ?>">
                          <?php 
                          foreach($product_attribute_data as $option_data) {

                            if($option_data['option_id'] == $v1->option_id) {
                              $arr_image = explode(",", $option_data['option_image']);
                              $j = 1;
                              $last_key = end(array_keys($arr_image));

                              foreach($arr_image as $k => $vimg) {
                                if($vimg != '') {
                                  $result .= $v1->option_id.'_'.$k.',';
                                   ?>
                                  <div style="margin:10px;display:inline-block;" id="removeimg_<?php echo $v1->option_id.'_'.$k; ?>">
                                    <input style="vertical-align:top;margin-right:10px;" type="hidden" name="is_defult" class="is_defult">
                                    <span>
                                      <img style="border-radius: 12px 12px 0px 0px;" width="210" height="210" src="<?php echo base_url().'upload/products/'.$vimg; ?>">
                                      <span style="cursor: pointer;" class="remove_img" onclick="remove_image1('removeimg_<?php echo $v1->option_id.'_'.$k; ?>',1,'<?php echo $option_data['id']; ?>');">
                                        <i class="fa fa-trash fa-fw"></i>Remove Image
                                      </span>
                                    </span>
                                  </div>
                                <?php }
                                $j++;
                              }
                            }
                          } ?>
                        </div>
                      </div>  
                    <?php } ?>
                  <?php } ?>
                <?php }
              } ?>
              <input type="hidden" id="old_attr_image" name="old_attr_image" value="<?php echo $result; ?>">
            </div>
          </div>
        </div>
        <div class="box box-primary"> 
          <h4 class="ml20">Product Images Details</h4>
          <div class="box-body">
            <div class="form-row">
              <div class="form-group col-md-6" style="display:none;">
                <label>Upload Product Main Image</label>
                <input type="file" class="upload_file1 form-control" id="upload_file1" name="main_image" onchange="preview_image1('upload_file1',2,'image_preview1');"/>  
                <div id="image_preview1">
                  <?php if (count($product_image_data) > 0) {
                    foreach ($product_image_data as $key => $single_img) {
                      if ($single_img['is_default'] == 1) { ?>
                        <div style='margin:10px;display:inline-block' id="removeupload_file1">
                          <input style='vertical-align:top;margin-right:10px;' type='hidden' name='is_defult' class='is_defult'>
                          <img  width='250' src='<?php echo base_url().'upload/products/'.$single_img['image']?>'>
                        </div>
                      <?php }
                    }
                  } ?>
                </div>
              </div>
              <div class="form-group col-md-12">
                <label>Upload Product Images</label>
                <input  type="file" class="form-control upload_file" id="upload_file" name="gallery_image[]" onchange="preview_image1('upload_file',2,'image_preview');" multiple/> 
                <div id="image_preview">
                  <?php if (count($product_image_data) > 0) {
                    foreach ($product_image_data as $key => $single_img) {
                      // if($single_img['is_default'] == 0) {
                      $result1 .= $single_img['id'].','; ?>
                      <div style='margin:10px;display:inline-block' id="remove_<?php echo $single_img['id'];?>">
                        <input style='vertical-align:top;margin-right:10px;' type='hidden' name='is_defult' class='is_defult'>
                        <span>
                          <img  width='250' src='<?php echo base_url().'upload/products/'.$single_img['image']?>'>
                          <span style="cursor: pointer;" class="remove_img" onclick="remove_image('remove_<?php echo $single_img['id'];?>',2,<?php echo $single_img['id']; ?>);"><i class="fa fa-trash fa-fw"></i>Remove Image</span>
                        </span>
                      </div>
                    <?php }
                  } ?>
                  <input type="hidden" name="old_gallery_image" id="old_gallery_image" value="<?php echo $result1; ?>">
                </div>
              </div>
            </div>
          </div>
          <div class="box-footer" align="center">
            <button class="btn btn-primary" type="submit" id="edit_game_update">Submit</button>
          </div>
        </div>    
      </form>
    </div>
  </div>
</section>
<script type="text/javascript">
  $('.product_is_customizable input[name="is_customizable"]').change(function () {
    ($(this).is(':checked')) ? $('.product_is_customizable_opt_div').show('slow').find('input').attr('required','') : $('.product_is_customizable_opt_div').hide('slow').find('input').removeAttr('required');
  });
  $('#category_id').change(function(){
    var category_id = $('#category_id').val();
    if (category_id != '') {
     $.ajax({
      url:"<?php echo base_url(); ?>admin/products/fetchSubcat",
      method:"POST",
      data:{category_id:category_id},
      success:function(data) {
       $('#sub_cat').html(data);     
      }
     });
    } else {
     $('#sub_cat').html('<option value="">Select Sub-Category</option>');   
    }
 });
// function preview_image1() 
// {
//  var total_file=document.getElementById("upload_file").files.length;
//  for(var i=0;i<total_file;i++)
//  {
//   $('#image_preview').append("<div style='margin:10px;display:inline-block'><input style='vertical-align:top;margin-right:10px;' type='hidden' name='is_defult' class='is_defult'><img  width='250' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
//  }
// }
function preview_image1(id,type,upload_div) 
{
    var total_file=document.getElementById(id).files.length;
    
    for(var i=0;i<total_file;i++)
    {
        var file_data = event.target.files[i];
        var form_data = new FormData();
        var product_id = $('#product_id').val();
        form_data.append('file', file_data);
        form_data.append('type', type);
        form_data.append('attr', id);
        form_data.append('product_id', product_id);
        $.ajax({
            url: base_url + "admin/products/upload_file",
            dataType: 'json', 
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (response) {               
                $('#'+upload_div).append("<div style='margin:10px;display:inline-block' id='"+response.id+"'><input style='vertical-align:top;margin-right:10px;' type='hidden' name='is_defult' class='is_defult'><span ><img  style='border-radius: 12px;' width='210' height='210' src='"+response.name+"'><span style='cursor:pointer;' class='remove_img'  onclick='remove_image("+response.id+","+type+","+response.id+");'><i class='fa fa-trash fa-fw'></i>Remove Image</span></span></div>");
                
            },
            error: function (response) {
                $('#msg').html(response); // display error response from the server
            }
        });
    }
  }
function remove_image1(id,type,img_name){
  $.ajax({
        url: base_url + "admin/products/delete_file?id="+img_name+"&type="+type,
        type: 'get',
        success: function (response) { 
            $('#'+id).remove();
            location.reload();
        },
        
    }); 
    // $('#'+id).remove();
    // if(type == 1){
    //     var str = $('#old_attr_image').val();
    //     str = str.replace(img_name+',', '');
    //     $('#old_attr_image').val(str);
    // } else {
    //     var str = $('#old_gallery_image').val();
    //     str = str.replace(img_name, '');
    //     $('#old_gallery_image').val(str);
    // }
    
   
  }

</script>
<style type="text/css">
.ml20 {
    margin-left: 20px;
    font-weight: bold;
}
input.chk-btn {
display: none;
}
input.chk-btn + label {
  border: 1px solid #fff;
  /*background: ghoswhite;*/
  background: red;
  color: #fff;
  padding: 5px 8px;
  cursor: pointer;
  border-radius: 5px;
}
input.chk-btn:not(:checked) + label:hover {
  box-shadow: 0px 1px 3px;
}
input.chk-btn + label:active,
input.chk-btn:checked + label {
  box-shadow: 0px 0px 3px inset;
  /*background: #eee;*/
  background: green;
  color: #fff;
}
.remove_img{
    display: block;
    color: #fff;
    background: #2c93eb;
    text-align: center;
    padding: 4px 0px;
    border-radius: 0px 0px 12px 12px;
}
</style>