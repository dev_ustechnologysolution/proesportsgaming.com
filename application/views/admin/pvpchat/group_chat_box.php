<section class="body-middle innerPage chatbox_main_div">
    <div class="row chat-box_fronside pvp_chatbox_main_row">
      <div class="col-md-12 col-sm-12 col-xs-12 " align="center">
        <div id="message-container_pvp" class="xwb-message-container pvp_msgbox-message-container">
                <div class="chat_logo_div" align="center"><img src="<?php echo base_url(); ?>assets/frontend/images/chat_top.png"></div>
               <div class="row overflow_manage">
               <div class="col-sm-10">
               <div id="message-inner" class="message-inner">

               </div>
               </div>
               <div class="col-sm-2 add_adminside_chat banner_on_box">
               </div>
               <div class="col-sm-12 sponsered_by_logo">
               </div>
               </div>
             
               
               <div class="msg-input input-group col-sm-11 msgbox_at_adminside">
                    <textarea id="message-input" class="form-control message-input message-input-admin-side" style="resize:none;" rows="4" name="message-input"></textarea>
                    <input type="hidden" name="user_id" class="user_id" id="user_id" value="<?php echo $CI->chatsocket->current_user; ?>">
                   <input name="ci" class="ci" id="ci" value="" type="hidden" />
                  <div class="input-group-btn message_send_div">
                      <button class="btn btn-success" id="send-message-to-pvp-player-from-admin">Send</button>
                    </div>
                </div>
                
                <div class="col-sm-12 msgbox_footer call_admin_link">
                  <div class="group_name"><span style="display:none" id="groupid"></span>
                    <h3 class="conversation-name"></h3>
                  </div>
                 </div>
         </div>
      </div>
    </div>
</section>