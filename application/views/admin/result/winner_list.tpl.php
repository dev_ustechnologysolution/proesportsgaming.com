<div class="col-sm-12">
	<div class="pull-right" >
		<a href="javascript:void(0);" class="bluk_delete" > Delete </a> 		 
	</div>
</div>
<div class="clearfix"></div>
<section class="content">
	<div class="box">
		<div class="box-body">
			<table id="example1" class="table table-bordered table-striped">
				<thead>
					<tr>
						<?php if($this->session->userdata['admin_user_role_id'] == 1) { ?>
						<th><input type="checkbox" id="select_all"/></th>
						<?php } ?>
						<th>Player Name</th>
						<th>Tag</th>
						<th>Game Name</th>
						<th>Total Price</th>
						<th>Phone Number</th>
						<th>Location</th>
						<!-- <th>Payment</th>
						<th>Message</th> -->
						<th>Date</th> 
						<?php if($this->session->userdata['admin_user_role_id'] == 1) { ?>
						<th>Action</th>
						<?php } ?>
					</tr>
				</thead>
				<tbody>
				<?php foreach($list as $value){?>
				<tr>
					<?php if($this->session->userdata['admin_user_role_id'] == 1) { ?>
					<td><input type="checkbox" class="check_user" name="check_list[]" value="<?php echo $value['id']; ?>"></td>
					<?php } ?>
					<td><?php echo $value['name']?></td>
					<td><?php echo $value['device_id']?></td>
					<td><?php echo $value['game_name']?></td>
					<td><?php echo CURRENCY_SYMBOL.($value['price'] + $value['price']);?></td>
					<td><?php echo $value['number']?></td>
					<td><?php echo $value['location']?></td>
					<!-- <td><a href="<?php echo base_url().'admin/Payment/index/'.$value['winner_id'].'/'.$value['id'] ?>"><button class="btn btn-primary" <?php if($value['flag']==1) echo 'disabled="disabled"' ?>>Pay</button></a></td>
					<td><?php if($value['flag']==0) { echo '<p style="color:red">payment is due.</p>'; } else { echo '<p style="color:green">You have already paid</p>';} ?></td> -->
					<td> <?php echo $value['win_lose_deta']; ?> </td>
					<?php if($this->session->userdata['admin_user_role_id'] == 1) { ?>
					<td>
					<!--<a href="<?php echo base_url().'admin/Winner/userEdit/'.$value['winner_id'];?>"><i class="fa fa-pencil fa-fw"></i></a>-->
					<a href="<?php echo base_url().'admin/Winner/delete/'.$value['id'];?>" id="<?php echo $value['id'] ?>" class="list_delete"><i class="fa fa-trash-o fa-fw"></i></a>
					</td>
					<?php } ?>
				</tr>
				<?php }?>
				</tbody>
			</table>
		</div>
	</div>
</section>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script> $.validate();</script>
<!-- Modal 1 (Basic)-->
<div class="modal fade  custom-width" id="modal-1">
	<div class="modal-dialog" style="width: 50%;">
		<div class="modal-content">
		</div>
	</div>
</div>

<script>
$(document).on('click','.list_delete',function(e){
	e.preventDefault();
	var id = $(this).attr('id');
	//var id = $(this).attr('href');
	if(id){
		$.ajax({
			url : '<?php echo site_url(); ?>admin/FrontUser/security_pass/'+id,
			success: function(result) {
				if(result){
					$('.modal-content').html(result);
					$('#modal-1').modal('show');															
				}
			}
		})
	}
})

$("#select_all").change(function(){  
	$(".check_user").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
});


$(document).on('submit','#security_pass',function(e){
	e.preventDefault();
	var data = $(this).serialize();
	var action_value = $('#action_value').val();
	if(data){
		$.ajax({
			url: "<?php echo site_url(); ?>admin/FrontUser/get_reset_data",
			data: data,
			type: 'POST',
			success: function(result) {
				if(result == 'success'){
					var action_url = $('#'+action_value).attr('href');
					window.location.href = action_url;
					$('#modal-1').modal('hide');
				} else {
					$('.modal-content').html(result);
					$('#modal-1').modal('show');				
				}
			}
		});
	}							
})


$(document).on('click','.bluk_delete',function(e){
	e.preventDefault();
	var val = [];
	$('.check_user:checkbox:checked').each(function(i){
		val[i] = $(this).val();
	});
		
	var id = val;
	if(id != '') {
	//var id = $(this).attr('href');
	if(id){
		$.ajax({
			url : '<?php echo site_url(); ?>admin/winner/security_pass_bluk/'+id,
			success: function(result) {
				if(result){
					$('.modal-content').html(result);
					$('#modal-1').modal('show');															
				}
			}
		})
	}
	}
})

$(document).on('submit','#security_pass_bluk',function(e){
	e.preventDefault();
	var data = $(this).serialize();
	var action_value = $('#action_value').val();
	if(data){
		$.ajax({
			url: "<?php echo site_url(); ?>admin/winner/get_reset_data",
			data: data,
			type: 'POST',
			success: function(result) {
				if(result == 'success'){
					$('#modal-1').modal('hide');
					window.location.href = "<?php echo site_url(); ?>admin/winner/";
				} else {
					$('.modal-content').html(result);
					$('#modal-1').modal('show');				
				}
			}
		});
	}							
})
</script>

