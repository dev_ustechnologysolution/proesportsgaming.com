<section class="content">
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('admin/common/show_message') ?>
        <div class="box box-primary">
               
          <form  action="<?php echo base_url(); ?>admin/screenshot/insertScreenshot" method="post" enctype="multipart/form-data">
            <div class="box-body">

              <div class="form-group">
                <label for="name">Name</label>
                <input type="text" placeholder="Name" id="name" class="form-control" name="name" required="required">
              </div>
              
              <div class="form-group">
                <label for="description">Short Description</label>
                <textarea class="form-control" id="description" placeholder="description" name="description"></textarea>
              </div>

              <div class="form-group">
                <label for="file">Image</label>
                <input type="file" class="form-control" id="file" placeholder="Image" name="image">
              </div>
              <div style="color:red;"><?php echo $this->session->flashdata('msg'); ?></div>
            </div>

            <div class="box-footer">
              <button class="btn btn-primary" type="submit">Submit</button>
            </div>
          </form>
      </div>
    </div>
  </div>
</section>