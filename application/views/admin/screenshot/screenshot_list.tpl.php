<section class="content">

  <div class="box">
    
    <div class="box-header">  
      <a href="<?php echo base_url().'admin/screenshot/addScreenshot' ?>"><button class="btn pull-right btn-primary btn-xl">Add ScreenShot</button></a>
    </div>
    <?php $this->load->view('admin/common/show_message'); ?>

    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th style="display:none"></th>
            <th>Sl No</th>
            <th>Name</th>
            <th>Description</th>
            <th>Image</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; foreach($list as $value){?>
          <tr>
            <td style="display:none"></td>
            <td><?php echo $i++; ?></td>
            <td><?php echo $value['game_name']?></td>
            <td><?php echo $value['description']?></td>
            <td><img src="<?php echo base_url().'upload/screenshot/'. $value['image'] ?>" height="80" width="100"></td>
            
            <td><a href="<?php echo base_url().'admin/screenshot/screenshotEdit/'.$value['id'];?>"><i class="fa fa-pencil fa-fw"></i></a>|<a href="<?php echo base_url().'admin/screenshot/delete/'.$value['id'];?>" onclick="return confirm('are you sure delete')"><i class="fa fa-trash-o fa-fw"></i></a></td>
           </tr>
          <?php }?>
        </tbody>
      </table>
    </div>
  </div>
</section>