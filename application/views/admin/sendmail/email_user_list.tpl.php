<section class="content">

  <div class="box">

    <div class="box-header">
  
      <a href="<?php echo base_url().'admin/Sendmail/sendBulkEmail'; ?>"><button class="btn pull-right btn-primary btn-xl">Send Bulk Mail</button></a>
    </div>

    <?php $this->load->view('admin/common/show_message') ?>

    <div class="box-body">

      <table id="example1" class="table table-bordered table-striped">

        <thead>

          <tr>

            <th>Sl No</th>

            <th>Title</th>

            <th>User Name</th>
            <th>User Email</th>
            <th>Status</th>
            <th>Date</th>
          </tr>

        </thead>

        <tbody>

          <?php $i=1; 
          foreach($list as $value){ ?>
          <tr>

            <td><?php echo $i++; ?></td>

            <td><?php echo $value['template_title']; ?></td>
            
            <td><?php echo $value['name']; ?></td>
            <td><?php echo $value['email']; ?></td>
            <td><?php echo ($value['status'] == 1) ? 'Success' : 'Fail'; ?></td>
            <td><?php echo $value['date']; ?></td>
           </tr>

          <?php } ?>

        </tbody>

      </table>

    </div>

  </div>

</section>
