<section class="content">
   <div class="box">
      <?php $this->load->view('admin/common/show_message') ?>
      <div class="box-body">
      <form class="form-horizontal form" action="">
         
         <div class="form-group required ">
            <label class="control-label col-sm-2" for="template">Select Email Template </label>
            <div class="col-sm-8"> 
               <select required id="template_id" name="template_id" class="form-control">
                <option value="">Select Email Template</option>
                 <?php if(count($template_list) > 0) { foreach($template_list as $template){ ?>
                 <option value="<?php echo $template['id'].'_'.$template['title'];?>"><?php echo $template['title']?></option>
                 <?php } } ?>
               </select>
            </div>
         </div>
        
         <div class="form-group"> 
            <div class="col-sm-offset-2 col-sm-10">
               <button type="submit" class="btn btn-primary send_mail" class="btn pull-left btn-primary btn-xl">Submit</button>
            </div>
         </div>
         </form>
      </div>
   </div>
</section>
<div class="modal fade  custom-width" id="modal-pw">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">
      
    </div>
  </div>
</div>
<script>
$("form").submit( function(e) {
e.preventDefault();     
   $.ajax({
      url : '<?php echo site_url(); ?>admin/Sendmail/send_mail_password_edit',
      success: function(result) {
        if(result){
           $('.modal-content').html(result);
           $('#modal-pw').modal('show');                              
        }
      }
   }) 
});
   
$(document).on('submit','#send_mail_mdl',function(e)
{
    e.preventDefault();    
    var template_id = $('#template_id').val();
    var security_password = $('#security_password').val();
  $.ajax({
    url: "<?php echo base_url();?>admin/sendmail/send_mail_password_change",
    data: {'security_password' : security_password,'template_id' : template_id},
    type: 'POST',
    success: function(result) {
      if(result == 'true'){          
        location.reload();       
      }
    }
  });           
});
</script>
