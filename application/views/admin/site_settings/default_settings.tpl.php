<?php $this->load->view('admin/common/show_message'); ?>
<div class="col-sm-12 cover_page_top_div" style="margin-bottom: 25px;">
  <div class="pull-right">
    <a class="btn btn-info" onclick="location.reload()"> Refresh </a>
  </div>
</div>
<section class="content">
  <div class="col-sm-12 padd0">
    <div class="box box-primary">
      <form action="<?php echo base_url(); ?>admin/Default_Settings/set_default_values" method="POST" style="padding: 30px;">
        <div class="form-group">
          <label>Default Stream Recording Limit</label>
          <input type="text" class="form-control" name="default_stream_record_limit" required value="<?php echo $default_stream_record_limit['option_value'];?>">
        </div>
        <div class="form-group">
          <label>Default Lobby Creation Limit</label>
          <input type="text" class="form-control" name="default_lobby_creation_limit" required value="<?php echo $default_lobby_creation_limit['option_value'];?>">
        </div>
        <div class="form-group">
          <label>Default Lobby Join Limit</label>
          <input type="text" class="form-control" name="default_lobby_join_limit" required value="<?php echo $default_lobby_join_limit['option_value'];?>">
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary" id="update_point">Submit</button>
        </div>
      </form>
    </div>
  </div>
  <div class="col-sm-6"> </div>
</section>