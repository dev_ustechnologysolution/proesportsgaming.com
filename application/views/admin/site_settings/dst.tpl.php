<?php $this->load->view('admin/common/show_message'); ?>
<div class="col-sm-12 cover_page_top_div" style="margin-bottom: 25px;">
  <div class="pull-left status_button hide">
    <label class="switch"><input type="checkbox" name="global_dst_option" <?php echo (!empty($global_dst_option) && $global_dst_option['option_value'] == 'on') ? 'checked' : ''; ?> class="global_dst_option_chk"><span class="slider round"></span></label>
  </div>
  <div class="pull-right">
    <a class="btn btn-info" onclick="location.reload()"> Refresh </a>
  </div>
</div>
<section class="content">
  <div class="col-sm-6 padd0">
    <div class="box box-primary dst_hourbox" <?php echo (!empty($global_dst_option) && $global_dst_option['option_value'] == 'on') ? 'style="display : block;"' : 'style="display : none;"'; ?>>
      <form action="<?php echo base_url(); ?>admin/daylight_saving/global_dst_hour_change" method="POST" style="padding: 30px;">
        <div class="form-group">
          <label>Day Light Saving Hour</label>
          <select class="form-control" name="global_dst_hour" required>
            <!-- <option value="">Select Hour</option> -->
            <?php
            $array = array('-1' => "Standard Time",'+1' => "Daylight Savings Time");
            foreach ($array as $key => $arr) {
              $checked_val = $key == $global_dst_hour['option_value'] ? 'selected' : '';
              echo '<option value="'.$key.'" '.$checked_val.'>'.$arr.'</option>';
            } ?>                                                      
          </select>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary" id="update_point">Submit</button>
        </div>
      </form>
    </div>
  </div>
  <div class="col-sm-6"> </div>
</section>
<script>
  $('.global_dst_option_chk').change(function(e){
    e.preventDefault();
    var global_dst_option = 'off';
    $('.dst_hourbox').hide();
    if ($(this).is(':checked')) {
      global_dst_option = 'on';
      $('.dst_hourbox').show();
    }
    if(global_dst_option){
      $.ajax({
        url: "<?php echo base_url();?>admin/daylight_saving/global_dst_opt_change",
        data: {global_dst_option:global_dst_option},
        type: 'POST',
        beforeSend: function() { admin_loader_show(); },
        success: function() {
          admin_loader_hide();
        }
      });
    }
  });
</script>

<!-- Modal 1 (Basic)-->
<!-- <div class="modal fade  custom-width" id="modal-pw">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">
      
    </div>
  </div>
</div>
 -->