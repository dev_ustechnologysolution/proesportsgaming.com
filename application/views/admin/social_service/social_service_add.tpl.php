<section class="content">
	<div class="row">
		<div class="col-md-12">
			<?php $this->load->view('admin/common/show_message') ?>
			<div class="box box-primary">
				<form  action="<?php echo base_url(); ?>admin/Social_service/add_social_service" method="post" enctype="multipart/form-data">
					<div class="box-body">
						<div class="form-group">
							<label for="name">Social Service</label>
							<input type="text" placeholder="Social Service Name" id="name" class="form-control name" name="name" required="required">
						</div>
						<div class="form-group">
							<label for="file">Image</label>
							<input type="file" class="form-control" id="file" placeholder="Image" name="image">
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button class="btn btn-primary" id="game_add" type="submit">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>