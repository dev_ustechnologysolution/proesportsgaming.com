<section class="content">

  <div class="row">

    <div class="col-md-12">

            <?php $this->load->view('admin/common/show_message') ?>

        <div class="box box-primary">

          <form  action="<?php echo base_url(); ?>admin/Social_service/socialserviceUpdate" method="post" enctype="multipart/form-data">

            <div class="box-body">

              <div class="form-group">
                <label for="name">Social Service</label>
                <input type="text" id="name" class="form-control" name="name" value="<?php if(isset($data[0]['name']))echo $data[0]['name'];?>" required="required">
              </div>

              <div class="form-group">

                    <label for="file">Image</label>

                    <input type="file" class="form-control" id="file" placeholder="Image" name="image">

                </div>

                <div class="form-group">
                  <?php if($data[0]['image'] == ''){
                      $data[0]['image'] = 'default_social.png';
                  }?>
                  <img src="<?php echo base_url().'upload/all_images/'.$data[0]['image']?>" height="150" width="150">

                </div>

              </div><!-- /.box-body -->

              <div class="box-footer">

                <input type="hidden" name="id" value="<?php if(isset($data[0]['id'])) echo $data[0]['id'];?>">

                <input type="hidden" name="old_name" value="<?php if(isset($data[0]['name'])) echo $data[0]['name'];?>">

                <input type="hidden" name="old_image" value="<?php if(isset($data[0]['image'])) echo $data[0]['image'];?>">

                <button class="btn btn-primary" type="submit">Update</button>

              </div>

          </form>

        </div>

      </div>

    </div>

</section>