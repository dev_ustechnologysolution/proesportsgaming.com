<section class="content">

  <div class="box">
    <div class="box-header">
      <a href="<?php echo base_url().'admin/Social_service/socialserviceAdd' ?>">
        <button class="btn btn-primary" style="float:right;margin-right: 7px;">Add Social Service</button>
      </a>
    </div>
    <?php $this->load->view('admin/common/show_message') ?>

    <div class="box-body">

      <table id="" class="table table-bordered table-striped">

        <thead>

          <tr>

            <th>Sl No</th>

            <th>Social Service</th>

            <th>Image</th>

            <th>Action</th>

          </tr>

        </thead>

        <tbody>

          <?php $i=1; foreach($list as $value){?>

          <tr>

            <td><?php echo $i++; ?></td>

            <td><?php echo $value['name'];?></td>

            <td>
              <?php
                if($value['image'] != ''){
                  $src = base_url().'/upload/all_images/'.$value['image'];
                }else{
                  $src = base_url().'/upload/all_images/default_social.png';
                }
              ?>
              <img src='<?php echo $src; ?>' height="60px" width="60px">
            </td>

            <td><a href="<?php echo base_url().'admin/Social_service/socialserviceEdit/'.$value['id'];?>"><i class="fa fa-pencil fa-fw"></i></a>  <a href="<?php echo base_url().'admin/Social_service/delete_social_service/'.$value['id'];?>"><i class="fa fa-trash fa-fw"></i></a> </td>

           </tr>

          <?php }?>

        </tbody>

      </table>

    </div>

  </div>

</section>