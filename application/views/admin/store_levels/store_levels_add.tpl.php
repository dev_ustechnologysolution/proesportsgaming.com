<section class="content">
	<div class="row">
		<div class="col-md-12">
			<?php $this->load->view('admin/common/show_message') ?>
			<div class="box box-primary">
				<form  action="<?php echo base_url(); ?>admin/Store_levels/add_store_levels" method="post" enctype="multipart/form-data">
					<div class="box-body">
						<div class="form-group">
							<label for="level">Level Number</label>
							<input type="number" placeholder="Enter Level Number" id="level" class="form-control level" name="level" required="required" min="<?php echo $max_level->level+1; ?>" value="<?php echo $max_level->level+1; ?>" readonly>
						</div>
						<div class="form-group">
							<label for="level_title">Level Title</label>
							<input type="text" placeholder="Enter Level Title" id="level_title" class="form-control level_title" name="level_title" required="required">
						</div>
						<div class="form-group">
							<label for="item_sold">Items Sold Total</label>
							<input type="number" placeholder="Enter Items Sold Total" id="item_sold" class="form-control item_sold" name="item_sold" min="0" required="required">
						</div>
						<div class="form-group">
							<label for="reward">Reward</label>
							<input type="number" placeholder="Enter Reward" id="reward" class="form-control reward" name="reward" min="0" required="required" step="any">
						</div>
						<div class="form-group">
							<label for="level_image">Level Image</label>
							<input type="file" placeholder="Enter Level Image" id="level_image" class="form-control level_image" name="level_image">
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button class="btn btn-primary" id="game_add" type="submit">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>