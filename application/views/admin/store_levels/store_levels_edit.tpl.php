<section class="content">
  <div class="row">
    <div class="col-md-12">
      <?php $this->load->view('admin/common/show_message') ?>
      <div class="box box-primary">
        <form  action="<?php echo base_url(); ?>admin/Store_levels/storelevelsUpdate" method="post" enctype="multipart/form-data">
          <div class="box-body">
            <div class="form-group">
              <label for="level">Level Number</label>
              <input type="number" placeholder="Enter Level Number" id="level" class="form-control level" name="level" required="required" value="<?php if(isset($data[0]['level']))echo $data[0]['level'];?>" readonly>
            </div>
            <div class="form-group">
              <label for="level_title">Items Title</label>
              <input type="text" placeholder="Enter Level Title" id="level_title" class="form-control level_title" name="level_title" value="<?php if(isset($data[0]['level_title']))echo $data[0]['level_title'];?>" required="required">
            </div>
            <div class="form-group">
              <label for="item_sold">Items Sold Total</label>
              <input type="number" placeholder="Enter Items Sold Total" id="item_sold" class="form-control item_sold" name="item_sold" min="0" value="<?php if(isset($data[0]['item_sold']))echo $data[0]['item_sold'];?>" required="required">
            </div>
            <div class="form-group">
              <label for="reward">Reward</label>
              <input type="number" placeholder="Enter Reward" id="reward" class="form-control reward" name="reward" min="0" step="any" value="<?php if(isset($data[0]['reward']))echo $data[0]['reward'];?>">
            </div>
            <div class="form-group">
              <label for="level_image">Level Image</label>
              <br>
              <div class="img" style="display: block; max-width: 150px">
                <a class="btn btn-info" onclick="$(this).closest('div.img').hide().siblings('.level_image').show();"> Change</a>
                <br><br>
                <img src="<?php echo base_url().'upload/all_images/'.$data[0]['level_img']; ?>" class="img img-responsive" alt="smile">
              </div>
              <input type="file" placeholder="Enter Level Image" id="level_image" class="form-control level_image" name="level_image" style="display: none;">
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <input type="hidden" name="id" value="<?php if(isset($data[0]['id'])) echo $data[0]['id'];?>">
            <input type="hidden" name="old_level" value="<?php if(isset($data[0]['level'])) echo $data[0]['level'];?>">
            <input type="hidden" name="old_item_sold" value="<?php if(isset($data[0]['item_sold'])) echo $data[0]['item_sold'];?>">
            <input type="hidden" name="old_reward" value="<?php if(isset($data[0]['reward'])) echo $data[0]['reward'];?>">
            <button class="btn btn-primary" type="submit">Update</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>