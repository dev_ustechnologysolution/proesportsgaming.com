  <section class="content">
  <?php $this->load->view('admin/common/show_message') ?>
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/datatables/rowReorder.dataTables.min.css'; ?>">
  <div class="box">
    <div class="box-header">
      <form id = "store_membership_status_change_form" action="<?php echo base_url().'admin/Store_levels/change_store_membership_status' ;?>" method = "POST">
        <input type="hidden" name="status" id="store_membership_status">
      </form> 
      <div class="status_button banner_page" style="position: absolute;">
          <b>Sell Items Without Membership</b> &nbsp;&nbsp;<label class="switch"><input type="checkbox" name="store_membership_status_tgl" value="store_membership_status_tgl" <?php echo ($store_membership_status['option_value'] == 'on') ? 'checked' : ''; ?> class="store_membership_status_tgl"><span class="slider round"></span></label>
      </div>
      <a href="<?php echo base_url().'admin/Store_levels/storelevelsAdd' ?>">
        <button class="btn btn-primary" style="float:right;margin-right: 7px;">Add Store Level</button>
      </a>
      <a class="btn btn-primary global_store_fee" action="global_store_fee" style="float:right;margin-right: 7px;">Add Global Store Fee</a>
    </div>
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped display example_row_reorder" style="width:100%">
        <thead>
          <tr>
            <!-- <th>Order by</th> -->
            <th>Level Number</th>
            <th>Level Title</th>
            <th>Items Sold Total</th>
            <th>Reward</th>
            <th>Level Image</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach($list as $value){?>
          <tr>

            <!-- <td title="Drag & Drop for change Order"><i class="fa fa-bars" style="padding-right: 10px;"></i><?php // echo $value['order_id'];?></td> -->
            <td><?php echo $value['level'];?></td>
            <td><?php echo $value['level_title'];?></td>
            <td><?php echo $value['item_sold'];?></td>
            <td><?php echo $value['reward'];?></td>
            <td>
              <?php if(!empty($value['level_img'])) { ?>
                <div style="max-width: 100px;"><img src="<?php echo base_url().'upload/all_images/'.$value['level_img']; ?>" class="img img-responsive" alt="Smile"></div>
              <?php } ?>
            </td>
            <td style="max-width: 100px;"><a href="<?php echo base_url().'admin/Store_levels/storelevelsEdit/'.$value['level'];?>" class="btn btn-info" title="Edit Level"><i class="fa fa-pencil fa-fw"></i></a> <a href="<?php echo base_url().'admin/Store_levels/view_users_of_level/'.$value['level'];?>" class="btn btn-info" title="View Level Users"><i class="fa fa-user fa-fw"></i></a> <a class="btn btn-info delete-data" id="<?php echo $value['level']; ?>" action="delete_store_level_act" title="Delete Level" winre_loc="<?php echo base_url().'admin/Store_levels/delete_store_levels/'.$value['level']; ?>"><i class="fa fa-trash fa-fw"></i></a></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="modal custom-width" id="global_store_fee" style="padding-right: 17px; overflow: auto !important">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title keyorder" style="color:#111; font-size:19px; font-weight:bold;"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <form action="<?php echo base_url(); ?>admin/Store_levels/global_store_fee" id="global_store_fee_form" name="global_store_fee_form" method="POST">
            <div class="box-body">
              <div class="form-group">
                <label>Global Order Fee </label>
                <input type="text" id="gsf" placeholder="Enter Global Store Fee" class="form-control" name="global_store_fee" value="<?php echo (isset($global_store_fee['option_value']) && $global_store_fee['option_value'] != null && $global_store_fee['option_value'] != '') ? $global_store_fee['option_value'] : ''; ?>" required="">
              </div>
            </div>
            <div class="box-footer">
              <input class="btn btn-primary" type="submit" value="Submit">
              <button type="button" class="btn btn-info pull-right" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<!-- <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="<?php // echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php // echo base_url().'assets/plugins/datatables/dataTables.rowReorder.min.js'; ?>"></script>
<script src="<?php // echo base_url().'assets/backend/js/row_reorder_custom.js'; ?>"></script> -->
<script type="text/javascript">
  // var table = $('#example_row_reorder').DataTable({rowReorder: true,scrollY: 700});
  // table.on('row-reorder', function(e, diff, edit) {
  //   var o = [], n = [];
  //   for (var i = 0, ien = diff.length ; i < ien ; i++) {
  //     o.push(diff[i].oldData), n.push(diff[i].newData); 
  //   }
  //   try { 
  //     $.ajax({
  //       type: "POST",
  //       url: base_url + "admin/store_levels/level_order_update",
  //       data: {'old':o,'new':n},
  //       beforeSend: function(){ admin_loader_show(); },
  //       success: function (data){ admin_loader_hide(); }
  //     });
  //   } catch(err) { alert(err); }
  // });

  $('.global_store_fee').click(function () {
    $('#global_store_fee').modal('show');
  });

  $('.store_membership_status_tgl').on('change', function(){
    if($(this).is(':checked')){
      var status = 'on';
    } else {
      var status = 'off';
    }
    // alert(status)
    $('#store_membership_status').val(status);
    $('#store_membership_status_change_form').submit();
  });
</script>