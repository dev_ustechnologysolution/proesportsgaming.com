<section class="content">
  <?php $this->load->view('admin/common/show_message') ?>
  <div class="box">
    <div class="box-body">
      <div class="box-header">
        <a class="btn btn-info pull-right delete-data" action="reset_level_players" id="<?php echo $level_id; ?>" winre_loc="<?php echo base_url().'admin/store_levels/reset_level_players/'.$level_id ?>">Reset Players</a>
      </div>
    </div>
    <div class="box-body">
      <table id="customize_tbl" class="table table-bordered table-striped" account_search_clm="0">
        <thead>
          <tr>
            <th>Acct#</th>
            <th>Name</th>
            <th>Level</th>
          </tr>
          <tr>
            <td colspan="3"><input type="text" id="search-acc" placeholder="Search Account" class="form-control" style=" width: 100%; "></td>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($list)) {
            foreach($list as $value) {
              if ($value['sold_items'] >= $value['tg_item_sold']) { ?>
                <tr>
                  <td><?php echo $value['account_no'];?></td>
                  <td><?php echo !empty($value['display_name']) ? $value['display_name'] : $value['name'];?></td>
                  <td><?php echo $value['level'];?></td>
                </tr>
              <?php }
            }
          } ?>
        </tbody>
      </table>
    </div>
  </div>
</section>