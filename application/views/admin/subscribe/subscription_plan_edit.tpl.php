<section class="content">
  <div class="row">   
    <div class="col-md-12">
      <?php $this->load->view('admin/common/show_message'); ?>
      <div class="box box-primary">
        <form  enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/subscription/subscribe_plan_update" method="post">          
      
            <div class="box-body">
              <div class="form-group row">
                <label for="title" class="col-sm-2">Title</label>
                <div class="col-sm-10">
                  <input required type="text" value="<?php echo ($subscription_plan['title']) ? $subscription_plan['title'] : '' ; ?>" class="form-control" name="title">
                </div>
              </div>
              <div class="form-group row">
                <label for="description" class="col-sm-2">Description</label>
                <div class="col-sm-10">
                  <textarea required name="description" class="form-control" rows="10" cols="80"><?php echo ($subscription_plan['description']) ? $subscription_plan['description'] : '' ; ?></textarea>
                </div>
              </div>
             
              <div class="form-group row upload-div  <?php echo ($subscription_plan['image']) ? 'active' : '' ; ?>" style="padding-left: 0;">
                <label class="col-sm-2">Image</label>
                <div class="col-sm-10">
                  <input type="hidden" name="selected_banner" id="edit_banner_image" value="edit_banner_image" <?php echo ($subscription_plan['image']) ? 'checked class="checked"' : '' ; ?>>
                  <a style="float: right; cursor: pointer; display: none;" class="col-sm-10 browse_new_image">Change Image</a>
                  <input type="file" class="form-control image_banner" id="file" placeholder="Image" name="image" accept="image/*" style="display: none;">
                <input type="hidden" id="old_image" name="image" value="<?php echo ($subscription_plan['image']) ? $subscription_plan['image'] : '' ; ?>">
                <div  style="margin-top: 5px; "><img src="<?php echo base_url().'upload/subscription/'.$subscription_plan['image']; ?>"  class="old_image_preview" height="150" width="150" style="display: none;">
                </div>
              </div>
                
              </div>
              

              <div class="form-group row">
                <label for="amount" class="col-sm-2">Amount ($)</label>
                <div class="col-sm-10">
                  <input required value="<?php echo ($subscription_plan['amount']) ? $subscription_plan['amount'] : '' ; ?>" type="number" class="form-control" min="1" name="amount">
                </div> 
              </div>

              <div class="form-group row">
                <label for="fees" class="col-sm-2">Fees ($)</label>
                <div class="col-sm-10">
                  <input required type="number" value="<?php echo ($subscription_plan['fees']) ? $subscription_plan['fees'] : '' ; ?>" class="form-control" min="0" name="fees">
                 </div> 
              </div>

              <div class="form-group row">
                <label for="terms_condition" class="col-sm-2">Terms & Policy</label>
                <div class="col-sm-10">
                  <textarea name="terms_condition" class="form-control" rows="10" cols="80"><?php echo ($subscription_plan['terms_condition']) ? $subscription_plan['terms_condition'] : '' ; ?></textarea>
                </div>
              </div>
   
            </div>
            <div class="box-footer col-sm-12" align="center"> 
             <input type="hidden" name="id" value="<?php echo ($subscription_plan['id']) ? $subscription_plan['id'] : '' ; ?>">           
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
      </div>
    </div>
  </div>
</section>