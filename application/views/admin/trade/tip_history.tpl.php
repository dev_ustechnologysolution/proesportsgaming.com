<section class="content">
      <div class="box">
          <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th style="display:none"></th>
                  <th>Reg #</th>
                  <th>Sender Name</th>
                  <th>Sender Acct#</th>
                  <th>Receiver Name</th>
                  <th>Receiver Acct#</th> 
                  <th>Tip Amt</th>
                  <th>Tip Date</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=1; foreach($tip_data as $value){
                  $sender_arr = $this->General_model->view_single_row('user_detail','user_id',$value->user_from); 
                  $reciver_arr = $this->General_model->view_single_row('user_detail','user_id',$value->user_to); ?>
                <tr>
                  <td style="display:none"></td>
                  <td><?php echo $i++; ?></td>
                  <td><?php echo  ucwords($sender_arr['name']); ?></td>
                  <td style="width: 20%"><?php echo $sender_arr['account_no']; ?></td>
                  <td><?php echo ucwords($reciver_arr['name']); ?></td>
                  <td><?php echo $reciver_arr['account_no']; ?></td>                  
                  <td><?php echo $value->tip_amt;?></td>
                  <td><?php $date_arr = explode(' ',$value->created); echo $date_arr[0];?></td>           
                 </tr>
                <?php }?>
              </tbody>
            </table>
          </div>
        </div>
</section>