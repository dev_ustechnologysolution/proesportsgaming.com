<section class="content">
  <div class="row">
    <div class="col-md-12">
    <?php $this->load->view('admin/common/show_message') ?>     
        <div class="box box-primary">
        	<!-- form start -->
        	<?php if(count($usr) > 0) { ?> 
        		<form  action="<?php echo base_url(); ?>admin/FrontUser/userUpdate" method="post" enctype="multipart/form-data">
        			<div class="box-header with-border">
        				<h3 class="box-title"></h3>
        				<a class="btn btn-info pull-right change_password" id="change_password" style="margin-left: 10px;">Create New Password</a>
                <a class="btn btn-info pull-right edit-data" action="exp_plyr_history" id="<?php echo $usr[0]['user_id']; ?>" winre_loc="<?php echo base_url().'admin/FrontUser/exportGameHistory/'; ?>"><i class="fa fa-print"></i> Export Player History</a>
              </div>
        			<div class="box-body">
        				<div class="form-group" style="font-size: 17px; font-weight: bold;">
			  				<label for="name">Ban Account : </label>
			  				<input  type="radio" id="ban" name="is_active" value="1" <?php echo ($details[0]['ban_option'] != 0)? 'checked' : '' ?>> Yes 
			  				<input  type="radio" id="ban2" name="is_active" value="0" <?php echo ($details[0]['ban_option'] == 0)? 'checked' : '' ?>> No
			  			</div>
			  			<?php $class = ($details[0]['ban_option'] != 0) ? "block" : "none"; ?>
							<div class="form-group <?php echo $class?>" id="ban_option">
								<label for="ban_option">Ban Option</label>
								<select class="form-control" name="ban_option">
									<option <?php echo (isset($details[0]['ban_option']) && $details[0]['ban_option'] == '24') ? 'selected' : '' ?> value="24">24 Hours</option>
									<option <?php echo (isset($details[0]['ban_option']) && $details[0]['ban_option'] == '48') ? 'selected' : '' ?> value="48">48 Hours</option>
									<option <?php echo (isset($details[0]['ban_option']) && $details[0]['ban_option'] == '168') ? 'selected' : '' ?> value="168">7 Days</option>
									<option <?php echo (isset($details[0]['ban_option']) && $details[0]['ban_option'] == '1') ? 'selected' : '' ?> value="1">Permanent</option>
								</select>
							</div>
				      <div class="form-group">
              	<label for="name">Full Name</label>
              	<input type="text" id="name" class="form-control" name="name" value="<?php if(isset($usr[0]['name']))echo $usr[0]['name'];?>">
            	</div>

              <div class="form-group">
                <label for="name">Email</label>
                <input type="text" id="name" class="form-control" name="email" value="<?php if(isset($details[0]['email']))echo $details[0]['email'];?>" >
              </div>
              <div class="form-group">
                <label for="dob">Date of Birth</label>
                <input readonly type="text" id="dob" class="form-control" name="dob" value="<?php if(isset($usr[0]['dob']))echo $usr[0]['dob'];?>" >
              </div>
                  
              <div class="form-group">
                <label for="phone">Phone Number</label>
                <input type="text" id="phone" class="form-control" name="number" value="<?php if(isset($usr[0]['number']))echo $usr[0]['number'];?>">
              </div>
              <div class="form-group">
                <label for="address">Location</label>
                <input class="form-control" id="address" name="location" value="<?php if(isset($usr[0]['location'])) echo $usr[0]['location'];?>" />
              </div>
              <div class="form-group">
              	<label for="address">Country</label>
              	<select id="countries_states1" class="form-control" name="country">
              		<?php foreach ($country_list as $key => $value) { echo '<option value="'.$value['country_id'].'"';if ($value['country_id'] == $usr[0]['country']) { echo ' selected="selected"'; } echo '>'.$value['countryname'].'</option>'; } ?>
              	</select>
              </div>				

              <div class="form-group">
                <label for="address">State</label>
                <select class="form-control" name="state" id="state">
                	<?php 
                		foreach ($state as $st) {
                			$selected = (isset($usr[0]['state']) && $usr[0]['state'] != '' && $usr[0]['state'] == $st['stateid']) ? 'selected' : '';
                			echo '<option value="'.$st['stateid'].'" '.$selected.'>'.$st['statename'].'</option>';
                		}
                 	?>		                   	
                 </select>
              </div>		
              <div class="form-group">
              	<label for="city">City</label>
              	<input class="form-control" name="city" id="city" value="<?php if(isset($usr[0]['city'])) echo $usr[0]['city'];?>" />
              </div>
              <div class="form-group">
              	<label for="zipcode">Zip Code</label>
              	<input class="form-control" name="zip_code" id="zip_code" value="<?php if(isset($usr[0]['zip_code'])) echo $usr[0]['zip_code'];?>" />
              </div>
              <div class="form-group">
              	<label for="timezone">Time Zone</label>
              	<?php
                $timezoneArray = timezone_identifiers_list();
                $select = '<select name="timezone" class="form-control" id="timezone" required>';
                $select .= '<option value="">Select Time zone</option>';
                $timezoneArray = preg_grep("/America/", $timezoneArray, PREG_GREP_INVERT);
                  array_push($timezoneArray, "HST", "America/Anchorage", "America/Los_Angeles", "MST", "America/Chicago", "EST");
                foreach ($timezoneArray as $key => $ta) {
                  $newta = str_replace(["HST", "America-Anchorage", "America-Los_Angeles", "MST", "America-Chicago", "EST"],["U.S-Hawaii-Aleutian", "U.S-Alaska", "U.S-Pacific", "U.S-Mountain", "U.S-Central", "U.S-Eastern"],str_replace('/','-',$ta));
                  $select .='<option value="'.$ta.'"';
                  $select .= ($ta == $usr[0]['timezone'] ? ' selected' : '');
                  $select .= '>'.$newta.'</option>';
                }
                $select.='</select>';
                echo $select;
                ?>
              </div>
              <div class="form-group">
              	<label class="cntnr"> Super Admin <input type="checkbox" name="is_admin" id="is_admin" <?php echo ($usr[0]['is_admin'] == '1') ? 'checked' :''; ?>><span class="chckmrk"></span></label>
              </div>
              <div class="form-group">
              	<?php $common_settings = json_decode($usr[0]['common_settings']);
              	$mbrshp_chckd = (!empty($common_settings) && $common_settings->free_membership == 'on')? 'checked': ''; ?>
              	<label class="cntnr"> Free Membership <input type="checkbox" name="free_membership" id="free_membership" <?php echo $mbrshp_chckd; ?>><span class="chckmrk"></span></label>
              </div>
              <!-- <div class="form-group">
								<label for="password">Password</label>
								<a href="#" class="change_password" id="change_password"> Create New Password </a>
							</div> -->
							<!-- <div class="form-group">
								<a href="<?php // echo base_url(); ?>admin/FrontUser/exportGameHistory/params?user_id=<?php // echo $usr[0]['user_id'];?>"> <i class="fa fa-print"></i> Export Player History</a>
							</div> -->
							<?php if ($this->session->userdata['admin_user_id'] == '1') {  ?>
								<div class="form-group">
									<label for="name">TAX ID <?php if(isset($usr[0]['taxid'])) {
										if($usr[0]['taxid'] != '') { ?>
											<img src="<?php echo base_url() ?>assets/frontend/images/green.png" width="10" />
										<?php } else { ?>
											<img src="<?php echo base_url() ?>assets/frontend/images/red.png" width="10" />
										<?php }
									} ?> </label>
									<input class="form-control" id="address" name="taxid" value="<?php if(isset($usr[0]['taxid'])) echo $usr[0]['taxid'];?>" />
								</div>
							<?php } ?>	
			        </div>
			        <div class="box-footer">
			        	<input type="hidden" name="id" value="<?php if(isset($details[0]['id'])) echo $details[0]['id'];?>">
			        	<button class="btn btn-primary" type="submit">Update</button>
			        </div>
            	</form>
          <?php }  else { ?> <h4>Data not found</h4> <?php } ?>
        </div>
      </div>
    </div>
  </div>   
</section>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script> $.validate();</script>
<div class="modal fade  custom-width" id="modal-1">
	<div class="modal-dialog" style="width: 50%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Set New  Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="password_change" name="password_change" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Set New Password</label>
								<input type="password" id="new_password" class="form-control" name="new_password" value="" data-validation="required" required>
								<input type="hidden" id="userid" class="form-control" name="userid" value="<?php echo $usr[0]['user_id']; ?>" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>
		</div>
	</div>
</div>
<style>
.none{
	display:none;
}
.block {
	display:block;
}
</style>
<script>
conf = {
	onElementValidate : function(valid, $el, $form, errorMess) {
		if( !valid ) {
			errors.push({el: $el, error: errorMess});
		}
	}
};
lang = {};

$(document).on('submit','#password_change',function(e){
	e.preventDefault();
	var data = $(this).serialize();
	if(data){
		$.ajax({
			url: "<?php echo site_url(); ?>admin/FrontUser/Change_password",
			data: data,
			type: 'POST',
			success: function(result) {
				window.location.href = "<?php echo base_url().'admin/FrontUser/userEdit/'.$usr[0]['user_id']; ?>";
			}
		});
	}							
})
$(document).on('click','.change_password',function(){
	$('#modal-1').modal('show');
})

$(document).on('click','#export_game_history',function(e){
	var user_id = $('#user_id').val();
	var data = {
		'user_id': user_id
	};
	if(data){
		$.ajax({
			url: "<?php echo site_url(); ?>admin/FrontUser/exportGameHistory/"+data['user_id'],
			data: data,
			type: 'POST',
			success: function(result) {
				// window.location.href = "<?php echo site_url(); ?>admin/FrontUser/userEdit/"+data['user_id'];				
			}
		});
	}
})

$('#countries_states1').on('change',function(){
  var countryID = $(this).val();
  if(countryID){
      $.ajax({
          type:'POST',
          url:base_url+'signup/select_country',
          data:'country_id='+countryID,
          success:function(html){
              $('#state').html(html);
          }
      }); 
  }else{
      $('#state').html('<option value="">Select State</option>');
  }
});
$('#ban').click(function(){	
		$('#ban_option').removeClass('none').addClass('block');
})
$('#ban2').click(function(){	
		$('#ban_option').removeClass('block').addClass('none');

})


</script>