<div class="col-sm-12">
	<div class="pull-right" >
		<a href="<?php echo base_url(); ?>admin/FrontUser/get_cron_details" id="get_url_1" class="" > Refresh </a> &nbsp;|&nbsp; <a href="<?php echo base_url(); ?>admin/FrontUser/get_reset_all" id="get_url_2" class="reset_get"> Reset All </a> &nbsp;|&nbsp; <a href="<?php echo base_url(); ?>admin/FrontUser/get_reset_won" id="get_url_3" class="reset_get"> Reset Win </a> &nbsp;|&nbsp; <a href="<?php echo base_url(); ?>admin/FrontUser/get_reset_lost" id="get_url_4" class="reset_get"> Reset Lost </a> &nbsp;|&nbsp; <a href="<?php echo base_url(); ?>admin/FrontUser/get_reset_with" id="get_url_5" class="reset_get"> Reset Withdrawal </a> &nbsp;|&nbsp; <a href="<?php echo base_url(); ?>admin/FrontUser/userExcel" id="get_url_6" class="reset_get"><i class="fa fa-print"></i> Export Excel</a> 
	</div>
</div>
<div class="clearfix"></div>
<section class="content">
	<div class="row">
		<div class="box-header">
			<?php $this->load->view('admin/common/show_message') ?>
		</div>
	</div>
	<div class="box">
		<div class="box-body">
	      	<table id="example_5" class="table table-bordered table-striped">		
				<thead>
					<tr>
						<td colspan="20"><input type="text" id="search-acc" placeholder="Search Account" class="form-control" style=" width: 100%; "></td>
					</tr>
					<tr>
				        <th>Acct</th>
				        <th>Name</th>
				        <th>Display Name</th>
				        <th>Esports Team</th>
				        <th>Is 18</th>
				        <th>Email</th>
				        <th>Discord Info</th>
				        <th>Phone Number</th>
				        <th>Membership</th>
				        <th>City</th>
				        <th>State</th>
				        <th>Country</th>
				        <th>Tax ID </th>
				        <th>Won Total</th>
				        <th>Lost Amt</th>
				        <th>Withdrawal Amt</th>
				        <th>Balance</th>
				        <th>Status</th>
				        <th>Last Login</th>
				        <th>Action</th>
				    </tr>
				</thead>
	          	<tbody>
	          		<?php if (!empty($list)) {
			          	foreach ($list as $key => $value) {
			          		$img = ($value['is_admin'] == 1) ? base_url().'assets/frontend/images/pro_player.png' : '';
			          		?>
			              <tr>
			              	<td><?php echo $value['account_no']?><a class="account_no" id="<?php echo $value['id']?>"><i class="fa fa-pencil fa-fw"></i></a><?php echo ($img != '') ? '<img width="15" height="15" src="'.$img.'">' : ''; ?> </td>
			                <td><?php echo $value['name']; ?></td>
			                <td><?php echo $value['display_name']; ?><a class="change_display_name" id="<?php echo $value['id']?>" display_name="<?php echo $value['display_name']; ?>" style="margin-left: 4px;"><i class="fa fa-pencil fa-fw"></i></a></td>
			                <td><?php echo $value['team_name']; ?><a class="change_team_name" id="<?php echo $value['id']?>" team_name="<?php echo $value['team_name']; ?>" style="margin-left: 4px;"><i class="fa fa-pencil fa-fw"></i></a></td>
			                <td><?php echo ($value['is_18'] == 1) ? '18+' :'Under 18'; ?></td>
			                <td><?php echo $value['email']; ?></td>
			                <td><?php echo $value['discord_id']; ?></td>
			                <td><?php echo $value['number']; ?></td>
			                <td><?php echo (in_array($value['id'], $active_mplans)) ? '<img src="'.base_url().'assets/frontend/images/green.png" width="10"/>&nbsp;' : '<img src="'.base_url().'assets/frontend/images/red.png" width="10"/>&nbsp&nbsp'; ?></td>
			                <td><?php echo $value['city']; ?></td>
			                <td><?php echo $value['statename']; ?></td>
			                <td><?php echo $value['countryname']; ?></td>
			                <td><?php echo ($value['taxid'] != '') ? '<img src="'.base_url().'assets/frontend/images/green.png" width="10"/>&nbsp' : '<img src="'.base_url().'assets/frontend/images/red.png" width="10"/>&nbsp&nbsp'; ?></td>
			                <td><?php echo $value['won_amt']; ?></td>
			                <td><?php echo $value['lost_amt']; ?></td>
			                <td><?php echo $value['withdrawal_amt']; ?></td>
			                <td><?php echo number_format($value['total_balance'], 2, '.', ''); ?> <a href="#" class="view" id="<?php echo $value['id']; ?>"><i class="fa fa-pencil fa-fw"></i></a></td>
			            		<td><?php echo ($value['is_active'] == 0 && $value['ban_option'] == 0) ? 'Active' : 'Banned'; ?></td>
											<td><?php echo date('m-d-Y H:i a',strtotime($value['last_login']));?></td>
			                <td><a href="<?php echo base_url().'admin/FrontUser/userEdit/'.$value['id'];?>"><i class="fa fa-pencil fa-fw"></i></a><a href="javascript:void(0)" class="delete" id="del_<?php echo $value['id']?>"><i class="fa fa-trash-o fa-fw"></i></a><a class="edit-data" action="exp_plyr_history" id="<?php echo $value['user_id']; ?>" winre_loc="<?php echo base_url().'admin/FrontUser/exportGameHistory/'; ?>"><i class="fa fa-print"></i></a></td>
			              </tr>
			            <?php }
			        } ?>
	          	</tbody>
	        </table>
	    </div>
	</div>
</section>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script> $.validate();</script>
<!-- Modal 1 (Basic)-->
<div class="modal fade custom-width" id="modal-1">
	<div class="modal-dialog" style="width: 50%;">
		<div class="modal-content"></div>
	</div>
</div>
<div class="modal fade custom-width" id="modal-display_name">
	<div class="modal-dialog" style="width: 50%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title keyorder" style="color:#111; font-size:19px; font-weight:bold;">Display Name</h4>
			</div>
			<div class="modal-body">
			    <div class="row">
					<form action="<?php echo base_url().'admin/FrontUser/update_display_name'; ?>" method="POST">
						<div class="box-body">
							<div class="form-group">
							    <label>Change Display Name</label>
							    <input type="hidden" class="form-control" name="user_id" value="">
							    <input type="text" id="display_name" class="form-control" name="display_name" value="" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit">
							<button type="button" class="btn btn-info pull-right" data-dismiss="modal">Close</button>
						</div>
					</form>
			    </div>
			</div>
    </div>
	</div>
</div>

<!-- change team name model -->
<div class="modal fade custom-width" id="modal-team_name">
	<div class="modal-dialog" style="width: 50%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title keyorder" style="color:#111; font-size:19px; font-weight:bold;">Esports Team</h4>
			</div>
			<div class="modal-body">
			    <div class="row">
					<form action="<?php echo base_url().'admin/FrontUser/update_team_name'; ?>" method="POST">
						<div class="box-body">
							<div class="form-group">
							    <label>Change Esports Team</label>
							    <input type="hidden" class="form-control" name="user_id" value="">
							    <input type="text" id="team_name" class="form-control" name="team_name" value="" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit">
							<button type="button" class="btn btn-info pull-right" data-dismiss="modal">Close</button>
						</div>
					</form>
			    </div>
			</div>
    </div>
	</div>
</div>

<script>
$(document).ready(function()  {
	var table = $('#example_5').DataTable();
	$('#search-acc').on('keypress keyup', function() {
		table.column(0).search(this.value).draw();
	});
});

$('.change_display_name').click(function() {
	var display_name = $(this).attr('display_name'), user_id = $(this).attr("id");
	$('#modal-display_name').find('input[name="user_id"]').val(user_id).siblings('input[name="display_name"]').val(display_name);
	$('#modal-display_name').modal('show');
})

// change team name
$('.change_team_name').click(function() {
	var display_name = $(this).attr('team_name'), user_id = $(this).attr("id");
	$('#modal-team_name').find('input[name="user_id"]').val(user_id).siblings('input[name="team_name"]').val(display_name);
	$('#modal-team_name').modal('show');
})

conf = {
	onElementValidate : function(valid, $el, $form, errorMess) {
		if( !valid ) {
			errors.push({el: $el, error: errorMess});
		}
	}
};
lang = {};
$(document).on('submit','#security_update_bal_id',function(e){
	e.preventDefault();
	var data = $(this).serialize();
	if(data){
		$.ajax({
			url: "<?php echo site_url(); ?>admin/FrontUser/ChangeBalance",
			data: data,
			type: 'POST',
			success: function(result) {
				$('.modal-content').html(result);
				$('#modal-1').modal('show');
			}
		});
	}							
})
$(document).on('submit','#account_security_id',function(e){
	e.preventDefault();
	var data = $(this).serialize();
	if(data){
		$.ajax({
			url: "<?php echo site_url(); ?>admin/FrontUser/ChangeAccountNo",
			data: data,
			type: 'POST',
			success: function(result) {
				$('.modal-content').html(result);
				$('#modal-1').modal('show');				
			}
		});
	}							
})
$(document).on('submit','#changebalance_id',function(e){
	e.preventDefault();
	var data = $(this).serialize();
	if(data){
		$.ajax({
			url: "<?php echo site_url(); ?>admin/FrontUser/UpdateBalance",
			data: data,
			type: 'POST',
			success: function(result) {
				window.location.href = "<?php echo site_url(); ?>admin/FrontUser/";
			}
		});
	}							
})
$(document).on('submit','#change_account_no',function(e){
	e.preventDefault();
	var data = $(this).serialize();
	if(data){
		$.ajax({
			url: "<?php echo site_url(); ?>admin/FrontUser/UpdateAccountNo",
			data: data,
			type: 'POST',
			success: function(result) {
				if(result){
					$('.modal-content').html(result);
					$('#modal-1').modal('show');
				} else{
					$('#modal-1').modal('hide');
					window.location.href = "<?php echo site_url(); ?>admin/FrontUser/";				
				}
			}
		});
	}							
})
$(document).on('click','.view',function(){
	var id = $(this).attr('id');
	if(id){
		$.ajax({
			url : '<?php echo site_url(); ?>admin/FrontUser/security_data/'+id,
			success: function(result) {
				if(result){
					$('.modal-content').html(result);
					$('#modal-1').modal('show');															
				}
			}
		})
	}
})
$(document).on('click','.account_no',function(){
	var id = $(this).attr('id');
	if(id){
		$.ajax({
			url : '<?php echo site_url(); ?>admin/FrontUser/account_security_data/'+id,
			success: function(result) {
				if(result){
					$('.modal-content').html(result);
					$('#modal-1').modal('show');															
				}
			}
		})
	}
})

$(document).on('click','.reset_get',function(e){
	e.preventDefault();
	var id = $(this).attr('id');
	//var id = $(this).attr('href');
	if(id){
		$.ajax({
			url : '<?php echo site_url(); ?>admin/FrontUser/security_pass/'+id,
			success: function(result) {
				if(result){
					$('.modal-content').html(result);
					$('#modal-1').modal('show');															
				}
			}
		})
	}
})


$(document).on('submit','#security_pass',function(e){
	e.preventDefault();
	var data = $(this).serialize();
	var action_value = $('#action_value').val();
	if(data){
		$.ajax({
			url: "<?php echo site_url(); ?>admin/FrontUser/get_reset_data",
			data: data,
			type: 'POST',
			success: function(result) {
				if(result == 'success'){
					var action_url = $('#'+action_value).attr('href');
					window.location.href = action_url;
					$('#modal-1').modal('hide');
				} else {
					$('.modal-content').html(result);
					$('#modal-1').modal('show');				
				}
			}
		});
	}							
})


$(document).on('click','.delete',function(){
	var id = $(this).attr('id');
	var idsplit = id.split('_');
	
	if(idsplit[1]){
		$.ajax({
			url : '<?php echo site_url(); ?>admin/FrontUser/delete_data/'+idsplit[1],
			success: function(result) {
				if(result){
					$('.modal-content').html(result);
					$('#modal-1').modal('show');															
				}
			}
		})
	}
})

$(document).on('submit','#security_password_id',function(e){
	e.preventDefault();
	var data = $(this).serialize();
	if(data){
		$.ajax({
			url: "<?php echo site_url(); ?>admin/FrontUser/delete",
			data: data,
			type: 'POST',
			success: function(result) {
				if(result == true){
					window.location.href = "<?php echo site_url(); ?>admin/FrontUser/";
				} else {
					alert('Password Error');
					return false;
				}
							
			}
		});
	}							
})

</script>