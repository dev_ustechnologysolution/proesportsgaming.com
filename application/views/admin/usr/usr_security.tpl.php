<section class="content">
  <div class="row">
    <div class="col-md-12">
    <?php $this->load->view('admin/common/show_message') ?>     
        <div class="box box-primary">
			<!-- form start -->         
            <form  action="<?php echo base_url(); ?>admin/FrontUser/security" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="form-group">
						<label for="name">Security Password</label>
						<input type="text" class="form-control" name="password" value="<?php if(isset($security_password[0]['password']))echo $security_password[0]['password'];?>">
					</div>
					<div class="box-footer">
					   <input type="hidden" name="id" value="<?php if(isset($security_password[0]['id'])) echo $security_password[0]['id'];?>">
					   <button class="btn btn-primary" type="submit">Submit</button>
					</div>
				</div>
            </form>         
        </div>
      </div>
    </div>
  </div>   
</section>