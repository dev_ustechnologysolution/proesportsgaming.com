<section class="content">
	<div class="row">
		<div class="col-md-12">
			<?php $this->load->view('admin/common/show_message'); ?>
			<div class="box subsfeedivbox box-primary">
	            <div class="box-body">
					<div class="box-header">
						<div class="pull-right">
							<button class="btn btn-primary" id="add-row"> Add new</button>
						</div>
		            </div>
		            <form action="<?php echo base_url(); ?>admin/withdrawal/update_withdrawal_fee" method="post" enctype="multipart/form-data" class="live_lobby_fees_point change_manual_deposite_fee">
		            	<table cellspacing="3" cellpadding="3" class="table table-striped">
			              	<tbody>
			              		<tr>
				                  <th style="width: 10px">#</th>
				                  <th>Range($)</th>
				                  <th>Fee Calculation Type</th>
				                  <th>Value</th>
				                  <th>Action</th>
				                </tr>
				                <?php $i=1;
								if (count($withdrawal_fee) != 0) {
					                foreach ($withdrawal_fee as $key => $pt) {
					                	$range = explode('-', $pt['fee_range']);
					                	$min_range = $range[0];
					                	$max_range = $range[1];
					                	$id = $pt['id'];
					                	$fee_dl_calc_type_sel = ($pt["fee_calc_type"] == "$")?"selected":'';
						                $fee_pr_calc_type_sel = ($pt["fee_calc_type"] == "%")?"selected":'';
						                $fee_value = ($pt["fee_value"] != '') ? $pt["fee_value"]:'';
						                ?>
						                <tr class="data_tr" id="<?php echo $id; ?>">
						                	<td class="index"><?php echo $i; ?></td>
						                	<td style="width: 300px;" class="range">
					                			<div class="wid_45">
													<input type="number" class="form-control min-range" name="min-range[]" placeholder="Min Range" value="<?php echo $min_range; ?>" required>
												</div>
												<div class="wid_10"> To </div>
												<div class="wid_45">
													<input type="number" class="form-control max-range" name="max-range[]" placeholder="Max Range" value="<?php echo $max_range; ?>" required>
												</div>
											</td>
											<td>
												<select class="form-control" name="calculation_type[]" required>
								                    <option value="$" <?php echo $fee_dl_calc_type_sel; ?>>$</option>
								                    <option value="%" <?php echo $fee_pr_calc_type_sel; ?>>%</option>
								                </select>
											</td>
											<td>
												<input type="number" class="form-control" name="fee_value[]" placeholder="Fee Value" value="<?php echo $fee_value; ?>" required>
											</td>
											<td>
												<a class="delete-data" id="<?php echo $id; ?>" action="delete_manual_deposite_fee"><i class="fa fa-trash-o"></i></a>
											</td>
										</tr>
										<input type="hidden" name="id[]" value="<?php echo $id; ?>">
									<?php $i++; }
								} else { ?>
									 <tr class="data_tr" id="1">
					                	<td class="index">1</td>
					                	<td style="width: 300px;" class="range">
				                			<div class="wid_45">
												<input type="number" class="form-control min-range" name="min-range[]" placeholder="Min Range" value="1" required>
											</div>
											<div class="wid_10"> To </div>
											<div class="wid_45">
												<input type="number" class="form-control max-range" name="max-range[]" placeholder="Max Range" value="" required>
											</div>
										</td>
										<td>
											<select class="form-control" name="calculation_type[]" required>
							                    <option value="$">$</option>
							                    <option value="%">%</option>
							                </select>
										</td>
										<td>
											<input type="number" class="form-control" name="fee_value[]" placeholder="Fee Value" value="" required>
										</td>
										<td></td>
									</tr>
								<?php } ?>
				                <tr>
				                	<td colspan="4" align="center">
				                		<button class="btn btn-primary" id="update_point" type="submit">Submit</button>
				                	</td>
				                </tr>
				            </tbody>
			          	</table>
		          	</form>
	            </div>
	        </div>
		</div>
	</div>
</section>