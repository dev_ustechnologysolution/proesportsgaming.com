<html>
<head>
    <style type="text/css">
        .panel-title {
        display: inline;
        font-weight: bold;
        }
        .display-table {
            display: table;
        }
        .display-tr {
            display: table-row;
        }
        .display-td {
            display: table-cell;
            vertical-align: middle;
            width: 61%;
        }
    </style>
</head>
<body>
     

<div class="login-signBlock" style="background-color: #1f1f1f; border: 1px solid #ff5000; width: 680px">
                <h2>Payment Details </br>$<?php echo  number_format($amount,2,".",".");?></h2>
              

                
                  <form action="<?php echo base_url(); ?>authorizesecond/pushPayment/" method="POST">

                                    
                        <div class='form-row row'>
                            <div class='col-xs-12 form-group card required'>
                                <label class='control-label'>Card Number</label>
                                <div class="input-group col-lg-12 col-sm-12 col-md-12"> <input
                                    autocomplete='off' name="card_num"  id="card_num" class='form-control card-number' size='20'
                                    type='text' required="true">
                                </div>
                            </div>
                        </div>
      
                        <div class='form-row row'>
                            <div class='col-xs-12 col-md-4 form-group cvc required'>
                                <label class='control-label'>CVC</label> 
                                <div class="input-group col-lg-8 col-sm-8 col-md-8">
                                <input autocomplete='off'
                                    class='form-control card-cvc' name="cvv" placeholder='ex. 311' size='4'
                                    type='text' required="true" maxlength="3">
                                </div>
                            </div>

                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                 <div class="input-group col-lg-8 col-sm-8 col-md-8">
                                        <label class='control-label'>Expiration Month</label> <input
                                        class='form-control card-expiry-month' name="exp_month" placeholder='MM' size='2'
                                        type='text' required="true" maxlength="2">
                                 </div>
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                <div class="input-group col-lg-8 col-sm-8 col-md-8">
                                    <label class='control-label'>Expiration Year</label> <input
                                    class='form-control card-expiry-year' name="exp_year" placeholder='YY' size='2'
                                    type='text' required="true" maxlength="2">
                                </div>
                            </div>
                        </div>
      
                                <div class='form-row row'>
                                    <div class='col-md-12 error form-group hide'>
                                        <div class='alert-danger alert'>Please correct the errors and try
                                            again.</div>
                                    </div>
                                </div>

                                 <div class="form-group">
                                        <input type="hidden" name="cmd" value="_xclick">
                                        <input type="hidden" name="custom" value='<?php echo $custom ?>'>
                                    </div>
                                    <div class="form-group">
                                        
                                        <input type="hidden" name="invoice" value="<?php echo $item_name ?>">
                                        
                                        
                                    </div>
                                    
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" name="amount" value="<?php echo $amount;?>" readonly>
                                    </div>
                                      
                                    <div class="form-group">
                                        <input type="hidden" name="currency_code" value="USD">
                                    </div>
                                  
                                    <input type="hidden" name="x_receipt_link_url" value="<?php echo base_url('authorize_callback/'); ?>" >
                                    <input type="hidden" name="x_relay_response" value="TRUE" >
                                    <input type="hidden" name="x_relay_url" value="<?php echo base_url('authorize_callback/'); ?>">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <button class="btn btn-primary btn-lg btn-block" type="submit">Pay Now</button>
                                            
                                        </div>
                                    </div>
                    </form>
                
</div>
     
</body>  
</html> 