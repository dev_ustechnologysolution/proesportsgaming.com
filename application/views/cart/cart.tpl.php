<section class="body-middle innerpage">
  <?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
  <div class="container">
    <div class="row header1">
        <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
        <div class="col-lg-4 col-sm-4 col-md-4"><h2>Cart</h2></div>
        <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
    </div>
    <?php $this->load->view('common/product_filter.tpl.php'); ?>    
    <div class="row">
      <div class="cart_product_listing_div common_product_list">
        <?php $i = 0;
        if (!empty($cart_item)) { ?>
          <form method="post" action="<?php echo base_url(); ?>cart/cart_items_update">
            <div class="cart_list_heading text-left clearfix">
              <div class="col-md-1 head_title">Image</div>
              <div class="col-md-3 head_title">Name</div>
              <div class="col-md-2 head_title">Option</div>
              <div class="col-md-2 head_title text-left">Quantity</div>
              <div class="col-md-1 head_title text-left">Price</div>
              <div class="col-md-1 head_title text-left">Processing</div>
              <div class="col-md-1 head_title text-left">Total</div>
              <div class="col-md-1 head_title text-right">Action</div>
            </div>
            <ul class="c_product_list">
              <?php 
              $SUBTOTAL = 0;
              $shipinfo = $vendor_shipping_fee_arr = $admin_item_name_arr = [];
              foreach ($cart_item as $key => $ci) { 
                $item_details = $ci;
                $variation_amount = 0;
                $sel_var_amt_array = explode(', ', $ci['sel_variation_amount']);

                if(count($sel_var_amt_array) > 0){
                  foreach ($sel_var_amt_array as $sel_var_amt) {
                    $sva = explode('&', $sel_var_amt);
                    $variation_amount += end(explode('_', $sva[2]));
                  }
                }
                $item_amount = $item_details['amount'] + $variation_amount;
                $sel_variation = !empty($item_details['sel_variation']) ? explode(', ', $item_details['sel_variation']) : '';
                $item_details['prdcts_total_price'] = number_format((float) ($item_details['qty'] * ($item_amount + $item_details['product_fee'])), 2, '.', '');
                ($ci['product_exist_id'] !='') ? $SUBTOTAL += $item_details['prdcts_total_price'] : '';
                if ($ci['seller_id'] == 0) {
                  $shipping_admin = (strtolower($this->session->userdata('shippaddress')['country']) == 'united states') ? 'inside_us_product_fees' : 'outside_us_product_fees';
                  $admin_item_name_arr[] = $ci['product_name'];
                } else {
                  $vendor_shipping_fee_arr[$ci['countryname']]['local_product_fee'] = $ci['local_product_fee'];
                  $vendor_shipping_fee_arr[$ci['countryname']]['international_product_fee'] = $ci['international_product_fee'];
                  $vendor_shipping_fee_arr[$ci['countryname']]['product_name'][] = $ci['product_name'];
                }
                ?>
                <li class="c_product_list_li item_<?php echo ($this->session->userdata('user_id') != '') ? $item_details['id'] : $key; ?>">
                  <div class="col-md-1 c_prdct_img">
                    <img src="<?php echo base_url().'upload/products/'.$item_details['product_image']; ?>" alt="<?php echo $item_details['product_image']; ?>" class="img img-responsive">
                  </div>
                  <div class="col-md-3 c_prdct_name">
                    <a href="<?php echo base_url() ?>Store/view_product/<?php echo $item_details['product_id']; ?>">
                      <div><?php echo (strlen($item_details['product_name']) > 40) ? substr($item_details['product_name'], 0, 40).'...' : $item_details['product_name']; ?></div>
                    </a>
                    <input type="hidden" name="product_name[]" value="<?php echo $item_details['product_name']; ?>">
                  </div>
                  <div class="col-md-2 c_slct_varation">
                    <?php if (isset($sel_variation) && $sel_variation != '') {
                      foreach ($sel_variation as $k => $sv) {
                        $sv = explode('&', $sv);
                        $var_title = end(explode('_', $sv[0]));
                        $attr_lable = 
                        $var_opt = end(explode('_', $sv[1]));
                        echo '<div><h4><span class="var_title">'.$var_title.'</span> : <span> '.$var_opt.'</span></h4></div>';
                      }
                    } ?>
                  </div>
                  <?php if ($ci['product_exist_id'] !='') { ?>
                    <div class="col-md-2 c_prdct_item select_prdct_qnt text-left">
                      <div>
                        <input type="hidden" class="cart_item_id" name="cart_item_id[]" value="<?php echo ($this->session->userdata('user_id') != '') ? $item_details['id'] : $key; ?>">
                        <input type="hidden" name="product_id[]" value="<?php echo $item_details['product_id']; ?>" class="product_id">
                        <button type="button" id="sub" class="sub cmn_btn"><i class="fa fa-minus"></i></button>
                        <input type="number" name="qty[]" class="form-control prdct_qntity" min="1" pattern="[0-9]+" required value="<?php echo $item_details['qty']; ?>" extra_amount="<?php echo number_format((float)$variation_amount, 2, '.', ''); ?>" readonly>
                        <button type="button" id="add" class="add cmn_btn"><i class="fa fa-plus"></i></button>
                      </div>
                      <div class="item_exceed_err"></div>
                    </div>
                    <div class="col-md-1 c_prdct_price text-left">
                      <lable style="font-size: 22px;"><?php echo '$<span class="itm_amount">'.number_format((float)$item_amount, 2, '.', '').'</span>'; ?></lable>
                    </div>
                    <div class="col-md-1 c_prdct_price text-left">
                      <lable style="font-size: 22px;"><?php echo '$<span class="product_fee">'.$item_details['product_fee'].'</span>'; ?></lable>
                    </div>
                    <div class="col-md-1 c_prdct_totalprice text-left">
                       <lable style="font-size: 22px;"><?php echo '$<span class="prdcts_total_price">'.$item_details['prdcts_total_price'].'</span>'; ?></lable>
                    </div> 
                  <?php } else { ?>
                    <div class="col-md-5 text-center">
                      <h3>Product is not available, <br> Please remove this product.</h3>
                    </div>
                  <?php } ?>
                  <div class="col-md-1 c_prdct_chckt_btn text-right">
                    <a class="cmn_btn" href="<?php echo base_url().'cart/remove_product_cart/'.(($this->session->userdata('user_id') != '') ? $ci['id'] : $key); ?>" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Remove from Cart"><i class="fa fa-trash-o"></i></a>
                  </div>
                </li>
              <?php } ?>
            </ul>
            <?php 
            // checkout calc
            $SUBTOTAL = number_format((float)$SUBTOTAL, 2, '.', '');
            $ship_data = 0;

            // Admin products calculation
            if (!empty($admin_shipping_fee_arr) && isset($shipping_admin) && $shipping_admin != '') {
              foreach ($admin_shipping_fee_arr as $key => $asfa) {
                if ($asfa['option_title'] == $shipping_admin) {
                  $shipping_cal_type = explode('_', $asfa['option_value']); 
                  $shipping_val = ($shipping_cal_type[0] == 1) ? $shipping_cal_type[1]: $SUBTOTAL*$shipping_cal_type[1]/100;
                  $ship_data += $shipping_val;
                }
              }
              $shipinfo[] = array(
                'item_name' => $admin_item_name_arr,
                'shipping_charge' => $shipping_val,
              );
            }
            // vendor products shipping calculation
            if (!empty($vendor_shipping_fee_arr)) {
              foreach ($vendor_shipping_fee_arr as $key => $vsfa) {
                if ($key != '') {
                  $shipping_cal_type = (strtolower($this->session->userdata('shippaddress')['country']) == strtolower($key)) ? explode('_', $vsfa['local_product_fee']) : explode('_', $vsfa['international_product_fee']);
                  $ship_data += ($shipping_cal_type[0] == 1) ? $shipping_cal_type[1] : $SUBTOTAL*$shipping_cal_type[1]/100;
                }
              }
              $shipinfo[] = array(
                'item_name' => $vendor_shipping_fee_arr[$ci['countryname']]['product_name'],
                'shipping_charge' => $shipping_val,
              );
            }
            $SHIPPING = number_format((float)$ship_data, 2, '.', '');
            // $total = number_format((float)($SHIPPING)+($SUBTOTAL), 2, '.', '');
            $total = number_format((float)$SUBTOTAL, 2, '.', '');
            ?>
            <div class="checkout_div text-center">
              <div class="row cnt_upcartbtn margin0">
                <div class="col-md-3"></div>
                <div class="col-md-6 text-right padd0 checkout_calc_div text-center">
                  <a href="<?php echo base_url().'cart/clear_cart/'.$this->session->userdata('user_id'); ?>" class="cmn_btn"> <i class="fa fa-trash-o"></i> Clear Cart</a>
                  <a href="<?php echo base_url(); ?>store" class="cmn_btn"> Continue Shopping </a>
                </div>
                <div class="col-md-3"></div>
              </div>
              <div class="shipping_charge_div clearfix hide">
                <div class="col-sm-12 shippcharge_div">
                  <table class="table-responsive table table-bordered text-left">
                    <thead>
                      <tr>
                        <th class="text-center">Shipping charge</th>
                        <th class="text-center">Product</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if (!empty($shipinfo)) {
                        foreach ($shipinfo as $key1 => $si) {
                          foreach ($si['item_name'] as $key2 => $itm) { ?>
                            <tr>
                              <?php if ($key2 == 0) { ?>
                                <td rowspan="<?php echo count($si['item_name']); ?>"><?php echo '$'.number_format((float)$si['shipping_charge'], 2, '.', ''); ?></td>
                              <?php } ?>
                              <td><?php echo $itm; ?></td>
                            </tr>
                          <?php } ?>
                        <?php }
                      } ?>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="text-center"><h1 style="font-family: Comic Sans MS, cursive, sans-serif;">OR</h1></div>
              <div class="row margin0">
                <div class="col-sm-3"></div>
                <div class="col-sm-6 checkout_calc_div">
                  <div class="title"> <h2 class="pull-left">Cart Total ($)</h2> <h2 class="pull-right"><?php echo '<span class="grand_total">'.$total.'</span>'; ?></h2></div>
                  <div class="checkout_calc_data">
                    <!-- <table class="table table-responsive">
                      <tr>
                        <th>SUBTOTAL ($)</th>
                        <td><?php // echo '<span class="subtotal">'.$SUBTOTAL.'</span>'; ?></td>
                      </tr>
                      <tr>
                        <th><div class="shipping_info_div" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Shipping charge detail">Shipping ($) <span class="shipping_info_icon"><i class="fa fa-info-circle"></i></span></div></th>
                        <td><?php // echo '<span class="shipping_charge">'.$SHIPPING.'</span>'; ?></td>
                      </tr>
                      <tr>
                        <th>Total ($)</th>
                        <td><?php // echo '<span class="grand_total">'.$total.'</span>'; ?></td>
                      </tr>
                    </table> -->
                    <div class="checkout_submit_div">
                      <div class="row">
                        <input type="hidden" name="subtotal" value="<?php echo $SUBTOTAL; ?>">
                        <input type="hidden" name="shipping" value="<?php echo $SHIPPING; ?>">
                        <input type="hidden" name="total" value="<?php echo $total; ?>">
                        <span class="shipping_cal_type hide"><?php echo $shipping_cal_type[0]; ?></span>
                        <span class="shipping_val hide"><?php echo $shipping_val; ?></span>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <a class="cmn_btn" id="type_model" countryname="<?php echo strtoupper(($cart_item[0]['seller_id'] == 0) ? 'United States' : $cart_item[0]['countryname']); ?>">Proceed to Checkout</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-3"></div>
            </div>
            <div class="paddtop20 margin-tpbtm-20"><img src="<?php echo base_url().'assets/frontend/images/playlikepro.png' ?>"></div>
          </div>
          </form>
        <?php } else { ?>
            <h5>
              <div class="emptyitem_lable"><div class='row bidhottest_err'><div class='col-sm-12' style='color: #FF5000;'>Cart is Empty</div></div></div>
            </h5>
            <br>
            <div class="text-center"><a href="<?php echo base_url() ?>store" class="cmn_btn" style="font-size: 20px;"> Go to Store</a></div>
        <?php } ?>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $('#type_model').on('click',function(){
    var countryname = $(this).attr('countryname');
    var senddata = '<div class="col-sm-12"><div class="col-sm-6 inside_us_choose">INSIDE <br>'+countryname+'</div><div class="col-sm-6 outside_us_choose">OUTSIDE <br>'+countryname+'</div></div>';
    var modaltitle = 'Select shipping address location';
    var data = {
      'modal_title': modaltitle,
      'senddata':senddata,
      'tag':'shipping_type'
    }
    commonshow(data);
  });
  $('.shipping_info_div').on('click',function(){
    var senddata = $('.shipping_charge_div').html();
    var modaltitle = 'Shipping info';
    var data = {
      'modal_title': modaltitle,
      'senddata':senddata,
      'tag':'commonmodalshow'
    }
    commonshow(data);
  });
</script>