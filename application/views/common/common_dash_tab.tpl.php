<?php
$my_profile_tab = in_array(strtolower($this->current_class), $this->myprofile_button_active) ? 'orange' : 'black';
$dashboard_tab = in_array(strtolower($this->current_class), $this->dashboard_button_active) ? 'orange' : 'black';
?>
<div class="profile_top row">
  <div class="col-sm-6 text-right">
    <a class="profile_top_btn <?php echo $my_profile_tab; ?>" href="<?php echo base_url('user/myprofile'); ?>">My Profile</a>
  </div>
  <div class="col-sm-6">
    <a class="profile_top_btn <?php echo $dashboard_tab; ?>" href="<?php echo base_url('membership/getMembershipPlan'); ?>">Dashboard</a>
  </div>
</div>