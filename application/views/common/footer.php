<div class="loader" id="loader" <?php echo (current_url() == base_url()) ? 'style="display:none;"': ''; ?>>
  <div class="loader_sub_div">
    <img src="<?php echo base_url(); ?>/assets/frontend/images/ajax-loader.gif">
  </div>
</div>
<div class="modal custmodal commonmodal" style="display: none;">
  <div class="modal-content">
     <div class="modal-header">
        <span class="close">&times;</span>
        <p class="modal_h1_msg text-left"></p>
        <hr>
        <div class="row modal_data">
        </div>
     </div>
  </div>
</div>
<input type="hidden" name="user_id" value="<?php echo $this->session->userdata('user_id'); ?>">
<div id="send_tip_amount_profile" class="modal custmodal send_teamtip_amount_profile" style="display: none;">
    <div class="modal-content">
      <div class="modal-header">
        <span class="close">&times;</span>  
        <h1 class="text-left">Please Enter Donation Amount</h1>
        <div class="modal_div_border row">
          <div class="row team_donate_bottom_div">
            <form id="profile_form" action="<?php echo base_url(); ?>livelobby/send_tip_to_profile" method="POST">
              <div class="col-sm-12">
                <div class="col-sm-6 profile_image_div" style="max-width: 225px; max-height: 225px;">
                  <img src="<?php if($view_lobby_fan_profile->image != null && $view_lobby_fan_profile->image != ''){ echo base_url('/upload/profile_img/'.$view_lobby_fan_profile->image); }else{echo base_url('/upload/profile_img/default_profile_img.png');} ?>" id="k" style="border-radius: 50%;" class="img img-responsive">
                </div>
                <div class="col-sm-6 text-left" style="vertical-align: middle;">
                  <div class="row" style="margin-top: 10px;"><h2 class="padd10"><?php echo (isset($view_lobby_fan_profile->display_name) && $view_lobby_fan_profile->display_name != '') ? $view_lobby_fan_profile->display_name : $view_lobby_fan_profile->name; ?></h2><h1>Account NO #<?php echo $view_lobby_fan_profile->account_no; ?></h1></div>
                  <div class="row">
                    <!-- <label class="text-left">AMOUNT</label> -->
                       <input type="number" class="form-control" name="tip_amount_input_profile" id="tip_amount_input_profile" placeholder="0.00" required>
                  </div><br>
                  <div class="row">
                      <input type="submit" value="Submit" class="lobby_btn tip_submit">
                  </div>
                </div>
                <input type="hidden" name="send_from" value="<?php echo $this->session->userdata('user_id');?>">
                <input type="hidden" name="send_to" value="<?php echo $view_lobby_fan_profile->id; ?>">
                <?php if (isset($_GET['lobby_id']) && $_GET['lobby_id'] != null && $_GET['lobby_id'] != ''){ ?>
                  <input type="hidden" name="lobby_id" value="<?php echo $_GET['lobby_id']; ?>">
                <?php } ?>
              </div>
            </form>
          </div>                  
          <div class="row tip_amount_error_row">
            <div class="col-sm-12">
              <div class="form-group">
                 <div class="input-group col-lg-12">
                    <div class="alert alert-danger tip_amount_error"></div>
                 </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> 
<?php
if (!empty($this->session->userdata('user_id')) && empty($this->session->userdata('timezone'))) { ?>
  <div class="modal custmodal" style="display: block;">
    <div class="modal-content">
       <div class="modal-header">
          <h2 class="modal_h1_msg text-left">Please Select Timezone</h2>
          <hr>
          <div class="modal_data text-left">
            <!-- <div class="col-sm-12"> -->
              <form  action="<?php echo base_url() ?>user/update_timezone" name="store_timezone" id="store_timezone" method="post">
                <div class="form-group paddtop10">
                    <?php echo $this->My_model->get_alltimezone(); ?>
                </div>
                <div class="form-group paddtop10">
                  <label class="chkbx_container " style="left: 0;"><input type="checkbox" name="save_forever"><span class="checkmark"></span>&nbsp;Save Permanent </label>
                </div>
                <div class="form-group paddtop10">
                  <input type="submit" name="submit" class="cmn_btn">
                </div>
              </form>
            <!-- </div> -->
          </div>
       </div>
    </div>
  </div>
<?php } ?>
<footer class="modal-footer">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-sm-6 col-md-4">
        <img src="<?php echo base_url() . 'assets/frontend/'; ?>images/logoo.png" alt="logo">
        <p><?php echo $this->footer_data[0]['content']; ?></p>
      </div>
      <div class="col-lg-2 col-sm-6 col-md-3">
        <h3>USEFUL LINKS</h3>
        <ul>
          <?php foreach ($this->footer_menu as $value) { ?>
            <li><a href="<?php echo base_url() . $value["link"]; ?>"><?php echo $value['title']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
      <div class="col-lg-2 col-sm-6 col-md-2">
        <h3>PLATFORMS</h3>
        <ul>
          <li><a href="<?php echo base_url(); ?>">PLAY STATION 4</a></li>
          <li><a href="<?php echo base_url(); ?>">XBOX 1</a></li>
          <li><a href="<?php echo base_url(); ?>">PC</a></li>
        </ul>
      </div>
      <div class="col-lg-2 col-sm-6 col-md-2">
        <?php foreach ($this->footer_cs as $value) {
          $cs_status = $value['status'];
        } if ($cs_status == 'active') { ?>
          <h3>Customer Service</h3>
          <ul>
            <li>
              <h5>
                <?php
                $CI =& get_instance();
                $result = $CI->chatsocket->getAdminConversation();
                if ($result['status'] == true) {
                  $conversation = $result['users_conversation'];
                  foreach ($conversation as $key => $value) {
                    $user_to = $value['user_to'];
                    $offline = 'offline';
                    $cn_id = $value['cn_id'];
                  } if (isset($_SESSION['user_id']) && isset($_SESSION['user_name'])) {
                    ?>
                    <a class="link_to_open_chat" onclick='OpenPopupCenter("<?php echo base_url(); ?>chat/chat_box/?user_to_from_customer=<?php echo $user_to; ?>")'>OPEN CHAT</a>
                  <?php } else { ?>
                    <a href="<?php echo base_url(); ?>login" class="link_to_open_chat">OPEN CHAT</a>
                  <?php }
                } else {
                  $list = $result['error'];
                } ?>
              </h5>
            </li>
          </ul>
        <?php } ?>
      </div>
      <div class="col-lg-2 col-sm-6 col-md-3 seclast">
        <h3>WE ARE SOCIAL</h3>
        <ul class="social-sec">
          <?php foreach ($this->footer_link as $value) { ?>
            <li>
              <a href="<?php echo $value['link']; ?>" target="_blank"><i class="<?php echo $value['class']; ?>" aria-hidden="true" ></i></a>
            </li>
          <?php } ?>
        </ul>
        <div class="xtraLogo">
          <?php foreach ($this->footer_logo as $value) { ?>
            <a href="#">
              <img src="<?php echo base_url() . 'upload/logo/' . $value['image']; ?>" alt="">
            </a>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-panel">
    <div class="container">
       <div class="row" style="text-align:center;">
          <p>Pro Esports Gaming would like to give this to the world where dreams can be a place of reality.</p>
       </div>
    </div>
  </div>
  <?php
  $CI =& get_instance();
  $socketUser = ($CI->chatsocket->current_user != null && $CI->chatsocket->current_user != '') ? $CI->chatsocket->current_user : 0; ?>
  <?php $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://"; ?>
  <script src="<?php echo $protocol . addhttp($CI->chatsocket->domain_name . ':' . $CI->chatsocket->socket_port . '/socket.io/socket.io.js'); ?>"></script> 
<!--   <link rel="stylesheet" href="<?php // echo base_url('xwb_assets/js/prettyPhoto-3.1.6/css/prettyPhoto.css'); ?>" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
  <script src="<?php // echo base_url('xwb_assets/js/prettyPhoto-3.1.6/js/jquery.prettyPhoto.js'); ?>" type="text/javascript" charset="utf-8"></script> -->
  <link rel="stylesheet" href="<?php echo base_url('xwb_assets/js/select2-4.0.3/dist/css/select2.min.css'); ?>" type="text/css" charset="utf-8" />
  <script src="<?php echo base_url('xwb_assets/js/select2-4.0.3/dist/js/select2.full.min.js'); ?>" type="text/javascript"></script>
  <script type="text/javascript" src="<?php echo base_url('xwb_assets/js/bootbox.min.js'); ?>"></script>
  <script type="text/javascript">
    if(typeof io != 'undefined') {
      var socket = io.connect("<?php echo addhttp($CI->chatsocket->domain_name . ':' . $CI->chatsocket->socket_port); ?>",{secure: true}); //connect to socket
    }
    var varSCPath = "<?php echo base_url('xwb_assets'); ?>";
    var socketUser = "<?php echo $socketUser; ?>";
    window.formKey = "<?php echo csFormKey(); ?>";
    if(typeof socket != 'undefined'){
      socket.emit( "socket id", { "user": socketUser } ); // pass the user identity to node
    }
  </script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/giveaway.js"></script>
  <script language="javascript" type="text/javascript">
    // $('#store_timezone').on('submit', function () {
    //   if ($(this).find('select[name="timezone"]').val() == '') {
    //     alert('Please select Timezone')
    //     return false;
    //   }
    // });
    function OpenPopupCenter(pageURL) {
      var targetWin = window.open(pageURL);
      targetWin.location = pageURL;
    }
    $(function () {
      $('#myCarousel').carousel({
        interval:2000,
        pause: "false"
      });
      $('#playButton').click(function () {
        $('#myCarousel').carousel('cycle');
        $("#playButton").removeClass('d-inline').addClass('none');
        $("#pauseButton").removeClass('none').addClass('d-inline');
      });
      $('#pauseButton').click(function () {
        $('#myCarousel').carousel('pause');
        $("#pauseButton").removeClass('d-inline').addClass('none');
        $("#playButton").removeClass('none').addClass('d-inline');
      });
    }); 
  </script>
</footer>