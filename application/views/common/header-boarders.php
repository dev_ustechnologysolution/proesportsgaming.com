<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- <link rel="icon" href="<?php //echo base_url().'assets/frontend/';?>../../favicon.ico"> -->
    <meta https-equiv="refresh" content="86400;url=<?php echo base_url(); ?>/login/logout/" />
	<title>PRO ESPORTS GAMING</title>

    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url() ?>assets/frontend/images/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url() ?>assets/frontend/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url() ?>assets/frontend/images/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>assets/frontend/images/favicon/favicon-16x16.png">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

  	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/frontend/';?>css/bootstrap.css">
 	<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/chat.css">
	<link href="https://fonts.googleapis.com/css?family=Squada+One" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/frontend/';?>css/set1.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/frontend/';?>css/custom.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/frontend/';?>css/custom_resonsive.css">
	<script src="<?php echo base_url().'assets/frontend/js/'; ?>countdown.js" type="text/javascript"></script>
  </head>
  	<body>
    	<div class="navbar-wrapper header-wrapper">
      		<div class="container">
        		<nav class="navbar navbar-inverse navbar-static-top">
          			<div class="container">
            			<div class="navbar-header">
              				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
              				<a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url().'assets/frontend/';?>images/logo.png" alt="logo"></a>
						</div>
						<div class="login-sec">
						<?php if($this->session->userdata('user_id')=='') { ?>
							<ul class="befor-login">
                    			<li><a href="<?php echo base_url().'login' ?>">login</a></li>
								<li><a href="<?php echo base_url().'signup' ?>">Signup</a></li>
							</ul>
						<?php } else { 
							$num1 = $this->session->userdata('total_balance');
							$num1 = number_format((float)$num1, 2, '.', '');
							echo '<div style="position: absolute;color: white;right: 16%;font-size: 20px;" class="notification header_available_balance"> Available balance : <span class="tot-balance">$'.  $num1. '</span></div>';     
							$pending_review = $this->General_model->pending_game_review();
							$pendingrequest_friendlist_count = count($this->General_model->pendingrequest_friendlist());
							$pending_unread_mail_count = count($this->General_model->get_all_unread_mails($_SESSION['user_id']));
							$pending_mail_count=$pending_unread_mail_count+$pendingrequest_friendlist_count;
							$online_friendlist_count = count($this->General_model->online_friendlist($_SESSION['user_id']));
				 			?>
							<a class="notification mail-notification" href="#">
								<i class="fa fa-envelope" aria-hidden="true"></i>
								<span style="top: 18px; position: absolute;" class="header_pending_review"><?php echo $pending_mail_count; ?></span>
							</a>				
							<a class="notification pending_review-notification" href="#">
								<i class="fa fa-bell" aria-hidden="true"></i>
								<span style="top: 18px; position: absolute;" class="header_pending_review"><?php echo count($pending_review); ?></span>
							</a>
							<a class="notification friend-request-notification" href="#">
								<i class="fa fa-user" aria-hidden="true"></i>
								<span style="top: 18px; position: absolute;" class="header_pending_review"><?php print_r($online_friendlist_count); ?></span>
							</a>
							<ul class="noticlist pending_review-list">
								<li><h5>Notifications</h5></li>
								<li class="store_li"><a href="<?php echo site_url().'Store';?>"> <img src="<?php echo base_url().'assets/frontend/';?>images/store_icon.png" height="15" width="15" alt="store">&nbsp; Store</a></li>
								<li><h5>Pending Result</h5></li>
								
								<?php if(!empty($pending_review)) { ?>
									<?php foreach($pending_review as $pend_review) { ?>
									<li>
										<a href="<?php echo site_url().'dashboard'; ?>"><span class="noti-ico"><img src="<?php echo base_url().'assets/frontend/';?>images/noimg1.png" alt="game"></span> <p><?php  echo $pend_review['game_name']; ?></p></a>
									</li>
									<?php } ?>
								<?php }
								else{
									echo '<li><a><p>No Pending Result <span style="color: #d6d1d1"></span></p></a></li>';
								}
								?>					
							</ul>
							<ul class="noticlist frndrqstlist">
								<li><h5>Online Friends</h5></li>
								<?php
								$online_friends = $this->General_model->online_friendlist($_SESSION['user_id']);
								if(!empty($online_friends)) { 
									foreach($online_friends as $olfrnds) { ?>
									<li>
										<a href="<?php echo site_url().'friend/'; ?>"> <p> 
											<?php if (!empty($olfrnds['image']) || $olfrnds['image'] != '') { ?>
												<img src="<?php echo base_url().'upload/profile_img/'.$olfrnds['image']; ?>" class="online_friends_profile_pic"> <?php } else {?>
												<img src="<?php echo base_url().'upload/profile_img/profile_placeholder.jpg'; ?>" class="online_friends_profile_pic"> <?php } ?> <span style="color: #d6d1d1"> <?php  echo $olfrnds['name']; ?></span></p></a>							 
									</li>
									<?php } 
								}
								else{
									echo '<li><a><p>No Online Friends <span style="color: #d6d1d1"></span></p></a></li>';
								}
								?>					
							</ul>
							<ul class="noticlist mailnotlist">
								<li><h5>Unread mail</h5></li>
								<?php
								$pending_unread_mail = $this->General_model->get_all_unread_mails($_SESSION['user_id']);
								if(!empty($pending_unread_mail)) { ?>
									<?php foreach($pending_unread_mail as $pend_review) { ?>
									<li>
										<a href="<?php echo site_url().'mail/'; ?>"> <p>New Mail from <span style="color: #d6d1d1"> <?php  echo $pend_review['name']; ?></span></p></a>
									</li>
									<?php } ?>
								<?php }
								else{
									echo '<li><a><p>No mails <span style="color: #d6d1d1"></span></p></a></li>';
								} ?>
								<li><h5>Pending Friend Request</h5></li>
								<?php
								$pending_friendlist = $this->General_model->pendingrequest_friendlist();
								if(!empty($pending_friendlist)) { ?>
									<?php foreach($pending_friendlist as $pend_review) { ?>
									<li>
										<a href="<?php echo site_url().'friend/pendingrequest_friendlist/'; ?>"> <p>New Friend Request from <span style="color: #d6d1d1"> <?php  echo $pend_review['name']; ?></span></p></a>
									</li>
									<?php } ?>
								<?php } else {
									echo '<li><a><p>No Pending Request <span style="color: #d6d1d1"></span></p></a></li>';
								}
								?>	

							</ul>
							<a id="loginDiv">
								<span class="userImg">
									<img src="<?php echo base_url().'upload/profile_img/'. $this->session->userdata('user_image');?>" alt="logo">
								</span>
								<span><i class="fa fa-caret-down" aria-hidden="true"></i></span>
							</a>
							<div id="expandDiv">
								<ul>
									<li><a href="<?php echo base_url().'user/myprofile'; ?>">My Profile</a></li>
									<li><a href="<?php echo base_url().'game/Newgame'; ?>">Dashboard</a></li>
									<li><a href="<?php echo base_url('livelobby/lobbyadd') ?>">Create Lobbies</a></li>
									<li><a href="<?php echo base_url('dashboard') ?>">My Lobbies</a></li>
									<li><a href="<?php echo base_url('friend') ?>">Friends</a></li>
									<li><a href="<?php echo base_url('mail') ?>">Mail</a></li>
									<li><a href="<?php echo base_url('user/profile') ?>">Settings</a></li>
									<li><a href="<?php echo base_url('login/logout') ?>">Logout</a></li>
								</ul>
							</div>
							<?php  } ?>
						</div>							
					</div>
				</nav>
			</div>
		</div> 
		<?php
			$data = $this->router->fetch_class();
			// echo $data;
			$method = $this->router->fetch_method();
			if($data == "Home" )
			{	$banner_data =$this->General_model->banner_order();
			} else if($data == "challenge_mail" || $data == "user" || $data == "page"|| $data == "store" || $data == "sendmoney" || $data == "points" || $data == "mail" || $data == "game" || $data == "friend" || $data == "dashboard" || $data == "Ranking" || $data == "Matches" || $data == "Live_lobby" || $data == "Challenge_mail"){

				if($method != 'gameDetails'){
					$banner_data =$this->General_model->tab_banner();
				}
			} else if($data == "chat") {
				$banner_data =$this->General_model->chat_banner();
			}else {
				$banner_data =array();
			}
			$class = '';
			if(empty($banner_data)){
				$class = 'margin_top';
			}

			if(!empty($banner_data)) { 
			?>
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<?php $i=0; 
						foreach ($banner_data as $value) { ?>
							<li data-target="#myCarousel" data-slide-to="<?php echo $i;?>" <?php if($i==0) echo 'class="active"'; ?>></li>
						<?php $i++; } ?>
				</ol>
				<div class="carousel-inner" role="listbox">
					<?php
						$i=0; foreach ($banner_data as $value) { ?>
					<?php if ($value['banner_video'] != '' && $value['banner_video_duration'] != '') {
							?>
					<div class="item <?php if($i==0) echo 'active'; ?>" id="slider_backvideo" data-interval="<?php echo $value['banner_video_duration']; ?>000">
						<video id="slider_video" width="100%">
							<source src="<?php echo base_url().'upload/banner/'.$value['banner_video']?>">
						</video>
						<span style="display: none;" id="banner_video_duration"><?php echo $value['banner_video_duration']; ?></span>
						<?php } else {
							?>
						<div class="item <?php if($i==0) echo 'active'; ?>" data-interval="4000">
							<img src="<?php echo base_url().'upload/banner/'.$value['banner_image'];?>" alt="">
							<?php } ?>
							<div class="container">
								<div class="carousel-caption">
									<h1><?php echo $value['banner_name'] ?></h1>
									<p><?php echo $value['banner_content'] ?></p>
								</div>
							</div>
						</div>
						<?php $i++; } ?>
					</div>
				</div>
			</div>            
			<?php }
			?>
			<div id="navbar" class="navbar-collapse  collapse menu_div <?php echo $class;?>">
				<div class="animated-box in">
				<ul class="nav navbar-nav ">        
					<!-- <li <?php // if($this->uri->segment(1)=='VR') echo 'class="active"';?>><a href="<?php // echo base_url().'VR' ?>">VR</a></li>
						<li <?php // if($this->uri->segment(1)=='PcTab') echo 'class="active"';?>><a href="<?php // echo base_url().'PcTab' ?>">PC</a></li>
						<li <?php // if($this->uri->segment(1)=='Ps4') echo 'class="active"';?>><a href="<?php // echo base_url().'Ps4' ?>">PS4</a></li>
						<li <?php // if($this->uri->segment(1)=='Xbox') echo 'class="active"';?>><a href="<?php // echo base_url().'Xbox' ?>">XBOX 1</a></li> -->
						<li <?php if($this->uri->segment(1)=='Ranking') echo 'class="active"';?>><a href="<?php echo base_url().'Ranking' ?>">World Ranking</a></li>
						<li <?php if($this->uri->segment(1)=='Tournaments') echo 'class="active"';?>><a href="#">Tournaments</a></li>
						<li <?php if($this->uri->segment(1)=='Store') echo 'class="active"';?>><a href="<?php echo base_url().'Store' ?>">Pro Esports Store</a></li>
						<li <?php if($this->uri->segment(1)=='Matches') echo 'class="active"';?>><a href="<?php echo base_url().'Matches' ?>">Matches</a></li>
						<li <?php if($this->uri->segment(1)=='Live_lobby') echo 'class="active"';?>><a href="<?php echo base_url() .'Live_lobby' ?>">Live Lobby</a></li>
						<li <?php if($this->uri->segment(1)=='home' || $this->uri->segment(1)=='') echo 'class="active"';?>><a href="<?php echo base_url() ?>">Home</a></li>
					<!--<li <?php // if($this->uri->segment(1)=='Playgame') echo 'class="active"';?>><a href="<?php // echo base_url().'Playgame' ?>">Sponsor</a></li>
						<li <?php // if($this->uri->segment(1)=='Newgame') echo 'class="active"';?>><a href="<?php // echo base_url().'Newgame' ?>">Coming Soon</a></li>  -->
				</ul>
			</div>
			</div> 

<style>
.margin_top {
	margin-top:100px;
}
</style>
<script>
function CommaFormatted(amount) {
	var delimiter = ","; // replace comma if desired
	var a = amount.split('.',2)
	var d = a[1];
	var i = parseInt(a[0]);
	if(isNaN(i)) { return ''; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	var n = new String(i);
	var a = [];
	while(n.length > 3) {
		var nn = n.substr(n.length-3);
		a.unshift(nn);
		n = n.substr(0,n.length-3);
	}
	if(n.length > 0) { a.unshift(n); }
	n = a.join(delimiter);
	if(d.length < 1) { amount = n; }
	else { amount = n + '.' + d; }
	amount = minus + amount;
	return amount;
}
</script>