<div class="animated-box">
   <div class="row filter-panel home_page_search_tabbing">
      <div class="col-lg-12">
         <div class="form-group">
            <?php if (count($lobby_list) > 0) { ?>
               <select name="lobby_system" id="lobby_system" class="form-control" onchange="lobby_game_search()">
                  <option class="select_title" value="">Please select a System</span></option>
                  <?php foreach($systemList as $val) { ?>
                     <option value="<?php echo $val['id'];?>"><?php echo $val['category_name'];?></option>
                  <?php } foreach($custsysList as $csl) { ?>
                     <option value="<?php echo $csl['id']; ?>"><?php echo $csl['category_name'];?></option>
                  <?php } ?>
               </select>
               <select class="form-control hide" name="lobby_gamesize"  id="lobby_gamesize" onchange="lobby_game_search()">
                  <option value="">Please select a Gamesize</option>
                  <?php for ($i = 1; $i <= 16; $i++) { ?>
                     <option value="<?php echo $i;?>" class="playstore_gamesize_option">
                        <?php echo '--' . $i . 'V' . $i . '--';?>
                     </option>
                  <?php } ?>
               </select>
               <select name="lobby_game" id="lobby_game" class="form-control hide" onchange="lobby_game_search()">
                  <option value="">Please select a Game</option>
               </select>
               <select class="form-control hide" name="lobby_subgame" id="lobby_subgame" onchange="lobby_game_search()">
                  <option value="">Please select a Subgame</option>
               </select>
               <div class="input-group add-on form-control" style="float: right;">
                  <input placeholder="Search Tag/Game Name" name="lobby_srch-term" id="lobby_srch-term" type="text"  onkeyup="lobby_game_search()" onkeydown="lobby_game_search()"><span class="glyphicon glyphicon-search form-control-feedback"></span>
               </div>
            <?php } ?>
         </div>
      </div>
   </div>
</div>