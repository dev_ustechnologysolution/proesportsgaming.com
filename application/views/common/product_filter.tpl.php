<div class="row padding-top-50 prdct_search_filter">
    <div class="col-lg-12 animated-box">
        <div class="row filter-panel home_page_search_tabbing store_page_filter">
            <?php $action = ($customstore != '') ? base_url().'Store/'.$customstore.'/?search_product=true' : base_url().'Store/?search_product=true' ?>
            <form method="post" action="<?php echo $action; ?>">
                <div class="form-group">
                    <select name="main_category" id="main_category" name="main_category" class="form-control">
                        <option class="select_title" value="">Please select Main Category</option>
                        <?php foreach ($category_list as $key => $cl) {
                            $select = ($selected_main_cat == $cl['id'])? 'selected' : '';
                            echo ($cl['parent_id'] == 0)? '<option value="'.$cl['id'].'" '.$select.'>'.$cl['name'].'</option>' : '';
                        } ?>
                    </select>
                    <select class="form-control" id="main_sub_cat" name="main_sub_cat">
                       <option value="">Please select a Sub Category</option>
                        <!-- <?php // foreach ($category_list as $key => $cl) {
                            // $select = ($selected_main_sub_cat == $cl['id'])? 'selected' : '';
                            // echo ($cl['parent_id'] != 0)? '<option value="'.$cl['id'].'" '.$select.'>'.$cl['name'].'</option>' : '';
                        //} ?> -->
                    </select>
                    <div class="input-group add-on form-control">
                       <input type="text" placeholder="Search Product" id="product_srch-term" name="product_srch-term" class="srch-input_term" value="<?php echo ($inputed_product_srch_term !='') ? $inputed_product_srch_term : ''; ?>">
                    </div>
                    <div class="input-group search_prdt_btn_div">
                        <input type="hidden" name="hidden_main_category" value="<?php echo $selected_main_cat; ?>">
                        <input type="hidden" name="hidden_main_sub_cat" value="<?php echo $selected_main_sub_cat; ?>">
                        <input type="hidden" name="hidden_product_srch-term" value="<?php echo $inputed_product_srch_term; ?>">
                        <button type="button" id="search_gear_btn" class="search_prdt_btn"><span class="search_icon glyphicon glyphicon-search" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Click to search"></span> </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>