<?php
$data = $this->router->fetch_class();
if($data == "Home" )
{	$banner_data =$this->General_model->banner_order();
} else if($data == "Ranking"){
    $banner_data =$this->General_model->tab_banner();
}
if(!empty($banner_data)) { 
?>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php $i=0; 
                foreach ($banner_data as $value) { ?>
                    <li data-target="#myCarousel" data-slide-to="<?php echo $i;?>" <?php if($i==0) echo 'class="active"'; ?>></li>
                <?php $i++; } ?>
        </ol>
        <div class="carousel-inner" role="listbox">
            <?php
                $i=0; foreach ($banner_data as $value) { ?>
            <?php if ($value['banner_video'] != '' && $value['banner_video_duration'] != '') {
                    ?>
            <div class="item <?php if($i==0) echo 'active'; ?>" id="slider_backvideo" data-interval="<?php echo $value['banner_video_duration']; ?>000">
                <video id="slider_video" width="100%">
                    <source src="<?php echo base_url().'upload/banner/'.$value['banner_video']?>">
                </video>
                <span style="display: none;" id="banner_video_duration"><?php echo $value['banner_video_duration']; ?></span>
                <?php } else {
                    ?>
                <div class="item <?php if($i==0) echo 'active'; ?>" data-interval="4000">
                    <img src="<?php echo base_url().'upload/banner/'.$value['banner_image'];?>" alt="">
                    <?php } ?>
                    <div class="container">
                        <div class="carousel-caption">
                            <h1><?php echo $value['banner_name'] ?></h1>
                            <p><?php echo $value['banner_content'] ?></p>
                        </div>
                    </div>
                </div>
                <?php $i++; } ?>
            </div>
        </div>
                
                <?php } ?>