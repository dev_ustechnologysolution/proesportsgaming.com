<?php $this->load->view('common/cover-header'); ?>
<?php $host_split = explode('.',$_SERVER['HTTP_HOST']); ?>
<section class="cover-page">
    <div class="container" style="width: 100%;">
        <div class="row coverpage_des_div">
            <div class="coverlogo">
                <div class="horizontle_logo clearfix" style="max-width: 40%;">
                    <img class="img img-responsive" src="<?php echo base_url() ?>assets/frontend/images/horizontle_logo.png" alt="smile" style="width: 100%;">
                </div>
            </div>
            <?php if ($host_split[0] == 'proesportsgaming' || $host_split[0] == 'localhost') { ?>
                <div class="closedmain_div clearfix">
                    <div class="site_closed_main_div">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="sitecloser_gif_img">
                                    <img src="<?php echo base_url(); ?>assets/frontend/images/sad1.gif" alt="smile" class="img img-responsive">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="closed_tag">
                                    <h1>IS CLOSED</h1>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="sitecloser_gif_img">
                                    <img src="<?php echo base_url(); ?>assets/frontend/images/sad2.gif" alt="smile" class="img img-responsive">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="login-signBlock clearfix">
                    <div>
                        <p><span style="color: red;">" All Events Are Postponed Until Further Notice " <br> For All Inquiries Contact: 24-7service@ProEsportsGaming.com</span></p>
                    </div>
                    <h2>Enter Password</h2>
                    <form role="form" class="signup-form has-validation-callback" action="<?php echo base_url();?>Home/cover_page" method="POST">
                        <!-- signup-form has-validation-callback -->
                        <div class="form-group">
                            <h5 style="color: red; text-align: center;"><?php echo $_SESSION['cover_page_password_error']; unset($_SESSION['cover_page_password_error']);?></h5>
                            <div class="input-group col-lg-12 col-sm-12 col-md-12">
                                <input type="password" class="form-control" name="password">
                            </div>
                        </div>
                        <div class="btn-group">
                            <input type="submit" value="Procced to site" class="btn-submit">
                        </div>
                    </form>
                </div>
            <?php } else { ?>
                <div class="login-signBlock clearfix" style="margin-top: 80px;">
                    <h2>Enter Password</h2>
                    <form role="form" class="signup-form has-validation-callback" action="<?php echo base_url();?>Home/cover_page" method="POST">
                        <!-- signup-form has-validation-callback -->
                        <div class="form-group">
                            <h5 style="color: red; text-align: center;"><?php echo $_SESSION['cover_page_password_error']; unset($_SESSION['cover_page_password_error']);?></h5>
                            <div class="input-group col-lg-12 col-sm-12 col-md-12">
                                <input type="password" class="form-control" name="password">
                            </div>
                        </div>
                        <div class="btn-group">
                            <input type="submit" value="Procced to Developing site" class="btn-submit">
                        </div>
                        <div class="form-group">
                            <p><span><a href="https://proesports.com/"> <i class="fa fa-link"></i> Go to Original site</a></span></p>
                        </div>
                    </form>
                </div>
            <?php } ?>
        </div>  
    </div>
</section>