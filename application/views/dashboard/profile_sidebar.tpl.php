<div class="col-lg-2 adm-panel">
    	<div class="adm-profile">
            <div>
                <img src="<?php echo($this->session->userdata('user_image'))? base_url().'upload/profile_img/'.$this->session->userdata('user_image') : base_url().'upload/profile_img/noimg.png'; ?>" alt="logo">
            </div>
            <div class="usrname" style="margin-bottom: 5px;"><?php echo ($_SESSION['display_name'] != '') ? $this->session->userdata('display_name') : $this->session->userdata('user_name'); ?></div>
            <?php $userdata = $this->General_model->view_data('user_detail',array('user_id'=>$this->session->userdata('user_id'))); ?>
            <div class="last_login">Account # <?php echo $userdata[0]['account_no']; ?></div>
        </div>
        <div class="adminList">
        	<ul>

                <!-- <li <?php //if($this->uri->segment('2')=='myprofile') echo 'class="active"'; ?>><a href="<?php //echo base_url().'user/myprofile'; ?>"><i class="fa fa-user" aria-hidden="true"></i> My profile </a></li>

                <li <?php //if($this->uri->segment('2')=='profile') echo 'class="active"'; ?>><a href="<?php //echo base_url().'game/newgame'; ?>"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard </a></li> -->

                <li <?php if($this->uri->segment('2')=='gameadd' || $this->uri->segment('2')=='lobbyadd' || $this->uri->segment('2')=='newgame') echo 'class="active"'; ?>><a href="<?php echo base_url().'livelobby/lobbyadd'; ?>"><i class="fa fa-flag-o" aria-hidden="true"></i> Create Stream</a></li> 

                <li <?php if($this->uri->segment('1')=='dashboard') echo 'class="active"'; ?>><a href="<?php echo base_url().'dashboard'; ?>"><i class="fa fa-list" aria-hidden="true"></i> My Stream</a></li> 
                <li <?php if($this->uri->segment('2')=='getUserEvents') echo 'class="active"'; ?>><a href="<?php echo base_url().'points/getUserEvents'; ?>"><i class="fa fa-list" aria-hidden="true"></i> My Events</a></li> 


                <li <?php if($this->uri->segment('2')=='buypoints') echo 'class="active"'; ?>><a href="<?php echo base_url().'points/buypoints'; ?>"><i class="fa fa-cart-plus" aria-hidden="true"></i> Purchase Funds </a></li>
                <li <?php if($this->uri->segment('2')=='history') echo 'class="active"'; ?>><a href="<?php echo base_url().'points/history'; ?>"><i class="fa fa-history" aria-hidden="true"></i>  Purchase History</a></li>


                <li <?php if($this->uri->segment('2')=='withdrawal') echo 'class="active"'; ?>><a href="javascript:void(0);" id = "withdraw_modal_menu"><i class="fa fa-bell" aria-hidden="true"></i>  Withdrawal</a></li>
                

                <?php
                $urisegment = strtolower(($this->router->fetch_method() == 'index') ? $this->router->fetch_class() : $this->router->fetch_method());
                $store_url_segarr = array('order','wishlist','products', 'my_store', 'category', 'player_orders');
                ?>
                <li id="my_order_li" class="togglemenu hover_effect" style="cursor: pointer;"><a class="show_store_opt" toggle_tg="my_store_exp"><i class="fa fa-user" aria-hidden="true"></i> <span>&nbsp; My Store Account </span> &nbsp; <span class="caret_icn"><?php echo (in_array($urisegment, $store_url_segarr)) ? '<i class="fa fa-caret-up"></i>' : '<i class="fa fa-caret-down"></i>'; ?></span></a></li>
                <div class="row submenu_div">
                    <div class="col-sm-12 sublist toogle_div my_store_exp" <?php echo (in_array($urisegment, $store_url_segarr)) ? 'style="display: block;"' : 'style="display: none;"'; ?>>  
                        <li class="<?php echo ($urisegment == 'my_store') ? 'active' : ''; ?>"><a href="<?php echo base_url().'store/my_store'; ?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp; My Store </a></li>                        

                        <li class="<?php echo ($urisegment == 'products') ? 'active' : ''; ?>"><a href="<?php echo base_url().'products'; ?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp; Sell Products </a></li>

                        <li id="my_order_li" class="hover_effect <?php echo ($urisegment == 'order') ? 'active' : ''; ?>"> <a href="<?php echo base_url().'order'; ?>" src="<?php echo base_url() ?>assets/frontend/images/my_order_icon_orange.png" hover_src="<?php echo base_url() ?>assets/frontend/images/my_order_icon_white.png" data-toggle="collapse" data-target="#demo"><img src="<?php echo ($urisegment == 'order') ? base_url().'assets/frontend/images/my_order_icon_white.png' : base_url().'assets/frontend/images/my_order_icon_orange.png'; ?>" class="my_order_icon" height="20" width="20" >&nbsp; My Orders </a></li>

                        <li id="my_order_li" class="hover_effect <?php echo ($this->uri->segment('2')=='player_orders') ? 'active' : ''; ?>"> <a href="<?php echo base_url().'order/player_orders'; ?>" src="<?php echo base_url() ?>assets/frontend/images/my_order_icon_orange.png" hover_src="<?php echo base_url() ?>assets/frontend/images/my_order_icon_white.png" data-toggle="collapse" data-target="#demo"><img src="<?php echo ($this->uri->segment('2')=='player_orders') ? base_url().'assets/frontend/images/my_order_icon_white.png' : base_url().'assets/frontend/images/my_order_icon_orange.png'; ?>" class="my_order_icon" height="20" width="20" >&nbsp; Player Orders </a></li>

                        <li class="<?php echo ($urisegment == 'category') ? 'active' : ''; ?>"><a href="<?php echo base_url().'category'; ?>"><i class="fa fa-bars" aria-hidden="true"></i>&nbsp; Category </a></li>

                        <li id="my_order_li" class="hover_effect <?php echo ($urisegment == 'wishlist') ? 'active' : ''; ?>"><a href="<?php echo base_url().'store/wishlist'; ?>"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp; Wishlist </a></li>
                    </div>
                </div>

                <li id="sendmoney_li" class="hover_effect <?php echo ($this->uri->segment('1')=='sendmoney') ? 'active' : ''; ?>"><a href="<?php echo base_url().'sendmoney'; ?>" src="<?php echo base_url() ?>assets/frontend/images/send_money.png" hover_src="<?php echo base_url() ?>assets/frontend/images/send_money_white.png"><img src="<?php echo base_url().'assets/frontend/images/send_money.png'; ?>" class="customer_service_icon" height="20" width="20" >&nbsp; Trade Money</a></li>

                <li id="trade_li" <?php if($this->uri->segment('2')=='tradeHistory') echo 'class="active"'; ?>><a href="<?php echo base_url().'trade/tradeHistory'; ?>"><i class="fa fa-exchange"></i>&nbsp; Trade History</a></li>

                <li id="trade_li" <?php if($this->uri->segment('2')=='tipHistory') echo 'class="active"'; ?>><a href="<?php echo base_url().'trade/tipHistory'; ?>"><i class="fa fa-money"></i>&nbsp; Tip History</a></li>

            </ul>

        </div>

    </div>
