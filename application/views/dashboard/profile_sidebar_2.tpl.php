<div class="col-lg-2 adm-panel">
    <div class="adm-profile">
        <div>
            <img src="<?php echo($this->session->userdata('user_image'))? base_url().'upload/profile_img/'.$this->session->userdata('user_image') : base_url().'upload/profile_img/noimg.png'; ?>" alt="logo">
        </div>
        <div class="usrname" style="margin-bottom: 5px;"><?php echo ($_SESSION['display_name'] != '') ? $this->session->userdata('display_name') : $this->session->userdata('user_name'); ?></div>
        <?php $userdata = $this->General_model->view_data('user_detail',array('user_id'=>$this->session->userdata('user_id'))); ?>
        <div class="last_login">Account # <?php echo $userdata[0]['account_no']; ?></div>
    </div>
    <div class="adminList">
        <ul>

                <!-- <li <?php //if($this->uri->segment('2')=='myprofile') echo 'class="active"'; ?>><a href="<?php echo base_url().'user/myprofile'; ?>"><i class="fa fa-user" aria-hidden="true"></i> My profile </a></li>

                <li <?php //if($this->uri->segment('2')=='profile') echo 'class="active"'; ?>><a href="<?php echo base_url().'game/newgame'; ?>"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard </a></li> -->

                <li <?php if($this->uri->segment('1')=='membership') echo 'class="active"'; ?> class="membership_li"><a href="<?php echo base_url().'membership/getMembershipPlan'; ?>" style="padding:4px 8px;"><img height ="30" class="customer_service_icon" <?php echo ($this->uri->segment('1')=='membership')?'src="'.base_url().'assets/frontend/images/gaming_white.png"':'src="'.base_url().'assets/frontend/images/gaming_orange.png"';?> >&nbsp;<span style="width: 73%;display: inline-block;vertical-align: middle;">Player Membership</span></a></li>

                <li <?php if($this->uri->segment('1')=='friend') echo 'class="active"'; ?>><a href="<?php echo base_url().'friend'; ?>"><i class="fa fa-users" aria-hidden="true"></i>&nbsp; Friends</a></li>

                <li class="team_li" <?php if ($this->uri->segment('1') == 'team') echo 'class="active"'; ?>><a href="<?php echo base_url().'friend'; ?>"><img src="<?php echo base_url() ?>assets/frontend/images/team_icon.png" class="customer_service_icon" height="20" width="20" >&nbsp; Esports Team</a></li>                

                <li <?php if($this->uri->segment('1')=='mail' || $this->uri->segment('1')=='challenge_mail') echo 'class="active"'; ?>><a href="<?php echo base_url().'mail'; ?>"><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp; Mail </a></li>

                <li id="subscription_li" <?php if($this->uri->segment('2')=='subscriptionlist') echo 'class="active"'; ?>><a href="<?php echo base_url().'Subscribeuser/subscriptionlist'; ?>"><i class="fa fa-newspaper-o"></i>&nbsp; My Subscriptions</a></li>


                <li <?php echo ($this->uri->segment('1')=='Streamsetting' && $this->uri->segment('2')!='savedStreams')?'class="active"':'class="stream_li"';?>><a href="<?php echo base_url(); ?>Streamsetting"><img <?php echo ($this->uri->segment('1')=='Streamsetting' && $this->uri->segment('2')!='savedStreams')?'src="'.base_url().'assets/frontend/images/broadcast_white.png"':'src="'.base_url().'assets/frontend/images/broadcast.png"';?> class="customer_service_icon" height="20" width="20" >&nbsp; Stream Settings</a></li>

                <li <?php echo ($this->uri->segment('2')=='savedStreams')?'class="active"':'class=""';?>><a href="<?php echo base_url(); ?>Streamsetting/savedStreams"><i class="fa fa-file-video-o" aria-hidden="true"></i>&nbsp; Saved Streams</a></li>

                <li <?php if($this->uri->segment('2')=='profile') echo 'class="active"'; ?>><a href="<?php echo base_url().'user/profile'; ?>"><i class="fa fa-cogs" aria-hidden="true"></i>&nbsp; Settings</a></li>
            </ul>

        </div>

    </div>
