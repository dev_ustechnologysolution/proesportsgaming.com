<footer id="footer">
	<div class="wrap">
    	<div class="footer-top">
        	<div class="cols cols3">
            	<div class="col">
                	<div class="addressindia" data-aos="zoom-in" data-aos-delay="700">
                    	<h3>INDIA Office</h3>
                        <ul>
                        	<li class="faddress">611 Golden Triangle, Near Sardar Patel Stadium, Navarangpura, Ahmedabad-380009</li>
                            <li class="fphone"><a href="callto:+919998328318">+91 9998328318</a></li>
                            <li class="fmail"><a href="mailto:info@aspireedge.com">info@aspireedge.com</a></li>
                        </ul>
                    </div><!--/.addressindia-->
                </div><!--/.col-->
                <div class="col">
                	<div class="addressusa" data-aos="zoom-in" data-aos-delay="900">
                    	<h3>USA Office</h3>
                        <ul>
                        	<li class="faddress">180 Elm Ct, Sunnyvale, CA 94086,<br> USA</li>
                            <li class="fphone"><a href="callto:+1(480)352-6125">+1 (480) 352-6125</a></li>
                            <li class="fmail"><a href="mailto:solanki.rupali10@gmail.com">solanki.rupali10@gmail.com</a></li>
                        </ul>
                    </div><!--/.addressusa-->
                </div><!--/.col-->
                <div class="col">
                	<div class="fmenulinks" data-aos="zoom-in" data-aos-delay="1100">
                        <h3>Navigation links</h3>
                        <ul class="fmenu">
                            <li><a href="#" title="Home">Home</a></li>
                            <li><a href="#" title="About Us">About Us</a></li>
                            <li><a href="#" title="Services">Services</a></li>
                            <li><a href="#" title="Portfolio">Portfolio</a></li>
                            <li><a href="#" title="Contact Us">Contact Us</a></li>
                        </ul><!--/.fmenu-->
                    </div><!--/.fmenulinks-->
                </div><!--/.col-->
            </div><!--/.cols3-->
        </div><!--/.footer-top-->
        <p class="copyright">Copyright &copy; All Right Reserved <span>|</span> <br class="breakline"> AspireEdge Solutions Pvt Ltd | 2018</p><!--/.copyright -->
    </div><!--/.wrap -->
</footer><!--/#footer -->
 <div class="page-loader loading">
        <div class="logo-holder">
            <img src="images/logo-blank.png" alt="Aspireedge"/>
            <span class="fill-logo" style="background-image: url(images/logo-fill.png);"></span>
        </div><!--/.logo-holder-->
    </div><!--/.page-loader-->

<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/jquery.matchHeight-min.js"></script>
<!-- <script src="js/vendor/slick.js"></script> -->
<script src="js/vendor/aos.js"></script>
<script src="js/vendor/pace.min.js"></script>
<script src="js/general.js"></script>
