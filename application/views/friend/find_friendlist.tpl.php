<div class="adm-dashbord">
  <?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
  <div class="container padd0">
    <?php $this->load->view('dashboard/profile_sidebar_2.tpl.php'); ?>
    <div class="col-lg-10">
      <div class="col-lg-12 padd0">
        <?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
        <div class="myprofile-edit">
            <h2>Search For Friends</h2>
            <div class="col-lg-12 find_friend_list_main">
              <div class="friend_list_second_row row">
                <div class="col-lg-4 col-sm-4 col-md-2"></div>
                <div class="col-lg-4 col-sm-4 col-md-2 search_div">
                  <div class="input-group add-on form-control">
                      <input placeholder="Find Friends/ID" name="srch-plyr" id="srch-plyr" type="text" onkeyup="serach_player()">
                      <span class="glyphicon glyphicon-search form-control-feedback"></span>
                  </div>
                </div>
                <div class="col-lg-4 col-sm-4 col-md-2">
                  <div class="row search_for_friends">
                    <a style="background:#333;margin-right: 30px;" href="<?php echo base_url();?>friend" class="">Friends List</a>
                    <a href="<?php echo base_url();?>friend/find_friendlist" class="">Search Friends</a>
                  </div>
                </div>
              </div>
              <div class="row friend_list_main_row">
                <ul>
                  <?php
                  if($friendlist) {
                    foreach($friendlist as $val) {
                      if ($val->user_id != 0 ) {
                        $name = $val->display_name_status == 1 ? $val->display_name : $val->name;
                      ?>
                      <li>
                        <div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
                            <img src="<?php echo base_url().'upload/profile_img/'. $val->image;?>" alt="">
                        </div>
                        <div class="col-lg-3 col-sm-6 col-md-3 bid-info">
                          <h3><?php echo $name; ?></h3>
                          <span class="postby"><a href="<?php echo base_url(); ?>user/user_profile?fans_id=<?php echo $val->user_id; ?>" target="_blank" class="view_profile">Profile</a></span>
                        </div>
                        <div class="col-lg-4 col-sm-3 col-md-2 bid-info team_info">
                          <p class="font_big">Esports Team</p>
                          <?php echo ($val->team_name) ? '<h3>'.$val->team_name. '</h3>' : '' ?>
                        </div>
                        <div class="col-lg-3 col-sm-5 col-md-3 primum-play play-n-rating">
                          <form role="form" action="<?php echo base_url(); ?>friend/add_friend/" name="addToFriendForm" class="add-to-friend-form" method="POST" enctype="multipart/form-data">
                            <?php if (in_array($val->user_id.'_1', $sent_friend_request)) { ?>
                            <span class="request_sent"> Request Sent <i class="fa fa-check"></i></span>
                            <?php } else if (in_array($val->user_id.'_0', $sent_friend_request)) { ?>
                            <span class="request_sent"> Already Friend </span>
                            <?php } else { ?>
                            <input type="submit" class="add_friend_button" value="Add Friend" onclick="return confirm('Are you sure you want to add <?php echo $val->name; ?> as a Friend in your Friend List ?')">
                            <?php } ?>
                            <textarea style="display: none;" name="mail">
                              <?php echo $val->name; ?> sent you friend request on
                              <?php echo base_url(); ?>.
                            </textarea>
                            <input type="hidden" name="user_to" value="<?php echo $val->user_id; ?>">
                          </form>
                        </div>
                      </li>
                  <?php } } } else { echo "<h5><div class='row bidhottest_err'><div class='col-sm-12' style='color: #FF5000;'>Friends Not Available</div></div></h5>"; }?>
                </ul>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>