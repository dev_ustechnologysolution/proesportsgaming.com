<div class="adm-dashbord">
    <?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
    <div class="container padd0">
        <?php $this->load->view('dashboard/profile_sidebar_2.tpl.php'); ?>
        <div class="col-lg-10">
            <div class="col-lg-12 padd0">
                <?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
                <div class="myprofile-edit">
                    <h2>Profile</h2>
                    <?php $this->load->view('common/show_message'); ?>
                    <div class="col-lg-12">
                        <?php $value = $friend_profile; ?>
                    <div class="row">
                        <div class="form-group profile_image_div">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="friend_profile_img" style="text-align:center;">
                                        <img src="<?php if(isset($value->image)) echo base_url().'upload/profile_img/'.$value->image;?>"  id="k"/>
                                    </div>
                                </div>
                                <div class="col-lg-12 text-center friend_plyr_name">
                                  <p class="white"><?php echo (isset($value->display_name)) ? $value->display_name : $value->name; ?></p>
                                </div>
                                <div class="col-lg-12 friend_plyr_id"   >
                                  <label style="font-size:40px;padding:0;">Account NO #<?php echo $value->account_no; ?></label>
                                </div>                                    
                                <div class="col-lg-12">
                                  <p>Esports Team</p>
                                </div>
                                <div class="col-lg-12 friend_plyr_id">
                                  <label class="white no-padding"><?php echo $value->team_name; ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-lg-12 text-center">
                                <a data-id="<?php echo $value->user_id; ?>" class="lobby_btn social_follow <?php echo ($value->user_id == $this->session->userdata('user_id')) ? 'disabled': ''?>"><?php echo $is_followed.'<span class="cunt" style="margin-left: 5px;">'.(($followercount > 0) ? $followercount: '').'</span>'; ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12"  align="center">
                        <div class="input-group back_button">
                          <a href="javascript:history.go(-1)">Back</a>
                        </div>
                    </div>  
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>


