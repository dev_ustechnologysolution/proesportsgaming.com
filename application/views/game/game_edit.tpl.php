<div class="adm-dashbord">
    <div class="container padd0">
        <?php $this->load->view('dashboard/profile_sidebar.tpl.php'); ?>
        <div class="col-lg-10">
            <div class="col-lg-11">
                <div> 
                    <div class="col-lg-11">
                        <div class="myprofile-edit">
                          <h2>Upload Video Url</h2>
                          <div class="col-lg-12">
                            <?php if(count($game)>0){?>
                            <form  action="<?php echo base_url().'game/insertVideo/'.$game[0]['game_id'] ?>" method="post">
                                
                                <div class="form-group">
                                    <label class="col-lg-4">Game Category</label>
                                    <div class="input-group col-lg-8">
                                        <input type="text" id="name" class="form-control" name="name" value="<?php if(isset($game_cate[0]['category_name']))echo $game_cate[0]['category_name'];?>" readonly="readonly">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-4">Game Name</label>
                                    <div class="input-group col-lg-8">
                                        <input type="text" id="name" class="form-control" name="name" value="<?php if(isset($game_data[0]['game_name']))echo $game_data[0]['game_name'];?>" readonly="readonly">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-4">Game Sub Name</label>
                                    <div class="input-group col-lg-8">
                                        <input type="text" id="name" class="form-control" name="name" value="<?php if(isset($game_subname[0]['sub_game_name']))echo $game_subname[0]['sub_game_name']; else echo 'Nill'; ?>" readonly="readonly">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-4">Price</label>
                                    <div class="input-group col-lg-8">
                                        <input type="text" placeholder="Price" id="price" class="form-control" name="price" value="<?php if(isset($game[0]['price']))echo $game[0]['price'];?>" readonly="readonly">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4">Device Name.</label>
                                    <div class="input-group col-lg-8">
                                        <input type="text" id="number" class="form-control" name="device_number" required value="<?php if(isset($game[0]['device_id']))echo $game[0]['device_id'];?>" readonly="readonly">
										
										<input type="hidden" id="game_select" class="form-control" name="game_select" required value="<?php echo $this->uri->segment(4); ?>" readonly="readonly">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4">Description</label>
                                    <div class="input-group col-lg-8">
                                        <textarea class="form-control" id="description" rows="5" cols="5" name="description" readonly="readonly"><?php if(isset($game[0]['description'])) echo $game[0]['description'];?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                  <label class="col-lg-4 col-sm-4 col-md-4">Game Image</label>
                                    <div class="col-lg-8 col-sm-8 col-md-8 form-upload">
                                    <div>
                                    <img src="<?php echo base_url().'upload/game/'.$game_img[0]['game_image']?>" height="100" width="100"/>
                                  </div>
                            
                                    </div>
                                </div>
								<!--
								<div class="form-group">
                                  <label class="col-lg-4 col-sm-4 col-md-4">Game Won/ Lose</label>
                                    <div class="input-group col-lg-8">
										<select type="text" required  id="game_select" class="form-control" name="game_select">
											<option value="">--Select Game Won/ Lose --</option>
											<option value="1">Won</option>
											<option value="0">Lose</option>							   
									   </select>
                                    </div>
                                </div>
								-->
                                <div class="form-group">
                                    <label class="col-lg-4">Video Url</label>
                                    <div class="input-group col-lg-8">
                                        <input type="text" class="form-control" name="url" value="<?php if(isset($game_url[0]['video_url']))echo $game_url[0]['video_url'];?>">
                                        <!--<a href="#" class="add_field_button">+ Add More Url</a>-->
										<div class="clearfix"></div>
										 <span style="color:red"><?php echo @$this->session->flashdata('message_video_error');?></span>
                                        <div class="input_fields_wrap"/>
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label class="col-lg-4"></label>
                                    <div class="input-group col-lg-8">
                                        <input type="checkbox" name="accept_risk" value="1"> I Accept Risk Because I Have No video/proof of  Victory
										<div class="clearfix"></div>
										 <span style="color:red"><?php echo @$this->session->flashdata('message_checkbox_error');?></span>
										
                                    </div>
                                </div>
								
								
								
                                <div class="btn-group">
                                    <div class="col-lg-8 pull-right padd0">
                                    <input type="hidden" name="id" value="<?php if(isset($game[0]['game_id'])) echo $game[0]['game_id'];?>">
									<input type="hidden" name="game_tbl_id" value="<?php echo $gameid;?>">
                                    <input type="hidden" name="sub_id" value="<?php if(isset($game[0]['sub_game_id'])) echo $game[0]['sub_game_id'];?>">
                                    <input type="submit" value="Submit" class="btn-submit">
                                    </div>
                                </div>
                            </form>
                            <?php } ?>
                           </div>
                        </div>
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>

