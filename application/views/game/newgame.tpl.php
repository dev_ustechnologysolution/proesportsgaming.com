<div class="adm-dashbord">
<div class="container padd0">
  <?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
  <?php $this->load->view('dashboard/profile_sidebar.tpl.php');?>
  <div class="col-lg-10">
    <div class="col-lg-12">
       <?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
       <div class="myprofile-edit">
          <h2>Challenge Information</h2>
          <div class="col-lg-12">
            <div class="ch_info_page_main_div text-center">
              <?php if (MATCH_MODULE == 'on') { ?>
                <div class="col-sm-5">
                    <a href="<?php echo base_url().'game/gameadd'?>" class="button btn">Create Match</a>
                </div>
                <div class="col-sm-2">
                    <h3 class="ch_info_or">OR</h3>
                </div>
              <?php } ?>
              <div class="<?php echo (MATCH_MODULE == 'on') ? 'col-sm-5' : 'col-sm-12'; ?>">
                <a href="<?php echo base_url().'livelobby/lobbyadd'?>" class="button btn">Create Stream</a>
              </div>
            </div>
          </div>
       </div>
    </div>
  </div>
</div>
