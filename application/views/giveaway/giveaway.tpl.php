<?php 
  $subscribed_plan = '';
  $payment_id = '';
  foreach ($this->General_model->subscribedlist($lobbycreator) as $key => $sl) {
    if ($sl->user_to == $this->session->userdata('user_id')) {
      $subscribed_plan = $subscribedlist->plan_detail;
      $payment_id = $subscribedlist->payment_id;
    }
  }
  $lby = $view_lby_det['lobby_id'];
  $usrrow = array_search($this->session->userdata('user_id'), array_column($giveaway_users, 'user_id'));
  $fan_tagarr = (!empty($fanscount)) ? '"'.implode('","', array_column($fanscount, 'fan_tag')).'"' : '';
?>
<section class="body-middle innerPage">
  <?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
  <div class="container view_lobby_container giveaway_main_div">
    <div class="row header1">
      <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
      <div class="col-lg-4 col-sm-4 col-md-4">
        <h2><?php echo (!empty($giveaway_data[0]['title'])) ? $giveaway_data[0]['title'].' - Giveaway' : 'Giveaway'; ?> </h2>
      </div>
      <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
    </div>
    <div class="row ga_timer">
    	<?php
      ($giveaway_data[0]['timezone'] != '') ? date_default_timezone_set($giveaway_data[0]['timezone']) : '';
      $creator_time = (in_array($giveaway_data[0]['timezone'], array('EST', 'MST')) && $global_dst_option['option_value'] == 'on' && $global_dst_hour['option_value'] == "+1") ? date('Y-m-d H:i:s') : date('Y-m-d H:i:s',strtotime('+1 hour'));
      if (!empty($giveaway_data[0]['date_time_registration_close'])) { ?>
        <div class="col-sm-3"> </div>
        <div class="col-sm-6 text-center giveaway_registration_close_countdown">
          <?php $date_time_registration_close = $giveaway_data[0]['date_time_registration_close'];
          if (strtotime($date_time_registration_close) > strtotime($creator_time)) {  ?>
            <h4>Registration Close in</h4>
            <div class="countdown_with_day clearfix" id="countdown_day_1" creator_time="<?php echo $creator_time; ?>" finish_action="ga_registration_close_countdown_finish">
              <div class="countdown_ele hide">
                <span class="year"><?php echo date('Y', strtotime($date_time_registration_close)); ?></span>
                <span class="month"><?php echo date('m', strtotime($date_time_registration_close)); ?></span>
                <span class="day"><?php echo date('d', strtotime($date_time_registration_close)); ?></span>
                <span class="hour"><?php echo date('H', strtotime($date_time_registration_close)); ?></span>
                <span class="minute"><?php echo date('i', strtotime($date_time_registration_close)); ?></span>
                <span class="second"><?php echo date('s', strtotime($date_time_registration_close)); ?></span>
              </div>
              <span class="livetimer"></span>
            </div>
          <?php } ?>
        </div>
        <div class="col-sm-3">
          <div class="ga_image" style="max-width: 120px; z-index: 0;">
            <img src="<?php echo base_url(); ?>upload/all_images/<?php echo $giveaway_data[0]['gw_img']; ?>" alt="smile" class="img img-responsive" style="height: 60px; margin: 0 auto;">
            <div class="clearfix text-center" style="margin : 20px auto;">
              <a href="<?php echo base_url().'livelobby/watchLobby/'.$giveaway_data[0]['lobby_id']; ?>" target="_self" class="cmn_btn ga_btn">Go back</a>
            </div>
          </div>
        </div>
      <?php } ?> 
    </div>
    <div class="row ga_timer">
      <div class="col-lg-12 col-sm-12 col-md-12 text-center">
        <h4><?php echo (!empty($giveaway_data[0]['gw_date_time_of_giveaway'])) ? date('F d, Y',strtotime($giveaway_data[0]['gw_date_time_of_giveaway'])) : '';?></h4>
      </div>
    </div>
    <div class="row ga_timer"> 
      <div class="col-sm-2"> </div>
    	<div class="col-sm-8 text-center giveaway_countdown_main_div">
        <?php
        if (!empty($giveaway_data[0]['gw_date_time_of_giveaway'])) {
          if (strtotime($giveaway_data[0]['gw_date_time_of_giveaway']) > strtotime($creator_time)) {  ?>
            <div class="countdown_with_day clearfix" id="countdown_day_0" creator_time="<?php echo $creator_time; ?>" finish_action="ga_countdown_finish">
              <div class="countdown_ele hide">
                <span class="year"><?php echo date('Y', strtotime($giveaway_data[0]['gw_date_time_of_giveaway'])); ?></span>
                <span class="month"><?php echo date('m', strtotime($giveaway_data[0]['gw_date_time_of_giveaway'])); ?></span>
                <span class="day"><?php echo date('d', strtotime($giveaway_data[0]['gw_date_time_of_giveaway'])); ?></span>
                <span class="hour"><?php echo date('H', strtotime($giveaway_data[0]['gw_date_time_of_giveaway'])); ?></span>
                <span class="minute"><?php echo date('i', strtotime($giveaway_data[0]['gw_date_time_of_giveaway'])); ?></span>
                <span class="second"><?php echo date('s', strtotime($giveaway_data[0]['gw_date_time_of_giveaway'])); ?></span>
              </div>
              <span class="livetimer"></span>
            </div>
          <?php }
        } ?>
    	</div>
      <div class="col-sm-2"> </div>
    </div>
    <div class="col-lg-12 padd0" style="margin: 30px auto;">
      <div class="myprofile-edit row">
        <div class="col-sm-12">
          <div class="form-group profile_image_div ga_default_stream_main_div" style="<?php echo (!empty($giveaway_data[0]['default_stream']) && $giveaway_data[0]['default_stream_showstatus'] == 1) ? 'display: block': 'display: none';?>">
            <div class="col-lg-12 padd10 text-center">
              <iframe <?php echo (!empty($giveaway_data[0]['default_stream']) && $giveaway_data[0]['default_stream_showstatus'] == 1) ? 'src="https://player.twitch.tv/?channel='.$giveaway_data[0]['default_stream'].'&parent='.$_SERVER['HTTP_HOST'].'"': '';?> height="400" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>" class="stream_iframe ga_default_stream"></iframe>
            </div> 
            <div class="col-lg-12 streamer_channel_detail">
              <div class="col-lg-4 friend_plyr_id"> 
                <label class="font35 pull-left">Account NO #<?php echo $giveaway_data[0]['creator_acc']; ?></label>
              </div>
              <div class="col-lg-8 padd10" style="text-align: right;">
                <a data-id="<?php echo $giveaway_data[0]['crtr']; ?>" class="lobby_btn social_follow <?php echo ($giveaway_data[0]['crtr'] == $this->session->userdata('user_id')) ? 'disabled': ''?>"><?php echo ucfirst($is_followed).'<span class="cunt" style="margin-left: 5px;">'.$followercount.'</span>'; ?></a>&nbsp;&nbsp;
                <?php // echo '<sup><span class="cunt" style="margin-left: 5px;">'.(($followercount > 0) ? $followercount: '').'</span></sup>'; ?>
                <a class="lobby_btn" href="<?php echo base_url().'Store/'.$giveaway_data[0]['crtr']; ?>" target="_blank">Store</a>&nbsp;&nbsp;
                <a class="lobby_btn subscribe_button commonmodalclick <?php echo ($giveaway_data[0]['crtr'] == $this->session->userdata('user_id')) ? 'disabled': ''?>" streamer_name='<?php echo $giveaway_data[0]['name']; ?>' streamer_id="<?php echo $giveaway_data[0]['crtr']; ?>" streamer_img="<?php echo ($giveaway_data[0]['image'] != '') ? $giveaway_data[0]['image'] : $this->General_model->get_user_img(); ?>" plan_subscribed="<?php echo $subscribed_plan; ?>" payment_id="<?php echo $payment_id; ?>">Subscribe</a>&nbsp;&nbsp;
                <a data-id="<?php echo $giveaway_data[0]['crtr']; ?>" class="lobby_btn profile_tip <?php echo ($giveaway_data[0]['crtr'] == $this->session->userdata('user_id')) ? 'disabled': ''?>">Tip</a>
                <input type="hidden" name="user_id" value="<?php echo $this->session->userdata('user_id')?>">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-5"></div>
        <div class="col-sm-2">
          <div class="total_registred_plyr text-center">
            <span class="count"><?php echo (isset($giveaway_users)) ? count($giveaway_users) : 0; ?></span>
          </div>
        </div>
        <div class="col-sm-5"></div>
      </div>
      <div class="row live_lby_in_thirow">
        <div class="row">
        	<div class="col-sm-5"></div>
         	<div class="col-sm-2 text-center padd0">
            <div class="ga_ready_div">
              
              <?php if ($lobbycreator == $this->session->userdata('user_id')) {
                echo '<div class="clearfix ga-cemara_title_div">'.(($giveaway_data[0]['default_stream_showstatus'] == 1 && !empty($giveaway_data[0]['default_stream'])) ? 'Disable' : 'Enable').' Your Stream</div>';
              } ?>
              <div class="clearfix ga-cemara_div">
                <span class="tooltip_div cemara_action cmn_btn ga_btn <?php echo ($lobbycreator == $this->session->userdata('user_id')) ? 'enable' : 'disabled'; ?>" stream_click_status="<?php echo ($giveaway_data[0]['default_stream_showstatus'] == 1) ? 'disable' : 'enable'; ?>" default_stream_name="<?php echo (!empty($giveaway_data[0]['default_stream'])) ? $giveaway_data[0]['default_stream'] : ''; ?>" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Change Default Stream">
                  <span is_fan="true" style="vertical-align: middle;"><?php echo (!empty($giveaway_data[0]['display_name'])) ? $giveaway_data[0]['display_name'] : $giveaway_data[0]['name']; ?></span>
                  <img src="<?php echo base_url(); ?>assets/frontend/images/<?php echo (!empty($giveaway_data[0]['default_stream']) && $giveaway_data[0]['default_stream_showstatus'] == 1) ? 'ga_live_stream_on.png' : 'ga_live_stream_off.png'; ?>" alt="live_stream" id="stream_<?php echo $giveaway_data[0]['crtr']; ?>"></span>
              </div>
            </div>
          </div>
          <div class="col-sm-5"></div>
        </div>
        <div class="ga_table clearfix">
          <?php
          // $ga_left_arr = $ga_right_arr = [];
          $entrieslimit = $giveaway_data[0]['gw_entrieslimit'];
          if (!empty($giveaway_users)) {
            $entrieslimit = $giveaway_data[0]['gw_entrieslimit'] - count($giveaway_users);
            // foreach ($giveaway_users as $key => $gausr) {
            //   ($key % 2 == 0) ? $ga_left_arr[] = $gausr : $ga_right_arr[] = $gausr;
            // }
          } ?>
         <!--  <div class="col-sm-6 ga_tables left">
            <?php if (!empty($ga_left_arr)) {
              foreach ($ga_left_arr as $key => $gla) { ?>
                <div class="left_table text-left ga_users_div" user_id="<?php echo $gla['user_id']; ?>">
                  <a href="<?php echo base_url(); ?>user/user_profile/?fans_id=<?php echo $gla['user_id']; ?>" class="ga_users" target="_blank" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="View Profile"><?php echo $gla['giveaway_tag']; ?></a>
                </div>
              <?php }
            } ?>
          </div>
          <div class="col-sm-6 ga_tables right border-right">
            <?php if (!empty($ga_right_arr)) {
              foreach ($ga_right_arr as $key => $gra) { ?>
                <div class="right_table text-right ga_users_div" user_id="<?php echo $gra['user_id']; ?>">
                  <a href="<?php echo base_url(); ?>user/user_profile/?fans_id=<?php echo $gra['user_id']; ?>" class="ga_users" target="_blank" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="View Profile"><?php echo $gra['giveaway_tag']; ?></a>
                </div>                
              <?php }
            } ?>
          </div> -->
          <div class="col-sm-12 ga_tables">
            <?php
            if (!empty($giveaway_users)) {
              foreach ($giveaway_users as $key => $gla) {
                $profile_img = ($gla['image'] != null && !empty($gla['image']) && file_exists(BASEPATH.'../upload/profile_img/'.$gla['image'])) ? $gla['image'] : 'placeholder-image.png';
                ?>
                <div class="left_table text-left ga_users_div" user_id="<?php echo $gla['user_id']; ?>">
                  <a href="<?php echo base_url(); ?>user/user_profile/?fans_id=<?php echo $gla['user_id']; ?>" class="ga_users" target="_blank" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="View Profile" style="background-color: <?php echo '#'.substr(md5(rand()), 0, 6); ?>">
                    <div class="profile_img_div">
                      <img src="<?php echo base_url().'upload/profile_img/'.$profile_img; ?>" class="img img-responsive" alt="smile">
                    </div>
                    <span><?php echo $gla['giveaway_tag']; ?></span>
                  </a>
                </div>
              <?php }
            } ?>
          </div>
          <?php if ($lobbycreator == $this->session->userdata('user_id')) { ?>
            <div class="col-sm-12 start_button text-center" style="<?php echo (!empty($giveaway_users) && strtotime($giveaway_data[0]['gw_date_time_of_giveaway']) < strtotime('now')) ? 'display: block' : 'display: none'; ?>">
              <a class="cmn_btn ga_btn shuffle_confirmation" action="start">Start</a>
            </div>
          <?php } ?>
        </div>
        <div class="lobby_tables_bottom">
          <div class="col-sm-2"></div>
          <div class="col-sm-3"></div>
          <div class="col-sm-2 border-right border-left border-top border_radious_4px">
            <div class="text-center join_ga_btn_div">
              <div class="clearfix joingabtn">
                <?php
                if (in_array($this->session->userdata('user_id'), array_column($giveaway_users, 'user_id'))) 
                  echo '<lable class="cmn_btn ga_btn already_joined">Joined <i class="fa fa-check"></i></lable>';
                else if (strtotime($giveaway_data[0]['date_time_registration_close']) > strtotime('now'))
                  echo '<lable class="join_giveaway_btn cmn_btn ga_btn">Join</lable>';
                ?>
              </div>
              <div class="margin-tpbtm-10 clearfix">
              	<label><?php echo $giveaway_data[0]['gw_entrieslimit'].' Entries'; ?></label>
              </div>
              <div class="margin-tpbtm-10 clearfix getanother_ticket_div">
                <?php if ($giveaway_data[0]['gw_is_multiple_entries'] == 1 && in_array($this->session->userdata('user_id'), array_column($giveaway_users, 'user_id')) && strtotime($giveaway_data[0]['date_time_registration_close']) > strtotime('now') && $entrieslimit > 0) { ?>
                  	<lable class="get_another_ticket_ga_btn cmn_btn ga_btn">Get Another Entry</lable>
                <?php } ?>
              </div>
              <div class="edit_ga_tag_div" style="<?php echo (in_array($this->session->userdata('user_id'), array_column($giveaway_users, 'user_id'))) ? 'display: block': 'display: none'; ?>">
                <div align="center">
                  <a class="cmn_btn ga_btn editga_tg_btn">Edit Player Tag</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-3"></div>
          <div class="col-sm-2"></div>
        </div>
      </div>
      <?php date_default_timezone_set($this->session->userdata('timezone')); ?>
      <input type="hidden" id="lobby_id" name="lobby_id" value="<?php echo (!empty($giveaway_data[0]['lobby_id'])) ? $giveaway_data[0]['lobby_id'] : ''; ?>">
      <input type="hidden" id="lobby_custom_name" name="lobby_custom_name" value="<?php echo (!empty($view_lby_det['custom_name'])) ? $view_lby_det['custom_name'] : ''; ?>">
      <input type="hidden" id="giveaway_amount" name="giveaway_amount" value="<?php echo (!empty($giveaway_data[0]['gw_amount'])) ? $giveaway_data[0]['gw_amount'] : ''; ?>">
      <input type="hidden" id="giveaway_id" name="giveaway_id" value="<?php echo (!empty($giveaway_data[0]['gw_id'])) ? $giveaway_data[0]['gw_id'] : ''; ?>">
      <input type="hidden" id="user_id" name="user_id" value="<?php echo $this->session->userdata('user_id'); ?>">
      <input type="hidden" id="total_balance" value="<?php echo $this->session->userdata('total_balance'); ?>">
      <input type="hidden" name="is_admin" value="<?php echo $this->session->userdata('is_admin'); ?>">
      <input type="hidden" id="ga_crtr" value="<?php echo $lobbycreator; ?>">
      <input type="hidden" id="ga_plyrtag" value="<?php echo $giveaway_users[$usrrow]['giveaway_tag']; ?>">
      <input type="hidden" id="fan_tag" name="fan_tag" value="<?php echo $get_my_fan_detail['fan_tag']; ?>">
      <input type="hidden" name="remaining_items" value="<?php echo $entrieslimit; ?>">
      <input type="hidden" name="global_gafee_calc_method" value="<?php echo (!empty($global_giveawayfees)) ? $global_giveawayfees[0] : ''; ?>">
      <input type="hidden" name="global_gafee" value="<?php echo (!empty($global_giveawayfees)) ? $global_giveawayfees[1] : ''; ?>">
      <?php if (!empty($giveaway_data[0]['fireworks_duration_time'])) { ?>
        <input type="hidden" id="fireworks_duration_time" name="fireworks_duration_time" value="<?php echo $giveaway_data[0]['fireworks_duration_time'] * 1000; ?>">
          <audio controls class="fireworks_audio_file" loop>
            <source src="<?php echo base_url().'upload/stream_setting/'.(($giveaway_data[0]['use_custom_fireworks_audio'] == 1 && !empty($giveaway_data[0]['fireworks_audio'])) ? $giveaway_data[0]['fireworks_audio'] : 'giveaway_default_sound.mp3'); ?>" type="">
          </audio>
          <input type="hidden" id="fireworks_audio" name="fireworks_audio" value="<?php echo ($giveaway_data[0]['use_custom_fireworks_audio'] == 1 && !empty($giveaway_data[0]['fireworks_audio'])) ? $giveaway_data[0]['fireworks_audio'] : 'giveaway_default_sound.mp3'; ?>">
      <?php } ?>
      <div class="row">
        <div class="col-sm-2"> </div>
        <div class="col-sm-8 giveaway_chatbox_main_div margin-tpbtm-40">
          <div class="row chat-box_fronside commonchatbox_chatbox_main_row">
            <div class="col-md-12 col-sm-12 col-xs-12 " align="center" id="commonchatbox_msgbox">
              <div id="message-container" class="xwb-message-container">
                 <div class="popup-content-wrapper_customer_fontside commonchatbox_msg_bx">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="clearfix margin-tpbtm-20">
                          <h2 align="center" class="live_lobby_h3">Chat in the Pro Esports Giveaway</h2>
                        </div>
                        <div id="message-inner" class="message-inner">
                          <?php if(isset($lobby_chat) && count($lobby_chat) > 0) {
                            foreach ($lobby_chat as $key => $value) {
                              $direction = 'left';
                              $download_attr = 'download';
                              $class = 'enable';
                              if ($value['user_id'] == $_SESSION['user_id']) {
                                $direction = 'right';
                                $download_attr = '';
                                $class = 'disable';
                              }
                              $msg = $value['message'];
                              if ($value['message_type'] == 'file') {
                                $msg = '<a href="'.base_url().'upload/all_images/'.$value['attachment'].'" class="download_attachment lobby_grpmsg_attach_link '.$class.'" '.$download_attr.'><i class="fa fa-download '.$class.'"></i><img class="attachment_image img img-thumbnail" src="'.base_url().'upload/all_images/'.$value['attachment'].'" class="lobby_grpmsg_attach" alt="pic" width="100px"></a>';
                              } else if ($value['message_type'] == 'tipmessage') {
                                $tipicn = '<img src="'.base_url().'upload/stream_setting/'.$value['attachment'].'" alt="tipimg" class="tipiconimg">'; 
                                if ($value['user_id'] == $_SESSION['user_id']) {
                                  $msg = $tipicn.$value['message'];
                                } else {
                                  $msg = $value['message'].$tipicn;
                                }
                              }
                              ?>
                              <div data-mid="<?php echo $value['message_id'];?>" data-cid="<?php echo $value['id'];?>" class="message-row msg_usr_<?php echo $value['user_id']; ?>">
                                <div class="col-md-12 col-sm-12 col-xs-12 message-box <?php echo ($lobbycreator == $this->session->userdata('user_id') || $this->session->userdata('is_admin') == 1) ? 'del_opt' : ''; ?>" tgcontmenu='<?php echo $value['message_id'];?>'>
                                  <div class="message_header">
                                    <?php
                                    if ($value['is_admin'] == 1) { ?>
                                      <img class="chat_image pull-<?php echo $direction;?>" width="20" height="20" src="<?php echo base_url().'upload/admin_dashboard/Chevron_3_Twitch_72x72.png'; ?>">
                                    <?php } ?>
                                    <small class="list-group-item-heading text-muted text-primary pull-<?php echo $direction;?>"><?php echo $value['fan_tag']; ?></small>
                                  </div>
                                  <div class="message_row_container in pull-<?php echo $direction;?>">
                                    <p class="list-group-item-text"></p>
                                    <p><?php echo $msg;?></p>
                                    <?php if ($lobbycreator == $this->session->userdata('user_id') || $this->session->userdata('is_admin') == 1) { ?>
                                      <div class="dropdown clearfix contmenu_<?php echo $value['message_id'];?> contextMenu">
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu" style="display:block;">
                                          <li>
                                            <a class="remove_msg" onclick="send_msg_delete(<?php echo $value['message_id'] ;?>,<?php echo $value['user_id'] ;?>);">Remove Msg</a>
                                          </li>
                                          <?php if ($direction == 'left' ) { ?>
                                            <li>
                                              <a class="remove_user" onclick="del_usr_from_chat(<?php echo $value['user_id'] ;?>);">Remove User</a>
                                            </li>
                                            <li>
                                              <a class="mute_user" onclick="mute_usr_from_chat(<?php echo $value['user_id'] ;?>);">Time Out User</a>
                                            </li>
                                          <?php } ?>
                                        </ul>
                                      </div>
                                    <?php } ?>
                                    <p></p>
                                   </div>
                                </div>
                              </div>
                            <?php }
                          } else { ?>
                            <div class="no_messages">
                              <h3 class="text-center no-more-messages">No Messages Available</h3>
                            </div>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                    <?php
                    if ($get_my_fan_detail['status'] == 1) {
                      $disable_class = '';
                      $disable_attr = '';
                      if ($get_my_fan_detail['mute_time'] != '') {
                        $mutediff = strtotime($get_my_fan_detail['mute_time']) - time();
                        if (time() < strtotime($get_my_fan_detail['mute_time'])){
                          $disable_class = 'disable';
                          $disable_attr = 'readonly';
                          echo '<div class="mute_div mutetimer_div">You can chat after <span class="mutetimer" data-seconds-left="'.$mutediff.'"></span> Seconds</div>';
                        } else {
                          echo '<div class="mute_div"></div>';
                        }
                      } ?>
                    <div class="msg-input input-group col-sm-12 giveaway_msg_bx grp_msgbox msgbox_userid_<?php echo $_SESSION['user_id']; ?> <?php echo $disable_class; ?>">
                      <div class="ms_snd_opt">
                        <div id="cmn_grp_msgbox_message-input" class="form-control message-input message-input-frontend-side cmn_grp_msgbox_message-input attach_emojioneicon" rows="4" name="message-input" contenteditable="true"></div>
                        <div id="emojionearea_display_div"></div>
                      </div>
                      <div class="chat_box_button_area">
                        <div class="attachfile_div">
                          <div class="input-group-btn">
                            <div class="attachfile btn-file">
                              <label for="file-input">
                                <i class="fa fa-paperclip" aria-hidden="true"></i>
                              </label>
                              <input type="file" class="file-input hide" name="attachfile" id="file-input" accept="image/png, image/PNG, image/jpeg, image/jpg, image/JPEG, image/JPG" data-rule-required="true" data-msg-accept="Your image formate should be in .jpg, .jpeg or .png">
                              <div class="filename-container hide"></div>
                            </div>
                          </div>
                        </div>
                        <div class="message_send_div">
                          <div class="input-group-btn">
                            <button class="btn btn-success send-message-to-cmn_group send-message-to-giveaway_group">Send</button>
                          </div>
                        </div>
                      </div>
                      <div class="play_sound">
                        <?php if (isset($_SESSION['getmysub_sound']) && $_SESSION['getmysub_sound'] != '') { ?>
                        <audio controls autoplay><source src="<?php echo base_url(); ?>upload/stream_setting/<?php echo $_SESSION['getmysub_sound']; ?>"></audio>
                        <?php } unset($_SESSION['getmysub_sound']); ?>
                      </div>
                    </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2"> </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/jquery.fireworks.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/giveaway_shuffle.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/countdown_with_day.js"></script>
<script type="text/javascript">
  var availableTags = [<?php print_r($fan_tagarr)?>];
	// $('.ga_tables').css('min-height',$(this).height()-200+"px");
  // $('.start_shuffle').click(function() {
  // 	var intrvl = setInterval(giveaway_shuffle, 500);
  // 	$(this).addClass('disabled');
  // 	setTimeout(function(){ 
  // 		clearTimeout(intrvl);
  // 		$(this).removeClass('disabled');
  // 		console.log($('.ga_tables .ga_users_div .ga_users.white_shuffle_ga').text() + ' You Won');
  // 		$('body').append('<div class="fireworks_blast"> <h1>'+$('.ga_tables .ga_users_div .ga_users.white_shuffle_ga').text() + ' You Won</h1></div>');
  // 		 $('.banner').show().animate({'font-size':'3em'},1000)
  // 		$('.fireworks_blast').fireworks().children('h1').animate({'font-size':'70px'},1000).animate({'font-size':'36px'},1000);
  // 	}, 10000);
  // });
  // function giveaway_shuffle() {
  // 	var random = Math.floor(Math.random() * $(".ga_tables .ga_users_div .ga_users").length);
  // 	$(".ga_tables .ga_users_div .ga_users").removeClass('white_shuffle_ga').eq(random).addClass('white_shuffle_ga');
  // }


  $('.profile_tip').on('click', function() {
    $('#send_tip_amount_profile').show()
  });
  $('.tip_submit').on('click', function(e){
    e.preventDefault();
    var total_amount = '<?php echo $this->session->userdata('total_balance');?>';
    var tip_amount = parseFloat($('#tip_amount_input_profile').val());
    if($('#tip_amount_input_profile').val() == '' || $('#tip_amount_input_profile').val() == null){
      $('.tip_amount_error').html('Please enter amount to donate.');
      $('.tip_amount_error_row').show();
      return false;
    }else if ((tip_amount > total_amount) || total_amount == 0) {
      $('.tip_amount_error').html('You have insufficient balance to donate.');
      $('.tip_amount_error_row').show();
      return false;
    }else{
      $('#profile_form').submit();
    }
  });
</script>