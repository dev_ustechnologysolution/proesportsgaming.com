<?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
<?php $this->load->view('home/second_banner.tpl.php', $second_banner_data); ?>
<section class="howit-work">
   <div class="container">
      <div class="row header1" style="margin-top: 100px;">
         <h2 class="margin_top hide"><?php echo $heading_dt[0]['heading1'] ?></h2>
      </div>
      <div class="row hide">
         <?php if (!empty($dt)) {
            foreach ($dt as $value) { ?>
               <div class="col-lg-3 col-sm-3 col-md-3">
                  <img src="<?php echo base_url().'upload/gameflow/'.$value['image'];?>" alt="">
                  <p><?php echo $value['description']; ?></p>
               </div>
            <?php } 
         } ?>
      </div>
      <div class="row">
         <div class="create-ch create-ch_home">
            <a href="<?php echo base_url().'game/newgame'; ?>" class="animated-box">CREATE STREAM</a>
         </div>
      </div>
   </div>
</section>
<section class="body-middle">
   <div class="container game_boxes">
   <div class="row header1">
      <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
      <div class="col-lg-4 col-sm-4 col-md-4">
         <h2>
            <?php echo $heading_dt[0]['heading2'] ?> <br>
         </h2>
      </div>
      <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
   </div>
   <div class="row latestvdo">
      <?php if (!empty($video_dt)) {
         foreach ($video_dt as $vd) {
            if ($vd->type == '0') {
               $a_href = ($vd->is_home == 1) ? base_url() : $vd->video_url;
               $w = explode('&',substr($vd->video_url,stripos($vd->video_url,"?")+3)); @$watch=$w[0].'?'.$w[2];
               if($w[0] != 'U') { ?>
               <div class="col-lg-3 col-sm-6 col-md-3 flip-block">
                  <a class="flip-block-inner" href="<?php echo $a_href; ?>" target="_blank">
                     <img class="first-block" src="https://img.youtube.com/vi/<?php echo $w[0]; ?>/mqdefault.jpg" alt="">
                     <div class="float-caption second-block">
                        <h5 class="animated-box"><?php echo $vd->tag; ?></h5>
                     </div>
                  </a>
               </div>
               <?php }
            } elseif($vd->type == '1') {
               $a_href1 = ($vd->is_home == 1) ? base_url() : base_url().'livelobby/watchLobby/'.$vd->lobby_id; ?>
               <div class="col-lg-3 col-sm-6 col-md-3 flip-block">
                  <a class="flip-block-inner" href="<?php echo $a_href1; ?>" target="_blank">
                     <iframe class="first-block" src="https://player.twitch.tv/?channel=<?php echo $vd->twitch_username; ?>&parent=<?php echo $_SERVER['HTTP_HOST']; ?>" height="auto" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
                     <div class="float-caption second-block">
                        <h5 class="animated-box"><?php echo $vd->tag; ?></h5>
                     </div>
                  </a>
               </div>
            <?php }else{
               $lby_detail = $this->General_model->lobby_details($vd->lobby_id)[0];
               $lobby_fan_detail = $this->General_model->view_single_row('lobby_fans', array('stream_status' => 'enable', 'user_id' => $lby_detail['user_id']),'*');
               $random_id = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(8/strlen($x)) )),1,8);
               $a_href1 = ($vd->is_home == 1) ? base_url() : base_url().'livelobby/watchLobby/'.$vd->lobby_id;
               // $base_url = (base_url() != 'https://proesportsgaming.com/') ? 'https://esportstesting.com/' : base_url();
               $base_url = base_url();
                ?>
                <div class="col-lg-3 col-sm-6 col-md-3 flip-block" id="<?php echo $random_id; ?>">
                   <a class="flip-block-inner" href="<?php echo $a_href1; ?>" target="_blank">
                      <video id="videojs-flvjs-player" autoplay class="obs_streaming video-js vjs-default-skin first-block" muted="muted" controls style="width: 100%; object-fit: cover;" poster="<?php echo base_url().'assets/frontend/images/stream_offline_img.jpeg'; ?>" preload="auto" data-key="<?php echo $lobby_fan_detail['obs_stream_channel']; ?>" data-user_id="<?php echo $lby_detail['user_id']; ?>" data-random_id="<?php echo $random_id; ?>" data-base_url="<?php echo $base_url; ?>" data-is_record="0"></video>
                     <div class="float-caption second-block">
                        <h5 class="animated-box"><?php echo $vd->tag; ?></h5>
                     </div>
                   </a>
                </div>
              <?php  }
         }
      } ?>
   </div>
   <div class="row">
      <div class="view-all"><a href="<?php echo base_url(); ?>Videos" class="animated-box">ALL ACTIVE STREAMS</a></div>
   </div>
   <div class="row header2" align="center">
      <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
      <div class="col-lg-4 col-sm-4 col-md-4">
         <h2>
            HOTTEST GAMES 
            <h3 style="color: #ff5000;">Live Streams</h3>
         </h2>
      </div>
      <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
   </div>
   <?php (!empty($lobby_list)) ? $this->load->view('common/live_lobby_filter.tpl.php') : ''; ?>
   <div class="row">
      <div class="create-ch">
         <a href="<?php echo base_url().'game/newgame'; ?>" class="animated-box">CREATE LIVE STREAM</a>
      </div>
   </div>
   <?php if (!empty($lobby_list)) { ?>
      <div class="row bidhottest hottestgame playstore_game_list_front playstore_game_list_front_view_all_stream" style="overflow-y: auto;">
         <li class="expiredtime_event row hidden_timer_row" style="display: none;">
          <div class="countdown_with_day rgclose clearfix">
          </div>
        </li>
         <li class="row livelobby_home_row hidden_row" style="display: none;">
            <div class="col-sm-3 bidimg padd0 lobby_stream_iframe_div">
               <iframe style="display: none;" class="live_stream_iframe" src="" height="auto" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
                <div style="height: 100%; display: none;" id="">
                  <div class="stream_error" class="alert alert-danger" style="display: none;"></div>
                  <video id="videojs-flvjs-player" autoplay muted="muted" class="video-js vjs-default-skin" controls style="width: 100%; height: 100%;" poster="<?php echo base_url().'assets/frontend/images/stream_offline_img.jpeg'; ?>" preload="auto" data-key="" data-random_id="" data-base_url="" data-user_id="" data-lobby_id="" data-group_bet_id="" data-is_record='0'></video>
               </div>
            </div>
            <div class="col-sm-3 bid-info">
               <div class="lb_list">
                  <div class="dly_lst_verticle">
                     <h3></h3>
                     <p></p>
                  </div>
               </div>
            </div>
            <div class="col-sm-3 divPrice">
               <div class="lb_list">
                  <div class="dly_lst_verticle">
                     <div class="game-price premium">
                        <h3></h3>
                     </div>
                     <p class="text-center margin0" style="color: #ff5807;"> Challenges Accepted:  <?php echo '<span>0</span> V <span>0</span>'; ?></p>
                  </div>
               </div>
            </div>
            <div class="col-sm-1 category_img">
               <div class="lb_list">
                  <div class="dly_lst_verticle">
                     <img src="<?php echo base_url(); ?>upload/all_images/XBOX.png" class="img img-responsive game_cat_img">
                  </div>
               </div>
            </div>
            <div class="col-sm-2 padd0 lobby_join_btn_div">
               <div class="lb_list">
                  <div class="dly_lst_verticle">
                     <div class="join_btn_div_lobby row">
                        <div class="join_btn clearfix animated rotateIn">
                           <a href="" data-original-title="'Join Live Event'" data-placement="bottom" data-toggle="tooltip">Join Stream</a>
                        </div>
                     </div>
                     <div class="clearfix primum-play play-n-rating lobby_device_tag device_id">
                        <a class="animated-box"></a>
                     </div>
                  </div>
               </div>
            </div>
         </li>
         <ul>
            <?php
            echo $this->session->flashdata('err_lobby');
            unset($_SESSION['err_lobby']);
            foreach ($lobby_list as $val) {
               ?>
               <li class="row livelobby_home_row">
                  <div class="col-sm-3 bidimg padd0 lobby_stream_iframe_div">
                     <?php if ($val['stream_status'] == 'enable' && ((empty($val['obs_stream_channel']) && !empty($val['stream_channel'])) || (!empty($val['obs_stream_channel']) && empty($val['stream_channel'])) || (!empty($val['obs_stream_channel']) && !empty($val['stream_channel'])))) {
                           if($val['stream_type'] == '1') {
                              $stream_url = 'https://player.twitch.tv/?autoplay=false&channel='.$val['stream_channel'].'&parent='.$_SERVER['HTTP_HOST'];                    
                        ?>
                              <iframe class="live_stream_iframe" src="<?php echo $stream_url; ?>" height="auto" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
                           <?php } else if($val['stream_type'] == '2') {
                                 $stream_key = $val['obs_stream_channel'];
                                 $new_stream_key = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(8/strlen($x)) )),1,8);
                                 // $base_url = (base_url() != 'https://proesportsgaming.com/') ? 'https://esportstesting.com/' : base_url();
                                 $base_url = base_url();
                                 $random_id = $new_stream_key;
                                 $user_id = $val['lobby_crtor'];
                                 // if(!empty($result->lobby_id)){$lobby_id = $result->lobby_id;}
                                 // if(!empty($result->group_bet_id)){$group_bet_id = $result->group_bet_id;}
                              ?>
                                 <div style="height: 100%;" id="<?php echo $random_id; ?>">
                                    <div class="stream_error" class="alert alert-danger" style="display: none;"></div>
                                    <video id="videojs-flvjs-player" autoplay muted="muted" class="obs_streaming video-js vjs-default-skin" controls style="width: 100%; height: 100%;" poster="<?php echo base_url().'assets/frontend/images/stream_offline_img.jpeg'; ?>" preload="auto" data-key="<?php echo $stream_key; ?>" data-random_id="<?php echo $random_id; ?>" data-base_url="<?php echo $base_url; ?>" data-user_id="<?php echo $user_id; ?>" data-lobby_id="<?php echo (isset($lobby_id)) ? $lobby_id : ''; ?>" data-group_bet_id="<?php echo (isset($group_bet_id)) ? $group_bet_id : ''; ?>" data-is_record='0'></video>
                                 </div>
                     <?php }} else { ?>
                        <img src="<?php echo base_url().'upload/game/'. $val['game_image'];?>" alt="">
                     <?php } ?>
                  </div>
                  <div class="col-sm-3 bid-info">
                     <div class="lb_list">
                        <div class="dly_lst_verticle">
                           <h3><?php echo $val['game_name']; ?></h3>
                           <p><?php echo $val['sub_game_name']; ?></p>
                        </div>
                     </div>
                  </div>
                  <!-- <div class="col-sm-1 uplod-day padd0">
                     <p class="best_of"> <?php // echo ($val['best_of'] != null)    ? 'Best of '.$val['best_of'] : ''?></p>
                  </div> -->
                  <div class="col-sm-3 divPrice">
                     <div class="lb_list">
                        <div class="dly_lst_verticle">
                           <div class="game-price premium">
                              <h3><?php $lobby_bet_price = number_format((float) $val['price'], 2 , '.', '');
                                 echo ($lobby_bet_price > 0) ? CURRENCY_SYMBOL.$lobby_bet_price : 'FREE';
                                 ?></h3>
                           </div>
                           <p class="text-center margin0" style="color: #ff5807;"> Challenges Accepted:  <?php echo '<span>'.$val['game_type'].'</span> V <span>'.$val['game_type'].'</span>'; ?></p>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-1 category_img">
                     <div class="lb_list">
                        <div class="dly_lst_verticle">
                           <?php echo ($val['cat_img'] != '') ? '<img src="'.base_url().'upload/all_images/'.$val['cat_img'].'" class="img img-responsive game_cat_img">' : ''; ?>      
                        </div>
                     </div>
                  </div>
                  <?php $lobbylink = ($val['is_event'] == 1) ? 'Live Event' : 'Live Stream'; ?>
                  <div class="col-sm-2 padd0 lobby_join_btn_div">
                     <div class="lb_list">
                        <div class="dly_lst_verticle">
                           <div class="join_btn_div_lobby row">
                              <div class="join_btn clearfix animated rotateIn">
                                 <a href="<?php echo base_url().'livelobby/watchLobby/'.$val['id'];?>" data-original-title="<?php echo 'Join '.$lobbylink; ?>" data-placement="bottom" data-toggle="tooltip">Join Stream</a>
                              </div>
                           </div>
                           <div class="clearfix primum-play play-n-rating lobby_device_tag device_id">
                              <a class="animated-box"><?php echo $val['device_id']; ?></a>
                           </div>
                           <!-- <div class="primum-play text-right desc_sec_div"><a class="animated-box" target="_blank">Rules</a><div class="description" style="display:none;">test</div></div> -->
                           <?php echo (!empty($val['description']) && $val['description'] != null) ? '<div class="primum-play text-right desc_sec_div"><a class="animated-box" target="_blank"  href="'.base_url().'livelobby/rulesAdd/'.$val['id'].'">Rules</a><div class="description" style="display:none;">'.$val['description'].'</div></div>' : ''; ?>
                        </div>
                     </div>
                  </div>
               </li>
            <?php } ?>
         </ul>
      </div>
<!--       <div class="row bidhottest hottestgame playstore_game_list_front">
         <ul>
            <?php echo $this->session->flashdata('err_lobby');
            unset($_SESSION['err_lobby']);
            foreach ($lobby_list as $val) { ?>
               <li class="row">
                  <div class="col-lg-2 col-sm-2 col-md-2 bidimg padd0">
                     <img src="<?php echo base_url().'upload/game/'. $val['game_image'];?>" alt="">
                  </div>
                  <div class="col-lg-3 col-sm-3 col-md-3 bid-info">
                     <h3><?php echo $val['game_name']; ?></h3>
                     <p><?php echo $val['sub_game_name']; ?></p>
                     <span class="postby">Tag: <?php echo $val['device_id']; ?></span> 
                  </div>
                  <div class="col-lg-2 col-sm-2 col-md-2 uplod-day">
                     <?php if ($val['game_timer'] != '' && $val['game_timer'] != "0000-00-00 00:00:00"){
                        $diff = strtotime($val['game_timer'])- time();
                        if (time() < strtotime($val['game_timer'])) { ?>
                        <?php } ?>
                        <h3 class="timer custom_timer" data-seconds-left="<?php echo $diff; ?>"></h3>
                     <?php } ?>
                     <p> Gamesize: <?php echo "--".$val['game_type']."V".$val['game_type']."--"; ?></p>
                     <p class="best_of"> <?php echo ($val['best_of'] != null) ? 'Best of '.$val['best_of'] : ''?></p>
                     <?php echo (!empty($val['description']) && $val['description'] != null) ? '<div class="primum-play text-center margin-tpbtm-20 desc_sec_div"><a class="animated-box" target="_blank"  href="'.base_url().'livelobby/rulesAdd/'.$val['id'].'">Rules</a><div class="description" style="display:none;">'.$val['description'].'</div></div>' : ''; ?>
                  </div>
                  <div class="col-lg-2 col-sm-2 col-md-2 divPrice">
                     <div class="game-price premium">
                        <h3>
                           <?php
                              $lobby_bet_price = number_format((float) $val['price'], 2 , '.', '');
                              echo ($lobby_bet_price > 0) ? CURRENCY_SYMBOL.$lobby_bet_price : 'FREE';
                           ?>
                        </h3>
                     </div>
                  </div>
                  <div class="col-lg-1 col-sm-1 col-md-1">
                     <?php echo ($val['cat_img'] != '') ? '<img src="'.base_url().'upload/all_images/'.$val['cat_img'].'" class="img img-responsive game_cat_img">' : ''; ?>
                  </div>
                  <?php $lobbylink = ($val['is_event'] == 1) ? 'Live Event' : 'Live Stream'; ?>
                  <div class="col-lg-2 col-sm-2 col-md-2 primum-play play-n-rating">
                     <a href="<?php echo base_url().'livelobby/watchLobby/'.$val['id'];?>" class="animated-box"><?php echo $lobbylink; ?></a>
                  </div>
               </li>
            <?php } ?>
         </ul>
      </div> -->
   <?php } else {
      echo "<h5><div class='row bidhottest_err'><div class='col-sm-12' style='color: #FF5000;'>Lobby Not Available</div></div></h5>";
   } ?>
   <div class="row">
      <div class="view-all"><a href="<?php echo base_url().'Livelobby/live_lobby_list' ?>" class="animated-box">VIEW ALL STREAMS</a></div>
   </div>
   <?php if (MATCH_MODULE == 'on') { ?>
      <div class="row header2" align="center">
         <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
         <div class="col-lg-4 col-sm-4 col-md-4">
            <h2>
               HOTTEST GAMES 
               <h3 style="color: #ff5000;">Matches</h3>
            </h2>
         </div>
         <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
      </div>
      <div class="animated-box-2">
         <div class="row filter-panel home_page_search_tabbing">
            <div class="col-lg-12">
               <div class="form-group">
                  <?php if (!empty($challangetDp)) { ?>
                     <select name="system" id="system" class="form-control" onchange="game_search()">
                        <option class="select_title" value="">Please select a System</option>
                        <?php foreach($systemList as $val) {  ?>
                           <option value="<?php echo $val['id'];?>">
                              <?php echo $val['category_name'];?>
                           </option>
                        <?php } ?>
                     </select>
                     <select class="form-control" name="gamesize"  id="gamesize" onchange="game_search()">
                        <option value="">Please select a Gamesize</option>
                        <?php for ($i = 1; $i <= 16; $i++){ ?>
                        <option value="<?php echo $i;?>" class="gamesize_option">
                           <?php echo '--' . $i . 'V' . $i . '--';?>
                        </option>
                        <?php } ?>
                     </select>
                     <select name="game" id="game" class="form-control" onchange="game_search()">
                        <option value="">Please select a Game</option>
                     </select>
                     <select class="form-control" name="subgame" id="subgame" onchange="game_search()">
                        <option value="">Please select a Subgame</option>
                     </select>
                     <div class="input-group add-on form-control" style="float: right;">
                        <input placeholder="Search Tag/Game Name" name="srch-term" id="srch-term" type="text" onkeyup="game_search()" onkeydown="game_search()">
                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                     </div>
                  <?php } ?>
               </div>
            </div>
         </div>
      </div>
      <div class="row bidhottest hottestgame xbox1">
         <ul>
            <?php 
            if (!empty($challangetDp)) { 
               echo $this->session->flashdata('err');
               unset($_SESSION['err']);
               foreach($challangetDp as $val) { ?>
                  <li class="row">
                     <div class="col-lg-2 col-sm-2 col-md-2 bidimg padd0">
                        <img src="<?php echo base_url().'upload/game/'. $val['game_image'];?>" alt="">
                     </div>
                     <div class="col-lg-3 col-sm-3 col-md-3 bid-info">
                        <h3><?php echo $val['game_name']; ?></h3>
                        <p><?php echo $val['sub_game_name']; ?></p>
                        <span class="postby">Tag: <?php echo $val['device_id']; ?></span> 
                        <!--<span class="postby">By <?php // echo $val['name']; ?></span>-->
                     </div>
                     <div class="col-lg-2 col-sm-2 col-md-2 uplod-day">
                        <?php if($val['game_timer'] != '' && $val['game_timer'] != "0000-00-00 00:00:00") {
                           $diff = strtotime($val['game_timer'])- time();
                           if (time() < strtotime($val['game_timer'])) { ?>
                           <?php } ?>
                           <h3 class="timer custom_timer" data-seconds-left="<?php echo $diff; ?>"></h3>
                        <?php } ?>
                        <p> Gamesize: <?php echo "--".$val['game_type']."V".$val['game_type']."--"; ?></p>
                        <p class="best_of"> <?php echo ($val['best_of'] != null) ? 'Best of '.$val['best_of'] : ''; ?></p>
                        <?php echo (!empty($val['description']) && $val['description'] != null) ? '<div class="primum-play text-center margin-tpbtm-20 desc_sec_div"><a class="animated-box" target="_blank" href="'.base_url().'livelobby/rulesAdd/'.$val['id'].'?type=match">Rules</a><div class="description" style="display:none;">'.$val['description'].'</div></div>' : ''; ?>
                     </div>
                     <div class="col-lg-2 col-sm-2 col-md-2 divPrice">
                        <div class="game-price premium">
                           <h3><?php echo CURRENCY_SYMBOL.number_format((float) $val['price'], 2 , '.', ''); ?></h3>
                        </div>
                     </div>
                      <div class="col-lg-1 col-sm-1 col-md-1">
                        <?php echo ($val['cat_img'] != '') ? '<img src="'.base_url().'upload/all_images/'.$val['cat_img'].'" class="img img-responsive game_cat_img">' : ''; ?>
                     </div>
                     <div class="col-lg-2 col-sm-2 col-md-2 primum-play play-n-rating">
                        <?php echo ($val['game_password']!='' && $val['game_password'] != null) ? '<a href="'.base_url().'home/gamePwAuth/'.$val['id'].'" class="animated-box">play</a>' : '<a href="'.base_url().'home/gameAuth/'.$val['id'].'" class="animated-box">play</a>'; ?>
                     </div>   
                  </li>
               <?php }
            } else {
               echo "<h5><div class='row bidhottest_err'><div class='col-sm-12' style='color: #FF5000;'>Games Not Available</div></div></h5>";
            } ?>
         </ul>
      </div>
      <div class="row">
         <div class="view-all "><a class="animated-box" href="<?php echo base_url().'Matches' ?>">VIEW ALL GAMES</a></div>
      </div>
   <?php } ?>
</section>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/video.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/videojs-flvjs.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/flv.min.js'; ?>"></script>

