<?php 
$deg = 0;
if (!empty($data)) { ?>
   <section class="howit-work">
      <div class="container">
         <div class="row" style="margin-top: 100px;">
            <div class="col-sm-1 stream_carousel_indecator">
               <div class="stream_btn prv_stream col-sm-6"><a class="pull-right" style="color: #ff5000"><i class="fa fa-caret-left"></i></a></div>
            </div>
            <div class="col-sm-10">
               <div class="stream_carousel_container">
                  <div class="stream_carousel">
                     <?php
                      foreach ($data as $key => $dt) {
                        if($dt->stream_type == '1') {
                          $stream_url ='https://player.twitch.tv/?autoplay=false&channel='.$dt->stream_channel.'&parent='.$_SERVER['HTTP_HOST']; ?>
                          <div class="item">
                            <iframe src="<?php echo $stream_url; ?>" height="300px" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
                          </div>
                        <?php } else if($dt->stream_type == '2'){
                          $stream_url = base_url()."live_stream?channel=".$dt->obs_stream_channel."&play=false&user_id=".$this->session->userdata('user_id');
                          $stream_key = $dt->obs_stream_channel;
                          $new_stream_key = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(8/strlen($x)) )),1,8);
                          // $base_url = (base_url() != 'https://proesportsgaming.com/') ? 'https://esportstesting.com/' : base_url();
                          $base_url = base_url();
                          $random_id = $new_stream_key;
                          $user_id = $dt->user_id;
                          if(!empty($dt->lobby_id)){$lobby_id = $dt->lobby_id;}
                          if(!empty($dt->group_bet_id)){$group_bet_id = $dt->group_bet_id;}
                        // $stream_key = (!empty($dt->stream_channel)) ? $dt->stream_channel : $dt->fan_stream_channel;
                        // $stream_url = base_url().'live_stream?channel='.$stream_key;
                        ?>
                        <div class="item">
                          <!-- <div class="strm_blk" style="height: 20%;">
                            <span>testing</span>
                            <a href="#">Joinstream</a>
                          </div> -->
                           <div class="obs_stream_video_div" style="height: 100%;" id="<?php echo $random_id; ?>">
                              <div class="stream_error" class="alert alert-danger" style="display: none;"></div>
                              <video id="videojs-flvjs-player" autoplay class="obs_streaming video-js vjs-default-skin" muted="muted" controls style="width: 100%; height: 100%;" poster="<?php echo base_url().'assets/frontend/images/stream_offline_img.jpeg'; ?>" preload="auto" data-key="<?php echo $stream_key; ?>" data-random_id="<?php echo $random_id; ?>" data-base_url="<?php echo $base_url; ?>" data-user_id="<?php echo $user_id; ?>" data-lobby_id="<?php echo $lobby_id; ?>" data-group_bet_id="<?php echo $group_bet_id; ?>" data-is_record='0'></video>
                           </div>
                           <div class="banner_carousal">
                            <div class="clearfix live_stream_lby_title"><?php echo $dt->game_name ?></div>
                            <div class="clearfix live_stream_lby_lnk"><a href="<?php echo base_url().'livelobby/watchLobby/'.$dt->lobby_id;?>" class="animated-box" target="_blank">Join Stream</a></div>
                           </div>
                        </div>
                      <?php }
                      // $deg = $deg+60;
                     } ?>

                    <?php if (count($data) >=1 && count($data) < 6) {  ?>
                      <div class="item">
                        <div class="obs_stream_video_div" style="height: 100%;" id="d5efas3as">
                          <div class="stream_error" style="display: none;"></div>
                          <video id="videojs-flvjs-player" autoplay="" class=" video-js vjs-default-skin" muted="muted" style="width: 100%; height: 100%;" poster="<?php echo base_url().'assets/frontend/images/stream_offline_img.jpeg'; ?>" preload="auto" data-key="" data-random_id="d5efas3as" data-base_url="<?php echo $base_url; ?>" data-is_record="0" controls="controls">
                        </div>
                        <div class="banner_carousal"></div>
                      </div>
                      <?php // $deg = $deg+60; ?>
                      <div class="item">
                        <div class="obs_stream_video_div" style="height: 100%;" id="d5s1dwas">
                          <div class="stream_error" style="display: none;"></div>
                          <video id="videojs-flvjs-player" autoplay="" class=" video-js vjs-default-skin" muted="muted" style="width: 100%; height: 100%;" poster="<?php echo base_url().'assets/frontend/images/stream_offline_img.jpeg'; ?>" preload="auto" data-key="" data-random_id="d5s1dwas" data-base_url="<?php echo $base_url; ?>" data-is_record="0" controls="controls">
                        </div>
                        <div class="banner_carousal"></div>
                      </div>
                      <?php // $deg = $deg+60; ?>
                      <div class="item">
                        <div class="obs_stream_video_div" style="height: 100%;" id="d5ef6was">
                          <div class="stream_error" style="display: none;"></div>
                          <video id="videojs-flvjs-player" autoplay="" class=" video-js vjs-default-skin" muted="muted" style="width: 100%; height: 100%;" poster="<?php echo base_url().'assets/frontend/images/stream_offline_img.jpeg'; ?>" preload="auto" data-key="" data-random_id="d5ef6was" data-base_url="<?php echo $base_url; ?>" data-is_record="0" controls="controls">
                        </div>
                        <div class="banner_carousal"></div>
                      </div>
                      <?php // $deg = $deg+60; ?>
                      <div class="item">
                        <div class="obs_stream_video_div" style="height: 100%;" id="d15efdwas">
                          <div class="stream_error" style="display: none;"></div>
                          <video id="videojs-flvjs-player" autoplay="" class=" video-js vjs-default-skin" muted="muted" style="width: 100%; height: 100%;" poster="<?php echo base_url().'assets/frontend/images/stream_offline_img.jpeg'; ?>" preload="auto" data-key="" data-random_id="d15efdwas" data-base_url="<?php echo $base_url; ?>" data-is_record="0" controls="controls">
                        </div>
                        <div class="banner_carousal"></div>
                      </div>
                      <?php // $deg = $deg+60; ?>
                      <div class="item">
                        <div class="obs_stream_video_div" style="height: 100%;" id="d5efdwa00">
                          <div class="stream_error" style="display: none;"></div>
                          <video id="videojs-flvjs-player" autoplay="" class=" video-js vjs-default-skin" muted="muted" style="width: 100%; height: 100%;" poster="<?php echo base_url().'assets/frontend/images/stream_offline_img.jpeg'; ?>" preload="auto" data-key="" data-random_id="d5efdwa00" data-base_url="<?php echo $base_url; ?>" data-is_record="0" controls="controls">
                        </div>
                        <div class="banner_carousal"></div>
                      </div>
                      <?php // $deg = $deg+60; ?>
                    <?php } ?>
                  </div>
               </div>
            </div>
            <div class="col-sm-1 stream_carousel_indecator">
               <div class="stream_btn nxt_stream col-sm-6"><a class="pull-left"  style="color: #ff5000"><i class="fa fa-caret-right"></i></a></div>
            </div>
         </div>
      </div>
   </section>
<?php } ?> 
<script>
var scc = $(".stream_carousel_container");
carouselcal(scc);
function carouselcal(root) {
     var figure = root.find('.stream_carousel'),
       images = figure.find(".item"),
       n = images.length,
       gap =  0,
       bfc = '',
       theta =  2 * Math.PI / n,
       currImage = 0;

     var v_width = parseFloat(images.width());
     setupCarousel(n, v_width);
     window.addEventListener('resize', () => { 
       setupCarousel(n, v_width) 
     });

     function setupCarousel(n, s) {
       var apothem = s / (2 * Math.tan(Math.PI / n));
       figure.css('transformOrigin', `50% 50% ${- apothem}px`);
       for (var i = 0; i < n; i++)
         images[i].style.padding = `${gap}px`;
       for (i = 0; i < n; i++) {
         images[i].style.transformOrigin = `50% 50% ${- apothem}px`;
         images[i].style.transform = `rotateY(${i * theta}rad)`;
       }
       rotateCarousel(currImage);
     }
     
      $(".nxt_stream").on("click", { d: "n" }, rotate_slider);
      $(".prv_stream").on("click", { d: "p" }, rotate_slider);
      function rotate_slider(e){
        if(e.data.d == "n"){
          currImage++;
        }
        if(e.data.d == "p"){
          currImage--;
        }
        rotateCarousel(currImage);
      }
     function rotateCarousel(imageIndex) {
       figure.css('transform', `rotateY(${imageIndex * -theta}rad)`);
     }
}
</script>