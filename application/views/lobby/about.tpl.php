<section class="body-middle innerpage">
    <?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
    <?php $lby = (isset($_GET['lobby_id']) && $_GET['lobby_id'] != null && $_GET['lobby_id'] != '') ? '?lobby_id='.$_GET['lobby_id'] : ''; ?>
    <div class="container">
        <div class="row header1">
            <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
            <div class="col-lg-4 col-sm-4 col-md-4"><h2>About Me</h2></div>
            <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
        </div>
        <form id="about_me_add_form" name="about_me_add_form"  action="<?php echo base_url().'livelobby/about_me_update/'.$user_data['user_id'].$lby;?>" method="post" enctype="multipart/form-data">
          <div class="row" style="margin-top: 25px; margin-bottom: 25px">
            <div class="form-group col-lg-12" id="content">
              <textarea id="about_me_description" name="about_me_description" class="form-control" placeholder="About Me" rows="7" cols="80" required><?php if(isset($user_data['about_me_description']) && $user_data['about_me_description'] != null && $user_data['about_me_description'] != '') echo $user_data['about_me_description'];?></textarea>
            </div>
          </div>
          <div class="btn-group">
            <div class="col-lg-12 text-center padd0">
              <input type="submit" id="submit" value="Submit" class="btn-update">
            </div>
          </div>
        </form>
    </div>
</section>
<!-- <script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>ckeditor/adapters/jquery.js"></script>   
<script>
  try { CKEDITOR.replace('description'); } catch {}
</script> -->