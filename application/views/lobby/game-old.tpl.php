<section class="login-signup-page">
	<div class="container">
    	<div class="row">
        	<div class="login-signBlock">
            	<h2>Challenge Information</h2>
				<label style="margin-left: 204px;"><?php if(isset($msg)) echo $msg;?></label>
                <form  action="<?php echo base_url(); ?>game/gameCreate" method="post" enctype="multipart/form-data" onsubmit="return image_size_validation()">
                	<div class="form-group">
                    	<label class="col-lg-4 col-sm-4 col-md-4">Game Name</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                        	<input type="text" placeholder="Name" id="name" class="form-control" name="name" required="required">
                        </div>
                    </div>
                    <!-- <div class="form-group">
                    	<label class="col-lg-4 col-sm-4 col-md-4">Price</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                        	<input type="text" placeholder="Price" id="price" class="form-control" name="price" required="required">
                        </div>
                    </div> -->
					<div class="form-group">
                    	<label class="col-lg-4 col-sm-4 col-md-4">Device No.</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                        	<input type="text" placeholder="Device Number" id="number" class="form-control" name="device_number" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                    	<label class="col-lg-4 col-sm-4 col-md-4">Description</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                        	<textarea class="form-control" id="description" placeholder="description" name="description"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="col-lg-4 col-sm-4 col-md-4">Image (190px-112px)</label>
                        <div class="col-lg-8 col-sm-8 col-md-8 form-upload">
                        <div class="fileUpload">
                            <span>Upload</span>
                            <label id="image_size" style="display:none"></label>
                            <input id="uploadBtn" type="file" name="image" class="upload" />
                        </div>
                        <input id="uploadFile" class="form-control" placeholder="Choose File" disabled="disabled" />
                        
                        </div>
                    </div>
                    <div class="btn-group">
                    	<input type="submit" value="Submit" class="btn-submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>