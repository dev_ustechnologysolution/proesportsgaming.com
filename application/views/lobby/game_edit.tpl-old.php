<section class="login-signup-page">
  <div class="container">
      <div class="row">
          <div class="login-signBlock">
              <h2>Game Edit</h2>
                <label style="margin-left: 204px;"><?php if(isset($msg)) echo $msg;?></label>
                <?php if(count($game)>0){?>
                  <form  action="<?php echo base_url(); ?>game/gameUpdate" method="post" enctype="multipart/form-data" onsubmit="return image_size_validation()">
                  <div class="form-group">
                      <label class="col-lg-4 col-sm-4 col-md-4">Game Name</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                          <input type="text" placeholder="Name" id="name" class="form-control" name="name" value="<?php if(isset($game[0]['name']))echo $game[0]['name'];?>">
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="col-lg-4 col-sm-4 col-md-4">Price</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                          <input type="text" placeholder="Price" id="price" class="form-control" name="price" value="<?php if(isset($game[0]['price']))echo $game[0]['price'];?>" required>
                        </div>
                    </div>
          <div class="form-group">
                      <label class="col-lg-4 col-sm-4 col-md-4">Device No.</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                          <input type="text" id="number" class="form-control" name="device_number" required value="<?php if(isset($game[0]['device_id']))echo $game[0]['device_id'];?>">
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="col-lg-4 col-sm-4 col-md-4">Description</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                          <textarea class="form-control" id="description" placeholder="description" name="description"><?php if(isset($game[0]['description'])) echo $game[0]['description'];?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="col-lg-4 col-sm-4 col-md-4">Image (190px-112px)</label>
                        <div class="col-lg-8 col-sm-8 col-md-8 form-upload">
                        <div class="fileUpload">
                            <span>Upload</span>
                            <label id="image_size" style="display:none"></label>
                            <input id="uploadBtn" type="file" name="image" class="upload" />
                        </div>
                        <input id="uploadFile" class="form-control" placeholder="Choose File" disabled="disabled" />
                        
                        </div>
                    </div>
                    <div class="form-group">
                    <div class="game-img col-lg-12 col-sm-12 col-md-12">
                      <img src="<?php echo base_url().'upload/game/'.$game[0]['image']?>" height="100" width="100"/>
                    </div>
                    </div>
                    <div class="btn-group">
                      <input type="hidden" name="id" value="<?php if(isset($game[0]['id'])) echo $game[0]['id'];?>">
                      <input type="hidden" name="old_image" value="<?php if(isset($game[0]['image'])) echo $game[0]['image'];?>">
                      <input type="submit" value="Update" class="btn-submit">
                    </div>
                </form>
                <?php } else {?>
                 <h4>Data not found</h4>
                 <?php }?>
            </div>
        </div>
    </div>
</section>

