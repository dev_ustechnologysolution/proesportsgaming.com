<section class="login-signup-page gamelist-page">
  <div class="container">
    
        <div class="row header2">
          <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
            <div class="col-lg-4 col-sm-4 col-md-4"><h2>GAME LISTS</h2></div>
            <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
        </div>
        
        <div class="row bidhottest">
          <ul class="gmlist">
      
            <?php foreach($list as $value) { ?>
              <li>
                  <div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
                      <img src="<?php echo base_url().'upload/game/'. $value['image'];?>" alt="">
                  </div>
                    <div class="col-lg-2 col-sm-5 col-md-2 bid-info">
                    <h3><?php echo $value['name']; ?></h3>
                    
                    </div>
                    <div class="col-lg-3 col-sm-5 col-md-2 bid-info">
                      <p><?php echo $value['description']; ?></p> 
                    </div>
                    <div class="col-lg-1 col-sm-3 col-md-2 uplod-day">
                      <h4><?php echo $value['created_at']; ?></h4>
                    </div>
                    <div class="col-lg-2 col-sm-4 col-md-2 divPrice">
                      <div class="game-price premium">
                          <h3><?php echo CURRENCY_SYMBOL.$value['price']; ?></h3>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-5 col-md-2 primum-play play-n-rating">
                      <a href="<?php echo base_url().'game/gameEdit/'.$value['id'];?>">Edit</a>
                      <!--<a href="<?php //echo base_url().'admin/game/delete/'.$value['id'];?>" onclick="return confirm('are you sure delete')">Delete</a>-->
                        <!--<span class="glyphicon glyphicon-star"></span>-->
                    </div>
              </li>
            <?php } ?>
        
          </ul>
        </div>
     
  </div>
</section>