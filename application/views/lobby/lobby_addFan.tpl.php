<section class="body-middle innerPage margin-top-10">
    <div class="container">
        <div class="row innerBanner">
            <img src="<?php echo base_url().'assets/frontend/images/inner-banner.jpg' ?>" alt="">
            <div class="bannerCaption">
                <h2>active</h2>
                <img src="<?php echo base_url().'assets/frontend/images/event-flug.png' ?>" alt="">
                <h3><?php echo $list[0]['game_type'].'V'.$list[0]['game_type']; ?></h3>
                <h1><?php echo $list[0]['game_name']; ?></h1>
                <h3> <span><?php echo $list[0]['sub_game_name']; ?></span></h3>
            </div>
        </div>
        <div class="row detailsRow challenge_accpt_form_div">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                 <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10 gm-details-right">
                        <form method="POST" action="<?php echo base_url().'livelobby/insertFan';?>" class="fan-add_form" id="fan-add_form">
                            <div>
                                <div class="col-lg-12 gm-title gm_profile_div">
                                    <div class="col-lg-4">
                                        <div class="challenge_accept_profile_img">
                                            <img src="<?php echo base_url().'upload/profile_img/'. $this->session->userdata('user_image'); ?>" alt="">
                                        </div>
                                    </div>
                                    <div class="col-lg-8" style="display: table;">
                                        <div class="add_fan_bx">
                                            <input type="text" placeholder="ENTER YOUR GAMER TAG" id="fan_device_id" class="form-control" name="fan_device_id" required style="margin-bottom: 6px;" /> 
                                            <input type="text" placeholder="Enter Team Name" class="form-control" name="team_name" value="<?php echo $this->session->userdata('team_name'); ?>" <?php echo ($list[0]['is_event'] == 1 || $list[0]['is_spectator'] == 1) ? 'required' : ''; ?> />
                                            <label style="font-size: 16px;">(Required for Tournaments)</label>
                                            <label class="toggle chkbx_container"><input type="checkbox" name="save_team_settings"> &nbsp;Save in Settings<span class="checkmark"></span></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 exept-chalange">
                                    <input type="hidden" id="lobby_id" name="lobby_id" value="<?php echo $list[0]['id']; ?>">
                                    <input type="submit" class="accept_btn accept_btn_boder-tran" value="Enter Live Lobby">
                                </div>
                            </div>
                            <span class="border_postion_absolute"></span>
                        </form>
                    </div>
                    <div class="col-sm-1"></div>
                 </div>
            </div>
            <div class="col-sm-2"></div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $('#fan-add_form').submit(function () {
        $(this).find('input').each(function(e) {
            if ($(this).attr('required') && ($(this).val() == '' || $(this).val() == null)) {
                $('.accept_btn').trigger('click');
                e.preventDefault();
                return false;
            } 
        });
    });
</script>