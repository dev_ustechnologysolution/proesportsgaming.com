<div class="adm-dashbord">  
  <div class="container padd0">
    <?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
    <?php $this->load->view('dashboard/profile_sidebar.tpl.php');?>
    <div class="col-lg-10">
      <?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
      <div class="col-lg-12">
        <div class="myprofile-edit">
          <h2>Stream Information</h2>
          <div class="col-lg-12">
            <div class="row custom_settings" align="center">
              <label style="display: none"> <input type="checkbox" class="" name="" id="" value="" > Custom Settings </label>
            </div>
            <div class="row">
              <form id="frm_game_create" name="frm_game_create"  action="<?php echo base_url();?>livelobby/lobbyCreate" method="post" enctype="multipart/form-data">
                <div class="form-group">
                  <label class="col-lg-4">Platform</label>
                  <div class="input-group col-lg-8">
                    <?php if (isset($game_cat)) { ?>
                      <select id="game_category" class="form-control game_category select21" name="game_category">
                        <option value="">-- Select a system --</option>
                        <option value="<?php echo $game_cat[0]['id']; ?>" cat_img="<?php echo $game_cat[0]['cat_img']; ?>"><?php echo $game_cat[0]['category_name'] ?></option>
                      </select>
                    <?php } else { ?>
                      <select id="game_category" class="form-control game_category select21" name="game_category">
                        <option value="">-- Select a system --</option>
                        <?php foreach ($game_category as $value) { ?>
                          <option value="<?php echo $value['id']; ?>" cat_img="<?php echo $value['cat_img']; ?>"><?php echo $value['cat_slug']; ?></option>
                        <?php } ?>
                      </select>
                    <?php } ?>
                  </div>
                </div>
                <div class="form-group select_games">
                  <label class="col-lg-4">Game Name</label>
                  <div class="input-group col-lg-8">
                    <?php if(isset($game_dat)) { ?>
                      <select id="name" class="form-control game_name select2" name="game_name">
                        <option value="<?php echo $game_dat[0]['id']; ?>"><?php echo $game_dat[0]['game_name'] ?></option>
                      </select>
                    <?php } else { ?>
                      <select id="name" class="form-control game_name select2" name="game_name">
                        <option value="">--Select a Game--</option>
                      </select>
                    <?php } ?>
                  </div>
                </div>
                <div class="form-group select_games">
                  <label class="col-lg-4">Game Sub Category</label>
                  <div class="input-group col-lg-8">
                    <?php if(isset($subgame) && !empty($subgame)) { ?>
                      <select id="sub_name" class="form-control subgame_name_id select2" name="subgame_name_id">
                        <option value="<?php echo $subgame[0]['id']; ?>"><?php echo $subgame[0]['sub_game_name'] ?></option>
                      </select>
                    <?php } else { ?>
                      <select id="sub_name" class="select2 form-control subgame_name_id" name="subgame_name_id">
                        <option value="0">--Select a Sub Game--</option>
                      </select>
                    <?php } ?>
                  </div>
                </div>
                <!-- <div class="form-group custom_games">
                  <label class="col-lg-4">Game System</label>
                  <div class="input-group col-lg-8"> -->
                     <!-- <?php // if (isset($game_cat)) { ?>
                      <select id="game_category" class="form-control game_category select21">
                        <option value="">-- Select a system --</option>
                        <option value="<?php // echo $game_cat[0]['id']; ?>" cat_img="<?php // echo $game_cat[0]['cat_img']; ?>"><?php // echo $game_cat[0]['category_name'] ?></option>
                      </select>
                    <?php // } else { ?>
                      <select id="game_category" class="form-control game_category select21">
                        <option value="">-- Select a system --</option>
                        <?php // foreach ($game_category as $value) { ?>
                          <option value="<?php // echo $value['id']; ?>" cat_img="<?php // echo $value['cat_img']; ?>"><?php // echo $value['cat_slug']; ?></option>
                        <?php // } ?>
                      </select>
                    <?php // } ?> -->
                    <!-- <?php// if(isset($cust_game_cat)) { ?>
                      <select id="game_category" class="select2 form-control game_category">
                        <option value="<?php// echo $cust_game_cat[0]['id']; ?>"><?php// echo $cust_game_cat[0]['category_name'] ?></option>
                      </select>
                    <?php// } else { ?>
                      <input type="text" placeholder="Enter Game Category" class="form-control select_game_category">
                    <?php// } ?> -->
                  <!-- </div>
                </div> -->
                <div class="form-group custom_games">
                  <label class="col-lg-4">Stream Title</label>
                  <div class="input-group col-lg-8">
                    <input type="text" placeholder="Stream Title" class="form-control select_game_name">
                  </div>
                </div>
                <div class="form-group custom_games">
                  <label class="col-lg-4">Game Info or Stream Info</label>
                  <div class="input-group col-lg-8">
                    <input type="text" placeholder="Game Info or Stream Info" class="form-control select_subgame_name_id">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-4">Game Size</label>
                  <div class="input-group col-lg-8">
                    <select class="select2 form-control" name="game_type"  id="game_type">
                      <option>--Select Game Size--</option>
                      <?php $game_type_raw = explode('|',$game_dat[0]['game_type']);
                      if(isset($game_type_raw[0])) {
                        foreach($game_type_raw as $j) {
                          if($j!='') { 
                            echo '<option value="' . $j . '">--' . $j . 'V' . $j . '--</option>';
                          }
                        }
                      } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-4">Entry Amount</label>
                  <div class="input-group col-lg-8">
                    <input type="text" placeholder="$0.00" id="lobby_amount" class="form-control" name="bet_amount" onkeyup="return isNumbKey(event,this.id)" onkeydown="return isNumbKey(event,this.id)">
                    <span id="demo" style="margin-left: -194px;color: red;"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-4">Gamer Tag</label>
                  <div class="input-group col-lg-8">
                    <input type="text" placeholder="Enter Your Gamer Tag" id="number" class="form-control" name="device_number" required="required">
                    <span style="color:red"><?php echo @$this->session->flashdata('required_msg');?></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-4">Password</label>
                  <div class="input-group col-lg-8 lobby_password_div">
                    <label class="switch lobby_password_switch_button">
                      <input type="checkbox" name="friends_mail" value="friends_mail">
                      <span class="slider round"></span>
                    </label>
                    <input type="text" class="form-control lobby_password" placeholder="Enter Password" style="display: none;">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-4">For 18+</label>
                  <div class="input-group col-lg-8 lobby_password_div">
                    <label class="switch lobby_for_18_plus_switch_button">
                      <input type="checkbox" name="for_18_plus">
                      <span class="slider round"></span>
                    </label>
                  </div>
                </div>
                <!-- <div class="form-group">
                  <label class="col-lg-4">Rules</label>
                  <div class="input-group col-lg-8">
                    <?php if(isset($game_dat) && !empty($game_dat)) { ?>
                      <textarea class="form-control" id="description" placeholder="Game Rules" rows="5" cols="5" name="description"><?php echo $game_dat[0]['game_description'] ?></textarea>
                    <?php } else { ?>
                      <textarea class="form-control" id="description" placeholder="Game Rules" rows="5" cols="5" name="description"></textarea>
                    <?php } ?>
                  </div>
                </div> -->
                <div class="form-group">
                  <label class="col-lg-4">Live Lobby link</label>
                  <div class="input-group col-lg-8">
                    <div class="col-lg-7">
                      <span style="color: #ff5000"><?php echo base_url().'livelobby/'; ?></span>
                    </div>
                    <div class="col-lg-5 pull-right">
                      <input type="text" name="txt_livelobby_link" id="txt_livelobby_link" class="form-control" placeholder="" required="">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <label class="lobby_exist_error_add pull-right" style="color: #fff;"></label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-4">Image</label>
                  <div class="input-group col-lg-8">
                    <div id="game_img">
                      <?php if (isset($game_img) && !empty($game_img)) {
                        foreach ($game_img as $value) { ?>
                          <img class="img-cls" id="img<?php echo $value['id'] ?>" style="margin-right: 31px;" onclick="a('<?php echo $value['id'] ?>')" src="<?php echo base_url().'upload/game/'.$value['game_image'] ?>" width="100" height="80"/>
                        <?php }
                      } ?>
                    </div>
                  </div>
                </div>
                <div class="btn-group">
                  <div class="col-lg-8 pull-right padd0">
                    <input type="hidden" name="personal_challenge_friend_id">
                    <input type="hidden" id="game_image_id" name="game_image_id" value="" class="new_game-image">
                    <input type="hidden" id="add_rules_hidden" name="add_rules_hidden">
                    <input type="button" id="submitPlaceChallenge" value="Submit" class="btn-update">
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>