<section class="body-middle innerpage">
   <div class="container">
      <!--Xbox Game Challange -->
      <div class="row header1" align="center">
         <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
         <div class="col-lg-4 col-sm-4 col-md-4">
            <h2>HOTTEST GAMES </h2>
            <h3 style="color: #ff5000;">Live Lobbies</h3>
         </div>
         <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
      </div>
      <?php if (!empty($lobby_list)) { ?>
         <?php $this->load->view('common/live_lobby_filter.tpl.php'); ?>
         <div class="bidhottest hottestgame playstore_game_list_front">
            <li class="expiredtime_event row hidden_timer_row" style="display: none;">
                <div class="countdown_with_day rgclose clearfix">
                </div>
              </li>
            <li class="row livelobby_home_row hidden_row" style="display: none;">
               <div class="col-sm-3 bidimg padd0 lobby_stream_iframe_div">
                  <iframe style="display: none;" class="live_stream_iframe" src="" height="auto" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
                   <div style="height: 100%; display: none;" id="">
                     <div class="stream_error" class="alert alert-danger" style="display: none;"></div>
                     <video id="videojs-flvjs-player" autoplay muted="muted" class="video-js vjs-default-skin" controls style="width: 100%; height: 100%;" poster="<?php echo base_url().'assets/frontend/images/stream_offline_img.jpeg'; ?>" preload="auto" data-key="" data-random_id="" data-base_url="" data-user_id="" data-lobby_id="" data-group_bet_id="" data-is_record='0'></video>
                  </div>
               </div>
               <div class="col-sm-3 bid-info">
                  <div class="lb_list">
                     <div class="dly_lst_verticle">
                        <h3></h3>
                        <p></p>
                     </div>
                  </div>
               </div>
               <div class="col-sm-3 divPrice">
                  <div class="lb_list">
                     <div class="dly_lst_verticle">
                        <div class="game-price premium">
                           <h3></h3>
                        </div>
                        <p class="text-center margin0" style="color: #ff5807;"> Challenges Accepted:  <?php echo '<span>0</span> V <span>0</span>'; ?></p>
                     </div>
                  </div>
               </div>
               <div class="col-sm-1 category_img">
                  <div class="lb_list">
                     <div class="dly_lst_verticle">
                        <img src="<?php echo base_url(); ?>upload/all_images/XBOX.png" class="img img-responsive game_cat_img">
                     </div>
                  </div>
               </div>
               <div class="col-sm-2 padd0 lobby_join_btn_div">
                  <div class="lb_list">
                     <div class="dly_lst_verticle">
                        <div class="join_btn_div_lobby row">
                           <div class="join_btn clearfix animated rotateIn">
                              <a href="" data-original-title="'Join Live Event'" data-placement="bottom" data-toggle="tooltip">Join Stream</a>
                           </div>
                        </div>
                        <div class="clearfix primum-play play-n-rating lobby_device_tag device_id">
                           <a class="animated-box"></a>
                        </div>
                     </div>
                  </div>
               </div>
            </li>

            <ul>
               <?php 
               echo $this->session->flashdata('err_lobby');
               unset($_SESSION['err_lobby']);
               foreach ($lobby_list as $val) {
                  ?>
                  <li class="row livelobby_home_row">
                     <div class="col-sm-3 bidimg padd0 lobby_stream_iframe_div">
                        <?php if ($val['stream_status'] == 'enable' && ((empty($val['obs_stream_channel']) && !empty($val['stream_channel'])) || (!empty($val['obs_stream_channel']) && empty($val['stream_channel'])) || (!empty($val['obs_stream_channel']) && !empty($val['stream_channel'])))) {
                              if($val['stream_type'] == '1'){
                                 $stream_url = 'https://player.twitch.tv/?autoplay=false&channel='.$val['stream_channel'].'&parent='.$_SERVER['HTTP_HOST'];                   
                           ?>
                           <iframe class="live_stream_iframe" src="<?php echo $stream_url; ?>" height="auto" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
                           <?php }else if($val['stream_type'] == '2'){
                                 $stream_key = $val['obs_stream_channel'];
                                 $new_stream_key = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(8/strlen($x)) )),1,8);
                                 // $base_url = (base_url() != 'https://proesportsgaming.com/') ? 'https://esportstesting.com/' : base_url();
                                 $base_url = base_url();
                                 $random_id = $new_stream_key;
                                 $user_id = $val['lobby_crtor'];
                                 // if(!empty($result->lobby_id)){$lobby_id = $result->lobby_id;}
                                 // if(!empty($result->group_bet_id)){$group_bet_id = $result->group_bet_id;}
                              ?>
                              <div style="height: 100%;" id="<?php echo $random_id; ?>">
                                 <div class="stream_error" class="alert alert-danger" style="display: none;"></div>
                                 <video id="videojs-flvjs-player" autoplay muted="muted" class="obs_streaming video-js vjs-default-skin" controls style="width: 100%; height: 100%;" poster="<?php echo base_url().'assets/frontend/images/stream_offline_img.jpeg'; ?>" preload="auto" data-key="<?php echo $stream_key; ?>" data-random_id="<?php echo $random_id; ?>" data-base_url="<?php echo $base_url; ?>" data-user_id="<?php echo $user_id; ?>" data-lobby_id="<?php echo (isset($lobby_id)) ? $lobby_id : ''; ?>" data-group_bet_id="<?php echo (isset($group_bet_id)) ? $group_bet_id : ''; ?>" data-is_record='0'></video>
                              </div>
                        <?php }} else { ?>
                           <img src="<?php echo base_url().'upload/game/'. $val['game_image'];?>" alt="">
                        <?php } ?>
                     </div>
                     <div class="col-sm-3 bid-info">
                        <div class="lb_list">
                           <div class="dly_lst_verticle">
                              <h3><?php echo $val['game_name']; ?></h3>
                              <p><?php echo $val['sub_game_name']; ?></p>
                           </div>
                        </div>
                     </div>
                     <!-- <div class="col-sm-1 uplod-day padd0">
                        <p class="best_of"> <?php // echo ($val['best_of'] != null)    ? 'Best of '.$val['best_of'] : ''?></p>
                     </div> -->
                     <div class="col-sm-3 divPrice">
                        <div class="lb_list">
                           <div class="dly_lst_verticle">
                              <div class="game-price premium">
                                 <h3><?php $lobby_bet_price = number_format((float) $val['price'], 2 , '.', '');
                                    echo ($lobby_bet_price > 0) ? CURRENCY_SYMBOL.$lobby_bet_price : 'FREE';
                                    ?></h3>
                              </div>
                              <p class="text-center margin0" style="color: #ff5807;"> Challenges Accepted:  <?php echo '<span>'.$val['game_type'].'</span> V <span>'.$val['game_type'].'</span>'; ?></p>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-1 category_img">
                        <div class="lb_list">
                           <div class="dly_lst_verticle">
                              <?php echo ($val['cat_img'] != '') ? '<img src="'.base_url().'upload/all_images/'.$val['cat_img'].'" class="img img-responsive game_cat_img">' : ''; ?>      
                           </div>
                        </div>
                     </div>
                     <?php $lobbylink = ($val['is_event'] == 1) ? 'Live Event' : 'Live Stream'; ?>
                     <div class="col-sm-2 padd0 lobby_join_btn_div">
                        <div class="lb_list">
                           <div class="dly_lst_verticle">
                              <div class="join_btn_div_lobby row">
                                 <div class="join_btn clearfix animated rotateIn">
                                    <a href="<?php echo base_url().'livelobby/watchLobby/'.$val['id'];?>" data-original-title="<?php echo 'Join '.$lobbylink; ?>" data-placement="bottom" data-toggle="tooltip">Join Stream</a>
                                 </div>
                              </div>
                              <div class="clearfix primum-play play-n-rating lobby_device_tag device_id">
                                 <a class="animated-box"><?php echo $val['device_id']; ?></a>
                              </div>
                              <!-- <div class="primum-play text-right desc_sec_div"><a class="animated-box" target="_blank">Rules</a><div class="description" style="display:none;">test</div></div> -->
                              <?php echo (!empty($val['description']) && $val['description'] != null) ? '<div class="primum-play text-right desc_sec_div"><a class="animated-box" target="_blank"  href="'.base_url().'livelobby/rulesAdd/'.$val['id'].'">Rules</a><div class="description" style="display:none;">'.$val['description'].'</div></div>' : ''; ?>
                           </div>
                        </div>
                     </div>
                  </li>
               <?php } ?>
            </ul>
         </div>
      <?php } else {
         echo "<h5><div class='row bidhottest_err'><div class='col-sm-12' style='color: #FF5000;'>Lobby Not Available</div></div></h5>";
      } ?>
      <div class="row pager-row">
         <div class="game_paging">
            <?php echo $paging_two; ?>
         </div>
      </div>
      
   </div>
</section>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/video.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/videojs-flvjs.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/flv.min.js'; ?>"></script>
