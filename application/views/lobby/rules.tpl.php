<section class="body-middle innerpage">
  <?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
  <div class="container">
    <div class="row header1">
      <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
      <div class="col-lg-4 col-sm-4 col-md-4"><h2>Rules</h2></div>
      <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
    </div>
    <?php 
    if (in_array($type_get, array('match','custom_event'))) {
      $rules_desc = $rules_data['description'];
    } else {
      if ($rules_data['is_event'] == 1)
        $rules_desc = $rules_data['event_description'];
      else
        $rules_desc = $rules_data['description'];
    }
    if ($rules_data['user_id'] != $this->session->userdata('user_id')) { ?>
      <div class="row">
        <div class="form-group col-lg-12">
          <?php if ($rules_desc != strip_tags($rules_desc)) {
            print_r($rules_desc);
          }else{
            print_r(nl2br($rules_desc));
          } ?>
        </div>
      </div>
    <?php } else { 
      if (isset($rules_desc) && $rules_desc != strip_tags($rules_desc)) {
        $chk = 'checked';
      } else {
        $chk = '';
      } ?>
      <form id="rules_add_form" name="rules_add_form"  action="<?php echo base_url();?>livelobby/rulesCreate/<?php echo $id;?>?type=<?php echo $type_get;?>" method="post" enctype="multipart/form-data">
        <div class="row">
          <div class="col-lg-12">
            <div class="col-lg-12 notifications_div">
                <label class="switch"><input type="checkbox" name="advance_setting_check" id="advance_setting_check" <?php echo $chk;?> ><span class="slider round"></span></label>
            </div>
            <div class="title" style="position: absolute; margin-left: 7%;">Advance Settings</div>
          </div>
        </div>
        <div class="row" style="margin-top: 25px; margin-bottom: 25px">
          <div class="form-group col-lg-12 simple" id="content">
            <textarea id="editor" class="form-control" placeholder="Enter Rules" name="description" rows="17" cols="80"><?php if(isset($rules_desc)) echo preg_replace('#<[^>]+>#', '', $rules_desc);?></textarea>
          </div>
          <div class="form-group col-lg-12 advance" id="content" style="display: none;">
            <textarea id="editor" name="description1" class="ckeditor" rows="10" cols="80"><?php if(isset($rules_desc)) echo $rules_desc;?></textarea>
          </div>
        </div>
        <div class="btn-group">
          <div class="col-lg-12 text-center padd0">
            <input type="submit" id="submit" value="Submit" class="btn-update">
          </div>
        </div>
      </form>
    <?php } ?>
  </div>
</section>
<script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>ckeditor/adapters/jquery.js"></script>   
<script type="text/javascript">
  $(document).ready(function(){
    if($('#advance_setting_check').is(":checked")) {
      $('.simple').hide();
      $('.advance').show();
    }else{
      $('.simple').show();
      $('.advance').hide();
    }
  });
  $('#advance_setting_check').on('change', function() {
    if($(this).is(":checked")) {
      $('.simple').hide();
      $('.advance').show();
    }else{
      $('.simple').show();
      $('.advance').hide();
    }
  });
</script>
<script>
  try { CKEDITOR.replace('description'); } catch {}
</script>