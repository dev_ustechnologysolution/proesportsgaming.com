<div id="amount_distribution_modal" class="modal custmodal" style="display: none;">
	<div class="modal-content" style="width:70%">
		<div class="modal-header">
			<div class="row">
				<span class="close">×</span>
				<h1 class="center">Distribute Tournament Amount</h1>
				<div class="col-sm-12 text-center total_tournament_price_coll">
	                <div class="distribute_total_price_div">
	                  	<div class="clearfix">
	                    	<h4>Total Distribution Amount</h4>
	                    	<h2 class="total_dist_amt"></h2>
	                    	<input type="hidden" name="dist_type" id="dist_type" value="percentage">
	                    	<input type="hidden" name="dist_amount_hidden" id="dist_amount_hidden">
	                  		<div class="distribution_switch_box">
			                  <label class="switch" data-original-title="Switch to change distribution type" data-placement="bottom" data-toggle="tooltip"><input type="checkbox" name="distribution_type_select" id="distribution_type_select"><span class="slider round"></span></label>
			                </div>
	                    	<h2 class="total_dist_per lable_orange"></h2>
	                  	</div>
	                </div>
	            </div>
			</div>
			<div class="amount_distribution_div">
				<div style="display: block;">
					<div class="modal_div_border remove_padding">
						<div class="ga_table">
							<div class="col-sm-12 amt_tables">
								
				            </div>
				        </div>
						<div class="">
							<div class="form-group text-left">
								<div class="input-group col-lg-12">
									<span class="span_error" id="greater_amount_error" style="color: red; display: none;">Total of all user's percentage shouldn't be greater than 100</span>
								</div>
								<div class="input-group col-lg-12">
									<span class="span_error" id="enough_balance_error" style="color: red; display: none;"></span>
								</div>
							</div>
						</div>
					</div>
					<div class="input-group col-lg-12">
						<h4 class="span_error" id="dist_note" style="color: red;">*Note: Distribution amount should be in percentage</h4>
					</div>
					<div class="form-group text-left" style="padding-top: 50px;">
						<div class="input-group col-lg-12">
							<a class="button yes distribution_submit" action="ok" stream_type="obs">Submit</a>
							<a class="button no">Cancel</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>