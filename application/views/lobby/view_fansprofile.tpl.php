<?php 
  $subscribedlist = $this->General_model->subscribedlist($this->session->userdata('user_id'));
  $subscribed_plan = '';
  $payment_id = '';
  foreach ($subscribedlist as $key => $sl) {
    if ($sl->user_to == $this->session->userdata('user_id')) {
      $subscribed_plan = $subscribedlist[0]->plan_detail;
      $payment_id = $subscribedlist[0]->payment_id;
    }
  }

  $default_stream = $this->General_model->get_stream_setting($view_lobby_fan_profile[0]->id,'','default_stream')[0];
?>
<section class="body-middle innerPage">
  <?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
  <div class="container view_lobby_container">
    <div class="row header1">
      <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
      <div class="col-lg-4 col-sm-4 col-md-4">
        <h2><?php //echo $view_lobby_fan_profile[0]->name; ?></h2>
        <h2><?php echo $view_lobby_fan_profile[0]->fan_tag.' Profile'; ?> </h2>
      </div>
      <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
    </div>
    <div class="col-lg-12 padd0">
      <div class="myprofile-edit row">
        <!-- <div class="col-sm-3"></div> -->
        <div class="col-sm-12">
          <div class="row">
          	<div class="col-sm-12">
              <div class="form-group profile_image_div">
                <div class="col-lg-12">
                  <div class="friend_profile_img text-center">
                    <img src="<?php if($view_lobby_fan_profile[0]->image != null && $view_lobby_fan_profile[0]->image != ''){ echo base_url('/upload/profile_img/'.$view_lobby_fan_profile[0]->image); }else{echo base_url('/upload/profile_img/default_profile_img.png');} ?>" id="k">
                  </div>
                </div>
                <div class="col-lg-12 text-center "> 
                  <?php //echo "<pre>"; print_r($view_lobby_fan_profile);?>                  
                  <p class="white"><?php echo (isset($view_lobby_fan_profile[0]->display_name) && $view_lobby_fan_profile[0]->display_name != '') ? $view_lobby_fan_profile[0]->display_name : $view_lobby_fan_profile[0]->name; ?></p>
                </div>
                <div class="col-lg-12 padd10">
                  <div class="text-center">
                    <iframe src="https://player.twitch.tv/?channel=<?php echo $default_stream->default_stream_channel; ?>&parent=<?php echo $_SERVER['HTTP_HOST']; ?>" height="400" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
                  </div>
                </div> 
                <div class="col-lg-12">
                  <div class="col-lg-4 friend_plyr_id"> 
                    <label class="font35 pull-left">Account NO #<?php echo $view_lobby_fan_profile[0]->account_no; ?></label>
                  </div>
                  <div class="col-lg-8 padd10" style="text-align: right;">
                    <?php if(($view_lobby_fan_profile[0]->about_me_description == '' || $view_lobby_fan_profile[0]->about_me_description == null) && $view_lobby_fan_profile[0]->user_id == $this->session->userdata('user_id')){?>

                      <a data-id="<?php echo $view_lobby_fan_profile[0]->user_id; ?>" href = "<?php echo base_url().'livelobby/add_about_me/'.$this->session->userdata('user_id').'?lobby_id='.$_GET['lobby_id']; ?>" class="lobby_btn">About Me</a>&nbsp;&nbsp;

                    <?php } ?>
                    <a data-id="<?php echo $view_lobby_fan_profile[0]->user_id; ?>" class="lobby_btn social_follow <?php echo ($view_lobby_fan_profile[0]->user_id == $this->session->userdata('user_id')) ? 'disabled': ''?>"><?php echo ucfirst($is_followed).'<span class="cunt" style="margin-left: 5px;">'.(($followercount > 0) ? $followercount: '').'</span>'; ?></a>&nbsp;&nbsp;

                    <a class="lobby_btn" href="<?php echo base_url().'Store/'.$view_lobby_fan_profile[0]->id; ?>" target="_blank">Store</a>&nbsp;&nbsp;

                    <a class="lobby_btn subscribe_button commonmodalclick <?php echo ($view_lobby_fan_profile[0]->user_id == $this->session->userdata('user_id')) ? 'disabled': ''?>" streamer_name='<?php echo $view_lobby_fan_profile[0]->name; ?>' streamer_id="<?php echo $view_lobby_fan_profile[0]->id; ?>" streamer_img="<?php echo ($view_lobby_fan_profile[0]->image != '') ? $view_lobby_fan_profile[0]->image : $this->General_model->get_user_img(); ?>" plan_subscribed="<?php echo $subscribed_plan; ?>" payment_id="<?php echo $payment_id; ?>">Subscribe</a>&nbsp;&nbsp;

                    <a data-id="<?php echo $view_lobby_fan_profile[0]->user_id; ?>" class="lobby_btn profile_tip <?php echo ($view_lobby_fan_profile[0]->user_id == $this->session->userdata('user_id')) ? 'disabled': ''?>">Tip</a>

                    <input type="hidden" name="user_id" value="<?php echo $this->session->userdata('user_id')?>">

                  </div>
                </div>
                <?php if(isset($view_lobby_fan_profile[0]->about_me_description) && $view_lobby_fan_profile[0]->about_me_description != '' && $view_lobby_fan_profile[0]->about_me_description != null){?>
                  <div class="col-lg-2"></div>
                  <div class="col-lg-8 myprofile-edit " style="border: 1px dashed #ff5000; border-radius: 5px;">
                      <div class="col-lg-12">
                        <span><?php echo nl2br($view_lobby_fan_profile[0]->about_me_description); ?></span>
                      </div>
                      <?php if($view_lobby_fan_profile[0]->user_id == $this->session->userdata('user_id')){ ?>
                        <div style="position: absolute;top: -10px;right: -12px; background-color: #ff5000; padding: 10px; border-radius: 50%;">
                          <a href = "<?php echo base_url().'livelobby/add_about_me/'.$this->session->userdata('user_id').'?lobby_id='.$_GET['lobby_id']; ?>" data-toggle = "tooltip" data-original-title = "Edit About Info">
                            <i class="pull-right fa fa-pencil" style="color: white;"></i>
                          </a>
                        </div>
                      <?php } ?>
                  </div>
                  <div class="col-lg-2"></div>
                <?php }?>
                <div class="col-lg-12 text-center myprofile-edit">
                  <?php if(isset($view_lobby_fan_profile[0]->team_name) && $view_lobby_fan_profile[0]->team_name != ''){?>
                    <div class="col-lg-12">
                      <p>Esports Team</p>
                    </div>
                    <div class="col-lg-12 friend_plyr_id">
                      <label class="white no-padding"><?php echo $view_lobby_fan_profile[0]->team_name; ?></label>
                    </div>
                  <?php }?>
                </div>
                <div class="col-lg-12" align="center">
                   <div class="input-group back_button">
                      <a href="javascript:history.go(-1)">Back</a>
                   </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- <div class="col-sm-3"></div> -->
      </div>
    </div>
  </div>
  <div id="send_tip_amount_profile" class="modal custmodal send_teamtip_amount_profile" style="display: none;">
    <div class="modal-content">
       <div class="modal-header">
          <span class="close">&times;</span>  
          <h1 class="text-left">Please Enter Donation Amount</h1>
          <div class="modal_div_border row">
            <div class="row team_donate_bottom_div">
              <form id="profile_form" action="<?php echo base_url(); ?>livelobby/send_tip_to_profile" method="POST">
              <div class="col-sm-12">
                <div class="col-sm-6 profile_image_div">
                  <img src="<?php if($view_lobby_fan_profile[0]->image != null && $view_lobby_fan_profile[0]->image != ''){ echo base_url('/upload/profile_img/'.$view_lobby_fan_profile[0]->image); }else{echo base_url('/upload/profile_img/default_profile_img.png');} ?>" id="k" style="border-radius: 50%;">
                </div>
                <div class="col-sm-6 text-left" style="vertical-align: middle;">
                  <div class="row" style="margin-top: 10px;"><h2 class="padd10"><?php echo (isset($view_lobby_fan_profile[0]->display_name) && $view_lobby_fan_profile[0]->display_name != '') ? $view_lobby_fan_profile[0]->display_name : $view_lobby_fan_profile[0]->name; ?></h2><h1>Account NO #<?php echo $view_lobby_fan_profile[0]->account_no; ?></h1></div>
                  <div class="row">
                    <!-- <label class="text-left">AMOUNT</label> -->
                       <input type="number" class="form-control" name="tip_amount_input_profile" id="tip_amount_input_profile" placeholder="0.00">
                  </div><br>
                  <div class="row">
                      <input type="button" value="Submit" class="lobby_btn tip_submit">
                  </div>
                </div>
                <input type="hidden" name="send_from" value="<?php echo $this->session->userdata('user_id');?>">
                <input type="hidden" name="send_to" value="<?php echo $view_lobby_fan_profile[0]->id;?>">
                <input type="hidden" name="lobby_id" value="<?php echo $_GET['lobby_id'];?>">
              </div>
              </form>
              </div>                  
                <div class="row tip_amount_error_row">
                  <div class="col-sm-12">
                    <div class="form-group">
                       <div class="input-group col-lg-12">
                          <div class="alert alert-danger tip_amount_error"></div>
                       </div>
                    </div>
                  </div>
               </div>
            </div>
          </div>
       </div>
    </div>  
</section>
<style>
.font35 {
   font-size:35px !important;
   padding:0px !important;
}
iframe{
  border: 1px solid #ff5000;
}
</style>
<script type="text/javascript">
  $('.profile_tip').on('click', function() {
    $('#send_tip_amount_profile').show()
  });
  $('.tip_submit').on('click', function(e){
    e.preventDefault();
    var total_amount = '<?php echo $this->session->userdata('total_balance');?>';
    var tip_amount = parseFloat($('#tip_amount_input_profile').val());
    if($('#tip_amount_input_profile').val() == '' || $('#tip_amount_input_profile').val() == null){
      $('.tip_amount_error').html('Please enter amount to donate.');
      $('.tip_amount_error_row').show();
      return false;
    }else if ((tip_amount > total_amount) || total_amount == 0) {
      $('.tip_amount_error').html('You have insufficient balance to donate.');
      $('.tip_amount_error_row').show();
      return false;
    }else{
      $('#profile_form').submit();
    }
  });
</script>