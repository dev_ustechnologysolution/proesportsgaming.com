<section class="body-middle innerPage padding-top-50">
  <?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
  <?php 
  $lobby_creat_fee = $this->General_model->view_all_data('admin_lobby_fee', 'id', 'asc');
  $price_add_decimal = number_format((float) $lobby_bet[0]['price'] + $lobby_creat_fee[0]['fee_per_game'], 2, '.', '');
  $lobby_joinfee =  number_format((float) ($lobby_bet[0]['price'] * $lobby_creat_fee[0]['fee_per_game']) / 100, 2, '.', '');
  $session_data  = array(
    'lobby_id' => $lobby_bet[0]['lobby_id'],
    'lobby_price' => $lobby_bet[0]['price']
  );
  $this->session->set_userdata($session_data);
  ?>
  <div class="container view_lobby_container view_lobby_container_width">
    <div class="row">
      <div class="col-sm-12 live_stream_div">
        <?php
        $lobby_creator = $lobby_creator_det->user_id;
        $lby_is_spectator = $lobby_bet[0]['is_spectator'];
        $spectator_list = $wtnglist['spectator_list'];
        $get_ticket_detail = $wtnglist['get_ticket_detail'];
        if ($lobby_creator == $_SESSION['user_id']) {
          $lbyctr = 1;
          echo "<input type='hidden' name='crtr' value='".$lobby_creator."'>";
        }
        $two_ch = $left_top_ch = $right_top_ch = $center_top_ch = [];
        $all_streamdetail = array_merge($wtnglist['leftstreamdetail'], $wtnglist['rightstreamdetail']);
        $creator_stream_key = array_search($lobby_creator, array_column($all_streamdetail, 'user_id'));

        $streamer_chat_id_set = [];
        foreach ($_SESSION['stream_store'] as $k => $ss) {
          if (!in_array(array_merge($left_top_ch,$right_top_ch),$ss['streamer_chat_id_set'])) {
            $getstreamer_id = array_search($ss['streamer_chat_id_set'], array_column($all_streamdetail, 'user_id'));

            if ($ss['streamer_chat_id_set'] == $lobby_creator && $all_streamdetail[$getstreamer_id]->stream_status == 'enable') {
              $center_top_ch[] = $ss['streamer_chat_id_set'];
            } else {
              if ($ss['streamer_chat_id_set'] == $all_streamdetail[$getstreamer_id]->user_id) {
                if (strtolower($all_streamdetail[$getstreamer_id]->table_cat) == "left") {
                  $left_top_ch[] = $ss['streamer_chat_id_set'];
                } else {
                  $right_top_ch[] = $ss['streamer_chat_id_set'];
                } 
              }
            }
            $streamer_chat_id_set[] = $ss['streamer_chat_id_set'];
          }
        }
        $everyone_readup = ($lobby_bet[0]['is_full_play'] == 'yes') ? 'everyone_readup = "yes"' : 'everyone_readup = "no"';
        
        $is_lefttable_full = $lobby_bet[0]['game_type'] == count($lobby_game_bet_left_grp) || $is_full_play == 'yes' ? 'yes' : 'no' ;
        $is_righttable_full = $lobby_bet[0]['game_type'] == count($lobby_game_bet_right_grp) || $is_full_play == 'yes' ? 'yes' : 'no' ;
        ?>
        <div class="row lobby_page_heading">
           <div class="col-sm-12">
              <h2 align="center" class="lobby_title" onclick="close_window(event);"><?php echo $lobby_bet[0]['lobby_name']; ?></h2>
              <h3 align="center"><?php echo $lobby_bet[0]['sub_game_name']; ?></h3>
              <div class="edit_lobby_det text-center padd10">
                <?php if ($lbyctr == 1) { ?>
                  <a class="lobby_btn make_lobby_event" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Add Event to this Lobby" is_spectator="<?php echo $lobby_bet[0]['is_spectator']; ?>" is_event="<?php echo $lobby_bet[0]['is_event']; ?>" event_image="<?php echo $lobby_bet[0]['event_image']; ?>" event_price='<?php echo $lobby_bet[0]['event_price']; ?>' spectate_price='<?php echo $lobby_bet[0]['spectate_price']; ?>' event_title="<?php echo $lobby_bet[0]['event_title']; ?>" event_description="<?php echo $lobby_bet[0]['event_description']; ?>" event_start_date="<?php echo ($lobby_bet[0]['event_start_date'] !='') ? $this->User_model->custom_date(array('date' => $lobby_bet[0]['event_start_date'],'format' => 'Y/m/d H:i')) : $this->User_model->custom_date(array('format' => 'Y/m/d H:i')); ?>" event_end_date="<?php echo ($lobby_bet[0]['event_end_date'] !='') ? $this->User_model->custom_date(array('date' => $lobby_bet[0]['event_end_date'],'format' => 'Y/m/d H:i')) : $this->User_model->custom_date(array('format' => 'Y/m/d H:i')); ?>" already_member="<?php echo (count($wtnglist['get_my_plan']) > 0 || json_decode($this->session->userdata('common_settings'), true)['free_membership'] == 'on') ? 'yes' : 'no'; ?>" event_key='<?php echo $lobby_bet[0]['event_key']; ?>'> Make Event </a>
                  <?php if (count($get_play_status_lobby_bet) == 0) { ?>
                    <a class="lobby_btn change_lobby_dtil" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Edit Lobby Detail"> Edit Lobby </a>
                  <?php } ?>
                <?php } else {
                  if ($lobby_bet[0]['is_spectator'] == 1 && $lobby_bet[0]['is_event'] == 1 && $get_ticket_detail->ticket_type == '2') {
                    echo '<a class="cmn_btn upgrade_spectator_to_event">Upgrade Ticket</a>';
                  } 
                } ?>
                <?php
                $is_event_description = (($lobby_creator_det->is_event == 1 || $lobby_creator_det->is_spectator == 1) && !empty($lobby_creator_det->event_description) && $lobby_creator_det->event_description != null) ? 1 : 0;
                $rules = ($lbyctr == 1) ? 'Edit Rules' : 'Rules';
                echo (($is_event_description == 1 || (!empty($lobby_creator_det->description) && $lobby_creator_det->description != null)) || $lbyctr == 1) ? '<a target="_blank" href="'.base_url().'livelobby/rulesAdd/'.$lobby_bet_detail[0]['lobby_id'].'" class="lobby_btn" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="'.$rules.' of lobby">'.$rules.'</a>' : '';
                ?>
                <?php if ($lbyctr == 1) { ?>
                  <a class="lobby_btn giveaway_settings" ga_title="<?php echo $giveaway_data[0]['title']; ?>" gw_id="<?php echo $giveaway_data[0]['gw_id']; ?>" gw_img="<?php echo $giveaway_data[0]['gw_img']; ?>" gw_amount="<?php echo $giveaway_data[0]['gw_amount']; ?>" gw_entrieslimit="<?php echo $giveaway_data[0]['gw_entrieslimit']; ?>" gw_datetime="<?php echo (!empty($giveaway_data[0]['gw_date_time_of_giveaway'])) ? date('m/d/Y H:i', strtotime($giveaway_data[0]['gw_date_time_of_giveaway'])) : date('m/d/Y H:i'); ?>" gw_description= "<?php echo $giveaway_data[0]['gw_description']; ?>" gw_multiple_entries="<?php echo $giveaway_data[0]['gw_is_multiple_entries']; ?>" ga_date_time_registration_close="<?php echo (!empty($giveaway_data[0]['date_time_registration_close'])) ? date('m/d/Y H:i', strtotime($giveaway_data[0]['date_time_registration_close'])) : date('m/d/Y H:i'); ?>" fireworks_audio="<?php echo $giveaway_data[0]['fireworks_audio']; ?>" fireworks_duration_seconds="<?php echo (!empty($giveaway_data[0]['fireworks_duration_time']) && $giveaway_data[0]['fireworks_duration_time'] != 0) ? $giveaway_data[0]['fireworks_duration_time'] : '20'; ?>" use_custom_fireworks_audio="<?php echo $giveaway_data[0]['use_custom_fireworks_audio']; ?>" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Giveaway setting">GiveAway <i class="fa fa-cog"></i></a>&nbsp;&nbsp;<a class="cmn_btn is_second_banner" is_second_banner='<?php echo $get_my_fan_tag->is_second_banner; ?>'> Promote Stream</a>
                <?php } ?>
              </div>
           </div>
           <?php if ($lobby_creator == $this->session->userdata('user_id') && $lobby_bet[0]['is_event'] == 1) { 
            $chk_reg_ev = ($lobby_creator_det->registration_status == 1) ? 'checked' : '';
            ?>
              <div class="col-sm-12" style="margin-top: 30px;">
                <div class="row" style="">
                  <div class="col-sm-1 col-sm-offset-3"></div>
                  <div class="col-sm-4">
                    <div class="row default_lobby_bdr eve_regi_div">
                      <div class="text-right" style="margin-right: 16px;">
                        <h4 style="margin: 0 auto;">Event Registration</h4>
                      </div> 
                      <div class="set_banner_home_stream text-left">
                        <label class="switch" style="margin: 0 auto;"><input type="checkbox" name="registration_status" id="registration_status_tgl" <?php echo $chk_reg_ev;?> ><span class="slider round"></span> </label>
                      </div>
                      <form id = "status_change_form" action="<?php echo base_url().'livelobby/change_registration_status' ;?>" method = "post">
                        <input type="hidden" name="lobby_id" value="<?php echo $lobby_bet[0]['lobby_id'];?>">
                        <input type="hidden" name="status" id="reg_status">
                      </form>
                    </div>
                  </div>
                </div>
              </div>
           <?php } ?>
           <?php
           $get_total_deducted_event_price = array_sum(array_column($event_info,'deducted_event_price'));
            // if ($lbyctr == 1 && $lobby_bet[0]['is_event_start'] == 0) {
            $total_deducted_event_price = (!empty($get_total_deducted_event_price) && $get_total_deducted_event_price != NULL) ? number_format((float) $get_total_deducted_event_price, 2, '.', '') : '0.00';
            if ($lobby_bet[0]['is_event_end'] == 0) { ?>
              <div class="col-sm-12 text-center total_tournament_price_coll">
                <div class="default_lobby_bdr" style="display: inline-block; padding: 20px; margin-top: 20px;">
                  <div class="clearfix">
                    <h4 style="margin: 0px auto 20px;">Total Tournament Prize Collected</h4>
                    <h2 style="margin: 0px auto 20px;"><?php echo ($lobby_bet[0]['is_override_prize'] == 1) ? '$ '.$lobby_bet[0]['override_prize_amt'] : '$ '.$total_deducted_event_price; ?></h2>
                  </div>
                  <div class="clearfix">
                    <?php if ($lbyctr == 1 && $lobby_bet[0]['is_event_start'] == 0 && $lobby_bet[0]['is_event'] == 1) {
                      // $total_deducted_event_price = count($event_info) * ($lobby_bet[0]['event_price'] - $lobby_bet[0]['event_fee']);
                      // $total_deducted_event_price = number_format((float) $total_deducted_event_price, 2, '.', '')
                      ?>
                      <a class="cmn_btn" id="start_trnmnt_btn"> START Tournament </a>
                      <a class="cmn_btn" id="end_trnmnt_btn" style="display: none;"> END Tournament </a>
                    <?php } else if ($lbyctr == 1 && ($lobby_bet[0]['is_event_start'] == 1 && $lobby_bet[0]['is_event_end'] == 0) && $lobby_bet[0]['is_event'] == 1) { ?>
                      <!-- <a class="cmn_btn" id="start_trnmnt_btn" style="display: none;"> START Tournament </a> -->
                      <a class="cmn_btn" id="end_trnmnt_btn"> END Tournament </a>
                    <?php } ?>
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>
        <div class="row streem-flex-box">
          <div class="col-sm-2 border-red pad-bot-20 streamer_list_box streamer_left_box cust-steamer-box">
            <div class="text-left col-sm-12 padd0 team_stream">
              <h4 class="title">Team One Stream</h4>
              <?php 
              $show_more_btn = 0;
              if (isset($wtnglist['leftstreamdetail']) && $wtnglist['leftstreamdetail'] !='') {
                foreach ($wtnglist['leftstreamdetail'] as $key => $left_stream) {
                  $show_more_btn = ($key >= 5) ? 1 : 0;
                  $divCls = ($left_stream->is_creator=='yes')?'center':'';
                  $div = ($left_stream->is_creator=='yes')?'centerdiv':'leftdiv';
                  if ($left_stream->stream_status == "enable" && (($left_stream->stream_type == 1 && !empty($left_stream->stream_channel)) || ($left_stream->stream_type == 2 && !empty($left_stream->obs_stream_channel)))) {
                    $disable_check = '';
                    if ($left_stream->user_id == $left_top_channel) {
                      $disable_check = 'checked ';
                      if ($left_stream->user_id != $_SESSION['user_id']) {
                        $disable_check = 'checked class="disable_stream_checkbox" disabled readonly';
                      }
                    } else if (in_array($left_stream->user_id, $streamer_chat_id_set)) {
                        $disable_check = 'checked ';
                    } ?>
                    <div class="streamer_item live_streamer_<?php echo $left_stream->user_id;?>" id="live_streamer_<?php echo $left_stream->user_id;?>" <?php echo $show_more_cls; ?>>
                      <label for="<?php echo $div.$left_stream->user_id;?>">
                        <input data-class='<?php echo $divCls; ?>' type="checkbox" id="<?php echo $div.$left_stream->user_id;?>" stream-id="stream_channel_<?php echo $left_stream->user_id;?>" stream_type="<?php echo $left_stream->stream_type; ?>" stream-channel_name="<?php echo ($left_stream->stream_type == 2) ? $left_stream->obs_stream_channel : $left_stream->stream_channel;?>" stream-streamer_name="<?php echo $left_stream->fan_tag;?>" <?php echo $disable_check; ?>>
                        <?php echo $left_stream->fan_tag; ?>
                      </label>
                    </div>
                    <?php
                  }
                }
              } ?>
            </div>
            <div class="col-sm-12 padd0">
              <div class="show_more_navigation right" <?php echo ($show_more_btn == 1) ? 'style="display:block;"': 'style="display:none;"'; ?>>
                <a class="cmn_btn left_prev"><<</a>
                <a class="cmn_btn left_next">>></a>
              </div>
            </div>
          </div>

          <div class="col-sm-7 text-center lobby_det">
            <?php
            // $array_left_var = array_column($wtnglist['leftstreamdetail'], 'user_id');
            // $array_right_var = array_column($wtnglist['rightstreamdetail'], 'user_id');
            // $result = null;
            // if (in_array($lobby_creator, $array_left_var)) {
            //   $resultKey = array_search($lobby_creator, $array_left_var);
            //   $result = $wtnglist['leftstreamdetail'][$resultKey];
            //   $flag = 'left'; 
            //   $center_top_ch = $left_top_ch;
            //   $center_top_channel = $left_top_channel;
            // } else {
            //   $resultKey = array_search($lobby_creator, $array_right_var);
            //   $result = $wtnglist['rightstreamdetail'][$resultKey];
            //   $flag = 'right';
            //   $center_top_ch = $right_top_ch;
            //   $center_top_channel = $right_top_channel;
            // }

            if ($result->stream_status == "enable" && (($result->stream_type == 1 && !empty($result->stream_channel)) || ($result->stream_type == 2 && !empty($result->obs_stream_channel)))) {
              $disable_check = '';
              $display = 'none';
              if (in_array($result->user_id,$center_top_ch)) {
                $disable_check = 'checked ';
                if ($result->user_id != $_SESSION['user_id']) {
                  $disable_check = 'checked class="disable_stream_checkbox" disabled readonly';
                }
              } else if (in_array($result->user_id, $streamer_chat_id_set)) {
                $disable_check = 'checked ';
                $display = 'unset';
              }
            } ?>
            <div class="streem-iframe streamer_channel text-center">
              <?php
              $creator_stream_cls = 'show';
              if (!empty($all_streamdetail[$creator_stream_key]) && $all_streamdetail[$creator_stream_key]->stream_status == "enable") {
                $result = $all_streamdetail[$creator_stream_key];
                $streamername = $result->fan_tag;
                $fan_tag = $this->General_model->view_single_row('lobby_fans', array(
                   'lobby_id' => $lobby_bet[0]['lobby_id'],
                   'user_id' => $result->user_id
                 ),'fan_tag');
                $sndclass = "send_tip_btn";
                $subsclass = "subscribe_button commonmodalclick"; 
                if ($result->user_id == $_SESSION['user_id']) {
                  $subsclass = 'not_allow';
                  $sndclass = "tip_disable";
                }
                if ($result->stream_status == "enable" && (($result->stream_type == 1 && !empty($result->stream_channel)) || ($result->stream_type == 2 && !empty($result->obs_stream_channel)))) {
                  $creator_stream_cls = 'hide';
                  $subscribedlist_arr = array_column($wtnglist['subscribedlist'],'user_to');
                  $subscribed_plan = (in_array($result->user_id, $subscribedlist_arr)) ? 'active' : '';
                  ?>
                  <div id="centerdiv<?php echo $result->user_id; ?>stream" class="<?php echo $result->user_id; ?>_live-stream stream_iframe" style="display:<?=$display?>">
                    <?php 
                    if ($result->stream_type == 1) {
                      $stream_url = 'https://player.twitch.tv/?channel='.$result->stream_channel.'&parent='.$_SERVER['HTTP_HOST'];?>
                      <iframe src="<?php echo $stream_url; ?>" height="300" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
                    <?php } else if ($result->stream_type == 2) {
                      $stream_url = base_url().'live_stream?channel='.$result->obs_stream_channel.'&user_id='.$result->user_id.'&lobby_id='.$result->lobby_id.'&group_bet_id='.$result->group_bet_id;
                      $stream_key = $result->obs_stream_channel;
                        $new_stream_key = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(8/strlen($x)) )),1,8);
                        // $base_url = (base_url() != 'https://proesportsgaming.com/') ? 'https://esportstesting.com/' : base_url();
                        $base_url = base_url();
                        $random_id = $new_stream_key;
                        $user_id = $result->user_id;
                        if(!empty($result->lobby_id)){$lobby_id = $result->lobby_id;}
                        if(!empty($result->group_bet_id)){$group_bet_id = $result->group_bet_id;}
                      ?>
                      <div style="height: 300px;width: 100%;box-shadow: 0px 0px 3px 0px #ff5000;" id="<?php echo $random_id; ?>">
                        <div class="stream_error" class="alert alert-danger" style="display: none;"></div>
                        <video id="videojs-flvjs-player" autoplay  class="obs_streaming video-js vjs-default-skin" controls style="width: 100%; height: 100%;" poster="<?php echo base_url().'assets/frontend/images/stream_offline_img.jpeg'; ?>" preload="auto" data-key="<?php echo $stream_key; ?>" data-random_id="<?php echo $random_id; ?>" data-base_url="<?php echo $base_url; ?>" data-user_id="<?php echo $user_id; ?>" data-lobby_id="<?php echo $lobby_id; ?>" data-group_bet_id="<?php echo $group_bet_id; ?>"></video>
                      </div>
                    <?php } ?>
                    <div class="row streamer_channel_detail">
                      <div class="col-sm-4 text-left">
                        <a data-id="<?php echo $result->user_id; ?>" class="lobby_btn social_follow <?php echo ($result->user_id == $this->session->userdata('user_id')) ? 'disabled': ''?>" data-html="true" data-toggle = "tooltip" data-placement="bottom" data-original-title = "<?php echo ucfirst($result->is_followed); ?>"><?php echo ($result->is_followed == 'follow') ? '<img src = "'.base_url().'assets/frontend/images/follow.png">' : '<img src = "'.base_url().'assets/frontend/images/follower.png">'; ?></a><?php echo '<sup><span class="cunt badge b_color" style="margin-left: 5px;">'.$result->followers_count.'</span></sup>';?>
                        <a class="lobby_btn" href="<?php echo base_url().'Store/'.$result->user_id; ?>" target="_blank" style="margin-left: -1.3em" data-html="true" data-toggle = "tooltip" data-placement="bottom" data-original-title = "Store"><img src = "<?php echo base_url().'assets/frontend/images/store.png'; ?>"></a>

                        <a href="<?php echo base_url().'user/user_profile?fans_id='.$result->user_id; ?>" target="_blank" style="margin-left: 0.3em" data-html="true" data-toggle = "tooltip" data-placement="bottom" data-original-title = "Profile"><img src = "<?php echo base_url().'upload/profile_img/'.$result->image;?>" class="profile_img"></a>
                      </div>
                      <div class="col-sm-4 text-center">
                         <h3 align="center" class="live_lobby_h3"><?php echo $result->fan_tag; ?></h3>
                      </div>
                      <div class="col-sm-4 text-right">
                        <?php if($result->stream_type == 2){?><a class="lobby_btn" style="margin-right: 0.3em;display: none;" href="<?php echo base_url().'Streamsetting/clip?streamer='.$result->user_id.'&lobby_id='.$result->lobby_id; ?>"  data-toggle = "tooltip" data-placement="bottom" data-original-title = "Clip" target="_blank">
                          <img src = "<?php echo base_url();?>assets/frontend/images/clip_ico.png">
                        </a><?php }?>
                        <a class="lobby_btn <?php echo $subsclass; ?>" streamer_name='<?php echo $streamername; ?>' streamer_id="<?php echo $result->user_id; ?>" streamer_img="<?php echo ($result->image != '') ? $result->image : $get_default_img ; ?>"plan_subscribed="<?php echo $subscribed_plan; ?>" data-html="true" data-toggle = "tooltip" data-placement="bottom" data-original-title = "Subscribe">
                          <?php echo ($subscribed_plan != '') ? '<img src = "'.base_url().'assets/frontend/images/subscription11.png">' : '<img src = "'.base_url().'assets/frontend/images/subscription1p.png">'; ?>
                        </a>
                        <a class="lobby_btn <?php echo $sndclass; ?>" streamer_name='<?php echo $streamername; ?>' streamer_id="<?php echo $result->user_id; ?>" style="margin-left: 0.3em" data-html="true" data-toggle = "tooltip" data-placement="bottom" data-original-title = "Tip">
                          <img src = "<?php echo base_url().'assets/frontend/images/tip2.png';?>">
                        </a>
                      </div>
                    </div>
                  </div>
                <?php }
              } ?>
              <div class='default_stream_img clearfix <?php echo $creator_stream_cls; ?>' style="padding-bottom: 35px;">
                <img src="<?php echo base_url().'assets/frontend/images/default_stream_box_transperent.png'; ?>" alt="Smiley face" class="img img-responsive" style="margin: 0 auto;">
              </div>
            </div>
          </div>
          <div class="col-sm-2 border-red pad-bot-20 streamer_list_box streamer_right_box cust-steamer-box">
            <div class="text-left col-sm-12 padd0 team_stream">
              <h4 class="title">Team Two Stream</h4>
              <div class="left_articles">  
                <?php
                $show_more_btn = 0;
                if (isset($wtnglist['rightstreamdetail']) && $wtnglist['rightstreamdetail'] !='') {
                  foreach ($wtnglist['rightstreamdetail'] as $key => $right_stream) {

                    $show_more_btn = ($key >= 5) ? 1 : 0;
                    $divCls = ($right_stream->is_creator=='yes')?'center':'';
                    $div = ($right_stream->is_creator=='yes')?'centerdiv':'rightdiv';
                    if ($right_stream->stream_status == "enable") {
                      $disable_check = '';

                      if ($right_stream->user_id == $right_top_channel) {
                        $disable_check = 'checked ';
                        
                        if ($right_stream->user_id != $_SESSION['user_id']) {
                          $disable_check = 'checked class="disable_stream_checkbox" disabled readonly';
                        }
                      } else if (in_array($right_stream->user_id, $streamer_chat_id_set)) {
                        $disable_check = 'checked ';
                      } ?>

                      <div class="streamer_item text-left live_streamer_<?php echo $right_stream->user_id;?>" id="live_streamer_<?php echo $right_stream->user_id;?>">
                        <label for="<?php echo $div.$right_stream->user_id;?>" >
                          <input data-class='<?php echo $divCls; ?>' type="checkbox" id="<?php echo $div.$right_stream->user_id;?>"  stream-id="stream_channel_<?php echo $right_stream->user_id;?>" stream_type="<?php echo $right_stream->stream_type; ?>" stream-channel_name="<?php echo ($right_stream->stream_type == 2) ? $right_stream->obs_stream_channel : $right_stream->stream_channel;?>" stream-streamer_name="<?php echo $right_stream->fan_tag;?>" <?php echo $disable_check; ?>>
                          <?php echo $right_stream->fan_tag;?>
                        </label>
                      </div>
                      <?php
                    }
                  }
                } ?>
              </div>
              <div class="col-sm-12 padd0">
                <div class="show_more_navigation left" <?php echo ($show_more_btn == 1) ? 'style="display:block;"': 'style="display:none;"'; ?>>
                  <a class="cmn_btn right_prev"><<</a>
                  <a class="cmn_btn right_next">>></a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row streem_btns">
          <div class="streem-btns-inner">
            <!-- <img src="<?php //echo base_url().'/upload/game/'.$lobby_bet[0]['lobby_image']; ?>" alt="Smiley face" height="90" width="100%"> -->
            <h3 align="center" class="lby_size"><?php echo $lobby_bet[0]['game_type'].' vs '.$lobby_bet[0]['game_type']; ?></h3>
            <div class="send_tip_team btn">
              <a class="lobby_btn send_tips" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Donate to Esports Team"> Team Donate</a>
            </div>
            <div class="add_friends btn">
              <a href="<?php echo base_url().'livelobby/view_fanslist/'.$lobby_bet[0]['lobby_id']; ?>" target='_blank' class="lobby_btn fans_btn" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Click to view Profiles" style="text-transform: initial;"> Add Friends <?php echo (isset($fanscount)) ? count($fanscount) : '0'; ?></a>
            </div>
            <div class="add_friends btn">
              <a class="lobby_btn fans_btn discord_btn" >Esports Discord</a>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6 text-left streamer_channel players_streamers">
            <?php
            if (isset($wtnglist['leftstreamdetail']) && $wtnglist['leftstreamdetail'] !='') {
              foreach ($wtnglist['leftstreamdetail'] as $key => $left_stream) {
                if($left_stream->is_creator !='yes'){
                
                  if (in_array($left_stream->user_id,$left_top_ch)) {  
                    // $streamername = $left_stream->display_name != '' ? $left_stream->display_name : $left_stream->name;
                    $streamername = $left_stream->fan_tag;
                    $fan_tag = $this->General_model->view_single_row('lobby_fans', array(
                       'lobby_id' => $lobby_bet[0]['lobby_id'],
                       'user_id' => $left_stream->user_id
                     ),'fan_tag');
                    $sndclass = "send_tip_btn";
                    $subsclass = "subscribe_button commonmodalclick"; 
                    if ($left_stream->user_id == $_SESSION['user_id']) {
                      $subsclass = 'not_allow';
                      $sndclass = "tip_disable";
                    }
                    if ($left_stream->stream_status == "enable" && (($left_stream->stream_type == 1 && !empty($left_stream->stream_channel)) || ($left_stream->stream_type == 2 && !empty($left_stream->obs_stream_channel)))) {
                      $subscribedlist_arr = array_column($wtnglist['subscribedlist'],'user_to');
                      $subscribed_plan = (in_array($left_stream->user_id, $subscribedlist_arr)) ? 'active' : '';
                      // if (in_array($left_stream->user_id, $subscribedlist_arr)) {
                      //   $subskey = array_search($left_stream->user_id, $subscribedlist_arr);
                      //   $subscribed_plan = $wtnglist['subscribedlist'][$subskey]->plan_detail;
                      //   $payment_id = $wtnglist['subscribedlist'][$subskey]->payment_id;
                      // }
                      ?>
                      <div id="leftdiv<?php echo $left_stream->user_id; ?>stream" class="<?php echo $left_stream->user_id; ?>_live-stream stream_iframe">
                        <?php 
                        if ($left_stream->stream_type == 1) {
                          $stream_url = 'https://player.twitch.tv/?channel='.$left_stream->stream_channel.'&parent='.$_SERVER['HTTP_HOST'];?>
                          <iframe src="<?php echo $stream_url; ?>" height="300" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
                        <?php } else if ($left_stream->stream_type == 2) {
                          $stream_url = base_url().'live_stream?channel='.$left_stream->obs_stream_channel.'&user_id='.$left_stream->user_id.'&lobby_id='.$left_stream->lobby_id.'&group_bet_id='.$left_stream->group_bet_id;
                          $stream_key = $left_stream->obs_stream_channel;
                          $new_stream_key = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(8/strlen($x)) )),1,8);
                          // $base_url = (base_url() != 'https://proesportsgaming.com/') ? 'https://esportstesting.com/' : base_url();
                          $base_url = base_url();
                          $random_id = $new_stream_key;
                          $user_id = $left_stream->user_id;
                          if(!empty($left_stream->lobby_id)){$lobby_id = $left_stream->lobby_id;}
                          if(!empty($left_stream->group_bet_id)){$group_bet_id = $left_stream->group_bet_id;}
                        ?>
                          <div style="height: 300px;width: 100%;box-shadow: 0px 0px 3px 0px #ff5000;" id="<?php echo $random_id; ?>">
                            <div class="stream_error" class="alert alert-danger" style="display: none;"></div>
                            <video id="videojs-flvjs-player" autoplay  class="obs_streaming video-js vjs-default-skin" controls style="width: 100%; height: 100%;" poster="<?php echo base_url().'assets/frontend/images/stream_offline_img.jpeg'; ?>" preload="auto" data-key="<?php echo $stream_key; ?>" data-random_id="<?php echo $random_id; ?>" data-base_url="<?php echo $base_url; ?>" data-user_id="<?php echo $user_id; ?>" data-lobby_id="<?php echo $lobby_id; ?>" data-group_bet_id="<?php echo $group_bet_id; ?>"></video>
                          </div>
                        <?php } ?>
                          <div class="row streamer_channel_detail">
                            <div class="col-sm-4 text-left">
                              <a data-id="<?php echo $left_stream->user_id; ?>" class="lobby_btn social_follow <?php echo ($left_stream->user_id == $this->session->userdata('user_id')) ? 'disabled': ''?>" data-html="true" data-toggle = "tooltip" data-placement="bottom" data-original-title = "<?php echo ucfirst($left_stream->is_followed); ?>"><?php echo ($left_stream->is_followed == 'follow') ? '<img src = "'.base_url().'assets/frontend/images/follow.png">' : '<img src = "'.base_url().'assets/frontend/images/follower.png">'; ?></a><?php echo '<sup><span class="cunt badge b_color" style="margin-left: 5px;">'.$left_stream->followers_count.'</span></sup>';?>
                              <a class="lobby_btn" href="<?php echo base_url().'Store/'.$left_stream->user_id; ?>" target="_blank" style="margin-left: -1.3em" data-html="true" data-toggle = "tooltip" data-placement="bottom" data-original-title = "Store"><img src = "<?php echo base_url().'assets/frontend/images/store.png'; ?>"></a>

                              <a href="<?php echo base_url().'user/user_profile?fans_id='.$left_stream->user_id; ?>" target="_blank" style="margin-left: 0.3em" data-html="true" data-toggle = "tooltip" data-placement="bottom" data-original-title = "Profile"><img src = "<?php echo base_url().'upload/profile_img/'.$left_stream->image;?>" class="profile_img"></a>
                            </div>
                            <div class="col-sm-4 text-center">
                               <h3 align="center" class="live_lobby_h3"><?php echo $fan_tag['fan_tag']; ?></h3>
                            </div>
                            <div class="col-sm-4 text-right">
                              <?php if($left_stream->stream_type == 2){?><a class="lobby_btn" style="margin-right: 0.3em;display: none;" href="<?php echo base_url().'Streamsetting/clip?streamer='.$left_stream->user_id.'&lobby_id='.$left_stream->lobby_id; ?>"  data-toggle = "tooltip" data-placement="bottom" data-original-title = "Clip" target="_blank">
                                <img src = "<?php echo base_url();?>assets/frontend/images/clip_ico.png">
                              </a><?php }?>
                              <a class="lobby_btn <?php echo $subsclass; ?>" streamer_name='<?php echo $streamername; ?>' streamer_id="<?php echo $left_stream->user_id; ?>" streamer_img="<?php echo ($left_stream->image != '') ? $left_stream->image : $get_default_img ; ?>" plan_subscribed="<?php echo $subscribed_plan; ?>" data-html="true" data-toggle = "tooltip" data-placement="bottom" data-original-title = "Subscribe">
                                <?php echo ($subscribed_plan != '') ? '<img src = "'.base_url().'assets/frontend/images/subscription11.png">' : '<img src = "'.base_url().'assets/frontend/images/subscription1p.png">'; ?>
                              </a>
                              <a class="lobby_btn <?php echo $sndclass; ?>" streamer_name='<?php echo $streamername; ?>' streamer_id="<?php echo $left_stream->user_id; ?>" style="margin-left: 0.3em" data-html="true" data-toggle = "tooltip" data-placement="bottom" data-original-title = "Tip">
                                <img src = "<?php echo base_url().'assets/frontend/images/tip2.png'; ?>">
                              </a>
                            </div>
                         </div>
                      </div>
                      <?php
                    }
                  }
                }
              }
            }
            ?>
          </div>
          <div class="col-sm-6 text-right streamer_channel players_streamers">
            <?php 
            if (isset($wtnglist['rightstreamdetail']) && $wtnglist['rightstreamdetail'] !='') {
              foreach ($wtnglist['rightstreamdetail'] as $key => $right_stream) {
                if($right_stream->is_creator !='yes'){
                  if (in_array($right_stream->user_id,$right_top_ch)) { 
                    // $streamername = $right_stream->display_name != '' ? $right_stream->display_name : $right_stream->name;
                    $streamername = $right_stream->fan_tag;
                    $fan_tag = $this->General_model->view_single_row('lobby_fans', array(
                       'lobby_id' => $lobby_bet[0]['lobby_id'],
                       'user_id' => $right_stream->user_id
                     ),'fan_tag');
                    $sndclass = "send_tip_btn";
                    $subsclass = "subscribe_button commonmodalclick"; 
                    if ($right_stream->user_id == $_SESSION['user_id']) {
                      $subsclass = 'not_allow';
                      $sndclass = "tip_disable";
                    }
                    if ($right_stream->stream_status == "enable" && (($right_stream->stream_type == 1 && !empty($right_stream->stream_channel)) || ($right_stream->stream_type == 2 && !empty($right_stream->obs_stream_channel)))) {
                      $subscribedlist_arr = array_column($wtnglist['subscribedlist'],'user_to');
                      $subscribed_plan = (in_array($right_stream->user_id, $subscribedlist_arr)) ? 'active' : '';
                      // if (in_array($right_stream->user_id, $subscribedlist_arr)) {
                      //   // $subskey = array_search($right_stream->user_id, $subscribedlist_arr);
                      //   // $subscribed_plan = $wtnglist['subscribedlist'][$subskey]->plan_detail;
                      //   // $payment_id = $wtnglist['subscribedlist'][$subskey]->payment_id;
                      // }
                      ?>
                      <div id="rightdiv<?php echo $right_stream->user_id; ?>stream" class="<?php echo $right_stream->user_id; ?>_live-stream stream_iframe">
                        <!-- <iframe src="https://player.twitch.tv/?channel=<?php // echo $right_stream->stream_channel;?>&parent=<?php //echo $_SERVER['HTTP_HOST']; ?>" height="300" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe> -->
                        <?php 
                        if ($right_stream->stream_type == 1) {
                          $stream_url = 'https://player.twitch.tv/?channel='.$right_stream->stream_channel.'&parent='.$_SERVER['HTTP_HOST'];?>
                          <iframe src="<?php echo $stream_url; ?>" height="300" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
                        <?php } else if ($right_stream->stream_type == 2) {
                          $stream_url = base_url().'live_stream?channel='.$right_stream->obs_stream_channel.'&user_id='.$right_stream->user_id.'&lobby_id='.$right_stream->lobby_id.'&group_bet_id='.$right_stream->group_bet_id;
                          $stream_key = $right_stream->obs_stream_channel;
                          $new_stream_key = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(8/strlen($x)) )),1,8);
                          // $base_url = (base_url() != 'https://proesportsgaming.com/') ? 'https://esportstesting.com/' : base_url();
                          $base_url = base_url();
                          $random_id = $new_stream_key;
                          $user_id = $right_stream->user_id;
                          if(!empty($right_stream->lobby_id)){$lobby_id = $right_stream->lobby_id;}
                          if(!empty($right_stream->group_bet_id)){$group_bet_id = $right_stream->group_bet_id;}
                        ?>
                          <div style="height: 300px;width: 100%;box-shadow: 0px 0px 3px 0px #ff5000;" id="<?php echo $random_id; ?>">
                            <div class="stream_error" class="alert alert-danger" style="display: none;"></div>
                            <video id="videojs-flvjs-player" autoplay  class="obs_streaming video-js vjs-default-skin" controls style="width: 100%; height: 100%;" poster="<?php echo base_url().'assets/frontend/images/stream_offline_img.jpeg'; ?>" preload="auto" data-key="<?php echo $stream_key; ?>" data-random_id="<?php echo $random_id; ?>" data-base_url="<?php echo $base_url; ?>" data-user_id="<?php echo $user_id; ?>" data-lobby_id="<?php echo $lobby_id; ?>" data-group_bet_id="<?php echo $group_bet_id; ?>"></video>
                          </div>
                        <?php } ?>
                        <div class="row streamer_channel_detail">
                          <div class="col-sm-4 text-left">
                            <a data-id="<?php echo $right_stream->user_id; ?>" class="lobby_btn social_follow <?php echo ($right_stream->user_id == $this->session->userdata('user_id')) ? 'disabled': ''?>" data-html="true" data-toggle = "tooltip" data-placement="bottom" data-original-title = "<?php echo ucfirst($right_stream->is_followed);?>"><?php echo ($right_stream->is_followed == 'follow') ? '<img src = "'.base_url().'assets/frontend/images/follow.png">' : '<img src = "'.base_url().'assets/frontend/images/follower.png">'; ?></a><?php echo '<sup><span class="cunt badge b_color" style="margin-left: 5px;">'.$right_stream->followers_count.'</span></sup>';?>
                            <a class="lobby_btn" href="<?php echo base_url().'Store/'.$right_stream->user_id; ?>" target="_blank" style="margin-left: -1.3em" data-html="true" data-toggle = "tooltip" data-placement="bottom" data-original-title = "Store"><img src = "<?php echo base_url().'assets/frontend/images/store.png';?>"></a>

                            <a href="<?php echo base_url().'user/user_profile?fans_id='.$right_stream->user_id; ?>" target="_blank" style="margin-left: 0.3em" data-html="true" data-toggle = "tooltip" data-placement="bottom" data-original-title = "Profile"><img src = "<?php echo base_url().'upload/profile_img/'.$right_stream->image;?>" class="profile_img"></a>
                          </div>
                          <div class="col-sm-4 text-center">
                             <h3 align="center" class="live_lobby_h3"><?php echo $fan_tag['fan_tag']; ?></h3>
                          </div>
                          <div class="col-sm-4 text-right">
                            <?php if($right_stream->stream_type == 2){?><a class="lobby_btn" style="margin-right: 0.3em;display: none;" href="<?php echo base_url().'Streamsetting/clip?streamer='.$right_stream->user_id.'&lobby_id='.$right_stream->lobby_id; ?>"  data-toggle = "tooltip" data-placement="bottom" data-original-title = "Clip" target="_blank">
                              <img src = "<?php echo base_url();?>assets/frontend/images/clip_ico.png">
                            </a><?php }?>
                            <a class="lobby_btn <?php echo $subsclass; ?>" streamer_name='<?php echo $streamername; ?>' streamer_id="<?php echo $right_stream->user_id; ?>" streamer_img="<?php echo ($right_stream->image != '') ? $right_stream->image : $get_default_img ; ?>"plan_subscribed="<?php echo $subscribed_plan; ?>" data-html="true" data-toggle = "tooltip" data-placement="bottom" data-original-title = "Subscribe">
                              <?php echo ($subscribed_plan != '') ? '<img src = "'.base_url().'assets/frontend/images/subscription11.png">' : '<img src = "'.base_url().'assets/frontend/images/subscription1p.png">'; ?>
                            </a>
                            <a class="lobby_btn <?php echo $sndclass; ?>" streamer_name='<?php echo $streamername; ?>' streamer_id="<?php echo $right_stream->user_id; ?>" style="margin-left: 0.3em" data-html="true" data-toggle = "tooltip" data-placement="bottom" data-original-title = "Tip">
                              <img src = "<?php echo base_url().'assets/frontend/images/tip2.png';?>">
                            </a>
                          </div>
                        </div>
                      </div>
                      <?php
                    }
                  }
                }
              }
            }
            ?>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <?php if ($lobby_bet[0]['is_event'] == 1) { ?>
        <div class="col-sm-6">
          <div class="live_custom_twitch_chat">
            <div class="row chatlble">
              <?php 
              $chat_user_id = ($_SESSION['streamer_chat_id_set'] != '') ? $_SESSION['streamer_chat_id_set'] : $defult_chat->user_id;
              ?>
              <h3 class="chat_open_with_person col-sm-6"><span>Open Twitch Chat with</span></h3>
              <div class="select_stream_chat text-center col-sm-6">
                <select class="form-control select_chat" name="select_head" table_cat="right">
                  <?php if (!empty($all_streamdetail)) { ?>
                    <option> Select chat option</option>
                    <?php foreach ($all_streamdetail as $key => $sd) {
                      if ($sd->stream_status != 'disable' && !empty($sd->stream_channel) && $sd->stream_type == 1) {
                        $ischk = ($chat_user_id == $sd->user_id) ? 'selected' : '';
                        echo '<option value="'.$sd->user_id.'" id="streamchat_'.$sd->user_id.'" stream-channel_name="'.$sd->stream_channel.'" stream-streamer_name="'.$sd->fan_tag.'" '.$ischk.'>'.$sd->fan_tag.'</option>';
                      }
                    } ?>
                  <?php } ?>
                </select>
              </div>
            </div>
            <?php
            // $_SESSION['streamer_chat_id_set']
            if (($defult_chat->stream_status == 'enable' && $defult_chat->stream_type == 1 && $defult_chat->stream_channel !='') || ($_SESSION['streamer_chat_id_set'] != '' && $_SESSION['stream_type'] == 1)) {
              $twitch_chat_channel = ($_SESSION['streamer_channel_set'] != '') ? $_SESSION['streamer_channel_set'] : $defult_chat->stream_channel;
              $chat_stream_name = ($_SESSION['streamer_name_set'] != '') ? $_SESSION['streamer_name_set'] : $defult_chat->fan_tag;
              // if ($defult_chat->stream_channel != '') {  ?>
            <div class="chatstreamer" t_streamerchat="<?php echo $chat_user_id; ?>">
              <div class="streamer_chat_iframe">
                <iframe src="https://www.twitch.tv/embed/<?php echo $twitch_chat_channel; ?>/chat?&parent=<?php echo $_SERVER['HTTP_HOST']; ?>" height="375" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
              </div>
            </div>
          <?php } else { ?>
            <div class='stream_chat_img' style="padding-bottom: 15px;">
              <img src="<?php echo base_url(); ?>assets/frontend/images/stream_chat_box_transperent.png" alt="Smiley face" class="img img-responsive">
            </div>
            <div class="chatstreamer"></div>
            <?php } ?>
          </div>
        </div>
      <?php } ?>
      <div class="text-center <?php echo ($lobby_bet[0]['is_event'] == 1) ? 'col-sm-6': 'col-sm-8 col-sm-offset-2'; ?>">
          <div class="row chat-box_fronside live_lobby_chatbox_main_row live_custom_chat">
             <div class="col-md-12 col-sm-12 col-xs-12 " align="center" id="live_lobby_msgbox">
                <div id="message-container" class="xwb-message-container">
                    <div class="popup-content-wrapper_customer_fontside lobby_group_msg_bx">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="row chatlble">
                            <h3 class="chat_open_with_person">
                              <div class="<?php echo ($lobby_bet[0]['is_event'] == 1) ? 'text-left' : 'text-center'; ?> col-sm-12">
                                <span style="font-weight: 500; font-size: 24px;">Chat in the Pro Esports lobby</span>
                              </div>
                            </h3>
                          </div>
                          <!-- <h3 align="center" class="pad-bot-20 live_lobby_h3">Chat in the Pro Esports lobby</h3> -->
                          <div id="message-inner" class="message-inner">
                            <?php if(isset($lobby_chat) && count($lobby_chat) > 0) {
                              foreach ($lobby_chat as $key => $value) {
                                $direction = 'left';
                                $download_attr = 'download';
                                $class = 'enable';
                                if ($value->user_id == $_SESSION['user_id']) {
                                  $direction = 'right';
                                  $download_attr = '';
                                  $class = 'disable';
                                }
                                $msg = $value->message;
                                if ($value->message_type == 'file') {
                                  $msg = '<a href="'.base_url().'upload/lobby_pics_msg/'.$value->attachment.'" class="download_attachment lobby_grpmsg_attach_link '.$class.'" '.$download_attr.'><i class="fa fa-download '.$class.'"></i><img class="attachment_image img img-thumbnail" src="'.base_url().'upload/lobby_pics_msg/'.$value->attachment.'" class="lobby_grpmsg_attach" alt="pic" width="100px" /></a>';
                                } else if ($value->message_type == 'tipmessage') {
                                  $tipicn = '<img src="'.base_url().'upload/stream_setting/'.$value->attachment.'" alt="tipimg" class="tipiconimg" />'; 
                                  if ($value->user_id == $_SESSION['user_id']) {
                                    $msg = $tipicn.$value->message;
                                  } else {
                                    $msg = $value->message.$tipicn;
                                  }
                                }
                                ?>
                                <div data-mid="<?php echo $value->message_id;?>" data-cid="<?php echo $value->id;?>" class="message-row msg_usr_<?php echo $value->user_id; ?>">
                                  <div class="col-md-12 col-sm-12 col-xs-12 message-box <?php echo ($lbyctr || $current_is_admin == 1) ? 'del_opt' : ''; ?>" tgcontmenu='<?php echo $value->message_id;?>'>
                                    <div class="message_header">
                                      <?php
                                      if ($value->is_admin == 1) { ?>
                                        <img class="chat_image pull-<?php echo $direction;?>" width="20" height="20" src="<?php echo base_url(); ?>upload/admin_dashboard/Chevron_3_Twitch_72x72.png">
                                      <?php } ?>
                                      <small class="list-group-item-heading text-muted text-primary pull-<?php echo $direction;?>"><?php $fan_tag = $this->General_model->view_single_row('lobby_fans', array('lobby_id' => $lobby_bet[0]['lobby_id'],'user_id' => $value->user_id),'fan_tag');
                                          echo $fan_tag['fan_tag']; ?></small>
                                    </div>
                                    <div class="message_row_container in pull-<?php echo $direction;?>">
                                      <p class="list-group-item-text"></p>
                                      <p><?php echo $msg;?></p>
                                      <?php if ($lbyctr || $current_is_admin == 1) { ?>
                                        <div class="dropdown clearfix contmenu_<?php echo $value->message_id;?> contextMenu">
                                          <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu" style="display:block;">
                                            <li><a class="remove_msg" onclick="send_msg_delete(<?php echo $value->message_id ;?>,<?php echo $value->user_id ;?>);">Remove Msg</a></li>
                                            <?php if ($direction == 'left' ) { ?>
                                              <li><a class="remove_user" onclick="del_usr_from_chat(<?php echo $value->user_id ;?>);">Remove User</a></li>
                                              <li><a class="mute_user" onclick="mute_usr_from_chat(<?php echo $value->user_id ;?>);">Time Out User</a></li>
                                            <?php } ?>
                                          </ul>
                                        </div>
                                      <?php } ?>
                                      <p></p>
                                     </div>
                                  </div>
                                </div>
                              <?php }
                            } else { ?>
                              <div class="no_messages">
                                <h3 class="text-center no-more-messages">No Messages Available</h3>
                              </div>
                            <?php } ?>
                          </div>
                        </div>
                      </div>
                      <?php if($get_my_fan_tag->fan_status == 1) {
                        $disable_class = '';
                        $disable_attr = '';
                        if($get_my_fan_tag->mute_time != ''){
                            $mutediff = strtotime($get_my_fan_tag->mute_time) - time();
                            if(time() < strtotime($get_my_fan_tag->mute_time)){
                              $disable_class = 'disable';
                              $disable_attr = 'readonly';
                              echo '<div class="mute_div mutetimer_div">You can chat after <span class="mutetimer" data-seconds-left="'.$mutediff.'"></span> Seconds</div>';
                            } else {
                              echo '<div class="mute_div"></div>';
                            }
                        }
                      ?>
                      <div class="msg-input input-group col-sm-12 lobby_grp_msgbox lobbymsgbox_userid_<?php echo $_SESSION['user_id']; ?> <?php echo $disable_class; ?>">
                        <div class="ms_snd_opt">
                          <div id="lobby_grp_msgbox_message-input" class="form-control message-input message-input-frontend-side lobby_grp_msgbox_message-input attach_emojioneicon" rows="4" name="message-input" contenteditable="true"></div>
                          <div id="emojionearea_display_div"></div>
                        </div>
                        <div class="chat_box_button_area">
                          <div class="attachfile_div">
                            <div class="input-group-btn">
                                <button for="file-input">
                                  <i class="fa fa-paperclip" aria-hidden="true"></i>
                                </button>
                                <input type="file" class="file-input hide" name="attachfile" id="file-input" accept="image/png, image/PNG, image/jpeg, image/jpg, image/JPEG, image/JPG" data-rule-required="true" data-msg-accept="Your image formate should be in .jpg, .jpeg or .png">
                                <div class="filename-container hide"></div>
                            </div>
                          </div>
                          <div class="message_send_div">
                            <div class="input-group-btn">
                              <button class="btn btn-success" id="send-message-to-lobby_group"><i class="fa fa-send-o" aria-hidden="true"></i></button>
                            </div>
                          </div>
                            <div class="message_send_div volume_adjustment" id="volume_adjustment_id" >
                              <input type="hidden" name="session_volume" id="session_volume" value="<?php echo $this->session->userdata('val_set'); ?>">
                              <div class="input-group-btn">
                                <button class="btn btn-success" id="volume-to-lobby_group"> 
                                    <i class="fa fa-volume-up volume vol_up" aria-hidden="true"></i>
                                    <i class="fa fa-volume-off volume vol_mute" aria-hidden="true"></i>
                                  </button>
                              </div>
                            </div>
                          <div class="message_send_div slider_adjustment" style="margin-left: 5px;">
                            <!-- <div class="volume">
                                <h2><span class="volume__val"></span></h2>
                                <div class="input-wrapper">
                                  <input type="range" name="" id="vol">
                                </div>
                            </div> -->
                            <div class="volume" id="player">
                              <div id="volume"></div>
                            </div>
                          </div>
                        </div>
                        <div class="play_sound">
                          <?php if (isset($_SESSION['getmysub_sound']) && $_SESSION['getmysub_sound'] != '') { ?>
                            <audio class="getmysub_sound_class" controls autoplay ><source src=<?php echo base_url(); ?>upload/stream_setting/<?php echo $_SESSION['getmysub_sound']; ?>></audio>
                          <?php } unset($_SESSION['getmysub_sound']); ?>
                        </div>
                      </div>
                      <?php } ?>
                    </div>
                </div>
             </div>
          </div>
      </div>
    </div>
    <div class="row live_lby_secrow">
      <?php $fan_tagarr = (!empty($fanscount)) ? '"'.implode('","', array_column($fanscount, 'fan_tag')).'"' : ''; ?>
      <div class="col-sm-3 select_head text-left padd0">
        <?php if ($lbyctr) { ?>
          <select class="form-control select_head_left" name="select_head" table_cat="left">
            <option value="0">SELECT LEADER</option>
            <?php foreach ($lobby_game_bet_left_grp as $key => $lgblg) {
              $fan_tag = $this->General_model->get_my_fan_tag($lobby_bet[0]['lobby_id'],$lgblg['user_id']);
              $selected = '';
              if ($lgblg['is_controller'] == 'yes') {
                $selected = 'selected';
              }
              echo '<option value="'.$lgblg['user_id'].'" class="assign_table_ctrl" id="'.$lgblg['user_id'].'" '.$selected.'>'.$fan_tag->fan_tag.'</option>';
            } ?>
          </select>
        <?php } ?>
      </div>
      <div class="col-sm-6 text-center crtrdiv">
        <!-- <div class="llby_crtr"> -->
          <?php 
          if ($lobby_creator == $_SESSION['user_id'] ) { ?>
            <img src="<?php echo base_url(); ?>assets/frontend/images/creator.png" width="30">
            <div class="is_draggable_div"><span>Drop here to Pass Controller</span><div class="drp_lbycrtr"></div></div>
          <?php } else { ?>
            <div class="clearfix text-center" style="font-size: 20px;">
              <?php echo '$'.number_format((float)$lobby_bet[0]['price'], 2, '.', ''). ' + $'.$lobby_joinfee.' fee'; ?>
            </div>
          <?php } ?>
        <!-- </div> -->
      </div>
      <div class="col-sm-3 select_head text-right padd0">
        <?php if ($lbyctr) { ?>
          <select class="form-control select_head_right" name="select_head" table_cat="right">
            <option value="0">SELECT LEADER</option>
            <?php foreach ($lobby_game_bet_right_grp as $key => $lgbrg) {
              $fan_tag = $this->General_model->get_my_fan_tag($lobby_bet[0]['lobby_id'],$lgbrg['user_id']);
              $selected = '';
              if ($lgbrg['is_controller'] == 'yes') {
                $selected = 'selected';
              }
              echo '<option value="'.$lgbrg['user_id'].'" class="assign_table_ctrl" id="'.$lgbrg['user_id'].'" '.$selected.'>'.$fan_tag->fan_tag.'</option>';
            } ?>
          </select>
        <?php } ?>
       </div>
    </div>
    <div class="row live_lby_thirow dragblearea">
      <div class="row live_lby_in_thirow">
           <div class="col-sm-2 text-left table_head_title right_brdr bottom_brdr"> Esports Player </div>
           <div class="col-sm-3 text-center table_head_title bottom_brdr"> Team Name </div>
           <div class="col-sm-2 text-center table_head_title padd0 left_brdr right_brdr bottom_brdr">
              <div class="live_lby_ready_timer">
                <div>Ready</div>
                <?php
                if(isset($get_freport) && $get_freport->updated_at != '' && $get_freport->updated_at != "0000-00-00 00:00:00") {
                  date_default_timezone_set($get_freport->timezone);date_default_timezone_set($get_freport->timezone);
                  $diff = strtotime($get_freport->updated_at) - time() + rand(0,10);
                  if(time() < strtotime($get_freport->updated_at)) {
                    echo '<div class="reporttimer" data-seconds-left="'.$diff.'"></div>';
                  } else { ?>
                    <script type="text/javascript">
                      var lobby_id, group_bet_id;
                      group_bet_id = <?php echo $lobby_bet[0]['id']; ?>;
                      lobby_id = <?php echo $lobby_bet[0]['lobby_id']; ?>;
                      var postData = {
                        'group_bet_id': group_bet_id,
                        'lobby_id':lobby_id
                      };
                      $.ajax({
                        type: 'post',
                        url: '<?php echo base_url(); ?>livelobby/report_timer_expire',
                        data: postData,
                        success: function(res){
                          var res = $.parseJSON(res);
                          window.location.href = '<?php echo base_url(); ?>livelobby/watchLobby/'+res.lobby_id;
                        }
                      });
                    </script>
                    <?php
                  }
                }
                ?>
              </div>
           </div>
           <div class="col-sm-3 text-center table_head_title bottom_brdr"> Team Name </div>
           <div class="col-sm-2 text-right table_head_title left_brdr bottom_brdr"> Esports Player </div>
        <div>
          <div class="col-sm-6 left_table_first_div <?php echo ($is_lefttable_full == 'no') ? 'left_table_droppable ' : '' ; echo ($is_full_play != 'yes' && $lbyctr) ? 'is_draggable' : '' ; ?>" droppable_table="LEFT">
            <?php if (isset($lobby_game_bet_left_grp)) {
              foreach ($lobby_game_bet_left_grp as $key => $left_row) {
                $fan_tag = $this->General_model->view_single_row('lobby_fans', array(
                  'lobby_id' => $lobby_bet[0]['lobby_id'],
                  'user_id' => $left_row['user_id']
                ),'fan_tag'); ?>
                <div class="left_table text-left row margin0 group_user_row_<?php echo $left_row['user_id'] ?>">
                  <?php
                  $fantag = '<span is_fan="true">'.$fan_tag['fan_tag'].'</span>';
                  echo '<div class="padd0 col-lg-4">';
                  if ($lobby_creator == $_SESSION['user_id'] ) {
                    echo '<a class="leave_lobby" self="yes" data-toggle="tooltip" data-placement="bottom" player_tag_name="'.$fan_tag['fan_tag'].'" lby_crtr="'.$left_row['is_creator'].'" lby_cntrlr="'.$left_row['is_controller'].'" title="Leave Live Lobby" usr_id="'.$left_row['user_id'].'" '.$everyone_readup.'><i class="fa fa-times"></i></a> ';
                    if ($left_row['user_id'] != $_SESSION['user_id']) {
                      if (count($get_play_status_lobby_bet) != count($lobby_game_bet_left_grp)+count($lobby_game_bet_right_grp)) {
                        $fantag = '<span class="pasctrl" id="'.$left_row['user_id'].'" fan_tag="'.$fan_tag['fan_tag'].'" is_fan="true">'.$fan_tag['fan_tag'].'</span>';
                      }
                    }
                    echo $fantag;
                  } else if ($left_row['user_id'] == $_SESSION['user_id'] && $lobby_creator != $_SESSION['user_id']) {
                    echo '<a class="leave_lobby" self="no" data-toggle="tooltip" player_tag_name="'.$fan_tag['fan_tag'].'" data-placement="bottom" lby_crtr="'.$left_row['is_creator'].'" lby_cntrlr="'.$left_row['is_controller'].'" title="Leave Live Lobby" usr_id="'.$left_row['user_id'].'" '.$everyone_readup.'><i class="fa fa-times"></i></a> ';
                    $fantag = '<span is_fan="true">'.$fan_tag['fan_tag'].'</span>';
                    echo $fantag;
                  } else {
                    echo $fantag;
                  }
                  $left_status_class = "disabled";
                  echo (in_array($left_row['user_id'], $spectator_list) && $lby_is_spectator == 1)?'<span class="is_spectator_span"><i class="fa fa-eye"></i></span>':'';
                  if ($left_row['user_id'] == $_SESSION['user_id']) {
                    $left_status_class = "stream_icon enabled";
                    $left_tooltip = 'data-toggle="tooltip" data-html="true" data-placement="bottom" title="Click to Start. <br/> Live Lobby Stream"';
                    if ($left_row['stream_status'] == 'enable' && (($left_row['stream_type'] == 1 && !empty($left_row['stream_channel'])) || ($left_row['stream_type'] == 2 && !empty($left_row['obs_stream_channel'])))) {
                      $left_tooltip = 'data-toggle="tooltip" data-html="true" data-placement="bottom" title="Click to Stop. <br/> Live Lobby Stream"';
                    }
                  }
                  $left_stream_click_status = 'stream_click_status="enable" stream_type="'.$left_row['stream_type'].'"';
                  $left_img_src = base_url().'assets/frontend/images/live_stream_off.png';
                  if ($left_row['stream_status'] == 'enable' && (($left_row['stream_type'] == 1 && !empty($left_row['stream_channel'])) || ($left_row['stream_type'] == 2 && !empty($left_row['obs_stream_channel'])))) {
                    $left_img_src = base_url().'assets/frontend/images/live_stream_on.png';
                    $left_stream_click_status = 'stream_click_status="disable" stream_type="'.$left_row['stream_type'].'"';
                  }

                  echo ' <span class="tooltip_div"><img src="'.$left_img_src.'" class="'.$left_status_class.'" alt="live_stream" id="stream_'.$left_row['user_id'].'" '.$left_tooltip.' '.$left_stream_click_status.'></span>';
                  $left_team_leader = base_url().'assets/frontend/images/table_leader_new.png ';
                  echo ' <span class="team_leader" id="teamhead_'.$left_row['user_id'].'">';

                  if ($left_row['is_controller'] == 'yes') {
                    echo '<img src="'.$left_team_leader.'" alt="Team Controller">';
                  }
                  echo '</span> ';
                  echo '</div>';
                  echo '<div class="padd0 col-lg-6 text-center">';
                  echo (!empty($fan_tag['team_name'])) ? "<span class='team_name'>".$fan_tag['team_name']."</span>" : '';
                  echo '</div>';
                  echo '<div class="padd0 col-lg-2">';
                  if($left_row['reported'] == 0) {
                    if ($lobby_bet[0]['game_type']*2 == count($lobby_game_bet_left_grp)+count($lobby_game_bet_right_grp) || $is_full_play == 'yes') {
                      if ($left_row['user_id'] == $_SESSION['user_id']){
                        if ($check_bet_reported_for_me[0]['play_status'] == 1) {
                          echo '<label class="checkbox_container disabled"><input type="checkbox" checked disabled data-user="'.$left_row["user_id"].'" class="text-right"><span class="checkmark"></span></label>';
                          if (count($get_play_status_lobby_bet) == count($lobby_game_bet_left_grp)+count($lobby_game_bet_right_grp) || $is_full_play == 'yes') {
                            if ($left_row['is_controller'] == 'yes') {
                              echo '<button class="bet_report button text-right">Report</button>';
                            }
                          }
                        } else {
                          echo '<label class="checkbox_container"><input type="checkbox" data-user="'.$left_row["user_id"].'" name="play_game" class="text-right play_game"><span class="checkmark"></span></label>';
                        }
                      } else {
                        if ($left_row['play_status'] == 1) {
                          echo '<label class="checkbox_container disabled"><input type="checkbox" checked disabled data-user="'.$left_row["user_id"].'" class="text-right"><span class="checkmark"></span></label>';
                        } else {
                          echo '<label class="checkbox_container disabled"><input type="checkbox" disabled class="text-right"><span class="checkmark op_bg"></span></label>';
                        }
                      }
                    }
                  } else {
                    echo '<button class="button text-right bet_reported">Reported <i class="fa fa-check"></i></button><label class="checkbox_container disabled"><input type="checkbox" checked="" disabled="" class="text-right" data-user="'.$left_row["user_id"].'"><span class="checkmark"></span></label>'; 
                  }
                  echo '</div>';
                  ?>
                </div>
              <?php }
              } ?>
          </div>
          <div class="col-sm-6 border-right right_table_first_div <?php echo ($is_righttable_full == 'no') ? 'right_table_droppable ' : '' ;  echo ($is_full_play != 'yes' && $lbyctr) ? 'is_draggable' : '' ; ?>" droppable_table="RIGHT">
            <?php if (isset($lobby_game_bet_right_grp)) {
              foreach ($lobby_game_bet_right_grp as $key => $right_row) { 
                $fan_tag = $this->General_model->view_single_row('lobby_fans', array(
                  'lobby_id' => $lobby_bet[0]['lobby_id'],
                  'user_id' => $right_row['user_id']
                ),'*');
                ?>
                <div class="right_table text-right row margin0 group_user_row_<?php echo $right_row['user_id'] ?>">
                  <?php
                  echo '<div class="padd0 col-lg-2">';
                  if($right_row['reported'] == 0) {
                    if ($lobby_bet[0]['game_type']*2 == count($lobby_game_bet_left_grp)+count($lobby_game_bet_right_grp) || $is_full_play == 'yes') {
                      if ($right_row['user_id'] == $_SESSION['user_id']) {
                        if ($check_bet_reported_for_me[0]['play_status'] == 1) {
                          echo '<label class="checkbox_container disabled"><input type="checkbox" checked disabled class="text-left" data-user="'.$right_row["user_id"].'"><span class="checkmark"></span></label>';
                          if (count($get_play_status_lobby_bet) == count($lobby_game_bet_left_grp)+count($lobby_game_bet_right_grp) || $is_full_play == 'yes') {
                            if ($right_row['is_controller'] == 'yes') {
                              echo '<button class="button text-left bet_report">Report</button>';
                            }
                          }
                        } else {
                          echo '<label class="checkbox_container"><input type="checkbox" name="play_game" class="text-left play_game" data-user="'.$right_row["user_id"].'"><span class="checkmark"></span></label>'; 
                        }
                      } else {
                        if ($right_row['play_status'] == 1) {
                          echo '<label class="checkbox_container disabled"><input type="checkbox" checked disabled data-user="'.$right_row["user_id"].'" class="text-left"><span class="checkmark"></span></label>'; 
                        } else {
                          echo '<label class="checkbox_container disabled"><input type="checkbox" disabled class="text-left"><span class="checkmark op_bg"></span></label>';
                        }
                      }
                    }
                  } else {
                    echo '<label class="checkbox_container disabled"><input type="checkbox" checked="" disabled="" class="text-left" data-user="'.$right_row["user_id"].'"><span class="checkmark"></span></label><button class="button text-left bet_reported">Reported <i class="fa fa-check"></i></button>';
                  }
                  $right_status_class = "disabled";
                  if ($right_row['user_id'] == $_SESSION['user_id']) {
                    $right_status_class = "stream_icon enabled";
                    $right_tooltip = 'data-toggle="tooltip" data-html="true" data-placement="bottom" title="Click to Start. <br/> Live Lobby Stream"';
                    if ($right_row['stream_status'] == 'enable' && (($right_row['stream_type'] == 1 && !empty($right_row['stream_channel'])) || ($right_row['stream_type'] == 2 && !empty($right_row['obs_stream_channel'])))) {
                      $right_tooltip = 'data-toggle="tooltip" data-html="true" data-placement="bottom" title="Click to Stop. <br/> Live Lobby Stream"'; 
                    }
                  }
                  $right_stream_click_status = 'stream_click_status="enable" stream_type="'.$right_row['stream_type'].'"';
                  $right_img_src = base_url().'assets/frontend/images/live_stream_off.png ';
                  $right_team_leader = base_url().'assets/frontend/images/table_leader_new.png ';
                  if ($right_row['stream_status'] == 'enable' && (($right_row['stream_type'] == 1 && !empty($right_row['stream_channel'])) || ($right_row['stream_type'] == 2 && !empty($right_row['obs_stream_channel'])))) {
                    $right_img_src = base_url().'assets/frontend/images/live_stream_on.png';
                    $right_stream_click_status = 'stream_click_status="disable" stream_type="'.$right_row['stream_type'].'"';
                  }
                  echo "</div>";
                  echo '<div class="padd0 col-lg-6 text-center">';
                  echo (!empty($fan_tag['team_name'])) ? "<span class='team_name'>".$fan_tag['team_name']."</span>" : '';
                  echo "</div>";
                  echo '<div class="padd0 col-lg-4">';
                  echo ' <span class="team_leader" id="teamhead_'.$right_row['user_id'].'">';
                  if ($right_row['is_controller'] == 'yes') {
                    echo '<img src="'.$right_team_leader.'" alt="Team Controller">';
                  }
                  echo '</span> ';
                  echo ' <span class="tooltip_div"><img src="'.$right_img_src.'" class="'.$right_status_class.'" alt="live_stream" id="stream_'.$right_row['user_id'].'" '.$right_tooltip.' '.$right_stream_click_status.'></span> ';
                  echo (in_array($right_row['user_id'], $spectator_list) && $lby_is_spectator == 1)?'<span class="is_spectator_span"><i class="fa fa-eye"></i></span>':'';
                  $fantag = '<span is_fan="true">'.$fan_tag['fan_tag'].'</span>'; 
                  if ($lobby_creator == $_SESSION['user_id']) {
                    if ($right_row['user_id'] != $_SESSION['user_id']) {
                      if (count($get_play_status_lobby_bet) != count($lobby_game_bet_left_grp)+count($lobby_game_bet_right_grp)) {
                        $fantag = '<span class="pasctrl" id="'.$right_row['user_id'].'" fan_tag="'.$fan_tag['fan_tag'].'" is_fan="true">'.$fan_tag['fan_tag'].'</span>';
                      }
                    }
                    echo $fantag;
                    echo ' <a class="leave_lobby" self="yes" data-toggle="tooltip" player_tag_name="'.$fan_tag['fan_tag'].'" data-placement="bottom" title="Leave Live Lobby" lby_crtr="'.$right_row['is_creator'].'" lby_cntrlr="'.$right_row['is_controller'].'" usr_id="'.$right_row['user_id'].'" '.$everyone_readup.'><i class="fa fa-times"></i> </a>';
                    } else if ($right_row['user_id'] == $_SESSION['user_id'] && $lobby_creator != $_SESSION['user_id']) {
                      echo $fantag;
                      echo ' <a class="leave_lobby" self="no" player_tag_name="'.$fan_tag['fan_tag'].'" data-toggle="tooltip" data-placement="bottom" title="Leave Live Lobby" lby_crtr="'.$right_row['is_creator'].'" lby_cntrlr="'.$right_row['is_controller'].'" usr_id="'.$right_row['user_id'].'" '.$everyone_readup.'><i class="fa fa-times"></i> </a>'; 
                    } else {
                      echo $fantag;
                    }
                    echo '</div>';
                    ?>
                </div>
              <?php }
            } ?>
          </div>
        </div>
        <div class="lobby_tables_bottom">
          <div class="col-sm-2"></div>
          <div class="col-sm-3"></div>
          <div class="col-sm-2 border-right border-left border-top border_radious_4px">
            <div class="text-center join_lby_game_btn_div">
              <div>
                <?php if (empty($get_grp_for_me)) { 
                  echo ($is_full_play == 'no' && $this->session->userdata('is_18') == 1) ? '<lable class="join_lby_game_btn join_lobby_btn">Join Table</lable>' : ''; 
                } else { ?>
                  <lable class="join_lobby_btn already_joined">Joined <i class="fa fa-check"></i></lable>
                <?php } ?>
              </div>
              <div><label><?php echo $lobby_bet[0]['game_type'].'v'.$lobby_bet[0]['game_type']; ?></label></div>
              <?php if ($lobby_creator == $_SESSION['user_id']) {
                if ($is_full_play == 'no') { ?>
                  <div class="change_game_size_div">
                    <label>Change Game Size</label>
                    <div align="center">
                      <select class="form-control" name="change_game_size">|
                        <option value="">Please select a Gamesize</option>
                        <?php for ($i = 1; $i <= 16; $i++) { ?>
                          <option value="<?php echo $i;?>" class="playstore_gamesize_option" <?php if ($lobby_bet[0]['game_type'] == $i) { echo 'selected'; } ?>>
                            <?php echo '--' . $i . 'V' . $i . '--'; ?>
                          </option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                <?php }
              } ?>
              <div class="edit_lobby_tag_div">
                <div align="center" class="clearfix edit_tag_div">
                  <a class="live_lobby_btn edit_lobby_tag">Edit Player Tag</a>
                </div>
                <div align="center" class="clearfix edit_tag_div">
                  <a class="live_lobby_btn edit_myteam_tag"><?php echo (!empty($get_my_fan_tag->team_name)) ? 'Edit Team Name' : 'Add Team Name'; ?></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-3"></div>
          <div class="col-sm-2"></div>
        </div>
        <input type="hidden" id="lobby_id" value="<?php echo $lobby_bet[0]['lobby_id']; ?>">
        <input type="hidden" id="group_bet_id" value="<?php echo $lobby_bet[0]['id']; ?>">
      </div>
    </div>
    <div class="row coin_flip_div paddtop20">
      <div id="coin">
        <div class="flip_side side-a">
          <img src="<?php echo base_url(); ?>assets/frontend/images/head.png" alt="head" class="head img img-responsive">
        </div>
        <div class="flip_side side-b">
          <img src="<?php echo base_url(); ?>assets/frontend/images/tail.png" alt="teil" class="teil img img-responsive">
        </div>
      </div>
      <h4 style="text-align: center; color: #ff5000; padding-top: 2px;">COIN TOSS</h4>
    </div>
    <div class="row dragblearea">
      <div class="col-sm-12 live_lby_waiting_list">
        <div class="col-sm-6 left_wtable">
          <div class="join_wtnglist_btn row">
            <a class="btn lobby_btn pull-left join_wtng_btn" data-toggle="tooltip" data-html="true" data-placement="right" data-original-title="Click to Join Left waiting table" tg_table="LEFT" <?php echo (!in_array($_SESSION['user_id'],$wtnglist['wlfanarr']))? 'style="display:block"':'style="display:none"'; ?>> Join Waiting List </a>
          </div>
          <div class="text-left wtnglstlistdiv left_tbl_wtnglst" wtnglst_table="LEFT">
            <h3> Left table Waiting list</h3>
            <?php
            if (!empty($wtnglist['leftwaitinglist'])) {
              foreach ($wtnglist['leftwaitinglist'] as $key => $lwl) { ?>
                <div class="waitingusr" id="<?php echo $lwl->fan_id; ?>" lobby_id="<?php echo $lwl->lobby_id; ?>" fan_tag="<?php echo $lwl->fan_tag; ?>" user_id="<?php echo $lwl->user_id; ?>" table="<?php echo $lwl->table_cat; ?>" is_eighteen='<?php echo ($lwl->is_18 == 1)? 'yes':'no'; ?>'>
                  <?php if ($_SESSION['user_id'] == $lwl->user_id || $lobby_creator == $_SESSION['user_id']) { ?>
                  <a class="leave_lobby" self="<?php echo ($lobby_creator == $_SESSION['user_id'])? 'yes':'no'; ?>" player_tag_name="<?php echo $lwl->fan_tag; ?>" data-toggle="tooltip" data-placement="bottom" title="" lby_crtr="<?php echo ($lobby_creator == $lwl->user_id)?'yes':'no'; ?>" usr_id="<?php echo $lwl->user_id; ?>" everyone_readup="<?php echo ($lobby_bet[0]['is_full_play'] == 'yes')? 'yes':'no'; ?>" data-original-title="Leave Waiting List" leave_from_waitinlist="yes"><i class="fa fa-times"></i></a>
                  <?php } ?>
                  <span class="fan_tag"><?php echo $lwl->fan_tag; ?></span>
                  <?php echo (in_array($lwl->user_id, $spectator_list) && $lby_is_spectator == 1)?'<span class="is_spectator_span"><i class="fa fa-eye"></i></span>':''; ?>
                  <?php if ($lobby_creator == $lwl->user_id || $lwl->is_18 == 0) { ?>
                  <span class="waiting_streamicon"> <img <?php echo ($lwl->stream_status == 'enable') ? 'src="'.base_url().'assets/frontend/images/live_stream_on.png" stream_click_status="disable"': 'src="'.base_url().'assets/frontend/images/live_stream_off.png" stream_click_status="enable"'; echo ($lwl->user_id == $_SESSION['user_id']) ? 'class="stream_icon enabled"' : 'class="disabled"'; ?> id="stream_<?php echo $lwl->user_id; ?>" alt="Stream Icon"></span>
                  <?php } ?>
                  <span class="eighteen_plus_img pull-right"><?php echo ($lwl->is_18 == 1)? '<img src="'.base_url().'assets/frontend/images/eighteenplus.png" class="img" alt="Eighteen plus img">':'<span class="under_eighteen_lable">Under 18</span>'; ?></span>
                  <?php echo (!empty($lwl->team_name)) ? "<span class='team_name'>".$lwl->team_name."</span>" : ''; ?>
                </div>
              <?php }
            } ?>
          </div>
        </div>
        <div class="col-sm-6 right_wtable">
          <div class="join_wtnglist_btn row">
            <a class="btn lobby_btn pull-right join_wtng_btn" data-toggle="tooltip" data-html="true" data-placement="left" data-original-title="Click to Join Right waiting table" tg_table="RIGHT" <?php echo (!in_array($_SESSION['user_id'],$wtnglist['wlfanarr']))? 'style="display:block"':'style="display:none"'; ?>> Join Waiting List </a>
          </div>            
          <div class="text-right wtnglstlistdiv right_tbl_wtnglst" wtnglst_table="RIGHT">
            <h3> Right table Waiting list</h3>
            <?php
            if (!empty($wtnglist['rightwaitinglist'])) {
              foreach ($wtnglist['rightwaitinglist'] as $key => $rwl) { ?>
                <div class="waitingusr" id="<?php echo $rwl->fan_id; ?>" lobby_id="<?php echo $rwl->lobby_id; ?>" fan_tag="<?php echo $rwl->fan_tag; ?>" user_id="<?php echo $rwl->user_id; ?>" table="<?php echo $rwl->table_cat; ?>" is_eighteen='<?php echo ($rwl->is_18 == 1)? 'yes':'no'; ?>'>
                  <span class="eighteen_plus_img pull-left"><?php echo ($rwl->is_18 == 1)? '<img src="'.base_url().'assets/frontend/images/eighteenplus.png" class="img" alt="Eighteen plus img">':'<span class="under_eighteen_lable">Under 18</span>'; ?></span>
                  <?php echo (!empty($rwl->team_name)) ? "<span class='team_name'>".$rwl->team_name."</span>" : ''; ?> 
                  <?php if ($lobby_creator == $rwl->user_id || $rwl->is_18 == 0) { ?>
                    <span class="waiting_streamicon"> <img <?php echo ($rwl->stream_status == 'enable') ? 'src="'.base_url().'assets/frontend/images/live_stream_on.png" stream_click_status="disable"': 'src="'.base_url().'assets/frontend/images/live_stream_off.png" stream_click_status="enable"'; echo ($rwl->user_id == $_SESSION['user_id']) ? 'class="stream_icon enabled"' : 'class="disabled"'; ?> id="stream_<?php echo $rwl->user_id; ?>" alt="Stream Icon"></span>
                  <?php } ?>
                  <?php echo (in_array($rwl->user_id, $spectator_list) && $lby_is_spectator == 1)?'<span class="is_spectator_span"><i class="fa fa-eye"></i></span>':''; ?>
                  <span class="fan_tag"><?php echo $rwl->fan_tag; ?></span>
                  <?php if ($_SESSION['user_id'] == $rwl->user_id || $lobby_creator == $_SESSION['user_id']) { ?>
                    <a class="leave_lobby" self="<?php echo ($lobby_creator == $_SESSION['user_id'])? 'yes':'no'; ?>" player_tag_name="<?php echo $rwl->fan_tag; ?>" data-toggle="tooltip" data-placement="bottom" title="" lby_crtr="<?php echo ($lobby_creator == $rwl->user_id)?'yes':'no'; ?>" usr_id="<?php echo $rwl->user_id; ?>" everyone_readup="<?php echo ($lobby_bet[0]['is_full_play'] == 'yes')? 'yes':'no'; ?>" data-original-title="Leave Waiting List" leave_from_waitinlist="yes"><i class="fa fa-times"></i></a>
                  <?php } ?>
                </div>
              <?php }
            } ?>
          </div>
        </div>
      </div>
    </div>
    <?php if (isset($lobby_ads) || $lobby_ads !='') { ?>
    <div class="container live_lobby_ads_container">
        <div class="row">
           <div class="col-sm-3"></div>
           <div class="col-sm-6">
              <a class="live_lobby_ads_desciption_div" href="<?php echo $lobby_ads[0]->sponsered_by_logo_link; ?>" target="_blank">
                 <div class="live_lobby_ads_img_div">
                  <img src="<?php echo base_url('upload/ads_sponsered_by_logo/'.$lobby_ads[0]->upload_sponsered_by_logo);?>" alt="<?php echo $lobby_ads[0]->upload_sponsered_by_logo; ?>" class="img img-responsive">
                 </div>
                 <div class="live_lobby_ads_content_div">
                  <div class="live_lobby_ads_desciption"><?php echo $lobby_ads[0]->sponsered_by_logo_description; ?></div>
                </div>
              </a>
           </div>
           <div class="col-sm-3"></div>
        </div>
    </div>
    <?php } ?>
    <audio controls class="fireworks_audio_file" loop>
      <!-- <source src="<?php //echo base_url().'upload/stream_setting/'.(($giveaway_data[0]['use_custom_fireworks_audio'] == 1 && !empty($giveaway_data[0]['fireworks_audio'])) ? $giveaway_data[0]['fireworks_audio'] : 'giveaway_default_sound.mp3'); ?>" type=""> -->
      <source src="<?php echo base_url().'upload/stream_setting/giveaway_default_sound.mp3'; ?>" type="">
    </audio>
    <input type="hidden" id="tip_fireworks_sound" name="tip_fireworks_sound" value="giveaway_default_sound.mp3">

    <?php if (isset($lobby_bet[0]['lobby_password']) && $lobby_bet[0]['lobby_password'] !='') { ?>
    <div id="lobby_password_model" class="modal" style="display: none;">
        <div class="modal-content">
           <div class="modal-header">
              <span class="close">&times;</span>
              <h1>Please Enter Lobby Password</h1>
              <div class="modal_div_border">
                 <form method="POST">
                    <div class="form-group">
                       <div class="input-group col-lg-12">
                          <input type="text" class="form-control lby_pw" id="lby_pw" name="lby_pw" value="">
                          <div class="clearfix"></div>
                          <span style="color:red"></span>
                          <div class="input_fields_wrap">
                          </div>
                       </div>
                    </div>
                    <div class="form-group">
                       <div class="input-group col-lg-12">
                          <div class="alert alert-danger lobby_password_error" style="display: none;"></div>
                       </div>
                    </div>
                    <div class="form-group">
                       <div class="input-group col-lg-12">
                          <input type="button" value="Submit" id="submit_lobby" class="btn-submit">
                       </div>
                    </div>
                 </form>
              </div>
           </div>
        </div>
    </div>
    <?php } ?>
    <div id="join_lobby" class="modal" style="display: none;">
        <div class="modal-content">
           <div class="modal-header">
            <?php if ($remaining_table == 'FULL') { ?>
              <span class="close">&times;</span> 
              <div class="col-sm-12 left_choose">
                <div class="row">
                  <h1 class="diable_choose_table"> Both Tables are FULL..!! </h1>
                </div>
              </div>
              <div align="center" class="col-sm-12 close_button_row">
                <button class="close close_button"> Close </button>
              </div>
            <?php } else { ?>
              <span class="close">&times;</span>
              <h1>Choose Table..</h1>
              <?php echo ($full_table == 'LEFT') ? '<div class="col-sm-6 left_choose diable_choose_left"><h1 class="diable_choose_table">'.$full_table." table is full.. Please Join ".$remaining_table." table</h1></div>" : '<div class="col-sm-6 left_choose"><div class="choose_table_cat" table_cat="LEFT">LEFT</div></div>'; ?>
              <?php echo ($full_table == 'RIGHT') ? '<div class="col-sm-6 right_choose diable_choose_right"><h1 class="diable_choose_table">'.$full_table." table is full.. Please Join ".$remaining_table." table</h1></div>" : '<div class="col-sm-6 right_choose"><div class="choose_table_cat" table_cat="RIGHT">RIGHT</div></div>'; ?>
            <?php } ?>
        </div>
    </div>
    </div>
  </div>
  <div id="lobby_accept" class="modal" style="display: none;">
    <div class="modal-content">
       <div class="modal-header">
          <span class="close">&times;</span>  
          <h4><?php echo '$ '.number_format((float) $lobby_bet[0]['price'], 2, '.', ''); ?> will be deducted from your account!</h4>
          <h4>The Hosting Lobby Fee will also be deducted from your account.</h4>
          <h3><a class="button yes">OK</a><a class="button no">Cancel</a></h3>
       </div>
    </div>
  </div>
  <div id="leave_lobby" class="modal" style="display: none;">
    <div class="modal-content">
       <div class="modal-header">
          <span class="close">&times;</span>  
          <h1 class="warning_msg"></h1>
          <h4>Are you sure to leave this lobby .?</h4>
          <h3><a class="button yes">OK</a><a class="button no">Cancel</a></h3>
       </div>
    </div>
  </div>
  <div id="bet_report_modal" class="modal" style="display: none;">
    <div class="modal-content">
       <div class="modal-header">
          <span class="close">&times;</span>  
          <h4>Are you sure you want to submit a report</h4>
          <h4></h4>
          <h3><a class="button yes">OK</a><a class="button no">Cancel</a></h3>
       </div>
    </div>
  </div>
  <div id="change_stream_modal" class="modal" style="display: none;">
    <div class="modal-content">
       <div class="modal-header">
          <span class="close">&times;</span>  
          <h4>Are you sure to change Stream status</h4>
          <h4></h4>
          <h3><a class="button yes">OK</a><a class="button no">Cancel</a></h3>
       </div>
    </div>
  </div>
  <div id="send_tip" class="modal" style="display: none;">
      <div class="modal-content">
         <div class="modal-header">
            <span class="close">&times;</span>  
            <h4></h4>
            <h3></h3>
            <h3><a class="button yes">OK</a><a class="button no">Cancel</a></h3>
         </div>
      </div>
   </div>
  <div id="change_pass_control" class="modal custmodal" style="display: none;">
    <div class="modal-content">
     <div class="modal-header">
        <span class="close">&times;</span>
        <h1 class="warning_msg">WARNING MESSAGE</h1>
        <h4 class="passctrlh4"></h4>
        <h3></h3>
        <h3><a class="button yes">OK</a><a class="button no">Cancel</a></h3>
     </div>
    </div>
  </div>
  <?php if ($lobby_creator == $_SESSION['user_id'] && count($get_play_status_lobby_bet) == 0) { ?>
  <div id="change_lobby_details" class="modal custmodal" style="display: none;">
    <?php 
    $checked = '';
    $selectopt_hide = 'style = "display:block;"';
    $imgup_hide = $customopt_hide = 'style = "display:none;"';
    if ($lobby_bet[0]['is_custom'] == 'yes') {
      $cust = true;
      $checked = 'checked';
      $customopt_hide = 'style = "display:block;"';
      $imgup_hide = 'style = "display:inline-block;"';
      $selectopt_hide = 'style = "display:none;"';
    }
    ?>
    <div class="modal-content">
      <div class="modal-header text-left">
        <span class="close">&times;</span>
        <h1 class="text-left">Lobby Edit</h1>
        <div class="text-left">
          <div class="myprofile-edit">
             <div class="col-lg-12">
                <div class="row custom_settings text-center">
                  <label  style="display: none"><input type="checkbox" class="custom_lobby" name="custom_lobby" id="custom_lobby_check" <?php echo $checked; ?>> Custom Settings</label>
                </div>
                <div class="row">
                   <form id="frm_game_create" class="edit_lobby_details" name="frm_game_create">
                      <div class="form-group select_games" <?php echo $selectopt_hide; ?>>
                         <label class="col-lg-4">Platform</label>
                         <div class="input-group col-lg-8">
                            <select type="text" class="form-control game_category select21" <?php echo ($cust)? '': 'id="game_category" name="game_category" required'?> style="width:100%">
                              <option value="">--Select a system--</option>
                              <?php foreach ($game_category as $value) { ?>
                                <option value="<?php echo $value['id'];?>" <?php echo ($lobby_bet[0]['game_category_id'] == $value['id']) ? 'selected' : '' ; ?> cat_img="<?php echo $value['cat_img']; ?>"><?php echo $value['cat_slug'] ?></option>
                              <?php } ?>
                            </select>
                         </div>
                      </div>
                      <div class="form-group select_games" <?php echo $selectopt_hide; ?>>
                        <label class="col-lg-4">Stream Title</label>
                        <div class="input-group col-lg-8">
                          <?php if(!empty($lobby_bet[0]['game_name'])) { ?>
                          <select type="text" class="form-control game_name select2" name="game_name" <?php echo($cust)? '': 'id="name" name="game_name"  required'?> style="width:100%">
                            <option>--Select a Stream Title--</option>
                            <?php foreach ($game_name as $value) { ?>
                              <option value="<?php echo $value['id'];?>" <?php echo ($lobby_bet[0]['game_id'] == $value['id']) ? 'selected' : '' ; ?>><?php echo $value['game_name'] ?></option>
                              <?php } ?>
                          </select>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group select_games" <?php echo $selectopt_hide; ?>>
                        <label class="col-lg-4">Game Info or Stream Info</label>
                        <div class="input-group col-lg-8">
                          <?php if(!empty($lobby_bet[0]['sub_game_name'])) { ?>
                          <select type="text" class="form-control subgame_name_id select2" <?php echo($cust)? '': 'id="sub_name" name="subgame_name_id" required'?> style="width:100%">
                            <option>--Select a Game Info or Stream Info--</option>
                            <?php foreach ($sub_game_name as $value) { ?>
                              <option value="<?php echo $value['asgid'];?>" <?php echo ($lobby_bet[0]['sub_game_id'] == $value['asgid']) ? 'selected' : '' ; ?>><?php echo $value['sub_game_name'] ?></option>
                              <?php } ?>
                          </select>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group custom_games" <?php echo $customopt_hide; ?>>
                        <label class="col-lg-4">Platform</label>
                        <div class="input-group col-lg-8">
                          <select type="text" class="form-control game_category select21" <?php echo($cust)? 'id="game_category" name="game_category" required': ''?> style="width:100%">
                              <option value="">--Select a system--</option>
                              <?php foreach ($game_category as $value) { ?>
                                <option value="<?php echo $value['id'];?>" <?php echo ($lobby_bet[0]['game_category_id'] == $value['id']) ? 'selected' : '' ; ?> cat_img="<?php echo $value['cat_img']; ?>"><?php echo $value['cat_slug'] ?></option>
                              <?php } ?>
                            </select>
                        </div>
                      </div>
                        <!-- <?php// if(isset($cust_game_cat)) { ?>
                          <select type="text" id="game_category" class="form-control game_category select21" required style="width:100%">
                            <option value="<?php// echo $cust_game_cat[0]['id'];?>" selected><?php// echo $cust_game_cat[0]['category_name'] ?></option>
                          </select>
                          <?php// } else { ?>
                          <input type="text" placeholder="Enter Game Category" class="form-control select_game_category">
                          <?php// } ?> -->
                      <div class="form-group custom_games" <?php echo $customopt_hide; ?>>
                        <label class="col-lg-4">Stream Title</label>
                        <div class="input-group col-lg-8">
                          <input type="text" placeholder="Stream Title" class="form-control select_game_name" required value="<?php echo($lobby_bet[0]['game_name'])? $lobby_bet[0]['game_name'] :'' ?>">
                         </div>
                      </div>
                      <div class="form-group custom_games" <?php echo $customopt_hide; ?>>
                        <label class="col-lg-4">Game Info or Stream Info</label>
                        <div class="input-group col-lg-8">
                          <input type="text" placeholder="Game Info or Stream Info" class="form-control select_subgame_name_id" required value="<?php echo($lobby_bet[0]['sub_game_name'])? $lobby_bet[0]['sub_game_name'] :'' ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-4">Game Size</label>
                        <div class="input-group col-lg-8">
                          <select type="text" class="form-control select2" name="game_type"  id="game_type" required style="width:100%">
                            <option>--Select Game Size--</option>
                            <?php
                              if (!empty($lobby_bet[0]['game_type'])) {
                                for ($i = 1; $i <=16; $i++) {
                                  $sel = ($lobby_bet[0]['game_type'] == $i) ? 'selected' : '';
                                  echo '<option value="'.$i.'" '.$sel.'>--'.$i.'V'.$i.'--</option>';
                                }
                              }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-4">Entry Amount ($)</label>
                        <div class="input-group col-lg-8">
                          <input type="text" placeholder="$0.00" id="lobby_amount" class="form-control" name="bet_amount" value="<?php echo (!is_null($lobby_bet[0]['price']))? $lobby_bet[0]['price'] :''; ?>" onkeyup="return isNumbKey(event,this.id)" onkeydown="return isNumbKey(event,this.id)" required>
                          <span id="demo" style="margin-left: -194px;color: red;"></span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-4">Password</label>
                        <div class="input-group col-lg-8 lobby_password_div">
                          <label class="switch lobby_password_switch_button">
                            <input type="checkbox" name="password_option" id="password_option" <?php echo (!empty($lobby_bet[0]['lobby_password'])) ? 'checked' : '' ; ?>>
                            <span class="slider round"></span>
                          </label>
                          <input type="text" id="lobby_password" class="form-control lobby_password" placeholder="Enter Password" <?php echo (!empty($lobby_bet[0]['lobby_password'])) ? 'value="'.$lobby_bet[0]['lobby_password'].'"' : 'style="display: none;"' ; ?>>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-4">For 18+</label>
                        <div class="input-group col-lg-8 lobby_password_div">
                          <label class="switch lobby_for_18_plus_switch_button">
                            <input type="checkbox" name="for_18_plus" id="for_18_plus" <?php echo ($lobby_bet[0]['for_18_plus'] == '1') ? 'checked' : '' ; ?>>
                            <span class="slider round"></span>
                          </label>
                        </div>
                      </div>
                      <!-- <div class="form-group">
                        <label class="col-lg-4">Rules</label>
                        <div class="input-group col-lg-8">
                          <textarea class="form-control" id="description" placeholder="Game Description" rows="5" cols="5" name="description" required><?php echo (!empty($lobby_creator_det->description)) ? $lobby_creator_det->description : ''; ?></textarea>
                        </div>
                      </div> -->
                      <div class="form-group">
                        <label class="col-lg-4">Image</label>
                        <div class="input-group col-lg-8">
                          <div id="game_img">
                            <?php
                            if (!empty($lobby_bet[0]['game_image'])) {
                              foreach ($game_imgs as $value) {   
                                $selected_class = "";
                                if ($lobby_bet[0]['game_image_id'] == $value['id']) {
                                  $selected_class = "selected";  
                                }
                              ?>
                              <img class="img-cls <?php echo $selected_class; ?>" id="img<?php echo $value['id'] ?>" style="margin-right: 31px;" onclick="a('<?php echo $value['id'] ?>')" src="<?php echo base_url().'upload/game/'.$value['game_image']; ?>" width="100" height="80"/>
                              <span class="change_custlby_img btn lobby_btn" <?php echo $imgup_hide; ?>> Change image</span>
                              <?php } } ?>
                          </div>
                        </div>
                      </div>
                      <div class="btn-group">
                        <div class="col-lg-8 pull-right padd0">
                          <input type="hidden" name="personal_challenge_friend_id">
                          <input type="hidden" name="event_key" value="<?php echo $lobby_bet[0]['event_key'] ?>">
                          <input type="hidden" id="game_image_id" name="game_image_id" value="<?php echo($lobby_bet[0]['game_image_id']) ? $lobby_bet[0]['game_image_id'] : '' ; ?>" class="new_game-image">
                          <a type="button" id="editLivelobby" value="Submit" class="btn-update lobby_btn">Submit</a>
                        </div>
                      </div>
                   </form>
                </div>
             </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
  <div id="edit_lobby_tag_mdl" class="modal custmodal" style="display: none;">
    <div class="modal-content">
       <div class="modal-header">
          <span class="close">&times;</span>
          <div class="text-left">
            <h1 class="tag_btn_div text-left">Edit Player Tag</h1>
          </div>
            <div class="modal_div_border">
              <div class="form-group">
                <!-- <div class="input-group col-lg-12">
                  <h3>Player Tag</h3> 
                </div> -->
                <div class="input-group col-lg-12">
                  <input type="text" class="form-control" name="change_lobby_tag" placeholder="Enter Player Tag" value="<?php echo ($get_my_fan_tag->fan_tag)? $get_my_fan_tag->fan_tag : '';  ?>">
                </div>
                <div class="input-group col-lg-12">
                  <div class="alert alert-danger lobby_tag_error" style="display: none;"></div>
                </div>
              </div>
              <!-- <div class="form-group">
                <div class="input-group col-lg-12">
                  <h3>Esports Team Name</h3> 
                </div>
                <div class="input-group col-lg-12">
                  <input type="text" class="form-control" name="change_team_name" placeholder="Esports Team Name" value="<?php // echo ($this->session->userdata('team_name')) ? $this->session->userdata('team_name') : '';  ?>">
                </div>
              </div> -->
            </div>
          <h3><a class="button yes">Update</a><a class="button no">Cancel</a></h3>
       </div>
    </div>
  </div>  
  <div id="edit_team_name_mdl" class="modal custmodal" style="display: none;">
    <div class="modal-content">
       <div class="modal-header">
          <span class="close">&times;</span>
          <div class="text-left">
            <h1 class="tag_btn_div text-left">Please Enter Your Team Name! </h1>
          </div>
            <div class="modal_div_border">
              <div class="form-group">
                <div class="input-group col-lg-12">
                  <input type="text" class="form-control" name="change_team_name" placeholder="Enter Your Team Name" value="<?php echo (!empty($get_my_fan_tag->team_name)) ? $get_my_fan_tag->team_name : (!empty($this->session->userdata('team_name')) ? $this->session->userdata('team_name') : ''); ?>">
                </div>
                <div class="input-group col-lg-12">
                  <div class="alert alert-danger lobby_team_name_error" style="display: none;"></div>
                </div>
                <div class="input-group col-lg-12 team_save_settings">
                  <label class="toggle chkbx_container"><input type="checkbox" name="save_stream"> &nbsp;Save in Settings<span class="checkmark"></span></label>
                </div>
              </div>
            </div>
          <h3><a class="button yes">Update</a><a class="button no">Cancel</a></h3>
       </div>
    </div>
  </div>      
  <div id="enter_stream_channel" class="modal" style="display: none;">
    <div class="modal-content" style="width:70%">
      <div class="modal-header">
        <div class="row">
          <span class="close">&times;</span>
        </div>
        <!-- <div class="stream_option_div" <?php // echo $lobby_bet[0]['is_event'] == '0' ? 'style="display: none;"' : 'style="display: block;"'?>>
          <div class="row" style="margin-bottom: 20px;">
            <h1 class="text-left" style="margin: 0px;">Choose your Stream</h1>
          </div>
          <div class="row">
            <div class="col-sm-6 stream_optn obs_stream left_choose" stream_type="obs">OBS</div>
            <div class="col-sm-6 stream_optn twitch_stream right_choose" stream_type="twitch">TWITCH</div>
          </div>
        </div> -->
        <!-- <?php // echo $lobby_bet[0]['is_event'] == '0' ? 'style="display: block;"' : 'style="display: none;"'; ?> -->
        <div class="stream_choosen_div">
          <div style="display: block;">
            <h1 class="text-left">Start own stream</h1>
            <h4></h4>
            <div class="stream_settings_stream_first_div modal_div_border">
              <div class="form-group">
                <div class="select_stream_type_switch_box">
                  <label class="switch" data-original-title="Switch to select stream" data-placement="bottom" data-toggle="tooltip"><input type="checkbox" name="stream_type_select" id="stream_type_select"><span class="slider round"></span></label>
                </div>
              </div>
              <div class="stream_eldiv obs_stream_detail" stream_type="obs">
                <div class="form-group text-left">
                  <label for="server_url"><h4>SERVER URL</h4></label>
                  <?php 
                  $host_server = 'rtmp://'.$_SERVER['HTTP_HOST'];
                  $rtmp_server_url = $host_server.':1935/live/';
                  // $rtmp_server_url = ($_SERVER['HTTP_HOST'] != 'proesportsgaming.com') ? 'rtmp://esportstesting.com:1935/live/' : 'rtmp://'.$_SERVER['HTTP_HOST'].':1935/live/';
                  ?>
                  <input type="text" name="server_url" class="form-control input-group" value="<?php echo $rtmp_server_url; ?>" readonly>
                </div>
                <div class="form-group text-left">
                  <label for="key"><h4>STREAM KEY</h4></label>
                  <input type="text" name="stream_channel_input" class="form-control input-group" value="<?php echo $random_string;?>" readonly>
                </div>
                <div class="form-group">
                  <ul style="padding-left: 20px;padding-bottom: 10px;" class="text-left">
                    <li>Copy this SERVER URL and STREAM KEY and paste it to on OBS software</li>
                    <li>Start streaming from OBS software</li>
                    <li>Then click this Submit button</li>
                  </ul>
                </div>
                <!-- <div class="form-group">
                  <div class="input-group col-lg-12 save_stream_radio">
                    <label class="toggle checkbox_container"><input type="checkbox" name="save_stream"> &nbsp;Save into Stream Settings<span class="checkmark"></span></label>
                  </div>
                </div> -->
                <div class="form-group">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="text-left">
                        <a class="cmn_btn yes stream_sbmit" action="sync" stream_type="obs">Sync</a>
                      </div>
                      <div class="alert alert-danger obs_sync_msg" style="display: none; margin: 15px 0 0;"></div>
                    </div>
                    <div class="col-sm-6">
                      <div class="force_start_stream_main_div pull-right">
                        <div style="margin-right: 16px;">
                          <h3 style="margin: 0 auto;">Force start camera</h3>
                        </div> 
                        <div class="text-left">
                          <label class="switch" style="margin: 0;"><input type="checkbox" name="force_start_stream_checkbox" id="force_start_stream_checkbox" <?php echo (!empty($force_start_stream_data) && $force_start_stream_data['default_value'] != 'off') ? 'checked' : ''; ?>><span class="slider round"></span></label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group text-left">
                  <div class="input-group col-lg-12">
                    <!-- <button type="submit" value="Submit" class="button" stream_type="obs">Ok</button> -->
                    <a class="button yes stream_sbmit" action="ok" stream_type="obs">OK</a>
                    <a class="button no">Cancel</a>
                  </div>
                </div>
              </div>
              <div class="stream_eldiv twitch_stream_detail" stream_type="twitch" style="display: none;">
                <h1 class="text-left">Enter Twitch ID</h1>
                <div class="">
                  <div class="form-group">
                    <div class="input-group col-lg-12">
                     <input type="text" class="form-control" name="stream_channel_input" placeholder="Enter your channel">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-lg-6 save_stream_radio">
                      <label class="toggle checkbox_container"><input type="checkbox" name="save_stream"> &nbsp;Save into Stream Settings<span class="checkmark"></span></label>
                    </div>
                    <div class="col-lg-6">
                      <div class="force_start_stream_main_div pull-right">
                        <div style="margin-right: 16px;">
                          <h3 style="margin: 0 auto;">Start Twitch Stream</h3>
                        </div> 
                        <div class="text-left">
                          <label class="switch" style="margin: 0;"><input type="checkbox" name="start_twitch_stream_checkbox" id="start_twitch_stream_checkbox" <?php echo (!empty($start_twitch_stream_data) && $start_twitch_stream_data['default_value'] != 'off') ? 'checked' : ''; ?>><span class="slider round"></span></label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="input-group col-lg-12">
                      <div class="alert alert-danger stream_error" style="display: none;"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="input-group col-lg-12">
                      <button type="submit" class="btn-submit stream_sbmit" action="ok" stream_type="twitch">Submit</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- <div class="stream_eldiv twitch_stream_detail" stream_type="twitch" style="display: none;">
            <h1 class="text-left">Enter Twitch ID</h1>
            <div class="modal_div_border">
              <div class="form-group">
                <div class="input-group col-lg-12">
                 <input type="text" class="form-control" name="stream_channel_input" placeholder="Enter your channel">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group col-lg-12 save_stream_radio">
                  <label class="toggle checkbox_container"><input type="checkbox" name="save_stream"> &nbsp;Save into Stream Settings<span class="checkmark"></span></label>
                </div>
              </div>
              <div class="form-group">
                <div class="input-group col-lg-12">
                  <div class="alert alert-danger stream_error" style="display: none;"></div>
                </div>
              </div>
              <div class="form-group">
                <div class="input-group col-lg-12">
                  <button type="submit" class="btn-submit stream_sbmit" stream_type="twitch">Submit</button>
                </div>
              </div>
            </div>
          </div> -->
        </div>
      </div>
    </div>
  </div>
  <div id="send_tip_amount" class="modal custmodal send_teamtip_amount" style="display: none;">
    <div class="modal-content">
       <div class="modal-header">
          <span class="close">&times;</span>  
          <h1 class="text-left">Please Enter Donation Amount</h1>
          <form id="upload_form" method="POST" enctype="multipart/form-data">
            <div class="modal_div_border row">
              <div class="col-sm-12">
                <div class="title_head_div text-center">
                  <label class="team-title"><h4>ESPORTS TEAM</h4></label>
                </div>
              </div>
              <div class="col-sm-6 table-left">
                <?php if ($lobby_game_bet_left_grp != '') {  ?>
                <div class="team_title text-center">
                  <div class="row">
                    <div class="mainteam_title"> <?php echo ($lobby_game_bet_left_grp[0]['team_name'])? $lobby_game_bet_left_grp[0]['team_name'] : '&nbsp;';?> </div>
                    <div class="col-sm-12 pull-left">
                      <label class="check_all_lable"><input type="checkbox" name="left_check"> Check All Left </label>
                    </div>
                  </div>
                </div>
                <div class="team_member">
                  <?php 
                  foreach ($lobby_game_bet_left_grp as $key => $left_row) {
                    $fan_tag = $this->General_model->get_my_fan_tag($lobby_bet[0]['lobby_id'],$left_row['user_id']);
                    ?>
                    <div class="form-group" id="activeusr_<?php echo $left_row['user_id']; ?>">
                      <div class="input-group col-lg-12">
                        <label><input type="checkbox" name="teap_team[<?php echo $left_row['user_id']; ?>]" value="<?php echo $left_row['user_id']; ?>"> <?php echo $fan_tag->fan_tag; ?> </label><span class="tipamount"></span>
                      </div>
                    </div>
                  <?php }
                } ?>
                </div>
              </div>
              <div class="col-sm-6 table-right">
                <?php if ($lobby_game_bet_right_grp != '') { ?>
                <div class="team_title text-center">
                  <div class="row">
                    <div class="mainteam_title"> <?php echo ($lobby_game_bet_right_grp[0]['team_name'])? $lobby_game_bet_right_grp[0]['team_name'] : '&nbsp;'; ?> </div>
                    <div class="col-sm-12 pull-right">
                      <label class="check_all_lable"> Check All Right <input type="checkbox" name="right_check"> </label>
                    </div>
                  </div>
                </div>
                <div class="team_member text-right">
                <?php foreach ($lobby_game_bet_right_grp as $key => $right_row) {
                    $fan_tag = $this->General_model->get_my_fan_tag($lobby_bet[0]['lobby_id'],$right_row['user_id']);
                    ?>
                    <div class="form-group" id="activeusr_<?php echo $right_row['user_id']; ?>">
                       <div class="input-group col-lg-12">
                        <span class="tipamount"></span>
                         <label> <?php echo $fan_tag->fan_tag; ?> <input type="checkbox" name="teap_team[<?php echo $right_row['user_id']; ?>]" value="<?php echo $right_row['user_id']; ?>"> </label>
                       </div>
                    </div>
                <?php }
                } ?>
                </div>
              </div>
              <div class="row team_donate_bottom_div">
                <div class="col-sm-12">
                  <div class="col-sm-4">
                    <div class="form-group">
                       <div class="input-group">
                         <label>EACH</label>
                         <input type="text" class="form-control" name="tip_amount_input" placeholder="0.00" onkeyup="return isNumberKey(event)">
                       </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                       <div class="input-group">
                          <label>TOTAL</label>
                          <input type="text" class="form-control" name="tip_total_amount_input" placeholder="0.00" readonly>
                        </div>
                       </div>
                  </div>
                </div>
                <div class="col-sm-12 form-group flex-container">
                  <div class="col-sm-4">
                     <div class="input-group">
                        <label>
                        <input type="checkbox" checked="true" id="default_sound" name="default_sound" value="1"> Default</label>
                      </div>
                  </div>
                  <div class="col-sm-4 select_audio" style="display: none">
                     <div class="file-upload active">
                          <div class="file-select">
                              <div class="file-select-button" id="fileName">Choose File</div>
                              <div class="file-select-name" id="noFile">No file chosen...</div> 
                              <input type="file" name="fireworks_audio_file" id="fireworks_audio" accept="audio/*">
                          </div>
                      </div>
                  </div>
                   <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
                  <input type="hidden" name="lobbygroup_bet_id" value="<?php echo $lobby_bet[0]['id']; ?>">
                  <input type="hidden" name="lobby_id" value="<?php echo $lobby_bet[0]['lobby_id']; ?>">
                  <div class="col-sm-4">
                       <div class="input-group w-100">
                          <input type="submit" id="submit_audio" value="Submit" class="btn-submit w-100">
                       </div>
                  </div>
                 </div>
                </div>                  
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                         <div class="input-group col-lg-12">
                            <div class="alert alert-danger tip_amount_error" style="display: none;"></div>
                         </div>
                      </div>
                    </div>
                 </div>
              </div>
            </div>
          </form>
       </div>
    </div>
  </div>
  <div id="bet_report_modal_status" class="modal" style="display: none;">
    <div class="modal-content">
       <div class="modal-header">
          <span class="close">&times;</span>  
          <h4>Lobby ID: <strong><?php echo $lobby_bet[0]['lobby_id']; ?></strong>&nbsp;&nbsp;&nbsp;&nbsp; Lobby Bet ID: <strong><?php echo $lobby_bet[0]['id']; ?></strong></h4>
          <div class="won_lost_status_btn row"><a class="button won" pop_up_id="won_div" form_id="win">WON</a><a class="button lost" pop_up_id="lost_div" form_id="lose">LOST</a><a class="button admin" pop_up_id="admin_div" form_id="admin">ADMIN</a></div>
          <div class="won_div" id="won_div" style="display: none;">
             <?php if(!empty($check_reported) && $check_reported[0]['win_lose_status']=='win') { ?>
             <div class="report_error">Player has already reported Won..!!<br>Please choose another option.</div>
             <?php } else { ?>
             <form class="won_lost_status" id="win" method="POST">
                <div class="form-group">
                   <label class="col-lg-4">Video Url</label>
                   <div class="input-group col-lg-8">
                      <input type="text" class="form-control video_url" name="url" value="">
                      <div class="clearfix"></div>
                      <span style="color:red"></span>
                      <div class="input_fields_wrap">
                      </div>
                   </div>
                </div>
                <div class="form-group">
                   <label class="col-lg-4"></label>
                   <div class="input-group col-lg-8">
                      <input type="checkbox" name="accept_risk" value="1" class="accept_risk"> I Accept Risk Because I Have No video/proof of  Victory
                      <div class="clearfix"></div>
                      <span style="color:red"></span>
                   </div>
                </div>
                <div class="form-group">
                   <label class="col-lg-4"></label>
                   <div class="input-group col-lg-8">
                      <input type="hidden" name="win_lose_status" value="win">
                      <input type="button" value="Submit" class="btn-submit won_submut" id="mehul">
                   </div>
                </div>
             </form>
             <?php } ?>
          </div>
          <div class="won_div" id="lost_div" style="display: none;">
             <?php if (!empty($check_reported) && $check_reported[0]['win_lose_status']=='lose') { ?>
             <div class="report_error">Player has already reported Lose..!!<br>Please choose another option.</div>
             <?php } else { ?>
             <form class="won_lost_status" id="lose" method="POST">
                <div class="form-group">
                   <div class="input-group col-lg-12">
                      <h4>Are you sure you want to report a loss?</h4>
                   </div>
                </div>
                <div class="form-group">
                   <div class="input-group col-lg-12">
                      <input type="hidden" name="win_lose_status" value="lose">
                      <input type="button" value="Submit" class="btn-submit admin_submut">
                   </div>
                </div>
             </form>
             <?php } ?>    
          </div>
          <div class="won_div" id="admin_div" style="display: none;">
             <form class="won_lost_status" id="admin" method="POST">
                <div class="form-group">
                   <label class="col-lg-4">Video Url</label>
                   <div class="input-group col-lg-8">
                      <input type="text" class="form-control video_url" name="url" value="">
                      <div class="clearfix"></div>
                      <span style="color:red"></span>
                      <div class="input_fields_wrap">
                      </div>
                   </div>
                </div>
                <div class="form-group">
                   <label class="col-lg-4">Please explain in detail to the admin</label>
                   <div class="input-group col-lg-8">
                      <textarea class="form-control" name="explain_admin"></textarea>
                      <div class="clearfix"></div>
                      <span style="color:red"></span>
                      <div class="input_fields_wrap"></div>
                   </div>
                </div>
                <div class="form-group">
                   <label class="col-lg-4"></label>
                   <div class="input-group col-lg-8">
                      <input type="checkbox" name="accept_risk" class="accept_risk" value="1"> I Accept Risk Because I Have No video/proof of  Victory
                      <div class="clearfix"></div>
                      <span style="color:red"></span>
                   </div>
                </div>
                <div class="form-group">
                   <label class="col-lg-4"></label>
                   <div class="input-group col-lg-8">
                      <input type="hidden" name="win_lose_status" value="admin">
                      <input type="button" value="Submit" class="btn-submit admin_submut">
                   </div>
                </div>
             </form>
          </div>
       </div>
    </div>
  </div>
  <?php
  if (!empty($giveaway_data[0]['gw_id'])) { ?>
    <div class="giveaway_part text-center ">
      <a class="giveaway_icon" href="<?php echo base_url().'giveaway/'.$giveaway_data[0]['lobby_id']; ?>" target="_self">
        <img class="img img-responsive" src="<?php echo base_url().'/upload/all_images/'.$giveaway_data[0]['gw_img']; ?>"> <span class="title">ENTER GIVEAWAY</span>
      </a>
    </div>
  <?php } ?>
  <div id="giveaway_modal" class="modal custmodal giveaway_modal" style="display: none;">
    <div class="modal-content">
      <div class="modal-header">
        <span class="close">&times;</span>
        <div class="text-left">
          <h1 class="giveaway_div"><span class="modal_h1_msg">Giveaway Settings</span></h1>
          <hr>
        </div>
        
      </div>
    </div>
  </div>
  <?php $this->load->view('lobby/tournament_amount_distribution_modal.tpl.php'); ?>
</section>
<input type="hidden" name="lobby_accept_total_balance" value="<?php echo $total_balance; ?>">
<input type="hidden" name="lobby_accept_price" value="<?php echo $lobby_bet[0]['price'] ?>">
<input type="hidden" name="lobby_price_add_decimal" value="<?php echo $price_add_decimal; ?>">
<input type="hidden" name="lobbygroup_bet_id" value="<?php echo $lobby_bet[0]['id']; ?>">
<input type="hidden" name="lobby_id" value="<?php echo $lobby_bet[0]['lobby_id']; ?>"> 
<input type="hidden" name="lobby_custom_name" value="<?php echo $lobby_bet[0]['custom_name']; ?>">
<input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
<input type="hidden" name="creator_id" value="<?php echo $lobby_creator; ?>">
<input type="hidden" name="lobby_event" value="<?php echo $lobby_bet[0]['is_event']; ?>">
<input type="hidden" name="is_admin" value="<?php $user_name = $this->General_model->view_single_row('user_detail','user_id',$_SESSION['user_id']); echo $user_name['is_admin']; ?>">
<input type="hidden" name="creator_timezone" value="<?php echo $lobby_creator; ?>">
<input type="hidden" name="event_tkt_mnt" value="<?php echo number_format((float) $lobby_bet[0]['event_price']-$lobby_bet[0]['spectate_price'], 2, '.', ''); ?>">
<?php //$lobby_bet[0]['event_fee'] ?>
<?php $this->load->view('common/realtime_stream.tpl.php'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/jquery.fireworks.js"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/video.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/videojs-flvjs.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/flv.min.js'; ?>"></script>
<script type="text/javascript">
  function recordstream(canvas){
    // var canvas = document.querySelector("video");
    // console.log('canvas ===>',canvas)
    // Optional frames per second argument.
    var clip_btn = canvas.parentNode.nextElementSibling.getElementsByClassName("text-right")[0].getElementsByTagName("A")[0];
    // console.log('clip_btn ===>',clip_btn)
    // var stream = canvas.captureStream ? canvas.captureStream(25) : canvas.mozCaptureStream(25);
    var stream = canvas.captureStream ? canvas.captureStream(25) : '';
    var recordedChunks = [];
    // if(stream.active == false){
    if(stream.active == false || stream == ''){  
      // console.log('offline');
      i = 1;
      setTimeout(event => {
        // console.log("stopping");
        recordstream(canvas);
        // mediaRecorder.stop();
      }, 5000);
    }else{
      // console.log('online');
      console.log('stream',stream);
      var options = { mimeType: "video/webm; codecs=vp8,opus", ignoreMutedMedia: false };
      mediaRecorder = new MediaRecorder(stream, options);

      mediaRecorder.ondataavailable = handleDataAvailable;
      // setTimeout(event => {
      //   console.log("stopping");
        // mediaRecorder.stop();
      mediaRecorder.start();
      // }, 5000);

      function handleDataAvailable(event) {
        console.log("data-available",event.data);
        if (event.data.size > 0) {
          recordedChunks.push(event.data);
          console.log(recordedChunks);
          download();
        } else {
          // ...
        }
      }
      function download() {
        var blob = new Blob(recordedChunks, {
          type: "video/webm"
        });
        uploadBlob(blob);
        // var url = URL.createObjectURL(blob);
        // var a = document.createElement("a");
        // document.body.appendChild(a);
        // a.style = "display: none";
        // a.href = url;
        // a.download = "test.webm";
        // a.click();
        // window.URL.revokeObjectURL(url);
      }
      function uploadBlob(blob) {
          var formData = new FormData();
          formData.append('file', blob);
          formData.append('video-filename', 'FileName.webm');
          formData.append('stream_key', canvas.getAttribute('data-key'));
          $.ajax({
              type: "post",
              url: "<?php echo base_url(); ?>Streamsetting/uploadBlob",
              data: formData,
              processData: false,
              contentType: false,
              success: function(response) {
                  console.log(response)
                  if(clip_btn.style.display='none'){
                    clip_btn.style.display='';
                  }
              },
              error: function(jqXHR, textStatus, errorMessage) {
                  console.log('Error:' + JSON.stringify(errorMessage));
              }
          });
      }
      // demo: to download after 9sec
      setTimeout(event => {
        console.log("stopping");
        mediaRecorder.stop();
        recordstream(canvas);
      }, 31000);
    }
  }
  // $('.giveaway_part').draggable();
  function close_window(event){
    document.fire("keypress",{altKey:true,keyCode:115,bubbles:true})
  }
  var availableTags = [<?php print_r($fan_tagarr)?>];
  function isNumberKey(evt){
    var val = $('input[name="tip_amount_input"]').val();
    if (val == '.') {}
    else {
      if (isNaN(val)) {
        val = val.replace(/[^0-9\.]/g,'');
        if (val.split('.').length>1) 
          val = val.replace(/\.+$/,"");
      }
    }
    $('input[name="tip_amount_input"]').val(val);
  }


$("#volume").slider({
    min: 0,
    max: 100,
    value: '<?php echo $this->session->userdata('val_set'); ?>',
    range: "min",
    slide: function(event, ui) {
    // console.log(ui.);
    // setVolume(ui.value / 100);

    }
  });
var sesssion_data = '<?php echo $this->session->userdata('val_set'); ?>';

if(sesssion_data=="")
{
   $('.volume_adjustment').removeClass("mute_button");
   $("#volume").slider('value',100);
     $(".vol_up").show();
     $('.vol_mute').hide();
}
else if( sesssion_data==0)
{
    $('.volume_adjustment').addClass("mute_button");
    $('.vol_mute').show();
    $(".vol_up").hide();
}
else
{   
     $('.volume_adjustment').removeClass("mute_button");
     $(".vol_up").show();
     $('.vol_mute').hide();

} 
// hide show
//   $(".volume_adjustment").hover(function(){
//     $('.slider_adjustment').show();
// },function(){
//     $('.slider_adjustment').hide();
// });

var sesssion_data = '<?php echo $this->session->userdata('val_set'); ?>';

$('.volume_adjustment').on('click',function(){
     $(this).toggleClass("mute_button");
  if ($(this).hasClass('mute_button'))
  {
        $("#volume").slider('value',0);
        // $(".vol_up").hide();
        // $('.vol_mute').show();
  } 
  else
  {
        if(sesssion_data!=0)
          {
            $("#volume").slider('value',sesssion_data);
          }
          else
          {
            $("#volume").slider('value',100);
          }
          
          // $(".vol_up").show();
          // $('.vol_mute').hide();
  }
        
    
});

$( "#volume" ).on( "slidechange", function( event, ui ) {
  var val_set = $("#volume").slider('values',$(this).val());
   ;
    $('#session_volume').val(val_set);
    var volume_val=val_set/100;
    var volume_val=volume_val.toFixed(1);
    // $('.fireworks_audio_file')[0].volume=volume_val;
     // $('audio,video')[0].volume=volume_val;
     $("audio,video").prop("volume", volume_val);

   // console.log('values',$(this).val());
   // console.log(event);
   // console.log(val_set);
   // AJAX request
       $.ajax({
         url: base_url+'livelobby/get_bar_value',
         type: 'post',
         data: {val_set:'"'+ val_set +'"'},
         success: function(result){
           // result = $.parseJSON(result);
           // console.log(result);
         } 
       });
     if(val_set==0)
     {
         $('.volume_adjustment').addClass("mute_button");
        $('.vol_mute').show();
        $(".vol_up").hide();
     }
     else
     { 
        $('.volume_adjustment').removeClass("mute_button");
        $(".vol_up").show();
        $('.vol_mute').hide();
     }
});  

</script> 
<style type="text/css">
  .b_color{
    background-color: #fff;
    color: #ff5000;
  }
  sup {
    top: -1.5em;
    right: 1.5em;
    /*position: relative;*/
    font-size: 75%;
    line-height: 0;
    vertical-align: baseline;
}
</style>