<section class="body-middle innerpage">
   <div class="container">
   <div class="row" align="center">
      <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
      <div class="col-lg-4 col-sm-4 col-md-4">
         <h2><?php echo $basic_details['lobby_details']['game_name']; ?></h2>
      </div>
      <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
   </div>
   <div class="row">
      <div class="col-lg-2"></div>
      <div class="col-lg-8 event_lobby"> 
         <?php
         $current_total_balance = (!empty($this->session->userdata('total_balance'))) ? $this->session->userdata('total_balance') : 0;
         if ($current_total_balance >= $basic_details['lobby_details']['spectate_price'] && $basic_details['lobby_details']['is_spectator'] == 1) { ?>
         <form method="post" action="<?php echo base_url().'livelobby/addUserToSpectate';?>">
            <input type="hidden" name="user_id" value="<?php echo $basic_details['user_id'];?>">
            <input type="hidden" name="lobby_id" value="<?php echo $basic_details['lobby_details']['id'];?>">
            <input type="hidden" name="spectate_price" value="<?php echo $basic_details['lobby_details']['spectate_price'];?>">
            <input type="hidden" name="total_balance" value="<?php echo $this->session->userdata('total_balance');?>">
           
            <h3 class="text-center">You are Entering a Live Event to see a multi Stream of different view points with different Celebrities/Models/Pro Streamers etc..</h3>
           
            <h3 class="text-center">You have Total Balance of $<?php echo number_format((float)$this->session->userdata('total_balance'), 2, '.', ''); ?></h3><h3 class="text-center"> Spectate Amount is $<?php echo $basic_details['lobby_details']['spectate_price'].' Per Ticket '; ?></h3>
            <div class="btn-group " style="margin:30px 0px;">
                <button type="submit" class="event_btn animated-box text-center" value="Submit">Confirm</button>
            </div>   
         </form>
         <?php } else { ?>
               <h3 class="text-center">You have Insufficient Wallet Balance So Please Add Money in Your Wallet.</h3>
               <h3 class="text-center">You have Total Balance of $<?php echo $this->session->userdata('total_balance'); ?></h3>
               <h3 class="text-center">Spectate Require Amount of $<?php echo $basic_details['lobby_details']['spectate_price']; ?></h3>
               <div class="btn-group" style="margin:30px 0px;">
                  <a href="<?php echo base_url('points/buypoints');?>" class="event_btn animated-box text-center">Add Money</a>
               </div>
         <?php }  ?>
      </div>
      <div class="col-lg-2"></div>
</div>
     
</section>
<script type="text/javascript">
   
</script>


