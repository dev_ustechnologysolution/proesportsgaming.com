<section class="login-signup-page">
  <div class="container">
      <div class="row">
          <div class="login-signBlock">
              <h2>ENTER YOUR REGISTERED EMAIL</h2>
                <p class="login-box-msg"><?php if(isset($err)) echo $err ;?></p>
                <form action="<?php echo base_url().'user/forgot_password'; ?>" method="post">
                  
                    <div class="form-group">
                      <label class="col-lg-4 col-sm-4 col-md-4">Email Address</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                          <input required type="email" class="form-control" name="email">
                        </div>
                    </div>
                    
                    <div class="btn-group">
                      <input type="submit" value="Submit" class="btn-submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

