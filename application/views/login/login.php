<?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
<section class="login-signup-page">
	<div class="container">
    	<div class="row">
        	<div class="login-signBlock">
                <h2>ENTER YOUR LOGIN</h2>
                <?php if(isset($err)) echo $err; ?>
                <label style="margin-left: 255px;color: #ff5000;"><?php echo $this->session->flashdata('message_succ');?></label>
                <form role="form" class="signup-form" action="<?php echo base_url().'login'; ?>" method="post">
                    <div class="form-group">
                    	<label class="col-lg-4 col-sm-4 col-md-4">Email Address</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                        	<input required autocomplete="off" type="email" class="form-control" name="email">
                        </div>
                    </div>
                    <div class="form-group">
                    	<label class="col-lg-4 col-sm-4 col-md-4">Password</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                        	<input required autocomplete="off" type="password" class="form-control" name="password">
                        </div>
                    </div>
                    <div class="btn-group">
                    	<input type="submit" value="Sign in" class="btn-submit">
                    </div>

                    <div id="giveaway_modal" class="modal custmodal giveaway_modal" style="display: none;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <span class="close">&times;</span>
                                <div class="text-left">
                                    <h1 class="giveaway_div"><span class="modal_h1_msg">Giveaway Detail</span></h1>
                                    <hr>
                                </div>
                                <div>
                                </div>
                                <h3>
                                </h3>
                            </div>
                        </div>
                    </div>
                </form>
                <p><span><a href="<?php  echo base_url().'user/forgot_password'; ?>">Forgot Password?</a></span> |  <span>New User? <a href="<?php echo base_url().'signup' ?>"> Signup</a></span></p>
            </div>
        </div>
    </div>
</section>