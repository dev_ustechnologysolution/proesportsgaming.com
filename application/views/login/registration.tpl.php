<section class="login-signup-page">
	<div class="container">
    	<div class="row">
        	<div class="login-signBlock">
            	<h2>REGISTER</h2>
				<label style="margin-left:193px; color:#ff5000;"><?php if(isset($msg)) echo $msg;?></label>
				<label style="margin-left:0px; color:#ff5000;"><?php echo $this->session->flashdata('message_err');?></label>
                <label style="margin-left:193px; color:#ff5000;"><?php echo $this->session->flashdata('msg');?></label>
                <form role="form" action="<?php echo base_url(); ?>signup/register" name="myForm" class="signup-form" method="POST" enctype="multipart/form-data" onsubmit="return getAge()">
                    <div class="form-group">
                        <label class="col-lg-4 col-sm-4 col-md-4"><small>**</small> First Name</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8 first_name">
                            <input required onkeyup="check_valid_name(this.value,'first_name');" type="text" class="form-control" name="name">
                        </div>
                        <div id="errors" style="margin-left: 255px;color: red;"></div>
                    </div>
                    <div class="form-group">
                    	<label class="col-lg-4 col-sm-4 col-md-4"><small>**</small> Last Name</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8 last_name">
                        	<input required onkeyup="check_valid_name(this.value,'last_name');" type="text" class="form-control" name="lname">
                        </div>
                        <div id="errorss" style="margin-left: 255px;color: red;"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 col-sm-4 col-md-4">Display Name</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8 display_name">
                            <input onkeyup="check_valid_name(this.value,'display_name');" type="text" class="form-control" name="display_name">
                        </div>
                        <div id="errorss" style="margin-left: 255px;color: red;"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 col-sm-4 col-md-4">Discord Name & Tag#</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                            <input  type="text" class="form-control" name="discord_id">
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 col-sm-4 col-md-4">Team Name</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8 team_name">
                            <input onkeyup="check_valid_name(this.value,'team_name');" type="text" class="form-control" name="team_name">
                        </div>
                        <div id="errorss" style="margin-left: 255px;color: red;"></div>
                    </div>
                    <div class="form-group">
                    	<label class="col-lg-4 col-sm-4 col-md-4"><small>**</small> Email Address</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                        	<input required type="email" class="form-control" name="email" id="user_email">
                        </div>
                        <div id="errors1" style="margin-left: 255px;color: red;"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4"><small>**</small> Mail Notifications</label>
                        <div class="input-group col-lg-8 notifications_div">
                            <div class="col-lg-4">
                                <div class="title">Friends Mail</div>
                                <div>
                                    <label class="switch"><input type="checkbox" name="friends_mail" value="friends_mail"><span class="slider round"></span></label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="title">Esports Team Mail</div>
                                <div>
                                    <label class="switch"><input type="checkbox" name="team_mail" value="team_mail"><span class="slider round"></span></label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="title">Challenge Mail</div>
                                <div>
                                    <label class="switch"><input type="checkbox" name="challenge_mail" value="challenge_mail"><span class="slider round"></span></label>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="form-group">
                    	<label class="col-lg-4 col-sm-4 col-md-4"><small>**</small> Password</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                        	<input required type="password" class="form-control" name="password" id="password">
                        </div>
                         <div id="errors2" style="margin-left: 255px;color: red;"></div>
                    </div>
                    <div class="form-group">
                    	<label class="col-lg-4 col-sm-4 col-md-4"><small>**</small> Re-type Password</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                        	<input required type="password" class="form-control" name="confirm_password" id="confirm_password">
                        </div>
                        <div id="errors3" style="margin-left: 255px;color: red;"></div>
                        <span id="message" style="margin-left:1255x;"></span>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 col-sm-4 col-md-4"><small>**</small> Phone Number</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                            <input required type="text" onkeypress='return event.charCode == 45 ||event.key === "Backspace" || (event.charCode >= 48 && event.charCode <= 57)' maxlength="15" class="form-control" name="number" id="number">
                        </div>
                        <div id="errors4" style="margin-left: 255px;color: red;"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 col-sm-4 col-md-4"><small>**</small> Home Address</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                            <input required type="text" class="form-control" name="location" id="pass2">
                        </div>
                        <div id="errors5" style="margin-left: 255px;color: red;"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 col-sm-4 col-md-4"><small>**</small> Select Country</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                            <select required class="form-control" name="country" data-country="US" id="countries_states1">
                                <option value="">Select Country</option>
                                <?php if($country_list > 0) {
                                    foreach ($country_list as $key => $value) {
                                        echo '<option value="'.$value['country_id'].'">'.$value['countryname'].'</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div id="errors6" style="margin-left: 255px;color: red;"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 col-sm-4 col-md-4"><small>**</small> Select State</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                            <select required class="form-control" name="state" id="state">
                                <option value="">Select State</option>
                            </select>
                        </div>
                        <div id="errors7" style="margin-left: 255px;color: red;"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 col-sm-4 col-md-4"><small>**</small> City Name</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                            <input required type="text" class="form-control" name="city" id="city">
                        </div>
                        <div id="errors9" style="margin-left: 255px;color: red;"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 col-sm-4 col-md-4"><small>**</small> Time Zone</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                            <?php echo $this->My_model->get_alltimezone(); ?>
                        </div>
                        <!-- <div id="errors9" style="margin-left: 255px;color: red;"></div> -->
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 col-sm-4 col-md-4"><small>**</small> Postal Code</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                            <input required type="text" class="form-control" name="zip_code" id="zip_code">
                        </div>
                        <div id="errors8" style="margin-left: 255px;color: red;"></div>
                    </div>
                    <div class="form-group">
                    	<label class="col-lg-4 col-sm-4 col-md-4"><small>**</small> Date of Birth <br></label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                            <input onBlur="getAge()" type="text" placeholder="MM/DD/YYYY" id="date" autocomplete="off" class="form-control only_datepicker" name="date" required="required" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 col-xs-12 col-sm-4 col-md-4">Image</label>
                        <div class="col-lg-8 col-sm-8 col-md-8 form-upload">
                            <div class="file-upload">
                                <div class="file-select">
                                    <div class="file-select-button" id="fileName">Choose File</div>
                                    <div class="file-select-name" id="noFile">No file chosen...</div> 
                                    <input type="file" name="image" id="chooseFile" accept="image/*">
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-lg-4 col-sm-4 col-md-4">&nbsp;</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                            <div class="age_overeighteen">
                                <label class="checkbox_container disabled"><input type="checkbox" checkedclass="text-right" id="under_18" name="is_18" value="0"><span class="checkmark"></span> Under 18 </label>
                                <label class="checkbox_container disabled"><input type="checkbox" checkedclass="text-right"  id="above_18" name="is_18" value="1"><span class="checkmark"></span> Above 18</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                    	<label class="col-lg-4 col-sm-4 col-md-4">&nbsp;</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                        	<!-- <div id="noclick">
                                <div class="slcheck w50">
                                    <input id="under_18" type="radio" name="is_18" value="0">You are under 18yrs of age.
                                </div>
                                <div class="slcheck w50">
                                    <input type="radio" id="above_18" name="is_18" value="1">You are above 18yrs of age.
                                </div>
                            </div> -->
                            <div class="accept_policydiv">
                                <label class="checkbox_container accept_policy_checkbox_con "><input required type="checkbox" name="checkbox"><span class="checkmark"></span> I accept the <a target="_blank"  id="policy_link" href="<?php echo base_url('policy');?>" >Policy, Rules & Terms.</a> </label>
                            </div>
                            <!-- <div  class="slcheck">
                                <input required type="checkbox" name="checkbox" >I accept the <a target="_blank"  id="policy_link" href="<?php // echo base_url('policy');?>" >Policy, Rules & Terms.</a>
                            </div> -->
                        </div>
                    </div>
                    <div class="btn-group">
                    	<input id="signup_btn" type="submit" value="Sign up" class="btn-submit">
                    </div>
                </form>
                <p>Already a User? <a href="<?php echo base_url().'login' ?>">Sign in.</a></p>
            </div>
        </div>
    </div>
</section>
<style>
    input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button { 
      -webkit-appearance: none; 
      margin: 0; 
    }
    #date_error {
       margin: 4px 0px;
        font-size: 15px;
        text-align: start; 
    }
    #error{
      color: red;  
    }
    #success{
        color: green;
    }
    .w50 {
        width: 48%;
        display: inline-block;
    }   
</style>
<script>
    function check_valid_name(name,type){
        var res_name = name.toLowerCase();
        var final_name = res_name.replace(/\s+/g, '-');
        if(final_name == 'pro-esports-gaming' || final_name == 'pro-esports' || final_name == 'proesports' || final_name == 'proesportsgaming' || final_name == 'esportsteam' || final_name == 'esports-team'){
            if(type == 'first_name'){
                $('.first_name').find('input:text').val('').append('<label class="display_label">You dont use '+type.replace('_', ' ')+' as Pro Esports Gaming word.</label>');
            } else if( type == 'last_name') {
                $('.last_name').find('input:text').val('').append('<label class="team_label">You dont use '+type.replace('_', ' ')+' as Pro esports gaming word.</label>');
            } else if(type == 'team_name') {
                $('.team_name').find('input:text').append('<label class="team_label">You dont use '+type.replace('_', ' ')+' as Pro esports gaming word.</label>');
            } else if(type == 'display_name') {
                $('.display_name').find('input:text').val('').append('<label class="team_label">You dont use '+type.replace('_', ' ')+' as Pro esports gaming word.</label>');
            }
        } else {
            $('.display_label').remove();
            $('.team_label').remove();
        }
    }
    // $(function() {
    //     document.getElementById('noclick').style.pointerEvents = 'none';
    // });
    function getAge() {
        var date = document.getElementById("date").value;
        if(date !="") {
            // var dateString = year+'/'+month+'/'+date;
            var today = new Date();
            var birthDate = new Date(date);
            var age = today.getFullYear() - birthDate.getFullYear();
            var m = today.getMonth() - birthDate.getMonth();
            var da = today.getDate() - birthDate.getDate();
            (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) ? age-- : '';
            (m < 0) ? m +=12 : '';
            (da < 0) ? da +=30 : '';
            $('#above_18, #under_18').prop('checked',false);
            (age > 18 ) ? $('#above_18').prop('checked',true) : $('#under_18').prop('checked',true);
            return true;
        }
    }
</script>



