<?php $this->load->view('admin/common/login_header'); ?>
<body class="hold-transition login-page" style="background:#000;"> 
	<div class="login-box">
      <div class="login-logo">
        <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url().'assets/frontend/images/logo.png'; ?>" width="100%" /></a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg"><?php if(isset($err)) echo $err ;?></p>
        <form action="<?php echo $_SERVER["REQUEST_URI"]; ?>" method="post">
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="New Password" name="new_password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Confirm Password" name="confirm_password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          
          <div class="row">           
            <div class="col-sm-12" align="center">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
              <button type="submit" class="btn text-center btn-primary btn-block btn-flat">Submit</button>
            </div>
            </div>
            <!-- /.col -->
          </div>
        </form>
  
        
       

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
<?php $this->load->view('admin/common/login_footer'); ?>
    
