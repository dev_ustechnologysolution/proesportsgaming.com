<section class="body-middle innerpage">
	<div class="container">
		<div class="row header1">
			<div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
			<div class="col-lg-4 col-sm-4 col-md-4 "><h2>MATCHES</h2></div>
			<div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
		</div>
		<div class="animated-box">
		<div class="row filter-panel home_page_search_tabbing">
			<div class="col-lg-12">
				<div class="form-group">
					<?php if (!empty($challangetDp['data'])) { ?>
						<select name="system" id="system" class="form-control" onchange="game_search()">
              <option class="select_title" value="">Please select a System</span></option>
              <?php foreach($systemList as $val) {  ?>
              	<option value="<?php echo $val['id'];?>">
              		<?php echo $val['category_name'];?>
              	</option>
              <?php } ?>
            </select> 
            <select class="form-control" name="gamesize"  id="gamesize" onchange="game_search()">
            	<option value="">Please select a Gamesize</option>
            	<?php for ($i = 1; $i <= 16; $i++) { ?>
            		<option value="<?php echo $i;?>" class="playstore_gamesize_option">
            			<?php echo '--' . $i . 'V' . $i . '--';?>
            		</option>
            	<?php } ?>
            </select>
            <select name="game" id="game" class="form-control" onchange="game_search()">
            	<option value="">Please select a Game</option>
            </select>
            <select class="form-control" name="subgame" id="subgame" onchange="game_search()">
            	<option value="">Please select a Subgame</option>
            </select>
            <div class="input-group add-on form-control" style="float: right;">
            	<input placeholder="Search Tag/Game Name" name="srch-term" id="srch-term" type="text"  onkeyup="game_search()" onkeydown="game_search()">
            	<span class="glyphicon glyphicon-search form-control-feedback"></span>
            </div>
          <?php } ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row filterGame-row">
		<div class="bidhottest hottestgame matches ">
			<ul>
				<?php if(!empty($Matches['data'])) {
					foreach($Matches['data'] as $k => $val) {  ?>
						<li class="row">
							<div class="col-lg-2 col-sm-2 col-md-2 bidimg padd0">
								<img src="<?php echo base_url().'upload/game/'. $val['game_image'];?>" alt="">
							</div>
							<div class="col-lg-3 col-sm-3 col-md-3 bid-info">
								<h3><?php echo $val['game_name']; ?></h3>
								<p><?php echo $val['sub_game_name']; ?></p>
								<span class="postby">Tag: <?php echo $val['device_id']; ?></span>
							</div>
							<div class="col-lg-2 col-sm-2 col-md-2 uplod-day">
								<h4><?php //echo date("jS F, Y", strtotime($val['created_at'])); ?></h4>
								<?php if($val['game_timer'] != '' && $val['game_timer'] != "0000-00-00 00:00:00") {
									$diff = strtotime($val['game_timer'])- time();
									if (time() < strtotime($val['game_timer'])) { 
									} ?>
									<h3 class="timer custom_timer" data-seconds-left="<?php echo $diff; ?>"></h3>
								<?php } ?>
								<p>Gamesize: <?php echo "--".$val['game_type']."V".$val['game_type']."--";?></p>
								<p class="best_of"> <?php echo ($val['best_of'] != null) ? 'Best of '.$val['best_of'] : ''; ?> </p>
								<?php echo (!empty($val['description']) && $val['description'] != null) ? '<div class="primum-play text-center margin-tpbtm-20 desc_sec_div"><a class="animated-box" target = "_blank" href = "'.base_url().'livelobby/rulesAdd/'.$val['id'].'?type=match" >Rules</a><div class="description" style="display:none;">'.$val['description'].'</div></div>' : ''; ?>
							</div>
							<div class="col-lg-2 col-sm-2 col-md-2 divPrice">
								<div class="game-price premium">
									<h3><?php echo CURRENCY_SYMBOL.$val['price']; ?></h3>
								</div>
							</div>
							<div class="col-lg-1 col-sm-1 col-md-1 ">
								<div class="col-lg-1 col-sm-1 col-md-1">
									<?php echo ($val['cat_img'] != '') ? '<img src="'.base_url().'upload/all_images/'.$val['cat_img'].'" class="img img-responsive game_cat_img">' : ''; ?>
								</div>
							</div>
							<div class="col-lg-2 col-sm-2 col-md-2 primum-play play-n-rating">
								<?php if ($val['game_password']!='' && $val['game_password']!=null) { ?>
									<a href="<?php echo base_url().'home/gamePwAuth/'.$val['id'];?>" class="animated-box">play</a>
								<?php } else { ?> 
									<a href="<?php echo base_url().'home/gameAuth/'.$val['id'];?>" class="animated-box">play</a>
								<?php } ?>
							</div>
						</li>
					<?php }
				} else {
					echo "<h5><div class='row bidhottest_err'><div class='col-sm-12' style='color: #FF5000;'>Hottest Games Not Available</div></div></h5>";
				} ?>
			</ul>
		</div>
	</div>
	<div class="row pager-row">
		<div class="game_paging">
			<?php echo $paging_two; ?>
		</div>	
	</div>
</section>
