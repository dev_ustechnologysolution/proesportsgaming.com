<div class="adm-dashbord">
  <div class="container padd0">
    <?php $this->load->view('dashboard/profile_sidebar_2.tpl.php');?>
    <div class="col-lg-10">
      <div class="col-lg-12">
        <?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
        <?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
        <div class="myprofile-edit membership_div">
          <h2 class="text-center">Pro Esports Player Membership</h2>
          <div class="col-lg-12">
            <div class="row">
              <?php if (!empty($plan)) {
                $date1 = $date2 = 0;
                $plan_expiry_remaining_time = new DateTime(date('Y-m-d H:i:s'));
                if (!empty($user_plan_data) && $active_plan != 0) {
                  switch ($user_plan_data->time_duration) {
                    case 'Day':
                      $plan_expiry_remaining_time = date('Y-m-d H:i:s', strtotime($user_plan_data->payment_date.' +1 day'));
                      break;
                    case 'Week':
                      $plan_expiry_remaining_time = date('Y-m-d H:i:s', strtotime($user_plan_data->payment_date.' +7 days'));
                      break;
                    case 'SemiMonth':
                      $plan_expiry_remaining_time = date('Y-m-d H:i:s', strtotime($user_plan_data->payment_date.' +15 days'));
                      break;
                    case 'Month':
                      $plan_expiry_remaining_time = date('Y-m-d H:i:s', strtotime($user_plan_data->payment_date.' +1 month'));
                      break;
                    case 'Year':
                      $plan_expiry_remaining_time = date('Y-m-d H:i:s', strtotime($user_plan_data->payment_date.' +1 year'));
                      break;
                  }
                  $date1 = new DateTime($plan_expiry_remaining_time);
                  $date2 = $date1->diff(new DateTime(date('Y-m-d H:i:s')));
                }
                foreach ($plan as $key => $sl) {
                  $amount = number_format((float) $sl['amount'], 2, '.', ''); ?>
                  <div class="col-lg-3 mb40">
                    <div class="text-center mb15">
                      <a class="lobby_btn membership_plan" membership_plan_id ="<?php echo $sl['id'];?>">Info</a>
                    </div>
                    <?php
                    $action = ($active_plan == $sl['id']) ? 'Membership/inactiveMembershipPlan' : 'Membership/activeMembershipPlan';

                    ?>
                    <form class="form_membership <?php echo ($active_plan == $sl['id']) ? 'active_plan' : ''; ?>" action="<?php echo base_url().$action; ?>" method="POST">
                      <input type="hidden" name="membership_subscribe_plan" value="<?php echo $sl['id']; ?>">
                      <input type="hidden" name="title" value="<?php echo $sl['title']; ?>">
                      <input type="hidden" name="fees" value="<?php echo $sl['fees']; ?>">
                      <input type="hidden" name="time_duration" value="<?php echo $sl['time_duration']; ?>">
                     <div class="membership_img_div">
                        <img src="<?php echo base_url();?>upload/membership/<?php echo $sl['image']; ?>" alt="<?php echo $sl['image']; ?>" class="img img_margin ">
                      </div>
                      <div class="clearfix subs_btm_bx">
                        <div class="animated-box mb20"></div>
                        <div class="subscrib_plan text-center mb20">
                          <h4><?php echo ucwords($sl['title']); ?></h4>
                        </div>
                        <div class="subscrib_amnt text-center mb20">
                          <h4>Amount : $<?php echo $amount; ?></h4>
                          <input type="hidden" name="membership_subscribe_amount" value="<?php echo $amount; ?>">
                        </div>
                        <div class="subscrib_btn text-center mb20">
                          <?php if (!empty($active_plan) && $active_plan != 0) {
                            if ($active_plan == $sl['id']) { ?>
                              <a class="btn cmn_btn subscribe" onclick="$(this).siblings('.unsubscribe_modal').show()">Unsubscribe</a>
                              <input type="hidden" name="membership_subscribed_plan" value="<?php echo $active_plan; ?>">
                              <input type="hidden" name="membership_payment_id" value="<?php echo $transaction_id; ?>">
                              <div class="modal custmodal unsubscribe_modal" id="unsubscribe_modal" style="display: none;">
                                <div class="modal-content">
                                   <div class="modal-header">
                                      <span class="close">×</span>
                                      <!-- <h1 class="warning_msg">UNSUBSCRIBE PLAN</h1><hr> -->
                                      <h3>Are you sure you want to unsubscribe current plan?</h3>
                                      <h3><input type="submit" name="membership_subscription_opt" value="Yes" class="btn cmn_btn active"> &nbsp;&nbsp;&nbsp; <a class="btn cmn_btn no">No</a></h3>
                                   </div>
                                </div>
                              </div>
                            <?php } else { ?>
                              <a class="btn cmn_btn" onclick="$(this).siblings('.update_subscribe_modal').show()">Subscribe</a>
                              <input type="hidden" name="membership_subscribed_plan" value="<?php echo $active_plan; ?>">
                              <input type="hidden" name="membership_payment_id" value="<?php echo $transaction_id; ?>">
                              <div class="modal custmodal update_subscribe_modal" id="update_subscribe_modal" style="display: none;">
                                <div class="modal-content">
                                   <div class="modal-header">
                                      <span class="close">×</span>
                                      <?php echo "<br><h3 style='line-height: 1.5;'>If you will update the Plan you will lose ".$date2->days." remaining days of your current ' <span style='color:#ff5000;'>".$user_plan_data->title."</span> ' plan. <br>If you want to update plan press Confirm otherwise press cancel for save your 31 remaining days of your current plan</h3>"; ?>
                                      <h3><input type="submit" name="membership_subscription_opt" value="Confirm" class="btn cmn_btn"> &nbsp;&nbsp;&nbsp; <a class="btn cmn_btn no">Cancel</a></h3>
                                   </div>
                                </div>
                              </div>
                            <?php }
                          } else { ?>
                            <input type="submit" name="membership_subscription_opt" value="Subscribe" class="btn cmn_btn">
                          <?php } ?>
                        </div>
                      </div>
                    </form>
                  </div>
                <?php }
              } ?>
              <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
  .img_margin{
    margin:30px 0px;
  }
</style>

