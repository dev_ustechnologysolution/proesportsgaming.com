<?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
<div class="adm-dashbord">
	<div class="container padd0">
		<?php $this->load->view('dashboard/profile_sidebar.tpl.php'); ?>
		<div class="col-lg-10 common_product_list">
			<div class="col-lg-12">
				<?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
				<div class="myprofile-edit row">
					<div class="col-sm-12">
						<h2>My Orders</h2>
					</div>
					<div class="common_product_list my_orders_list col-sm-12">
						<?php if (!empty($order_list)) { ?>
							<ul class="c_product_list text-center col-sm-12">
								<li class="clearfix" style="display: flex; align-items: center; ">
										<div class="col-md-1 text-left"><label>Order ID</label></div>
										<div class="col-md-2 text-left"><label>Tracking ID</label></div>
										<div class="col-md-2 text-left"><label>Payment Method</label></div>
										<div class="col-md-1 text-right"><label>Shipping Fee</label></div>
										<div class="col-md-1 text-right"><label>Wallet Deducted</label></div>
										<div class="col-md-1 text-right"><label>Paypal Deducted</label></div>
										<div class="col-md-1 text-right"><label>Card Deducted</label></div>
										<div class="col-md-1 text-right"><label>Total</label></div>
										<div class="col-md-1 text-left"><label>Status</label></div>
										<div class="col-md-2 text-right"><label>Action</label></div>
									</li>
							</ul>
							<ul class="c_product_list text-center order-ul col-sm-12">
								<?php $i = 1;
								foreach ($order_list as $key => $ol) {
	      							$paymentmethod = ($ol->payment_method == '0') ? 'Wallet' : (($ol->payment_method == '1') ? 'Paypal' : (($ol->payment_method == '2') ? 'Wallet and Paypal' : (($ol->payment_method == '3') ? 'Card' : (($ol->payment_method == '4') ? 'Wallet and Card' : ''))));
									?>
									<li class="c_product_list_li font-18">
										<div class="col-md-1 text-left"><div><?php echo $ol->transaction_id; ?></div></div>
										<div class="col-md-2 text-left"><div><?php echo (!empty($ol->tracking_id)) ? '<button class="trknginfo_clk lobby_btn" attr_tag='.$ol->tracking_id.'>Tracking Info</button>' : '&nbsp;'; ?> </div></div>
										<div class="col-md-2 text-left"><div><?php echo $paymentmethod; ?></div></div>
										
										<div class="col-md-1 text-right"><div>$<?php echo $ol->shipping_charge; ?></div></div>
										<div class="col-md-1 text-right"><div>$<?php echo number_format((float)$ol->wallet_deducted, 2, '.', ''); ?></div></div>
										<div class="col-md-1 text-right"><div>$<?php echo number_format((float)$ol->paypal_deducted, 2, '.', ''); ?></div></div>
										<div class="col-md-1 text-right"><div>$<?php echo number_format((float)$ol->card_deducted, 2, '.', ''); ?></div></div>
										<div class="col-md-1 text-right"><div>$<?php echo $ol->sub_total; ?></div></div>
										<div class="col-md-1 text-left"><div><?php echo ucwords(strtolower($ol->order_status)) ;?></div></div>
										<div class="col-md-2 c_prdct_chckt_btn text-right">
											<a class="cmn_btn view_order" order_status="<?php echo $ol->order_status; ?>"  data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="View <br> Order" id="<?php echo $ol->id; ?>"><i class="fa fa-eye"></i></a>
											<?php if ($ol->order_status == 'placed') { ?>
												<a class="cmn_btn cancel_order" data-order_id="<?php echo $ol->id; ?>" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Cancel <br> Order" payment_method="<?php echo $paymentmethod; ?>"><i class="fa fa-times-circle"></i></a>
											<?php } ?>
										</div>
									</li>
								<?php $i++; } ?>
							</ul>
						<?php } else { ?>
							<div class="emptyitem_lable"><div class='row bidhottest_err'><div class='col-sm-12' style='color: #FF5000;'>No orders available</div></div></div>
							<br>
							<div class="text-center"><a href="<?php echo base_url() ?>store" class="cmn_btn" style="font-size: 20px;"> Go to Store</a></div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
