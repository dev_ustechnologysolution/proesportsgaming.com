<section class="login-signup-page">
	<div class="container">
    	<div class="row">
        	<div class="login-signBlock pay-failed">
				<h2>Failure Page <span><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></span></h2>
				<h4>payment failure due to some reason</h4>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
   window.setTimeout(function(){
    window.location.href = "<?php echo base_url().'dashboard' ?>";
	}, 3000);
</script>