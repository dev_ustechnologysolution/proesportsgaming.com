<div class="adm-dashbord">
	<div class="container padd0">
		<?php $this->load->view('dashboard/profile_sidebar.tpl.php');?>
    <?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
		<div class="col-lg-10">
      <?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
			<div>
			  <div class="row bidhottest mygamelist">
          <h2>Packages</h2>
          <ul>
            <?php foreach ($packages as $value) { ?>
              <li class="clearfix">
								<div class="col-lg-12 col-sm-6 col-md-3 bid-info">
									<h3><?php print_r($value['point_title']);?></h3>
									<p><?php print_r($value['point_description']);?></p>
								</div>
								<div class="col-lg-4 col-sm-4 col-md-2 divPrice">
									<div class="game-price">
										<h3>Amount : $<?php print_r($value['amount']);?></h3>
									</div>
								</div>
								<div class="col-lg-4 col-sm-4 col-md-2 divPrice">
									<div class="game-price">
										<!-- <h3>Points :<?php print_r($value['points']);?></h3> -->
                  </div>
								</div>
								<div class="col-lg-4 col-sm-4 col-md-2 divPrice">
									<div class="game-status">
										<div class="col-lg-5 col-sm-5 col-md-3 game-action" style="padding:0;">
                      <a href="<?php echo base_url()?>points/payment/<?php echo $value['id'];?>/<?php echo $value['points'];?>/<?php echo $value['id'];?>" >Proceed</a>
                    </div>
									</div>
								</div>
              </li>
            <?php } ?>
          </ul>
        </div>
        <h3>Purchase Credit </h3>
        <form action="<?php echo base_url('points/payment/dpayment/amount/0');?>" method="POST">
          <div class="row">
            <label class="col-lg-2">Amount ($)</label>
					  <div class="col-lg-3 text-center">
              <div class="input-group ">
                <input type="text" class="form-control" name="amount" id="amount" required="required" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46" >
              </div>
            </div>
            <div class="col-lg-2">
              <div class="btn-group">
                <div class="col-lg-12 pull-right padd0">
                  <!-- <input type="submit" value="submit" id="point_add" class="btn-update">-->
                  <input type="hidden" name="percentage" value="<?php echo $admin_points_percentage[0]['point_percentage'];?>">
                  <button class="btn btn-primary" id="point_add" type="submit">Submit</button>
                </div>
              </div>
            </div>
            <div class="col-lg-5">&nbsp;</div>
          </div>
          <div class="row text-center">
            <div class="col-lg-7 game-price_error">
              <span style="color:red" class="am_error"> <?php if(@$this->session->flashdata('min_msg')) { echo 'Note:'.@$this->session->flashdata('min_msg'); } ?></span>
              <!--<span style="color:red">Note: Minimum of $5</span>-->                    
            </div>
          </div>
          <div class="row purchase_credit_fee_lable">
            <div class="col-lg-2"></div>
            <div class="col-lg-3" style="padding-top: 5px;font-size: initial;">Create Your Own Package</div>
            <div class="col-lg-5"></div>
            <!-- <?php
              // foreach ($manual_points_fee as $key => $mpf) { 
              //   $fee_lbl = ($mpf['fee_calc_type'] == '$')? $mpf['fee_calc_type'].$mpf['fee_value'] : $mpf['fee_value'].$mpf['fee_calc_type'] ;
              //   $fee_lbl = $fee_lbl .' Fee amount will be added for '.$mpf['fee_range'];
              //   echo '<p>'.$fee_lbl.'</p>';
              // } 
              ?> -->
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function isNumberKey(evt,id) {
    if (evt.keyCode >= 96 && evt.keyCode <= 105) {
      var val = $('#amount').val();
      if (val == '.') {
      } else {
        if(isNaN(val)){
          val = val.replace(/[^0-9\.]/g,'');
          if(val.split('.').length>1)
            val =val.replace(/\.+$/,"");
        }
      }
      $('#amount').val(val);
    } else {
      $('#amount').val("");
    }
  }
  $('#amount').on('blur focusout', function(){
    var val = $('#amount').val();
    if (val >= 10) {
      $('#amount').val(val);
      $('.game-price_error .am_error').html("");
    } else {
      $('#amount').val("");
      $('.game-price_error .am_error').html("Note:The Miniumum Amount is $10");
    }
  });
</script>      

