<div class="adm-dashbord">
	<div class="container padd0">
		<?php $this->load->view('dashboard/profile_sidebar.tpl.php');
		?>
		<div class="col-lg-10">
			<?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
				<div>
					<div class="row bidhottest mygamelist">
						<h2>Cash Available : $<?php echo $total_balance;?> </h2>
					</div>
					<div class="row bidhottest mygamelist">
						<h2>History of Purchases:</h2>
						<ul>
							<?php foreach($paymentsHistory as $payment){?>
								<li class="clearfix">
									<div class="col-lg-12 col-sm-6 col-md-3 bid-info">
										<h3><?php echo $payment['point_title']?></h3>
										<p><?php echo $payment['point_description']?></p>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-2 divPrice">
										<div class="game-price">
											<h3>Amount : $<?php echo $payment['amount']?></h3>
										</div>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-2 divPrice">
										<div class="game-price"></div>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-2 divPrice">
										<div class="game-status">
											<h4>Purchase Date : <?php echo $this->User_model->custom_date(array('date' => $payment['payment_date'],'format' => 'Y-m-d')); ?></h4>
										</div>
									</div>
								</li>
							<?php } ?>
						</ul>
					</div>
				</div>
		</div>
	</div>
</div>
