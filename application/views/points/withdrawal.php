<div class="adm-dashbord">
	<div class="container padd0">
		<?php $this->load->view('dashboard/profile_sidebar.tpl.php');?>
		<?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
		<div class="col-lg-10">
			<?php
			$disabled_class = "";
			$read_only = "";
			if ($request_pending == 1) {
				$disabled_class = "disabled";
				$read_only = "readonly";
			} ?>
			<div class="col-lg-12">
				<?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
				<div>
					<h5><?php $this->load->view('common/show_message') ?> </h5>
					<div class="row bidhottest mygamelist">
						<h2>Cash Available : $<?php echo $total_balance; ?></h2>
					</div>
					<h2><hr> Withdrawal </h2>
					<form role="form" action="<?php echo base_url().'points/withdrawl_request';?>" class="withdraw_request_form" method="POST" enctype="multipart/form-data">
						<div class="row has-error withdrawal_amount_err_div" style="display: none;">
							<div class="col-lg-12">
								<span class="help-error withdrawal_amount_error"></span>
							</div>
						</div>
						<div class="row">
							<label class="col-lg-2">Cash Amt.</label>
							<div class="col-lg-2">
								<div class="input-group">
									<!-- <input type="text" required class="form-control <?php echo $disabled_class; ?>" name="withdraw_points" id="withdraw_points" onkeyup="return isNumberKey(event,this.id)" <?php echo $read_only; ?>> -->
									<input type="text" required class="form-control <?php echo $disabled_class; ?>" name="withdraw_points" id="withdraw_points" data-error-msg="Please Enter Withdraw Amount" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46"  <?php echo $read_only; ?>>
								</div>
								</span>
							</div>
							<div class="col-lg-2">
								<div class="btn-group">
									<div class="col-lg-12">
										<input type="hidden" name="remaining_point" value=" <?php echo $total_balance; ?>">
										<input type="submit" id="point_add" value="Submit" class="btn-update <?php echo $disabled_class; ?>" <?php echo $disabled_class; ?>>
									</div>
								</div>
							</div>
							<div class="col-lg-6">&nbsp;</div>
						</div>
						<br><br>
						<!-- <div class="form-group"><br>
							[NOTE:  Please Confirm your Profile e-mail is the same as your Paypal.<br>Request takes 24/72 Hours and you can only make 1 request at a time.<br>Also is a Minimum $20 Dollar Withdrawal.   ]
						</div>
						<div class="form-group">
						[NOTE:  Fees are $5 Dollars under $50. Over $50 is 10%.   ]
						</div> -->
						<div class="form-group">
							<p><span style="font-size: 19px; font-weight: bold; color: #ff5000;">PLEASE CONFIRM</span> Your Profile E-Mail is the SAME as your PayPal E-Mail.</p>
							<p>Request takes 72hrs. Working Weekdays.</p>
							<p>You can only make 1 request at a time.</p>
							<p>A $20 Dollar Minimum Withdrawal.</p>

							<p><span style="font-weight: bold; color: #ff5000;">FEE"S</span> are $5 Dollars under $50. Over $50 is 10%</p>
							<p><span style="font-weight: bold; color: #ff5000;">PayPal</span> also Charges Processing Fee</p>
							<p><span style="font-weight: bold; color: #ff5000;">FEE's</span> are subject to change.</p>
							<p>PRO ESPORTS GAMiNG is a WORLD WiDE COMPANY</p>
						</div>	
					</form>
					<div class="row bidhottest mygamelist">
						<h2>History of Withdrawal</h2>
						<ul>
							<?php foreach ($withdraw_request as $key => $value) { ?>
								<li class="clearfix">
									<div class="col-lg-12 col-sm-6 col-md-3 bid-info">
									<div class="col-sm-6">
										<h3>Cash Requested : $ <?php echo $value['amount_requested']?></h3>
										<p></p>
									</div>
									<?php if($value['status'] == 0){ ?>
										<div class="col-sm-6 pull-right">
											<h3><a href="<?php echo base_url().'admin/Game/remove_withdrawal_user/'.$value['id'];?>" style="text-decoration: none; color: white;"><button class="btn" style="background-color: #ff5000;">Remove</button></a></h3>
											<p></p>
										</div>
				                  	<?php } ?>
				                  </div>
				                  <div class="col-lg-4 col-sm-4 col-md-2 divPrice">
				                      <div class="game-price">
				                          <h3>Request Date : <?php echo $value['req_date']?></h3>
				                      </div>
				                  </div>
				                  <div class="col-lg-5 col-sm-4 col-md-2 divPrice">
				                      <div class="game-price"></div>
				                  </div>
				                  <div class="col-lg-3 col-sm-4 col-md-2 divPrice">
				                      <div class="game-price">
										<h3>Status : <?php if($value['status'] == 0){
												echo 'Due';
											} else if($value['status'] == 1) {
												echo 'Completed';
											} ?>
										</h3>
                                    </div>
                                </div>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function validateForm(){
		var withdraw_amount = $('#withdraw_points').val();
		var total_balance = '<?php echo $total_balance; ?>';
		if (total_balance < withdraw_amount){
			$('.withdraw_request_form .has-error.withdrawal_amount_err_div .withdrawal_amount_error').html("You have insufficient balance for withdraw money");
			$('.withdraw_request_form .has-error.withdrawal_amount_err_div').show();
			$('#withdraw_points').val("");
			return false;
		} else {
			$('.withdraw_request_form .has-error.withdrawal_amount_err_div .withdrawal_amount_error').html("");
			$('.withdraw_request_form .has-error.withdrawal_amount_err_div').hide();
			$('#withdraw_points').val("");
			return true;
		}
	}
	function isNumberKey(evt,id) {
		if (evt.keyCode >= 96 && evt.keyCode <= 105) {
			var val = $('#withdraw_points').val();
			if (val == '.') {
			} else {
				if(isNaN(val)){
					val = val.replace(/[^0-9\.]/g,'');
					if(val.split('.').length>1)
						val =val.replace(/\.+$/,"");
				}
			}
			$('#withdraw_points').val(val);
		} else {
			$('#withdraw_points').val("");
		}
	}
	$('#withdraw_points').not('#withdraw_points.disabled').on('focusout', function(){
		var val = $('#withdraw_points').val();
		var total_balance = '<?php echo $total_balance; ?>';
		var withdraw_points = parseFloat(val).toFixed(2);
		

		if (withdraw_points < 20) {
			$('.withdraw_request_form .has-error.withdrawal_amount_err_div .withdrawal_amount_error').html('The Miniumum Amount is $20 for Withdrawal');
			$('.withdraw_request_form .has-error.withdrawal_amount_err_div').show();
			$('#withdraw_points').val("");
			
		} else if(withdraw_points > parseFloat(total_balance)){
			$('.withdraw_request_form .has-error.withdrawal_amount_err_div .withdrawal_amount_error').html("You have insufficient balance for withdraw money");
			$('.withdraw_request_form .has-error.withdrawal_amount_err_div').show();
			$('#withdraw_points').val("");
		} else {
			$('#withdraw_points').val(val);
			$('.withdraw_request_form .has-error.withdrawal_amount_err_div .withdrawal_amount_error').html('');
			$('.withdraw_request_form .has-error.withdrawal_amount_err_div').hide();
			// $('.withdraw_request_form .has-error.withdrawal_amount_err_div .withdrawal_amount_error').html('The Miniumum Amount is $20 Withdrawal');
			// $('.withdraw_request_form .has-error.withdrawal_amount_err_div').show();
			// $('#withdraw_points').val("");
		}
	});
</script>
