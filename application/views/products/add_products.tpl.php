<?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
<div class="adm-dashbord">
  <div class="container padd0">
    <?php $this->load->view('dashboard/profile_sidebar.tpl.php'); ?>
    <div class="col-lg-10 common_product_list add_selling_product">
      <div class="col-lg-12">
        <?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
        <div class="myprofile-edit row">
          <div class="col-sm-12 wishlist_product_list">
            <h2>Add Product </h2>
            <div class="product_info row">
              <form action="<?php echo base_url(); ?>products/selling_product_insert" method="post" enctype="multipart/form-data" class="add_sllngprdct_form">
                <div class="addproduct_tabs">
                  <div class="col-sm-2">
                    <div class="prdct_nav_opt">
                      <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item active"> <a class="nav-link active" id="pills-basic_details-tab" data-toggle="pill" href="#pills-basic_details" role="tab" aria-controls="pills-basic_details" aria-selected="true">Basic</a></li>
                        <li class="nav-item"> <a class="nav-link" id="pills-price_details-tab" data-toggle="pill" href="#pills-price_details" role="tab" aria-controls="pills-price_details" aria-selected="false" ct-rl="#pills-basic_details" tg-rl="#pills-price_details">Price</a></li>
                        <li class="nav-item"> <a class="nav-link" id="pills-attributes_details-tab" data-toggle="pill" href="#pills-attributes_details" role="tab" aria-controls="pills-attributes_details" aria-selected="false" ct-rl="#pills-price_details" tg-rl="#pills-attributes_details">Attributes</a></li>
                        <li class="nav-item"> <a class="nav-link" id="pills-image_details-tab" data-toggle="pill" href="#pills-image_details" role="tab" aria-controls="pills-image_details" aria-selected="false" ct-rl="#pills-attributes_details" tg-rl="#pills-image_details">Image</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-sm-10 tab-content" id="pills-tabContent">
                    <input type="hidden" name ="type" value="<?php echo $_GET['type'];?>">
                    <input type="hidden" id="addproduct" value="1">
                    <div class="section_div clearfix tab-pane fade active in" id="pills-basic_details" role="tabpanel" aria-labelledby="pills-basic_details-tab">
                      <h4 class="col-sm-12">Product Basic Details</h4>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Title:</label>
                          <input type="text" name="product_name" id="product_name" class="form-control" placeholder="Product Title" required>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label>Select Main Category:</label>
                          <!-- category_list -->
                          <select required class="form-control"  name="category_id" id="category_id">
                            <option value="">Please Select Main</option>
                            <?php foreach ($category_list as $key => $value) { ?>
                              <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label>Select Sub Category:</label>
                          <select required class="form-control" name="subcategory_id" id="sub_cat">
                            <option value="">Please Select Sub</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label>Description:</label>
                          <textarea required placeholder="Please enter product Description" class="form-control" rows="4" name="description" style="resize: none;"></textarea>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group">
                          <a class="lobby_btn pull-right next_button" ct-rl="#pills-basic_details" tg-rl="#pills-price_details">Next</a>
                        </div>
                      </div>
                    </div>
                    <div class="section_div clearfix tab-pane fade" id="pills-price_details" role="tabpanel" aria-labelledby="pills-price_details-tab">
                      <h4 class="col-sm-12">Product Price Details</h4>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Price:</label>
                          <input required onkeypress="return (event.charCode >= 48 &amp;&amp; event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46" type="text" name="price" class="form-control" placeholder="Please Enter Price">
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Qty:</label>
                          <input required type="number" name="qty" class="form-control" placeholder="Please Enter Qty">
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group">
                          <a class="lobby_btn pull-left prev_button" ct-rl="#pills-price_details" tg-rl="#pills-basic_details">Prev</a>
                          <a class="lobby_btn pull-right next_button" ct-rl="#pills-price_details" tg-rl="#pills-attributes_details">Next</a>
                        </div>
                      </div>
                    </div>
                    <div class="section_div clearfix product-attr tab-pane fade" id="pills-attributes_details" role="tabpanel" aria-labelledby="pills-attributes_details-tab">
                      <h4 class="col-sm-12">Product Attributes Details</h4>
                      <div class="col-sm-6">
                        <div class="form-group col-md-4">
                          <?php if($_GET['type'] != 1) { ?>
                            <label>Upload Game/Music</label>
                            <small>[ Upload only .zip,.rar,.7zip file ]</small>
                            <input type="file" class="form-control" accept=".zip,.rar,.7zip" name="game_file" required />
                          <?php } ?>
                        </div>
                      </div>
                      <?php if($type == 1) { ?>
                        <div class="col-sm-12 padd0">
                          <?php if(count($attribute_list) > 0) {
                            foreach($attribute_list as $key => $v) {
                              $arr_attr = explode('_',$key); ?>
                              <div class="form-group col-md-12">
                                <label class="col-sm-12 padd0"><?php echo ucfirst($arr_attr[0]).' Turned on/off' ; ?></label>
                                <?php foreach($v as $v1) {
                                  if($arr_attr[0] == $v1->title) { ?>
                                    <input name="attr_id[<?php echo $v1->attr_id.'_'.$v1->option_id;?>]" cust-name="<?php echo $arr_attr[0];?>" type="checkbox" id='<?php echo $v1->attr_id.'_'.$v1->option_id; ?>' image_upload="<?php echo ($v1->image_upload == 1) ? 'true' : 'false'; ?>" class='chk-btn' value="<?php echo $v1->option_label; ?>"/>
                                    <label for='<?php echo $v1->attr_id.'_'.$v1->option_id; ?>'><?php echo $v1->option_label; ?></label>
                                  <?php }
                                } ?>
                              </div>
                            <?php }
                          } ?>
                        </div>
                        </br>
                        <div class="col-sm-12">
                          <div class="prdctattr_img_det" id="dynamic_attr"></div>
                        </div>
                        <input type="hidden" name="final_attr" id="final_attr">
                      <?php } ?>
                      <div class="col-sm-12">
                        <div class="form-group">
                          <a class="lobby_btn pull-left prev_button" ct-rl="#pills-attributes_details" tg-rl="#pills-price_details">Prev</a>
                          <a class="lobby_btn pull-right next_button" ct-rl="#pills-attributes_details" tg-rl="#pills-image_details">Next</a>
                        </div>
                      </div>
                    </div>
                    <div class="section_div clearfix prdct_img_det tab-pane fade" id="pills-image_details" role="tabpanel" aria-labelledby="pills-image_details-tab">
                      <h4 class="col-sm-12">Product Images Details</h4>
                      <div class="col-md-12 form-group prdct_imd_upld">
                        <label>Upload Product Images</label>
                        <div class="margin-tpbtm-10">
                          <input type="file" class="form-control upload_file" id="upload_file" name="gallery_image[]" onchange="preview_image('upload_file',2,'image_preview');" multiple="" required>
                          <div id="image_preview">
                            <div class="plcholdr_img">
                              <!-- <img style="background-color: #331c0f;margin-top: 15px;" src="<?php // echo base_url(); ?>assets/frontend/images/placeholder-image.png" alt="smile" class="img img-responsive"> -->
                            </div>
                          </div>
                          <input type="hidden" name="final_img" id="final_img" val="">
                        </div>
                      </div>
                      <div class="col-sm-12 text-center">
                        <a class="lobby_btn pull-left prev_button" ct-rl="#pills-image_details" tg-rl="#pills-attributes_details">Prev</a>
                        <button type="submit" name="submit" value="Submit" class="lobby_btn">Submit</button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
