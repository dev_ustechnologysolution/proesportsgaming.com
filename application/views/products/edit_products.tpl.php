<?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
<div class="adm-dashbord">
  <div class="container padd0">
    <?php $this->load->view('dashboard/profile_sidebar.tpl.php'); ?>
    <div class="col-lg-10 common_product_list add_selling_product">
      <div class="col-lg-12">
        <?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
        <div class="myprofile-edit row">
          <div class="col-sm-12 wishlist_product_list">
            <h2>Edit Product </h2>
            <div class="product_info row">
              <form action="<?php echo base_url(); ?>products/selling_product_update" method="post" enctype="multipart/form-data" class="edit_sllngprdct_form">
                <div class="addproduct_tabs">
                  <div class="col-sm-2">
                    <div class="prdct_nav_opt">
                      <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item active"> <a class="nav-link active" id="pills-basic_details-tab" data-toggle="pill" href="#pills-basic_details" role="tab" aria-controls="pills-basic_details" aria-selected="true">Basic</a></li>
                        <li class="nav-item"> <a class="nav-link" id="pills-price_details-tab" data-toggle="pill" href="#pills-price_details" role="tab" aria-controls="pills-price_details" aria-selected="false" ct-rl="#pills-basic_details" tg-rl="#pills-price_details">Price</a></li>
                        <li class="nav-item"> <a class="nav-link" id="pills-attributes_details-tab" data-toggle="pill" href="#pills-attributes_details" role="tab" aria-controls="pills-attributes_details" aria-selected="false" ct-rl="#pills-price_details" tg-rl="#pills-attributes_details">Attributes</a></li>
                        <li class="nav-item"> <a class="nav-link" id="pills-image_details-tab" data-toggle="pill" href="#pills-image_details" role="tab" aria-controls="pills-image_details" aria-selected="false" ct-rl="#pills-attributes_details" tg-rl="#pills-image_details">Image</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-sm-10 tab-content" id="pills-tabContent">
                    <input type="hidden" id="product_id" name="id" value="<?php echo $product_data[0]['id']; ?>">
                    <input type="hidden" id="userid" name="userid" value="<?php echo $this->session->userdata('user_id'); ?>">
                    <input type="hidden" id="addproduct" value="0">
                    <div class="section_div clearfix tab-pane fade active in" id="pills-basic_details" role="tabpanel" aria-labelledby="pills-basic_details-tab">
                      <h4 class="col-sm-12">Product Basic Details</h4>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Title:</label>
                          <input type="text" id="product_name" class="form-control" name="product_name" placeholder="Please enter product name" value="<?php echo $product_data[0]['name']; ?>" required/>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label>Select Main Category:</label>
                          <!-- category_list -->
                          <select required class="form-control" name="category_id" id="category_id">
                            <option value="">Please Select Category</option>
                            <?php foreach ($category_list as $key => $value) { $arr_category = explode(",", $product_data[0]['category_id']);
                            if($value['parent_id'] == 0) { ?>
                              <option <?php echo ($arr_category[0] == $value['id']) ? 'selected' : '';?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                            <?php } } ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <label>Select Sub Category:</label>
                          <select required class="form-control" name="subcategory_id" id="sub_cat">
                            <option value="">Please Select Category</option>
                            <?php foreach ($category_list as $key => $value) { $arr_category = explode(",", $product_data[0]['category_id']);
                            if($value['parent_id'] == $arr_category[0]) { ?>
                              <option <?php echo ($arr_category[1] == $value['id']) ? 'selected' : '';?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                            <?php } } ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label>Description:</label>
                           <textarea required placeholder="Please enter product Description" class="form-control" rows="4" name="description" style="resize: none;"><?php echo $product_data[0]['description']; ?> </textarea>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group">
                          <a class="lobby_btn pull-right next_button" ct-rl="#pills-basic_details" tg-rl="#pills-price_details">Next</a>
                        </div>
                      </div>
                    </div>
                    <div class="section_div clearfix tab-pane fade" id="pills-price_details" role="tabpanel" aria-labelledby="pills-price_details-tab">
                      <h4 class="col-sm-12">Product Price Details</h4>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Price:</label>
                          <input required onkeypress="return (event.charCode >= 48 &amp;&amp; event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46" type="text" name="price" class="form-control" placeholder="Please Enter Price" value="<?php echo $product_data[0]['price']; ?>">
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Qty:</label>
                          <input required type="number" name="qty" class="form-control" placeholder="Please Enter Qty" value="<?php echo $product_data[0]['qty']; ?>">
                        </div>
                      </div>
                      <!-- <div class="col-sm-4">
                        <div class="form-group">
                          <label>Fee:</label>
                          <input onkeypress="return (event.charCode >= 48 &amp;&amp; event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46" name="fee" type="text" class="form-control" placeholder="Please Enter Fee" required value="<?php // echo $product_data[0]['fee']; ?>">
                        </div>
                      </div> -->
                      <div class="col-sm-12">
                        <div class="form-group">
                          <a class="lobby_btn pull-left prev_button" ct-rl="#pills-price_details" tg-rl="#pills-basic_details">Prev</a>
                          <a class="lobby_btn pull-right next_button" ct-rl="#pills-price_details" tg-rl="#pills-attributes_details">Next</a>
                        </div>
                      </div>
                    </div>
                    <div class="section_div clearfix product-attr tab-pane fade" id="pills-attributes_details" role="tabpanel" aria-labelledby="pills-attributes_details-tab">
                      <h4 class="col-sm-12">Product Attributes Details</h4>
                      <div class="col-sm-6">
                        <!-- <div class="form-group">
                          <label>Is 18 YRS</label>
                          <select name="is_18" class="form-control">
                            <option <?php // echo ($product_data[0]['is_18'] == 0) ? 'selected' : '';?> value="0">No</option>
                            <option <?php // echo ($product_data[0]['is_18'] == 1) ? 'selected' : '';?> value="1">Yes</option>
                          </select>
                        </div> -->
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group col-md-4">
                          <?php if($product_data[0]['type'] != 1) { ?>
                            <label>Upload Game/Music</label><small>[ Upload only .zip,.rar,.7zip file ]</small>
                            <input type="file" class="form-control" accept=".zip,.rar,.7zip" name="game_file"/>
                          <?php } ?>
                        </div>
                      </div>
                      <?php if ($product_data[0]['type'] == 1) { ?>
                        <div class="clearfix">
                          <?php if(count($attribute_list) > 0) {
                            $arr_selected_options = array_column($product_attribute_data, 'option_id');
                            foreach($attribute_list as $key => $v) { $arr_attr = explode('_',$key); ?> 
                              <div class="form-group col-md-12">
                                <label class="col-sm-12 padd0"><?php echo $arr_attr[0]; ?>&nbsp;</label>
                                <?php foreach($v as $v1) {
                                  if($arr_attr[0] == $v1->title) { ?>
                                    <input cust-name="<?php echo $arr_attr[0];?>" name="attr_id[<?php echo $v1->attr_id.'_'.$v1->option_id;?>]" <?php echo (in_array($v1->option_id, $arr_selected_options)) ? 'checked' : '';  ?> type="checkbox" id='<?php echo $v1->attr_id.'_'.$v1->option_id; ?>' image_upload="<?php echo ($v1->image_upload == 1) ? 'true' : 'false'; ?>"  class='chk-btn' value="<?php echo $v1->option_label; ?>"/>
                                    <label for='<?php echo $v1->attr_id.'_'.$v1->option_id; ?>'><?php echo $v1->option_label; ?></label>
                                  <?php } ?>
                                <?php } ?>
                              </div>
                            <?php } ?>
                          <?php } ?>
                        </div>
                      <?php } ?>
                      <div class="col-md-12 prdctattr_img_det" id="dynamic_attr">
                        <?php if (count($attribute_list) > 0) {
                          $arr_selected_options = array_column($product_attribute_data, 'option_id');
                          foreach($attribute_list as $key => $v) {
                            $arr_attr = explode('_',$key); ?> 
                            <?php foreach($v as $v1) {
                              if ($v1->image_upload == 1 && in_array($v1->option_id, $arr_selected_options)){ ?>
                                <div class="form-group chooseimg_single_div" id="remove<?php echo $key; ?>">
                                  <label for='<?php echo $v1->attr_id.'_'.$v1->option_id; ?>'><?php echo $v1->option_label; ?></label> 
                                  <input  multiple type="file" id='<?php echo 'img_'.$v1->attr_id.'_'.$v1->option_id; ?>' name="attr_image[<?php echo $v1->attr_id.'_'.$v1->option_id;?>][]" class='form-control' onchange="preview_image1('<?php echo 'img_'.$v1->attr_id.'_'.$v1->option_id; ?>',1,'image_<?php echo $v1->option_label; ?>');"/>
                                  <div id="image_<?php echo $v1->option_label; ?>" class="image_<?php echo $v1->option_label; ?>">
                                    <?php foreach($product_attribute_data as $option_data) {
                                      if ($option_data['option_id'] == $v1->option_id) {
                                        $arr_image = explode(",", $option_data['option_image']);
                                        $j = 1;
                                        $last_key = end(array_keys($arr_image));

                                        foreach($arr_image as $k => $vimg) {
                                          if($vimg != '') {
                                            $result .= $v1->option_id.'_'.$k.',';
                                             ?>
                                            <div style="display:inline-block;" id="removeimg_<?php echo $v1->option_id.'_'.$k; ?>" class="singleimg_div">
                                              <input style="vertical-align:top;margin-right:10px;" type="hidden" name="is_defult" class="is_defult">
                                              <img style="" src="<?php echo base_url().'upload/products/'.$vimg; ?>">
                                              <span style="cursor: pointer;" class="remove_img" onclick="remove_image1('removeimg_<?php echo $v1->option_id.'_'.$k; ?>',1,'<?php echo $option_data['id']; ?>');">
                                                  <i class="fa fa-trash fa-fw"></i>Remove Image</span>
                                            </div>
                                          <?php }
                                          $j++;
                                        }
                                      }
                                    } ?>
                                  </div>
                                </div>  
                              <?php } ?>
                            <?php } ?>
                          <?php }
                        } ?>
                        <input type="hidden" id="old_attr_image" name="old_attr_image" value="<?php echo $result; ?>">
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group">
                          <a class="lobby_btn pull-left prev_button" ct-rl="#pills-attributes_details" tg-rl="#pills-price_details">Prev</a>
                          <a class="lobby_btn pull-right next_button" ct-rl="#pills-attributes_details" tg-rl="#pills-image_details">Next</a>
                        </div>
                      </div>
                    </div>
                    <div class="section_div clearfix prdct_img_det tab-pane fade" id="pills-image_details" role="tabpanel" aria-labelledby="pills-image_details-tab">
                      <h4 class="col-sm-12">Product Images Details</h4>
                      <div class="col-md-12 form-group prdct_imd_upld">
                        <label>Upload Product Images</label>
                        <div class="margin-tpbtm-10">
                          <input type="file" class="form-control upload_file" id="upload_file" name="gallery_image[]" onchange="preview_image('upload_file',2,'image_preview');" multiple="">
                          <!-- <input type="hidden" name="final_img" id="final_img" val=""> -->
                          <div id="image_preview" class="image_preview">
                            <?php if (count($product_image_data) > 0) { 
                              foreach ($product_image_data as $key => $single_img) {
                                $result1 .= $single_img['id'].','; 
                                ?>
                                <div id="remove_<?php echo $single_img['id'];?>" class="singleimg_div" style=''>
                                  <input style='vertical-align:top;margin-right:10px;' type='hidden' name='is_defult' class='is_defult'>
                                  <img  src='<?php echo base_url().'upload/products/'.$single_img['image']?>'>
                                  <span style="cursor: pointer;" class="remove_img" onclick="remove_image('remove_<?php echo $single_img['id'];?>',2,<?php echo $single_img['id']; ?>);"><i class="fa fa-trash fa-fw"></i>Remove Image</span>
                                </div>
                              <?php }
                            } ?>
                            <input type="hidden" name="old_gallery_image" id="old_gallery_image" value="<?php echo $result1; ?>">
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12 text-center">
                        <a class="lobby_btn pull-left prev_button" ct-rl="#pills-image_details" tg-rl="#pills-attributes_details">Prev</a>
                        <button type="submit" name="submit" value="Submit" class="lobby_btn">Submit</button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function remove_image1(id,type,img_name){
    $.ajax({
      url: base_url + "admin/products/delete_file?id="+img_name+"&type="+type,
      type: 'get',
      success: function (response) {
        $('#'+id).remove();
        location.reload();
      },
    });
  }
  function preview_image1(id,type,upload_div) {
    var total_file=document.getElementById(id).files.length;
    for(var i=0; i < total_file; i++) {
      var file_data = event.target.files[i];
      var form_data = new FormData();

      var get_product_id = $('#product_id').val();
      var guser_id = $('#user_id').val();
      var product_id = (get_product_id != undefined && get_product_id != '') ? get_product_id : 0;
      var user_id = (guser_id != undefined && guser_id != '') ? guser_id : 0;

      form_data.append('file', file_data);
      form_data.append('type', type);
      form_data.append('attr', id);
      form_data.append('product_id', product_id);
      form_data.append('user_id', user_id);

      $.ajax({
        url: base_url + "admin/products/upload_file",
        dataType: 'json', 
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (response) {
          $('#'+upload_div).append("<div style='display:inline-block' id='"+response.id+"' class='singleimg_div'><input style='vertical-align:top;margin-right:10px;' type='hidden' name='is_defult' class='is_defult'><span ><img  style='' src='"+response.name+"'><span style='cursor:pointer;' class='remove_img'  onclick='remove_image("+response.id+","+type+","+response.id+");'><i class='fa fa-trash fa-fw'></i>Remove Image</span></span></div>");
        },
        error: function (response) {
          $('#msg').html(response);
        }
      });
    }
  }
</script>