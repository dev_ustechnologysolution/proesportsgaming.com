<?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
<div class="adm-dashbord">
  <div class="container padd0">
    <?php $this->load->view('dashboard/profile_sidebar.tpl.php'); ?>
    <div class="col-lg-10 common_product_list selling_product_list">
      <div class="col-lg-12">
        <?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
        <div class="myprofile-edit row">
          <div class="col-sm-12 wishlist_product_list">
            <h2>My Selling Products <div class="pull-right"><a href="<?php echo base_url(); ?>Sellingproducts/add_selling_product?type=1" class="lobby_btn">Add Product</a></div></h2>
            <?php  $i = 0; if (!empty($list)) { ?>
              <ul class="c_product_list col-sm-12">
                <li class="clearfix" style="display: flex; align-items: center;">
                  <div class="col-md-1 text-left"><label>Item No</label></div>
                  <div class="col-md-1 text-left"><label>Image</label></div>
                  <div class="col-md-2 text-left"><label>Item Title</label></div>
                  <div class="col-md-3 text-left"><label>Item Description </label></div>
                  <div class="col-md-1 text-left"><label>Quantity</label></div>
                  <div class="col-md-1 text-right"><label>Amount</label></div>
                  <div class="col-md-1 text-right"><label>Fee </label></div>
                  <div class="col-md-2 text-right"><label>Action</label></div>
                </li>
              </ul>
              <ul class="c_product_list order-ul col-sm-12">
                <?php $i = 1;
                foreach($list as $k => $product) {
                  $image_arr = $this->General_model->view_single_row('product_images',array('product_id' => $product->id),'image');
                  ?>
                  <li class="c_product_list_li font-18">
                    <div class="col-md-1 text-left"><div><?php echo $product->id; ?></div></div>
                    <div class="col-md-1 text-left"><div class="text-left"><img width="50" height="60" src='<?php echo base_url()."upload/products/".$image_arr["image"];?>'></div></div>
                    <div class="col-md-2 text-left"><div><span><?php echo (strlen($product->name) > 20) ? substr($product->name,0,20)."..." : $product->name ; ?></span></div></div>
                    <div class="col-md-3 text-left"><div><span><?php echo (strlen($product->description) > 30) ? substr($product->description,0,30)."..." : $product->description; ?></span></div></div>
                    <div class="col-md-1 text-left"><div><span><?php echo $product->qty; ?></span></div></div>
                    <div class="col-md-1 text-right"><div><span>$<?php echo $product->price; ?></span></div></div>
                    <div class="col-md-1 text-right"><div><span>$<?php echo $product->fee; ?></span></div></div>
                    <div class="col-md-2 c_prdct_chckt_btn text-right">
                      <a class="cmn_btn" href="<?php echo base_url().'Sellingproducts/product_edit/'.$product->id; ?>"><i class="fa fa-pencil"></i></a> &nbsp; <a class="cmn_btn delete_prdct_plrstr" product-id="<?php echo $product->id; ?>"><i class="fa fa-trash-o"></i></a>
                      <!-- <a class="cmn_btn view_order" order_status="canceled" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="View <br> Order" id="<?php echo $product->id; ?>"><i class="fa fa-trash-o"></i></a> -->
                    </div>
                  </li>
                <?php } ?>
              </ul>
            <?php } else { ?>
                <div class="emptyitem_lable" style="margin-top:30px;font-size:28px;">Your Selling Products list is Empty</div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>