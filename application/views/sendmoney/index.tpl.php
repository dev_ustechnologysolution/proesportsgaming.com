<div class="adm-dashbord">
	<?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
  <div class="container padd0">
    <?php $this->load->view('dashboard/profile_sidebar.tpl.php'); ?>
    <div class="col-lg-10">
    	<div class="col-lg-11">
    		<?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
    		<div>					
					<div class="row bidhottest mygamelist">
						<h2>Send Money to  Friends / Partners</h2>
						<div class="col-lg-12">
							<div style="color:#FF0000">
								<div class="col-lg-4"></div>
								<div class="col-lg-8">
									<?php $this->load->view('common/show_message') ?>
								</div>
							</div>
							<div class="row">								
								<form role="form" action="<?php echo base_url().'sendmoney/money_request';?>" id="sendmoney_form" class="send-signup-form" method="POST" enctype="multipart/form-data">
									<input type="hidden" id="current_user_account" value="<?php echo $_SESSION['account_no'];?>">
									<input type="hidden" id="current_user_balance" value="<?php echo $_SESSION['total_balance'];?>">
									<div class="first_phase">
										<div class="form-group">
											<label class="col-lg-4">Enter Amt ($)</label>
											<div class="input-group col-lg-8">
												<input type="text" onkeyup="return isNumbKey(event,this.id)" onkeydown="return isNumbKey(event,this.id)"  required class="form-control" name="transfer_amt" id="transfer_amt">
												<p class="error_msg_amt text-start"></p>
											</div>										
										</div>
										<div class="form-group">
											<label class="col-lg-4">Enter Person Account Number.</label>
											<div class="input-group col-lg-8">
												<input type="text" required class="form-control" name="account_number" id="account_number">
												<input type="hidden" class="form-control" name="account_from_number" value="<?php echo $this->session->userdata('account_no'); ?>" id="account_from_number">
												<p class="error_msg text-start"></p>
											</div>
											
										</div>									
										
										<div class="form-group">
											<label class="col-lg-4"></label>
											<div class="input-group col-lg-8">
												<input type="button" id="next_page" value="NEXT" class="btn orange">
											</div>
										</div>
									</div>
									<div class="second_phase none">
									
										<div class="col-lg-12">
											<div class="form-group profile_image_div">
												
											</div>							
											<div class="form-group">
												<div class="col-lg-6">
													<input type="button" id="send_money_back_page" value="BACK" class="btn orange pull-right">
												</div>
												<div class="col-lg-6">
													<input type="button" id="point_add_id" value="SUBMIT" class="btn orange pull-left">
												</div>
											</div>
								   
						 				</div>
									</div>
								</form>
								<div class="clearfix"></div>
								<div class="form-group text-center"><br>
									[Send Money to friends to play or Send your gaming partners their cut]
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
  </div>
</div>
<style>
.none {
	display:none;
}
input#next_page ,input#send_money_back_page ,input#point_add_id {
    color: #fff;
}
input#next_page:hover ,input#send_money_back_page ,input#point_add_id{
	background: #ff0000;
}
</style>