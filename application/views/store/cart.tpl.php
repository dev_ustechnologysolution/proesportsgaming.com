<section class="body-middle innerpage">
  <?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
  <div class="container">
    <div class="row header1">
        <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
        <div class="col-lg-4 col-sm-4 col-md-4"><h2>Cart</h2></div>
        <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
    </div>
    <?php $this->load->view('common/product_filter.tpl.php'); ?>    
    <div class="row">
      <div class="cart_product_listing_div common_product_list">
        <?php $i = 0;
        if (!empty($cart_item)) { ?>
          <form method="post" action="<?php echo base_url(); ?>store/cart_update">
            <div class="cart_list_heading row">
              <div class="col-md-1 head_title"></div>
              <div class="col-md-3 head_title">Product Name</div>
              <div class="col-md-2 head_title">Option</div>
              <div class="col-md-2 head_title">Quantity</div>
              <div class="col-md-1 head_title padd0">Price</div>
              <div class="col-md-1 head_title padd0">Fee</div>
              <div class="col-md-1 head_title padd0">Total</div>
              <div class="col-md-1 head_title">Action</div>
            </div>
            <ul class="c_product_list">
              <?php 
              $SUBTOTAL = $shipping_fee = 0;
              foreach ($cart_item as $key => $ci) { 
                // echo "<pre>"; print_r($ci);
                $shipping_fee = $ci['int_fee'];
                $item_details = $ci;
                $sel_variation = !empty($item_details['sel_variation']) ? explode(', ', $item_details['sel_variation']) : '';
                $ci['prdcts_total_price'] = number_format((float) (($item_details['prdct_qntity'] * $item_details['price']) + $item_details['product_fee']), 2, '.', '');
                $SUBTOTAL += $ci['prdcts_total_price'];
                ?>
                <li class="c_product_list_li">
                  <div class="col-md-1 c_prdct_img">
                    <img src="<?php echo base_url().'upload/products/'.$item_details['select_img']; ?>" alt="<?php echo $item_details['select_img']; ?>" class="img img-responsive">
                    <input type="hidden" name="product_image[]" min="1" value="<?php echo $item_details['select_img']; ?>">
                    <input type="hidden" name="product_id[]" value="<?php echo $item_details['product_id']; ?>">
                  </div>
                  <div class="col-md-3 c_prdct_name">
                    <a href="<?php echo base_url() ?>Store/view_product/<?php echo $item_details['product_id']; ?>"><div><?php echo $item_details['product_name']; ?></div></a>
                    <input type="hidden" name="product_name[]" value="<?php echo $item_details['product_name']; ?>">
                  </div>
                  <div class="col-md-2 c_slct_varation">
                    <?php if (isset($sel_variation) && $sel_variation != '') {
                      foreach ($sel_variation as $k => $sv) {
                        $sv = explode('&', $sv);
                        $var_title = end(explode('_', $sv[0]));
                        $var_opt = end(explode('_', $sv[1]));
                        echo '<div><h4><span class="var_title">'.$var_title.'</span> : <span> '.$var_opt.'</span></h4></div>';
                        echo '<input type="hidden" name="variation_'.$item_details['product_id'].'[]" value="'.$var_title.'_'.$var_opt.'">';
                      }
                    } ?>
                  </div>
                  <div class="col-md-2 c_prdct_item select_prdct_qnt">
                    <button type="button" id="sub" class="sub cmn_btn"><i class="fa fa-minus"></i></button>
                    <input type="number" name="prdct_qntity[]" class="form-control prdct_qntity" min="1" pattern="[0-9]+" required value="<?php echo $item_details['prdct_qntity']; ?>">
                    <button type="button" id="add" class="add cmn_btn"><i class="fa fa-plus"></i></button>
                  </div>
                  <div class="col-md-1 c_prdct_price">
                    <!-- <input type="number" name="prdct_price[]" class="form-control prdct_price" pattern="[0-9]+" required value="<?php echo $item_details['price']; ?>" readonly> -->
                    <lable style="font-size: 22px;"><?php echo '$'.$item_details['price']; ?></lable>
                  </div>
                   <div class="col-md-1 c_prdct_price">
                    <!-- <input type="number" name="prdct_price[]" class="form-control prdct_price" pattern="[0-9]+" required value="<?php echo $item_details['price']; ?>" readonly> -->
                    <lable style="font-size: 22px;"><?php echo '$'.$item_details['product_fee']; ?></lable>
                  </div>
                  <div class="col-md-1 c_prdct_totalprice">
                   <!--  <input type="number" name="prdct_total_price[]" class="form-control prdct_total_price" required value="<?php echo $ci['prdcts_total_price']; ?>" readonly> -->
                     <lable style="font-size: 22px;"><?php echo '$'.$ci['prdcts_total_price']; ?></lable>
                  </div> 
                  <div class="col-md-1 c_prdct_chckt_btn">
                    <a class="cmn_btn" href="<?php echo base_url(); ?>store/remove_product_cart/<?php echo $key; ?>"><i class="fa fa-trash-o"></i></a>
                  </div>
                </li>
              <?php } ?>
            </ul>
            <?php 
            // checkout calc
            $shipping = 0;
            if (!empty($user_detail) && $user_detail['countryname'] != 'United States') {
              $shipping = $shipping_fee;
            } else if (!empty($this->session->userdata('shippaddress')) && $this->session->userdata('shippaddress')['country'] != 'United States') {
              $shipping = $shipping_fee;
            }
            $SHIPPING = number_format((float)$shipping, 2, '.', '');
            $SUBTOTAL = number_format((float)$SUBTOTAL, 2, '.', '');
            $total = number_format((float)($SHIPPING)+($SUBTOTAL), 2, '.', '');
            ?>
            <div class="checkout_div">
              <div class="row cnt_upcartbtn margin0">
                <div class="col-md-8"></div>
                <div class="col-md-4 text-right padd0">
                  <a href="<?php echo base_url(); ?>store" class="cmn_btn"> Continue Shopping </a>
                  <button type="submit" name="submit" class="cmn_btn"> Update Cart </button>
                </div>
              </div>
              <div class="row margin0">
                <div class="col-sm-7"></div>
                <div class="col-sm-5 checkout_calc_div">
                  <div class="title"> <h2>Cart Total</h2> </div>
                  <div class="checkout_calc_data">
                    <table class="table table-responsive">
                      <tr>
                          <th>SUBTOTAL ($)</th>
                          <td><?php echo $SUBTOTAL; ?></td>
                      </tr>
                      <tr>
                          <th>SHIPPING ($)</th>
                          <td><?php echo $SHIPPING; ?></td>
                      </tr>
                      <tr>
                          <th>Total ($)</th>
                          <td><?php echo $total; ?></td>
                      </tr>
                    </table>
                    <div class="checkout_submit_div">
                      <div class="row">
                        <input type="hidden" name="subtotal" value="<?php echo $SUBTOTAL; ?>">
                        <input type="hidden" name="shipping" value="<?php echo $SHIPPING; ?>">
                        <input type="hidden" name="total" value="<?php echo $total; ?>">
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <a href="<?php echo base_url(); ?>checkout" class="cmn_btn">Proceed to Checkout</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        <?php } else { ?>
            <div class="emptyitem_lable">Cart is Empty</div>
            <br>
            <div class="text-center"><a href="<?php echo base_url() ?>store" class="cmn_btn" style="font-size: 20px;"> Go to Store</a></div>
        <?php } ?>
      </div>
    </div>
  </div>
</section>
