<?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
<div class="adm-dashbord">
  <div class="container padd0">
    <?php $this->load->view('dashboard/profile_sidebar.tpl.php'); ?>
    <div class="col-lg-10 common_product_list add_selling_product">
      <div class="col-lg-12">
        <?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
        <div class="myprofile-edit row">
          <div class="col-sm-12 wishlist_product_list">
            <h2>Add Category </h2>
            <div class="product_info row">
              <form  action="<?php echo base_url(); ?>category/<?php echo (isset($category_data)) ? 'category_update' : 'category_insert'; ?>"  method="post" enctype="multipart/form-data" class="add_sllngprdct_form">
                <div class="addproduct_tabs">
                  <div class="col-sm-12">
                    <input type="hidden" value="<?php echo (isset($category_data)) ? $category_data[0]['id'] : ''; ?>" name="id">
                    <input type="hidden" value="<?php echo $type; ?>" name="type">
                    <div class="section_div clearfix" style="padding-top: 20px;">
                      <?php if ($type == 0) { ?>
                        <div class="col-sm-12">
                          <div class="form-group">
                            <label for="name">Select Main Category</label>
                            <select class="form-control" name="parent_id" required>
                              <option value="0">Please Select Category</option>
                              <?php foreach ($list as $key => $value) { 
                                if ($value['parent_id'] == 0) { ?>
                                  <option <?php echo ((isset($category_data)) && ($category_data[0]['parent_id'] == $value['id'])) ? 'selected' : ''; ?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                <?php }
                              } ?>
                            </select>
                          </div>
                        </div>
                      <?php } ?>
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="name"><?php echo ($type == 0) ? 'Sub Category Name' : 'Category Name'; ?></label>
                          <input type="text" required id="name" class="form-control" name="name" value="<?php echo (isset($category_data)) ? $category_data[0]['name'] : ''; ?>"/ placeholder="Enter Category Name">
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group text-center">
                          <button type="submit" name="submit" class="lobby_btn next_button">Submit</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
