<?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
<div class="adm-dashbord">
  <div class="container padd0">
    <?php $this->load->view('dashboard/profile_sidebar.tpl.php'); ?>
    <div class="col-lg-10 common_product_list selling_product_list">
      <div class="col-lg-12">
        <?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
        <div class="myprofile-edit row">
          <div class="col-sm-12 wishlist_product_list">
            <h2 class="row"> <div class="col-sm-8">My Categories </div> <div class="col-sm-4 padd0 text-right"><a href="<?php echo base_url(); ?>category/category_add/1" class="lobby_btn" style="margin-right: 10px; display: inline-block;">Add Category </a><a href="<?php echo base_url(); ?>category/category_add/0" class="lobby_btn" style="display: inline-block;">Add Sub Category </a></div></h2>
            <?php  $i = 0; if (!empty($list)) { ?>
              <ul class="c_product_list col-sm-12">
                <li class="clearfix" style="display: flex; align-items: center;">
                  <div class="col-md-1 text-left"><label>Sl no.</label></div>
                  <div class="col-md-3 text-left"><label>Parent Category</label></div>
                  <div class="col-md-4 text-left"><label>Category Title</label></div>
                  <div class="col-md-2 text-left"><label>Status</label></div>
                  <div class="col-md-2 text-right"><label>Action</label></div>
                </li>
              </ul>
              <ul class="c_product_list order-ul col-sm-12">
                <?php $i = 1;
                foreach($list as $k => $cat) {
                  if($cat['parent_id'] == 0){
                    $main_category['name'] = 'Main Category';
                    $m_category = '';
                    $type = 1;
                  } else {
                    $main_category['name'] = 'Sub Category';
                    $m_category = $this->General_model->view_single_row('product_categories_tbl','id',$cat['parent_id']);
                    $type = 0;
                  }
                  ?>
                  <li class="c_product_list_li font-18">
                    <div class="col-md-1 text-left"><div><?php echo $i++; ?></div></div>
                    <div class="col-md-3 text-left"><?php echo $main_category['name']; ?></div>
                    <div class="col-md-4 text-left"><?php echo ($m_category['name']!= '') ? $m_category['name'].' | '.$cat['name'] : $cat['name']; ?></div>
                    <div class="col-md-2 text-left">
                      <label class="myCheckbox"><input <?php echo ($cat['status'] == 1) ? 'checked' :'';?> id="is_banner<?php echo $k;?>" class="single-checkbox custom-cb" type="checkbox" name="progress" value="<?php echo $cat['id'];?>"><span></span></label>
                    </div>
                    <div class="col-md-2 c_prdct_chckt_btn text-right">
                      <a class="cmn_btn" href="<?php echo base_url().'category/category_edit/'.$type.'/'.$cat['id']; ?>"><i class="fa fa-pencil"></i></a> &nbsp; <a class="cmn_btn delete_plr_cat" cat-id="<?php echo $cat['id']; ?>"><i class="fa fa-trash-o"></i></a>
                    </div>
                  </li>
                <?php } ?>
              </ul>
            <?php } else { ?>
                <div class="emptyitem_lable"><div class='row bidhottest_err'><div class='col-sm-12' style='color: #FF5000;'>Your Category list is Empty</div></div></div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
.myCheckbox input {
  visibility:hidden;
  position: absolute;
  z-index: -9999;
}
.myCheckbox span {
  width: 15px;
  height: 15px;
  display: block;
  background: red;
  border-radius:4px;
}
.myCheckbox input:checked + span {
  background: green;
}
</style>
<script type="text/javascript">
  $(document).ready(function(){
    var $checkboxes = $('.single-checkbox');
    $('.single-checkbox').click(function() {
      var countCheckedCheckboxes = $checkboxes.filter(':checked').length;
      var checked_val = $(this).prop("value");
      var type = ($(this).prop("checked") == true) ? 1 : 0; 
      $.ajax({
        url : '<?php echo site_url(); ?>Category/update_is_status/'+checked_val+'/'+type,
        success: function(result) {                   
            // window.location = window.location;
        }
      });
    });
  });
</script>