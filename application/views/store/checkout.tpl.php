<section class="body-middle innerpage">
  <?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
  <div class="container">
    <div class="row header1">
        <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
        <div class="col-lg-4 col-sm-4 col-md-4"><h2>Checkout</h2></div>
        <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
    </div>
    <?php $this->load->view('common/product_filter.tpl.php'); ?>
    <?php 
    $SUBTOTAL = 0;
    $lgin_act = $lgin_in = $bill_info_act = $bill_info_in = $ch_pmnt_act = $ch_pmnt_in = '';
    if ($this->session->userdata('user_id') =='' && $this->session->userdata('guest_user') =='') {
      $lgin_act = 'active'; $lgin_in = 'in';
    }
    if (($this->session->userdata('billaddress') =='' || $this->session->userdata('shippaddress') =='') && ($this->session->userdata('user_id') !='' || $this->session->userdata('guest_user') !='')) {
      $bill_info_act = 'active'; $bill_info_in = 'in';
    }
    if (($this->session->userdata('checkout_payment') =='') && ($this->session->userdata('billaddress') !='' || $this->session->userdata('shippaddress') !='') && ($this->session->userdata('user_id') !='' || $this->session->userdata('guest_user') !='')) {
      $ch_pmnt_act = 'active'; $ch_pmnt_in = 'in';
    }
    ?>
    <div class="row">
      <div class="checkout_page_div row">
        <?php if (isset($cart_item) && $cart_item !='') { ?>
        <div class="col-sm-9">
          <div class="panel panel-default login_div <?php echo $lgin_act; ?>" id="accordion">
            <form class="form-horizontal frm_lgn_chckt" action="<?php echo base_url(); ?>checkout/login" method="post">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <?php echo ($lgin_act == '') ? '<a> Logged in <span class="pull-right green"><i class="fa fa-check"></i></span></a>' : '<a> Please Login </a>'; ?>
                </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse <?php echo ($this->session->userdata('user_id') !='' || $this->session->userdata('guest_user') !='') ? '':'in'; ?>">
                <div class="panel-body">
                  <div class="col-md-12 padd0">
                    <div class="lginfrmdiv col-sm-7 padd0">
                      <div class="form-group">
                        <label class="col-lg-4 col-sm-4 col-md-4">Email Address</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                          <input autocomplete="off" type="email" class="form-control" name="email" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-4 col-sm-4 col-md-4">Password</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                          <input autocomplete="off" type="password" class="form-control" name="password" required>
                        </div>
                      </div>
                      <div class="form-group text-center">
                        <p><span><a href="<?php echo base_url(); ?>user/forgot_password">Forgot Password?</a></span> | <span>New User? <a href="<?php echo base_url(); ?>signup"> Signup</a></span></p>
                      </div>
                    </div>
                    <div class="gustfrmdiv col-sm-5">
                      <div class="form-group text-center"><span class="cck_or"> OR </span></div>
                      <div class="text-center check_as_guest_div">
                        <label class="check_as_guest_lbl checkbox_container"><input type="checkbox" name="check_as_guest"><span class="checkmark"></span>&nbsp;Checkout as Guest </label>
                      </div>
                    </div>
                    <div class="col-sm-12 chkt-lgnbtn padd0">
                      <div class="form-group">
                        <div class="col-sm-12 text-center">
                          <input type="submit" value="Continue to Billing Information »" class="btn-submit cmn_btn">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class="panel panel-default contact_billing_div <?php echo $bill_info_act; ?>">
            <form class="form-horizontal frm_billing_chckt" action="<?php echo base_url(); ?>checkout/contact_bill_info" method="post" id="checkout-form">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a>Contact and Billing Information</a>
                  <?php echo ($this->session->userdata('billaddress') !='' || $this->session->userdata('shippaddress') !='') ? '<span class="pull-right green" style="margin-left: 10px;"><i class="fa fa-check"></i></span>':''; ?>
                  <a href="<?php echo base_url(); ?>checkout/reset_shippingnfo" class="pull-right change_info">Change</a>
                </h4>
              </div>
              <div id="collapseTwo" class="panel-collapse collapse <?php echo $bill_info_in; ?>">
                <div class="panel-body">
                  <table class="table">
                    <tr>
                      <td style="width: 175px;">
                        <label for="id_email">Email:</label>
                      </td>
                      <td>
                        <input type="email" class="form-control" id="id_email" name="email" required placeholder="Enter email address" value="<?php echo (isset($user_detail['email']) && $user_detail['email'] !='') ? $user_detail['email'] : '';?>" autocomplete="off"/>
                      </td>
                    </tr>
                    <tr>
                      <td style="width: 175px;">
                        <label for="id_first_name">Name:</label></td>
                        <td>
                          <input type="text" class="form-control" id="id_first_name" name="name" required="required" placeholder="Enter name" value="<?php echo (isset($user_detail['name']) && $user_detail['name'] !='') ? $user_detail['name'] : '';?>" autocomplete="off"/>
                        </td>
                    </tr>
                    <tr>
                      <td style="width: 175px;">
                        <label for="id_address_line_1">Address:</label>
                      </td>
                      <td>
                        <textarea class="form-control" rows="3" id="id_address_line_1" name="address" required="required" placeholder="Enter address" autocomplete="off"><?php echo (isset($user_detail['location']) && $user_detail['location'] !='') ? $user_detail['location'] : '';?></textarea>
                      </td>
                    </tr>
                    <tr>
                      <td style="width: 175px;">
                        <label for="id_country">Country:</label>
                      </td>
                      <td>
                        <select id="countries_states1" class="form-control countries_states1" name="country" required tg_attr="billaddress" autocomplete="off">
                          <?php 
                          echo '<option value="">Select Country</option>';
                          foreach ($country_list as $key => $value) {
                            $select = ($value['country_id'] == $user_detail['country'] || $value['country_id'] == $cart_item[0]['country'])? 'selected="selected"' : '';
                            if ($shipping_location_type == 'inside') {
                              if ($value['country_id'] == $cart_item[0]['country']) {
                                echo '<option value="'.$value['countryname'].'" '.$select.' country_id='.$value['country_id'].' country_shortname="'.$value['sortname'].'">'.$value['countryname'].'</option>';
                              }
                            } else {
                              if ($value['country_id'] != $cart_item[0]['country']) {
                                echo '<option value="'.$value['countryname'].'" '.$select.' country_id='.$value['country_id'].' country_shortname="'.$value['sortname'].'">'.$value['countryname'].'</option>';
                              }
                            }
                          } ?>
                        </select>
                        <input type="hidden" name="bill_countrysortname" class="selected_country_shortname">
                      </td>
                    
                    </tr>
                    <tr>
                      <td style="width: 175px;">
                        <label for="id_state">State:</label>
                      </td>
                      <td>
                        <select class="form-control" name="state" id="state" tg_attr="billaddress" required autocomplete="off">
                          <?php 
                          echo '<option value="">Select State</option>';
                          foreach ($state_list as $key => $sl) {
                            $select = ($sl->stateid == $user_detail['stateid'])? 'selected="selected"' : '';
                            echo '<option value="'.$sl->statename.'" '.$select.'>'.$sl->statename.'</option>';
                          } ?>
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td style="width: 175px;">
                        <label for="id_city">City:</label>
                      </td>
                      <td>
                        <input type="text" class="form-control" id="id_city" name="city" required="required" value="<?php echo (isset($user_detail['city']) && $user_detail['city'] !='') ? $user_detail['city'] : '';?>" autocomplete="off"/>
                      </td>
                    </tr>
                    <tr>
                      <td style="width: 175px;">
                        <label for="id_postalcode">Zip Code:</label>
                      </td>
                      <td>
                        <input type="text" class="form-control" id="id_postalcode" name="zip_code" required="required" placeholder="ZIP Code" value="<?php echo (isset($user_detail['zip_code']) && $user_detail['zip_code'] !='') ? $user_detail['zip_code'] : '';?>" autocomplete="off"/>
                      </td>
                    </tr>
                    <tr>
                      <td style="width: 175px;">
                        <label for="id_phone">Phone:</label>
                      </td>
                      <td>
                        <input type="text" class="form-control" id="id_phone" name="phone" required="required" value="<?php echo (isset($user_detail['number']) && $user_detail['number'] !='') ? $user_detail['number'] : '';?>" autocomplete="off"/>
                      </td>
                    </tr>
                    <tr>
                      <td style="width: 175px;"></td>
                      <td>
                        <div class="text-left ship_to_diffadd">
                          <label class="ship_to_diffadd_lbl checkbox_container"><input type="checkbox" name="ship_to_diffadd_check"><span class="checkmark"></span>&nbsp;Ship to different address </label>
                        </div>
                      </td>
                    </tr>
                    <tr class="shipping_hidden" style="display:none;">
                      <td style="width: 175px;">
                        <label for="id_address_line_1">Address:</label>
                      </td>
                      <td>
                        <textarea class="form-control shipping_hidden" style="display:none;" rows="3" id="id_address_line_1" name="shipping_address" placeholder="Enter address" autocomplete="off"></textarea>
                      </td>
                    </tr>
                    <tr class="shipping_hidden" style="display:none;">
                      <td style="width: 175px;">
                        <label for="id_country">Country:</label>
                      </td>
                      <td>
                        <select id="countries_states1" class="form-control shipping_hidden countries_states1" style="display:none;" name="shipping_country" tg_attr="shippingaddress" autocomplete="off">
                          <?php 
                          echo '<option value="">Select Country</option>';
                          foreach ($country_list as $key => $value) {
                            if($shipping_location_type == 'inside'){
                              if($value['country_id'] == $cart_item[0]['country']){
                                echo '<option value="'.$value['countryname'].'" country_id='.$value['country_id'].' country_shortname="'.$value['sortname'].'">'.$value['countryname'].'</option>';
                              }
                            }else{
                              if($value['country_id'] != $cart_item[0]['country']){
                                echo '<option value="'.$value['countryname'].'" country_id='.$value['country_id'].' country_shortname="'.$value['sortname'].'">'.$value['countryname'].'</option>';
                              }
                            }
                          } ?>
                        </select>
                        <input type="hidden" name="shipp_countrysortname" class="selected_country_shortname">
                      </td>
                    </tr>
                    <tr class="shipping_hidden" style="display:none;">
                      <td style="width: 175px;">
                        <label for="id_state">State:</label>
                      </td>
                      <td>
                        <select class="form-control shipping_hidden" style="display:none;" name="shipping_state" id="state" tg_attr="shippingaddress" autocomplete="off">
                          <?php 
                          echo '<option value="">Select State</option>';
                          foreach ($state_list as $key => $sl) {
                            echo '<option value="'.$sl->statename.'">'.$sl->statename.'</option>';
                          } ?>
                        </select>
                      </td>
                    </tr>
                    <tr class="shipping_hidden" style="display:none;">
                      <td style="width: 175px;">
                        <label for="id_city">City:</label>
                      </td>
                      <td>
                        <input type="text" class="form-control shipping_hidden" style="display:none;" id="id_city" name="shipping_city" placeholder="City" autocomplete="off"/>
                      </td>
                    </tr>
                    <tr class="shipping_hidden" style="display:none;">
                      <td style="width: 175px;">
                        <label for="id_postalcode">Zip Code:</label>
                      </td>
                      <td>
                        <input type="text" class="form-control shipping_hidden" style="display:none;" id="id_postalcode" name="shipping_zip_code" placeholder="ZIP Code" autocomplete="off"/>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2">
                        <div class="col-sm-12 chkt-lgnbtn padd0">
                          <div class="form-group">
                            <div class="col-sm-12 text-center">
                              <input type="submit" value="Continue to Payment »" class="btn-submit cmn_btn">
                              <input type="hidden" name="shipping_location_type" value="<?php echo $shipping_location_type ?>">
                            </div>
                          </div>
                        </div>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </form>
          </div>
          <div class="panel panel-default checkout_payment_info <?php echo $ch_pmnt_act; ?>">
            <form action="<?php echo base_url(); ?>checkout/checkoutpay" name="frmPaypal" method="post" id="frmPaypal" class="has-validation-callback">
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a>Payment Information</a>
                  </h4>
              </div>
              <div id="collapseThree" class="panel-collapse collapse <?php echo $ch_pmnt_in ?>">
                <div class="panel-body">
                  <div class="row">
                    <div class="form-group col-sm-12 pyplbtnbrdr">
                      <input type="hidden" name="shipping_fee" value="<?php echo $shipping_fee; ?>">
                      <?php if ($this->session->userdata('user_id') !='') { ?>
                        <div class="row">
                          <div class="col-sm-6">
                              <label class="frm_wl_lbl checkbox_container"><input type="checkbox" name="check_paypal[]" value="from_wallet" checked class="frm_wllt" required><span class="checkmark"></span>&nbsp;From Wallet </label>
                          </div>
                          <div class="col-sm-6">
                            <label class="pull-right"><?php echo '$ '.number_format((float) $this->session->userdata('total_balance'), 2, '.', ''); ?></label>
                          </div>
                        </div>
                      <?php } ?>
                    </div>
                    <div class="form-group col-sm-12 pyplbtnbrdr">
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="">
                            <label class="frm_wl_lbl checkbox_container"><input type="checkbox" id="paypal" name="check_paypal[]" value="pay_with_paypal" class="frm_pypl" <?php echo ($this->session->userdata('user_id') !='') ? '': 'required';?>><span class="checkmark"></span>&nbsp;Pay with Paypal </label>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="">
                            <img src="<?php echo base_url(); ?>upload/paypal_img/paypal-buttons-peg.png" alt="paypal" class="img img-responsive pull-right">
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- paywith card -->
                     <div class="form-group col-sm-12 pyplbtnbrdr">
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="">
                            <label class="frm_wl_lbl checkbox_container"><input type="checkbox" id="card" name="check_paypal[]" value="pay_with_card" class="frm_pypl" <?php echo ($this->session->userdata('user_id') !='') ? '': 'required';?>><span class="checkmark"></span>&nbsp;Pay with Credit/Debit Card </label>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="">
                            <img src="<?php echo base_url().'upload/paypal_img/paypal-buttons-peg3.png';?>"
                         alt="PayPal - The safer, easier way to pay online" class="img img-responsive pull-right">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group col-sm-12 plcordr">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="text-center plc_ordr_div">
                            <input type="submit" value="Place Order" name="submit" class="btn-submit cmn_btn">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="order_summry row">
            <h3>Order Summary</h3>
            <div class="itms">
              <?php if (isset($cart_item) && $cart_item !='') { ?>
                <div class="itmcnt_ordr_summry">
                  <span><?php echo count($cart_item); ?> Item in Cart</span>
                </div>
                <?php
                foreach ($cart_item as $key => $ci) { 
                  $sv = explode('&', $ci['sel_variation_amount']);
                  $variation_amount = end(explode('_', $sv[2]));
                  $item_amount = $ci['amount'] + $variation_amount;
                  $ci['prdcts_total_price'] = number_format((float) $ci['qty'] * ($ci['amount']+$ci['product_fee']+$variation_amount), 2, '.', '');
                  ($ci['product_exist_id'] !='') ? $SUBTOTAL += $item_details['prdcts_total_price'] : '';

                  if ($ci['seller_id'] == 0) {
                    $shipping_admin = (strtolower($this->session->userdata('shippaddress')['country']) == 'united states') ? 'inside_us_product_fees' : 'outside_us_product_fees';

                    $shipping_admin = ($shipping_location_type == 'inside') ? 'inside_us_product_fees' : 'outside_us_product_fees';
                    $admin_item_name_arr[] = $ci['product_name'];
                  }
                  $vendor_shipping_fee_arr[$ci['seller_id']] = array(
                    'local_product_fee' => $ci['local_product_fee'],
                    'international_product_fee' => $ci['international_product_fee'],
                    'product_name' => array($ci['product_name']),
                    'sold_by_accno' => $ci['account_no'],
                    'sold_by' => ($ci['display_name'] != '' && $ci['display_name'] != null) ? $ci['display_name'] : $ci['username'],
                  );
                  // $vendor_shipping_fee_arr[$ci['seller_id']]['local_product_fee'] = $ci['local_product_fee'];
                  // $vendor_shipping_fee_arr[$ci['seller_id']]['international_product_fee'] = $ci['international_product_fee'];
                  // $vendor_shipping_fee_arr[$ci['seller_id']]['product_name'][] = $ci['product_name'];
                  // $vendor_shipping_fee_arr[$ci['seller_id']]['sold_by_accno'] = $ci['account_no'];
                  // $vendor_shipping_fee_arr[$ci['seller_id']]['sold_by'] = ($ci['display_name'] != '' && $ci['display_name'] != null) ? $ci['display_name'] : $ci['username'];
                  $SUBTOTAL += $ci['prdcts_total_price'];               
                  ?>
                  <div class="items_short_info row">
                    <div class="col-sm-3">
                      <div class="prdct_img">
                        <img src="<?php echo base_url().'upload/products/'.$ci['product_image']; ?>" alt="<?php echo $ci['product_image']; ?>" class="img img-responsive">
                      </div>
                    </div>
                    <div class="col-sm-9 padd0">
                      <a href="<?php echo base_url().'store/view_product/'.$ci['product_id'] ?>">
                        <div class="item_title">
                          <span><?php echo $ci['product_name'];?></span>
                        </div>
                      <div class="qty">Qty: <?php echo $ci['qty'];?></div>
                      <!-- <div class="qty">Tax: <?php echo '$ '.number_format((float) $ci['product_fee'], 2, '.', '');?></div> -->
                      </a>
                    </div>
                    <!-- <div class="col-sm-3">
                      <div class="price">
                        <span>$ <?php echo number_format((float)$item_amount, 2, '.', ''); ?></span>
                      </div>
                    </div>
                    <div class="col-sm-3 padd0">
                      <span>$ <?php echo $ci['prdcts_total_price']; ?></span>
                    </div> -->
                  </div>
                <?php } ?>
                <?php 
                // checkout calc
                $SUBTOTAL = number_format((float)$SUBTOTAL, 2, '.', '');
                $ship_data = 0;
                // Admin products shipping calculation
                if (!empty($admin_shipping_fee_arr) && isset($shipping_admin) && $shipping_admin !='') {
                  foreach ($admin_shipping_fee_arr as $key => $asfa) {
                    if ($asfa['option_title'] == $shipping_admin) {
                      $shipping_cal_type = explode('_', $asfa['option_value']); 
                      $shipping_val = ($shipping_cal_type[0] == 1) ? $shipping_cal_type[1]: $SUBTOTAL*$shipping_cal_type[1]/100;
                      $ship_data += $shipping_val;
                    }
                  }
                  $shipinfo[] = array(
                    'item_name' => $admin_item_name_arr,
                    'shipping_charge' => $shipping_val,
                    'sold_by_accno' => 0,
                    'sold_by' => 'Esports',
                  );
                }
                // vendor products shipping calculation
                if (!empty($vendor_shipping_fee_arr)) {
                  foreach ($vendor_shipping_fee_arr as $key => $vsfa) {
                    if ($key != '') {
                      // $shipping_cal_type = (strtolower($this->session->userdata('shippaddress')['country']) == strtolower($key)) ? explode('_', $vsfa['local_product_fee']) : explode('_', $vsfa['international_product_fee']);
                      $shipping_cal_type = ($shipping_location_type == 'inside') ? explode('_', $vsfa['local_product_fee']) : explode('_', $vsfa['international_product_fee']);
                      
                      $ship_charge = ($shipping_cal_type[0] == 1) ? $shipping_cal_type[1] : $SUBTOTAL*$shipping_cal_type[1]/100;
                      $ship_data += $ship_charge;
                      $shipinfo[] = array(
                        'item_name' => $vsfa['product_name'],
                        'shipping_charge' => $ship_charge,
                        'sold_by_accno' => $vsfa['sold_by_accno'],
                        'sold_by' => $vsfa['sold_by'],
                      );
                    }
                  }
                }
                $_SESSION['current_shipping_charge'] = $SHIPPING = number_format((float)$ship_data, 2, '.', '');
                $total = number_format((float)($SHIPPING)+($SUBTOTAL), 2, '.', '');
                ?>
                <div class="itmorder_count row">
                  <div class="col-sm-12">
                    <div class="col-sm-6 padd0" style="display: none;"><span>SUBTOTAL :</span></div>
                    <div class="col-sm-6 text-right padd0" style="display: none;">
                      <span><?php echo '$'.number_format((float) $SUBTOTAL, 2, '.', ''); ?></span>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="col-sm-6 padd0">
                      <div class="shipping_info_div" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Shipping charge detail">Shipping ($) <span class="shipping_info_icon"><i class="fa fa-info-circle"></i></span></div>
                    </div>
                    <div class="col-sm-6 text-right padd0">
                      <span><?php echo '$'.number_format((float) $SHIPPING, 2, '.', ''); ?></span>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="col-sm-6 padd0"><span>TOTAL :</span></div>
                    <div class="col-sm-6 text-right padd0">
                      <span><?php echo '$'.number_format((float) $total, 2, '.', ''); ?></span>
                    </div>
                  </div>
                </div>
              <?php } ?>
            </div>
            <div class="count_div">
              
            </div>
          </div>
          <div class="shipping_charge_div clearfix hide">
            <div class="col-sm-12 shippcharge_div">
              <table class="table-responsive table table-bordered text-left">
                <thead>
                  <tr>
                    <th class="text-center">Shipping charge</th>
                    <th class="text-center">Products</th>
                    <th class="text-center">Sold by</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if (!empty($shipinfo)) {
                    foreach ($shipinfo as $key1 => $si) {
                      foreach ($si['item_name'] as $key2 => $itm) { ?>
                        <tr>
                          <?php if ($key2 == 0) { ?>
                            <td rowspan="<?php echo count($si['item_name']); ?>"><?php echo '$'.number_format((float)$si['shipping_charge'], 2, '.', ''); ?></td>
                          <?php } ?>
                          <td><?php echo $itm; ?></td>
                          <?php if ($key2 == 0) { ?>
                            <td rowspan="<?php echo count($si['item_name']); ?>"><a href="<?php echo ($si['sold_by_accno'] != 0) ? base_url().'store/'.$si['sold_by_accno'] : base_url().'store'; ?>" target="_blank" class="strlink_from_shipping" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Go to Store"><?php echo $si['sold_by']; ?> <i class="fa fa-link"></i></a></td>
                          <?php } ?>
                        </tr>
                      <?php } ?>
                    <?php }
                  } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      <?php } else { ?>
        <div class="col-sm-12">
          <div class="emptyitem_lable">Please add item in cart</div>
          <br>
          <div class="text-center"><a href="<?php echo base_url() ?>store" class="cmn_btn" style="font-size: 20px;"> Go to Store</a></div>
        </div>
      <?php } ?>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $('.shipping_info_div').on('click',function(){
    var senddata = $('.shipping_charge_div').html();
    var modaltitle = 'Shipping info';
    var data = {
      'modal_title': modaltitle,
      'senddata':senddata,
      'tag':'commonmodalshow'
    }
    commonshow(data);
  });
$( document ).ready(function() {
    $("#paypal").click(function() {
      if(this.checked) {
          $("#card").attr("disabled", true);
      }
      else
      {
        $("#card").removeAttr("disabled");
      }
  });
    $("#card").click(function() {
     
      if(this.checked) {
          $("#paypal").attr("disabled", true);
      }
      else
      {
        $("#paypal").removeAttr("disabled");
      }
  });
});
</script>