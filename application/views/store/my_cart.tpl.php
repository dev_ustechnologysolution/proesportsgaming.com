<section class="body-middle innerpage">
    <?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
    <div class="container">
        <div class="row header1">
            <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
            <div class="col-lg-4 col-sm-4 col-md-4"><h2>Cart</h2></div>
            <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
        </div>
        <?php $this->load->view('common/product_filter.tpl.php'); ?>    
        <div class="row">
            <div class="cart_product_listing_div">
                <ul class="c_product_list">
                    <?php 
                    $i = 0;
                    foreach ($cart_item as $key => $ci) { 
                        $item_details = unserialize($ci['item_details']);
                        ?>
                        <li class="c_product_list_li">
                            <div class="col-md-2 c_prdct_img">
                                <img src="<?php echo base_url().'upload/products/'.$item_details['select_img']; ?>" alt="<?php echo $item_details['select_img']; ?>" class="img img-responsive">
                            </div>
                            <div class="col-md-3 c_prdct_name">
                                <label><?php echo $item_details['product_name']; ?></label>
                            </div>
                            <div class="col-md-2 c_prdct_price">
                                <label><?php echo $item_details['price']; ?></label>
                            </div>
                            <div class="col-md-2 c_prdct_item">
                                <label></label>
                            </div>
                            <div class="col-md-3 c_prdct_chckt_btn">
                                <button class="cmn_btn"><i class="fa fa-trash-o"></i></button>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</section>
