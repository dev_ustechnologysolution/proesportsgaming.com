<?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
<div class="adm-dashbord">
  <div class="container padd0">
    <?php $this->load->view('dashboard/profile_sidebar.tpl.php'); ?>
    <div class="col-lg-10 common_product_list">
      <div class="col-lg-12">
        <?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
        <div class="myprofile-edit row my_store">
          <div class="col-sm-12 wishlist_product_list">
            <h2>Store Details</h2>
          </div>
          <div class="col-sm-12">
            <div class="row report_bx_main_div">
              <div class="col-sm-4">
                <div class="rprtbox_div">
                  <div class="rprtbx">
                    <div class="inner">
                      <h1 class="cnt_lbl">
                        <?php
                        echo (!empty($order_detail[0]['total_gross_selling'])) ? $order_detail[0]['total_gross_selling'] : '0'; ?></h1>
                      <p>Gross Selling</p>
                    </div>
                  </div>
                  <div class="icon">
                    <img src="<?php echo base_url(); ?>assets/frontend/images/save-money_white.png" class="icnimg img img-responsive">
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="rprtbox_div">
                  <div class="rprtbx">
                    <div class="inner">
                      <h1 class="cnt_lbl"><?php echo $order_detail[0]['total_items_sold'] ? $order_detail[0]['total_items_sold'] : '0'; ?></h1>
                      <p>Sold Items</p>
                    </div>
                  </div>
                  <div class="icon">
                    <img src="<?php echo base_url(); ?>assets/frontend/images/shopping-bag_white.png" class="icnimg img img-responsive">
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="rprtbox_div">
                  <div class="rprtbx">
                    <div class="inner">
                      <h1 class="cnt_lbl"><?php echo $order_detail[0]['total_orders']; ?></h1>
                      <p>Order Received</p>
                    </div>
                  </div>
                  <div class="icon">
                    <img src="<?php echo base_url(); ?>assets/frontend/images/sold_items_white.png" class="icnimg img img-responsive">
                  </div>
                </div>
              </div>
            </div>
            <?php if (!empty($current_level)): ?>
              <div class="row reached_level_box">
                <div class="col-sm-12">
                  <div class="bx clearfix">
                    <div class="col-sm-3">
                      <div class="img-box">
                        <img src="<?php echo base_url().'upload/all_images/'.$current_level['level_img']; ?>" class="img img-responsive" alt="smile" style="height: 112px;">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="lvl_cont text-center">
                        <div class="clearfix lvl_cont_ttl">
                          <h1>Current Level</h1>
                        </div>
                        <div class="clearfix lvl_cont_num">
                          <h3>Level : <?php echo $current_level['level_title']; ?></h3>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="img-box">
                        <img src="<?php echo base_url().'upload/all_images/'.$current_level['level_img']; ?>" class="img img-responsive" alt="smile" style="height: 112px;">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php endif ?>
            <div class="level_chart_div clearfix">
              <div style="width:100%;">
                <canvas id="canvas"></canvas>
              </div>
              <div class="row" style="padding: 15px 0 0;">
                <div class="col-sm-12 bid-info form-group" style="color: #ff5000">
                  <div class="col-sm-2">
                    <?php 
                      $rwd = $this->General_model->view_single_row('store_levels',array('level' => $user_level['current_level']), '*');
                      $nxt_rwd = $this->General_model->view_single_row('store_levels',array('level' => $user_level['current_level']+1), '*');
                      if (!empty($user_level['current_level']) && !empty($rwd['reward'])) {
                        echo '<h4>Level '.$user_level['current_level'].' = $'. $rwd['reward'].'</h4>';
                      }
                    ?> 
                  </div>
                  <div class="col-sm-10 text-right">
                    <?php
                    if ($rwd['item_sold'] > $user_level['sold_items'] && $user_level['sold_items'] != 0) {
                      echo '<h4> Need '.($rwd['item_sold'] - $user_level['sold_items']).' more sales to reach current level = $'.$rwd['reward'];
                    } else if ($rwd['item_sold'] > $user_level['sold_items'] && $user_level['sold_items'] == 0) {
                      echo '<h4> Need '.($rwd['item_sold'] - $user_level['sold_items']).' more sales to reach next level = $'.$rwd['reward'];
                    } ?> 
                    </h4>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="clearfix text-right">
                    <button class="cmn_btn" onclick="$('.lvl_lsit_info').toggle();"> <i class="fa fa-bar-chart"></i> List <i class="fa fa-caret-down"></i></button>
                  </div>
                </div>
              </div>
              <br>
              <div class="clearfix lvl_lsit_info" style="display: none;">
                <div class="col-sm-12 lvl_heading">
                  <div class="level-item col-sm-2">Level Num</div>
                  <div class="level-item col-sm-3">Name</div>
                  <div class="level-item col-sm-2">Reward</div>
                  <div class="level-item col-sm-2">Item sold</div>
                  <div class="level-item col-sm-3">Status</div>
                </div>
                <?php if (!empty($levels)) {
                  foreach ($levels as $key => $lvl) { ?>
                    <div class="col-sm-12 lvl_info_row">
                      <div class="level-item col-sm-2"><?php echo $lvl['level']; ?></div>
                      <div class="level-item col-sm-3"><?php echo $lvl['level_title']; ?></div>
                      <div class="level-item col-sm-2"><?php echo '$'.number_format((float)$lvl['reward'],'2','.',''); ?></div>
                      <div class="level-item col-sm-2"><?php echo $lvl['item_sold']; ?></div>
                      <div class="level-item col-sm-3"><?php echo (in_array($lvl['level'], array_column($user_level_data, 'level')) && $user_level['current_level'] != $lvl['level']) ? '<span class="btn-success" style="padding: 5px 8px;">Reached</span>' : '-'; ?></div>
                    </div>
                  <?php }
                } ?>
              </div>
            </div>

            <div class="stream_settings_stream_first_div created_lobbys_list row store_settings">
              <form action="<?php echo base_url(); ?>store/store_banner_update" method="POST" class=" has-validation-callback" enctype="multipart/form-data">
                  <label for="file" class="clearfix col-sm-12 paddbot10">Store Banner</label>
                  <div class="form-group clearfix">
                    <div class="col-sm-10">
                      <input type="file" class="form-control" id="file" placeholder="Image" name="image">
                      <input type="hidden" name="old_image" value="<?php if($this->session->userdata('store_banner') != '' && $this->session->userdata('store_banner') != null) echo $this->session->userdata('store_banner');?>">
                    </div>
                    <div class="col-sm-2">
                      <input type="submit" name="submit" class="cmn_btn pull-right" value="Upload">
                    </div>
                  </div>
                  <?php if($this->session->userdata('store_banner') != '' || $this->session->userdata('store_banner') != null){ ?>
                      <div class="form-group col-sm-12">
                        <img src="<?php echo base_url().'upload/all_images/'.$this->session->userdata('store_banner')?>" height="150" width="150"><br>
                      </div>
                      <div class="form-group col-sm-12">
                        <a href="<?php echo base_url().'store/delete_store_banner';?>"><input type="button" name="remove" class="cmn_btn" value="Remove"></a>
                      </div>
                  <?php } ?>
              </form>
            </div>
            <div class="title">Change your Store link</div>
            <div class="stream_settings_stream_first_div created_lobbys_list row store_settings">
              <form class="has-validation-callback store_link_submit">
                  <div class="col-sm-12 bid-info">
                    <h5 class="custom_link_label">Store link : <span class="pull-right">Default link : <?php echo $this->session->userdata('store_link'); ?></span></h5>
                    <label class="lobby_lnk">
                      <div class="row">
                        <div class="lnk_loc col-sm-8">
                          <span><?php echo base_url(); ?>store/</span>
                        </div>
                        <div class="col-sm-4">
                          <div class="pull-right">
                            <input type="text" name="store_link" placeholder="Store Link" class="form-control" value="<?php echo $this->session->userdata('store_link'); ?>" required="" pattern="[a-zA-Z0-9]+" title="Store Link should only contain letters and number.">
                            <input type="hidden" name="old_store_link" value="<?php echo $this->session->userdata('store_link'); ?>">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-3">
                          <a href="<?php echo base_url(); ?>store/<?php echo $this->session->userdata('store_link'); ?>" class="cmn_btn custom_btn">Go to store</a>
                        </div>
                        <div class="col-sm-6"></div>
                        <div class="col-sm-3">
                          <input type="button" name="submit" class="cmn_btn custom_btn submit_custom_url" value="Submit">
                          <div class="lobby_exist_loader"><span></span></div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <label class="store_exist_error pull-right" style="color: #fff;"><span></span></label>
                        </div>
                      </div>
                    </label>
                  </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo base_url();?>assets/plugins/chartjs/Chart.js"></script>
<script type="text/javascript">
  var chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: '#ff5000',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(231,233,237)'
  };
  var randomScalingFactor = function() {
    return (Math.random() > 5 ? 1 : 1) * Math.round(Math.random() * 100);
  }
  var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  var config = {
    type: 'bar',
    data: {
      labels: <?php print_r(json_encode($levels_array)) ;?>,
      datasets: [{
        label: "Reached Quantities",
        backgroundColor: chartColors.orange,
        borderColor: chartColors.orange,
        data: <?php print_r(json_encode($user_level_data_array)) ;?>,
        fill: false,
      },{
        label: "Total Quantities",
        backgroundColor: chartColors.grey,
        borderColor: chartColors.grey,
        data: <?php print_r(json_encode($user_level_data_array_remain));?>,
        fill: false,
      }]
    },
    options: {
      legend: {
        display: false,
        labels : {
          fontColor: '#ffffff'
        }
      },
      responsive: true,
      title: {
        fontColor: '#fff',
        display: true,
        text: 'Level Chart'
      },
      tooltips: {
        enabled: false
      },
      hover: {
        mode: 'nearest',
        intersect: true
      },
      scales: {
        xAxes: [{
          stacked: true,
          display: true,
          ticks : {
            fontColor:'#fff'
          },
          scaleLabel: {
            fontColor: '#fff',
            display: true,
            labelString: 'Levels'
          },
          gridLines: {
            color:'#ffffff',
            display: false
          }
        }],
        yAxes: [{
          stacked: true,
          display: true,
          ticks : {
            fontColor:'#fff'
          },
          scaleLabel: {
            fontColor: '#fff',
            display: true,
            labelString: 'Sold Items',
          },gridLines: {
            color:'#ffffff',
            display: false
          }
        }],
      }
    }
  };

  var ctx = document.getElementById("canvas").getContext("2d");
  window.myLine = new Chart(ctx, config);
</script>