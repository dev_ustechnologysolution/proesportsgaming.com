<section class="body-middle innerpage">
    <?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
    <div class="container">
        <div class="row header1">
            <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
            <div class="col-lg-4 col-sm-4 col-md-4"><h2>store</h2></div>
            <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
        </div>
        <?php $this->load->view('common/product_filter.tpl.php'); ?>    
        <div class="row product_single_page">
        	<div class="col-sm-1 padd0 small-img">
                <div class="img_arr small-container">
                    <div id="prev-img" class="cr_arrw icon-left <?php echo (!empty($prdct_img_arr) && count($prdct_img_arr) <= 4) ? 'hide': ''; ?>"><span class="sldr_spn top_spn"><i class="fa fa-chevron-circle-up"></i></span></div>
                    <ul class="prdct_img_ul" id="small-img-roll" style="<?php echo (!empty($prdct_img_arr) && count($prdct_img_arr) > 4) ? 'overflow-y: scroll;' : 'overflow-y: unset;'; ?>">
        			<?php if (!empty($prdct_img_arr)) { ?>
                        <?php foreach ($prdct_img_arr as $key => $p_img) { ?>
        					<li class="prdctimg <?php echo ($p_img == $default_sel_img) ? 'active' : ''; ?>" data-tg_img="<?php echo $p_img; ?>">
                                <img class="prdct_thmbnl img img-responsive show-small-img" src="<?php echo base_url().'upload/products/'.$p_img; ?>" alt="<?php echo ($p_img == $default_sel_img) ? 'now' : ''; ?>">
        					</li>
        				<?php } ?>
        			<?php } ?>
        			</ul>
                    <div id="next-img" class="cr_arrw icon-right <?php echo (!empty($prdct_img_arr) && count($prdct_img_arr) <= 4) ? 'hide': ''; ?>"><span class="sldr_spn bottom_spn"><i class="fa fa-chevron-circle-down"></i></span></div>
        		</div>
        	</div>
            <div class="col-sm-11">
                <div class="product_main_img show zoom" href="<?php echo base_url().'upload/products/'.$default_sel_img; ?>">
                    <img class="img img-responsive" src="<?php echo base_url().'upload/products/'.$default_sel_img; ?>" alt="<?php echo $default_sel_img; ?>" id="show-img">
                </div>
                <div class="prdct_right-side">
                    <div class="product_title_div"> <?php echo $product_detail[0]->name; ?> </div>
                    <?php //if ($product_detail[0]->username != '') { ?>
                        <!-- <div class="product_vendor_div"> - <?php // echo ($product_detail[0]->display_name !='') ? $product_detail[0]->display_name : $product_detail[0]->username; ?> </div> -->
                    <?php //} ?>
            		<div class="product_price_div margin-tpbtm-10">
                        <div>$ <span class="price"><?php echo number_format((float)$product_detail[0]->price, 2, '.', ''); ?></span></div>
            		</div>
                    <form class="add-to-cart_form" method="post" action="<?php echo base_url(); ?>cart/add_product_cart">
                        <input type="hidden" name="ref_seller_id" value="<?php echo $ref_id;?>">                    
                        <div class="row addtocart_div">
                            <div class="col-sm-4">
                                <div class="select_prdct_qnt">
                                    <?php if ($product_detail[0]->qty > 0) { ?>
                                        <button type="button" id="sub" class="sub cmn_btn"><i class="fa fa-minus"></i></button>
                            			<input type="number" name="qty" class="form-control prdct_qntity" min="1" max="<?php echo $product_detail[0]->qty; ?>" pattern="[0-9]+" required readonly value="1">
                                        <button type="button" id="add" class="add cmn_btn"><i class="fa fa-plus"></i></button>
                                    <?php } else { ?>
                                        <label class="outof_stock">Out of Stock.!</label>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <?php if ($product_detail[0]->qty > 0) { ?>
                                    <!-- <div class="col-sm-2"></div> -->
                                    <!-- <?php // if($this->session->userdata('user_id') != '') { ?> -->
                                    <div class="add_to_wishlist_div col-sm-6 pull-right padd0" style="padding: 0px 0px 0px 15px;">
                                        <button class="cmn_btn" type="submit" name="add_to_wishlist" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Add this product into Wishlist"> Add to Wishlist </button>
                                    </div>
                                    <!-- <?php //} else { ?> -->
                                    <!-- <div class="add_to_wishlist_div col-sm-5 pull-right" style="padding: 0px 0px 0px 15px;">
                                        <a class="cmn_btn"  href="<?php //echo base_url('login'); ?>" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Add this product into Wishlist"> Add to Wishlist</a>
                                    </div> -->
                                    <!-- <?php //} ?> -->
                                    <div class="add_to_cart_div col-sm-6 pull-right padd0">
                                        <button class="cmn_btn" type="submit" name="add_to_cart" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Add this product into Cart"> Add to Cart </button>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 prdct_fee">
                                <?php // if ($ship_amnt != '') { ?>
                                    <!-- <label class="checkbox_container text-left">
                                        <input type="checkbox"><span class="checkmark"></span> -->
                                        <!-- <input type="hidden" name="ship_amnt" value="<?php echo $ship_amnt; ?>">
                                        <h5><?php  //echo $ship_amnt; ?> Shipping Fee will be added outside of the United States</h5> -->
                                    <!-- </label> -->
                                <?php //} ?>
                                <?php //if ($product_detail[0]->fee != '' && $product_detail[0]->fee>0) { ?>
                                    <!-- <div>Note : $ <?php // echo number_format((float)$product_detail[0]->fee, 2, '.', ''); ?> fee will be count on each Product.</div> -->
                                <?php //} ?>
                            </div>
                        </div>
                        <?php if ($product_detail[0]->qty > 0) { ?>
                            <?php if (!empty($attribute_type_list)) { ?>
                                <div class="row selvariation_div margin-tpbtm-10">
                                    <ul class="variation_ul">
                                        <?php
                                        foreach($attribute_list as $key => $al) { ?>
                                            <li class="select_var <?php echo $key; ?>_variation">
                                                <label><?php echo ucfirst($key); ?></label>
                                                <?php if ($attribute_type_list[$key] == 3) { ?>
                                                    <select required class="form-control <?php echo $key; ?>">
                                                        <option value="" attribute_lable="<?php echo $key; ?>">Please choose <?php echo $key; ?></option>
                                                <?php } ?>
                                                <?php foreach ($attribute_list[$key] as $k => $opt_arr) { 
                                                    if ($attribute_type_list[$key] == 1) {
                                                        ?>
                                                        <div class="form-group margin0">
                                                            <label class="checkbox_container">
                                                                <input type="checkbox" name="<?php echo $opt_arr->title; ?>" value="atr_<?php echo $opt_arr->attr_id; ?>&optid_<?php echo $opt_arr->option_id; ?>" attribute_id="<?php echo $opt_arr->attr_id; ?>" attribute_lable="<?php echo $opt_arr->title; ?>" option_id="<?php echo $opt_arr->option_id; ?>" option_lable="<?php echo $opt_arr->option_label; ?>" extra_price="<?php echo $opt_arr->extra_amount; ?>" class="<?php echo $key; ?>"><span class="checkmark"></span> <span class="attrttle"><?php echo $opt_arr->option_label; ?></span>
                                                            </label>
                                                        </div>
                                                    <?php } elseif ($attribute_type_list[$key] == 2) { ?>
                                                        <div class="form-group margin0 radio_button_div">
                                                            <label class="checkbox_container"><input type="radio" name="<?php echo $opt_arr->title; ?>" value="atr_<?php echo $opt_arr->attr_id; ?>&optid_<?php echo $opt_arr->option_id; ?>" attribute_id="<?php echo $opt_arr->attr_id; ?>" attribute_lable="<?php echo $opt_arr->title; ?>" option_id="<?php echo $opt_arr->option_id; ?>" option_lable="<?php echo $opt_arr->option_label; ?>" extra_price="<?php echo $opt_arr->extra_amount; ?>" class="<?php echo $key; ?>"><span class="checkmark"></span> <span class="attrttle"><?php echo $opt_arr->option_label; ?></span></label>
                                                        </div>
                                                    <?php } elseif ($attribute_type_list[$key] == 3) { ?>
                                                        
                                                        <option value="atr_<?php echo $opt_arr->attr_id; ?>&optid_<?php echo $opt_arr->option_id; ?>" attribute_id="<?php echo $opt_arr->attr_id; ?>" attribute_lable="<?php echo $opt_arr->title; ?>" option_id="<?php echo $opt_arr->option_id; ?>" option_lable="<?php echo $opt_arr->option_label; ?>" extra_price="<?php echo $opt_arr->extra_amount; ?>"><?php echo $opt_arr->option_label; ?> </option>
                                                    <?php } ?>
                                                <?php } ?>
                                                <?php if ($attribute_type_list[$key] == 3) { ?>
                                                    </select>
                                                <?php } ?>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            <?php } ?>
                            <?php if ($product_detail[0]->is_customizable == 1) { ?>
                                <div>
                                    <a class="cmn_btn customizeble_product"> Customize Product </a>
                                </div>
                                <input type="hidden" name="Custom_Tag" value="optlbl_PRO ESPORTS GAMiNG" class="form-control custom_tag_variation">
                                <!-- <input type="hidden" name="sel_variation_ids[]" value=""> -->
                            <?php } ?>
                        <?php } ?>
                        <input type="hidden" name="product_image" value="<?php echo ($default_sel_img) ? $default_sel_img : '';?>">
                        <input type="hidden" name="product_id" value="<?php echo ($product_id) ? $product_id : '';?>">
                        <input type="hidden" name="amount" value="<?php echo number_format((float)$product_detail[0]->price, 2, '.', ''); ?>">
                        <input type="hidden" name="product_original_name" value="<?php echo number_format((float)$product_detail[0]->price, 2, '.', ''); ?>" class="originlprice">
                        <input type="hidden" name="product_fee" value="<?php echo($product_detail[0]->fee>0 && $product_detail[0]->fee !='')? number_format((float)$product_detail[0]->fee, 2, '.', '') :'';?>">
                        <input type="hidden" name="int_fee" value="<?php echo($product_detail[0]->int_fee>0 && $product_detail[0]->int_fee !='')? number_format((float)$product_detail[0]->int_fee, 2, '.', '') :'';?>">
                        <!-- <input type="hidden" name="extra_amount" class="extra_amount"> -->
                        <input type="hidden" name="product_name" value="<?php echo ($product_detail[0]->name) ? $product_detail[0]->name : ''; ?>">
                        <div class="selected_var"></div>
                    </form>
            	</div>
            </div>
            <?php if (!empty($product_ads)) { ?>
            <div class="col-sm-12 add_listing_div margin-tpbtm-40">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <a class="ads_container_desciption_div" href="<?php echo $product_ads->sponsered_by_logo_link; ?>" target="_blank">
                       <div class="ads_img_div col-sm-4">
                        <img src="<?php echo base_url() ?>upload/ads_master_upload/<?php echo $product_ads->upload_sponsered_by_logo; ?>" alt="<?php echo $product_ads->upload_sponsered_by_logo; ?>" class="img img-responsive">
                       </div>
                       <div class="ads_content_div col-sm-8">
                        <div class="ads_desciption"><?php echo $product_ads->sponsered_by_logo_description; ?></div>
                      </div>
                    </a>
                </div>
                <div class="col-md-3"></div>
            </div>
            <!-- <div class="product_shortdesc_div"> <h5><?php // echo $product_detail[0]->description; ?></h5></div> -->
            <?php } ?>
            <?php if ($product_detail[0]->description !='') { ?>
                <div class="col-sm-12 pr_description_div">
                    <div class="desc_title margin-tpbtm-10">About the product</div>
                    <div class="desc_cntnt margin-tpbtm-10"> <?php echo $product_detail[0]->description; ?> </div>
                </div>
            <?php } ?>
            <?php if (!empty($product_ads)) {
                if ($product_ads->youtube_link != '' || $product_ads->custom_commercial !='') { ?>
                    <div class="col-sm-12 add_listing_div commercial_add_div margin-tpbtm-40">
                        <div class="col-sm-12 margin-tpbtm-40">
                            <div class="ads_title">
                                <h2><font color="FD5103">PRO </font><font color="FFC22418">ESPORTS </font><font color="FDFDFD">GAMiNG </font> Review</h2>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <?php if ($product_ads->youtube_link != '') { 
                                $iframe = $this->General_model->getYoutubeEmebedUrl($product_ads->youtube_link,1); ?>
                                <?php echo $iframe['embeded_iframe']; ?>
                            <?php } else if ($product_ads->custom_commercial != '') { ?>
                            <video class="commercial_ads" loop autoplay muted controls src="<?php echo base_url(); ?>upload/ads_master_upload/<?php echo $product_ads->custom_commercial; ?>" width="100%"></video>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <div class="modal custmodal commonmodal customize_product_image myprofile-edit" style="display: none;">
        <div class="modal-content">
            <div class="modal-header">
                <span class="close">×</span>
                <p class="modal_h1_msg text-left" style="color: #ff5000;"><?php echo (!empty($product_detail[0]->customizable_tag)) ? $product_detail[0]->customizable_tag : '';?></p>
                <hr>
                <div class="row product_cust_design">
                    <div class="col-sm-12" id="controls">
                        <?php if (!empty($product_detail[0]->customizable_tag_description)) { ?>
                            <div class="form-group">
                                <div class="input-group col-lg-12 text-left">
                                    <h3 class="margin0"><?php echo $product_detail[0]->customizable_tag_description; ?></h3>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <div class="input-group col-lg-12">
                                <input type="text" placeholder="Please enter tag" name="customizable_tag" class="form-control" value="PRO ESPORTS GAMiNG">
                            </div>
                        </div>
                        <div class="form-group custom_tag_error" style="display: none;">
                            <div class="input-group col-lg-12">
                                <div class="alert alert-danger">Please Enter Custom Tag</div>
                            </div>
                        </div>
                        <br>
                        <div class="form-group text-center">
                            <div class="input-group col-lg-12">
                                <a class="cmn_btn save_customized_tag">Save</a>
                            </div>
                        </div>
                    </div>
                </div>

               <!--  <div class="row modal_data product_cust_design">
                    <div class="col-sm-6">
                        <div class="customize_prdct_img clearfix" style="width: 320px; height: 300px;">
                            <img src="" class="img img-responsive" alt="smile" style="width: 320px; height: 300px;">
                            <div class="dragable_div custm_text">Pro Esports Gaming</div>
                        </div>
                    </div>
                    <div class="col-sm-6" id="controls">
                        <div class="form-group">
                            <label class="col-lg-4">Text : </label>
                            <div class="input-group col-lg-8">                                 
                                <input type="text" id="set_text_prdct" placeholder="Please enter text" class="form-control" value="Pro Esports Gaming">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4">Font Size : </label>
                            <div class="input-group col-lg-8">                                 
                                <input id="font_size_sldr" type="range" min="12" max="40" value="14">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4">Color : </label>
                            <div class="input-group col-lg-8">                                 
                                <input type="text" class="form-control simple-color-picker prdct_text_color" value="#ff5000">
                            </div>
                        </div>
                    </div>
                    <a class="cmn_btn pull-right save_customized_img">Save</a>
                </div> -->
            </div>
        </div>
    </div>
</section>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css" />
<script src="<?php echo base_url(); ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<script async type="text/javascript" src="<?php echo base_url() ?>assets/frontend/js/product_designing.js"></script>
<script async type="text/javascript" src="<?php echo base_url() ?>assets/frontend/js/html2canvas.js"></script>
