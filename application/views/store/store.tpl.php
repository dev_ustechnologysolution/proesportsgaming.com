<section class="body-middle innerpage">
    <?php 
        $this->load->view('common/flash_msg_show.tpl.php'); 
        $_SESSION['ref_id'] = $user_id;
    ?>
    <div class="container">
        <div class="row header1">
            <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
            <div class="col-lg-4 col-sm-4 col-md-4"><h2>Store</h2></div>
            <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
        </div>
        <?php if($cus_store == 1){ ?>
            <div class="profile_top row">
                <div class="col-sm-6 text-right">
                    <a class="profile_top_btn orange" id = "admin_gear_btn" href="javascript:void(0);">Pro Esports Store</a>
                </div>
                <div class="col-sm-6">
                    <a class="profile_top_btn black" id = "player_gear_btn" href="javascript:void(0);">Player's World Store</a>
                </div>    
            </div>
        <?php } ?>
        <input type="hidden" name="store_user_id" id="store_user_id" value="0">
        <input type="hidden" name="item_per_page" id="item_per_page" value="<?php echo $itemperpage; ?>">
        <?php if($user_data['store_banner'] != '' && $user_data['store_banner'] != null){ ?>
            <div class="row str_img" style="display: none;">
                <img src="<?php echo base_url().'upload/all_images/'.$user_data['store_banner']?>" height="300px" width="100%">
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-sm-9"><?php $this->load->view('common/product_filter.tpl.php'); ?></div>
            <div class="col-sm-1">&nbsp;</div>
            <div class="col-sm-2">
                <div class="row padding-top-50 prdct_search_filter">
                    <div class="col-lg-12 animated-box">
                        <div class="clearfix filter-panel home_page_search_tabbing store_page_filter">
                            <div class="form-group" style="padding: 0 8px;">
                                <select name="select_num_prdct" id="select_num_prdct" class="form-control" style="width: 100%;">
                                    <option class="select_title" value="25">Select display products</option>
                                    <option value="50">50</option>
                                    <option value="75">75</option>
                                    <option value="100">100</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="product_listing_first_div">
                <div class="count_product"><?php echo ($count_product !='') ? $count_product : ''; ?></div>
                <ul class="product_list">
                    <?php if (!empty($list)) { ?>
                        <?php 
                        $i = $last_order = $last_product = 0;
                        foreach ($list as $key => $product) {
                            $last_product = $product['id'];
                            $last_order = $product['product_order'];
                            $image_arr = $this->General_model->view_single_row('product_images',array('product_id' => $product['id']),'image');

                            if ($key % 5 == 0 && $key != 0) { ?>
                                <li class="ads row">
                                    <div class="add_listing_div">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">
                                            <a class="ads_container_desciption_div" href="<?php echo $store_ads_list[$i]->sponsered_by_logo_link; ?>" target="_blank">
                                               <div class="ads_img_div col-sm-4">
                                                <img src="<?php echo base_url() ?>upload/ads_master_upload/<?php echo $store_ads_list[$i]->upload_sponsered_by_logo; ?>" alt="<?php echo $store_ads_list[$i]->upload_sponsered_by_logo; ?>" class="img img-responsive">
                                               </div>
                                               <div class="ads_content_div col-sm-8">
                                                <div class="ads_desciption"><?php echo $store_ads_list[$i]->sponsered_by_logo_description; ?></div>
                                              </div>
                                            </a>
                                        </div>
                                        <div class="col-md-3"></div>
                                    </div>
                                </li>
                            <?php $i++; } ?>
                            <li class="product_itm product_<?php echo $product['id']; ?>" order_id="<?php echo $product['product_order'];  ?>">
                                <div class="main_product">
                                    <div class="product_img_dv"><img src='<?php echo base_url()."upload/products/".$image_arr["image"];?>' alt="<?php echo $image_arr["image"]; ?>" class="img img-responsive"></div>
                                    <div class="product_title_dv">
                                        <label class="title"><?php echo $product['name']; ?></label>
                                    </div>
                                    <div class="product_view_dv">
                                        <a class="view_product" href="<?php echo base_url(); ?>Store/view_product/<?php echo $product['id']; ?>"> View Item</a>
                                    </div>
                                </div>
                            </li>
                        <?php } ?>
                        <?php 
                        if (count($list) >= $itemperpage) { ?>
                            <li class="load_more_div row text-center">
                                <a class="load_more_btn"> Load More</a>
                                <input type="hidden" id="totalcount" value="<?php echo $get_last_id['count']; ?>">
                                <input type="hidden" id="all" value="<?php echo $last_order; ?>">
                                <input type="hidden" id="last_product" value="<?php echo $last_product; ?>">
                            </li>
                        <?php } ?>
                    <?php } else { ?>
                        <h5><div class='row bidhottest_err prdctempty'><div class='col-sm-12' style='color: #FF5000;'>Products not available</div></div></h5>
                    <?php } ?>
                </ul>
                <div class="temp_append" style="display: none;">
                    <li class="app_pro">
                        <div class="main_product">
                            <div class="product_img_dv"><img class="img img-responsive"></div>
                            <div class="product_title_dv">
                                <label class="title"></label>
                            </div>
                            <div class="product_view_dv">
                                <a class="view_product" > View Item</a>
                            </div>
                        </div>
                    </li>
                    <li class="row ads">
                        <div class="add_listing_div">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <a class="ads_container_desciption_div" target="_blank">
                                   <div class="ads_img_div col-sm-4">
                                    <img class="img img-responsive">
                                   </div>
                                   <div class="ads_content_div col-sm-8">
                                    <div class="ads_desciption"></div>
                                  </div>
                                </a>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                    </li>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        $(window).on('load', function(){
            $('#admin_gear_btn').click();
        })

        $(window).keydown(function(event){
            if(event.keyCode == 13) {
              event.preventDefault();
              $('#search_gear_btn').click();
              return false;
            }
          });

        $('#player_gear_btn').on('click', function(){
            var sid = '<?php echo $user_id;?>';
            $('.str_img').show();
            $('#admin_gear_btn').removeClass('orange').addClass('black');
            $('#player_gear_btn').removeClass('black').addClass('orange');
            $('#store_user_id').val(sid);
        });
        $('#admin_gear_btn').on('click', function(){
            $('.str_img').hide();
            $('#player_gear_btn').removeClass('orange').addClass('black');
            $('#admin_gear_btn').removeClass('black').addClass('orange');
            $('#store_user_id').val('0');
        });
    })
</script>
