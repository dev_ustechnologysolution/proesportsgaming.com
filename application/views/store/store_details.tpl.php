<?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
<div class="adm-dashbord">
  <div class="container padd0">
    <?php $this->load->view('dashboard/profile_sidebar.tpl.php'); ?>
    <div class="col-lg-10 common_product_list">
      <div class="col-lg-12">
        <?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
        <div class="myprofile-edit row">
          <div class="col-sm-12 wishlist_product_list">
            <h2>Store Details</h2>
          </div>
          <div class="col-sm-12">
            <div class="title">Change your Live Lobby link</div>
              <div class="stream_settings_stream_first_div created_lobbys_list row">
                <form action="<?php echo base_url(); ?>/Streamsetting/update_lobby_url" method="POST" lobby_id="141" class="update_lobby_custom_url has-validation-callback">
                    <div class="col-sm-3 bidimg padd0">
                      <img src="<?php echo base_url(); ?>/upload/game/1543635359Battlefield-5 (Mini).jpg" alt="">
                    </div>
                    <div class="col-sm-9 bid-info">
                      <h5 class="custom_link_label">Custom link : <span class="pull-right">Default ID : 141</span></h5>
                      <label class="lobby_lnk">
                        <div class="row">
                          <div class="lnk_loc col-sm-8">
                            <span><?php echo base_url(); ?>/livelobby/</span>
                          </div>
                          <div class="col-sm-4">
                            <div class="pull-right">
                              <input type="text" name="custom_name" placeholder="Custom" class="form-control" value="account4" lobby_id="141" required="">
                              <input type="hidden" name="lobby_id" value="141">
                              <input type="hidden" name="old_custom_name" lobby_id="141" value="account4">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-3">
                            <a href="<?php echo base_url(); ?>/livelobby/watchLobby/141" class="cmn_btn custom_btn">Live Event</a>
                          </div>
                          <div class="col-sm-6"></div>
                          <div class="col-sm-3">
                            <input type="submit" name="submit" class="cmn_btn custom_btn submit_custom_url" value="Submit" lobby_id="141">
                            <div class="lobby_exist_loader" lobby_id="141"><span></span></div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-12">
                            <label class="lobby_exist_error pull-right" style="color: #fff;" lobby_id="141"></label>
                          </div>
                        </div>
                      </label>
                    </div>
                </form>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>