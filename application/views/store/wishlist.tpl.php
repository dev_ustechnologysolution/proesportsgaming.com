<?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
<div class="adm-dashbord">
  <div class="container padd0">
    <?php $this->load->view('dashboard/profile_sidebar.tpl.php'); ?>
    <div class="col-lg-10 common_product_list">
      <div class="col-lg-12">
        <?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
        <div class="myprofile-edit row">
          <div class="col-sm-12 wishlist_product_list">
            <h2>My Wishlist</h2>
            <?php  $i = 0; if (!empty($wishlist_item)) { ?>
              <form method="post">
                <div class="cart_list_heading text-left clearfix">
                  <div class="col-md-1 head_title"><label> Image </label></div>
                  <div class="col-md-3 head_title"><label> Name </label></div>
                  <div class="col-md-2 head_title"><label> Option </label></div>
                  <div class="col-md-1 head_title text-right"><label> Quantity </label></div>
                  <div class="col-md-1 head_title text-right"><label> Price </label></div>
                  <div class="col-md-1 head_title text-right"><label> Processing </label></div>
                  <div class="col-md-1 head_title text-right"><label> Total </label></div>
                  <div class="col-md-2 head_title text-right"><label> Action </label></div>
                </div>
                <ul class="c_product_list wishlist_ul">
                  <?php  foreach ($wishlist_item as $key => $item_details) {
                      $variation_amount = 0;
                      $sel_var_amt_array = explode(', ', $item_details['sel_variation_amount']);

                      if(count($sel_var_amt_array) > 0){
                        foreach ($sel_var_amt_array as $sel_var_amt) {
                          $sva = explode('&', $sel_var_amt);
                          $variation_amount += end(explode('_', $sva[2]));
                        }
                      }
                     $price = number_format((float)$item_details['amount'] + $variation_amount, 2, '.', '');
                     $sel_variation = !empty($item_details['sel_variation']) ? explode(', ', $item_details['sel_variation']) : '';
                     $item_details['prdcts_total_price'] = number_format((float) ($item_details['qty'] * ($item_details['amount'] + $item_details['product_fee'])), 2, '.', '');
                    ?>
                    <li class="c_product_list_li item_<?php echo $item_details['id']; ?>">
                      <div class="col-md-1 c_prdct_img">
                        <img src="<?php  echo base_url().'upload/products/'.$item_details['product_image']; ?>" alt="<?php  echo $item_details['product_image']; ?>" class="img img-responsive">
                        <input type="hidden" name="product_id[]" value="<?php  echo $item_details['product_id']; ?>">
                      </div>
                      <div class="col-md-3 c_prdct_name">
                        <a href="<?php  echo base_url() ?>Store/view_product/<?php  echo $item_details['product_id']; ?>">
                          <div>
                            <?php echo (strlen($item_details['product_name']) > 40) ? substr($item_details['product_name'], 0, 40).'...' : $item_details['product_name']; ?>
                          </div>
                        </a>
                      </div>
                      <div class="col-md-2 c_slct_varation">
                        <?php  if (isset($sel_variation) && $sel_variation != '') {
                           foreach ($sel_variation as $k => $sv) {
                             $sv = explode('&', $sv);
                             $var_title = end(explode('_', $sv[0]));
                             $var_opt = end(explode('_', $sv[1]));
                             echo '<div><h5><span class="var_title">'.$var_title.'</span> : <span> '.$var_opt.'</span></h5></div>';
                           }
                         } ?>
                      </div>
                      <div class="text-right col-md-1 c_prdct_item select_prdct_qnt">
                        <div>
                          <input type="hidden" class="cart_item_id" name="cart_item_id[]" value="<?php  echo ($this->session->userdata('user_id') != '') ? $item_details['id'] : $key; ?>">
                          <input type="hidden" name="product_id[]" value="<?php  echo $item_details['product_id']; ?>" class="product_id">
                          <lable style="font-size: 19px;"><?php  echo $item_details['qty']; ?></lable>
                        </div>
                        <div class="item_exceed_err"></div>
                      </div>
                      <div class="text-right col-md-1 c_prdct_price">
                        <lable style="font-size: 19px;"><?php  echo '$<span class="itm_amount">'.$price.'</span>'; ?></lable>
                      </div>
                      <div class="text-right col-md-1 c_prdct_price">
                        <lable style="font-size: 19px;"><?php  echo '$<span class="product_fee">'.$item_details['product_fee'].'</span>'; ?></lable>
                      </div>
                      <div class="text-right col-md-1 c_prdct_totalprice">
                         <lable style="font-size: 19px;"><?php  echo '$<span class="prdcts_total_price">'.number_format((float)($price + $item_details['product_fee']), 2, '.', '').'</span>'; ?></lable>
                      </div> 
                      <div class="text-right col-md-2 c_prdct_chckt_btn">
                        <a class="cmn_btn add_to_cart_frmwishlist" href="<?php  echo base_url(); ?>cart/add_to_cart_from_whislist?id=<?php  echo $item_details['id']; ?>" name="add_to_cart" style="margin-right:10px;" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Add this product into Cart"> <i class="fa fa-shopping-cart"></i><span><i class="fa fa-plus"></i></span> </a>
                        <a class="cmn_btn" href="<?php  echo base_url(); ?>store/removeProductWishlist?id=<?php  echo $item_details['id']; ?>" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Remove from Wishlist"><i class="fa fa-trash-o"></i></a>
                      </div>
                    </li>
                  <?php  } ?>
                </ul>
              </form>
            <?php  } else { ?>
                <div class="emptyitem_lable"><div class='row bidhottest_err'><div class='col-sm-12' style='color: #FF5000;'>Your Wishlist is Empty</div></div></div>
                <br>
                <div class="text-center"><a href="<?php  echo base_url() ?>store" class="cmn_btn" style="font-size: 20px;"> Go to Store</a></div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>