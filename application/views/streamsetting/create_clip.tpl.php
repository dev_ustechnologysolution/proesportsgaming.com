<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/frontend/css/ion_rangeslider.css">
<div class="adm-dashbord">
  <?php
    // $stream_setting=$this->General_model->get_stream_setting($_GET['streamer'],'','default_stream')[0];
    // // print_r($stream_setting->default_stream_channel);exit;
    // $output_dir = str_replace('application\views\streamsetting','upload/streams',__DIR__);
    // $filelist = 'concat:';
    // if ($handle = opendir($output_dir.'/'.$stream_setting->default_stream_channel.'/')) {
    //     while ($entry = readdir($handle)) {
    //         if ($entry != "." && $entry != ".." && $entry != 'FileName.webm' && $entry != 'stream') {
    //           $filelist .= $entry.'|';
    //         }
    //     }
    //     closedir($handle);
    // }
    // $filelist = rtrim($filelist,'|');
    // $output = $output_dir.'/'.$stream_setting->default_stream_channel.'/stream/'.uniqid().'.mp4';
    // $process = exec("ffmpeg -i $filelist -c:a libx264 -c:v copy $output", $result);
    // print_r($process);exit;
  ?>
  <div class="container center padd0">
    <div class="col-md-2"></div>
    <div class="col-md-8">
      <form method="POST" enctype="multipart/form-data" id="clip_form">
        <div id="form-contents">
          <video controls autoplay id="vdo" style="width: 100%">
              <source src="<?php echo $stream_url;?>">
          </video><br>
          <input id="range-slider" type="text" name="range" value="" /><br>
          <textarea class="form-control" name="description" id="description" placeholder="Clip Description"></textarea>
          <!-- <label for="extension">Convert to:</label></br>
          <select name="extension" id="extension">
            <option value="none">Default</option>
            <option value="flv">flv</option>
            <option value="mp4">mp4</option> -->
            <!-- You can add other format here -->
          <!-- </select></br> -->
          <input type="hidden" name="stream_url" id="stream_url" value="<?php echo $stream_url;?>" placeholder="example: flv"/>
          <input type="hidden" name="streamer_id" id="streamer_id" value="<?php echo $_GET['streamer'];?>" placeholder="example: flv"/>
          <input type="hidden" name="lobby_id" id="lobby_id" value="<?php echo $_GET['lobby_id'];?>" placeholder="example: flv"/>
          <input type="hidden" name="extension" id="extension" value="mp4" placeholder="example: flv"/>
          <!-- <label for="start_from">Start From:</label></br> -->
          <input type="hidden" name="start_from" id="start_from" value="" placeholder="example: 00:02:21"/>
            <!-- </br>
            <label for="length">Length:</label></br> -->
            <input type="hidden" name="length" id="length" value="" placeholder="example: 10"/>
          </br>
        </div>                
      </form>
      <input class="btn btn-block lobby_btn" type="submit" id="clip_btn" name="submit" value="Clip">
    </div>
    <div class="col-md-2"></div>
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontend/js/ion_rangeslider.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    setTimeout(event => {
        var player = document.getElementById("vdo");
        var min = 0;
        var max_interval = 1800;
        var step = 5;
        var from = 0;
        var to = 30;
        var videotime = 0;
        var $d5 = $("#range-slider");
        var $d5_buttons = $(".slider-controls");
        $d5.ionRangeSlider({
            type: "double",
            skin: "round",
            min: min,
            max: player.duration,
            max_interval: max_interval,
            step: step,
            from: from,
            to: to,
            drag_interval: true,
            grid: false,
            grid_snap: true,
            prettify: hhmmsss_prettify,
            onStart: function (data) {
                limit_filter(data)
            },
            onChange: function (data) {
                limit_filter(data)
            },
            onFinish: function (data) {
                var length = data.to - data.from
                var start = hhmmsss_prettify(data.from);
                $('#start_from').val(start)
                $('#length').val(length)
                player.currentTime=data.from;
                // if(data.to > data.from){
                //     player.loadVideoById({
                //         videoId: player.getVideoData()['video_id'],
                //         startSeconds: data.from,
                //         endSeconds: data.to
                //     });
                // }
            },
            onUpdate: function (data) {
            }
        })
        function limit_filter(data) {
            let duration = data.to - data.from
            if(duration == 0) {
                $("#d-pscreen").css('opacity','1').css('cursor','pointer')
                $("#d-file").css('opacity','0.5').css('cursor','no-drop')
                $("#d-audio").css('opacity','0.5').css('cursor','no-drop')
                $("#d-gif").css('opacity','0.5').css('cursor','no-drop')
            } else {
                $("#d-pscreen").css('opacity','0.5').css('cursor','no-drop')
                $("#d-file").css('opacity','1').css('cursor','pointer')
                $("#d-audio").css('opacity','1').css('cursor','pointer')
                $("#d-gif").css('opacity','1').css('cursor','pointer')
            }

            if(duration >= 5 && duration <= 10) {
                $("#d-gif").css('opacity','1').css('cursor','pointer')
            } else {
                $("#d-gif").css('opacity','0.5').css('cursor','no-drop')
            }

            let percentage = duration / max_interval * 100
            let color = hsl_col_perc(percentage, 0, 100)
        }
        function hsl_col_perc(percent, start, end) {
            var a = percent / 100,
            b = (start - end) * a,
            c = b + end;
          // Return a CSS HSL string
          return 'hsl('+c+', 100%, 50%)';
        }
        function hhmmsss_prettify (n) {
            var sec_num = parseInt(n, 10); // don't forget the second param
            var hours   = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            var seconds = sec_num - (hours * 3600) - (minutes * 60);
            var milisec = n;

            if (hours   < 10) {hours   = "0"+hours;}
            if (minutes < 10) {minutes = "0"+minutes;}
            if (seconds < 10) {seconds = "0"+seconds;}
            if (milisec < 10) {milisec = "0"+milisec;}
            return hours+':'+minutes+':'+seconds+'.'+(milisec % 1).toFixed(1).substring(2);
        }

        $('#clip_btn').on('click',function(){
          $('#loader').show();
          var formData = $('#clip_form').serialize();
          $.ajax({
              type:"post",
              url: "<?php echo base_url(); ?>Streamsetting/create_clip",
              data:formData,
              success:function(response){
                $('#loader').hide();
                var response = JSON.parse(response);
                var senddata = '<div class="col-sm-12 mb20"><input class="form-control" type="text" readonly disabled value="'+response.url+'"></div><div class="col-sm-12"><div class="col-sm-2"><a data-toggle="tooltip" data-link="'+response.url+'" data-html="true" data-placement="bottom" data-original-title="Copy Link To Clipboard" class="btn cmn_btn rvbtn copy_clip"><span><i class="fa fa-copy"></i></span></a></div><div class="col-sm-2"><a href="http://www.facebook.com/sharer.php?u='+response.url+'" target="_blank" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Share on FaceBook" class="btn cmn_btn rvbtn"><span>&nbsp;<i class="fa fa-facebook"></i>&nbsp;</span></a></div><div class="col-sm-2"><a href="http://twitter.com/share?text='+response.description+'&amp;url='+response.url+'" target="_blank" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Share on Twitter" class="btn cmn_btn rvbtn"><span><i class="fa fa-twitter"></i></span></a></div></div>';
                var modaltitle = 'Share Clip';
                var data = {
                  'modal_title': modaltitle,
                  'senddata':senddata,
                  'tag':'share_clip'
                }
                commonshow(data);
              }
          });
        })
      }, 2000);
    

});
</script>