<!DOCTYPE html>
<html>
<head>
	<!-- <link rel="stylesheet" type="text/css" href="<?php //echo base_url().'assets/frontend/css/bootstrap.css';?>"> -->
	<link href="<?php echo base_url().'assets/frontend/css/video-js.css'; ?>" rel="stylesheet">
	<style type="text/css">
		.vjs-loading-spinner { display : none !important; }
	</style>
</head>
<body style="margin:0">
	<div style="height: 100%;" id="<?php echo $random_id; ?>">
		<div class="stream_error" class="alert alert-danger" style="display: none;"></div>
		<!-- <video id="videojs-flvjs-player" class="obs_streaming video-js vjs-default-skin" <?php echo $autoplay; ?> muted="muted" controls style="width: 100%; height: 100%; display: none;" poster="<?php echo base_url().'assets/frontend/images/stream_offline_img.jpeg'; ?>" preload="auto" onended="var frm = new FormData();frm.append('key', '<?php echo $stream_key; ?>');frm.append('streamer', '<?php echo $user_id; ?>');frm.append('lobby', '<?php echo $lobby_id; ?>');setTimeout(event=>{$.ajax({type: 'post',url: '<?php echo base_url(); ?>Streamsetting/removeStreams',data: frm,processData: false,contentType: false,success: function(response) { endLiveStream('<?php if(isset($user_id)){echo $user_id;}?>','<?php if(isset($lobby_id)){echo $lobby_id;}?>','<?php if(isset($group_bet_id)){echo $group_bet_id;}?>');console.log('removeStream Data',response)},error: function(jqXHR, textStatus, errorMessage) {console.log('Error:' + JSON.stringify(errorMessage));}});},5000);" data-is_record='0'></video> -->
		<video id="videojs-flvjs-player" autoplay muted="muted" class="obs_streaming video-js vjs-default-skin" controls style="width: 100%; height: 100%;" poster="<?php echo base_url().'assets/frontend/images/stream_offline_img.jpeg'; ?>" preload="auto" data-key="<?php echo $stream_key; ?>" data-random_id="<?php echo $random_id; ?>" data-base_url="<?php echo $base_url; ?>" data-user_id="<?php echo $user_id; ?>" data-lobby_id="<?php echo (isset($lobby_id)) ? $lobby_id : ''; ?>" data-group_bet_id="<?php echo (isset($group_bet_id)) ? $group_bet_id : ''; ?>" data-is_record='0'></video>
	</div>
	<input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
</body>
<script type="text/javascript" src="<?php echo base_url().'assets/plugins/jQuery/jQuery-2.1.4.min.js'; ?>"></script>

<?php
  $CI =& get_instance();
  $socketUser = ($CI->chatsocket->current_user != null && $CI->chatsocket->current_user != '') ? $CI->chatsocket->current_user : 0; ?>
  <?php $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://"; ?>
  <script src="<?php echo $protocol . addhttp($CI->chatsocket->domain_name . ':' . $CI->chatsocket->socket_port . '/socket.io/socket.io.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/video.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/videojs-flvjs.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/live_stream.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/flv.min.js'; ?>"></script>
<!-- <script src="<?php // echo base_url().'assets/frontend/js/bootstrap.min.js';?>"></script> --> 
<!-- <script src="<?php // echo base_url().'assets/frontend/';?>js/custom.js"></script> -->
</html>
<style type="text/css">
	html,
	body { height: 100%; }
</style>
<script type="text/javascript">
	// $(document).ready(function(){
	// 	var stream_arr = {
	// 		'stream_key' : '<?php echo $stream_key; ?>',
	// 		'base_url' : '<?php echo $base_url; ?>',
	// 		'random_id' : '<?php echo $random_id; ?>',
	// 	};
	// 	stream_launch(stream_arr);
	// 	// recordstream();
	// });

	// function recordstream(canvas){
	// 	// var canvas = document.querySelector("video");

	// 	// Optional frames per second argument.
	// 	var stream = canvas.captureStream ? canvas.captureStream(25) : canvas.mozCaptureStream(25);
	// 	var recordedChunks = [];
	// 	if(stream.active == false){
	// 		i = 1;
	// 		setTimeout(event => {
	// 		  // console.log("stopping");
	// 			recordstream(canvas);
	// 		  // mediaRecorder.stop();
	// 		}, 5000);
	// 	}else{
	// 		console.log('stream',stream);
	// 		var options = { mimeType: "video/webm; codecs=vp9" };
	// 		mediaRecorder = new MediaRecorder(stream, options);

	// 		mediaRecorder.ondataavailable = handleDataAvailable;
	// 		// setTimeout(event => {
	// 		//   console.log("stopping");
	// 		  // mediaRecorder.stop();
	// 		mediaRecorder.start();
	// 		// }, 5000);

	// 		function handleDataAvailable(event) {
	// 		  console.log("data-available");
	// 		  if (event.data.size > 0) {
	// 		    recordedChunks.push(event.data);
	// 		    console.log(recordedChunks);
	// 		    download();
	// 		  } else {
	// 		    // ...
	// 		  }
	// 		}
	// 		function download() {
	// 		  var blob = new Blob(recordedChunks, {
	// 		    type: "video/webm"
	// 		  });
	// 		  uploadBlob(blob);
	// 		  // var url = URL.createObjectURL(blob);
	// 		  // var a = document.createElement("a");
	// 		  // document.body.appendChild(a);
	// 		  // a.style = "display: none";
	// 		  // a.href = url;
	// 		  // a.download = "test.webm";
	// 		  // a.click();
	// 		  // window.URL.revokeObjectURL(url);
	// 		}
	// 		function uploadBlob(blob) {
	// 		    var formData = new FormData();
	// 		    formData.append('file', blob);
	// 		    formData.append('video-filename', 'FileName.webm');
	// 		    formData.append('stream_key', '<?php echo $stream_key; ?>');
	// 		    $.ajax({
	// 		        type: "post",
	// 		        url: "<?php echo base_url(); ?>Streamsetting/uploadBlob",
	// 		        data: formData,
	// 		        processData: false,
	// 		        contentType: false,
	// 		        success: function(response) {
	// 		            console.log(response)
	// 		        },
	// 		        error: function(jqXHR, textStatus, errorMessage) {
	// 		            console.log('Error:' + JSON.stringify(errorMessage));
	// 		        }
	// 		    });
	// 		}
	// 		// demo: to download after 9sec
	// 		setTimeout(event => {
	// 		  console.log("stopping");
	// 		  mediaRecorder.stop();
	// 		  recordstream(canvas);
	// 		}, 31000);
	// 	}
	// }
</script>
<script type="text/javascript">
    if(typeof io != 'undefined') {
      var socket = io.connect("<?php echo addhttp($CI->chatsocket->domain_name . ':' . $CI->chatsocket->socket_port); ?>",{secure: true}); //connect to socket
    }
    var varSCPath = "<?php echo base_url('xwb_assets'); ?>";
    var socketUser = "<?php echo $socketUser; ?>";
    window.formKey = "<?php echo csFormKey(); ?>";
    if(typeof socket != 'undefined'){
      socket.emit( "socket id", { "user": socketUser } ); // pass the user identity to node
    }
  </script>
<script type="text/javascript">var base_url = '<?php echo base_url(); ?>'; </script>
<script src="<?php echo base_url('xwb_assets/js/chatsocket.js');?>"></script>