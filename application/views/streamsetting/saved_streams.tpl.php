<div class="adm-dashbord">
  <div class="container padd0">
    <?php $this->load->view('dashboard/profile_sidebar_2.tpl.php');?>
    <div class="col-lg-10">
      <div class="col-lg-12">
        <?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
        <?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
        <div class="myprofile-edit membership_div">
          <h2 class="text-center">Saved Stream Recordings</h2>
          <div class="col-lg-12">
            <?php if($success == 1){?>
              <div class="row <?php echo (count($streams) > 0) ? 'bidhottest_err' : '';?>" <?php echo (count($streams) > 0) ? "style='display:block'" : '';?>>
                <?php
                  if(count($streams) > 0){
                    foreach ($streams as $value) {
                ?>
                  <div class="col-lg-6">
                    <div class="record_sream_div">
                      <div class="col-sm-8 mb20 mt20 desc_input">
                        <input class="form-control" type="text" name="description" value="<?php echo $value['description'];?>" id="<?php echo $value['id'];?>" placeholder="Add Description">
                        <p style="display: none;" class="desc_update_msg msg_<?php echo $value['id'];?>"></p>
                      </div>
                      <div class="col-sm-2 mb20 mt20 desc_btn">
                        <button class="btn lobby_btn save_desc" data-toggle = "tooltip" data-placement="bottom" data-original-title = "Save Description"><i class="fa fa-save"></i></button>
                      </div>
                      <div class="col-sm-2 mb20 mt20 saved_stream_clip">
                        <a class="btn lobby_btn saved_stream_create_clip" href="<?php echo base_url().'Streamsetting/clip/'.$value['stream'].'?streamer='.$value['streamer_id'].'&lobby_id='.$value['lobby_id']; ?>" data-toggle = "tooltip" data-placement="bottom" data-original-title = "Clip" target="_blank"><img src = "<?php echo base_url();?>assets/frontend/images/clip_ico.png"></a>
                      </div>
                      <video muted controls>
                        <source src="<?php echo base_url().'upload/streams/recorded_streams/'.$value['stream'];?>" type="video/mp4">
                      </video>
                      <div class="col-sm-12 mb20 mt20">
                        <input class="form-control" type="text" readonly disabled value="<?php echo base_url().'Streamsetting/savedStreams/'.$value['stream'];?>">
                      </div>
                      <div class="col-sm-12 mb20">
                        <div class="col-sm-2">
                          <a data-toggle="tooltip" data-link="<?php echo base_url().'Streamsetting/savedStreams/'.$value['stream'];?>" data-html="true" data-placement="bottom" data-original-title="Copy Link To Clipboard" class="btn cmn_btn rvbtn copy_clip">
                            <span>
                              <i class="fa fa-copy"></i>
                            </span>
                          </a>
                        </div>
                        <div class="col-sm-2">
                          <a href="http://www.facebook.com/sharer.php?u=<?php echo base_url().'Streamsetting/savedStreams/'.$value['stream'];?>" target="_blank" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Share on FaceBook" class="btn cmn_btn rvbtn">
                            <span>
                              &nbsp;<i class="fa fa-facebook"></i>&nbsp;
                            </span>
                          </a>
                        </div>
                        <div class="col-sm-2">
                          <a href="http://twitter.com/share?url=<?php echo base_url().'Streamsetting/savedStreams/'.$value['stream'];?>" target="_blank" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Share on Twitter" class="btn cmn_btn rvbtn">
                            <span>
                              <i class="fa fa-twitter"></i>
                            </span>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php }}else{?>
                  <h5><div class='row bidhottest_err prdctempty'><div class='col-sm-12' style='color: #FF5000;'>Stream recordings not available</div></div></h5>
                <?php }?>
              </div>
            <?php }elseif($success == 2){?>
              <?php if($description != '' && $description != null){echo '<h3 style="text-align: center;">'.$description.'</h3><br>';}?>
              <div class="bidhottest_err">
                <video id="videojs-flvjs-player" class="video-js vjs-default-skin" autoplay muted="muted" controls style="width: 100%; height: 100%" poster="<?php echo base_url().'assets/frontend/images/stream_offline_img.jpeg'; ?>" preload="auto">
                  <source src="<?php echo $stream_url;?>" type="video/mp4">
                </video>
              </div>
            <?php }else{?>
              <h5><div class='row bidhottest_err prdctempty'><div class='col-sm-12' style='color: #FF5000;'><?php echo $error_message;?></div></div></h5>
            <?php }?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
  .img_margin{
    margin:30px 0px;
  }
</style>
<script type="text/javascript">
  $('.copy_clip').on('click', function(){
      var copyText = $(this).attr('data-link');
      var textarea = document.createElement("textarea");
        textarea.textContent = copyText;
        textarea.style.position = "fixed"; // Prevent scrolling to bottom of page in MS Edge.
        document.body.appendChild(textarea);
        textarea.select();
        document.execCommand("copy");
        document.body.removeChild(textarea);
      document.execCommand("copy");
      alert('copied link')
    });
  $('.save_desc').on('click',function(){
    var description = $(this).parent('div').prev('div').find('input').val();
    var id = $(this).parent('div').prev('div').find('input').attr('id');
    $.ajax({
      url:"<?php echo base_url();?>Streamsetting/save_stream_description",
      type:"POST",
      data:{id:id, description:description},
      success:function(response){
        $('.msg_'+response).html('Description Updated');
        $('.msg_'+response).show();
        setTimeout(event=>{
          $('.msg_'+response).hide();          
        },5000)
      }
    });
  });
</script>
