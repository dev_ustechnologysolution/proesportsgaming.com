<?php
$common_settings = json_decode($get_user_detail->common_settings);
$auto_sync_stream = ($common_settings->auto_sync_stream == 'on') ? 'on' : 'off';
$this->load->view('common/flash_msg_show.tpl.php');
$all_social_services=$this->General_model->view_all_data('social_service', 'id', 'asc'); 
$socialicns = array();
foreach ($all_social_services as $key => $value) {
  $socialicns['options'][] = "<option value=".$value['id'].">".ucfirst($value['name'])."</option>";
  $socialicns['iconcode'][] = ['id' => $value['id'], 'name' => $value['name']];
}
?>
<div class="adm-dashbord">
  <div class="container padd0">
    <?php $this->load->view('dashboard/profile_sidebar_2.tpl.php');?>
    <div class="col-lg-10">
      <?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
      <div class="myprofile-edit row stream_settings">
        <div style="color:#FF0000">
          <div class="col-lg-4"></div>
          <div class="col-lg-8">
            <?php $this->load->view('common/show_message') ?>
          </div>
        </div>
        <div class="col-sm-12"><h2>Stream Setting</h2></div>
        <div class="col-sm-12 mb20">
          <div class="title text-center" style="font-size: 30px;">Start own stream</div>
          <div class="stream_settings_stream_first_div">
            <div class="form-group">
              <label for="server_url"><h4>SERVER URL</h4></label>
              <?php
              $host_server = 'rtmp://'.$_SERVER['HTTP_HOST'];
              $rtmp_server_url = $host_server.':1935/live/';
              ?>
              <input type="text" name="server_url" class="form-control input-group" value="<?php echo $rtmp_server_url; ?>" readonly>
            </div>
            <div class="form-group">
              <label for="key"><h4>STREAM KEY</h4></label>
              <input type="text" name="key" class="form-control input-group" value="<?php echo $random_string;?>" readonly>
            </div>
            <ul style="padding-left: 20px;padding-bottom: 10px;">
              <li>Copy this SERVER URL and STREAM KEY and paste it to on OBS software</li>
              <li>Start streaming from OBS software</li>
            </ul>
            <?php if (!empty($default_obs_stream->default_stream_channel)) { ?>
              <div class="form-group sync_stream_btn">
                <a class="cmn_btn" id="reset_streaming" href="<?php echo base_url().'Streamsetting/reset_streaming_key/'; ?>">Reset Key</a>
              </div>
            <?php } else { ?>
              <div class="form-group sync_stream_btn">
                <a class="cmn_btn" id="sync_streaming">Sync</a>
              </div>
            <?php } ?>
            <div class="live_sw_stream form-group">
              <!-- <div id="stream_error" class="alert alert-danger" style="display: none;"></div>
              <video id="obs_streaming" autoplay="" muted="muted" controls style="width: 100%; height: auto; display: none;"></video> -->
              <?php if (!empty($default_obs_stream->default_stream_channel)) { 
                $stream_key = $default_obs_stream->default_stream_channel;
                $new_stream_key = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(8/strlen($x)) )),1,8);
                // $base_url = (base_url() != 'https://proesportsgaming.com/') ? 'https://esportstesting.com/' : base_url();
                $base_url = base_url();
                $random_id = $new_stream_key;
                $user_id = $this->session->userdata('user_id');
              ?>
                
                <!-- <iframe src="https://player.twitch.tv/?channel=<?php //echo $default_stream->default_stream_channel;?>&parent=<?php // echo $_SERVER['HTTP_HOST']; ?>" height="300" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe> -->
                <div style="height: 100%;width: 600px;box-shadow: 0px 0px 3px 0px #ff5000;" id="<?php echo $random_id; ?>">
                  <div class="stream_error" class="alert alert-danger" style="display: none;"></div>
                  <video id="videojs-flvjs-player" autoplay muted="muted" class="obs_streaming video-js vjs-default-skin" controls style="width: 100%; height: 100%;" poster="<?php echo base_url().'assets/frontend/images/stream_offline_img.jpeg'; ?>" preload="auto" data-key="<?php echo $stream_key; ?>" data-random_id="<?php echo $random_id; ?>" data-base_url="<?php echo $base_url; ?>" data-user_id="<?php echo $user_id; ?>" data-lobby_id="<?php echo (isset($lobby_id)) ? $lobby_id : ''; ?>" data-group_bet_id="<?php echo (isset($group_bet_id)) ? $group_bet_id : ''; ?>" data-is_record='0'></video>
                </div>
              <?php } ?>
            </div>
            <div class="form-group">
              <div class="row force_start_stream_main_div" style="margin: 0;">
                <div class="text-right" style="margin-right: 16px;">
                  <h3 style="margin: 0 auto;">Force start camera</h3>
                </div> 
                <div class="text-left">
                  <label class="switch" style="margin: 0;"><input type="checkbox" name="force_start_stream_checkbox" id="force_start_stream_checkbox" <?php echo (!empty($force_start_stream) && $force_start_stream != 'off') ? 'checked' : ''; ?>><span class="slider round"></span></label>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row force_start_stream_main_div" style="margin: 0;">
                <div class="text-right" style="margin-right: 16px;">
                  <h3 style="margin: 0 auto;">Auto Sync Stream</h3>
                </div> 
                <div class="text-left">
                  <label class="switch" style="margin: 0;"><input type="checkbox" name="auto_sync_stream_checkbox" id="auto_sync_stream_checkbox" <?php echo (!empty($auto_sync_stream) && $auto_sync_stream != 'off') ? 'checked' : ''; ?>><span class="slider round"></span></label>
                </div>
              </div>
            </div>
          </div>
          <div class="title text-center" style="font-size: 30px;">Set Default Twitch Login ID</div>
          <div class="stream_settings_stream_first_div">
            <form action="<?php echo base_url(); ?>Streamsetting/update_default_stream" method="POST">
              <div class="form-group">
                <input type="text" name="default_stream" class="form-control input-group" placeholder="Enter Default Twitch Login ID" value="<?php echo ($get_defaultstream[0]->default_stream_channel)? $get_defaultstream[0]->default_stream_channel : '' ; ?>">
              </div>
              <div class="form-group">
                <input type="submit" name="submit" class="lobby_btn" value="Submit">
              </div>
            </form>
          </div> 
        </div>
        <div class="col-sm-12 tip_div_stream_setting mb20">
          <div class="title">
            <nav>
              <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-add_tip_pic" data-toggle="tab" href="#add_tip_pic" role="tab" aria-controls="nav-add_tip_pic" aria-selected="true">Add Tip Gif or Pictures</a>
                <a class="nav-item nav-link" id="nav-add_tip_sound" data-toggle="tab" href="#add_tip_sound" role="tab" aria-controls="nav-add_tip_sound" aria-selected="false">Add Tip Sounds</a>
                <a class="nav-item nav-link" id="nav-add_sub_sound" data-toggle="tab" href="#add_sub_sound" role="tab" aria-controls="nav-add_sub_sound" aria-selected="false">Add Subscription Sounds</a>
              </div>
            </nav>
          </div>
          <div class="tab-content" id="nav-tabContent">
            <div class="stream_settings_tip_first_div tab-pane fade active in" id="add_tip_pic" role="tabpanel" aria-labelledby="nav-add_tip_pic">
              <form class="col-sm-12 padd0 add_tipicon_form" action="<?php echo base_url(); ?>Streamsetting/add_data?add_tipimg=true" method="POST" enctype="multipart/form-data">
                <div class="">
                  <div class="file-upload col-sm-10 padd0" style="padding-right: 0px;">
                    <div class="file-select">
                      <div class="file-select-button" id="fileName">Choose File</div>
                      <div class="file-select-name" id="noFile">No file chosen...</div>
                      <input type="file" name="new_tip_img" id="chooseFile" accept="image/*" required>
                    </div>
                  </div>
                  <div class="col-sm-2 text-center padd0 pull-right" style="padding-right: 0px;">
                    <input type="submit" name="upload" value="Add" class="lobby_btn">
                  </div>
                </div>
              </form>
              <form action="<?php echo base_url(); ?>Streamsetting/update_settings?update_data=tip_icon" method="POST">
                <ul>
                  <?php
                  $icon_arr = explode(',', $get_tipicon[0]->tip_icon_imgs);
                  if (count($get_tipicon[0]->tip_icon_imgs) != 0) {
                  foreach ($icon_arr as $key => $ic) {
                    $file_name = (strpos($ic,'_-image-ini-file_'))? explode('_-image-ini-file_', $ic)[1] : $ic;
                    ?>
                    <li class="tip_li tipicon_<?php echo $get_tipicon[0]->id; ?>" id="">
                      <div>
                        <div class="select_tipicon">
                          <label><input type="checkbox" checkas="tip_icon" name="seltipicon" id="tipicon_<?php echo $get_tipicon[0]->id; ?>" value="<?php echo $ic; ?>" <?php echo ($get_tipicon[0]->tip_selicon_img == $ic) ? 'checked required' : ''; ?>> Set Default </label>
                        </div>
                        <div class="tipicon_image">
                          <div class="tipimage_div_sec">
                            <div class="file_label">
                              <div class="lbl_nm"><i class="fa fa-file-image-o"></i>&nbsp; <?php echo $file_name; ?></div>
                              <span class="edit_file" file_type="tip_icon" file_name='<?php echo $file_name; ?>' original_name="<?php echo $ic; ?>"><i class="fa fa-pencil"></i></span>
                            </div>
                            <div>
                              <img src="<?php echo base_url() ?>/upload/stream_setting/<?php echo $ic; ?>" alt="tipicon" class="img img-responsive">
                            </div>
                          </div>
                        </div>
                        <a class="delete_data" href="<?php echo base_url(); ?>Streamsetting/delete_data?img=tip_icon&&delete_file=<?php echo $ic; ?>" onclick="return confirm('Are you sure to Delete?');">
                          <span><i class="fa fa-trash"></i></span>
                        </a>
                      </div>
                    </li>
                  <?php }
                  ?>
                </ul>
                <div class="save_button text-left">
                  <input type="submit" name="upload" value="Submit" class="lobby_btn">
                </div>
                <?php  } else {
                    echo '<span style="color:red;">Gif not available</span>';
                  } ?>
              </form>
            </div>
            <div class="stream_settings_tip_third_div tab-pane fade" id="add_tip_sound" role="tabpanel" aria-labelledby="nav-add_tip_sound">
              <form class="col-sm-12 add_tipsound_form" action="<?php echo base_url(); ?>Streamsetting/add_data?add_sound=tip_sound" method="POST" enctype="multipart/form-data">
                <div class="">
                  <div class="file-upload add_tip_sound col-sm-10 padd0" style="padding-right: 0px;">
                    <div class="file-select">
                      <div class="file-select-button add_tip_sound" id="fileName">Choose File</div>
                      <div class="file-select-name add_tip_sound no_File" id="no_File">No file chosen...</div>
                      <input type="file" name="new_audio" class="choose_File" id="chooseAudioFile" attachtype="add_tip_sound" accept="audio/*" required>
                    </div>
                  </div>
                  <div class="col-sm-2 text-center padd0 pull-right" style="padding-right: 0px;">
                    <input type="submit" name="upload" value="Add" class="cmn_btn">
                  </div>
                </div>
              </form>
              <form action="<?php echo base_url(); ?>Streamsetting/update_settings?update_data=tip_sound" method="POST" class="has-validation-callback">
                <ul>
                  <?php
                  $tip_sound_arr = explode(',', $get_tipsound[0]->sound_audios);
                  if (count($get_tipsound[0]->sound_audios) != 0) {
                  foreach ($tip_sound_arr as $key => $tc) {
                    $file_name = (strpos($tc,'_-audio-ini-mp3_'))? explode('_-audio-ini-mp3_', $tc)[1] : $tc;
                    ?>
                    <li class="tip_li tipicon_<?php echo $get_tipsound[0]->id; ?>" id="">
                      <div>
                        <div class="select_tipicon">
                          <label><input type="checkbox" name="selsound" id="tipicon_<?php echo $get_tipsound[0]->id; ?>" checkas="tip_sound" value="<?php echo $tc; ?>" <?php echo ($get_tipsound[0]->selsound_audio == $tc) ? 'checked required' : ''; ?>> Set default</label>
                        </div>
                        <div class="tipicon_image">
                          <div class="audio_div_sec">
                            <div class="file_label">
                              <div class="lbl_nm">
                                <i class="fa fa-file-audio-o"></i>&nbsp; <?php echo $file_name; ?>
                              </div>
                              <span class="edit_file" file_type="tip_sound" file_name="<?php echo $file_name; ?>" original_name="<?php echo $tc; ?>"><i class="fa fa-pencil"></i></span>
                            </div>
                            <div>
                              <img src="<?php echo base_url(); ?>upload/stream_setting/audio.png" alt="">
                            </div>
                            <div>
                              <audio controls>
                                <source src="<?php echo base_url(); ?>upload/stream_setting/<?php echo $tc; ?>">
                              </audio>
                            </div>
                          </div>
                        </div>
                        <a class="delete_data" href="<?php echo base_url(); ?>Streamsetting/delete_data?sound=tip_sound&delete_file=<?php echo $tc; ?>"onclick="return confirm('Are you sure to Delete?');">
                          <span><i class="fa fa-trash"></i></span>
                        </a>
                      </div>
                    </li>
                  <?php } ?>
                </ul>
                <div class="save_button text-left">
                  <input type="submit" name="upload" value="Submit" class="lobby_btn">
                </div>
              <?php  } else {
                echo '<span style="color:red;">Tip Sounds not available</span>';
              } ?>
              </form>
            </div>
            <div class="stream_settings_tip_third_div tab-pane fade" id="add_sub_sound" role="tabpanel" aria-labelledby="nav-add_sub_sound">
              <form class="col-sm-12 add_tipsound_form" action="<?php echo base_url(); ?>Streamsetting/add_data?add_sound=sub_sound" method="POST" enctype="multipart/form-data">
                <div class="">
                  <div class="file-upload add_tip_sound col-sm-10 padd0" style="padding-right: 0px;">
                    <div class="file-select">
                      <div class="file-select-button add_tip_sound" id="fileName">Choose File</div>
                      <div class="file-select-name add_tip_sound no_File" id="no_File">No file chosen...</div>
                      <input type="file" name="new_audio" class="choose_File" id="chooseAudioFile" attachtype="add_tip_sound" accept="audio/*" required>
                    </div>
                  </div>
                  <div class="col-sm-2 text-center padd0 pull-right" style="padding-right: 0px;">
                    <input type="submit" name="upload" value="Add" class="cmn_btn">
                  </div>
                </div>
              </form>
              <form action="<?php echo base_url(); ?>Streamsetting/update_settings?update_data=sub_sound" method="POST" class="has-validation-callback">
                <ul>
                  <?php
                  $sub_sound_arr = explode(',', $get_subsound[0]->sound_audios);
                  if (count($get_subsound[0]->sound_audios) != 0) {
                  foreach ($sub_sound_arr as $key => $tc) {
                    $file_name = (strpos($tc,'_-audio-ini-mp3_'))? explode('_-audio-ini-mp3_', $tc)[1] : $tc;
                    ?>
                    <li class="tip_li tipicon_<?php echo $get_subsound[0]->id; ?>" id="">
                      <div>
                        <div class="select_tipicon">
                          <label><input type="checkbox" name="selsound" id="tipicon_<?php echo $get_subsound[0]->id; ?>" checkas="sub_sound" value="<?php echo $tc; ?>" <?php echo ($get_subsound[0]->selsound_audio == $tc) ? 'checked required' : ''; ?>> Set default</label>
                        </div>
                        <div class="tipicon_image">
                          <div class="audio_div_sec">
                            <div class="file_label">
                              <div class="lbl_nm">
                                <i class="fa fa-file-audio-o"></i>&nbsp; <?php echo $file_name; ?>
                              </div>
                              <span class="edit_file" file_type="sub_sound" file_name='<?php echo $file_name; ?>' original_name="<?php echo $tc; ?>"><i class="fa fa-pencil"></i></span>
                            </div>
                            <div><img src="<?php echo base_url(); ?>upload/stream_setting/audio.png" alt=""></div>
                            <div>
                              <audio controls>
                                <source src="<?php echo base_url(); ?>upload/stream_setting/<?php echo $tc; ?>">
                              </audio>
                            </div>
                          </div>
                        </div>
                        <a class="delete_data" href="<?php echo base_url(); ?>Streamsetting/delete_data?sound=sub_sound&delete_file=<?php echo $tc; ?>"onclick="return confirm('Are you sure to Delete?');">
                          <span><i class="fa fa-trash"></i></span>
                        </a>
                      </div>
                    </li>
                  <?php } ?>
                </ul>
                <div class="save_button text-left">
                  <input type="submit" name="upload" value="Submit" class="lobby_btn">
                </div>
              <?php  } else {
                echo '<span style="color:red;">Subscription Sounds not available</span>';
              } ?>
              </form>
            </div>
          </div>
        </div>
        <div class="col-sm-12 mb20">
          <div class="title text-center" style="font-size: 30px;">Add your Social Media Links</div>
            <!-- <div class="text-center row" style="margin-top: 20px;">
              <a class="cmn_btn add-row"> Add Links </a>
            </div> -->
          <div class="stream_settings_stream_first_div created_lobbys_list row">
            <form action="<?php echo base_url(); ?>Streamsetting/update_social_link" method="POST" lobby_id="<?php echo $cl['lobby_id']; ?>" class="update_lobby_custom_url">
              <?php $class = 'none';
              if (!empty($social_links)) {
                $cnt_social = 1;
                foreach ($social_links as $key => $link) {
                  $class = '';
                  $socialicns['options'] = array_map('ucfirst', $socialicns['options']);
                  if($cnt_social == 1){?>
                  <div class="row socialli <?php echo 'remove_'.$link['social_id'].$link['id']; ?>">
                    <div class="col-md-4">
                      <select required class="form-control" name="social_id[]">
                        <option value="">Please Select Social Media</option>
                        <?php foreach ($socialicns['iconcode'] as $key => $sicn) {
                          $select = ($sicn['id'] == $link['social_id'])? 'selected' : '';
                          echo '<option value="'.$sicn['id'].'" '.$select.'>'.ucfirst($sicn['name']).'</option>';
                        } ?>
                      </select>
                    </div>
                    <div class="col-md-7">
                      <input required type="url" name="option_label[]" value="<?php echo $link['social_link'];?>" class="form-control" placeholder="Social Media Link">
                    </div>
                    <div class="col-md-1 padd0 text-left">
                      <a data-id="<?php echo $link['social_id'].$link['id']; ?>" class="cmn_btn add-row rvbtn"> <i class="fa fa-plus"></i> </a>
                    </div>
                  </div>
                  <?php }else{?>
                    <div class="row socialli <?php echo 'remove_'.$link['social_id'].$link['id']; ?>">
                    <div class="col-md-4">
                      <select required class="form-control" name="social_id[]">
                        <option value="">Please Select Social Media</option>
                        <?php foreach ($socialicns['iconcode'] as $key => $sicn) {
                          $select = ($sicn['id'] == $link['social_id'])? 'selected' : '';
                          echo '<option value="'.$sicn['id'].'" '.$select.'>'.ucfirst($sicn['name']).'</option>';
                        } ?>
                      </select>
                    </div>
                    <div class="col-md-7">
                      <input required type="url" name="option_label[]" value="<?php echo $link['social_link'];?>" class="form-control" placeholder="Social Media Link">
                    </div>
                    <div class="col-md-1 padd0 text-left">
                      <a data-id="<?php echo $link['social_id'].$link['id']; ?>" class="cmn_btn remove_field rvbtn"> <i class="fa fa-minus"></i> </a>
                    </div>
                  </div>
                  <?php }?>
                <?php $cnt_social++; } ?>
              <?php }else{ ?>
                <div class="form-group socialli row remove_2">
                  <div class="col-md-4">
                    <select required class="form-control" name="social_id[]">
                      <option value="">Please Select Social Media Type</option>
                        <?php print_r(implode('',$socialicns['options'])); ?>
                    </select>
                  </div>
                  <div class="col-md-7">
                    <input type="url" required class="form-control" name="option_label[]" value="" placeholder="Social Media Link">
                  </div>
                  <div class="col-md-1 padd0 text-left">
                    <a data-id="2" class="cmn_btn add-row rvbtn"> <i class="fa fa-plus"></i> </a>
                  </div>
                </div>
              <?php }?>
              <div class="add_social"></div>
              <div class="row socialicon_div mt20" align="center" style="<?php echo (!empty($social_links)) ? 'display: show' : 'display: none'; ?>">
                <div class="col-md-6">
                  <input type="submit" value="Submit" class="cmn_btn social_submit_btn pull-right">
                </div>
                <div class="col-md-6">
                  <input type="button" value="Remove" class="cmn_btn pull-left rmv_all">
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="col-sm-12 mb20">
          <div class="title text-center" style="font-size: 30px;">Change your Live Lobby Link</div>
          <?php if (count($created_lobbys) > 0) {
            foreach ($created_lobbys as $key => $cl) { ?>
              <div class="stream_settings_stream_first_div created_lobbys_list row">
                <form action="<?php echo base_url(); ?>Streamsetting/update_lobby_url" method="POST" lobby_id="<?php echo $cl['lobby_id']; ?>" class="update_lobby_custom_url">
                    <div class="col-sm-3 bidimg padd0">
                      <img src="<?php echo base_url().'upload/game/'. $cl['game_image'];?>" alt="">
                    </div>
                    <div class="col-sm-9 bid-info">
                      <h5 class="custom_link_label">Custom link : <span class="pull-right">Default ID : <?php echo $cl['lobby_id']; ?></span></h5>
                      <label class="lobby_lnk">
                        <div class="row">
                          <div class="lnk_loc col-sm-8">
                            <span><?php echo base_url().'livelobby/'; ?></span>
                          </div>
                          <div class="col-sm-4">
                            <div class="pull-right">
                              <input type="text" name="custom_name" placeholder="Custom" class="form-control" value="<?php echo $cl['custom_name']; ?>" lobby_id="<?php echo $cl['lobby_id']; ?>" required>
                              <input type="hidden" name="lobby_id" value="<?php echo $cl['lobby_id']; ?>">
                              <input type="hidden" name="old_custom_name" lobby_id="<?php echo $cl['lobby_id']; ?>" value="<?php echo $cl['custom_name']; ?>">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-3">
                            <?php $self="no";
                            $lobbylink = 'Live Stream';
                            if ($cl['is_event'] == 1) {
                              $lobbylink = 'Live Event';
                            } ?>
                            <a href="<?php echo base_url().'livelobby/watchLobby/'.$cl['lobby_id'];?>" class="cmn_btn custom_btn"><?php echo $lobbylink; ?></a>
                          </div>
                          <div class="col-sm-6"></div>
                          <div class="col-sm-3">
                            <input type="submit" name="submit" class="cmn_btn custom_btn submit_custom_url" value="Submit" lobby_id="<?php echo $cl['lobby_id']; ?>">
                            <div class="lobby_exist_loader" lobby_id="<?php echo $cl['lobby_id']?>"><span></span></div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-12">
                            <label class="lobby_exist_error pull-right" style="color: #fff;" lobby_id="<?php echo $cl['lobby_id']?>"></label>
                          </div>
                        </div>
                      </label>
                    </div>
                </form>
              </div>
            <?php }
          } else { ?>
            <div class="stream_settings_stream_first_div created_lobbys_list row">
              <form action="<?php echo base_url(); ?>Streamsetting/update_lobby_url" method="POST" lobby_id="<?php echo $cl['lobby_id']; ?>" class="update_lobby_custom_url">
                  <div class="col-sm-3 bidimg padd0">
                    <img src="<?php echo base_url().'upload/profile_img/'. $get_user_detail->image;?>" alt="">
                  </div>
                  <div class="col-sm-9 bid-info">
                    <h5 class="custom_link_label">Default link :
                      <!-- <span class="pull-right">Default ID : <?php // echo $get_user_detail->id; ?></span> -->
                    </h5>
                    <label class="lobby_lnk">
                      <div class="row">
                        <div class="lnk_loc col-sm-9">
                          <span><?php echo base_url().'livelobby/'.$get_user_detail->custom_name; ?></span>
                        </div>
                        <div class="col-sm-3">
                          <input type="text" name="custom_name" placeholder="Custom" class="form-control default_user_custom_link" value="<?php echo $get_user_detail->custom_name; ?>" required>
                          <input type="hidden" name="old_custom_name" lobby_id="<?php echo $cl['lobby_id']; ?>" value="<?php echo $get_user_detail->custom_name; ?>">

                          <input type="submit" name="submit" class="cmn_btn custom_btn disabled submit_custom_url" value="Submit">
                          <div class="custom_default_link_exist_loader"><span></span></div>
                        </div> 
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <label class="custom_default_link_exist_error pull-right" style="color: #fff;"></label>
                        </div>
                      </div>
                    </label>
                  </div>
              </form>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- <script src="https://cdn.bootcss.com/flv.js/1.5.0/flv.min.js"></script> -->
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/video.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/videojs-flvjs.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/flv.min.js'; ?>"></script>
<script type="text/javascript">
   $(document).ready(function(){
    removeSocialLink();
    var x = 2;
    var max_fields_limit = 10;
    var socialicns = "<?php print_r(implode('',$socialicns['options'])); ?>";
    $(".add-row").click(function(e) {
      e.preventDefault();
      var val = $(this).parent('div').siblings().find('input.optlbl').val();
      if ($('.rvbtn').length < max_fields_limit) { 
        x++; 
        var nwrw = '<div class="form-group socialli row remove_'+x+'"><div class="col-md-4"><select required class="form-control" name="social_id[]"><option value="">Please Select Social Media Type</option>'+socialicns+'</select></div><div class="col-md-7"><input type="url" required class="form-control" name="option_label[]" value="" placeholder="Social Media Link"></div><div class="col-md-1 padd0 text-left"><a data-id="'+x+'" class="cmn_btn remove_field rvbtn"> <i class="fa fa-minus"></i> </a></div></div>';
        $('.add_social').append(nwrw); 
        removeSocialLink();
      } else {
          $('.add-row').attr('disabled','true');
      }
    });

    $('.rmv_all').on('click', function(){
      var senddata = '<div class="col-sm-12"><h2 class="text-left"> Would you like to remove? </h2></div><div class="col-sm-12"><h3><a href="<?php echo base_url(); ?>Streamsetting/remove_all_social_links" id = "rmv_all_ok" class="button no">Yes</a><a class="button no" id = "rmv_all_cancle">No</a></h3></div>';
      var modaltitle = 'Remove All Social Media Links';
      var add_class_in_modal_data = 'rmv_all_modal';
      var data = {
        'add_class_in_modal_data':add_class_in_modal_data,
        'modal_title': modaltitle,
        'senddata':senddata,
        'tag':'commonmodalshow'
      }
      
      commonshow(data);
    });

    function removeSocialLink() {
      ($('.socialli').length > 0) ? $('.socialicon_div').show() : $('.socialicon_div').hide();
      $(".remove_field").click(function(e){           
          data = $(this).attr('data-id');
          $('.remove_'+data).remove();
          // ($('.socialli').length > 0) ? $('.socialicon_div').show() : $('.socialicon_div').hide();
      });
    }
    $('#sync_streaming').click(function () {
      var streaming_key = '<?php echo $random_string; ?>';
      $('#stream_error').hide();
        if (streaming_key != undefined && streaming_key != '') {
          $.ajax({
            url : base_url +'streamsetting/streaming_key_update/',
            data : { 'streaming_key' : streaming_key },
            type: 'POST',
            beforeSend: function(){ loader_show(); },
            success: function(res) {
              loader_hide();
              res = $.parseJSON(res);
              var uid = <?php echo $this->session->userdata('user_id');?>;
              if (res.streaming_key != undefined) {
                var iframe = '<iframe src="'+base_url+'live_stream?channel='+res.streaming_key+'&user_id='+uid+'" height="300" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>';
                // var iframe = '<iframe src="https://player.twitch.tv/?channel='+res.streaming_key+'&parent='+$(location).attr('hostname')+'" height="300" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>';
                $('.live_sw_stream').html(iframe);
                $('.sync_stream_btn').html('<a class="cmn_btn" id="reset_streaming" href="'+base_url+'Streamsetting/reset_streaming_key/">Reset Key</a>');
              }
            }
          });
        }
    });
  });
</script>
