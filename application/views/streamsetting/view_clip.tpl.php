<div class="adm-dashbord">
  <div class="container center padd0">
    <div class="col-md-2"></div>
    <div class="col-md-8">
      <?php if($success == 1){?>
        <form method="POST" enctype="multipart/form-data" id="clip_form">
          <div id="form-contents">
            <div class="stream_error" class="alert alert-danger" style="display: none;"></div>
            <?php if($description != '' && $description != null){echo '<h2 style="text-align: center;">'.$description.'</h2><br>';}?>
            <div class="bidhottest_err">
              <video id="videojs-flvjs-player" class="video-js vjs-default-skin" autoplay muted="muted" controls style="width: 100%; height: 100%" poster="<?php echo base_url().'assets/frontend/images/stream_offline_img.jpeg'; ?>" preload="auto">
                <source src="<?php echo $clip_url;?>" type="video/mp4">
              </video>
            </div>
          </div>                
        </form>
      <?php }else{?>
        <h5><div class='row bidhottest_err prdctempty'><div class='col-sm-12' style='color: #FF5000;'><?php echo $error_message;?></div></div></h5>
      <?php }?>
    </div>
    <div class="col-md-2"></div>
  </div>
</div>
<!-- <script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/video.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/videojs-flvjs.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/flv.min.js'; ?>"></script>
<script type="text/javascript">
  if (flvjs.isSupported()) {
    var videoElement = $('#videojs-flvjs-player')[0];
    var stream_url = "<?php echo $clip_url;?>"
    console.log(videoElement);
    var flvPlayer = flvjs.createPlayer({
      "type": 'flv',
      "url": stream_url
    });
    flvPlayer.attachMediaElement(videoElement);
    $(videoElement).show();
    flvPlayer.load();
    ($(videoElement).attr('autoplay')) ? flvPlayer.play() : '';
  } else {
      $('#form-contents .stream_error').html('Your browser is not supported FLV player').show();
  }
</script> -->