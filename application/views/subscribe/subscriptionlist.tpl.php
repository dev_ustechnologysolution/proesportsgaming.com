<div class="adm-dashbord">
  <div class="container padd0">
     <?php $this->load->view('dashboard/profile_sidebar_2.tpl.php');?>
     <div class="col-lg-10">
       <?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
       <div class="myprofile-edit1 clearfix">
          <h2>My Subscriptions</h2>
          <div class="">
            <div class="subscriblist_main_div">
              <ul class="">
                <?php if ($subscribedlist) { 
                  foreach ($subscribedlist as $key => $sl) {
                    $plan_name = $this->General_model->view_single_row('subscription_plans', array('id'=>$sl->subscribption_plan_id),'*');
                    $plan_amnt = number_format((float) $sl->amount, 2, '.', '');
                    $userimg = $sl->image != '' ? $sl->image : $this->General_model->get_user_img();
                    $name = $sl->display_name != '' ? $sl->display_name : $sl->name;
                    ?>
                    <li class="subscribedto_<?php echo $sl->user_to; ?> subscriblist">
                      <form class="">
                        <div class="subscribed_img_div subscrib_img">
                          <a href="<?php echo base_url(); ?>user/user_profile?fans_id=<?php echo $sl->user_to; ?>" target="_blank">
                            <img src="<?php echo base_url();?>upload/profile_img/<?php echo $userimg; ?>" alt="smile" class="img img-responsive" onerror="this.src='<?php echo base_url(); ?>assets/frontend/images/placeholder-image.png'">
                            <div class="subs_img_overlay_div">
                               <i class="fa fa-link"></i> 
                            </div>
                          </a>
                          <div class="subs_img_hr">
                            <hr>
                          </div>
                        </div>
                        <div class="subscrib_plan">
                          <h4>Plan : <?php echo ucwords($plan_name['title']); ?></h4>
                        </div>
                        <div class="subscrib_amnt">
                          <h4>Amount : $<?php echo $plan_amnt; ?></h4>
                        </div>
                        <div class="subscrib_btn">
                          <a class="lobby_btn subscribe_button commonmodalclick" streamer_name="<?php echo $name;?>" streamer_id="<?php echo $sl->user_to;?>" streamer_img="<?php echo $userimg; ?>">Update</a>
                        </div>
                        <div class="subscrib_btn">
                          <input type="hidden" name="user_subscription_id" value="<?php echo $sl->transaction_id; ?>">
                          <a class="btn cmn_btn user_unsubscription_opt" transaction_id="<?php echo $sl->transaction_id; ?>">Unsubscribe</a>
                          <!-- <input type="submit" name="user_subscription_opt" value="Unsubscribe" class="btn cmn_btn"> -->
                        </div>
                      </form>
                    </li>

                    <?php } } else { ?> <h5><div class='row bidhottest_err'><div class='col-sm-12' style='color: #FF5000;'>Subscriptions list empty</div></div></h5> <?php } ?>
              </ul>
              <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
            </div>
          </div>
       </div>
       <div class="myprofile-edit1 mt20 clearfix">
          <h2>My Subscribers</h2>
          <div class="">
            <div class="subscriblist_main_div">
              <ul class="">
                <?php if ($subscribedbylist) { 
                  foreach ($subscribedbylist as $key => $sl) {
                    $plan_name = $this->General_model->view_single_row('subscription_plans', array('id'=>$sl->subscribption_plan_id),'*');
                    $plan_amnt = number_format((float) $sl->amount, 2, '.', '');
                    $get_user_lobby_data = $this->General_model->view_single_row('user_detail','user_id',$sl->user_from);

                    $userimg = $get_user_lobby_data['image'] != '' ? $get_user_lobby_data['image'] : $this->General_model->get_user_img();
                   ?>
                    <li class="subscribedto subscriblist">
                      <div class="subscribed_img_div subscrib_img">
                        <a href="<?php echo base_url(); ?>user/user_profile?fans_id=<?php echo $sl->user_from  ; ?>" target="_blank">
                          <img src="<?php echo base_url();?>upload/profile_img/<?php echo $userimg; ?>" alt="smile" class="img img-responsive"  onerror="this.src='<?php echo base_url(); ?>assets/frontend/images/placeholder-image.png'">
                          <div class="subs_img_overlay_div">
                             <i class="fa fa-link"></i> 
                          </div>
                        </a>
                        <div class="subs_img_hr">
                          <hr>
                        </div>
                      </div>
                      <div class="subscrib_plan">
                        <h4>Plan : <?php echo ucwords($plan_name['title']); ?></h4>
                        <h4><?php echo ($get_user_lobby_data['display_name']) ? $get_user_lobby_data['display_name'] : $get_user_lobby_data['name']; ?></h4>
                        <span class="postby"><?php echo $get_user_lobby_data['team_name']; ?></span>
                      </div>
                      <div class="subscrib_amnt">                            
                        <h4>Amount : $<?php echo $plan_amnt; ?></h4>
                        <h4><?php echo date('Y-m-d',strtotime($sl->valid_from)); ?></h4>
                      </div>
                    </li>
                  <?php }
                } else { ?>
                  <h5>
                    <div class='row bidhottest_err'>
                      <div class='col-sm-12' style='color: #FF5000;'>Subscribers list empty</div>
                    </div>
                  </h5>
                <?php } ?>
              </ul>
            </div>
          </div>
       </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $('.user_unsubscription_opt').on('click',function(){
    var senddata = '<form action="'+base_url+'Subscribeuser/manually_unsubscribe_plan" method="POST">';
    senddata += '<input type="hidden" name="user_subscription_id" value="'+$(this).attr('transaction_id')+'"><input type="hidden" name="redirect" value="'+base_url+'Subscribeuser/subscriptionlist/"><input type="hidden" name="user_subscription_opt" value="Unsubscribe">';
    senddata += '<h3><input type="submit" class="btn cmn_btn" value="Yes"> &nbsp;&nbsp;&nbsp; <a class="btn cmn_btn no">No</a></h3></div></div>';
    senddata += '</form>';
    var modaltitle = 'Are you sure you want to unsubscribe current plan ?';
    var data = {
      'modal_title': modaltitle,
      'senddata':senddata,
      'tag':'confirm_modal'
    }
    commonshow(data);
  });
  
</script>