<link href='<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar.css' rel='stylesheet' />
<script src='<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar.min.js'></script>
<section class="body-middle innerpage">
    <?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
    <div class="container">
        <br>
        <div class="row">
          <div class="product_listing_first_div frntend_eventcalendar">
            <div class="event_calendar" id='calendar'></div>
            <div class="eve_key_frontend">
            <?php if (!empty($keylist)) {
              foreach ($keylist as $key => $kl) { ?>
                <div class="evelist">
                  <div class="eve_img">
                    <img src="<?php echo base_url().'upload/event_key_img/'.$kl->key_image; ?>" alt="<?php echo $kl->key_image; ?>">
                  </div>
                  <div class="eve_cnt"><?php echo $kl->key_title; ?></div>
                </div>
              <?php }
            } ?>
            </div>
          </div>
        </div>
        <div class="row header2" align="center">
          <div class="col-lg-3 col-sm-3 col-md-3 onlyborder"></div>
          <div class="col-lg-6 col-sm-6 col-md-6">
              <h2>Pro Esport Tournaments</h2>
          </div>
          <div class="col-lg-3 col-sm-3 col-md-3 onlyborder"></div>
        </div>
        <?php if (!empty($archive_lobby_list)) { ?>
            <?php $this->load->view('common/live_lobby_filter.tpl.php'); ?>
            <div class="bidhottest hottestgame playstore_game_list_front event_list">
              <li class="expiredtime_event row hidden_timer_row" style="display: none;">
                <div class="countdown_with_day rgclose clearfix">
                </div>
              </li>
              <li class="row livelobby_home_row hidden_row" style="display: none;">
                <div class="col-sm-3 bidimg padd0 lobby_stream_iframe_div">
                   <iframe style="display: none;" class="live_stream_iframe" src="" height="auto" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
                   <div style="height: 100%; display: none;" id="">
                     <div class="stream_error" class="alert alert-danger" style="display: none;"></div>
                     <video id="videojs-flvjs-player" autoplay muted="muted" class="video-js vjs-default-skin" controls style="width: 100%; height: 100%;" poster="<?php echo base_url().'assets/frontend/images/stream_offline_img.jpeg'; ?>" preload="auto" data-key="" data-random_id="" data-base_url="" data-user_id="" data-lobby_id="" data-group_bet_id="" data-is_record='0'></video>
                  </div>
                </div>
                <div class="col-sm-3 bid-info">
                   <div class="lb_list">
                      <div class="dly_lst_verticle">
                         <h3></h3>
                         <p></p>
                      </div>
                   </div>
                </div>
                <div class="col-sm-3 divPrice">
                   <div class="lb_list">
                      <div class="dly_lst_verticle">
                         <div class="game-price premium">
                            <h3></h3>
                         </div>
                         <p class="text-center margin0" style="color: #ff5807;"> Challenges Accepted:  <?php echo '<span>0</span> V <span>0</span>'; ?></p>
                      </div>
                   </div>
                </div>
                <div class="col-sm-1 category_img">
                   <div class="lb_list">
                      <div class="dly_lst_verticle">
                         <img src="<?php echo base_url(); ?>upload/all_images/XBOX.png" class="img img-responsive game_cat_img">
                         <div class="evetime">
                            <label class="text-center"></label>
                          </div>
                      </div>
                   </div>
                </div>
                <div class="col-sm-2 padd0 lobby_join_btn_div">
                   <div class="lb_list">
                      <div class="dly_lst_verticle">
                         <div class="join_btn_div_lobby row">
                            <div class="join_btn clearfix animated rotateIn">
                               <a href="" data-original-title="'Join Live Event'" data-placement="bottom" data-toggle="tooltip">Join Stream</a>
                            </div>
                         </div>
                         <div class="clearfix primum-play play-n-rating lobby_device_tag device_id">
                            <a class="animated-box"></a>
                         </div>
                      </div>
                   </div>
                </div>
              </li>
              <ul>
                <?php
                echo $this->session->flashdata('err_lobby');
                unset($_SESSION['err_lobby']);
                foreach($archive_lobby_list as $key => $val) {
                  ($val->creator_timezone != '') ? date_default_timezone_set($val->creator_timezone) : '';
                  $creator_time = (in_array($val->creator_timezone, array('EST', 'MST')) && $global_dst_option['option_value'] == 'on' && $global_dst_hour['option_value'] == "+1") ? date('Y-m-d H:i:s', strtotime('+1 hours')) : date('Y-m-d H:i:s');
                  $price = ($val->is_event == 1) ? $val->event_price : $val->price;
                  $lobby_bet_price = number_format((float) $price, 2 , '.', '');
                  ?>
                  <li class="expiredtime_event row">
                    <?php if ($val->registration_status == 1) {
                      // echo strtotime($val->start).'<br>'.strtotime(date('Y-m-d\TH:i:sP')).'<br>';
                      // echo date('Y-m-d H:i:s', strtotime($val->start)).'<br>'.date('Y-m-d H:i:s').'<br>';
                      // echo date('Y-m-d H:i:s').'>'.$creator_time.'<br>';
                      
                      if (strtotime($val->start) > strtotime($creator_time)) { ?>
                        <div class="countdown_with_day clearfix" id="countdown_day_<?php echo $key; ?>" creator_time="<?php echo $creator_time; ?>">
                          <div class="countdown_ele hide">
                            <span class="year"><?php echo date('Y', strtotime($val->start)); ?></span>
                            <span class="month"><?php echo date('m', strtotime($val->start)); ?></span>
                            <span class="day"><?php echo date('d', strtotime($val->start)); ?></span>
                            <span class="hour"><?php echo date('H', strtotime($val->start)); ?></span>
                            <span class="minute"><?php echo date('i', strtotime($val->start)); ?></span>
                            <span class="second"><?php echo date('s', strtotime($val->start)); ?></span>
                          </div>
                          <div class="clearfix tim_elediv animated rotateIn eventremaing_label">
                            <label>Registration Close in</label>
                          </div>
                          <span class="livetimer"></span>
                        </div>
                      <?php } else { ?>
                        <div class="countdown_with_day clearfix">
                          <div class="clearfix tim_elediv animated rotateIn">
                            <label>Event in Progress - Registration Open</label>
                          </div>
                        </div>
                      <?php }
                    } else { ?>
                      <div class="countdown_with_day rgclose clearfix">
                        <div class="clearfix tim_elediv animated rotateIn">
                          <label>Event in Progress - Registration Closed</label>
                        </div>
                      </div>
                    <?php } ?>
                  </li>
                  <!-- }?> -->
                  <?php if (!empty($this->session->userdata('timezone'))) {
                    date_default_timezone_set($this->session->userdata('timezone'));
                    // $val->start = (in_array($val->creator_timezone, array('EST', 'MST')) && $global_dst_option['option_value'] == 'on' && $global_dst_hour['option_value'] == "+1") ? date('Y-m-d H:i:s', strtotime('+1 hours',strtotime($val->start))) : date('Y-m-d H:i:s', strtotime($val->start));
                  } ?>
                  <li class="row livelobby_home_row">
                    <div class="col-sm-3 bidimg padd0 lobby_stream_iframe_div">
                      <?php if ($val->stream_status == 'enable' && ((empty($val->obs_stream_channel) && !empty($val->stream_channel)) || (!empty($val->obs_stream_channel) && empty($val->stream_channel)) || (!empty($val->obs_stream_channel) && !empty($val->stream_channel)))) { 
                          if($val->stream_type == '1'){
                                 $stream_url = 'https://player.twitch.tv/?autoplay=false&channel='.$val->stream_channel.'&parent='.$_SERVER['HTTP_HOST'];
                        ?>
                        <iframe class="live_stream_iframe" src="<?php echo $stream_url; ?>" height="auto" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
                        <?php }else if($val->stream_type == '2'){
                            $stream_key = $val->obs_stream_channel;
                            $new_stream_key = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(8/strlen($x)) )),1,8);
                            $base_url = base_url();
                            $random_id = $new_stream_key;
                            $user_id = $val->lobby_creator;
                            // if(!empty($result->lobby_id)){$lobby_id = $result->lobby_id;}
                            // if(!empty($result->group_bet_id)){$group_bet_id = $result->group_bet_id;}
                        ?>
                            <div style="height: 100%;" id="<?php echo $random_id; ?>">
                               <div class="stream_error" class="alert alert-danger" style="display: none;"></div>
                               <video id="videojs-flvjs-player" autoplay muted="muted" class="obs_streaming video-js vjs-default-skin" controls style="width: 100%; height: 100%;" poster="<?php echo base_url().'assets/frontend/images/stream_offline_img.jpeg'; ?>" preload="auto" data-key="<?php echo $stream_key; ?>" data-random_id="<?php echo $random_id; ?>" data-base_url="<?php echo $base_url; ?>" data-user_id="<?php echo $user_id; ?>" data-lobby_id="<?php echo (isset($lobby_id)) ? $lobby_id : ''; ?>" data-group_bet_id="<?php echo (isset($group_bet_id)) ? $group_bet_id : ''; ?>" data-is_record='0'></video>
                            </div>
                      <?php }} else { ?>
                        <img src="<?php echo base_url().'upload/game/'. $val->game_image;?>" alt="">
                      <?php } ?>
                    </div>
                    <div class="col-sm-3 bid-info">
                      <div class="lb_list">
                        <div class="dly_lst_verticle">
                          <h3><?php echo $val->game_name; ?></h3>
                          <p><?php echo $val->sub_game_name; ?></p>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-3 divPrice">
                      <div class="lb_list">
                        <div class="dly_lst_verticle">
                          <div class="game-price premium">
                            <h3>
                              <?php $lobby_bet_price = number_format((float) $val->price, 2 , '.', '');
                              echo ($lobby_bet_price > 0) ? CURRENCY_SYMBOL.$lobby_bet_price : 'FREE';
                              ?>
                            </h3>
                          </div>
                          <p class="text-center margin0" style="color: #ff5807;"> Challenges Accepted:  <?php echo '<span>'.$val->game_type.'</span> V <span>'.$val->game_type.'</span>'; ?></p>
                          <!-- <p class="best_of"> <?php// echo ($val->best_of != null) ? 'Best of '.$val->best_of : ''; ?></p> -->
                          <?php
                          if ($val->game_timer != '' && $val->game_timer != "0000-00-00 00:00:00") {
                            $diff = strtotime($val->game_timer) - time();
                            if (time() < strtotime($val->game_timer)) { ?>
                            <?php } ?>
                            <h3 class="timer custom_timer" data-seconds-left="<?php echo $diff; ?>"></h3>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-1 category_img">
                      <div class="lb_list">
                        <div class="dly_lst_verticle">
                           <?php echo ($val->cat_img != '') ? '<img src="'.base_url().'upload/all_images/'.$val->cat_img.'" class="img img-responsive game_cat_img">' : ''; ?>
                           <div class="evetime">
                              <label class="text-center">Start : <?php echo strtoupper(date('D m/d', strtotime($val->start))); ?> <br> <?php echo strtoupper(date('h:i A', strtotime($val->start))); ?> </label>
                            </div>     
                        </div>
                      </div>
                    </div>
                    <?php $lobbylink =  ($val->is_event == 1) ? 'Live Event' : 'Live Stream'; ?>
                    <div class="col-sm-2 padd0 lobby_join_btn_div">
                       <div class="lb_list">
                          <div class="dly_lst_verticle">
                             <div class="join_btn_div_lobby row">
                                <div class="join_btn clearfix animated rotateIn">
                                   <a href="<?php echo base_url().'livelobby/watchLobby/'.$val->id;?>" data-original-title="<?php echo "Join ".$lobbylink; ?>" data-placement="bottom" data-toggle="tooltip">Join Stream</a>
                                </div>
                             </div>
                             <div class="clearfix primum-play play-n-rating lobby_device_tag device_id">
                                <a class="animated-box"><?php echo $val->device_id; ?></a>
                             </div>
                             <!-- <div class="primum-play text-right desc_sec_div"><a class="animated-box" target="_blank">Rules</a><div class="description" style="display:none;">test</div></div> -->
                             <?php echo (!empty($val->description) && $val->description != null) ? '<div class="primum-play text-right desc_sec_div"><a class="animated-box" target="_blank"  href="'.base_url().'livelobby/rulesAdd/'.$val->id.'">Rules</a><div class="description" style="display:none;">'.$val->description.'</div></div>' : ''; ?>
                          </div>
                       </div>
                    </div>
                  </li>
                <?php } ?>
              </ul>
              <input type="hidden" id="is_event" value="1">
            </div>
        <?php } else {
            echo "<h5><div class='row bidhottest_err'><div class='col-sm-12' style='color: #FF5000;'>Lobby Not Available</div></div></h5>";
        } ?>
    </div>
</section>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/video.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/videojs-flvjs.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/flv.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/countdown_with_day.js"></script>

<script>
  var events_arr = <?php echo $events; ?>;
  $('#calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    selectable: true,
    events: events_arr,
    eventRender: function(event, element, view) {
      (event.color != '') ? element.css('background',event.color) : '';
      (event.is_archive == 1) ? element.css('color','black') : '';

    },
    eventClick: function (event, element, view) {
      var evetype = (event.event_type == "lobby_event") ? 'lobby' : ((event.event_type == "custom_event") ? 'custom_event' : '');
      var modal_title = ' <div class="mdl_eve_title text-center">'+event.is_proplayre+event.title+'</div>';
      var go_event = (event.url != '' && event.event_is_expired == 'no') ? '<a class="cmn_btn" href="'+event.url+'" target="_blank"> Go to the Event </a>' : '';
      (event.description != '') ? go_event += '<a class="cmn_btn" target="_blank" href="'+base_url+'livelobby/rulesAdd/'+event.id+'?type='+evetype+'"> Rules </a>' : '';
      var senddata = '<div class="col-sm-12 event_modal"><div class="mdl_eve_goto_eve col-sm-12 text-center">'+go_event+'</div></div>';
      // senddata += '<div class="col-sm-12 event_modal"><div class="eve_cal_desc text-left">' + event.description + '</div>';

      (event.event_bg_img != undefined && event.event_bg_img != '') ? senddata += "<div style='' class='col-sm-12 margin-tpbtm-30'><img src='"+base_url+"upload/event_key_img/"+event.event_bg_img+"' class='img img-responsive'></div>" : '';
      senddata += '</div>';
      var data = {
        'add_class_in_modal_data': 'view_event_data',
        'add_class_in_modal' : 'view_event',
        'modal_title' : modal_title,
        'senddata' : senddata,
        'tag' : 'commonmodalshow'
      }
      commonshow(data);
      return false;
    },
    eventMouseover: function(event, element, view) {
      if (event.is_proplayre != '') {
        $("body").append('<div class="tooltipevent tooltip_eve_calendar"><div class="eve_cal_title">' + event.is_proplayre + '</div>');
        var bg_color = (event.bg_color.indexOf('_') != -1) ? 'linear-gradient(200deg, '+event.bg_color.split('_')[0]+', '+event.bg_color.split('_')[1]+') 0% 0% / 200% 200%' : event.bg_color;
        $('.tooltipevent').css({"z-index": 10000, "background": bg_color, "color": "#fff", "border": "1px solid #fff" });
        $(this).mousemove(function(e) { $('.tooltipevent').css('top', e.pageY + 10).css('left', e.pageX + 20); });
      }


      // (event.event_bg_img != undefined && event.event_bg_img != '') ? $('.tooltipevent').append("<div style='text-align: center;padding: 10px 0;' class='clearfix'><img src='"+base_url+"upload/event_key_img/"+event.event_bg_img+"' class='img img-responsive'></div>"):'';

    },
    eventMouseout: function(calEvent, jsEvent) {
      $(this).css('z-index', 8);
      $('.tooltipevent').remove();
      // $('.key_shw_div').html('').hide();
    }
  });
</script>
