<div class="adm-dashbord">
<div class="container padd0">
   <?php $this->load->view('dashboard/profile_sidebar.tpl.php');?>
   <div class="col-lg-10">
      <div class="col-lg-12 padd0">
         <div>
            <div class="col-lg-12">
               <?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
               <div class="myprofile-edit">
                <h2>Tip Detail <a class="lobby_btn pull-right" href="<?php echo base_url().'trade/download_tip_history'?>">Download History</a></h2>
                <div class="mygamelist collapse_margin">
                  <h2 data-toggle="collapse" data-target="#tip_sent_collapse" class="tip_sent_lable">Tip Sent <span class="pull-right"><i class="fa fa-caret-up"></i></span></h2>
                  <div class="collapse_border">
                    <div id="tip_sent_collapse" class="tradelist collapse in">
                  <?php if (count($tip_send) != 0) { ?>
                      <ul class="trade_history_ul">
                        <?php 
                        foreach ($tip_send as $key => $gsh) {
                          $name = $gsh->display_name ? $gsh->display_name : $gsh->name;
                          $myname = $my_data['display_name'] ? $my_data['display_name'] : $my_data['name'];
                          $time = $this->User_model->custom_date(array('date' => $gsh->created,'format' => 'm-d-Y'));
                          $amount =  number_format((float)$gsh->tip_amt, 2, '.', '');
                          $my_img = $this->General_model->get_user_img($my_data['user_id']);
                          // echo "<pre>"; print_r($my_data['user_id']);
                          $reciever_img = $this->General_model->get_user_img($gsh->user_to);
                           ?>
                            <li class="clearfix">
                              <div class="col-lg-2 col-sm-2 col-md-3 bidimg padd0">
                                <img src="<?php echo base_url(); ?>upload/profile_img/<?php echo $my_img; ?>" alt="">
                              </div>
                              <div class="col-lg-3 col-sm-3 col-md-3">
                                <h3><?php echo $myname; ?> </h3>
                                <h3>ACC# <?php echo $my_data['account_no']; ?></h3>
                                <span class="postby"><?php echo $my_data['team_name']; ?></span>
                              </div>
                              <div class="col-lg-2 col-sm-2 col-md-3">
                                <div><?php echo $time; ?></div>
                                <div><span class="postby">Amount: $<?php echo $amount; ?></span></div>
                              </div>
                              <div class="col-lg-3 col-sm-3 col-md-3">
                                <div class="text-right">
                                  <h3><?php echo $name; ?> </h3> <h4>ACC# <?php echo $gsh->account_no; ?></h4>
                                  <span class="postby"><?php echo $gsh->team_name; ?></span>
                                </div>
                              </div>                              
                              <div class="col-lg-2 col-sm-2 col-md-2 bidimg padd0">
                                 <div class="pull-right">
                                  <img src="<?php echo base_url(); ?>upload/profile_img/<?php echo $reciever_img; ?>" alt="">
                                 </div>
                              </div>
                           </li>
                        <?php } ?>
                      </ul>
                  <?php } ?>
                    </div>
                    </div>
                </div>
                  <div class="mygamelist collapse_margin">
                    <h2 data-toggle="collapse" data-target="#tip_received_collapse" class="tip_received_lable">Tip Received <span class="pull-right"><i class="fa fa-caret-up"></i></span></h2>
                    <div class="collapse_border">
                    <div id="tip_received_collapse" class="tradelist collapse in">
                  <?php if (count($tip_receive) != 0) { ?>
                      <ul class="trade_history_ul">
                        <?php 
                        foreach ($tip_receive as $key => $grh) {
                          $name = $grh->name;
                          if ($grh->display_name != '') {
                            $name = $grh->display_name;
                          }
                          $time = $this->User_model->custom_date(array('date' => $grh->created,'format' => 'm-d-Y'));
                          $amount =  number_format((float)$grh->tip_amt, 2, '.', ''); 
                          $rec_img = $this->General_model->get_user_img($gsh->user_from);?>
                          <li class="clearfix">
                              <div class="col-lg-2 col-sm-6 col-md-3 bidimg padd0">
                                <img src="<?php echo base_url(); ?>upload/profile_img/<?php echo $rec_img; ?>" alt="">
                              </div>
                              <div class="col-lg-3 col-sm-6 col-md-3">
                                <h3><?php echo $name; ?></h3>
                                <span class="postby"><?php echo $grh->team_name; ?></span>
                              </div>
                              <div class="col-lg-3 col-sm-6 col-md-3">
                                <h3>ACC# <?php echo $grh->account_no; ?></h3>
                              </div>
                              <div class="col-lg-4 col-sm-4 col-md-2">
                                 <div class="text-right">
                                    <div><?php echo $time; ?></div>
                                    <div><span class="postby">Amount: $<?php echo $amount; ?></span></div>
                                 </div>
                              </div>
                           </li>
                        <?php } ?>
                      </ul>
                  <?php } ?>
                    </div>
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
