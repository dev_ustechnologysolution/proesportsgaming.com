<div class="adm-dashbord">

    <div class="container padd0">
    <?php
     $value = $friend_profile;
     $default_stream = $this->General_model->get_stream_setting($value->user_id,'','default_stream');
     $default_stream = $default_stream[0];
  ?>
        <?php $this->load->view('dashboard/profile_sidebar.tpl.php'); ?>

        <div class="col-lg-10">

            <div class="col-lg-12 padd0">

                <div> 
                  <?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
                    <div class="col-lg-11">

                        <div class="myprofile-edit">

                          <!-- <h2>My Profile</h2> -->

                          <?php $this->load->view('common/show_message'); ?>

                          <div class="col-lg-12">
                           
                            <div class="row" style="margin-left: 8%;">

                                <div class="form-group profile_image_div">
                                  <div class="row">
                                    <div class="col-lg-12">

                                    <div class="friend_profile_img" style="text-align:center;">

                                      <img src="<?php if(isset($value->image) && $value->image != null && $value->image != ''){ echo base_url('/upload/profile_img/'.$value->image); }else{echo base_url('/upload/profile_img/default_profile_img.png');} ?>" id="k">

                                    </div>
                                    
                                    </div>
                                    <div class="col-lg-12 text-center "> 
                                    <?php //echo "<pre>"; print_r($value);?>                  
                                    <p class="white"><?php echo (isset($value->display_name) && $value->display_name != '') ? $value->display_name : $value->name; ?></p>
                                  </div>
                                    <div class="col-lg-12 padd10">
                                     
                                    <div class="text-center">
                                      <input type="hidden" name="channel_name" id="channel_name" value="<?php echo $default_stream->default_stream_channel; ?>">
                                       <?php if($default_stream->default_stream_channel != '')
                                      { ?>
                                      <iframe src="https://player.twitch.tv/?channel=<?php echo $default_stream->default_stream_channel; ?>&parent=<?php echo $_SERVER['HTTP_HOST']; ?>" height="400" width="94%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
                                        <?php } ?>
                                    </div>
                                 
                                  </div>
                                  <div class="col-lg-12 ac-abt-row">
                                    <div class="col-lg-4 friend_plyr_id"> 
                                      <label class="font30 pull-left">Account NO #<?php echo $value->account_no; ?></label>
                                    </div>
                                    <div class="col-lg-8 padd10" style="text-align: right;">
                                      
                                        <a data-id="<?php echo $value->user_id; ?>" href = "<?php echo base_url().'livelobby/add_about_me/'.$this->session->userdata('user_id'); ?>" target="_blank" class="lobby_btn">About Me</a>&nbsp;&nbsp;

                                      <input type="hidden" name="user_id" value="<?php echo $this->session->userdata('user_id')?>">

                                    </div>
                                    <br><br><br><br>
                                  </div>
                                    <?php if(isset($value->team_name) && $value->team_name != null && $value->team_name != ''){?>
                                        <div class="col-lg-12">
                                          <p>Esports Team</p>
                                        </div>
                                        <div class="col-lg-12 friend_plyr_id">
                                          <label class="white no-padding"><?php echo $value->team_name; ?></label>
                                        </div>
                                      <?php }?>
                                    </div>
                                </div>
                             </div>
                             <div class="col-lg-12"  align="center" style="display:none;">
                                    <div class="input-group back_button">
                                      <a href="javascript:history.go(-1)">Back</a>
                                    </div>
                                </div>
                                    
                          </div>

                        </div>

                    </div>

                </div>

            </div>   

        </div>

    </div>

</div>
<script>
 var ch_name = $('#channel_name').val();
 if(ch_name=="")
 {
    $('.profile_image_div').addClass("center-desc");
 }

</script>
<style>
.font30 {
   font-size:30px !important;
   padding:0px !important;
}
iframe{
  border: 1px solid #ff5000;
}
</style>



