<?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
<div class="adm-dashbord">
    <div class="container">
        <?php $this->load->view('dashboard/profile_sidebar_2.tpl.php'); ?>
        <div class="col-lg-10">
            <div class="col-lg-12">
                <?php $this->load->view('common/common_dash_tab.tpl.php'); ?>
                <div class="myprofile-edit">
                    <h2>Edit Profile</h2>
                    <div class="col-lg-12">
                      	<div class="row">
                            <form role="form" action="<?php echo base_url().'user/profile_edit';?>" class="signup-form prof_ed_form" method="POST" enctype="multipart/form-data">
                                <input type="hidden" id="is_admin" value="<?php echo $user_detail[0]['is_admin']?>">
                                <div class="form-group">
                                    <label class="col-lg-4">Name</label>
                                    <div class="input-group col-lg-8">                                 
                                        <input readonly type="text" class="form-control" name="name" value="<?php if(isset($user_detail[0]['name'])) echo $user_detail[0]['name']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-4 web_display_name"> <label>Display Name </label>
                                        <label class="switch">
                                            <input  type="checkbox" <?php if(isset($user_detail[0]['display_name_status']) && $user_detail[0]['display_name_status'] == 1) echo 'checked'; ?> name="display_name_status">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <div class="input-group col-lg-8 web_display_name input_display">
                                        <input onkeyup="check_valid_name(this.value,'display_name');" type="text" class="form-control" name="display_name" value="<?php if(isset($user_detail[0]['display_name'])) echo $user_detail[0]['display_name']; ?>" <?php if(isset($user_detail[0]['display_name_status']) && $user_detail[0]['display_name_status'] == 0) echo 'readonly'; ?>>
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-lg-4">Discord Name & Tag#</label>
                                    <div class="input-group col-lg-8">
                                        <input type="text" class="form-control" name="discord_id" id="discord_id" value="<?php if(isset($user_detail[0]['discord_id'])) { echo $user_detail[0]['discord_id']; } ?>" >
                                    </div>
                                </div><div class="form-group">
                                    <label class="col-lg-4">Esports Team Name</label>
                                    <div class="input-group col-lg-8 input_team">
                                        <input type="text" onkeyup="check_valid_name(this.value,'team_name');" class="form-control" name="team_name" value="<?php if(isset($user_detail[0]['team_name'])) echo $user_detail[0]['team_name']; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4">Email</label>
                                    <div class="input-group col-lg-8">
                                        <input required type="text" class="form-control" name="email" value="<?php if(isset($user_detail[0]['email'])) echo $user_detail[0]['email']; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4">Mail Notifications</label>
                                    <div class="input-group col-lg-8 notifications_div">
                                        <div class="col-lg-4">
                                            <div class="title">Friends Mail</div>
                                            <div>
                                                <label class="switch"><input type="checkbox" <?php if(isset($mail_categories_fun[0]['friends_mail']) && $mail_categories_fun[0]['friends_mail'] == 'yes') echo 'checked'; ?> name="friends_mail" value="friends_mail"><span class="slider round"></span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="title">Esports Team Mail</div>
                                            <div><label class="switch">
                                                  <input type="checkbox" <?php if(isset($mail_categories_fun[0]['team_mail']) && $mail_categories_fun[0]['team_mail'] == 'yes') echo 'checked'; ?> name="team_mail" value="team_mail">
                                                  <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="title">Challenge Mail</div>
                                            <div><label class="switch">
                                                  <input type="checkbox" <?php if(isset($mail_categories_fun[0]['challenge_mail']) && $mail_categories_fun[0]['challenge_mail'] == 'yes') echo 'checked'; ?> name="challenge_mail" value="challenge_mail">
                                                  <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                
                                <div class="form-group">
                                    <label class="col-lg-4">New Password</label>
                                    <div class="input-group col-lg-8">
                                        <input type="password" class="form-control" name="password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4">Phone</label>
                                    <div class="input-group col-lg-8">
                                        <input  type="text" class="form-control" name="number" value="<?php if(isset($user_detail[0]['number'])) echo $user_detail[0]['number']; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4">Location</label>
                                    <div class="input-group col-lg-8">
                                        <input type="text" class="form-control" name="location" value="<?php if(isset($user_detail[0]['location'])) echo $user_detail[0]['location']; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4">Country</label>
                                    <div class="input-group col-lg-8">
                                         <select id="countries_states1" class="form-control" name="country" required>
                                            <?php echo '<option value="">Select Country</option>';
                                            foreach ($country_list as $key => $value) {
                                                echo '<option value="'.$value['country_id'].'"';
                                                if ($value['country_id'] == $user_detail[0]['country']) { echo ' selected="selected"'; }
                                                echo '>'.$value['countryname'].'</option>';   
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4">State</label>
                                    <div class="input-group col-lg-8">
                                         <select class="form-control" name="state" id="state" required>
                                            <?php 
                                                foreach ($state as $st) {
                                                    $selected = (isset($user_detail[0]['state']) && $user_detail[0]['state'] != '' && $user_detail[0]['state'] == $st['stateid']) ? 'selected' : '';
                                                    echo '<option value="'.$st['stateid'].'" '.$selected.'>'.$st['statename'].'</option>';
                                                }
                                            ?>
                                         </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4">City</label>
                                    <div class="input-group col-lg-8">
                                        <input type="text" class="form-control" name="city" id="city" value="<?php echo (!empty($user_detail[0]['city'])) ? $user_detail[0]['city'] : ''; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4">Postal Code</label>
                                    <div class="input-group col-lg-8">
                                        <input type="text" class="form-control" name="zip_code" id="zip_code" value="<?php echo (!empty($user_detail[0]['zip_code'])) ? $user_detail[0]['zip_code'] : ''; ?>" required>
                                    </div>
                                </div>
    							<div class="form-group">
                                    <label class="col-lg-4">TAX ID</label>
                                    <div class="input-group col-lg-8">
                                        <input type="text" class="form-control" name="taxid" id="taxid" value="<?php echo (!empty($user_detail[0]['taxid'])) ? $user_detail[0]['taxid'] : ''; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4">Time Zone</label>
                                    <div class="input-group col-lg-8">
                                        <?php
                                        $timezoneArray = timezone_identifiers_list();
                                        $select = '<select name="timezone" class="form-control" id="form-control" required>';
                                        $select .= '<option value="">Select Time zone</option>';
                                        $timezoneArray = preg_grep("/America/", $timezoneArray, PREG_GREP_INVERT);
                                        array_push($timezoneArray, "HST", "America/Anchorage", "America/Los_Angeles", "MST", "America/Chicago", "EST");
                                        foreach ($timezoneArray as $key => $ta) {
                                            $newta = str_replace(["HST", "America-Anchorage", "America-Los_Angeles", "MST", "America-Chicago", "EST"],["U.S-Hawaii-Aleutian", "U.S-Alaska", "U.S-Pacific", "U.S-Mountain", "U.S-Central", "U.S-Eastern"],str_replace('/','-',$ta));
                                            $select .='<option value="'.$ta.'"';
                                            $select .= ($ta == $user_detail[0]['timezone'] ? ' selected' : '');
                                            $select .= '>'.$newta.'</option>';
                                        }
                                        $select.='</select>';
                                        echo $select;
                                        ?>
                                    </div>
                                </div>
                                <?php if (!empty($global_dst_option) && $global_dst_option['option_value'] == 'on') { ?>
                                    <div class="form-group daylight_saving clearfix">
                                        <label class="col-lg-4">Daylight Saving</label>
                                        <div class="col-lg-8 padd0">
                                            <div>
                                                <label class="switch">
                                                    <input  type="checkbox" name="daylight_saving_settings" class="daylight_saving_settings_chk" <?php echo ($common_settings->daylight_option == 'off') ? 'value="off"': 'checked value="on"'; ?>>
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="form-group">
                                    <label class="col-lg-4 col-sm-4 col-md-4">Profile Picture</label>
                                    <div class="col-lg-8 col-sm-8 col-md-8 form-upload">
                                        <div class="profileImg">
                                            <img src="<?php if(isset($user_detail[0]['image'])) echo base_url().'upload/profile_img/'.$user_detail[0]['image']?>" height="50" width="50" id="k"/>
                                        </div>
                                        <div class="fileUpload">
                                            <span>Change Picture</span>
                                            <label style="display:none"></label>
                                            <input type="file" name="image" class="upload" onchange="previewFile()" data-validation=" mime size" data-validation-allowing="jpg, png, gif" data-validation-max-size="10M" data-validation-error-msg-size="You can not upload images larger than 10MB"/>
                                        </div>
                                        <div>
                                            <h4>Max Size 10MB </h4>
        								</div>
                                           <div style="color:red;margin-left:192px;"><?php echo $this->session->flashdata('msg'); ?></div>
                                    </div>
                                </div>
                                <div class="btn-group">
                                    <div class="col-lg-8 pull-right padd0">
                                        <input type="hidden" name="old_image" value="<?php if(isset($user_detail[0]['image'])) echo $user_detail[0]['image']; ?>" >
                                        <input type="submit" value="Update" class="btn-update">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$('.daylight_saving_settings_chk').change(function(){
    if ($(this).is(':checked')) {
        $(this).val('on');
    } else {
        $(this).val('off');
    }
});
function check_valid_name(name,type) {
    var is_admin = $('#is_admin').val();
    if(is_admin == 0) {
        var res_name = name.toLowerCase();
        var final_name = res_name.replace(/\s+/g, '-');
        if(final_name == 'pro-esports-gaming' || final_name == 'pro-esports' || final_name == 'proesports' || final_name == 'proesportsgaming' || final_name == 'esportsteam' || final_name == 'esports-team'){
            if(type == 'display_name' || type == 'first_name'){
                $('.input_display').find('input:text').val('');  
                $('.input_display').append('<label class="display_label">You dont use '+type.replace('_', ' ')+' as Pro Esports Gaming word.</label>');
            } else if(type == 'team_name' || type == 'last_name') {
                $('.input_team').find('input:text').val(''); 
                $('.input_team').append('<label class="team_label">You dont use '+type.replace('_', ' ')+' as Pro esports gaming word.</label>');
            }
        } else {
            $('.display_label').remove();
            $('.team_label').remove();
        }
    }
}
</script>


