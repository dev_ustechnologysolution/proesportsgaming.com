<section class="body-middle innerPage">
	<div class="container">
    <div class="row">
            <!-- left column -->
            <div class="col-md-12">
			<?php $this->load->view('admin/common/show_message') ?>
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                 <!-- <h3 class="box-title">Add New Game</h3>-->
                </div><!-- /.box-header -->
                <!-- form start -->
                <form  action="<?php echo base_url(); ?>video/videoUpdate" method="post" enctype="multipart/form-data">
                <div class="box-body">
                 <div class="form-group">
                        <label for="game_id">Game Name</label>
                        <select type="text" class="form-control" id="game_id" name="game_id" required>
                        <option value="">Select State</option>
                        <?php foreach ($game as $val) { ?>
                         <option value="<?php if(isset($val['id'])) echo $val['id'] ?>" <?php if(isset($video[0]['game_id']) && $video[0]['game_id']==$val['id']) echo 'selected' ?>><?php echo $val['name']; ?></option>
                         <?php } ?>
                         </select>
                 </div>
					
                    <div class="form-group">
                        <label for="video_url">Video Url</label>
                         <input type="text" placeholder="video url" id="video_url" class="form-control" name="video_url" required="required" value="<?php if(isset($video[0]['video_url']))echo $video[0]['video_url'];?>">
                     </div>
                      
                     </div><!-- /.box-body -->

                  <div class="box-footer">
                   <input type="hidden" name="id" value="<?php if(isset($video[0]['id'])) echo $video[0]['id'];?>">
                    <button class="btn btn-primary" type="submit">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

              <!-- Form Element sizes -->
                </div><!--/.col (left) -->
            
              </div><!-- /.box -->
            </div><!--/.col (right) -->
             </div>
    </div>
</section>