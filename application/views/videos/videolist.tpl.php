<section class="body-middle innerpage">
    <div class="container">
        <div class="row header1">
            <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
            <div class="col-lg-4 col-sm-4 col-md-4"><h2>LATEST ACTION</h2></div>
            <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
        </div>
        <br>
        <br>
        <br>
        <div class="row videos_list">
          <?php if (!empty($video_dt)) {
            foreach ($video_dt as $vd) {
              if ($vd->type == '0') {
                $a_href = ($vd->is_home == 1) ? base_url() : $vd->video_url;
                $w = explode('&',substr($vd->video_url,stripos($vd->video_url,"?")+3)); @$watch=$w[0].'?'.$w[2];
                if ($w[0] != 'U') { ?>
                  <div class="col-lg-3 col-sm-6 col-md-3 videoimgs">
                     <a href="<?php echo $a_href; ?>" target="_blank">
                        <img src="https://img.youtube.com/vi/<?php echo $w[0]; ?>/mqdefault.jpg" alt="" class="img img-responsive">
                        <div class="float-caption">
                          <h5 class="animated-box"><?php echo $vd->tag; ?></h5>
                       </div>
                     </a>
                  </div>
                <?php }
              } elseif($vd->type == '1') {
                $a_href1 = ($vd->is_home == 1) ? base_url() : base_url().'livelobby/watchLobby/'.$vd->lobby_id; ?>
                <div class="col-lg-3 col-sm-6 col-md-3 videoimgs">
                   <a href="<?php echo $a_href1; ?>" target="_blank">
                      <iframe src="https://player.twitch.tv/?channel=<?php echo $vd->twitch_username; ?>&parent=<?php echo $_SERVER['HTTP_HOST']; ?>" height="auto" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
                     <div class="float-caption">
                        <h5 class="animated-box"><?php echo $vd->tag; ?></h5>
                     </div>
                   </a>
                </div>
              <?php }else{ 
               $lby_detail = $this->General_model->lobby_details($vd->lobby_id)[0];
               $lobby_fan_detail = $this->General_model->view_single_row('lobby_fans', array('stream_status' => 'enable', 'user_id' => $lby_detail['user_id']),'*');
               $random_id = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(8/strlen($x)) )),1,8);
                $a_href1 = ($vd->is_home == 1) ? base_url() : base_url().'livelobby/watchLobby/'.$vd->lobby_id; 
                // $base_url = (base_url() != 'https://proesportsgaming.com/') ? 'https://esportstesting.com/' : base_url();
                $base_url = base_url();
                ?>
                <div class="col-lg-3 col-sm-6 col-md-3 videoimgs" id="<?php echo $random_id; ?>">
                   <a href="<?php echo $a_href1; ?>" target="_blank">
                      <video id="videojs-flvjs-player" autoplay class="obs_streaming video-js vjs-default-skin" muted="muted" controls style="width: 100%; object-fit: cover;" poster="<?php echo base_url().'assets/frontend/images/stream_offline_img.jpeg'; ?>" preload="auto" data-key="<?php echo $lobby_fan_detail['obs_stream_channel']; ?>" data-random_id="<?php echo $random_id; ?>" data-base_url="<?php echo $base_url; ?>" data-user_id="<?php echo $lby_detail['user_id'];?>" data-is_record="0"></video>
                     <div class="float-caption">
                        <h5 class="animated-box"><?php echo $vd->tag; ?></h5>
                     </div>
                   </a>
                </div>
              <?php }
            }
          } ?>
       </div>        
    </div>
</section>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/video.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/videojs-flvjs.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/flv.min.js'; ?>"></script>
<style type="text/css">
  
  .videoimgs a img,
  .videoimgs a video,
  .videoimgs a iframe{height: 150px; object-fit: cover;}
</style>
