function admin_loader_show() { $('#adminloader').show(); }
function admin_loader_hide() { $('#adminloader').hide(); }
$(window).load(function() { $("#adminloader").fadeOut("slow"); });

var hash = window.location.hash;
hash && $('ul.nav a[href="' + hash + '"]').tab('show');

$('.nav-tabs a').click(function (e) {
  $(this).tab('show');
  var scrollmem = $('body').scrollTop();
  window.location.hash = this.hash;
  $('html,body').scrollTop(scrollmem);
});

$('.select_2').select2();
if ($(".dashboard_vid").length) {
    $(".dashboard_vid")[0].play();
    $(".dashboard_vid")[0].autoplay = true;
}
$('.admin_map_th .switch input.map_status').click(function() {
    var default_id = $('.default_id').val();
    var custom_id = $('.custom_id').val();
    if (this.checked) {
        $('tbody tr:first td').show();
        $('.custom_map_video').hide();
        $.ajax({
            url: base_url + '/admin/Content/changeStatus/' + default_id + '/' + custom_id,
            success: function(result) {}
        });
    } else {
        $('tbody tr:first td').hide();
        $('.custom_map_video').show();
        $.ajax({
            url: base_url + '/admin/Content/changeStatus/' + custom_id + '/' + default_id,
            success: function(result) {}
        });
    }
});
($('.enable_id').val() == '0') ? $('.custom_map_video').hide() : '';

($('.custom_datetimepicker').length) ? $('.custom_datetimepicker').datetimepicker({ format: 'YYYY-MM-DD HH:mm:ss'}) : '';

function nonempty(id_valu) {
    var para = document.getElementById(id_valu).value;
    $("#" + id_valu).css("border", "");
    if (para != "") {
        return true;
    } else {
        $("#" + id_valu).css("border", "1px solid #ff0000");
        document.getElementById(id_valu).focus();
        return false;
    }
}
$('.cal_event_form .select_only_date .onlydate').click(function () {
    ($(this).is(':checked')) ? $('.eve_datetimepicker').datetimepicker({ viewMode: 'days', format: 'YYYY-MM-DD' }) : $('.eve_datetimepicker').datetimepicker({ format: 'YYYY-MM-DD HH:mm:ss'});
});

//user/change_password					  

$(document).ready(function() {
    $("#chang_pass").click(function() {
        var current_password = $("#current_password").val();
        var new_password = $("#new_password").val();
        var confirm_password = $("#confirm_password").val();
        var base_url = $("#base_url").val();
        if (nonempty('current_password')) {
            if (nonempty('new_password')) {
                if (nonempty('confirm_password')) {
                    $.ajax({
                        url: base_url + "admin/user/change_password",
                        type: "POST",
                        data: {
                            current_password: current_password,
                            new_password: new_password,
                            confirm_password: confirm_password
                        },
                        success: function(data) {
                            $("#ajax_msg").html(data);
                        },
                    });
                }
            }
        }
    });

});

//For deactive
$(document).on('click', '.deactiveMenuStatus', function() {

    var menu_id = $(this).attr("id");

    $.ajax({

        url: base_url + "admin/menu/change_to_deactive",

        type: "POST",

        data: {
            menu_status_id: menu_id
        },

        success: function(data) {

            console.log(data);
            var url = base_url + 'admin/menu/menuEdit/' + menu_id;
            $('#menu_status_td_' + menu_id).html('<span class="label label-danger">Deactive</span>');
            htm = '';
            htm = '<button class="btn btn-block btn-success btn-xs activeMenuStatus" id="' + menu_id + '" >Active</button>';
            htm = htm + '<a href="' + url + '"><button class="btn btn-block btn-primary btn-xs" id="<?php echo $value[id]; ?>" > Edit</button></a><button onclick="myFunction(<?php echo $value[id]; ?>)" class="btn btn-block btn-danger delete btn-xs" > Delete</button>';
            $('#menu_action_td_' + menu_id).html(htm);

        },
    });
});



//For Active
$(document).on('click', '.activeMenuStatus', function() {

    var menu_id = $(this).attr("id");

    $.ajax({

        url: base_url + "admin/menu/change_to_active",

        type: "POST",

        data: {
            manustatus_id: menu_id
        },

        success: function(data) {

            console.log(data);
            var url = base_url + 'admin/menu/menuEdit/' + menu_id;
            $('#menu_status_td_' + menu_id).html('<span class="label label-success">Active</span>');
            htm = '';
            htm = '<button class="btn btn-block btn-danger btn-xs deactiveMenuStatus" id="' + menu_id + '" >Deactive</button>';
            htm = htm + '<a href="' + url + '"><button class="btn btn-block btn-primary btn-xs" id="<?php echo $value[id]; ?>" > Edit</button></a><button onclick="myFunction(<?php echo $value[id]; ?>)" class="btn btn-block btn-danger delete btn-xs" > Delete</button>';
            $('#menu_action_td_' + menu_id).html(htm);

        },
    });
});


//For Deactive Customer service status
$(document).on('click', '.deactiveCsStatus', function() {

    var cs_id = $(this).attr("id");

    $.ajax({

        url: base_url + "admin/customer_service/change_to_deactive",

        type: "POST",

        data: {
            cs_status_id: cs_id
        },

        success: function(data) {

            console.log(data);
            // var url = base_url+'admin/customer_service/menuEdit/'+menu_id;
            $('#cs_status_td_' + cs_id).html('<span class="label label-danger">Deactive</span>');
            htm = '';
            htm = '<button class="btn btn-block btn-success btn-xs activeCsStatus" id="' + cs_id + '" >Active</button>';
            htm = htm;
            $('#cs_action_td_' + cs_id).html(htm);

        },
    });
});



//For Active Customer service status
$(document).on('click', '.activeCsStatus', function() {

    var cs_id = $(this).attr("id");

    $.ajax({

        url: base_url + "admin/customer_service/change_to_active",

        type: "POST",

        data: {
            csstatus_id: cs_id
        },

        success: function(data) {

            console.log(data);
            $('#cs_status_td_' + cs_id).html('<span class="label label-success">Active</span>');
            htm = '';
            htm = '<button class="btn btn-block btn-danger btn-xs deactiveCsStatus" id="' + cs_id + '" >Deactive</button>';
            htm = htm;
            $('#cs_action_td_' + cs_id).html(htm);

        },
    });
});



//for add row in game add page

var i = 1;
$(document).ready(function() {

    $('.add_field_button').click(function(e) {
        e.preventDefault();
        i++;
        if (i <= 6) {
            $('.input_fields_wrap').append('<div><input type="file" name="image[]" class="form-control"/><a href="#" class="remove_field">Remove</a></div>');
        } else {
            alert('You can add only six images');
        }

    });

    $('.input_fields_wrap').on("click", ".remove_field", function(e) {
        e.preventDefault();
        i = 1;
        $(this).parent('div').remove();
    })

});
$('#edit_game_update').click(function() {
	var game_already_exist = $('.already_exist').html();
	if (game_already_exist != undefined && game_already_exist.length>0) {
		alert(game_already_exist);
		$('#game_name').focus();
		return false;
	}
});
/*game add page validation*/
$('#game_add').click(function() {
    //alert('opk');
    var game_name = $('#name').val();
    var game_description = $('#description').val();
    var game_image = $('#file').val();
    var game_category = $('#category').val();
    var game_already_exist = $('.already_exist').html();
	if (game_already_exist.length>0) {
		alert(game_already_exist);
		$('#game_name').focus();
		return false;
	}
    if (game_name == '') {
        alert('please provide the game title');
        $('#game_name').focus();
        return false;
    } else if (game_description == '') {
        alert('please provide the game description');
        $('#game_description').focus();
        return false;
    } else if (game_image == '') {
        alert('please provide the game image');
        $('#game_image').focus();
        return false;
    } else if (game_category == '') {
        alert('please select a game category');
        $('#game_category').focus();
        return false;
    }
});

/* sub game add page validation*/
$('#submit_subgame').click(function() {

    var game_name = $('#subgame').val();
    var subgame_name = $('#sub_game_name').val();
    if (game_name == '') {
        alert('please select a game');
        $('#game_name').focus();
        return false;
    } else if (subgame_name == '') {
        alert('please provide the sub game name');
        $('#subgame_name').focus();
        return false;
    }
});


var _URL = window.URL || window.webkitURL;

$("#upload_sponsered_by_logo").change(function(e) {

    var file, img;

    if ((file = this.files[0])) {

        img = new Image();

        img.onload = function() {

            $("#sponsered_by_logo_size").html(this.width + "-" + this.height);

        };

        img.src = _URL.createObjectURL(file);

    }

});


$("#upload_banner").change(function(e) {

    var file, img;

    if ((file = this.files[0])) {

        img = new Image();


        img.onload = function() {
            $("#banner_size").html(this.width + "-" + this.height);
        };

        img.src = _URL.createObjectURL(file);

    }

});


function adsimage_size_validation() {
	var v = $("#sponsered_by_logo_size").html();
    var banner = $("#banner_size").html();
    //Check whether HTML5 is supported.
    if (v != '') {
        var r = v.split('-');
        if (r[0] == 120 && r[1] == 80) {
            return true;
        } else {
            alert('Your Image Size ' + r[0] + 'px * ' + r[1] + 'px \n\n Please Upload 120px * 80px Size Image');
            return false;
        }
    }
    if (banner != '') {
        var new_banner = banner.split('-');
        if (new_banner[0] == 200 && new_banner[1] == 300) {
            return true;
        } else {
            alert('Your Banner Size ' + new_banner[0] + 'px * ' + new_banner[1] + 'px \n\n Please Upload 200px * 300px Size Image');
            return false;
        }
    }
}


function validate_game() {
	var game_name, old_game_name;
    game_name = $('.game-name').val();
    old_game_name = $('.old_game_name').val();
    $.ajax({
        url: base_url + "admin/game/validate_game",
        type: "POST",
        data: {
            game_name: game_name
        },
        success: function(data) {
            var data = $.parseJSON(data);
            if (old_game_name != game_name) {
	            if (data.game_already_exist == 'yes') {
	                $('.already_exist').html(game_name + ' Game is already exist').show();
	            } else {
	                $('.already_exist').hide().html('');
	            }
            } else {
            	    $('.already_exist').hide().html('');
            }
        },
    });
}

$('form.game_category_name input[name="category_name"]').on('focus blur', function(){
    var game_cat_name, old_game_cat_name;
    game_cat_name = $(this).val();
    old_game_cat_name = $('.old_game_cat_name').val();
    $.ajax({
        url: base_url + "admin/game/validate_game_cat",
        type: "POST",
        data: {
            game_cat_name: game_cat_name
        },
        success: function(data) {
            var data = $.parseJSON(data);
            $('.already_exist').html('').hide();
            if (data.cat_already_exist == 'yes') {
                $('.already_exist').html(game_cat_name + ' is already exist').show();
                if (old_game_cat_name != undefined && old_game_cat_name == game_cat_name) {
                    $('.already_exist').html('').hide();
                } else {
                    $('form.game_category_name input[name="category_name"]').val('');
                }
            }
        },
    });
});

function lobbyadsimage_size_validation() {
    var v = $("#sponsered_by_logo_size").html();
    if (v != '') {
        var r = v.split('-');
        if (r[0] <= 400 && r[1] <= 300) {
            return true;
        } else {
            alert('Your Image Size ' + r[0] + 'px * ' + r[1] + 'px \n\n Please Upload 400px * 300px Size Image');
            return false;
        }
    }
}


// Banner js

$('#selected_banner_image').click(function() {
    $('.image_banner').show('normal');
    $('.banner_youtube_link_input, .banner_youtube_link_duration, .video_banner, .video_duration, .upload_progress_bar, .progress.progress-sm').hide('normal').val('');
});
$('#selected_banner_video').click(function() {
    $('.banner_youtube_link_input, .banner_youtube_link_duration, .image_banner').hide('normal').val('');
    $('.video_banner, .video_duration, .upload_progress_bar').show('normal');
});
$('#selected_banner_youtube_link').click(function() {
   $('.banner_youtube_link_input, .banner_youtube_link_duration').show('normal');
   $('.browse_new_image, .old_image_preview, .video_banner, .image_banner, .video_duration, .upload_progress_bar').hide('normal').val(''); 
});


var BannerImageisChecked = $('#edit_banner_image').prop('checked');
var BannerVideoisChecked = $('#edit_banner_video').prop('checked');
var BannerYoutubeLinkisChecked = $('#selected_banner_youtube_link').prop('checked');

if (BannerImageisChecked == true) {
    $('.browse_new_image, .old_image_preview').show('normal');
    $('.banner_youtube_link_input, .banner_youtube_link_duration, .video_banner, .video_duration, .old_video_preview, .upload_progress_bar, .progress.progress-sm').hide('normal');
    $('.browse_new_image').click(function() {
        $('.image_banner').show('normal');
        $('.video_banner, .video_duration, .old_image_preview, .upload_progress_bar, .progress.progress-sm').hide('normal').val('');
    });
    $('#edit_banner_image').click(function() {
        $('.browse_new_image, .old_image_preview').show('normal');
        $('.banner_youtube_link_input, .banner_youtube_link_duration, .video_banner, .video_duration, .upload_progress_bar, .progress.progress-sm').hide('normal').val('');
    });
} else {
    $('#edit_banner_image').click(function() {
        $('.banner_youtube_link_input, .banner_youtube_link_duration, .video_banner, .old_image_preview, .old_video_preview, .browse_new_image, .browse_new_video, .progress.progress-sm').hide('normal').val('');
        $('.image_banner').show('normal');
    });
}
if (BannerYoutubeLinkisChecked == true) {
    $('.old_image_preview, .image_banner, .browse_new_image, .upload_progress_bar').hide('normal').val('');
    $('.banner_youtube_link_input, .banner_youtube_link_duration').show('normal');
}
if (BannerVideoisChecked == true) {
    $('.banner_youtube_link_input, .banner_youtube_link_duration, .old_image_preview, .image_banner, .browse_new_image, .upload_progress_bar').hide('normal').val('');
    $('.old_video_preview, .browse_new_video').show('normal');
    $('.browse_new_video').click(function() {
        $('.video_banner, .video_duration, .upload_progress_bar').show('normal');
        $('.old_video_preview, .progress.progress-sm').hide('normal');
    });
    $('#edit_banner_video').click(function() {
        $('.old_video_preview, .browse_new_video').show('normal');
        $('.banner_youtube_link_input, .banner_youtube_link_duration, .old_image_preview, .image_banner, .browse_new_image, .progress.progress-sm, .upload_progress_bar').hide('normal').val('');
    });
} else {
    $('#edit_banner_video').click(function() {
        $('.banner_youtube_link_input, .banner_youtube_link_duration, .old_image_preview, .image_banner, .browse_new_image, .browse_new_video, .progress.progress-sm, .upload_progress_bar').hide('normal').val('');
        $('.video_banner, .video_duration').show('normal');
    });
}



// Ads js

$('#selected_youtube_ads').click(function() {
    $('#youtube_link').show('normal').attr('required', true);
    $('#youtube_link_title').show('normal').attr('required', true);
    $('#upload_youtube_lable').show('normal');

    $('#upload_commercial').hide('normal').removeAttr('required');
    $('#upload_commercial_lable').hide('normal');
    $('#commercial_title').hide('normal').removeAttr('required');
});
$('#selected_commercial_ads').click(function() {
    $('#upload_commercial').show('normal').attr('required', true);
    $('#upload_commercial_lable').show('normal');
    $('#commercial_title').show('normal').attr('required', true);
    $('#youtube_link').hide('normal').removeAttr('required');
    $('#youtube_link_title').hide('normal').removeAttr('required');
    $('#upload_youtube_lable').hide('normal');
});
$('#youtube_link').keyup(function() {
    $('#upload_commercial').val('');
    $('#commercial_title').val('');
    $('#old_commercial_file').html('');
    $('#upload_commercial_vid').val('');
});
$('#youtube_link_title').keyup(function() {
    $('#upload_commercial').val('');
    $('#commercial_title').val('');
    $('#old_commercial_file').html('');
    $('#upload_commercial_vid').val('');
});
$('#upload_commercial').click(function() {
    $('#youtube_link').val('');
    $('#youtube_link_title').val('');
});
$('#commercial_title').keyup(function() {
    $('#youtube_link').val('');
    $('#youtube_link_title').val('');
});

var SelectedYoutubeAds = $('#edit_selected_youtube_ads').prop('checked');
var SelectedCommercialAds = $('#edit_selected_commercial_ads').prop('checked');

if (SelectedYoutubeAds == true) {
    $('#youtube_link').show('normal').attr('required', true);
    $('#upload_youtube_lable').show('normal');
    $('#youtube_link_title').show('normal').attr('required', true);

    $('#change_upload_commercial').hide('normal');
    $('#upload_commercial').hide('normal').removeAttr('required');
    $('#upload_commercial_lable').hide('normal').removeAttr('required');
    $('#commercial_title').hide('normal').removeAttr('required');
    $('#old_commercial_file').hide('normal').removeAttr('required');

    $('#edit_selected_youtube_ads').click(function() {
        $('#youtube_link').show('normal').attr('required', true);
        $('#upload_youtube_lable').show('normal');
        $('#youtube_link_title').show('normal').attr('required', true);
        $('#commercial_title').hide('normal').removeAttr('required');
        $('#upload_commercial_lable').hide('normal');
        $('#upload_commercial').hide('normal').removeAttr('required');
        $('#change_upload_commercial').hide('normal');
        $('#old_commercial_file').hide('normal');
    });
} else {
    $('#edit_selected_youtube_ads').click(function() {
        $('#youtube_link').show('normal').attr('required', true);
        $('#upload_youtube_lable').show('normal');
        $('#youtube_link_title').show('normal').attr('required', true);

        $('#commercial_title').hide('normal').removeAttr('required');
        $('#upload_commercial_lable').hide('normal');
        $('#upload_commercial').hide('normal').removeAttr('required');
        $('#change_upload_commercial').hide('normal');
        $('#old_commercial_file').hide('normal');
    });
}
if (SelectedCommercialAds == true) {
    $('#old_commercial_file').show('normal');
    $('#change_upload_commercial').show();
    $('#youtube_link').hide('normal').removeAttr('required');
    $('#upload_youtube_label').hide('normal');
    $('#youtube_link_title').hide('normal').removeAttr('required');
    $('#change_upload_commercial').click(function() {
        $('#commercial_title').show('normal').attr('required', true);
        $('#upload_commercial_lable').show('normal');
        $('#upload_commercial').show('normal').attr('required', true);
        $('#old_commercial_file').hide('normal');
    });
    $('#edit_selected_commercial_ads').click(function() {
        $('#old_commercial_file').show('normal');
        $('#change_upload_commercial').show('normal');
        $('#youtube_link').hide('normal').removeAttr('required');
        $('#upload_youtube_lable').hide('normal');
        $('#youtube_link_title').hide('normal').removeAttr('required');
    });
} else {
    $('#edit_selected_commercial_ads').click(function() {
        $('#commercial_title').show('normal').attr('required', true);
        $('#upload_commercial_lable').show('normal');
        $('#upload_commercial').show('normal').attr('required', true);
        $('#youtube_link').hide('normal').removeAttr('required');
        $('#upload_youtube_lable').hide('normal');
        $('#youtube_link_title').hide('normal').removeAttr('required');
    });
}
function IsNumeric(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
       return false;

    return true;
}

$("#add-row").click(function(){
    var index_no = Number($('.data_tr:last .index').text());
        index_no += Number(1);
    var append_min_val = Number($('.data_tr:last .range input[name="max-range[]"]').val());
        append_min_val += append_min_val == 0 ? 0 : Number(1);
    var appaned_data = '<tr class="data_tr"> <td class="index">'+index_no+'</td> <td style="width: 300px;" class="range"><div class="wid_45"><input type="number" class="form-control min-range" name="min-range[]" placeholder="Min Range" value="'+append_min_val+'" required></div><div class="wid_10"> To </div><div class="wid_45"><input type="number" class="form-control max-range" name="max-range[]"  placeholder="Max Range" required></div></td> <td><select class="form-control" name="calculation_type[]" required><option value="$">$</option><option value="%">%</option></select></td> <td><input type="number" class="form-control" name="fee_value[]" placeholder="Fee Value" required></td> </tr>';
    $(appaned_data).insertAfter('.change_manual_deposite_fee .data_tr:last');
});
$('.common_modal_show').click(function () {
    var action_mdl_class;
    var id = $(this).attr('tg_id');
    var action = $(this).attr('action');
    var action_url = $(this).attr('url');
    switch (action) {
        case 'commonshow': 
        // $('#modal-basic .modal-content').html(data.result.data_value);
        // $('#modal-basic .modal-content').find('#security_id').attr({form_tg_action:form_tg_action,dataid:id});
        // $('#modal-basic').modal('show');
        break;
        case 'show_ga_winner':
        if (id != undefined && action_url != '') {
            $.ajax({
                url : action_url,
                data: {id:id},
                type: 'POST',
                beforeSend: function() { admin_loader_show(); },
                success: function(data) {
                    data = $.parseJSON(data);
                    admin_loader_hide();
                    if (data) {
                        var append_data = $('.clone_data').clone().removeClass('clone_data').attr('id','DataTables_Table_0_wrapper');
                        append_data.find('.box-header').html('<h1>Giveaway Winner</h1>');
                        append_data.find('.msg').remove();
                        var new_html = '<table id="example2" class="table table-bordered table-striped"><thead><tr><th>Acc#</th><th>Name</th><th>Giveaway Tag</th></tr></thead><tbody>';
                        if (data.get_winners != null) {
                            // for (var i = data.get_winners.length - 1; i >= 0; i--) {
                            //     new_html += '<tr>';
                            //     new_html += '<td>'+data.get_winners[i].account_no+'</td>';
                            //     new_html += '<td>'+((data.get_winners[i].display_name != '') ? data.get_winners[i].display_name : data.get_winners[i].name)+'</td>';
                            //     new_html += '<td>test</td>';
                            //     new_html += '</tr>';
                            // }

                            new_html += '<tr>';
                            new_html += '<td>'+data.get_winners.account_no+'</td>';
                            new_html += '<td>'+((data.get_winners.display_name != null && data.get_winners.display_name != '') ? data.get_winners.display_name : data.get_winners.name)+'</td>';
                            new_html += '<td>test</td>';
                            new_html += '</tr>';
                        }
                        new_html += '</tbody></table>';
                        append_data.find('.box-body').html(new_html);
                        $('#modal-basic .modal-content').html(append_data);
                        $('#modal-basic').modal('show');
                        // $('#example2').datatable();
                    }
                }
            });
        }

        break;
        default:
        form_tg_action = action;
        break;
    }
});
$('.delete-data, .edit-data').click(function() {
    var action_url, action_mdl_class, winre_loc;
    var id = $(this).attr('id');
    var action = $(this).attr('action');
    action_url = base_url+'admin/My_Controller/security_pw_auth/';
    winre_loc = ($(this).attr('winre_loc') != undefined && $(this).attr('winre_loc') != '') ? $(this).attr('winre_loc') : '';
    switch (action) {
        case 'delete_manual_deposite_fee': 
        form_tg_action = 'change_manual_deposite_fee';
        break;
        case 'delete_subscription_plan': 
        form_tg_action = 'delete_subscription_plan';
        break;
        case 'unsubscribe_plan': 
        form_tg_action = 'unsubscribe_active_plan';
        break;
        case 'delete_game_category': 
        form_tg_action = 'delete_game_category';
        break;
        case 'delete_ads': 
        form_tg_action = 'delete_ads';
        break;
        case 'delete_product_ads': 
        form_tg_action = 'delete_product_ads';
        break;
        case 'delete_product_id': 
        form_tg_action = 'delete_product_id';
        break;
        case 'edit_tracking_data': 
        form_tg_action = 'edit_tracking_data';
        break;
        case 'edit_order_action_cmplt': 
        form_tg_action = 'edit_order_action_cmplt';
        break;
        case 'edit_order_action_cancel': 
        form_tg_action = 'edit_order_action_cancel';
        break;
        case 'edit_glbl_evefee':
        form_tg_action = 'edit_glbl_evefee';
        break;
        default:
        form_tg_action = action;
        break;
    }
    $.ajax({
        url : action_url,
        data: {id:id, winre_loc:winre_loc},
        type: 'POST',
        beforeSend: function() {
            admin_loader_show();
        },
        success: function(data) {
            data = $.parseJSON(data);
            if (data) {
                admin_loader_hide();
                $('#modal-basic .modal-content').html(data.result.data_value);
                $('#modal-basic .modal-content').find('#security_id').attr({form_tg_action:form_tg_action,dataid:id,winre_loc:winre_loc});
                $('#modal-basic').modal('show');
            }
        }
    });
});
$(document).on('submit','#security_id',function(e){
    var action_url, winre_loc, dataid, security_password;
    e.preventDefault();
    var data = $(this).serialize(), dataid = $(this).attr('dataid'), winre_loc = $(this).attr('winre_loc');
    security_password = $('#security_password').val();
    action_url = base_url+'admin/My_Controller/security_pw_auth/'+security_password+'/'+dataid;
    var form_tg_action = $(this).attr('form_tg_action');
    switch (form_tg_action) {
        case 'change_manual_deposite_fee': 
        winre_loc = base_url+"admin/points/deletepointmanualPercentage/"+dataid;
        break;
        case 'delete_subscription_plan': 
        winre_loc = base_url+"admin/subscription/delete_subscription_plan/"+dataid;
        break;
        case 'unsubscribe_active_plan': 
        winre_loc = base_url+"admin/trade/unsubscribe/"+dataid;
        break;
        case 'delete_game_category': 
        winre_loc = base_url+"admin/game/delete_gameCat/"+dataid;
        break;
        case 'delete_ads': 
        winre_loc = base_url+"admin/ads/delete_ads_data/"+dataid;
        break;
        case 'delete_product_ads': 
        winre_loc = base_url+"admin/ads/delete_ads_data/"+dataid;
        break;
        case 'delete_product_id': 
        winre_loc = base_url+"admin/products/delete?id="+dataid;
        break;
        case 'delete_live_lobby':
        winre_loc = base_url+"admin/live_lobby/delete_lobby_data/"+dataid;
        break;
        case 'edit_order_action_cmplt': 
        winre_loc = base_url+"admin/orders/savedata?id="+dataid+'&action=completed';
        break;
        case 'edit_order_action_cancel': 
        winre_loc = base_url+"admin/orders/savedata?id="+dataid+'&action=canceled';
        break;
        case 'reset_giveaway': 
        winre_loc = base_url+"admin/giveaway/reset_giveaway/"+dataid;
        break;
        case 'edit_tracking_data':
        var data_id = dataid.split('_');
        var edit_id = data_id[0];
        var sold_by = data_id[1];
        var grand_total = data_id[2];
        var ref_seller = data_id[3];
        var edit_modal = '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Traking Info</h4></div>';
        edit_modal +='<div class="modal-body"><div class="row"><form action="'+base_url+'admin/orders/edit_tracking_id" id="changebalance_id" name="changebalance_id" method="POST"><div class="box-body"><div class="form-group"><label for="name">Edit Traking ID</label><input type="hidden" id="editid" class="form-control" name="editid" value="'+edit_id+'" required=""><input type="hidden" id="item_sold_by" class="form-control" name="item_sold_by" value="'+sold_by+'" required=""><input type="hidden" id="grand_total" class="form-control" name="grand_total" value="'+grand_total+'" required=""><input type="hidden" id="ref_seller" class="form-control" name="ref_seller" value="'+ref_seller+'" required=""><input type="text" id="edit_traking_id" class="form-control" name="edit_traking_id" value="" required=""></div></div><div class="box-footer"><input class="btn btn-primary" type="submit" value="Submit"></div></form></div></div>';
        edit_modal +='<div class="modal-footer"><button type="button" class="btn btn-info" data-dismiss="modal">Close</button></div>';
        break;
        case 'edit_product_qty_action':
        var data_id = dataid.split('_');
        var product_id = data_id[0];
        var qty = data_id[1];
        var edit_modal = '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Edit Product Qty</h4></div>';
        edit_modal +='<div class="modal-body"><div class="row"><form action="'+base_url+'admin/products/updateqty/'+product_id+'" method="POST"><div class="box-body"><div class="form-group"><label for="name">Qty</label><input type="number" id="qty" class="form-control" name="qty" value="'+qty+'" required=""></div></div><div class="box-footer"><input class="btn btn-primary" type="submit" value="Submit"></div></form></div></div>';
        edit_modal +='<div class="modal-footer"><button type="button" class="btn btn-info" data-dismiss="modal">Close</button></div>';
        break;
        case 'edit_game_cat_order_data':
        var data_id = dataid.split('_'); var cat_id = data_id[0]; var dsply_rdr = data_id[1];
        var edit_modal = '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Game Category display order</h4></div>';
        edit_modal +='<div class="modal-body"><div class="row"><form action="'+base_url+'admin/game/cat_display_order/'+cat_id+'" id="changebalance_id" name="changebalance_id" method="POST"><div class="box-body"><div class="form-group"><label for="name">Change display order</label><input type="number" id="edit_display_order" class="form-control" name="edit_display_order" value="'+dsply_rdr+'" required=""></div></div><div class="box-footer"><input class="btn btn-primary" type="submit" value="Submit"></div></form></div></div>';
        edit_modal +='<div class="modal-footer"><button type="button" class="btn btn-info" data-dismiss="modal">Close</button></div>';
        break;
        case 'edit_membership_order_data':
        var data_id = dataid.split('_'); var mem_id = data_id[0]; var dsply_rdr = data_id[1];
        var edit_modal = '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Membership display order</h4></div>';
        edit_modal +='<div class="modal-body"><div class="row"><form action="'+base_url+'admin/membership/membership_display_order/'+mem_id+'" id="changebalance_id" name="changebalance_id" method="POST"><div class="box-body"><div class="form-group"><label for="name">Change display order</label><input type="number" id="edit_display_order" class="form-control" name="edit_display_order" value="'+dsply_rdr+'" required=""></div></div><div class="box-footer"><input class="btn btn-primary" type="submit" value="Submit"></div></form></div></div>';
        edit_modal +='<div class="modal-footer"><button type="button" class="btn btn-info" data-dismiss="modal">Close</button></div>';
        break;
        // case 'edit_player_membership_amount':
        // var mem_id = data_id;
        // var edit_modal = '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Membership display order</h4></div>';
        // edit_modal +='<div class="modal-body"><div class="row"><form action="'+base_url+'admin/membership/change_player_membership/'+mem_id+'" id="changebalance_id" name="changebalance_id" method="POST"><div class="box-body"><div class="form-group"><label for="name">Change display order</label><input type="number" id="edit_display_order" class="form-control" name="edit_display_order" value="'+dsply_rdr+'" required=""></div></div><div class="box-footer"><input class="btn btn-primary" type="submit" value="Submit"></div></form></div></div>';
        // edit_modal +='<div class="modal-footer"><button type="button" class="btn btn-info" data-dismiss="modal">Close</button></div>';
        // break;
        case 'event_key_order_change':
        var data_id = dataid.split('_'); var mem_id = data_id[0]; var dsply_rdr = data_id[1];
        var edit_modal = '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Membership display order</h4></div>';
        edit_modal +='<div class="modal-body"><div class="row"><form action="'+base_url+'admin/membership/membership_display_order/'+mem_id+'" id="changebalance_id" name="changebalance_id" method="POST"><div class="box-body"><div class="form-group"><label for="name">Change display order</label><input type="number" id="edit_display_order" class="form-control" name="edit_display_order" value="'+dsply_rdr+'" required=""></div></div><div class="box-footer"><input class="btn btn-primary" type="submit" value="Submit"></div></form></div></div>';
        edit_modal +='<div class="modal-footer"><button type="button" class="btn btn-info" data-dismiss="modal">Close</button></div>';
        break;
        case 'edit_glbl_gafee':
        var type = (dataid.indexOf('_') != -1) ? dataid.split('_')[0] : dataid;
        var fee = (dataid.indexOf('_') != -1) ? dataid.split('_')[1] : 0;
        var ga_fee_collector = $('input[name="ga_fee_collector"]').val();
        var edit_modal = '<div class="modal-header" style="border: none;"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h4 class="modal-title text-center" style="font-weight:bold;">Set Global Giveaway Fee</h4></div>';
        edit_modal += '<div class="modal-body nav-tabs-custom"><ul class="nav nav-tabs" role="tablist"><li class="active"><a role="tab" data-toggle="tab">Esports Fee</a></li></ul><div class="tab-content"><div class="row tab-pane active"><form action="'+base_url+'admin/giveaway/update_ga_fee" id="change_event_fee" name="change_event_fee" method="POST"><div class="box-body">';
        edit_modal += '<div class="form-group row"><label class="col-sm-2">Type </label><div class="col-sm-10"><select name="type" class="form-control"><option selected="" value="2">Percentage</option></select></div></div>';
        edit_modal += '<div class="form-group row"><label class="col-sm-2">Fee </label><div class="col-sm-10"><input required="" type="text" onkeypress="return (event.charCode >= 48 &amp;&amp; event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46" class="form-control event_fee" name="event_fee" value="'+parseFloat(fee)+'"></div></div>';
        edit_modal += '<div class="form-group row"><label class="col-sm-2">Fee Default Acc #</label><div class="col-sm-10"><input required="" type="number" onkeypress="return (event.charCode >= 48 &amp;&amp; event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46" class="form-control event_fee" name="default_fee_collect_account_no" value="'+ga_fee_collector+'"></div></div>';
        // edit_modal += '<div class="form-group row"><label class="col-sm-2"></label><div class="col-sm-10"><span style="color:red;"> [ This fee will be calculate on new events ] </span></div></div></div>';
        // edit_modal +='<div class="modal-footer"><button type="button" class="btn btn-info" data-dismiss="modal">Close</button></div>';
        edit_modal += '<div class="box-footer"><input class="btn btn-primary" type="submit" value="Submit"><button type="button" class="btn btn-info pull-right" data-dismiss="modal">Close</button></div></form></div></div></div>';
        break;
        case 'exp_plyr_history':
        winre_loc = (dataid != undefined && dataid != '') ? winre_loc+dataid : winre_loc;
        var edit_modal = '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Export Player History</h4></div>';
        edit_modal +='<div class="modal-body">';
        edit_modal +='<form action="'+winre_loc+'" name="exp_plyr_history_frm" method="POST"><div class="box-body"><div class="form-group row"><label class="col-sm-2 text-right">Min date </label><div class="col-sm-3"><input type="text" class="form-control custom_datetimepicker" dt_format="YYYY-MM-DD" name="min_date" placeholder="Min Date" required></div><label class="col-sm-2 text-right">Max date</label><div class="col-sm-3"><input type="text" class="form-control custom_datetimepicker" dt_format="YYYY-MM-DD" name="max_date" placeholder="Max Date" required></div><div class="col-sm-2"><button type="submit" value="submit" class="btn btn-primary">Submit</button></div></div></div></form><hr>';
        edit_modal +='<form action="'+winre_loc+'" name="exp_plyr_history_frm" method="POST"><div class="box-body"><div class="form-group row" style="margin-top: 15px;"><label class="col-sm-2 text-right">Year</label><div class="col-sm-3"><input type="text" class="form-control custom_datetimepicker" dt_format="YYYY" name="year" placeholder="Year"></div><label class="col-sm-2 text-right">Month</label><div class="col-sm-3"><input type="text" class="form-control custom_datetimepicker" dt_format="MM" name="month" placeholder="Month"></div><div class="col-sm-2"><button type="submit" value="submit" class="btn btn-primary">Submit</button></div></div></div></form> <hr>';
        edit_modal +='<form action="'+winre_loc+'" name="exp_plyr_history_frm" method="POST"><div class="box-body"><div class="form-group row" style="margin-top: 15px;"><div class="col-sm-12 text-center"><input type="hidden" name="alldata" value="yes"><button type="submit" value="submit" class="btn btn-primary">All History</button></div></div></div></form> <hr style="margin: 0;">';
        edit_modal +='</div>';
        edit_modal +='<div class="modal-footer"><button type="button" class="btn btn-info" data-dismiss="modal">Close</button></div>';
        break;
        break;
        default:
        break;
    }
    if (data) {
        $.ajax({
            url: action_url,
            data: data,
            type: 'POST',
            beforeSend: function() {
                admin_loader_show();
            },
            success: function(data) {
                data = $.parseJSON(data);
                admin_loader_hide();
                if (data.result.data_id != undefined && data.result.data_id !=''){
                    if (form_tg_action == 'edit_glbl_evefee') {
                        changeEventFee();
                        return false;
                    }
                	if (form_tg_action == 'exp_plyr_history' || form_tg_action == 'edit_tracking_data' || form_tg_action == 'edit_product_qty_action' || form_tg_action == 'edit_game_cat_order_data' || form_tg_action == 'edit_membership_order_data' || form_tg_action == 'edit_glbl_gafee') {
                		$('#modal-basic .modal-content').html(edit_modal);
                		$('#modal-basic').modal('show');
                    } else {
                        window.location.href = winre_loc;
                    }
                } else {
                    $('#security_password').val('');
                    alert('Password Error');
                    return false;
                }
                common_show();       
            }
        });
    }
});
function common_show() {
    if ($('.custom_datetimepicker').length) {
        $('.custom_datetimepicker').each(function(i) {
            if ($(this).attr('dt_format')) {
                var format = $(this).attr('dt_format');
                $(this).datetimepicker({ format: format }).keydown(false);
            } else {
                $(this).datetimepicker({ format: 'YYYY-MM-DD HH:mm:ss'}).keydown(false);
            }
        });
    }
}

$('.admin_game_edit_form .choose_cate_id input[type="checkbox"]').click(function(){
    if (!this.checked) {
        var category_id = $(this).val();
        var game_id = $('.admin_game_edit_form input[name="id"]').val();
        var old_game_name = $('.admin_game_edit_form input[name="old_name"]').val();
        var postData = {
            'category_id':category_id,
            'game_id':game_id,
            'old_game_name':old_game_name
        }
        $.ajax({
            url: base_url + "admin/game/check_game_category/"+category_id+'/'+game_id,
            type: "POST",
            data:postData,
            beforeSend: function(){
                $('.game_category_loader.loader_'+category_id).html('<i class="fa fa-circle-o-notch fa-spin"></i>');
            },
            success: function(res) {
                var res = $.parseJSON(res);
                if (res.result.length > 0) {
                    $('.game_is_live_error_'+category_id).html("Players are using this game live now, you can't change this category.");
                    $('.admin_game_edit_form .choose_cate_id input[value="'+category_id+'"]').prop('checked', true);
                }
                $('.game_category_loader.loader_'+category_id).html('');
            },

        });
    }
});
$('form.add_ads input[name="select_type"]').change(function () {
    var showClass, hideClass;
    var type = $(this).val();
    showClass = type;
    hideClass = 'commercial_ads';
    (type == 'commercial_ads') ? hideClass = 'static_dis_ads' : '';
    hideshowmg(showClass,hideClass);
    $('.ads_master .hide input, .ads_master .hide textarea, .ads_master .hide select').removeAttr('required');
    $('.ads_master .show input, .ads_master .show textarea, .ads_master .show select').not('.ads_master .show .hide input, .ads_master .show .hide textarea, .ads_master .hide .show input, .ads_master .hide .show textarea, .ads_master .show input.novalidate').attr('required','');
});
$('form.add_ads select[name="select_vids_type"]').change(function () {
    var showClass, hideClass;
    var type = $(this).val();
    showClass = type;
    (type == 'custom_vids') ? hideClass = 'youtube_link' : (type == 'youtube_link') ? hideClass = 'custom_vids' : hideClass = '.custom_vids,.youtube_link';
    hideshowmg(showClass,hideClass);
    $('.ads_master .show .hide input, .ads_master .show .hide textarea').removeAttr('required');
    $('.ads_master .show .show input, .ads_master .show .show textarea').not('.ads_master .show .show input.novalidate').attr('required','');
});
function hideshowmg(showClass,hideClass) {
    if (showClass.length && hideClass.length) {
        $('.'+showClass).removeClass('hide').addClass('show');
        $('.'+hideClass).removeClass('show').addClass('hide');
    } else {
        $(hideClass).removeClass('show').addClass('hide');
    }
}

function preview_image(id,type,upload_div) 
{
    var total_file=document.getElementById(id).files.length;
    
    for(var i=0;i<total_file;i++)
    {
        var file_data = event.target.files[i];
        var form_data = new FormData();
        
        form_data.append('file', file_data);
        form_data.append('type', type);
        form_data.append('attr', id);
        $.ajax({
            url: base_url + "admin/products/upload_file",
            dataType: 'json', 
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (response) {               
                $('#'+upload_div).append("<div style='margin:10px;display:inline-block' id='"+response.id+"'><input style='vertical-align:top;margin-right:10px;' type='hidden' name='is_defult' class='is_defult'><span ><img  style='border-radius: 12px;' height='210' width='210' src='"+response.name+"'><span style='cursor:pointer;' class='remove_img'  onclick='remove_image("+response.id+","+type+","+response.id+");'><i class='fa fa-trash fa-fw'></i>Remove Image</span></span></div>");
            },
            error: function (response) {
                $('#msg').html(response); // display error response from the server
            }
        });
    }
  }
   
  function remove_image(id,type,img_name)
  {
    var add_type = $('#addproduct').val();
    // if(add_type == 1)
    // {
        $.ajax({
        url: base_url + "admin/products/delete_file?id="+img_name+"&type="+type,
        type: 'get',
        success: function (response) { 
            $('#'+id).remove();
            // location.reload();
        },
        
    });    
    // if(type == 1)
    // {
        
       
    //         var str = $('#old_attr_image').val();
    //         str = str.replace(img_name, '');
    //         $('#old_attr_image').val(str);
             
    // } else {
    //     var add_type = $('#addproduct').val();
    //     if(add_type == 1)
    //     {
    //         var new_str =$('#final_img').val();
    //         new_str = new_str.replace(img_name+',', '');            
    //         $('#final_img').val(new_str);
    //     } else {
    //         var str = $('#old_gallery_image').val();
    //         str = str.replace(img_name, '');
    //         $('#old_gallery_image').val(str);
    //     }
        
    // }
    
   
  }

$(function () {
    $(".chk-btn").click(function () {
        var id = $(this).attr('id'), value = $(this).attr('value');
        if ($(this).attr('image_upload') == 'true') {
            if ($(this).is(":checked")) {
                var appen_data = '<div class="form-group col-md-4" id="remove'+id+'"><label>'+value+'</label>';
                if ($(this).closest('form').attr('formaction') == 'update')
                    appen_data += '<input type="file" class="form-control" name="attr_image['+id+'][]" id="img_'+id+'" onchange="preview_image1(`img_'+id+'`,1,`image_'+value+'`);" multiple></input>';
                else 
                    appen_data += '<input type="file" class="form-control" name="attr_image['+id+'][]" id="img_'+id+'" onchange="preview_image(`img_'+id+'`,1,`image_'+value+'`);" multiple></input>';
                appen_data += '<div id="image_'+value+'"></div>';
                $('#dynamic_attr').append(appen_data);
            } else {
                $('#remove'+id).remove();
            }
        }
    });
});

//check for youtube link validation
function validateYouTubeUrl() {    
    var url = $('#youTubeUrl').val();
    if (url != undefined || url != '') {        
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
        var match = url.match(regExp);
        if (match && match[2].length == 11) {
            // Do anything for being valid
            // if need to change the url to embed url then use below line            
            $('#videoObject').attr('src', 'https://www.youtube.com/embed/' + match[2] + '?autoplay=1&enablejsapi=1');
        } else {
            alert('not valid');
            // Do anything for not being valid
        }
    }
}

$('.ads_master select[name="select_cat_id"], .ads_master select[name="select_sub_cat_id"]').change(function () {
    var category = $(this).val();
    var category_type = $(this).attr('category_type');
    var postData = { 'category' : category, 'category_type':category_type }
    $.ajax({
        url: base_url + "admin/products/search_category",
        type: "POST",
        data: postData,
        success: function (data) { 
            var data = $.parseJSON(data);
            if (data.suboption != undefined && data.suboption !='') {
                $('select[name="select_sub_cat_id"]').val('').trigger('change').html(data.suboption);
                $('select#select-prdct_name').html('').trigger('change');
            }
            if (data.productname_list != undefined && data.productname_list !='') {
                $('select#select-prdct_name').html('').trigger('change').html(data.productname_list);
            }
        }
    });
});
if ($('#customize_tbl').length > 0) {
    var table = $('#customize_tbl').DataTable();
    $('#search-acc').on('keypress keyup', function() {
        table.column($('#customize_tbl').attr('account_search_clm')).search(this.value).draw();
    });
}