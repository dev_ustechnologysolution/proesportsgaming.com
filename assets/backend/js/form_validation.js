function validateDate() 
{ 
    var expiry_date=$('#expiry_date_time').val();
    var date_regex = /^\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}$/ ;
    return date_regex.test(expiry_date);
}