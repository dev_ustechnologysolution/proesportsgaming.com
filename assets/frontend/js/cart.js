$(function (){
	'use strict';
	// product image carousel and zoom
	($('.product_main_img.zoom').length) ? $('.product_main_img.zoom').zoom() : '';
	$('.select_prdct_qnt .sub, .select_prdct_qnt .add').click(function () {
		var old_prdct_qty = $(this).siblings('.prdct_qntity').val();
		var extra_amount = $(this).siblings('.prdct_qntity').attr('extra_amount');
		if (this.className == 'add cmn_btn') {
			$(this).prev().val(+$(this).prev().val() + (($(this).prev().attr('max') == $(this).prev().val()) ? 0: 1));
		} else if (this.className == 'sub cmn_btn') {
			($(this).next().val() > 1) ? $(this).next().val(+$(this).next().val() - 1) : '';
		}
		var prdct_qty = $(this).siblings('.prdct_qntity').val();
		var cart_item_id = $(this).siblings('.cart_item_id').val();
		var product_id = $(this).siblings('.product_id').val();
		if (product_id != undefined && product_id != '' && cart_item_id != undefined && cart_item_id !='') {
			var postData = {
				'extra_amount' : extra_amount,
				'prdct_qty' : prdct_qty,
				'cart_item_id' : cart_item_id,
				'product_id':product_id,
			}
			$.ajax({
				type: "POST",
				url: base_url + "cart/cart_items_update",
				data: postData,
				beforeSend: function(){
					loader_show();
				},
				success: function (data) { 
					var data = $.parseJSON(data);
					$('.item_exceed_err').html('');
					loader_hide();
					if (data.err != undefined && data.err !='') {
						$('.item_'+data.item.cart_item_id+' .prdct_qntity').val(old_prdct_qty);
						$('.item_'+data.item.cart_item_id+' .item_exceed_err').html(data.err);
						return false;
					} else {
						$('.item_'+data.item.cart_item_id+' .prdcts_total_price').html(data.prdcts_total_price);
						var subtotal = 0;
						var shipping_cal_type = $('.shipping_cal_type').html();
						var shipping_val = $('.shipping_val').html();
						// extra_amount
						$('.prdcts_total_price').each(function() { subtotal += parseFloat($(this).html()); });
						var shipping_charge = (shipping_cal_type == 1) ? shipping_val : subtotal*shipping_val/100;
						$('.checkout_calc_data .subtotal').html(parseFloat(subtotal).toFixed(2));
						$('.checkout_calc_data .shipping_charge').html(parseFloat(shipping_charge).toFixed(2));
						// var grand_total = parseFloat(shipping_charge);
						// var grand_total += parseFloat(subtotal);
						$('.grand_total').html(parseFloat(subtotal).toFixed(2));
					}
				}
			});
		}
	});
	var sel_variation = [];
	var extra_amount = [];
	$('.variation_ul .select_var select, .variation_ul .select_var input').change(function (e){
		// if ($(this).prop("tagName") == 'SELECT') {
		// 	var sel_attribute_lable = $(this).children('option:selected').attr('attribute_lable');
		// 	var sel_option_lable = $(this).children('option:selected').attr('option_lable');
		// 	var attribute_id = $(this).children('option:selected').attr('attribute_id');
		// 	var option_id = $(this).children('option:selected').attr('option_id');
		// } else {
		// 	var sel_attribute_lable = $(this).attr('attribute_lable');
		// 	var sel_option_lable = $(this).attr('option_lable');
		// 	var attribute_id = $(this).attr('attribute_id');
		// 	var option_id = $(this).attr('option_id');
		// 	$(this).closest('.form-group').siblings().find('input[type="checkbox"]').prop('checked',false);
		// 	$('input.extra_amount').val('atr_'+attribute_id+'&opt_'+option_id+'&variation_amount_'+$(this).attr('extra_price'));
		// }

		if ($(this).prop("tagName") == 'SELECT') {
			var sel_attribute_lable_title = $(this).children('option:selected').attr('attribute_lable');
			var sel_option_lable = $(this).children('option:selected').attr('option_lable');
			var attribute_id = $(this).children('option:selected').attr('attribute_id');
			var sel_attribute_lable = attribute_id;
			var option_id = $(this).children('option:selected').attr('option_id');
			// var ext_amt = 'atr_'+attribute_id+'&opt_'+option_id+'&variation_amount_'+$(this).children('option:selected').attr('extra_price');
			var ext_amt = $(this).children('option:selected').attr('extra_price');
		} else {
			var sel_attribute_lable_title = $(this).attr('attribute_lable');
			var sel_option_lable = $(this).attr('option_lable');
			var attribute_id = $(this).attr('attribute_id');
			var sel_attribute_lable = attribute_id;
			var option_id = $(this).attr('option_id');
			$(this).closest('.form-group').siblings().find('input[type="checkbox"]').prop('checked',false);
			// var ext_amt = 'atr_'+attribute_id+'&opt_'+option_id+'&variation_amount_'+$(this).attr('extra_price');
			var ext_amt = $(this).attr('extra_price');
		}
		// alert(sel_attribute_lable);
		$('.selected_var .'+sel_attribute_lable).remove();
		// $('.selected_var .'+attribute_id).remove();
		// var app_append = '<input type="hidden" name="sel_variation[]" class="'+sel_attribute_lable+'" value="'+'atrlbl_'+sel_attribute_lable_title+'&optlbl_'+sel_option_lable+'">';
		// app_append += '<input type="hidden" name="sel_variation_ids[]" class="'+sel_attribute_lable+'" value="'+'atr_'+attribute_id+'&opt_'+option_id+'">';
		// app_append += '<input type="hidden" name="extra_amount[]" class="'+sel_attribute_lable+'" value="'+ext_amt+'">';
		
		var app_append = '<input type="hidden" name="sel_variation['+sel_attribute_lable+'][]" class="select_variation '+sel_attribute_lable+'" value="'+option_id+'">';
		// app_append += '<input type="hidden" name="sel_variation['+sel_attribute_lable_title+']['+sel_option_lable+'][extra_amount]" class="select_variation '+sel_attribute_lable+'" value="'+ext_amt+'">';
		// app_append += '<input type="hidden" name="sel_variation['+sel_attribute_lable+'][]" class="select_variation '+sel_attribute_lable+'" value="'+attribute_id+'">';

		// app_append += '<input type="hidden" name="sel_variation_ids['+sel_attribute_lable_title+'][]" class="'+sel_attribute_lable+'" value="'+option_id+'">';
		app_append += '<input type="hidden" name="extra_amount['+sel_attribute_lable+']['+option_id+']" class="select_variation '+sel_attribute_lable+'" value="'+ext_amt+'">';
	
		$('.selected_var').append(app_append);

		var product_id = $("input[name='product_id']").val();
		sel_variation[sel_attribute_lable] = option_id;
		if (sel_variation != undefined && sel_variation != '') {
			var postData = {
				'sel_variation' : sel_variation,
				'product_id' : product_id
			}
			$.ajax({
				type: "POST",
			    url: base_url + "store/get_img_selvar",
			    data: postData,
			    beforeSend: function(){
			    	loader_show();
			    },
			    success: function (data) { 
			      loader_hide();
			      var data = $.parseJSON(data);
			      if (data.img != undefined && data.img != '' && data.selected_img != undefined && data.selected_img !='') {
			      	$('.product_single_page .img_arr .prdct_img_ul').html(data.img);
			      	$('.product_single_page .product_main_img').attr('href',data.zoom_img).children('img').attr('src',data.zoom_img);
			      	$('input[name="product_image"]').val(data.selected_img);
			      	if (data.show_arrows.length) {
			      		if (data.show_arrows == 'yes') {
			      			$('.body-middle .product_single_page .img_arr .cr_arrw').removeClass('hide').addClass('show');
				      		$('.product_single_page .img_arr .prdct_img_ul').css('overflow-y','scroll');
			      		} else {
			      			$('.body-middle .product_single_page .img_arr .cr_arrw').removeClass('show').addClass('hide');
				      		$('.product_single_page .img_arr .prdct_img_ul').css('overflow-y','unset');
			      		}
			      	}
			      	$(this).next('.loader').html('');
			      	$('.show-small-img').click(function () { show_small_img_slide(this); });
			      	$('#next-img').click(function () { next_img_slide(this); });
			      	$('#prev-img').click(function () { prev_img_slide(this); });
			      }
			    }
			});
		}
	});
	function load_more_data(){
		$('.product_list li.load_more_div .load_more_btn, #admin_gear_btn, #player_gear_btn, #search_gear_btn').on('click',function(){
			var arr = {
				'this' : $(this)
			};
			product_listing_filter(arr);
		});
		$('#select_num_prdct').on('change',function() {
			if ($(this).find('option:selected').val() != undefined && $(this).find('option:selected').val() != '') {
				var arr = {
					'this' : this,
					'num_display_prdct' : $(this).find('option:selected').val()
				};
				product_listing_filter(arr);
			}
		})
	}
	function product_listing_filter(arr) {
		var store_id = $('#store_user_id').val();
		var this_id = (arr.this != undefined && arr.this.length && arr.this != '') ? $(arr.this)[0].id : '';
		var allcount = Number($('#all').val());
		var last_product = Number($('#last_product').val());
		var totalcount = Number($('#totalcount').val());
		var item_per_page = $('#item_per_page').val();
		var itemperdiv = 5;
		var main_category, main_sub_cat, product_srch_term;
		main_category = $('.store_page_filter #main_category').val();
		main_sub_cat = $('.store_page_filter #main_sub_cat').val();
		product_srch_term = $('.store_page_filter #product_srch-term').val();
		
		var postData = {
			'main_category' : main_category,
			'main_sub_cat' : main_sub_cat,
			'product_srch_term' : product_srch_term,
			'totalcount' : totalcount,
			'allcount' : allcount,
			'store_id' : store_id,
			'btn_id' : this_id,
		}
		if (arr.num_display_prdct != undefined && arr.num_display_prdct != '') {
			postData.num_display_prdct = arr.num_display_prdct;
		}
		$.ajax({
		    type: "POST",
		    url: base_url + "Store/search_product",
		    data: postData,
		    beforeSend:function(){
		      $(".load_more_btn").html('<i class="fa fa-spinner fa-spin" style="font-size: 25px;font-weight: bold;"></i>');
		    },
		    success: function (data) { 
		      var data = $.parseJSON(data);
		      var ads = data.ads;
		      var last_id = 0;
		      var products = data.result;
		      var get_last_id = data.get_last_id;
		      var last_order = 0
		      // var page_data = '';
		      if(products.length > 0){
		      	var j = 0;
		      	var last_product = $('.product_listing_first_div ul.product_list li.product_itm').last();
		      	if(this_id != ''){
		      		$('.product_listing_first_div ul.product_list').html('');
		      		last_product = '';
		      	}
		      	$.each(products, function( i, val ) {
		      		last_order = val.product_order;
			      	var page_data = $('.temp_append li.app_pro').clone();
					if(i %  5 == 0){
			      		var get_clone = $('.temp_append li.ads').clone();
						// var get_clone = $('li.ads').eq(0).clone();
						if(ads.length != 0){
							get_clone.find('.ads_container_desciption_div').attr('href', ads[j].sponsered_by_logo_link);
							get_clone.find('.ads_img_div img').attr('src', base_url+'upload/ads_master_upload/'+ads[j].upload_sponsered_by_logo);
							get_clone.find('.ads_img_div img').attr('alt', ads[j].upload_sponsered_by_logo);
							get_clone.find('.ads_desciption').html(ads[j].sponsered_by_logo_description	);
						}
						// page_data += get_clone;
						// $('.product_listing_first_div ul.product_list ').append(get_clone);
						if($('.product_listing_first_div ul.product_list').html() == ''){
							last_product = '';
						}else{
							$(get_clone).insertAfter(last_product);
							last_product = $('.product_listing_first_div ul.product_list li.ads').last();
						}
						if(ads.length > (j+1)){
							j++;
						}
					}
					// var page_data = $('li.product_itm').eq(0).clone();
					page_data.removeClass().addClass('product_itm product_'+val.id);
					page_data.attr('order_id',val.product_order);
					page_data.find('.product_img_dv img').attr('src', base_url+'upload/products/'+val.image);
					page_data.find('.product_img_dv img').attr('alt', val.image);
					page_data.find('.product_title_dv .title').html(val.name);
					page_data.find('.view_product').attr('href', base_url+'Store/view_product/'+val.id);
					if ($('.product_listing_first_div ul.product_list li.product_itm').length == 0){
						$('.product_listing_first_div ul.product_list').html(page_data);
					} else {
						// $('.product_listing_first_div ul.product_list').append(page_data);
						$(page_data).insertAfter(last_product);
					}
					last_product = $('.product_listing_first_div ul.product_list li.product_itm').last();
					// $('.product_listing_first_div ul.product_list').append(page_data);
					last_id = val.id;
				});
		      }else{
		      	$('.product_listing_first_div ul.product_list').html('<h5><div class="row bidhottest_err prdctempty"><div class="col-sm-12" style="color: #FF5000;">Products not available</div></div></h5>');
		      }
		      if(this_id != '' && products.length >= item_per_page){
		      	$('<li class="load_more_div row text-center"><a class="load_more_btn"> Load More</a><input type="hidden" id="totalcount" value="'+get_last_id.count+'"><input type="hidden" id="all" value="'+last_order+'"><input type="hidden" id="last_product" value="'+last_id+'"></li>').insertAfter(last_product);
		      	load_more_data();
		      }else{
		      	$('.product_listing_first_div ul.product_list li.load_more_div #all').val($('.product_listing_first_div ul.product_list li.product_itm').length);
		      $('.product_listing_first_div ul.product_list li.load_more_div #last_product').val(last_id);
		      var total_prdct = $('.product_listing_first_div ul .product_itm').length;
		      (total_prdct != totalcount) ? $(".load_more_btn").html('Load More') : $(".load_more_btn").remove();
		      }
					// $('.product_listing_first_div ul.product_list').append(get_clone);
		      // $(page_data).insertAfter('.product_listing_first_div ul.product_list li.product_'+last_product);
		      
		    }
		});
	}
	load_more_data();
	$('.store_page_filter select#main_category[name="main_category"], .add_selling_product form select[name="category_id"]').change(function () {
	  var main_category = $(this).val();
	  var postData = {
	    'main_category' : main_category
	  }	
	  $.ajax({
	    type: "POST",
	    url: base_url + "store/search_category",
	    data: postData,
	    success: function (data) { 
	      var data = $.parseJSON(data);
	      if (data.suboption.length) {
	        $('select#main_sub_cat[name="main_sub_cat"], .add_selling_product form select[name="subcategory_id"]').html(data.suboption);
	      }
	    }
	  });
	});

	$('.checkout_page_div .panel-default .gustfrmdiv input[name="check_as_guest"]').click(function(){ ($(this).is(":checked")) ? $('.checkout_page_div .frm_lgn_chckt input').removeAttr('required') : $('.checkout_page_div .frm_lgn_chckt input').attr('required',''); });
	$('.checkout_page_div .frm_billing_chckt .ship_to_diffadd input[name="ship_to_diffadd_check"]').click(function(){ ($(this).is(":checked")) ? $('.shipping_hidden').show().attr('required','') : $('.shipping_hidden').hide().removeAttr('required'); });
	$('form#frmPaypal .frm_pypl').click(function () { ($(this).is(":checked")) ? $('form#frmPaypal .frm_wllt').removeAttr('required') : $('form#frmPaypal .frm_wllt').attr('required',''); });

	// Product Image Gallery Crousel customize js
	var scroll_top = '';
	// var smallimg_height = ($('.prdct_img_ul .prdctimg').length) ? $('.prdct_img_ul .prdctimg.active').children('.show-small-img').height() :'';
	var smallimg_height = 80;
	$('.show-small-img').click(function () { show_small_img_slide(this); })
	$('#next-img').click(function () { next_img_slide(this); });
	$('#prev-img').click(function () { prev_img_slide(this); });

	function show_small_img_slide(this_cont) {
		$('#show-img, #big-img, .product_main_img.zoom img[role="presentation"]').attr('src', $(this_cont).attr('src'));
		$('.product_main_img.zoom').attr('href', $(this_cont).attr('src'));
		$(this_cont).attr('alt', 'now').parent('.prdctimg').addClass('active').siblings().removeClass('active').find('.show-small-img').removeAttr('alt');
		if ($('#small-img-roll .prdctimg').length > 4) {
			scroll_top = ($(".show-small-img[alt='now']").parent('.prdctimg').index() >= 2 && $(".show-small-img[alt='now']").parent('.prdctimg').index() < $('#small-img-roll .prdctimg').length - 1) ? ($(".show-small-img[alt='now']").parent('.prdctimg').index() - 1) * smallimg_height : (($(".show-small-img[alt='now']").parent('.prdctimg').index() == $('#small-img-roll .prdctimg').length - 1) ? ($('#small-img-roll .prdctimg').length - 4) * smallimg_height : 0);
		    $('#small-img-roll').animate({scrollTop:scroll_top},'fast');
		}
		$('.add-to-cart_form input[name="product_image"]').val($(this_cont).parent('.prdctimg').attr('data-tg_img'));
	}
	function next_img_slide(this_cont){
		$('#show-img, #big-img, .product_main_img.zoom img[role="presentation"]').attr('src', $(".show-small-img[alt='now']").parent('.prdctimg').next().find('.show-small-img').attr('src'));
		// data-tg_img
		$('.product_main_img.zoom').attr('href', $(this_cont).attr('src'));
		$(".show-small-img[alt='now']").parent('.prdctimg').next().addClass('active').siblings().removeClass('active');
		$('#small-img-roll .prdctimg .show-small-img').removeAttr('alt');
		$('#small-img-roll .prdctimg.active ').children('.show-small-img').attr('alt', 'now');
		if ($('#small-img-roll .prdctimg').length > 4) {
	    scroll_top = ($(".show-small-img[alt='now']").parent('.prdctimg').index() >= 4 && $(".show-small-img[alt='now']").parent('.prdctimg').index() < $('#small-img-roll .prdctimg').length - 1) ? ($(".show-small-img[alt='now']").parent('.prdctimg').index() - 1) * smallimg_height : (($(".show-small-img[alt='now']").parent('.prdctimg').index() == $('#small-img-roll .prdctimg').length - 1) ? ($('#small-img-roll .prdctimg').length - 2) * smallimg_height : 0);
	    $('#small-img-roll').animate({scrollTop:scroll_top},'fast');
  	}
  	$('.add-to-cart_form input[name="product_image"]').val($(this_cont).parent('.prdctimg').attr('data-tg_img'));
	}
	function prev_img_slide(this_cont) {
		$('#show-img, #big-img, .product_main_img.zoom img[role="presentation"]').attr('src', $(".show-small-img[alt='now']").parent('.prdctimg').prev().find('.show-small-img').attr('src'));
		$('.product_main_img.zoom').attr('href', $(this_cont).attr('src'));
		$(".show-small-img[alt='now']").parent('.prdctimg').prev().addClass('active').siblings().removeClass('active');
		$('#small-img-roll .prdctimg .show-small-img').removeAttr('alt');
		$('#small-img-roll .prdctimg.active ').children('.show-small-img').attr('alt', 'now');
		if ($('#small-img-roll .prdctimg').length > 4) {
	    scroll_top = ($(".show-small-img[alt='now']").parent('.prdctimg').index() >= 1   && $(".show-small-img[alt='now']").parent('.prdctimg').index() < $('#small-img-roll .prdctimg').length - 1) ? ($(".show-small-img[alt='now']").parent('.prdctimg').index() - 0) * smallimg_height : (($(".show-small-img[alt='now']").parent('.prdctimg').index() == $('#small-img-roll .prdctimg').length - 1)? ($('#small-img-roll .prdctimg').length - 2) * smallimg_height : 0);
	    $('#small-img-roll').animate({scrollTop:scroll_top},'fast');
  	}
  	$('.add-to-cart_form input[name="product_image"]').val($(this_cont).parent('.prdctimg').attr('data-tg_img'));
	}

	$('.edit_tracking_modal').click(function(){
		var order_data = $(this).attr('data-order_data');
		var order_data = order_data.split('_');
        var edit_id = order_data[0];
        var sold_by = order_data[1];
        var grand_total = order_data[2];
        var ref_seller = order_data[3];        
		var modaltitle = 'Edit Tracking ID';
		var add_class_in_modal = 'edit_tracking_detail myprofile-edit';
		var senddata = '<div class="col-sm-12 text-left"></div><div class="col-sm-12"><form action="'+base_url+'order/edit_tracking_id" id="edit_tracking_id_form" method="POST"><input type="text" class="form-control" name="edit_traking_id" id="edit_traking_id" placeholder="Enter Tracking ID" required><input type="hidden" id="editid" class="form-control" name="editid" value="'+edit_id+'" required=""><input type="hidden" id="item_sold_by" class="form-control" name="item_sold_by" value="'+sold_by+'" required=""><input type="hidden" id="grand_total" class="form-control" name="grand_total" value="'+grand_total+'" required=""><input type="hidden" id="ref_seller" class="form-control" name="ref_seller" value="'+ref_seller+'" required=""></form></div><div class="col-sm-12"><h3><a href="javascript:void(0);" id = "edit_tracking_id_submit" class="button no">Yes</a><a class="button no" id = "rmv_all_cancle">Cancel</a></h3></div>';
		var data = {
			'add_class_in_modal':add_class_in_modal,
			'modal_title': modaltitle,
			'senddata':senddata,
			'tag':'edit_tracking'
		};
		commonshow(data);
	});

	$('.view_order').click(function () {
		var id = $(this).attr('id');
		var order_status = $(this).attr('order_status');
		var user_id = $(this).attr('data-user_id');
		if(user_id == 'undefined' || user_id == '' || user_id == null){
			user_id = "";
		}
		if (id.length && order_status.length) {
			var postdata = { 'id':id, 'order_status':order_status, 'user_id':user_id };
			$.ajax({
			    url : base_url +'order/order_items',
			    data: postdata,
			    type: 'POST',
			    beforeSend: function(){
			    	$(this).html('<i class="fa fa-circle-o-notch fa-spin"></i>');
			    },
			    success: function(res) {
			    	var res = $.parseJSON(res);
			    	var modaltitle = 'Order Details';
						var add_class_in_modal = 'order_detail myprofile-edit';
						var senddata = res.order_item;
						var data = {
							'add_class_in_modal':add_class_in_modal,
							'modal_title': modaltitle,
							'senddata':senddata,
							'tag':'view_order_item'
						};
						commonshow(data);
			    	$(this).html('<i class="fa fa-eye"></i>');
			    	return false;
			    }
			}); 
		}
	});
	$('.cancel_order').click(function () {
		var order_id = $(this).attr('data-order_id');
		var payment_method = $(this).attr('payment_method');
		if (order_id.length) {
			var senddata = '<div class="col-sm-12"><h4 style="color: #ff5000;">';
			senddata += (payment_method == 'Paypal' || payment_method == 'Wallet and Paypal') ? 'Paypal amount will be credited in seven business days.!' : '';
			senddata += '</h4></div><div class="col-sm-12"><h3><a class="button yes" id="cancel_ordr" order_id="'+order_id+'">OK</a><a class="button no">Cancel</a></h3></div>';
			var modaltitle = 'Are you sure to cancel this order ?';
			var add_class_in_modal_data = 'cancel_order_modal';
			var data = {
				'add_class_in_modal_data':add_class_in_modal_data,
				'modal_title': modaltitle,
				'senddata':senddata,
				'tag':'cancel_order'
			}
			commonshow(data);
		}
	});

	$('.trknginfo_clk').click(function () {
		var tracking_id = $(this).attr('attr_tag');
		if (tracking_id.length) {
			var senddata = '<div class="col-sm-12"><h2 class="text-left">';
			senddata += tracking_id;
			senddata += '</h2></div><div class="col-sm-12"><h3><a class="button no">OK</a><a class="button no">Cancel</a></h3></div>';
			var modaltitle = 'Tracking ID';
			var add_class_in_modal_data = 'trknginfo_order_modal';
			var data = {
				'add_class_in_modal_data':add_class_in_modal_data,
				'modal_title': modaltitle,
				'senddata':senddata,
				'tag':'cmninfo_shw'
			}
			commonshow(data);
		}
	});
	
	
});