(function ($) {
	'use strict';
	$.fn.countdown = function (options) {
		return $.fn.countdown.begin(this, $.extend({
			year: 2016, // YYYY Format
			month: 1, // 1-12
			day: 1, // 1-31
			hour: 0, // 24 hour format 0-23
			minute: 0, // 0-59
			second: 0, // 0-59
			timezone: -6, // http://en.wikipedia.org/wiki/List_of_tz_database_time_zones
			labels: true, // If false days hours seconds and monutes labels will not be created
			new_setting_id : options.id,
			start_time : options.creator_time,
			finish_action: options.finish_action,
			onFinish: function () {
				if (options.finish_action != undefined && options.finish_action != '') {
					if (options.finish_action == 'ga_countdown_finish') {
						$('.countdown_with_day#'+options.id).remove();
						$('.start_button').show();
					} else if (options.finish_action == 'ga_registration_close_countdown_finish') {
						$('.countdown_with_day#'+options.id+', .getanother_ticket_div, .join_giveaway_btn, .giveaway_registration_close_countdown').remove();
					}
				}
			}  // Executes client side when timer is zero
		}, options));
	};

	$.fn.countdown.begin = function (parent, settings) {
		
		// Define Variables
		var timespan, start, end;

		// Define Target Date/time
		end = new Date(settings.year, settings.month - 1, settings.day, settings.hour, settings.minute, settings.second);

		// Converts Local Timezone to Target Timezone
		start = new Date(settings.start_time);
		// start = $.fn.countdown.convertTimezone(settings.timezone);
		// Defines countdown data
		timespan = $.fn.countdown.getTimeRemaining(start, end, settings);

		// Check if the script has run before
		if (!settings.init) {
			settings.init = true;
			parent.find('.livetimer').html('');
			// Create elements
			// Define variables being used
			var container, wrapper, time, label;
			// Create elemnt container
			container = '<div class="clearfix tim_elediv animated rotateIn" id="days"><div class="wrapper"><span class="time">'+timespan.days+'</span><span class="label">Days</span></div></div>';
			container += '<div class="clearfix tim_elediv animated rotateIn" id="hours"><div class="wrapper"><span class="time">'+timespan.hours+'</span><span class="label">Hours</span></div></div>';
			container += '<div class="clearfix tim_elediv animated rotateIn" id="minutes"><div class="wrapper"><span class="time">'+timespan.minutes+'</span><span class="label">Minutes</span></div></div>';
			container += '<div class="clearfix tim_elediv animated rotateIn" id="seconds"><div class="wrapper"><span class="time">'+timespan.seconds+'</span><span class="label">Seconds</span></div></div>';
			// Add elements to parent element
			parent.find('.livetimer').html(container);
			// Tell the script that it has already been run
		} else {
			// Update each element
			parent.find('#days .time').html(timespan.days);
			parent.find('#hours .time').html(timespan.hours);
			parent.find('#minutes .time').html(timespan.minutes);
			parent.find('#seconds .time').html(timespan.seconds);
		}
		settings.start_time = new Date(settings.start_time);
		settings.start_time.setSeconds(settings.start_time.getSeconds() + 1);		

		// Check if target date has beeen reached
		if (settings.target_reached) {
			// Executetes function once timer reaches zero
			settings.onFinish();
		} else {
			// Updates the time every second for the visitor
			countdown_timeounteve = setTimeout(function () {
				$.fn.countdown.begin(parent, settings);
			}, 1000);
		}
	};
	// Removes the S in days hours minutes seconds
	$.fn.countdown.singularize = function (str) {
		return str.substr(0, str.length - 1);
	};

	// Converts local timezone to target timezone
	$.fn.countdown.convertTimezone = function (timezone) {
		var now, local_time, local_offset, utc;
		now = new Date();
		local_time = now.getTime();
		local_offset = now.getTimezoneOffset() * 60000;
		utc = local_time + local_offset;
		return new Date(utc + (3600000 * timezone));
	};

	// Returns time remaining data for view
	$.fn.countdown.getTimeRemaining = function (start, end, settings) {
		var timeleft, remaining, setting_id;
		remaining = {};
		timeleft = (end.getTime() - start.getTime());
		timeleft = (timeleft < 0 ? 0 : timeleft);
		

		// timeleft = (end.getTime() - new Date($.now()));
		// setting_id = settings.id
		// Check if target date has been reached
		(timeleft === 0) ? settings.target_reached = true : '';
		// Built deturn object

		remaining.days = Math.floor(timeleft / (24 * 60 * 60 * 1000));
		remaining.hours = Math.floor((timeleft / (24 * 60 * 60 * 1000) - remaining.days) * 24);
		remaining.minutes = Math.floor(((timeleft / (24 * 60 * 60 * 1000) - remaining.days) * 24 - remaining.hours) * 60);
		remaining.seconds = Math.floor(timeleft / 1000 % 60);
		return remaining;
	};
}(jQuery));