function changeCat(catSlug) {
  window.location.href = base_url + catSlug;
}
window.onload = function() { $('.home_page_search_tabbing.store_page_filter #main_category, .home_page_search_tabbing.store_page_filter #main_sub_cat, .home_page_search_tabbing.store_page_filter #product_srch-term').val(''); }
function loader_show() { $('#loader').show(); }
function loader_hide() { $('#loader').hide(); }
$(window).load(function() { $("#loader").fadeOut("slow"); });

// var carousel = $(".stream_carousel"), currdeg  = 0;
// $(".nxt_stream").on("click", { d: "n" }, rotate_slider);
// $(".prv_stream").on("click", { d: "p" }, rotate_slider);
// function rotate_slider(e){
//   if(e.data.d == "n"){
//     currdeg = currdeg - 60;
//   }
//   if(e.data.d == "p"){
//     currdeg = currdeg + 60;
//   }
//   carousel.css({
//     "-webkit-transform": "rotateY("+currdeg+"deg)",
//     "-moz-transform": "rotateY("+currdeg+"deg)",
//     "-o-transform": "rotateY("+currdeg+"deg)",
//     "transform": "rotateY("+currdeg+"deg)"
//   });
// }

$(".chk-btn").click(function () {
  var id = $(this).attr('id');
  var value = $(this).attr('value');
  if ($(this).attr('image_upload') == 'true') {
    if ($(this).is(":checked")) {
      var image_btn = '<div class="form-group chooseimg_single_div" id="remove'+id+'">'
      image_btn += '<label>'+value+'</label>';
      image_btn += '<input type="file" class="form-control" name="attr_image['+id+'][]" id="img_'+id+'" onchange="preview_image(`img_'+id+'`,1,`image_'+value+'`);" multiple />';
      image_btn += '<div id="image_'+value+'"></div>';
      $('#dynamic_attr').append(image_btn);
    } else {
      $('#remove'+id).remove()
    }
  }
});
$('.addproduct_tabs .next_button').click(function () {
  var ct_rl = $(this).attr('ct-rl'), tg_rl = $(this).attr('tg-rl'), required = 0;
  $(ct_rl+' input[required], '+ct_rl+' select[required], '+ct_rl+' textarea[required]').each(function(){ required = ($(this).val() != undefined && $(this).val() == '') ? 1 : 0; });
  if (required == 1) {
    $('.addproduct_tabs button[name="submit"]').click();
  } else {
    active_tab_show(tg_rl);
  }
});
$('.addproduct_tabs .prev_button').click(function () {
  var ct_rl = $(this).attr('ct-rl'), tg_rl = $(this).attr('tg-rl');
  active_tab_show(tg_rl);
});
function active_tab_show(tg_href){
  $('.addproduct_tabs .nav-item a[href="'+tg_href+'"]').click();
};
// $('.addproduct_tabs input[name="submit"]').click(function () {
//   console.log($(this).closest('form').find('[required]:first').html());
// });

function preview_image(id,type,upload_div) {
  var total_file=document.getElementById(id).files.length;
  for(var i=0; i<total_file; i++) {
    var file_data = event.target.files[i];
    var form_data = new FormData();
    var get_product_id = $('#product_id').val();
    var guser_id = $('#user_id').val();
    var product_id = (get_product_id != undefined && get_product_id != '') ? get_product_id : 0;
    var user_id = (guser_id != undefined && guser_id != '') ? guser_id : 0;

    form_data.append('file', file_data);
    form_data.append('type', type);
    form_data.append('attr', id);
    form_data.append('product_id', product_id);
    form_data.append('user_id', user_id);
    $.ajax({
      url: base_url + "Products/upload_file",
      dataType: 'json', 
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      type: 'post',
      success: function (response) {
        $('.plcholdr_img').remove();
        $('#'+upload_div).append("<div style='display:inline-block' id='"+response.id+"' class='singleimg_div'><input style='vertical-align:top;margin-right:10px;' type='hidden' name='is_defult' class='is_defult'><span ><img  style='' src='"+response.name+"'><span style='cursor:pointer;' class='remove_img'  onclick='remove_image("+response.id+","+type+","+response.id+");'><i class='fa fa-trash fa-fw'></i>Remove Image</span></span></div>");
      },
      error: function (response) {
        $('#msg').html(response); // display error response from the server
      }
    });
  }
}
function remove_image(id,type,img_name) {
  var add_type = $('#addproduct').val();
  $.ajax({
    url: base_url + "products/delete_file?id="+img_name+"&type="+type,
    type: 'get',
    success: function (response) { $('#'+id).remove(); }
  });
}

$('#chooseFile').bind('change', function(){
  var filename = $("#chooseFile").val();
  if (/^\s*$/.test(filename)) {
    $(".file-upload").removeClass('active');
    $("#noFile").html("No file chosen...");
  } else {
    $(".file-upload").addClass('active');
    $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
  }
});

$('#fireworks_audio').bind('change', function(){
  var filename = $("#fireworks_audio").val();
  if (/^\s*$/.test(filename)) {
    $(".file-upload").removeClass('active');
    $("#noFile").html("No file chosen...");
  } else {
    $(".file-upload").addClass('active');
    $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
  }
});

$(document).on('change','.event_image_upload',function(){
 var filename = $(".event_image_upload").val();
  if (/^\s*$/.test(filename)) {
    $(".file-upload").removeClass('active');
    $(".file-select-name").html("No file chosen...");
  } else {
    $(".file-upload").addClass('active');
    $(".file-select-name").text(filename.replace("C:\\fakepath\\", ""));
  }
});

$('.choose_File').bind('change', function(){
  var filename = $(this).val();
  var attachtype = $(this).attr('attachtype');
  if (/^\s*$/.test(filename)) {
    $(".file-upload."+attachtype).removeClass('active');
    $(".no_File."+attachtype).html("No file chosen...");
  } else {
    $(".file-upload."+attachtype).addClass('active');
    $(".no_File."+attachtype).text(filename.replace("C:\\fakepath\\", ""));
  }
});
$('.stream_settings .title .choosetip input[name="choose_tipicon_opt"]').click(function(){
  if ($(this).is(":checked")) {
    $('.stream_settings .stream_settings_tip_first_div').hide();
    $('.stream_settings .stream_settings_tip_sec_div').show();
  } else {
    $('.stream_settings .stream_settings_tip_first_div').show();
    $('.stream_settings .stream_settings_tip_sec_div').hide();
  }
});
$('.stream_settings .select_tipicon input[type="checkbox"]').click(function(){
  var checkas = $(this).attr('checkas');
  if ($(this).is(":checked")) {
    $('.stream_settings .select_tipicon input[type="checkbox"][checkas="'+checkas+'"]').not(this).prop('checked',false).removeAttr('required');
    $(this).attr('required','');
  } else {
    $('.stream_settings .select_tipicon input[type="checkbox"][checkas="'+checkas+'"]').prop('checked',false).removeAttr('required');
  }
});
$('.stream_settings .select_tipicon input[name="seltipsound"]').click(function(){
  if ($(this).is(":checked")) {
    $('.stream_settings .stream_settings_tip_third_div .select_tipicon input[name="seltipsound"]').not(this).prop('checked',false).removeAttr('required');
    $(this).attr('required','');
  } else {
    $('.stream_settings .stream_settings_tip_third_div .select_tipicon input[name="seltipsound"]').prop('checked',false).removeAttr('required');
  }
});
$('.stream_settings_stream_first_div.created_lobbys_list input[name="custom_name"], input[name="txt_livelobby_link"]').on('keypress', function(e){
  if (e.which == 13) {
    $(this).blur();
    e.preventDefault();
    return false;
  }
});

$('#sendmoney_form #transfer_amt, #sendmoney_form #account_number').on('keypress', function(e){
  if (e.which == 13) {
    $('#next_page').click();
  }
});
$('.stream_settings_stream_first_div.created_lobbys_list input[name="custom_name"]').on('focusout', function(e) {
  var lobby_name = $(this).val();
  var lobby_id = $(this).attr('lobby_id'); 
  var lobby_name = lobby_name.replace(/[^a-z0-9\s]/gi, '_').replace(/[_\s]/g, '_');
  $(this).val(lobby_name);
  lobby_exist_check(lobby_id,lobby_name,e);
});

$('#txt_livelobby_link').on('focusout', function(e) {
  var lobby_name = $(this).val();
  var lobby_name = lobby_name.replace(/[^a-z0-9\s]/gi, '_').replace(/[_\s]/g, '_');
  $(this).val(lobby_name);
});
$('.store_settings .store_link_submit input[name="store_link"]').on('keypress focus',function(e) {
  $('.store_exist_error span').html('');
  if (e.which == 13) {
    e.preventDefault();
    $(this).blur().closest('form').find('input[name="submit"]').trigger('click');
  }
})
$('.store_settings .store_link_submit .submit_custom_url').click(function(e) {
  var form = $(this).closest('form');
  var store_link =  form.find('input[name="store_link"]').val();
  if (store_link != undefined && store_link !='') {
    var postData = { 'store_link' : store_link }
    $.ajax({
      url : base_url +'store/store_link_update',
      data: postData,
      type: 'POST',
      beforeSend: function(){ loader_show(); },
      success: function(res) {
        loader_hide();
        var data = $.parseJSON(res);
        if (data.error != undefined && data.error != '') {
          $('.store_link_submit .store_exist_error').html(data.error);
          $(this).addClass('disabled');  
        } else {
          $(this).removeClass('disabled');
          window.location.reload();
        }
      }
    });
  }
});
$('.stream_settings_stream_first_div .default_user_custom_link').on('focusout', function(e) {
  var default_link_name = $(this).val();
  var default_link_name = default_link_name.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
  $(this).val(default_link_name);
  var postData = {
    'default_link_name' : default_link_name
  }
  $.ajax({
    url : base_url +'streamsetting/check_default_user_link',
    data: postData,
    type: 'POST',
    beforeSend: function(){
      $('.update_lobby_custom_url .custom_btn.submit_custom_url').addClass('disabled');
      if (e.which == 13) {
        return false;
      }
      $('.stream_settings_stream_first_div .custom_default_link_exist_loader span').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
    },
    success: function(res) {
      var data = $.parseJSON(res);
      $('.custom_default_link_exist_loader span').html('');
      if (data.error != undefined && data.error != '' && $('input[name="old_custom_name"]').val() != default_link_name) {
        $('.custom_default_link_exist_error').html(data.error);
        if (e.which == 13) {
          return false;
        }
        $('.update_lobby_custom_url .custom_btn.submit_custom_url').addClass('disabled');
      } else {
        $('.update_lobby_custom_url .custom_btn.submit_custom_url').removeClass('disabled');
      }
      if($('input[name="old_custom_name"]').val() == default_link_name) {
        if (e.which == 13) {
          return false;
        }
        $('.update_lobby_custom_url .custom_btn.submit_custom_url').addClass('disabled');
      }
    }
  });
});
$('.stream_settings .tip_div_stream_setting .nav-tabs .nav-link').click(function(){
  $('.stream_settings .tip_div_stream_setting .nav-tabs .nav-link').removeClass('active').attr('aria-selected','false');
  $(this).addClass('active').attr('aria-selected','true');
});


$('.ticket_bought_btn').click(function(e) {
  var postData = { get_my_membership : true };
  var is_admin = $(this).attr('is_admin');
  var free_membership = $(this).attr('free_membership');
  $.ajax({
    url : base_url +'membership/get_mebership_for_ticket',
    data: postData,
    type: 'POST',
    beforeSend: function() { loader_show(); },
    success: function(res) {
      var res = $.parseJSON(res);
      res.id = e.target.id
      loader_hide();
      ticket_membership_div(res);
      return false;
    }
  });  
});
$('.force_start_stream_main_div #force_start_stream_checkbox').click(function(e) {
  var force_start_stream = $(this).is(":checked") ? 'on' : 'off';
  var postData = { 'force_start_stream' : force_start_stream };
  $.ajax({
    url : base_url +'streamsetting/force_start_stream',
    data: postData,
    type: 'POST',
    beforeSend: function() { loader_show(); },
    success: function(res) {
      loader_hide()
      return false;
    }
  });  
});
$('.force_start_stream_main_div #start_twitch_stream_checkbox').click(function(e) {
  var start_twitch_stream = $(this).is(":checked") ? 'on' : 'off';
  var postData = { 'start_twitch_stream' : start_twitch_stream };
  $.ajax({
    url : base_url +'Livelobby/start_twitch_stream',
    data: postData,
    type: 'POST',
    beforeSend: function() { loader_show(); },
    success: function(res) {
      loader_hide()
      return false;
    }
  });  
});
$('.force_start_stream_main_div #auto_sync_stream_checkbox').click(function(e) {
  var auto_sync_stream = $(this).is(":checked") ? 'on' : 'off';
  var postData = { 'auto_sync_stream' : auto_sync_stream };
  $.ajax({
    url : base_url +'streamsetting/auto_sync_stream',
    data: postData,
    type: 'POST',
    beforeSend: function() { loader_show(); },
    success: function(res) {
      loader_hide()
      return false;
    }
  });  
});

$('.balance_view_icon').click(function(e) {
  var show_available_balance = ($(this).hasClass('balance_show')) ? 'on' : (($(this).hasClass('balance_hide')) ? 'off' : '');
  var postData = { 'show_available_balance' : show_available_balance };
  $.ajax({
    url : base_url +'User/show_available_balance',
    data: postData,
    type: 'POST',
    beforeSend: function() { loader_show(); },
    success: function(res) {
      if($('.balance_view_icon').hasClass('balance_show')){
        $('.balance_view_icon').addClass('balance_hide');
        $('.balance_view_icon i').addClass('fa-eye-slash');
        $('.balance_view_icon i').removeClass('fa-eye');
        $('.balance_view_icon').attr('data-original-title', 'Hide <br> Balance');
        $('.balance_view_icon').removeClass('balance_show');
        $('.header_available_balance div').removeClass('hide_balance');
      }else{
        $('.balance_view_icon').addClass('balance_show');
        $('.balance_view_icon i').addClass('fa-eye');
        $('.balance_view_icon i').removeClass('fa-eye-slash');
        $('.balance_view_icon').attr('data-original-title', 'Show <br> Balance');
        $('.balance_view_icon').removeClass('balance_hide');
        $('.header_available_balance div').addClass('hide_balance');
      }
      $('.balance_view_icon').tooltip('hide');
      $('.balance_view_icon').blur();
      loader_hide();
      return false;
    }
  });  
});

function ticket_membership_div(args) {
  var allow_free_ticket = (args.is_admin == '1' || args.is_free_membership == '1') ? '1': '0';
  var event_ticket_form = '<div class="lobby_data_hidden_data hide">'+$('#event_ticket_form').html()+'</div>';
  var dic_lble = player_label = duration_button = free_admin_div = current_membership = month_membership_loop = year_membership_loop = '';
  
  if (allow_free_ticket == '1') {
    player_label = (args.is_admin == '1') ? 'ADMIN' : ((args.is_free_membership == '1') ? 'FREE MEMBERSHIP': '');
    
    free_admin_div += '<form class="membership_tkt_dis monthly_membership_plan" action="'+base_url+'livelobby/purchase_ticket_with_membership" method="post">'
    free_admin_div += '<h1 style="color: #ff5000;">'+player_label+'</h1>';
    free_admin_div += '<div class="col-sm-12 text-center margin-tpbtm-10"><button type="submit" name="without_membership" data-toggle="tooltip" data-placement="bottom" data-original-title="Proceed to Tournament" class="proceed_tournament">No Thanks Continue to Tournament</button></div>';
    free_admin_div += event_ticket_form;
    free_admin_div += '</form>';
  } else {
    duration_button = '<div class="row membership_switch_box"><div class="col-sm-12"><label class="switch" data-original-title="Switch Membership Duration" data-placement="bottom" data-toggle="tooltip"><input type="checkbox" name="membership_duration" id="membership_duration"><span class="slider round"></span></label></div></div>'

    if (args.get_activated_membership != null) {
      if (args.get_activated_membership.tournament_ticket_discount_type == 1) {
        dic_lble = args.get_activated_membership.tournament_ticket_discount_amount+'% Off Tournaments';
      } else if (args.get_activated_membership.tournament_ticket_discount_type == 2) {
        dic_lble = 'Max $'+args.get_activated_membership.tournament_ticket_discount_amount+' Off Tournaments';
      } else if (args.get_activated_membership.tournament_ticket_discount_type == 3) {
        dic_lble = 'ALL Tournaments FREE';
      }
      current_membership = '<div class="clearfix membership_list_div current_membership_plan">';
      current_membership += '<div class="row"><div class="col-sm-12 text-left"><h3>Current Membership</h3></div></div>';
      current_membership += '<div class="row select_membership">';
      current_membership += '<div class="col-sm-2" id = "current_plan" data-current_mplan = "'+args.get_activated_membership.amount+'">$'+args.get_activated_membership.amount+'</div>';
      current_membership += '<div class="col-sm-5 word_nowrap_ellipsis">'+dic_lble+'</div>';
      current_membership += '<div class="col-sm-2 word_nowrap_ellipsis">'+args.get_activated_membership.title+'</div>';
      current_membership += '<div class="col-sm-3 word_nowrap_ellipsis">'+((args.get_activated_membership.time_duration == "Month") ? 'Monthly': ((args.get_activated_membership.time_duration == "Year") ? 'Yearly' : args.get_activated_membership.time_duration))+' Membership</div>';
      current_membership += '</div></div>';
    }

    var month_membership_loop = '<div class="clearfix membership_list_div month_membership_list" id="month_membership_list" style="display: block;">';
    month_membership_loop += '<form class="membership_tkt_dis monthly_membership_plan" action="'+base_url+'livelobby/purchase_ticket_with_membership" method="post">';
    var month_plans_list = args.month_plans;
    if (month_plans_list.length > 0) {
      for (var i = 0; i < month_plans_list.length; ++i) {
        var dic_lble = '';
        if (month_plans_list[i].tournament_ticket_discount_type == 1) {
          dic_lble = month_plans_list[i].tournament_ticket_discount_amount+'% Off Tournaments';
        } else if (month_plans_list[i].tournament_ticket_discount_type == 2) {
          dic_lble = 'Max $'+month_plans_list[i].tournament_ticket_discount_amount+' Off Tournaments';
        } else if (month_plans_list[i].tournament_ticket_discount_type == 3) {
          dic_lble = 'ALL Tournaments FREE';
        }
        month_membership_loop += '<div class="row select_membership" onclick="$(this).find(`input:radio`).click();">';
        month_membership_loop += '<div class="col-sm-2"><input type="radio" name="select_membership" data-membership_amount ="'+month_plans_list[i].amount+'"  value="'+month_plans_list[i].id+'"> $'+month_plans_list[i].amount+'</div>';
        month_membership_loop += '<div class="col-sm-4 word_nowrap_ellipsis">'+dic_lble+'</div>';
        month_membership_loop += '<div class="col-sm-3 word_nowrap_ellipsis">'+month_plans_list[i].title+'</div>';
        month_membership_loop += '<div class="col-sm-3 word_nowrap_ellipsis">Membership</div>';
        month_membership_loop += '</div>';
      }
      month_membership_loop += '<div class="membership_discount"></div><div class="row margin-tpbtm-10">';
      month_membership_loop += '<div class="col-sm-12 text-center margin-tpbtm-10"><button type="submit" name="membership_purchase" value="1" data-toggle="tooltip" data-placement="bottom" data-original-title="Upgrade Membership"  class="btn btn-primary btn-block btn-lg membership_purchase_btn">Upgrade Membership</button></div>';
      (args.id == 'event_btn') ? month_membership_loop += '<div class="col-sm-12 text-center margin-tpbtm-10"><button type="submit" data-toggle="tooltip" data-placement="bottom" data-original-title="Proceed to Tournament" name="membership_purchase" value="0" class="proceed_tournament">No Thanks Continue to Tournament</button></div>' : '';
      month_membership_loop += '<input type="hidden" name="return_url" value="'+window.location.href+'">';
      month_membership_loop += '</div>';
      month_membership_loop += event_ticket_form;
    } else {
      month_membership_loop += 'Monthly Membership of discounts on tournaments is not available';  
      (args.id == 'event_btn') ? month_membership_loop += '<div class="col-sm-12 text-center margin-tpbtm-10"><button type="submit" name="membership_purchase" value="0" data-toggle="tooltip" data-placement="bottom" data-original-title="Proceed to Tournament" class="proceed_tournament">No Thanks Continue to Tournament</button></div>' : '';
      month_membership_loop += event_ticket_form;
    }
    month_membership_loop += '</form>';
    month_membership_loop += '</div>';

    var year_membership_loop = '<div class="clearfix membership_list_div year_membership_list" id="year_membership_list" style="display: none;">';
    year_membership_loop += '<form class="membership_tkt_dis yearly_membership_plan" action="'+base_url+'livelobby/purchase_ticket_with_membership" method="post">';
    var year_plans_list = args.year_plans;
    if (year_plans_list.length > 0) {
      for (var i = 0; i < year_plans_list.length; ++i) {
        var dic_lble = '';
        if (year_plans_list[i].tournament_ticket_discount_type == 1) {
          dic_lble = year_plans_list[i].tournament_ticket_discount_amount+'% Off Tournament';
        } else if (year_plans_list[i].tournament_ticket_discount_type == 2) {
          dic_lble = 'Max $'+year_plans_list[i].tournament_ticket_discount_amount+' Off Tournament';
        } else if (year_plans_list[i].tournament_ticket_discount_type == 3) {
          dic_lble = 'ALL Tournaments FREE';
        }
        year_membership_loop += '<div class="row select_membership">';
        year_membership_loop += '<div class="col-sm-2"><input type="radio" name="year_select_membership" data-membership_amount ="'+year_plans_list[i].amount+'" value="'+year_plans_list[i].id+'"> $'+year_plans_list[i].amount+'</div>';
        year_membership_loop += '<div class="col-sm-2 word_nowrap_ellipsis">'+year_plans_list[i].title+'</div>';
        year_membership_loop += '<div class="col-sm-5 word_nowrap_ellipsis">'+dic_lble+'</div>';
        year_membership_loop += '<div class="col-sm-3 word_nowrap_ellipsis">Membership</div>';
        year_membership_loop += '</div>';
      }
      year_membership_loop += '<div class="year_membership_discount"></div><div class="row margin-tpbtm-10">';
      year_membership_loop += '<div class="col-sm-12 text-center margin-tpbtm-10"><button type="submit" name="with_membership" data-toggle="tooltip" data-placement="bottom" data-original-title="Upgrade Membership" class="btn btn-primary btn-block btn-lg membership_purchase_btn" value = "1">Upgrade Membership</button></div>';
      (args.id == 'event_btn') ? year_membership_loop += '<div class="col-sm-12 text-center margin-tpbtm-10"><button type="submit" name="without_membership" data-toggle="tooltip" data-placement="bottom" data-original-title="Proceed to Tournament" class="proceed_tournament">No Thanks Continue to Tournament</button></div>' : '';
      year_membership_loop += '<input type="hidden" name="return_url" value="'+window.location.href+'">';
      year_membership_loop += '</div>';
      year_membership_loop += event_ticket_form;
    } else {
      year_membership_loop += 'Yearly Membership of discounts on tournaments is not available';  
      (args.id == 'event_btn') ? year_membership_loop += '<div class="col-sm-12 text-center margin-tpbtm-10"><button type="submit" name="without_membership" data-toggle="tooltip" data-placement="bottom" data-original-title="Proceed to Tournament" class="proceed_tournament">No Thanks Continue to Tournament</button></div>' : '';
      year_membership_loop += event_ticket_form;
    }
    year_membership_loop += '</form>';
    year_membership_loop += '</div>';
  }

  var senddata = '<div class="col-sm-12">';
  senddata += current_membership;
  senddata += '<div class="membership_subs_bx clearfix">';
  senddata += duration_button;
  senddata += month_membership_loop;
  senddata += year_membership_loop;
  senddata += free_admin_div;
  senddata += '</div></div>';
  var modaltitle = 'Tournaments Discounts';
  var add_class_in_modal_data = 'get_membership_discount_plan';
  var data = {
    'add_class_in_modal_data':add_class_in_modal_data,
    'modal_title': modaltitle,
    'senddata':senddata,
    'tag':'confirm_modal'
  }
  commonshow(data);
}


$(document.body).on('change', 'input[name^="file[]"]', function() {
  var filename = [];
  var get_id = $(this).attr('id');
  var data = $(this).attr('data-file');
  var datasplit = data.split('_');
  var sum_files = parseInt(datasplit[1]);
  sum_files += 1;
  var images = '';
  for (var i = 0; i < $(this).get(0).files.length; ++i) {
    filename.push($(this).get(0).files[i].name);
    images += '<img width="100" height="100" src="'+URL.createObjectURL(event.target.files[i])+'" class="hasfile_'+sum_files+'" />';
  }
  if (/^\s*$/.test(filename)) {
    $("#"+get_id).removeClass('active');
    $("#"+get_id+" #noFile").html("No file chosen...");
    $("#noFileselect_"+get_id).html("No file chosen...");
  } else {
    $(".image_output").append(images);
    $("#"+get_id).addClass('active');
    $('input[data-file="hasfile_'+datasplit[1]+'"]').hide();
    $('.file-select').append('<input type="file" name="file[]" data-file="hasfile_'+sum_files+'" class="chooseFile send_from_in">');
    var all_files = $('#file_upload_firstdiv>.image_output>img').length;
    $("#noFile").text(all_files+" files");
    $("#noFile.noFile_from_out").text(all_files+" files");
    // $("#noFileselect_"+get_id).text(all_files+" files");
  }
});

$(document.body).on('change', 'input[name^="reply_file[]"]', function() {
  var filename = [];
  var get_id = $(this).attr('id');
  var data = $(this).attr('data-file');
  var datasplit = data.split('_');
  var sum_files = parseInt(datasplit[1]);
  sum_files += 1;
  var images = '';
  for (var i = 0; i < $(this).get(0).files.length; ++i) {
    filename.push($(this).get(0).files[i].name);
    images += '<img width="100" height="100" src="'+URL.createObjectURL(event.target.files[i])+'" class="hasfile_'+sum_files+'" />';
  }
  if (/^\s*$/.test(filename)) {
    $("#"+get_id).removeClass('active');
    $("#"+get_id+" #noFile").html("No file chosen...");
    $("#noFileselect_"+get_id).html("No file chosen...");
  }
  else {
    $("#upload_div_"+get_id+" .image_output").append(images);
    $("#"+get_id).addClass('active');
    $('#upload_div_'+get_id+'>input[data-file="hasfile_'+datasplit[1]+'"]').hide();
    $('#upload_div_'+get_id+'>.file-select').append('<input type="file" name="reply_file[]" data-file="hasfile_'+sum_files+'" id="'+get_id+'" class="chooseFile">');
    var all_files = $('#upload_div_'+get_id+'>.image_output>img').length;
    $("#noFileselect_"+get_id).text(all_files+" files");
  }
});

$('.bet_report').click(function(){
  $('#bet_report_modal').show();
});
$('#bet_report_modal .close, #bet_report_modal .no').click(function(){
  $('#bet_report_modal').hide();
});
$('#bet_report_modal_status .close').click(function(){
  $('#bet_report_modal').hide();
  $('#bet_report_modal_status').hide();
});

$('.won_lost_status_btn .button').click(function(){
  $id = $(this).attr('pop_up_id');
  $form_id = $(this).attr('form_id');
  $('#'+$id+' form input.video_url').attr('required','');
  $('#'+$id+' form input.video_url').addClass('required_element');
  $('#'+$id+' .accept_risk').change(function(){
    if ($('#'+$id+' .active .accept_risk').is(':checked')) {
      $('#'+$id+' form input.video_url').removeAttr('required');
      $('#'+$id+' .active .required_element').css({'border': 'none','box-shadow': 'none'});
      $('#'+$id+' form input.video_url').removeClass('required_element');
      $('#'+$id+' form input.video_url').val('');
    } else {
      $('#'+$id+' form input.video_url').attr('required','');
      $('#'+$id+' form input.video_url').addClass('required_element');
      $('#'+$id+' form input.video_url').val('');
    }
  });
  $('form').not('form#'+$form_id).removeClass('active');
  $('#'+$id+' form#'+$form_id).toggleClass('active');
  $('.won_div').not('#'+$id).hide();
  $('.won_lost_status_btn .button').not('#'+$id).removeClass('active');
  $(this).toggleClass('active');
  $('#'+$id).toggle();
});

function lobby_exist_check(lobby_id,lobby_name,e) {
  var postData = {
    'lobby_id' : lobby_id,
    'lobby_name' : lobby_name
  }
  $.ajax({
    url : base_url +'streamsetting/check_lobby_name',
    data: postData,
    type: 'POST',
    beforeSend: function(){
      $('.lobby_exist_loader[lobby_id="'+lobby_id+'"] span').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
      return (e.which == 13) ? false : true;
    },
    success: function(res) {
      var data = $.parseJSON(res);
      $('.lobby_exist_loader span').html('');
      if (data.error != undefined && data.error != '') {
        (lobby_id != undefined && lobby_id != '') ? $('.created_lobbys_list .lobby_exist_error[lobby_id="'+lobby_id+'"]').html(data.error) : '';
        $('.lobby_exist_error_add').html(data.error);
        $('.custom_btn.submit_custom_url[lobby_id="'+lobby_id+'"]').addClass('disabled');
        (e.target.id != 'submitPlaceChallenge') ? $('#submitPlaceChallenge').attr("disabled", true) : '';
        return (e.which == 13) ? false : true;
      } else {
        (lobby_id != undefined && lobby_id != '') ? $('.created_lobbys_list .lobby_exist_error[lobby_id="'+lobby_id+'"]').html(data.error) : '';
        $('.lobby_exist_error_add').html(data.error);
        $('.custom_btn.submit_custom_url[lobby_id="'+lobby_id+'"]').removeClass('disabled');
        $('#submitPlaceChallenge').attr("disabled", false);
        if (e.target.id == 'submitPlaceChallenge') {
          var challenge_type = ($('#'+e.target.id).attr('challenge_type') != undefined && $('#'+e.target.id).attr('challenge_type') != '' && $('#'+e.target.id).attr('challenge_type') != null) ? $('#'+e.target.id).attr('challenge_type') : '';
          lobby_is_valid(challenge_type);
        }
      }
      // if($('input[name="old_custom_name"][lobby_id="'+lobby_id+'"]').val() == lobby_name) {
      //   $('.update_lobby_custom_url .custom_btn.submit_custom_url').removeClass('disabled');
      //   return (e.which == 13) ? false : true;
      // $('.update_lobby_custom_url .custom_btn.submit_custom_url').addClass('disabled');
      // }
    }
  });
}
function lobby_is_valid(challenge_type='') {
  var amount      = $('#lobby_amount').val();
  var game_image_id   = $('#game_image_id').val();
  var sub_name      = $('#sub_name').val();
  var game_type     = $('#game_type').val();
  var game_timer    = $('#game_timer').val();
  var game_category   = $('#game_category').val();
  var game_name     = $('#name').val();
  var device_number   = $('#number').val();
  var select_game_category = $('#select_game_category').val();
  var select_game_name = $('#select_game_name').val();
  var select_subgame_name_id = $('#select_subgame_name_id').val();
  var best_of = $('#best_of').val();
  var livelobby_link = $('#txt_livelobby_link').val();
  if (game_category == '') {
    alert('please select a game system');
    return false;
  } else if (game_name == '0'){
    alert('please select a game name to continue');
    return false;
  } else if (sub_name == '0'){
    alert('please select a game sub category to continue');
    return false;
  } else if (select_game_category == ''){
    alert('please enter game system');
    return false;
  } else if (select_game_name == ''){
    alert('please enter stream title to continue');
    return false;
  } else if (select_subgame_name_id == ''){
    alert('please enter game info or stream info to continue');
    return false;
  } else if(game_type == '0' || !$.isNumeric(game_type)){
    alert('please select a game size to continue');
    return false; 
  } else if(game_timer == ''){
    alert('please select a wait timer to continue');
    return false; 
  } else if(device_number == ''){
    alert('please enter your gamer tag');
    return false;
  } else if (amount == '') {
    alert('please enter the amount');
    return false;
  } else if (best_of == '') {
    alert('please Select best of');
    return false;
  } else if (game_image_id == ''){
    alert('please select a game image to continue');
    return false;
  } else if ($('.file-upload #chooseGameimage').get(0) != undefined && $('.file-upload #chooseGameimage').get(0).files.length === 0){
    var imagename = $("#chooseGameimage").val();
    if (imagename != null && imagename != '') {
      var image_extension = imagename.substr((imagename.lastIndexOf('.') + 1));
      validExtensions = ["gif","jpeg","png","jpg","GIF","JPEG","PNG","JPG"];
      if ($.inArray(image_extension, validExtensions) == -1){
        alert('This type of files are not allowed!');
        return false;
      }
    } else {
      alert('Please upload image');
      return false;
    }
  } else if (livelobby_link ==''){
    alert('Please Enter Live Lobby link');
    $('#txt_livelobby_link').focus();
    return false;
  } else {
    //  if($('.file-upload #chooseGameimage').get(0).files.length){
    //   var imagename = $("#chooseGameimage").val();
    //   var image_extension = imagename.substr((imagename.lastIndexOf('.') + 1));
    //   validExtensions = ["gif","jpeg","png","jpg","GIF","JPEG","PNG","JPG"];
    //   if ($.inArray(image_extension, validExtensions) == -1){
    //     alert('This type of files are not allowed!');
    //     return false;
    //   }
    // }
    if ($('#change_lobby_details #frm_game_create').length > 0 || challenge_type == "game_cration") {
      return true;
    } else {
      var senddata = '<div class="col-sm-12"><h2 class="text-left"> Would you like to input rules? </h2></div><div class="col-sm-12"><h3><a id = "add_rules_yes" class="button no">Yes</a><a class="button no" id = "add_rules_no">No</a></h3></div>';
      var modaltitle = 'Add Rules';
      var add_class_in_modal_data = 'add_rules_modal';
      var data = {
        'add_class_in_modal_data':add_class_in_modal_data,
        'modal_title': modaltitle,
        'senddata':senddata,
        'tag':'add_rules_modal_show'
      }
      commonshow(data);
      // $('#frm_game_create').submit(); 
    }
  }
}
$('#start_trnmnt_btn').click(function () {
    var lobby_id = $('input[name="lobby_id"]').val();
    var user_id = $('input[name="user_id"]').val();
    var senddata = '<div class="col-sm-12">';
    senddata += '</div><div class="col-sm-12"><h3><a class="button yes confirm_start_tournament" id="confirm_start_tournament">OK</a><a class="button no">Cancel</a></h3></div>';
    var modaltitle = 'Are you sure to start Tournament';
    var add_class_in_modal_data = 'start_tournament_action_cls';
    var data = {
      'add_class_in_modal_data':add_class_in_modal_data,
      'modal_title': modaltitle,
      'senddata':senddata,
      'tag':'confirm_modal'
    }
    commonshow(data);    
});

$('#confirm_start_tournament').click(function () {
    var lobby_id = $('input[name="lobby_id"]').val();
    var user_id = $('input[name="user_id"]').val();
    var postData = {
      'lobby_id': lobby_id,
      'user_id':user_id,
    };
    $.ajax({
      url: base_url+'livelobby/start_tournament',
      type: "post",
      data: postData,
      success: function(data) {
        // var data = $.parseJSON(data);
        // $('#bet_report_modal').hide();
        // $('#bet_report_modal_status').hide();
        // var lobby_custom_name = $('input[name="lobby_custom_name"]').val();
        // socket_data = {
        //   'group_bet_id': data.get_updated_detail['group_bet_id'],
        //   'lobby_id': lobby_custom_name,
        //   'table_cat': data.get_updated_detail['table_cat'],
        //   'win_lose_status': data.get_updated_detail['win_lose_status'],
        //   'user_id' : data.get_updated_detail['user_id'],
        //   'ref_id' : data.ref_id
        // };
        // socket.emit( 'send_report_group_data', socket_data );
      }
    });
});
function remove_friend(user_id,friend_id) {
  if(confirm("Are you sure you want to delete this friend?")){
    var user_id = user_id;
    var friend_id = friend_id;
    var postData = {
      'user_id': user_id,
      'friend_id':friend_id
    };
    $.ajax({
      type: 'post',
      url: base_url+'Friend/remove_friend/',
      data: postData,
      success: function(res){
        var res = $.parseJSON(res);
        $('.friend_list_main_row>ul>li#frind_row'+friend_id).remove();
      }
    });
  }
  else{
    return false;
  }        
}

function pwauth() {
  var id = $('.game_pw_submit .game_id').val();
  var pw = $('.game_pw_submit .game_password').val();
  var postData = {
    'game_id': id,
    'game_password':pw
  };
  $.ajax({
    type: 'post',
    url: base_url+'Game/gamePasswordAuth/',
    data: postData,
    success: function(res){
      if (res=='no') {
        $('.game_pw_submit .game_pw .pw_error').remove();
        $('.game_pw_submit .game_pw').append('<div style="color:red;" class="pw_error">** Please Enter Correct Password</div>')
      }
      else{
        $('.game_pw_submit .game_pw .pw_error').remove();
        window.location.href = base_url+"game/gameDetails/"+id;
        return true;
      }
    }
  });
}
$('.game_password_div .game_password_switch_button input').click(function(){
  if(this.checked){
    $('.game_password_div .game_password').show();
    $('.game_password_div .game_password').attr('name','game_password');
    $('.game_password_div .game_password').attr('required','');
  }else{
    $('.game_password_div .game_password').hide();
    $('.game_password_div .game_password').removeAttr('name');
    $('.game_password_div .game_password').removeAttr('required');
  }
});

$('.lobby_password_div .lobby_password_switch_button input').click(function(){
  if(this.checked){
    $('.lobby_password_div .lobby_password').show();
    $('.lobby_password_div .lobby_password').attr('name','lobby_password');
    $('.lobby_password_div .lobby_password').attr('required','');
  }else{
    $('.lobby_password_div .lobby_password').hide();
    $('.lobby_password_div .lobby_password').removeAttr('name');
    $('.lobby_password_div .lobby_password').removeAttr('required');
  }
});


$("#ckbCheckAll").click(function(){
 $(".image_output img").remove();
 $("#noFile.noFile_from_out").html("No file chosen...");             
 $("#noFile.noFile_from_in").html("No file chosen...");
 $("#noFile").html("No file chosen...");    
 $('.send_mail_form').hide();
 $('.create_message').click(function(){
  $('.send_mail_to_all').toggle();
  var myCheckboxes = new Array();
  $("input.select_mail:checked").each(function() {
    myCheckboxes.push($(this).val());
  });
  $('.send_mail_form').hide();
  $('#user_id_array').val(myCheckboxes);
});
 if(this.checked){
  $('.select_mail').each(function(){
    this.checked = true;
  })
  $('.create_message').show();
  $('#accordion').hide();
}else{
  $('.select_mail').each(function(){
    this.checked = false;
  })
  $('.create_message').hide();
  $('.send_mail_to_all').hide();
}
});

function fortest()
{
  $('.mail_txtarea').toggle();
  $('input.send_mail_friend').toggle();
  $('.send_mail_box .file-upload_div').toggle();
}
$('.team_li a').hover(function(){
  $('.team_li img').attr('src', base_url+'assets/frontend/images/team_icon_white.png');
},
function(){
  $('.team_li img').attr('src', base_url+'assets/frontend/images/team_icon.png');
});

$('.stream_li a').hover(function(){
  $('.stream_li img').attr('src', base_url+'assets/frontend/images/broadcast_white.png');
},
function(){
  $('.stream_li img').attr('src', base_url+'assets/frontend/images/broadcast.png');
});

$('.hover_effect.active img').attr('src', $('.hover_effect.active a').attr('hover_src'));
$('.hover_effect a').not('.hover_effect.active a').hover( function() {
  $(this).children('img').attr('src', $(this).attr('hover_src'));
}, function() {
  $(this).children('img').attr('src', $(this).attr('src'));
});

$('.store_li a').hover(function(){
  $('.store_li img').attr('src', base_url+'assets/frontend/images/store_icon_white.png');
},
function(){
  $('.store_li img').attr('src', base_url+'assets/frontend/images/store_icon.png');
});

$('.adminList .store_li a').hover(function(){
  $('.adminList .store_li img').attr('src', base_url+'assets/frontend/images/store_icon_white.png');
},
function(){
  $('.adminList .store_li img').attr('src', base_url+'assets/frontend/images/store_icon_orange.png');
});

$('li.togglemenu').click(function(){
  $('.toogle_div.'+$(this).children('a').attr('toggle_tg')).toggle();
  ($(this).find('span.caret_icn>i').hasClass('fa-caret-down')) ?  $(this).find('span.caret_icn>i').attr('class','fa fa-caret-up') : $(this).find('span.caret_icn>i').attr('class','fa fa-caret-down');
});

function zip_code_form()
{
  var zip_code = document.getElementById("zip_code").value;
  if (zip_code != '') {
    var regex = /[0-9]/g;
    var result = zip_code.match(regex);
    if(result == null) {
     document.getElementById('errors8').innerHTML = "* Please enter valid zip_code";
     return false;
   }
   document.getElementById('errors8').innerHTML = "";
   zip_code.style.borderColor = "green";
 }
 else {
  document.getElementById('errors8').innerHTML = "* Please enter zip_code";
  return false;
}
}




function mob_no_form() {
  var number = document.getElementById("number").value;
  if (number != '') {
    var regex = /[0-9]/g;
    var result = number.match(regex);
    if (result == null) {
      document.getElementById('errors4').innerHTML = "* Please enter valid number";
      return false;
    }
    document.getElementById('errors4').innerHTML = "";
    number.style.borderColor = "green";
  } else {
    document.getElementById('errors4').innerHTML = "* Please enter number";
    return false;
  }
}


function validateForm() {
  var x = document.myForm.name;

  if (x.value == null || x.value == '') {
    x.style.borderColor = "red";
    document.getElementById('errors').innerHTML = "Please enter your name";
    document.myForm.name.focus();
    return false;
  }
  else {
    document.getElementById('errors').innerHTML = "";
    x.style.borderColor = "green";
  }

  var x1 = document.myForm.lname;
  if (x1.value == null || x1.value == '') {
    x1.style.borderColor = "red";
    document.getElementById('errorss').innerHTML = "Please enter your last name";
    document.myForm.lname.focus();
    return false;
  }
  else {
    document.getElementById('errorss').innerHTML = "";
    x1.style.borderColor = "green";
  }

  var em = document.myForm.email.value;
  var atpos = em.indexOf("@");
  var dotpos = em.lastIndexOf(".");
  if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= em.length) {
    document.myForm.email.style.borderColor = "red";
    document.getElementById('errors1').innerHTML = "* Please enter a valid email";
    document.myForm.email.focus();
    return false;
  }
  else {
    document.getElementById('errors1').innerHTML = "";
    document.myForm.email.style.borderColor = "green";
  }

  var pasw = document.myForm.password;

  if (pasw.value == null || pasw.value == '') {
    pasw.style.borderColor = "red";
    document.getElementById('errors2').innerHTML = "* Please enter password";
    document.myForm.password.focus();
    return false;
  }
  else {
    document.getElementById('errors2').innerHTML = "";
    pasw.style.borderColor = "green";
  }

  var pas = document.myForm.confirm_password;

  if (pas.value == null || pas.value == '') {
    pas.style.borderColor = "red";
    document.getElementById('errors3').innerHTML = "* Please retype the password";
    document.myForm.confirm_password.focus();
    return false;
  }
  else {
    document.getElementById('errors3').innerHTML = "";
    pas.style.borderColor = "green";
  }

  var num = document.myForm.number;

  if (num.value == null || num.value == '') {
    num.style.borderColor = "red";
    document.getElementById('errors4').innerHTML = "* Please enter a number";
    document.myForm.number.focus();
    return false;
  }
  else {
    document.getElementById('errors4').innerHTML = "";
    num.style.borderColor = "green";
  }

  var loc = document.myForm.location;

  if (loc.value == null || loc.value == '') {
    loc.style.borderColor = "red";
    document.getElementById('errors5').innerHTML = "* Please enter Location";
    document.myForm.location.focus();
    return false;
  }
  else {
    document.getElementById('errors5').innerHTML = "";
    loc.style.borderColor = "green";
  }

  var country = document.myForm.country;

  if (country.value == null || country.value == '') {
    country.style.borderColor = "red";
    document.getElementById('errors6').innerHTML = "* Please select country";
    document.myForm.country.focus();
    return false;
  }
  else {
    document.getElementById('errors6').innerHTML = "";
    country.style.borderColor = "green";
  }

  var state = document.myForm.state;

  if (state.value == null || state.value == '') {
    state.style.borderColor = "red";
    document.getElementById('errors7').innerHTML = "* Please select state";
    document.myForm.state.focus();
    return false;
  }
  else {
    document.getElementById('errors7').innerHTML = "";
    state.style.borderColor = "green";
  }

  var zip_code = document.myForm.zip_code;

  if (zip_code.value != '') {
   zip_code.style.borderColor = "red";
   document.getElementById('errors8').innerHTML = "";
   zip_code.style.borderColor = "green";
   document.myForm.zip_code.focus();
 }
 else {
  zip_code.style.borderColor = "red";
  document.getElementById('errors8').innerHTML = "* Please enter zip_code";
  document.myForm.zip_code.focus();
  return false;
}



var chbox = document.myForm.chekbox;
if (!chbox.checked) {
  alert('You must agree that your age is not bellow then 18 years.');
  return false;
}

var cbox = document.myForm.checkbox;
if (!cbox.checked) {
  alert('You must agree to the terms first.');
  return false;
}
}

$('#confirm_password').on('keyup', function () {
  if ($(this).val() == $('#password').val()) {
    $('#message').html('matching').css('color', 'green');
  } else $('#message').html('not matching').css('color', 'red');
});

$('#amount').blur(function () {
  if ($(this).val() < 1) {
    $('#demo').html('The Miniumum Entry is $1');
    $(this).val('');
  } else {
    $('#demo').html('');
  }
});
$('#lobby_amount').blur(function () {
  var min_amnt = (free_event == 'free') ? '0' : '0.05';
  if ($(this).val() < min_amnt) {
    $('#demo').html('The Miniumum Entry is $'+min_amnt);
    $(this).val('');
  } else {
    $('#demo').html('');
  }
});
$('select#system').click(function(){
  $("select#game option.game_list_front_side").remove();
  $("select#subgame option.subgame_list_front_side").remove();
});

$('select#system').change(function(){
 $(this).find(':selected').addClass('selected')
 .siblings('option').removeClass('selected');
});
$('select#game').click(function(){
  $("select#subgame option.subgame_list_front_side").remove();
});
$('select#game').change(function(){
  $(this).find(':selected').addClass('selected')
  .siblings('option').removeClass('selected');
});
$('select#subgame').change(function(){
  $(this).find(':selected').addClass('selected')
  .siblings('option').removeClass('selected');
});
$('select#playstore_system').click(function(){
  $("select#playstore_game option.playstore_game_list_front_side").remove();
  $("select#playstore_subgame option.playstore_subgame_list_front_side").remove();
});
$('select#playstore_system').change(function(){
  $(this).find(':selected').addClass('selected')
  .siblings('option').removeClass('selected');
});
$('select#playstore_game').click(function(){
  $("select#playstore_subgame option.playstore_subgame_list_front_side").remove();
});
$('select#playstore_game').change(function(){
  $(this).find(':selected').addClass('selected')
  .siblings('option').removeClass('selected');
});
$('select#playstore_subgame').change(function(){
  $(this).find(':selected').addClass('selected')
  .siblings('option').removeClass('selected');
});
$('#countries_states1, .countries_states1').on('change',function(){
  var countryID = $(this).val();
  var tg_attr = ($(this).attr('tg_attr') != undefined && $(this).attr('tg_attr') !='') ? $(this).attr('tg_attr') : '';
  $(this).attr('country_id',$(this).children('option:selected').attr('country_id'));
  if ($('.selected_country_shortname').length>0) {
    $(this).next('.selected_country_shortname').val($(this).children('option:selected').attr('country_shortname'));
  }
  if (!$.isNumeric(countryID) && $(this).attr('country_id').length>0) {
    var countryID = $(this).attr('country_id');
  }
  if(countryID){
    $.ajax({
      type:'POST',
      url:base_url+'signup/select_country',
      data:'country_id='+countryID,
      success:function(html) {
        (tg_attr !='') ? $('select[tg_attr="'+tg_attr+'"]#state').html(html) : $('#state').html(html);
      }
    }); 
  } else {
    $('#state').html('<option value="">Select State</option>');
  }
});



function serach_pending_player() {
  var srch_pending_plyr = $('#srch_pending_plyr').val();
  var postData = { 'srch_pending_plyr': srch_pending_plyr };
  $.ajax({
    type: "post",
    url: base_url + "friend/search_pending_request",
    data: postData,
    success: function (res) {
      $('.friend_list_main_row ul li').remove();
      var jdata = $.parseJSON(res);
      if (jdata['srch_pending_plyr_list']!=null) {
        $('.friend_list_main_row ul h5.no_payers_availble').hide();
        $('.friend_list_main_row ul').append(jdata['srch_pending_plyr_list']);
      } else {
       $('.friend_list_main_row ul').html('<h5 class="no_payers_availble"><div class="row bidhottest_err"><div class="col-sm-12" style="color: #FF5000;">Request Not Available</div></div></h5>');
     }
   }
 });
}
function serach_player() {
  var srch_plyr = $('#srch-plyr').val();
  var postData = { 'srch_plyr': srch_plyr };
  $.ajax({
    type: "post",
    url: base_url + "friend/search_friend",
    data: postData,
    success: function (res) {
      $('.friend_list_main_row ul li').remove();
      var jdata = $.parseJSON(res);
      if (jdata['srch_plyr_list']!=null) {
        $('.friend_list_main_row ul h5.no_payers_availble').hide();
        $('.friend_list_main_row ul').append(jdata['srch_plyr_list']);
      } else {
       $('.friend_list_main_row ul').html('<h5 class="no_payers_availble"><div class="row bidhottest_err"><div class="col-sm-12" style="color: #FF5000;">Friends Not Available</div></div></h5>');
     }
   }
 });
}
// name="fan_device_id"
$(".fan-add_form input#fan_device_id").keypress(function(event) {
  if ($(this).val() != '') {
    if (event.which == 13) {
      event.preventDefault();
      $(".fan-add_form").submit();
    }
  }
});
function search_my_player() {
  var srch_plyr = $('#srch-plyr').val();
  var postData = {
    'srch_plyr': srch_plyr
  };

  $.ajax({

    type: "post",

    url: base_url + "friend/search_my_friend",

    data: postData,

    success: function (res) {
      $('.friend_list_main_row ul li').remove();
      var jdata = $.parseJSON(res);
      if (jdata['srch_plyr_list']!=null) {
        $('.friend_list_main_row ul h5.no_payers_availble').hide();
        $('.friend_list_main_row ul').append(jdata['srch_plyr_list']);
      }
      else
      {
       $('.friend_list_main_row ul').html('<h5 class="no_payers_availble"><div class="row bidhottest_err"><div class="col-sm-12" style="color: #FF5000;">Friends Not Available</div></div></h5>');
     }
   }
 });
}    
function fans_search(lobby_id) {
  var fans_id, fan_tag, srch_fan, lobby_id;
  fans_id = $('#fans_id').val();
  fan_tag = $('#fan_tag').val();
  srch_fan = $('#srch-term').val();
  lobby_id = lobby_id;
  var postData = {
    'fans_id': fans_id,
    'fan_tag': fan_tag,
    'srch_fan': srch_fan,
    'lobby_id':lobby_id
  };
  $.ajax({
    type: "post",
    url: base_url + "livelobby/lobby_fan_search",
    data: postData,
    success: function (data) {
      var jdata = $.parseJSON(data);
      var append = '<ul>';
      for (var i = jdata.view_lobby_fans_list.length - 1; i >= 0; i--) {
        append += '<li class="frind_row_li" id="frind_row_'+jdata.view_lobby_fans_list[i].user_id+'"><div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0"><img src="'+base_url+'upload/profile_img/'+jdata.view_lobby_fans_list[i].image+'" alt=""></div><div class="col-lg-7 col-sm-6 col-md-3 bid-info"><h2>'+jdata.view_lobby_fans_list[i].fan_tag+'</h2></div><div class="col-lg-3 col-sm-5 col-md-3 primum-play play-n-rating"><span class=""><a href="'+base_url+'user/user_profile/?lobby_id='+jdata.view_lobby_fans_list[i].lobby_id+'&fans_id='+jdata.view_lobby_fans_list[i].user_id+'" target="_blank" class="view_profile">Profile</a></span></div></li>';
      }
      append += '</ul>';
      $('.body-middle .view_lobby_container .fanslist').html(append);
    }
  });
}
function game_search() {
  var game_name, sub_game_name, game_size, srch_term, system;
  system = $('#system').val();
  game_name = $('#game').val();
  sub_game_name = $('#subgame').val();
  game_size = $('#gamesize').val();
  srch_term = $('#srch-term').val();
  
  var postData = {
    'system': system,
    'game_name': game_name,
    'sub_game_name': sub_game_name,
    'game_size': game_size,
    'srch_term': srch_term
  };

  $.ajax({
    type: "post",
    url: base_url + "game/search_matches",
    data: postData,
    success: function (res) {
      var jdata = $.parseJSON(res);
      if (jdata.search_game == null) {
        $(".bidhottest.hottestgame.xbox1 ul li").remove();
        $(".bidhottest.hottestgame.xbox1 ul").append().html('<h5><div class="row bidhottest_err"><div class="col-sm-12" style="color: #FF5000;">Games Not Available</div></div></h5>');
        $(".bidhottest.hottestgame.matches ul li").remove();
        $(".bidhottest.hottestgame.matches ul").append().html('<h5><div class="row bidhottest_err"><div class="col-sm-12" style="color: #FF5000;">Games Not Available</div></div></h5>');
      } else {
        $(".bidhottest.hottestgame.xbox1 ul li, .bidhottest.hottestgame.xbox1 ul h5, .bidhottest.hottestgame.matches ul li, .bidhottest.hottestgame.matches ul h5").remove();
        for(i=0; i< jdata.game_name.length;i++)  {
          var append_data = '<li class="row"><div class="col-lg-2 col-sm-2 col-md-2 bidimg padd0">'+jdata.game_image[i]+'</div><div class="col-lg-3 col-sm-3 col-md-3 bid-info"><h3>'+jdata.game_name[i]+'</h3><p>'+jdata.sub_game_name[i]+'</p><span class="postby">Tag: '+jdata.device_id[i]+'</span></div><div class="col-lg-2 col-sm-2 col-md-2 uplod-day">';
          if (jdata.new_time[i] != '0') {
            append_data += '<h3 class="timer custom_timer" data-seconds-left='+jdata.new_time[i]+'></h3>';
          }
          append_data +='<p> Gamesize: --'+jdata.game_type[i]+"V"+jdata.game_type[i]+'--</p>';
          append_data += (jdata.best_of[i] != null) ? '<p class="best_of"> Best of '+jdata.best_of[i]+'</p>' : '';
          append_data += (jdata.description[i] !='' && jdata.description[i] != undefined) ? '<div class="primum-play text-center margin-tpbtm-20 desc_sec_div"><a class="animated-box" target="_blank" href="'+base_url+'livelobby/rulesAdd/'+jdata[i].id+'?type=match">Rules</a><div class="description" style="display:none;">'+jdata.description[i]+'</div></div>' : '';
          append_data +='</div><div class="col-lg-2 col-sm-2 col-md-2 divPrice"><div class="game-price premium"><h3>'+jdata.price[i]+'</h3></div></div><div class="col-lg-1 col-sm-1">';
          append_data += (jdata.game_icon[i] != undefined && jdata.game_icon[i] != '') ? '<img src="'+jdata.game_icon[i]+'" class="img img-responsive game_cat_img">' : '';
          append_data += '</div><div class="col-lg-2 col-sm-2 col-md-2 primum-play play-n-rating"><a href="'+jdata.id[i]+'" class="animated-box">play</a></div></li>';
          $(".bidhottest.hottestgame.matches ul").append(append_data); 
          $(".bidhottest.hottestgame.xbox1 ul").append(append_data); 
        }
      }
      if (jdata.game_list != null) {
        $("select#game option.game_list_front_side:selected").hide();
        var selectedGame = $('select#game option.game_list_front_side:selected').val();
        var elements = $();
        for(i=0; i< jdata.game_list.length;i++)  {
          elements = elements.add("<option class='game_list_front_side' value='"+jdata.game_list_id[i]+"'>"+jdata.game_list_name[i]+"</option>");
        }
        if (selectedGame != "") {
         $("select#game option.game_list_front_side").not('select#game option.game_list_front_side:selected').remove(); 
       } else {
        $("select#game option.game_list_front_side").remove();  
      }
      $("select#game").append(elements);
    } else {
      $("select#game option.game_list_front_side").remove();   
    }
    if (jdata.sub_game_list != null) {
      $("select#subgame option.subgame_list_front_side:selected").hide();
      var selectedsubgame = $('select#subgame option.subgame_list_front_side:selected').val();
      var sub_elements = $();
      for(i=0; i< jdata.sub_game_list.length;i++)  {
        sub_elements = sub_elements.add("<option class='subgame_list_front_side' value='"+jdata.subgame_list_id[i]+"'>"+jdata.subgame_list_name[i]+"</option>");
      }
      if (selectedsubgame != '') {
        $("select#subgame option.subgame_list_front_side").not("select#subgame option.subgame_list_front_side:selected").remove();   
      } else {
        $("select#subgame option.subgame_list_front_side").remove(); 
      }
      $("select#subgame").append(sub_elements);
    } else {
      $("select#subgame option.subgame_list_front_side").remove(); 
    }
    $('.bidhottest.hottestgame.xbox1 .timer').startTimer({
      classNames: {
        minutes: 'myClass-minutes',
        seconds: 'myClass-seconds',
        clearDiv: 'myClass-clearDiv',
        timeout: 'myClass-timeout'
      }
    });
    var data = { 'event': 'single_page_load' };
    commonappend(data);
    $('.bidhottest.hottestgame.matches .timer').startTimer({
      classNames: {
        minutes: 'myClass-minutes',
        seconds: 'myClass-seconds',
        clearDiv: 'myClass-clearDiv',
        timeout: 'myClass-timeout'
      }
    });
  },
});
}
function search_friends_mail() {
  var search_friends_mail = $('#search_friends_mail').val();
  var postData = {
    'search_friends_mail': search_friends_mail
  };
  $.ajax({
    type: "post",
    url: base_url + "mail/search_friends_mail",
    data: postData,
    success: function (res) {
      // search_friends_list_mail
      $('.friends_list .single_friend_in_list').remove();
      var jdata = $.parseJSON(res);
      if (jdata['search_friends_list_mail']!=null) {
        $('.friends_list h5.no_friends_availble').hide();
        $('.friends_list').append(jdata['search_friends_list_mail']);
      }
      else
      {
       $('.friends_list').html('<h5 class="no_friends_availble"><div class="row bidhottest_err"><div class="col-sm-12" style="color: #FF5000;">Friends Not Available</div></div></h5>');
     }
   }
 });
}  
function search_challenge_mail() {
  var search_challenge_mail = $('#search_challenge_mail').val();
  var postData = {
    'search_challenge_mail': search_challenge_mail
  };
  $.ajax({
    type: "post",
    url: base_url + "challenge_mail/search_challenge_mail",
    data: postData,
    success: function (res) {
      // search_friends_list_mail
      $('.friends_list .single_friend_in_list').remove();
      var jdata = $.parseJSON(res);
      if (jdata['search_friends_list_mail']!=null) {
        $('.friends_list h5.no_friends_availble').hide();
        $('.friends_list').append(jdata['search_friends_list_mail']);
      }
      else
      {
       $('.friends_list').html('<h5 class="no_friends_availble"><div class="row bidhottest_err"><div class="col-sm-12" style="color: #FF5000;">Friends Not Available</div></div></h5>');
     }
   }
 });
}
function lobby_game_search() {
  var game_name, sub_game_name, game_size, srch_term, system, is_event;
  system = $('#lobby_system').val();
  game_name = $('#lobby_game').val();
  sub_game_name = $('#lobby_subgame').val();
  game_size = $('#lobby_gamesize').val();
  srch_term = $('#lobby_srch-term').val();
  is_event = ($('input#is_event').val() == 1) ? 1 : 0;
  var postData = {
    'game_name': game_name,
    'sub_game_name': sub_game_name,
    'system': system,
    'game_size': game_size,
    'srch_term': srch_term,
    'is_event': is_event
  };
  $.ajax({
    type: "post",
    url: base_url + "livelobby/search_lobby",
    data: postData,
    // beforeSend: function(){ loader_show(); },
    success: function(res) {
      // loader_hide();
      var jdata = $.parseJSON(res);
      if (jdata.id == undefined) {
        $(".playstore_game_list_front ul li").remove();
        $(".playstore_game_list_front ul").append().html('<h5><div class="row bidhottest_err"><div class="col-sm-12" style="color: #FF5000;">Stream Not Available</div></div></h5>');
      } else { 
        var clone_data = $('.playstore_game_list_front .livelobby_home_row.hidden_row').clone();
        var clone_data_tm = $('.playstore_game_list_front .expiredtime_event.hidden_timer_row').clone();
        $(".playstore_game_list_front ul li, .playstore_game_list_front ul h5").remove();
        var append_data = [];
        if (jdata.lobby_list.length > 0) {
          for (var i = 0; i < jdata.lobby_list.length; i++) {
            var livewatchlink =  (jdata.is_event[i] == 1) ? 'Live Event' : 'Live Stream';
            // var append_data = '';
            if (jdata.is_event[i] == 1) {
              // append_data += '<li class="expiredtime_event row">'
              clone_data_tm.find('.countdown_with_day').removeClass('rgclose');
              if (jdata.registration_status[i] == 1) {
                if (jdata.is_past_event[i] == 'no') {
                  var countdown_ele = '<span class="year">'+jdata.evestart_year[i]+'</span><span class="month">'+jdata.evestart_month[i]+'</span><span class="day">'+jdata.evestart_day[i]+'</span><span class="hour">'+jdata.evestart_hour[i]+'</span><span class="minute">'+jdata.evestart_minute[i]+'</span><span class="second">'+jdata.evestart_second[i]+'</span>';
                  clone_data_tm.find('.countdown_with_day').attr({'id':'countdown_day_'+i, 'creator_time':jdata.creator_datetime[i]}).find('.countdown_ele').html(countdown_ele);

                  // append_data += '<div class="countdown_with_day clearfix" id="countdown_day_'+i+'" creator_time="'+jdata.creator_datetime[i]+'">';
                  // append_data += '<div class="countdown_ele hide"><span class="year">'+jdata.evestart_year[i]+'</span><span class="month">'+jdata.evestart_month[i]+'</span><span class="day">'+jdata.evestart_day[i]+'</span><span class="hour">'+jdata.evestart_hour[i]+'</span><span class="minute">'+jdata.evestart_minute[i]+'</span><span class="second">'+jdata.evestart_second[i]+'</span></div>';
                  // append_data += '<div class="clearfix tim_elediv animated rotateIn"><label>Registration Close in</label></div><span class="livetimer"></span>';
                  // append_data += '</div>';
                } else {
                  clone_data_tm.find('.countdown_with_day').html('<div class="clearfix tim_elediv animated rotateIn"><label>Event in Progress - Registration Open</label></div></div>');
                  // append_data += '<div class="countdown_with_day clearfix"><div class="clearfix tim_elediv animated rotateIn"><label>Event in Progress - Registration Open</label></div></div>';
                }
              } else {
                clone_data_tm.find('.countdown_with_day').addClass('rgclose').html('<div class="clearfix tim_elediv animated rotateIn"><label>Event in Progress - Registration Closed</label></div>');
                // append_data += '<div class="countdown_with_day rgclose clearfix"><div class="clearfix tim_elediv animated rotateIn"><label>Event in Progress - Registration Closed</label></div></div>';
              }
              // append_data += '</li>';
            }

            if (jdata.stream_status[i] == 'enable') {
              if(jdata.stream_type[i] == 1){
                var stream_url = 'https://player.twitch.tv/?channel='+jdata.stream_channel[i]+'&parent='+$(location).attr('hostname');
                clone_data.find('.lobby_stream_iframe_div .live_stream_iframe').attr('src',stream_url);
                clone_data.find('.lobby_stream_iframe_div .live_stream_iframe').show();
              }else if(jdata.stream_type[i] == 2){
                var rid = generateRandomId();
                var key = jdata.obs_stream_channel[i];
                clone_data.find('.lobby_stream_iframe_div .live_stream_iframe').next('div').attr('id',rid);
                clone_data.find('.lobby_stream_iframe_div .live_stream_iframe').next('div').find('video').addClass('obs_streaming');
                clone_data.find('.lobby_stream_iframe_div .live_stream_iframe').next('div').find('video').attr('data-key',key);
                clone_data.find('.lobby_stream_iframe_div .live_stream_iframe').next('div').find('video').attr('data-random_id',rid);
                clone_data.find('.lobby_stream_iframe_div .live_stream_iframe').next('div').find('video').attr('data-base_url',base_url);
                clone_data.find('.lobby_stream_iframe_div .live_stream_iframe').next('div').find('video').attr('data-user_id',jdata[i].lobby_crtor);
                clone_data.find('.lobby_stream_iframe_div .live_stream_iframe').next('div').show();
                // console.log($('#'+rid+' .obs_streaming'))
                var stream_arr = {
                   'stream_key' : key,
                   'base_url' : base_url,
                   'random_id' : rid,
                };
                setTimeout(function(){
                  stream_launch(stream_arr);
                },5000);
              }
            }
            clone_data.find('.bid-info h3').html(jdata.game_name[i]);
            clone_data.find('.bid-info p').html(jdata.sub_game_name[i]);
            clone_data.find('.divPrice .game-price h3').html(jdata.price[i]);
            clone_data.find('.divPrice p span').html(jdata.game_type[i]);
            (jdata.game_icon[i] != undefined && jdata.game_icon[i] !='') ? clone_data.find('.category_img img').attr('src',jdata.game_icon[i]) : '';
            (jdata.is_event[i] == 1) ? clone_data.find('.category_img .evetime label').html('Start : '+jdata.start_event[i]+' <br> '+jdata.start_event_time[i]):'';
            clone_data.find('.join_btn_div_lobby a').attr({'href': base_url+'livelobby/watchLobby/'+jdata.lobby_id[i], 'data-original-title': 'Join '+livewatchlink});
            clone_data.find('.lobby_device_tag a').html(jdata.device_id[i]);
            clone_data.find('.lobby_join_btn_div').find('.desc_sec_div').remove();
            (jdata.description[i] !='' && jdata.description[i] != undefined) ? clone_data.find('.lobby_join_btn_div .lb_list .dly_lst_verticle').append('<div class="primum-play text-right desc_sec_div"><a class="animated-box" target="_blank"  href="'+base_url+'livelobby/rulesAdd/'+jdata.lobby_id[i]+'">Rules</a><div class="description" style="display:none;">'+jdata.description[i]+'</div></div>') : '';
            // append_data += '<li class="row"><div class="col-lg-2 col-sm-2 col-md-2 bidimg padd0">'+jdata.game_image[i]+'</div><div class="col-lg-3 col-sm-3 col-md-3 bid-info"><h3>'+jdata.game_name[i]+'</h3><p>'+jdata.sub_game_name[i]+'</p><span class="postby">Tag: '+jdata.device_id[i]+'</span></div><div class="col-lg-2 col-sm-2 col-md-2 uplod-day">';
            // append_data += '<p> Gamesize: --'+jdata.game_type[i]+"V"+jdata.game_type[i]+'--</p>';
            // append_data += (jdata.description[i] !='' && jdata.description[i] != undefined) ? '<div class="primum-play text-center margin-tpbtm-20 desc_sec_div"><a class="animated-box" target="_blank" href="'+base_url+'livelobby/rulesAdd/'+jdata[i].id+'">Rules</a><div class="description" style="display:none;">'+jdata.description[i]+'</div></div>' : '';
            // append_data += '</div><div class="col-lg-2 col-sm-2 col-md-2 divPrice"><div class="game-price premium"><h3>'+jdata.price[i]+'</h3></div></div><div class="col-lg-1 col-sm-1 col-md-1">';
            // append_data += (jdata.game_icon[i] != undefined && jdata.game_icon[i] !='') ? '<img src="'+jdata.game_icon[i]+'" class="img img-responsive game_cat_img">' : '';
            // append_data += '</div><div class="col-lg-2 col-sm-2 col-md-2 primum-play play-n-rating"><a href="'+jdata.id[i]+'" class="animated-box">'+livewatchlink+'</a>';
            // (jdata.is_event[i] == 1) ? append_data += '<div class="evetime"><label class="text-center">Start : '+jdata.start_event[i]+' <br> '+jdata.start_event_time[i]+' </label></div>' : '';
            // append_data += '</div></li>';
            // append_data.push(clone_data);
            var append_data_with_timer = (jdata.is_event[i] == 1) ? '<li class="expiredtime_event row">'+$(clone_data_tm).html()+'</li><li class="row livelobby_home_row">'+$(clone_data).html()+'</li>' : '<li class="row livelobby_home_row">'+$(clone_data).html()+'</li>';
            $(".playstore_game_list_front ul").append(append_data_with_timer);
          }
          countdown_with_day();
        }
      }
      if (jdata.game_list != null) {
        $("select#lobby_game option.lobby_game_list_front_side:selected").hide();
        var selectedGame = $('select#lobby_game option.lobby_game_list_front_side:selected').val();
        var elements = $();
        for(i=0; i< jdata.game_list.length;i++)  {
          elements = elements.add("<option class='lobby_game_list_front_side' value='"+jdata.game_list_id[i]+"'>"+jdata.game_list_name[i]+"</option>");
        }
        if (selectedGame != ""){
          $("select#lobby_game option.lobby_game_list_front_side").not('select#lobby_game option.lobby_game_list_front_side:selected').remove(); 
        } else {
          $("select#lobby_game option.lobby_game_list_front_side").remove();  
        }
        $("select#lobby_game").append(elements);
      } else {
        $("select#lobby_game option.lobby_game_list_front_side").remove();
      }
      if (jdata.sub_game_list != null) {
        $("select#lobby_subgame option.playstore_subgame_list_front_side:selected").hide();
        var selectedsubgame = $('select#lobby_subgame option.playstore_subgame_list_front_side:selected').val();
        var sub_elements = $();
        for(i=0; i< jdata.sub_game_list.length;i++)  {
          sub_elements = sub_elements.add("<option class='playstore_subgame_list_front_side' value='"+jdata.subgame_list_id[i]+"'>"+jdata.subgame_list_name[i]+"</option>");
        }
        if (selectedsubgame != '') {
          $("select#lobby_subgame option.playstore_subgame_list_front_side").not("select#lobby_subgame option.playstore_subgame_list_front_side:selected").remove();   
        } else {
          $("select#lobby_subgame option.playstore_subgame_list_front_side").remove(); 
        }
        $("select#lobby_subgame").append(sub_elements);
      } else {
        $("select#lobby_subgame option.playstore_subgame_list_front_side").remove();
      }
      $('.playstore_game_list_front .timer').startTimer({
        classNames: {
          minutes: 'myClass-minutes',
          seconds: 'myClass-seconds',
          clearDiv: 'myClass-clearDiv',
          timeout: 'myClass-timeout'
        }
      });

      var data = { 'event': 'single_page_load' };
      commonappend(data);
    },
  });
}
$(document).ready(function () {
  var collapse_array = [".creates_live_lobbies_lable", ".joined_live_lobbies_lable", ".friend_challanges_lable", ".open_challanges_lable", ".tournaments_lable", ".live_events_lable", ".gifted_events_by_lable", ".gifted_events_to_lable", ".money_sent_lable", ".money_received_lable", ".tip_sent_lable", ".tip_received_lable", ".pending_results_lable", ".winning_games_lable", ".lost_challanges_lable"];
  $( collapse_array ).each(function( i,value ) {
    if ($(value).siblings('.collapse_border').find('ul').length == 0 || $(value).siblings('.collapse_border').children('div, span').find('ul').length == 0){
      // $(value).find('span').remove();
      // $(value).removeAttr('data-toggle');
      // $(value).siblings('div').removeClass('collapse_border');
      // $(value).parent('div').removeClass('collapse_margin');
    }
  });
  $('.tip_amount_error_row').hide();

  $('#submitPlaceChallenge').click(function (e) {
    var lobby_name = $('input[name="txt_livelobby_link"]').val();
    var lobby_id = '';
    lobby_exist_check('',lobby_name,e);
  });
  $('#change_lobby_details input[name="change_lobby_pw"]').keypress(function(event) {
    if (event.keyCode == 13) {
      $('#change_lobby_details #submitPlaceChallenge').click();
    }
  });
  $('#change_lobby_details #editLivelobby').click(function(){
    if (lobby_is_valid() === false) { return false; }
    var lobby_id = $('input[name="lobby_id"]').val();
    var group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
    var system = $('form.edit_lobby_details .game_category').val();
    var gamename = $('form.edit_lobby_details #name').val();
    var subgamename = $('form.edit_lobby_details #sub_name').val();
    var game_type = $('form.edit_lobby_details #game_type').val();
    var game_amount = $('form.edit_lobby_details #lobby_amount').val();
    var lobby_password = $('form.edit_lobby_details #lobby_password').val();
    var for_18_plus = $('form.edit_lobby_details #for_18_plus').prop('checked');
    if (for_18_plus == true) {
      for_18_plus = 'on';
    } else {
      for_18_plus = 'off';
    }
    // var description = $('form.edit_lobby_details #description').val();
    var image_id = $('form.edit_lobby_details #game_image_id').val();
    var custom_lby = $('#change_lobby_details #custom_lobby_check').prop('checked');
    var game_image_id = new FormData();
    if (custom_lby == true) {
      system = $('form.edit_lobby_details .custom_games #game_category').val();
      gamename = $('form.edit_lobby_details .custom_games .select_game_name').val();
      subgamename = $('form.edit_lobby_details .custom_games .select_subgame_name_id').val();
      custom_lby = 'on';
      var imgs = $('#frm_game_create #game_img #chooseGameimage').prop('files');
      if (imgs != undefined && imgs.length != 0) {
      for (var i = imgs.length - 1; i >= 0; i--) {
        game_image_id.append( 'game_image_id[]', imgs[i]);
      }
      } else {
        game_image_id.append('game_image_id', image_id);
      }
    } else {
      game_image_id.append('game_image_id', image_id);
      custom_lby = 'off';
    }
    var password_option = $('form.edit_lobby_details #password_option').prop('checked');
    if (password_option == true) {
      password_option = 'on';
    } else {
      password_option = 'off';
    }
    if (custom_lby !='' && lobby_id !='' && group_bet_id !='' && system !='' && gamename !='' && subgamename !='' && game_type !='' && game_amount !='' && password_option !='' && game_image_id !='') {
        lobby_details_change(custom_lby,lobby_id,group_bet_id,system,gamename,subgamename,game_type,game_amount,lobby_password,password_option,game_image_id,for_18_plus);
    }
  });

  // custom settings 
  $('#frm_game_create .custom_games').not('#change_lobby_details #frm_game_create .custom_games').hide();
  $('#frm_game_create .custom_game_size').not('#change_lobby_details #frm_game_create .custom_game_size').hide();
  var game_type_option = '<option>--Select Game Size--</option>';
  for (var i = 1; i <=16; i++) {
    game_type_option += '<option value="'+i+'">--'+i+'V'+i+'--</option>';
  }
  $('.custom_settings input[type=checkbox]').change(function(){
    $('#game_category').val('').trigger('change');
    if ($('.custom_settings  input:checkbox').is(':checked')) {
      $('input[name="personal_challenge_friend_id"]').val('1');
      $('#custom_game').val('yes');
      $('#frm_game_create .custom_games').show();
      $('#frm_game_create .custom_games .game_category').attr('name','game_category');
      $('#frm_game_create .custom_games .select_game_name').attr({ id:'select_game_name', name:'game_name' });
      $('#frm_game_create .custom_games .select_game_name, #frm_game_create .custom_games .select_subgame_name_id').val('');
      $('#frm_game_create .custom_games .select_subgame_name_id').attr({ id:'select_subgame_name_id', name:'subgame_name_id' });
      $('#frm_game_create .custom_games .select_game_category').attr('id','select_game_category');
      $('#frm_game_create #game_type').attr('name','game_type');
      $('#frm_game_create .select_games').hide();
      $('#frm_game_create .select_games select, .new_game-image').removeAttr('id name');
      $('#frm_game_create select#game_type option').remove();
      $('#frm_game_create select#game_type').append(game_type_option);
      $('#frm_game_create #game_img .file-upload, #frm_game_create #game_img .img-cls').remove();
      $('#frm_game_create #game_img').append('<div class="file-upload"><div class="file-select"><div class="file-select-button" id="fileName">Choose File</div><div class="file-select-name" id="noFile">No file chosen...</div> <input type="file" name="game_image" id="chooseGameimage" accept="image/*"></div></div>');
      if ($('#chooseGameimage').length) {
        $('#chooseGameimage').bind('change', function(){
          var filename = $("#chooseGameimage").val();
          if (/^\s*$/.test(filename)) {
            $(".file-upload").removeClass('active');
            $("#noFile").html("No file chosen...");
          } else {
            $(".file-upload").addClass('active');
            $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
          }
        });
      }
      commonfun();
    } else {
      $('#custom_game').val("");
      $('#frm_game_create .select_games').show();
      $('input[name="personal_challenge_friend_id"]').val('0');
      $('#frm_game_create .select_games .game_category').attr({ id:'game_category', name:'game_category' });
      $('#frm_game_create .select_games .game_name').attr({ id:'name', name:'game_name' });
      $('#frm_game_create .select_games .subgame_name_id').attr({ id:'sub_name', name:'subgame_name_id' });
      $('#frm_game_create #game_type').attr('name','game_type');
      $('#frm_game_create .select_games .game_category option:first, #frm_game_create .select_games .game_name option:first, #frm_game_create .select_games .subgame_name_id option:first').prop('selected',true);
      $('#frm_game_create select#game_type option').remove();
      $('#frm_game_create select#game_type').append('<option>--Select Game Size--</option>');
      $('#frm_game_create .custom_games').hide();
      $('#frm_game_create .custom_games input[type=text]').removeAttr('id');
      $('#frm_game_create .custom_games input[type=text], #frm_game_create .custom_games .game_category').removeAttr('name'); 
      $('#frm_game_create .custom_game_size').hide();
      $('#frm_game_create #game_img .file-upload').remove();
      $('.new_game-image').attr({ id:'game_image_id', name:'game_image_id' });
      chooseGameimage();
      commonfun();
    }
  });
  ($('.custom_settings input[type=checkbox]').html() != undefined && $('.custom_settings input[type=checkbox]').html() != null) ? $('.custom_settings input[type=checkbox]').not('#change_lobby_details .custom_settings input[type=checkbox]').trigger('click') : '';
  $('.edit_lobby_details #game_img .change_custlby_img').click(function(){
    $('.edit_lobby_details #game_img').html('<div class="file-upload"><div class="file-select"><div class="file-select-button" id="fileName">Choose File</div><div class="file-select-name" id="noFile">No file chosen...</div> <input type="file" name="game_image" id="chooseGameimage" accept="image/*" required></div></div>');
    chooseGameimage();
    commonfun();
  });

  var cnt = 1;
  $('.add_field_button').click(function (e) {
    e.preventDefault();    
    if(cnt < 3){
      $('.input_fields_wrap').append('<div><input type="text" name="url[]" class="form-control"/><a href="#" class="remove_field">Remove</a></div>');
      cnt++;
      console.log(cnt);
    }
  });
  
  $('.input_fields_wrap').on("click", ".remove_field", function (e) {
    e.preventDefault();    
    $(this).parent('div').remove();
    cnt--;
  });

  $("#user_email").on('change', function () {
    var useremail = $("#user_email").val();
    $.ajax({
      type: "POST",
      url: base_url + "signup/check_email",
      data: { uemail: useremail },
      success: function (data) {
        $("#errors1").html(data);
      },
    });
  });
});
function isNumbKey(evt,id) {
  var val = $('#'+id).val();
  if (val != '.') { 
    if (isNaN(val)) {
      val = val.replace(/[^0-9\.]/g,'');
      if(val.split('.').length>1)
        val =val.replace(/\.+$/,"");
    }
  }
  $('#'+id).val(val);
}
commonfun();
function commonfun() {
  $("#game_category").on('change', function() {
    var category_id = this.value;
    $.ajax({
      type: "POST",
      url: base_url + "game/get_game",
      data: { game_category_id: category_id },
      success: function (data) {
        var jdata = $.parseJSON(data);
        $("#name").empty();
        $("#name").append('<option value="0">--Select a Game--</option>');
        $.each(jdata, function (index, value) {
          $("#name").append('<option value="' + value.id + '"> ' + value.game_name + '</option>');
        });
      },
    });
  });
  $("#name").on('change', function (){
    var game_id = this.value;
    $.ajax({
      type: "POST",
      url: base_url + "game/get_subgame",
      data: { ajax_game_id: game_id },
      success: function (data) {
        var jdata = $.parseJSON(data);
        $("#sub_name").empty();
        $("#sub_name").append('<option value="0">--Select a Sub Game--</option>');
        $.each(jdata[0], function (index, value) {
          $("#sub_name").append('<option value="' + value.id + '"> ' + value.sub_game_name + '</option>');
        });
        $("#game_img").empty();
        var w = 0;
        $.each(jdata[2], function (index, value) {
          $("#game_img").append('<img class="img-cls" id="img' + value.id + '" style="margin-right: 31px;" onclick="a(' + value.id + ')" src="' + base_url + 'upload/game/' + value.game_image + '" width="100" height="80"/>');
          if (w == 0) {
            $('#game_image_id').val(value.id);
          }
          w++;
        });
        $("#description").html(jdata[1][0].game_description);
        var game_type_raw = jdata[1][0].game_type;
        var game_type = game_type_raw.split("|");
        var game_type_html = '';
        game_type_html = game_type_html + '<option value="0">--Select Game Type--</option>';
        for (i = 0; i < game_type.length; i++) {
          if (game_type[i] != '') {
            game_type_html = game_type_html + '<option value="' + game_type[i] + '">--' + game_type[i] + 'V' + game_type[i] + '--</option>';
          }
        }
        $('#game_type').html(game_type_html);
      },
    });
  });
  $('.delete_prdct_plrstr').click(function () {
    var product_id = $(this).attr('product-id');
    if (product_id.length) {
      var senddata = '<div class="col-sm-12"><h4 style="color: #ff5000;">';
      senddata += '</h4></div><div class="col-sm-12"><h3><a class="button yes" tg_id="'+product_id+'">OK</a><a class="button no">Cancel</a></h3></div>';
      var modaltitle = 'Are you sure to delete this product ?';
      var add_class_in_modal_data = 'delete_prdct_modal';
      var tgid = product_id;
      var data = {
        'add_class_in_modal_data':add_class_in_modal_data,
        'modal_title': modaltitle,
        'senddata':senddata,
        'tgid' : tgid,
        'tag':'commonmodalshow'
      }
      commonshow(data);
    }    
  });
  $('.is_second_banner').click(function () {
    var is_second_banner = $(this).attr('is_second_banner');
    var checked = (is_second_banner == '1') ? 'checked' : '';

    var lobby_id = $('input[name="lobby_id"]').val();
    var user_id = $('input[name="user_id"]').val();
    var senddata = '<div class="col-sm-12">';
    senddata += '<div class="text-left clearfix"><label class="switch"><input type="checkbox" name="is_stream_banner" id="is_stream_banner" '+checked+'><span class="slider round"></span></label></div>';
    senddata += '</div><div class="col-sm-12"><h3><a class="button yes">OK</a><a class="button no">Cancel</a></h3></div>';
    var modaltitle = 'Stream banner on Home page';
    var add_class_in_modal_data = 'set_banner_home_stream';
    var data = {
      'add_class_in_modal_data':add_class_in_modal_data,
      'modal_title': modaltitle,
      'senddata':senddata,
      'tag':'confirm_modal'
    }
    commonshow(data);    
  });
  $('.giveaway_settings').click(function () {
    var lobby_id = $('input[name="lobby_id"]').val();
    var amount = $(this).attr('gw_amount'),
        entrieslimit = $(this).attr('gw_entrieslimit'),
        date_of_giveaway = $(this).attr('gw_datetime'),
        description = $(this).attr('gw_description'),
        is_multiple_entries = $(this).attr('gw_multiple_entries') == 1 ? 'checked' : '',
        img = $(this).attr('gw_img'),
        fireworks_duration_seconds = $(this).attr('fireworks_duration_seconds'),
        gw_id = $(this).attr('gw_id'),
        registration_close_time = $(this).attr('ga_date_time_registration_close'),
        img = $(this).attr('gw_img'),
        fireworks_audio = $(this).attr('fireworks_audio'),
        use_custom_fireworks_audio = $(this).attr('use_custom_fireworks_audio') == 1 ? 'checked' : '',
        default_fireworks_audio = 'giveaway_default_sound.mp3',
        select_custom_sounds_css = default_sounds_css = 'display: none;',
        ga_title = $(this).attr('ga_title');

    if (img != '')
      var img_show = 'style="display: block"', file_upload_show = 'style="display: none"';
    else
      var img_show = 'style="display: none"', file_upload_show = 'style="display: block"';

    if (use_custom_fireworks_audio == 'checked') {
      select_custom_sounds_css = 'display: show;';
      if (fireworks_audio != '')
        var fireworks_audio_show = 'style="display: flex; align-items: center;"', fireworks_audio_upload_show = 'style="display:none"';
      else
        var fireworks_audio_show = 'style="display: none;"', fireworks_audio_upload_show = 'style="display:block" required';
    } else {
      default_sounds_css = 'display: show;';
      if (fireworks_audio != '')
        var fireworks_audio_show = 'style="display: flex; align-items: center;"', fireworks_audio_upload_show = 'style="display:none"';
      else
        var fireworks_audio_show = 'style="display: none;"', fireworks_audio_upload_show = 'style="display:block"';
    }

    if (parseFloat(amount).toFixed(2) == 0)
      var is_multiple_entries = '', is_multiple_entries_switch_disable = 'disabled';
    else 
      var is_multiple_entries_switch_disable = '';

    var senddata = '<div class="col-sm-12 myprofile-edit text-left">';
    senddata += '<form id="giveaway_setting" class="giveaway_setting_form" name="giveaway_setting" action="'+base_url+'giveaway/giveaway_setting/'+lobby_id+'" method="post"  enctype="multipart/form-data" files="true">';
    senddata += '<div class="form-group clearfix"><label class="col-lg-4">Giveaway Title </label><div class="input-group col-lg-8"><input type="text" name="ga_title" placeholder="Enter Giveaway Title" class="form-control" required value="'+ga_title+'"></div></div>';
    senddata += '<div class="form-group clearfix"><label class="col-lg-4">Set amount to Enter </label><div class="input-group col-lg-8"><input type="number" name="amount" placeholder="Enter amount" class="form-control" required value="'+amount+'" min="0"></div></div>';
    senddata += '<div class="form-group clearfix"><label class="col-lg-4">Limit Total Entries </label><div class="input-group col-lg-8"><input type="number" name="entrieslimit" placeholder="Enter Limits of entries" class="form-control" required value="'+entrieslimit+'" min="1" pattern="[0-9]"></div></div>';
    senddata += '<div class="form-group clearfix"><label class="col-lg-4">Registration Close Time </label><div class="input-group col-lg-8"><input type="text" name="registration_close_time" placeholder="MM/DD/YYYY HH:MM" class="form-control only_datetimepicker_giveaway" value="'+registration_close_time+'" required></div></div>';
    senddata += '<div class="form-group clearfix"><label class="col-lg-4">Date and Time of Giveaway </label><div class="input-group col-lg-8"><input type="text" name="datetime_giveaway" id="date" placeholder="MM/DD/YYYY HH:MM" class="form-control only_datetimepicker_giveaway" value="'+date_of_giveaway+'" required></div><div class="col-lg-4"></div><label class="col-lg-8 padd0 alert alert-danger datetime_giveaway_error_div" style="display: none;">Please Enter Giveaway time Longer than Registration Close time.</label></div>';
    senddata += '<div class="form-group clearfix"><label class="col-lg-4">Enter Description of Prize & Rules </label><div class="input-group col-lg-8"><textarea name="description" placeholder="Enter Description" class="form-control" required rows="4" style="resize: none;">'+description+'</textarea></div></div>';
    senddata += '<div class="form-group clearfix"><label class="col-lg-4">Enable Multiple Entries Per Player</label><div class="input-group col-lg-8"><label class="switch '+is_multiple_entries_switch_disable+'"><input type="checkbox" name="is_multiple_entries" '+is_multiple_entries+'><span class="slider round"></span></label></div></div>';
    senddata += '<div class="form-group clearfix"><label class="col-lg-4">Image </label><div class="input-group col-lg-8"><div '+img_show+'><span class="btn lobby_btn" onclick="$(this).parent(`div`).hide().next(`input`).show().attr(`required`,true);"> Change image</span><img class="img-cls img img-responsive" style="display: inline-block; width: 100px; padding: 4px;" src="'+base_url+'upload/all_images/'+img+'" alt="smile" /></div><input type="file" name="giveaway_img" accept="image/*" class="form-control" '+file_upload_show+' '+((gw_id != '' && gw_id != undefined) ? '' : 'required')+' onchange="PreviewAudio(this, $(this).hide().siblings(`div`).show().find(`img`))"></div></div>';
    senddata += '<div class="form-group clearfix"><label class="col-lg-4">Use Custom Fireworks sounds</label><div class="input-group col-lg-8"><label class="switch"><input type="checkbox" name="use_custom_fireworks_audio" '+use_custom_fireworks_audio+'><span class="slider round"></span></label></div></div>';
    senddata += '<div class="form-group select_custom_sounds clearfix " style="'+select_custom_sounds_css+' "><label class="col-lg-4">Select sounds</label><div class="input-group col-lg-8"><div '+fireworks_audio_show+'><span class="btn lobby_btn" onclick="$(this).parent(`div`).hide().next(`input`).show().attr(`required`,true);"> Change audio</span><audio controls="" style="display: inline-block; margin: 0 7px;"><source src="'+base_url+'upload/stream_setting/'+fireworks_audio+'"></audio></div><input type="file" name="fireworks_audio" accept="audio/mp3" class="form-control" '+fireworks_audio_upload_show+' onchange="PreviewAudio(this, $(this).hide().siblings(`div`).css(`display`,`flex`).css(`align-items`,`center`).find(`audio`))"></div></div>';
    senddata += '<div class="form-group default_sounds clearfix " style="'+default_sounds_css+' "><label class="col-lg-4">Default sounds</label><div class="input-group col-lg-8"><div style="display: flex; align-items: center;"><audio controls="" style="display: inline-block; margin: 0 7px;"><source src="'+base_url+'upload/stream_setting/'+default_fireworks_audio+'"></audio></div></div></div>';
    senddata += '<div class="form-group clearfix"><label class="col-lg-4">Winning Fireworks End Duration</label><div class="input-group col-lg-8"><div class="clearfix fireworks_duration_div"><input type="number" name="fireworks_duration_seconds" placeholder="Enter Seconds" class="form-control col-sm-4" required min="1" value="'+fireworks_duration_seconds+'"> <label class="col-sm-2"> Seconds </label></div></div></div>';
    if (gw_id != '' && gw_id != undefined) {
      senddata += '<input type="hidden" name="gw_id" value="'+gw_id+'">';
    } 
    senddata += '<div class="btn-group"><div class="col-lg-12 pull-right padd0"><button type="submit" value="Submit" class="btn-update lobby_btn giveaway_settings_submit">Submit</button></div></div>';
    senddata += '</form></div>';
    var modaltitle = 'Giveaway Settings <a class="btn-update lobby_btn pull-right reset_giveaway_entries" style="margin: 15px;">Reset Giveaway</a>';
    var add_class_in_modal_data = 'change_giveaway_settings';
    var data = {
      'add_class_in_modal_data':add_class_in_modal_data,
      'modal_title': modaltitle,
      'senddata':senddata,
      'tag':'commonmodalshow'
    }
    commonshow(data);
  });
  $('.delete_plr_cat').click(function () {
    var cat_id = $(this).attr('cat-id');
    if (cat_id.length) {
      var senddata = '<div class="col-sm-12"><h4 style="color: #ff5000;">';
      senddata += '</h4></div><div class="col-sm-12"><h3><a class="button yes" tg_id="'+cat_id+'">OK</a><a class="button no">Cancel</a></h3></div>';
      var modaltitle = 'Are you sure to delete this Category ?';
      var add_class_in_modal_data = 'delete_plrcat';
      var tgid = cat_id;
      var data = {
        'add_class_in_modal_data':add_class_in_modal_data,
        'modal_title': modaltitle,
        'senddata':senddata,
        'tgid' : tgid,
        'tag':'commonmodalshow'
      }
      commonshow(data);
    }    
  });  
}

// common edit button
$('.edit_data').click(function () {
  var data_id = $(this).attr('data-id');
  var tag = $(this).attr('tag');
  if (data_id.length) {
    switch(tag) {
      case 'product_fee_change_frntnd':
      var fee_type = $(this).attr('fee_type');
      var cal_type = $(this).attr('cal_type');
      var senddata = '<form action="'+base_url+'products/update_product_fee" method="post"><div class="col-sm-12 product_fee_modal"><div class="form-group">';
      senddata += '<h3 class="margin-tpbtm-10 clearfix text-left">'+fee_type+' Fee</h3><div class="margin-tpbtm-10 clearfix pr_fee_sel_div">';
      senddata += '<div class="padd10 clearfix"><lable class="col-sm-2 text-right">Type</lable><div class="col-sm-10"><select name="type" required class="form-control"><option value="1" '+((cal_type == 1) ? 'selected' : '')+'>Fix Amount</option><option value="2" '+((cal_type == 2) ? 'selected' : '')+'>Percentage</option></select></div></div>';
      senddata += '<div class="padd10 clearfix"><lable class="col-sm-2 text-right">Value</lable><div class="col-sm-10"><input type="number" name="fee_update" class="form-control" value="'+data_id+'" min="0" required></div></div>';
      senddata += '</div><input type="hidden" value="'+fee_type+'" name="fee_type">';
      senddata += '</div></div><div class="col-sm-12"><h3 class="text-left margin-tpbtm-10"><button class="cmn_btn" type="submit">Update</button></h3></div></form>';
      var modaltitle = 'Shipping tax';
      var add_class_in_modal_data = 'update_shipping_fee';
      var data = {
        'add_class_in_modal_data':add_class_in_modal_data,
        'modal_title': modaltitle,
        'senddata':senddata,
        'tag':'commonmodalshow'
      }
      commonshow(data);
      break;
      default:
    }
  }
});

//for taking image id and send the id to add game frontend
function a(va) {
  var htmlid = "img" + va;
  $(".img-cls").each(function () {
    $(this).css({ "width": "100", "height": "80" });
  });
  $("#" + htmlid).css({ "width": "80", "height": "80px" });
  $("#game_image_id").val(va);
}

var _URL = window.URL || window.webkitURL;
$("#uploadBtn").change(function (e) {
  var file, img;
  if ((file = this.files[0])) {
    img = new Image();
    img.onload = function () {
      $("#image_size").html(this.width + "-" + this.height);
    };
    img.src = _URL.createObjectURL(file);
  }
});
function image_size_validation() {
  var v = $("#image_size").html();
  if (v != '') {
    var r = v.split('-');
    if (r[0] <= 190 && r[1] <= 112) {
      return true;
    } else {
      alert('your image size ' + r[0] + 'px - ' + r[1] + 'px');
      return false;
    }
  }
}

function previewFile() {
  var preview = document.getElementById("k");
  var file = document.querySelector('input[type=file]').files[0];
  var reader = new FileReader();
  reader.addEventListener("load", function () {
    preview.src = reader.result;
  }, false);
  if (file) {
    reader.readAsDataURL(file);
  }
}

$('.web_display_name input[name="display_name_status"]').change(function(){
  if ($(this).is(":checked")) {
    $('.web_display_name input[name="display_name"]').attr('placeholder','Enter Display Name');
    $('.web_display_name input[name="display_name"]').removeAttr('readonly');
  } else {
    $('.web_display_name input[name="display_name"]').val('');
    $('.web_display_name input[name="display_name"]').removeAttr('placeholder');
    $('.web_display_name input[name="display_name"]').attr('readonly','true');
  }
});
$('.web_display_name input[name="gift_ticket_status"]').change(function(){

  if ($(this).is(":checked")) {   
    $('.web_display_name input[name="gift_ticket_account"]').attr('placeholder','Enter Account No');
    $('.web_display_name input[name="gift_ticket_account"]').attr('required','true');
    $('.web_display_name input[name="gift_ticket_account"]').removeAttr('readonly');
  } else {
    $('.web_display_name input[name="gift_ticket_account"]').val('');
    $('.web_display_name input[name="gift_ticket_account"]').removeAttr('placeholder');
    $('.web_display_name input[name="gift_ticket_account"]').removeAttr('required');
    $('.web_display_name input[name="gift_ticket_account"]').attr('readonly','true');
    
  }
});
$('form.prof_ed_form .btn-update').click(function(){
  var display_name = $('.web_display_name input[name="display_name"]').val();
  if ($('.web_display_name input[name="display_name_status"]').is(":checked")) {
    if (display_name == '') {
     $('.web_display_name input[name="display_name"]').css('border-color','red');
     $('.web_display_name input[name="display_name"]').focus();
     $('.input-group.web_display_name').append('<div id="errors" style="color: red;">Please enter display name</div>');
     return false;
   }
 } 
});

function CommaFormatted(amount) {
  var delimiter = ","; // replace comma if desired
  var a = amount.split('.',2)
  var d = a[1];
  var i = parseInt(a[0]);
  if(isNaN(i)) { return ''; }
  var minus = '';
  if(i < 0) { minus = '-'; }
  i = Math.abs(i);
  var n = new String(i);
  var a = [];
  while(n.length > 3) {
    var nn = n.substr(n.length-3);
    a.unshift(nn);
    n = n.substr(0,n.length-3);
  }
  if(n.length > 0) { a.unshift(n); }
  n = a.join(delimiter);
  if(d.length < 1) { amount = n; }
  else { amount = n + '.' + d; }
  amount = minus + amount;
  return amount;
}

function error_modal(error_msg, modal_title){
  var senddata = '<div class="col-sm-12"><h2 class="text-left">';
      // senddata += "Unfortunately you can't Trade all your funds creating a zero balance on your account";
      senddata += error_msg;
      senddata += '</h2></div><div class="col-sm-12"><h3><a class="button no">OK</a><a class="button no">Cancel</a></h3></div>';
      // var modaltitle = 'Insufficient Balance';
      var modaltitle = modal_title;
      var add_class_in_modal_data = 'low_balance_modal';
      var data = {
        'add_class_in_modal_data':add_class_in_modal_data,
        'modal_title': modaltitle,
        'senddata':senddata,
        'tag':'cmninfo_shw'
      }
      commonshow(data);
      return false;
    }
    $('#next_page').click(function(e) {
      e.preventDefault();
      var transfer_amt_final  = $('#transfer_amt').val();  
      var transfer_amt = parseFloat(parseFloat(transfer_amt_final).toFixed(2));
      var account_to    = $('#account_number').val(); 
      var current_user_account = $('#current_user_account').val();
      var current_user_balance = parseFloat($('#current_user_balance').val());
      if(isNaN(transfer_amt)){
        transfer_amt = '';
      }
      if(current_user_balance <= 0)
      {
        $('#transfer_amt').val('');
        var msg = "Unfortunately you can't Trade all your funds creating a zero balance on your account.";
        var title = "Insufficient Balance";
        return error_modal(msg, title);
      }
      $('.error_msg').html('');
      $('.error_msg_amt').html('');

      if (transfer_amt == '') {
    // $('#transfer_amt').focus();
    $('.error_msg_amt').html("Please enter amount.");
  }  else if(transfer_amt > current_user_balance) {
    $('#transfer_amt').val('');
    // $('#transfer_amt').focus();
    var msg = "You have Insufficient Balance.";
    var title = "Insufficient Balance";
    return error_modal(msg, title);
    // $('.error_msg_amt').html("You have Insufficient Balance.");
  } else if(account_to == '') {
    $('#account_number').focus();
    $('.error_msg').html('Please enter Friend account no.');
  } else if(account_to == current_user_account) {
   $('#account_number').focus();
   $('.error_msg').html('Please enter your valid friend account no.');
 } else {
  if(transfer_amt != '' && account_to != '' && current_user_balance > 0){
    $.ajax({
      type: "POST",  
      url: base_url + "sendmoney/get_payer_details",
      data: { account_to: account_to },
      success: function (result) {
        var data = JSON.parse(result);
        $('.first_phase').addClass('none');
        $('.second_phase').removeClass('none').addClass('block');
        $('.user_div').remove();
        $('.profile_image_div').html(data.msg);
        if(data.is_submit == 0){
          $('#point_add_id').parent('div').remove();
          $('#send_money_back_page').parent('div').removeClass('col-lg-6');
          $('#send_money_back_page').parent('div').addClass('col-lg-12');
          $('#send_money_back_page').removeClass('pull-right');
          $('#send_money_back_page').parent('div').css('text-align','center');
        }
      },
    });
  } 
}
})
    $('#send_money_back_page').click(function() {
      window.location = base_url + "sendmoney";
    })
    $('#point_add_id').click(function () {
      var transfer_amt  = $('#transfer_amt').val();
      var account_to    = $('#account_number').val();
      var account_from  = $('#account_from_number').val();

      if(transfer_amt != '' && account_to != '' ){
        var retVal = confirm('Are you Sure you want to transer  $'+transfer_amt+' from Acct#'+account_from+ ' to Acct#'+account_to+' ?');
        if( retVal == true ){
          $('#sendmoney_form').submit();
        } else {
          return false;
        }
      }

    })
    function chooseGameimage() {
      if ($('#chooseGameimage').length) {
        $('#chooseGameimage').bind('change', function(){
          var filename = $(this).val();
          if (/^\s*$/.test(filename)) {
            $(this).closest(".file-upload").removeClass('active');
            $(this).siblings(".file-select-name").html("No file chosen...");
          } else {
            $(this).closest(".file-upload").addClass('active');
            $(this).siblings(".file-select-name").text(filename.replace("C:\\fakepath\\", ""));
          }
        });
      }
    }
    $('.winning_result').click(function(e){
      e.preventDefault();
      var retVal = confirm('Are you Sure ?');
      if( retVal == true ){
        var $href = $(this).attr('href');
        window.location.href = $href;
      } else {
        return false;
      }
    })
    $('.lose_result').click(function(e){
      e.preventDefault();
      var retVal = confirm('Are you Sure ?');
      if( retVal == true ){
        var $href = $(this).attr('href');
        window.location.href = $href; 
      } else {
        return false;
      }
    })
    $('.admin_result').click(function(e){
      e.preventDefault();
      var retVal = confirm('Are you Sure ?');
      if( retVal == true ){
        var $href = $(this).attr('href');
        window.location.href = $href;
      } else {
        return false;
      }
    })
    $(document).on('click','.close',function(e){
      $('#myModal').hide();
    });
    youtube_link_reset();
    if ($('.bs_banner_customize .item.active').find('video').length) {
      $('.bs_banner_customize .item.active').find('video').get(0).play();
    }
    $('#myCarousel').carousel({pause: false,interval:$('.bs_banner_customize .item.active').attr('data-interval')});
    $('#myCarousel').bind('slid.bs.carousel', function (e) {
      youtube_link_reset();
      if ($('.bs_banner_customize .item.active video').length) {
        $('.bs_banner_customize .item.active video').each(function () {
          $(this).get(0).play();
        });
      }
      if ($('.bs_banner_customize .item video').length) {
        $('.bs_banner_customize .item video').not($('.bs_banner_customize .item.active video')).each(function () {
          $(this).load().get(0).pause();
        });
      }
    });
    function youtube_link_reset() {
      var src = $('.carousel-inner .item.active iframe').attr('src');
      if ($('.carousel-inner .item.active iframe[src*="autoplay=0&enablejsapi=1"]').length) {
        src = src.replace('autoplay=0&enablejsapi=1','autoplay=1&enablejsapi=1&start=0');
        $('.carousel-inner .item.active iframe').attr('src',src);
      }
      $('.carousel-inner .item iframe[src*="autoplay=1&enablejsapi=1"]').not('.carousel-inner .item.active iframe').each(function() {
        var src = $(this).attr('src');
        src = src.replace('autoplay=1', 'autoplay=0');
        $(this).attr('src',src);
      });
    }
    $(function (){
    //handle Bootstrap carousel slide event
    $('.carousel').carousel({pause: false,interval:$('.item.active').attr('data-interval')});
    $('.carousel').on("slide.bs.carousel", function (e){
      $('.carousel').carousel({
        pause: false,
            interval: 4000 //set the initial interval
          });
        //get the next interval from the data- HTML attribute
        var interval = parseInt($(e.relatedTarget).data("interval"));
        //set the interval by first getting a reference to the widget
        $('.carousel').data("bs.carousel").options.interval =  interval;
      });
  });
    function checkAccount(lobby_id) {
     var acc_no =$('#gift_ticket_account').val();
     var radioValue = $("input[name='is_event_flag']:checked").val();
     var button = document.getElementById('event_btn');
     var type = (radioValue == 0) ? 2 : 1 ;
     if(acc_no) {
      $.ajax({
        type: "POST",
        url: base_url + "livelobby/check_account",
        data: { acc_no: acc_no,lobby_id:lobby_id,type:type },
        success: function (result) { 
          var res = $.parseJSON(result);
          if(res.status == 1) {
            $('.second_phase').removeClass('none').addClass('block');
            $('.user_div').remove();
            $('.profile_image_div').html(res.user_data);
            $('#feedback').remove();
            $('#gift_to').val(res.user_id);  
            if(res.btn_flg == 1) {
              button.innerText = button.textContent = 'Confirm';
              document.getElementById("event_btn").disabled = false;
            }  else {         
              button.innerText = button.textContent = 'Already Gifted';
              document.getElementById("event_btn").disabled = true;
            }   
            $('.message').append('<span id="feedback" style="color:green;font-size:15px;margin-top:16px;">'+res.msg+'</span>');
          } else {
            $('#feedback').remove();
            $('#gift_to').val(0);
            $('.message').append('<span id="feedback" style="color:red;font-size:15px;margin-top:16px;">'+res.msg+'</span>');
            document.getElementById("event_btn").disabled = true;
          }
        },
      });
    }
  }


  $('#withdraw_modal_menu').on('click', function(){
    var senddata = '<div class="col-sm-12"><h2 class="text-left"> Would you like to order something from the store before withdrawal? </h2></div><div class="col-sm-12"><h3><a href = "'+base_url+'Store" class="button no">Yes</a><a href = "'+base_url+'points/withdrawal" class="button no">Proceed to Withdraw</a></h3></div>';
    var modaltitle = 'Wanna Shopping ?';
    var add_class_in_modal_data = 'withdraw_modal';
    var data = {
      'add_class_in_modal_data':add_class_in_modal_data,
      'modal_title': modaltitle,
      'senddata':senddata,
      'tag':'commonmodalshow'
    }
    if(window.location.href != base_url+'points/withdrawal'){
      commonshow(data);
    }
  })

  $('.creates_live_lobbies_lable, .joined_live_lobbies_lable, .friend_challanges_lable, .open_challanges_lable, .tournaments_lable, .live_events_lable, .gifted_events_by_lable, .gifted_events_to_lable, .money_sent_lable, .money_received_lable, .tip_sent_lable, .tip_received_lable, .pending_results_lable, .winning_games_lable, .lost_challanges_lable').on('click', function(){
    if($(this).attr("aria-expanded") === "false"){
      $(this).find('span i').removeClass('fa fa-caret-down');
      $(this).find('span i').addClass('fa fa-caret-up');
      $(this).parent('.mygamelist').addClass('collapse_margin');
      $(this).parent('.collapse_margin').children('div').addClass('collapse_border');
    }else{
      $(this).find('span i').removeClass('fa fa-caret-up');
      $(this).find('span i').addClass('fa fa-caret-down');
      $(this).parent('.collapse_margin').children('div').removeClass('collapse_border');
      $(this).parent('.mygamelist').removeClass('collapse_margin');
    }
  });
  // Event Registration Count down with days 
  countdown_with_day();
  var countdown_timeounteve = '';
  function countdown_with_day() {
    for (var i = 0; i < countdown_timeounteve; i++) {
      clearTimeout(i);
    }
    if ($('.countdown_with_day').length) {
      $('.countdown_with_day').each(function (i) {
        if ($(this).attr('id')) {
          var id = $(this).attr('id');
          var finish_action = ($(this).attr('finish_action') != undefined && $(this).attr('finish_action') != '') ? $(this).attr('finish_action') : '';
          $('.countdown_with_day#'+id).countdown({
            year: $(this).find('.year').html(), // YYYY Format
            month: $(this).find('.month').html(), // 1-12
            day: $(this).find('.day').html(), // 1-31
            hour: $(this).find('.hour').html(), // 24 hour format 0-23
            minute: $(this).find('.minute').html(), // 0-59
            second: $(this).find('.second').html(), // 0-59
            creator_time : $(this).attr('creator_time'),
            finish_action : finish_action,
            id : id,
          });
        }
      });
    }
  }
  $('#registration_status_tgl').on('change', function(){
    if($(this).is(':checked')){
      var status = 1;
    } else {
      var status = 0;
    }
    $('#reg_status').val(status);
    $('#status_change_form').submit();
  });

  // discord model
  $('.discord_btn').click(function () {
    var senddata = '<div class="text-game-model"><h3><a class="no cmn_btn" href="https://discord.me/ptg" target="_blank">Pro Esports Tournaments</a>     <a class="no cmn_btn" href="https://discord.me/peg" target="_blank">Pro Esports Gaming</a></h3></div>';
    var modaltitle = '';
    var add_class_in_modal_data = 'discord_btn_set';
    var data = {
       'modal_title': modaltitle,
      'add_class_in_modal_data':add_class_in_modal_data,
      'senddata':senddata,
      'tag':'confirm_modal'
    }
    commonshow(data);  
  });