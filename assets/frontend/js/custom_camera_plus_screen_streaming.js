var mixer;
var videoPreview = $('.screenshare_video video');
// var mixerOptions = $('.streamer_list_box .streamer_item input[type="checkbox"]');

// const peerconection = new RTCPeerConnection();
// $('.streamer_list_box .streamer_item input[type="checkbox"]').click(function () {
//     alert('testa');
// });
// mixerOptions.onchange = function() {
//     localStorage.setItem('mixer-selected-options', this.value);
//     location.reload();
// };
// if(localStorage.getItem('mixer-selected-options')) {
//     mixerOptions.value = localStorage.getItem('mixer-selected-options');
// }

// ......................................................
// .......................UI Code........................
// ......................................................


// document.getElementById('open-or-join-room').onclick = function() {
//     disableInputButtons();
//     connection.openOrJoin(document.getElementById('room-id').value, function(isRoomExist, roomid) {
//         if (isRoomExist === false && connection.isInitiator === true) {
//             // if room doesn't exist, it means that current user will create the room
//             showRoomURL(roomid);
//         }

//         if(isRoomExist) {
//           connection.sdpConstraints.mandatory = {
//               OfferToReceiveAudio: false,
//               OfferToReceiveVideo: true
//           };
//         }
//     });
// };  

// ......................................................
// ..................RTCMultiConnection Code.............
// ......................................................

var connection = new RTCMultiConnection();

// by default, socket.io server is assumed to be deployed on your own URL
console.log(location.hostname);
connection.socketURL = (location.hostname == 'localhost') ? 'http://localhost:11000/' : base_url.replace(/\/$/, '')+':11000/';

// comment-out below line if you do not have your own socket.io server
// connection.socketURL = 'https://rtcmulticonnection.herokuapp.com:443/';

connection.socketMessageEvent = 'screen-sharing-demo';

connection.session = {
    audio: true,
    screen: true,
    oneway: true
};
connection.mediaConstraints = {
    audio: true,
    video: false
};

connection.sdpConstraints.mandatory = {
    OfferToReceiveVideo: false,
    OfferToReceiveAudio: true
};

// https://www.rtcmulticonnection.org/docs/iceServers/
// use your own TURN-server here!
connection.iceServers = [{
    'urls': [
        'stun:stun.l.google.com:19302',
        'stun:stun1.l.google.com:19302',
        'stun:stun2.l.google.com:19302',
        'stun:stun.l.google.com:19302?transport=udp',
    ]
}];

connection.videosContainer = document.getElementById('videos-container');


// connection.onstream = function(event) {

//     var existing = document.getElementById(event.streamid);
//     if(existing && existing.parentNode) {
//       existing.parentNode.removeChild(existing);
//     }

//     event.mediaElement.removeAttribute('src');
//     event.mediaElement.removeAttribute('srcObject');
//     event.mediaElement.muted = true;
//     event.mediaElement.volume = 0;

//     var video = document.createElement('video');

//     try {
//         video.setAttributeNode(document.createAttribute('autoplay'));
//         video.setAttributeNode(document.createAttribute('playsinline'));
//     } catch (e) {
//         video.setAttribute('autoplay', true);
//         video.setAttribute('playsinline', true);
//     }
//     video.setAttribute('style','width:100%; height:auto;');
//     if(event.type === 'local') {
//       video.volume = 0;
//       try {
//           video.setAttributeNode(document.createAttribute('muted'));
//       } catch (e) {
//           video.setAttribute('muted', true);
//       }
//     }
//     video.srcObject = event.stream;

//     var width = '100%';
//     var mediaElement = getHTMLMediaElement(video, {
//         title: event.userid,
//         buttons: ['mute-audio', 'mute-video'],
//         width: width,
//         showOnMouseEnter: false
//     });

//     connection.videosContainer.appendChild(mediaElement);

//     setTimeout(function() {
//         mediaElement.media.play();
//     }, 5000);

//     mediaElement.id = event.streamid;
// };

// function stream_start(roomid) {
// }
connection.onstream = function(event) {

    // connection.videosContainer = document.getElementsByClassName('.text-left.streamer_channel');
    var existing = document.getElementById(event.streamid);
    if(existing && existing.parentNode) {
      existing.parentNode.removeChild(existing);
    }

    event.mediaElement.removeAttribute('src');
    event.mediaElement.removeAttribute('srcObject');
    // event.mediaElement.muted = false;
    event.mediaElement.style.position = 'absolute';
    // event.mediaElement.volume = 1;

    var video = document.createElement('video');
    try {
        video.setAttributeNode(document.createAttribute('autoplay'));
        video.setAttributeNode(document.createAttribute('playsinline'));
        video.setAttributeNode(document.createAttribute('controls'));
    } catch (e) {
        video.setAttribute('autoplay', true);
        video.setAttribute('playsinline', true);
    }
    video.setAttribute('style','width:100%; height:auto;');
    if(event.type === 'local') {
      // video.volume = 0;
      try {
          // video.setAttributeNode(document.createAttribute('muted'));
      } catch (e) {
          // video.setAttribute('muted', true);
      }
    }
    video.srcObject = event.stream;

    var width = '100%';
    var mediaElement = getHTMLMediaElement(video, {
        title: event.userid,
        buttons: ['mute-audio', 'mute-video', 'full-screen'],
        width: width,
        showOnMouseEnter: true
    });
    $('.text-left.streamer_channel').append(mediaElement);

    setTimeout(function() {
        mediaElement.media.play();
    }, 5000);

    mediaElement.id = event.streamid;
};

connection.onstreamended = function(event) {
    var mediaElement = document.getElementById(event.streamid);
    if (mediaElement) {
        mediaElement.parentNode.removeChild(mediaElement);

        if(event.userid === connection.sessionid && !connection.isInitiator) {
          alert('Broadcast is ended. We will reload this page to clear the cache.');
          location.reload();
        }
    }
};

connection.onMediaError = function(e) {
    if (e.message === 'Concurrent mic process limit.') {
        if (DetectRTC.audioInputDevices.length <= 1) {
            alert('Please select external microphone. Check github issue number 483.');
            return;
        }

        var secondaryMic = DetectRTC.audioInputDevices[1].deviceId;
        connection.mediaConstraints.audio = {
            deviceId: secondaryMic
        };

        connection.join(connection.sessionid);
    }
};

// ..................................
// ALL below scripts are redundant!!!
// ..................................

// function disableInputButtons() {
//     document.getElementById('open-or-join-room').disabled = true;
//     document.getElementById('room-id').disabled = true;
// }

// ......................................................
// ......................Handling Room-ID................
// ......................................................

// function showRoomURL(roomid) {
//     var roomHashURL = '#' + roomid;
//     var roomQueryStringURL = '?roomid=' + roomid;

//     var html = '<h2>Unique URL for your room:</h2><br>';

//     html += 'Hash URL: <a href="' + roomHashURL + '" target="_blank">' + roomHashURL + '</a>';
//     html += '<br>';
//     html += 'QueryString URL: <a href="' + roomQueryStringURL + '" target="_blank">' + roomQueryStringURL + '</a>';

//     var roomURLsDiv = document.getElementById('room-urls');
//     roomURLsDiv.innerHTML = html;

//     roomURLsDiv.style.display = 'block';
// }

(function() {
    var params = {},
        r = /([^&=]+)=?([^&]*)/g;

    function d(s) {
        return decodeURIComponent(s.replace(/\+/g, ' '));
    }
    var match, search = window.location.search;
    while (match = r.exec(search.substring(1)))
        params[d(match[1])] = d(match[2]);
    window.params = params;
})();

var roomid = '';
if (localStorage.getItem(connection.socketMessageEvent)) {
    roomid = localStorage.getItem(connection.socketMessageEvent);
} else {
    roomid = connection.token();
}
// document.getElementById('room-id').value = roomid;
// document.getElementById('room-id').onkeyup = function() {
//     localStorage.setItem(connection.socketMessageEvent, document.getElementById('room-id').value);
// };

var hashString = location.hash.replace('#', '');
if (hashString.length && hashString.indexOf('comment-') == 0) {
    hashString = '';
}

var roomid = params.roomid;
if (!roomid && hashString.length) {
    roomid = hashString;
}

if (roomid && roomid.length) {
    document.getElementById('room-id').value = roomid;
    localStorage.setItem(connection.socketMessageEvent, roomid);

    // auto-join-room
    (function reCheckRoomPresence() {
        connection.checkPresence(roomid, function(isRoomExist) {
            if (isRoomExist) {
                connection.join(roomid);
                return;
            }

            setTimeout(reCheckRoomPresence, 5000);
        });
    })();

    disableInputButtons();
}

// detect 2G
if(navigator.connection &&
   navigator.connection.type === 'cellular' &&
   navigator.connection.downlinkMax <= 0.115) {
  alert('2G is not supported. Please use a better internet service.');
}

function updateMediaHTML(html) {
    videoPreview.closest('.vid').siblings('h2').hrml(html);
}

// document.querySelector('.streamer_list_box .streamer_item input[type="checkbox"]').onclick = function() {
//     this.disabled = true;

//     if(mixerOptions.attr('stream_type') === 'camera-screen') {
//         // updateMediaHTML('Capturing screen');
//         getMixedCameraAndScreen();
//     }

//     if(mixerOptions.attr('stream_type') === 'multiple-cameras-default' || mixerOptions.attr('stream_type') === 'multiple-cameras-customized') {
//         // updateMediaHTML('Capturing camera');
//         getMixedMultipleCameras(mixerOptions.attr('stream_type') === 'multiple-cameras-customized');
//     }

//     if(mixerOptions.value === 'microphone-mp3') {
//         // updateMediaHTML('Capturing mp3+microphone');
//         getMixedMicrophoneAndMp3();
//     }
// };

function afterScreenCaptured(screenStream) {
    navigator.mediaDevices.getUserMedia({
        video: true,
        audio:true
    }).then(function(cameraStream) {
        screenStream.fullcanvas = true;
        screenStream.width = screen.width; // or 3840
        screenStream.height = screen.height; // or 2160 

        cameraStream.width = parseInt((30 / 100) * screenStream.width);
        cameraStream.height = parseInt((30 / 100) * screenStream.height);
        cameraStream.top = screenStream.height - cameraStream.height;
        cameraStream.left = screenStream.width - cameraStream.width;

        // fullCanvasRenderHandler(screenStream, 'Your Screen!');
        // normalVideoRenderHandler(cameraStream, 'Your Camera!');

        mixer = new MultiStreamsMixer([screenStream, cameraStream]);

        mixer.frameInterval = 1;
        mixer.startDrawingFrames();
        videoPreview.attr('src',URL.createObjectURL(mixer.getMixedStream())).show();
        
        // peerconection.addStream(mixer.getMixedStream());
        // peerconection.createOffer()
        // .then(sdp => peerconection.setLocalDescription(sdp))
        // .then(function() {
        //     var lobby_id = $('input[name="lobby_custom_name"]').val();
        //     var socket_data = {
        //         'lobby_id' : lobby_id,
        //         'srcdata' : peerconection.localDescription,
        //         'on' : 'send_realtime_stream',
        //     };
        //     socket.emit('sendrealtimedata', socket_data );
        // });
        // updateMediaHTML('Mixed Screen+Camera!');

        addStreamStopListener(screenStream, function() {
            mixer.releaseStreams();
            videoPreview.pause();
            videoPreview.attr('src',null);

            cameraStream.getTracks().forEach(function(track) {
                track.stop();
            });
        });
    });
}

function getMixedCameraAndScreen() {
    if(navigator.getDisplayMedia) {
        navigator.getDisplayMedia({video: true, audio: true}).then(screenStream => {
            afterScreenCaptured(screenStream);
        });
    }
    else if(navigator.mediaDevices.getDisplayMedia) {
        navigator.mediaDevices.getDisplayMedia({video: true, audio: true}).then(screenStream => {
            afterScreenCaptured(screenStream);
        });
    }
    else {
        alert('getDisplayMedia API is not supported by this browser.');
    }
}


// via: https://www.webrtc-experiment.com/webrtcpedia/
function addStreamStopListener(stream, callback) {
    stream.addEventListener('ended', function() {
        callback();
        callback = function() {};
    }, false);
    stream.addEventListener('inactive', function() {
        callback();
        callback = function() {};
    }, false);
    stream.getTracks().forEach(function(track) {
        track.addEventListener('ended', function() {
            callback();
            callback = function() {};
        }, false);
        track.addEventListener('inactive', function() {
            callback();
            callback = function() {};
        }, false);
    });
}

function fullCanvasRenderHandler(stream, textToDisplay) {
    // on-video-render:
    // called as soon as this video stream is drawn (painted or recorded) on canvas2d surface
    stream.onRender = function(context, x, y, width, height, idx) {
        context.font = '50px Georgia';
        var measuredTextWidth = parseInt(context.measureText(textToDisplay).width);
        x = x + (parseInt((width - measuredTextWidth)) - 40);
        y = y + 80;
        context.strokeStyle = 'rgb(255, 0, 0)';
        context.fillStyle = 'rgba(255, 255, 0, .5)';
        roundRect(context, x - 20, y - 55, measuredTextWidth + 40, 75, 20, true);
        var gradient = context.createLinearGradient(0, 0, width * 2, 0);
        gradient.addColorStop('0', 'magenta');
        gradient.addColorStop('0.5', 'blue');
        gradient.addColorStop('1.0', 'red');
        context.fillStyle = gradient;
        context.fillText(textToDisplay, x, y);
    };
}

function normalVideoRenderHandler(stream, textToDisplay, callback) {
    // on-video-render:
    // called as soon as this video stream is drawn (painted or recorded) on canvas2d surface
    stream.onRender = function(context, x, y, width, height, idx, ignoreCB) {
        if(!ignoreCB && callback) {
            callback(context, x, y, width, height, idx, textToDisplay);
            return;
        }

        context.font = '40px Georgia';
        var measuredTextWidth = parseInt(context.measureText(textToDisplay).width);
        x = x + (parseInt((width - measuredTextWidth)) / 2);
        y = (context.canvas.height - height) + 50;
        context.strokeStyle = 'rgb(255, 0, 0)';
        context.fillStyle = 'rgba(255, 255, 0, .5)';
        roundRect(context, x - 20, y - 35, measuredTextWidth + 40, 45, 20, true);
        var gradient = context.createLinearGradient(0, 0, width * 2, 0);
        gradient.addColorStop('0', 'magenta');
        gradient.addColorStop('0.5', 'blue');
        gradient.addColorStop('1.0', 'red');
        context.fillStyle = gradient;
        context.fillText(textToDisplay, x, y);
    };
}

/**
 * Draws a rounded rectangle using the current state of the canvas.
 * If you omit the last three params, it will draw a rectangle
 * outline with a 5 pixel border radius
 * @param {CanvasRenderingContext2D} ctx
 * @param {Number} x The top left x coordinate
 * @param {Number} y The top left y coordinate
 * @param {Number} width The width of the rectangle
 * @param {Number} height The height of the rectangle
 * @param {Number} [radius = 5] The corner radius; It can also be an object 
 *                 to specify different radii for corners
 * @param {Number} [radius.tl = 0] Top left
 * @param {Number} [radius.tr = 0] Top right
 * @param {Number} [radius.br = 0] Bottom right
 * @param {Number} [radius.bl = 0] Bottom left
 * @param {Boolean} [fill = false] Whether to fill the rectangle.
 * @param {Boolean} [stroke = true] Whether to stroke the rectangle.
 */
// via: http://stackoverflow.com/a/3368118/552182
function roundRect(ctx, x, y, width, height, radius, fill, stroke) {
    if (typeof stroke == 'undefined') {
        stroke = true;
    }
    if (typeof radius === 'undefined') {
        radius = 5;
    }
    if (typeof radius === 'number') {
        radius = {
            tl: radius,
            tr: radius,
            br: radius,
            bl: radius
        };
    } else {
        var defaultRadius = {
            tl: 0,
            tr: 0,
            br: 0,
            bl: 0
        };
        for (var side in defaultRadius) {
            radius[side] = radius[side] || defaultRadius[side];
        }
    }
    ctx.beginPath();
    ctx.moveTo(x + radius.tl, y);
    ctx.lineTo(x + width - radius.tr, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius.tr);
    ctx.lineTo(x + width, y + height - radius.br);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius.br, y + height);
    ctx.lineTo(x + radius.bl, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius.bl);
    ctx.lineTo(x, y + radius.tl);
    ctx.quadraticCurveTo(x, y, x + radius.tl, y);
    ctx.closePath();
    if (fill) {
        ctx.fill();
    }
    if (stroke) {
        ctx.stroke();
    }
}
if (typeof socket != 'undefined') {
    socket.on('getrealtimedata',function(getdata){
        var lobby_id = getdata.data.lobby_id;
        var winlocation = window.location.href.split('#')[0].toLowerCase();
        var getloc = (base_url+'livelobby/'+lobby_id).toLowerCase();
        if (getloc == winlocation) {
            if (getdata.data.on == 'send_realtime_stream') {
                var videoPreview = $('.screenshare_video video');
                videoPreview.attr('src',URL.createObjectURL(getdata.data.srcdata)).show();               
            }
        }
    });
}