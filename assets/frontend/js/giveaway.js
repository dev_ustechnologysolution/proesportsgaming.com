(function ($) {
	'use strict';
	$(document).keydown(function(event) { (event.keyCode == 27) ? $('.close').trigger('click') : ''; });
	var timout, intrvl;
	commongiveawayfunction();
	function commongiveawayfunction(args = '') {
		$('.giveaway_msg_bx .attachfile input[name="attachfile"], .send-message-to-cmn_group').unbind('click dblclick change keyup keypress keydown');
		$('.join_giveaway_btn').click(function () {
			var total_balance = $('#total_balance').val(), giveaway_amount = $('#giveaway_amount').val(), remaining_items = $('input[name="remaining_items"]').val();
			if (parseFloat(giveaway_amount) <= parseFloat(total_balance) && remaining_items > 0) {
				var senddata = '<div class="col-sm-12">';
				senddata += '<div class="">';
				senddata += '<h3 class="text-left" style="color: #ff5000;"> Please Enter Your Player Tag</h3>';
				senddata += '<div class="modal_div_border">';
	      senddata += '<div class="form-group"><div class="input-group col-lg-12"><input type="text" class="form-control" name="giveaway_tag" placeholder="Giveaway ID"></div></div>';
	      senddata += '<div class="form-group giveaway_error" style="display: none;"><div class="input-group col-lg-12"><div class="alert alert-danger"></div></div></div>';
	      senddata += '</div>';
	      senddata += '</div>';
	      senddata += '</div><div class="col-sm-12"><h3><a class="button yes confirm_giveaway">Submit</a><a class="button no">Cancel</a></h3></div>';
				var modaltitle = 'Join Giveaway';
	      var add_class_in_modal_data = 'join_giveaway_modal';
	      var tgid = '';
	      var data = {
	        'add_class_in_modal_data':add_class_in_modal_data,
	        'modal_title': modaltitle,
	        'senddata' : senddata,
	        'tgid' : tgid,
	        'tag' : 'commonmodalshow'
	      }
	    } else if (parseFloat(giveaway_amount) <= parseFloat(total_balance) && remaining_items == 0) {
	    	var senddata = '<div class="col-sm-12"><h4 class="text-left"> This Giveaway is full. </h4></div>';
				senddata += '<div class="col-sm-12"><h3><a class="button no"> OK </a><a class="button no">Cancel</a></h3></div>';
				var modaltitle = 'Giveaway Full';
				var add_class_in_modal_data = 'giveaway_full';
		    var data = {
		      'add_class_in_modal_data' : add_class_in_modal_data,
		      'modal_title': modaltitle,
		      'senddata' : senddata,
		      'tag' : 'commonmodalshow'
		    }
			} else {
				var senddata = '<div class="col-sm-12"><h4 class="text-left"> You dont have Enough Credits to Join Giveaway. Please Proceed to Purchase tab for more </h4></div>';
				senddata += '<div class="col-sm-12"><h3><a class="button yes" href="'+base_url+'points/buypoints">Proceed to Purchase</a><a class="button no">Cancel</a></h3></div>';
				var modaltitle = 'Insufficient Balance';
				var add_class_in_modal_data = 'giveaway_start_shuffle';
		    var data = {
		      'add_class_in_modal_data' : add_class_in_modal_data,
		      'modal_title': modaltitle,
		      'senddata' : senddata,
		      'tag' : 'commonmodalshow'
		    }
			}
      commongashow(data);
		});
		$('.get_another_ticket_ga_btn').click(function () {
			var max_entries = $('input[name="remaining_items"]').val();
			if (max_entries > 0) {
				var senddata = '<div class="col-sm-12">';
				senddata += '<div class="">';
				senddata += '<h3 class="text-left" style="color: #ff5000;"> How many Entry you want ? </h3>';
				senddata += '<div class="modal_div_border">';
	      senddata += '<div class="form-group row"><div class="col-sm-12 text-left"><div class="number_opr_div"><button type="button" id="sub" class="sub cmn_btn"><i class="fa fa-minus"></i></button><input type="number" name="number_toopr" class="form-control number_toopr" min="1" max="'+max_entries+'" pattern="[0-9]+" required="" readonly="" value="1"><button type="button" id="add" class="add cmn_btn"><i class="fa fa-plus"></i></button></div></div>';
	      senddata += '</div></div>';
	      senddata += '</div>';
	      senddata += '</div><div class="col-sm-12"><h3><a class="button yes confirm_giveaway">Submit</a><a class="button no">Cancel</a></h3></div>';
	      var modaltitle = 'Get Another Entries';
	      var add_class_in_modal_data = 'ga_buy_more_entries';
	      var tgid = '';
	      var data = {
	        'add_class_in_modal_data':add_class_in_modal_data,
	        'modal_title': modaltitle,
	        'senddata' : senddata,
	        'tgid' : tgid,
	        'tag' : 'commonmodalshow'
	      }
	      commongashow(data);
	    } else {
	    	var senddata = '<div class="col-sm-12"><h4 class="text-left"> This Giveaway is full. </h4></div>';
				senddata += '<div class="col-sm-12"><h3><a class="button no"> OK </a><a class="button no">Cancel</a></h3></div>';
				var modaltitle = 'Giveaway Full';
				var add_class_in_modal_data = 'giveaway_full';
		    var data = {
		      'add_class_in_modal_data' : add_class_in_modal_data,
		      'modal_title': modaltitle,
		      'senddata' : senddata,
		      'tag' : 'commonmodalshow'
		    }
		    commongashow(data);
	    }
		});
		$('.ga_ready_div .cemara_action.enable').click(function () {
			var default_stream_name = $(this).attr('default_stream_name');
			var changed_status = $(this).attr('stream_click_status');
			var clickclass = (default_stream_name != '') ? 'change_giveway_stream_status' : 'change_giveway_stream_enable';
			var senddata = '<div class="col-sm-12">';
			senddata += (changed_status == 'enable' && default_stream_name != '') ? '<div class="modal_div_border" style="margin: 10px auto 0;"><div class="form-group edit_add_ga_stream_status"><div class="input-group col-lg-12"><input type="text" class="form-control" name="ga_default_stream" placeholder="Enter your channel" value="'+default_stream_name+'"></div></div><div class="form-group ga_default_stream_error" style="display: none;"><div class="input-group col-lg-12"><div class="alert alert-danger"></div></div></div></div>' : '';
			senddata += '<h3><a class="button yes '+clickclass+'" '+((changed_status == 'enable' && default_stream_name != '') ? 'new_stream="true"' : '')+' change_status="'+changed_status+'" default_stream_name="'+default_stream_name+'">OK</a><a class="button no">Cancel</a></h3>';
			senddata += '</div>';
			var modaltitle = (changed_status != 'enable') ? '<h3 class="clearfix text-center">Are you sure to Disable Stream ?</h3>' : '<h3 class="clearfix text-center">Enter Twitch ID to Enable Stream ?</h3>';
			var add_class_in_modal_data = 'change_ga_stream_status';
	    var data = {
	      'add_class_in_modal_data' : add_class_in_modal_data,
	      'modal_title': modaltitle,
	      'senddata' : senddata,
	      'tag' : 'confirm_modal'
	    }
	    commongashow(data);
			return false;
		});
		$('.shuffle_confirmation').click(function() {
			var action = $(this).attr('action');
			var shuffle_action = (action == 'start') ? 'start' : 'stop';
			var senddata = '<div class="col-sm-12"><h3><a class="button yes '+shuffle_action+'_shuffle" action="'+shuffle_action+'">OK</a><a class="button no">Cancel</a></h3></div>';
			var modaltitle = 'Are you sure to '+shuffle_action+' Giveaway ?';
			var add_class_in_modal_data = 'giveaway_'+shuffle_action+'_shuffle';
			var data = {
	      'add_class_in_modal_data' : add_class_in_modal_data,
	      'modal_title': modaltitle,
	      'senddata' : senddata,
	      'tag' : 'confirm_modal'
	    }
	    commongashow(data);
	  });
		$('.editga_tg_btn').click(function () {
			var ga_plyrtag = $('#ga_plyrtag').val();
			var senddata = '<div class="col-sm-12">';
			senddata += '<div class="">';
			senddata += '<div class="modal_div_border">';
      senddata += '<div class="form-group"><div class="input-group col-lg-12"><input type="text" class="form-control" name="giveaway_tag" placeholder="Giveaway ID" value="'+ga_plyrtag+'"></div></div>';
      senddata += '<div class="form-group giveaway_error" style="display: none;"><div class="input-group col-lg-12"><div class="alert alert-danger"></div></div></div>';
      senddata += '</div></div>';
      senddata += '</div>';
      senddata += '</div><div class="col-sm-12"><h3><a class="button yes edit_plyertag_submit">Update</a><a class="button no">Cancel</a></h3></div>';
      var modaltitle = 'Edit Giveaway tag';
      var add_class_in_modal_data = 'ga_edit_playertag';
      var tgid = '';
      var data = {
        'add_class_in_modal_data':add_class_in_modal_data,
        'modal_title': modaltitle,
        'senddata' : senddata,
        'tgid' : tgid,
        'tag' : 'commonmodalshow'
      }
      commongashow(data);
		});
		$('.send-message-to-giveaway_group').click(function() {
			var imgs = $('.attachfile_div .attachfile input[type="file"]').prop('files'),
				message = $('.cmn_grp_msgbox_message-input .emojionearea-editor').html(),
				lobby_id = $('input[name="lobby_id"]').val(),
				user_id = $('input[name="user_id"]').val(),
				giveaway_id = $('input[name="giveaway_id"]').val(),
				is_admin = $('input[name="is_admin"]').val(),
				fan_tag = $('input[name="fan_tag"]').val();

		    message = message.replace(/<div><br><\/div>/g,'');
		    message = message.replace(/<br>/g,'')
		    message = message.replace(/<div><\/div>/g,'');
	    	var arr = {
				'lobby_id' : lobby_id,
				'giveaway_id' : giveaway_id,
				'message' : message,
				'user_id' : user_id,
				'is_admin' : is_admin,
				'fan_tag' : fan_tag,
			};
	    if (message.length != 0) {
				send_message_to_giveaway_grp(arr);
			} else {
				$('.cmn_grp_msgbox_message-input .emojionearea-editor').html('');
			}
		});
		$('.giveaway_msg_bx .attachfile input[name="attachfile"]').change(function() {
			var filename = [];
			var images = '';
			var imgs = $('.attachfile_div .attachfile input[type="file"]').prop('files'),
			message = $('.cmn_grp_msgbox_message-input .emojionearea-editor').html(),
			lobby_id = $('input[name="lobby_id"]').val(),
			user_id = $('input[name="user_id"]').val(),
			giveaway_id = $('input[name="giveaway_id"]').val(),
			is_admin = $('input[name="is_admin"]').val(),
			fan_tag = $('input[name="fan_tag"]').val();
			var formData = new FormData();
			for (var i = imgs.length - 1; i >= 0; i--) {
				formData.append( 'file[]', imgs[i]);
			}
			formData.append('lobby_id',lobby_id);
			formData.append('user_id',user_id);
			formData.append('giveaway_id',giveaway_id);
			var arr = {
				'lobby_id' : lobby_id,
				'giveaway_id' : giveaway_id,
				'message' : message,
				'user_id' : user_id,
				'is_admin' : is_admin,
				'fan_tag' : fan_tag,
				'formData' : formData,
			};
			send_message_to_giveaway_grp(arr);
		});
	}
	function commongashow(data) {
		// .send-message-to-giveaway_group, .cmn_grp_msgbox_message-input .emojionearea-editor
		if (data.tag == 'commonmodalshow') {
			$('.modal_h1_msg').removeClass().addClass('modal_h1_msg text-left '+data.add_class_in_modal_data+'_div').html(data.modal_title);
			$('.modal.commonmodal').removeClass().addClass('modal custmodal commonmodal '+data.add_class_in_modal_data);
			$('.modal.commonmodal .modal_data').html(data.senddata);
			$('.modal.commonmodal').show();
		}
		if (data.tag == 'confirm_modal') {
			$('.modal.commonmodal hr').hide();
			$('.modal_h1_msg').removeClass().addClass('modal_h1_msg text-left '+data.add_class_in_modal_data+'_div').html(data.modal_title);
			$('.modal.commonmodal').removeClass().addClass('modal custmodal commonmodal '+data.add_class_in_modal_data);
			$('.modal.commonmodal .modal_data').html(data.senddata);
			$('.modal.commonmodal').show();
		}
		if (data.tag == 'send_socket_msg_ga_grp') {
			var is_admin = $('input[name="is_admin"]').val(),
				user_id = $('input[name="user_id"]').val(),
				lobby_custom_name = $('input[name="lobby_custom_name"]').val(),
				lobby_id = $('input[name="lobby_id"]').val(),
				giveaway_id = $('input[name="giveaway_id"]').val();

			var getmytip_sound = (data.getmytip_sound != undefined && data.getmytip_sound != '') ? data.getmytip_sound : '';
			var socket_data = {
				'msg_data': data.msg_data,
				'lobby_id' : lobby_id,
				'giveaway_id' : giveaway_id,
				'name': data.name,
				'session_id': user_id,
				'getmytip_sound': getmytip_sound,
				'is_admin': is_admin,
				'event' : 'giveaway',
				'on': 'send_message_in_giveaway_grp',
			};
			socket.emit('sendrealtimedata', socket_data);
		}		
		commongaappend();
	}
	function commongaappend() {
		var lobby_id = $('#lobby_id').val();
		$('.change_giveway_stream_enable').click(function() {
			var senddata = '<div class="col-sm-12">';
			senddata += '<div class="modal_div_border">';
			senddata += '<div class="form-group"><div class="input-group col-lg-12"><input type="text" class="form-control" name="ga_default_stream" placeholder="Enter your channel"></div></div>';
      senddata += '<div class="form-group ga_default_stream_error" style="display: none;"><div class="input-group col-lg-12"><div class="alert alert-danger"></div></div></div>';
      senddata += '</div></div>';
      senddata += '<div class="col-sm-12"><h3><a class="button yes change_giveway_stream_status" new_stream="true" change_status="enable">OK</a><a class="button no">Cancel</a></h3></div>';
			var modaltitle = '<h2 class="clearfix text-left paddbot10">Please Enter Your Twitch Username Login ?</h2>';
			var add_class_in_modal_data = 'add_ga_stream_status';
	    var data = {
	      'add_class_in_modal_data' : add_class_in_modal_data,
	      'modal_title': modaltitle,
	      'senddata' : senddata,
	      'tag' : 'confirm_modal'
	    }
	    commongashow(data);
			return false;
		});
		$('input[name="ga_default_stream"]').keypress(function() {
			if (event.which == 13) {
				event.preventDefault();
				$('.change_giveway_stream_status').trigger('click');
			}	
		});
		$('.change_giveway_stream_status').click(function() {
			var new_stream = ($(this).attr('new_stream') != undefined) ? $(this).attr('new_stream') : '';
			var ga_default_stream = '';
			var change_status = $(this).attr('change_status');
			if (new_stream != undefined && new_stream != '') {
				$('.ga_default_stream_error').hide().find('.alert').html('');
				ga_default_stream = $('input[name="ga_default_stream"]').val();
				if (ga_default_stream == '') {
					$('.ga_default_stream_error').show().find('.alert').html('Please Enter Channel');
					return false;
				}
			}
			$('.modal').hide();
			if (change_status != '') {
				var giveaway_id = $('#giveaway_id').val();
				var postData = {
					'change_status': change_status,
	      	'giveaway_id' : giveaway_id,
	      	'lobby_id' : lobby_id,
	      	'ga_default_stream' : ga_default_stream
				};
				$.ajax({
					url: base_url+'giveaway/change_ga_stream_status',
					type: "post",
					data: postData,
					beforeSend: function() { loader_show(); } ,
					success: function(result) {
						var result = $.parseJSON(result);
						loader_hide();
						if (result.updated != undefined && result.updated != '') {
							if (result.update_data.default_stream_showstatus == 0) {
								$('.ga-cemara_title_div').text('Enable Your Stream');
								$('.cemara_action').attr({stream_click_status:'enable'}).find('img').attr({'src':base_url+'assets/frontend/images/ga_live_stream_off.png'});
							} else {
								$('.ga-cemara_title_div').text('Disable Your Stream');
								$('.cemara_action').attr({stream_click_status:'disable', default_stream_name : result.update_data.default_stream}).find('img').attr({'src':base_url+'assets/frontend/images/ga_live_stream_on.png'});
							}
							var socket_data = {
								'lobby_id' : result.giveaway_data.lobby_id,
								'giveaway_id' : result.giveaway_data.id,
								'default_stream' : result.giveaway_data.default_stream,
								'default_stream_showstatus' : result.giveaway_data.default_stream_showstatus,
								'event' : 'giveaway',
								'on' : 'ga_stream_action'
							};
							socket.emit('sendrealtimedata', socket_data);
						}
					},
				});
			}
		});
		$('.ga_edit_playertag input[name="giveaway_tag"]').keypress(function(event) {
			if (event.which == 13) {
				event.preventDefault();
				$('.ga_edit_playertag .edit_plyertag_submit').trigger('click');
			}	
		})
		$('.ga_edit_playertag .edit_plyertag_submit').click(function() {
			var giveaway_id = $('#giveaway_id').val(), lobby_id = $('#lobby_id').val(), giveaway_tag = $('.ga_edit_playertag input[name="giveaway_tag"]').val();
			$('.ga_edit_playertag .giveaway_error').hide().find('.alert').text('');
			if (giveaway_tag == '') {
				$('.ga_edit_playertag .giveaway_error').show().find('.alert').text('Please Enter Giveaway tag');
				return false;
			}
			var postData = {
      	'giveaway_id' : giveaway_id,
      	'lobby_id' : lobby_id,
      	'giveaway_tag' : giveaway_tag
			};
			$.ajax({
				url: base_url+'giveaway/edit_giveaway_tag',
				type: "post",
				data: postData,
				beforeSend: function() { loader_show(); } ,
				success: function(result) {
					var result = $.parseJSON(result);
					loader_hide();
					if (result.updated == true) {
						var socket_data = {
							'lobby_id' : result.updated_where.lobby_id,
							'giveaway_id' : result.updated_where.giveaway_id,
							'user_id' : result.updated_where.user_id,
							'event' : 'giveaway',
							'on' : 'page_refresh'
						};
						socket.emit('sendrealtimedata', socket_data);
					}
				}
			});
		})
		$('.join_giveaway_modal input[name="giveaway_tag"]').on('keypress', function(event) {
			if (event.which == 13) {
				event.preventDefault();
				$('.join_giveaway_modal .confirm_giveaway').trigger('click');
			}
		});

		$('.procced_to_join .submit_giveaway').click(function() {
			var giveaway_amount = $('#giveaway_amount').val(), giveaway_id = $('#giveaway_id').val(), lobby_id = $('#lobby_id').val(), giveaway_tag = $('.procced_to_join input[name="giveaway_tag"]').val();
			var postData = {
				'giveaway_amount': giveaway_amount,
      	'giveaway_id' : giveaway_id,
      	'lobby_id' : lobby_id,
      	'giveaway_tag' : giveaway_tag
			};
			$.ajax({
				url: base_url+'giveaway/join_giveaway',
				type: "post",
				data: postData,
				beforeSend: function() { loader_show(); } ,
				success: function(result) {
					var result = $.parseJSON(result);
					loader_hide();
					if (result.inserted != undefined && result.inserted != '' && result.inserted != false) {
						var user_id = $('#user_id').val();
						$('.procced_to_join').hide();
						$('.join_giveaway_btn').html('Joined <i class="fa fa-check"></i>').removeClass('join_giveaway_btn').addClass('already_joined');
						// var left_length = $('.ga_tables.left .ga_users_div').length, right_length = $('.ga_tables.right .ga_users_div').length;
						// var tg_tbl = (left_length == right_length || left_length < right_length) ? 'left' : 'right';
						var random_color = '#'+ ('000000' + Math.floor(Math.random()*16777215).toString(16)).slice(-6);
						var giveaway_new_user = '<div class="left_table text-left ga_users_div" user_id="'+user_id+'">';
						giveaway_new_user += '<a href="'+base_url+'user/user_profile/?fans_id='+user_id+'" class="ga_users" target="_blank" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="View Profile" style="background-color:'+random_color+';">';
						giveaway_new_user += '<div class="profile_img_div"><img src="'+base_url+'upload/profile_img/'+result.get_img+'" class="img img-responsive" alt="smile"></div>';
            giveaway_new_user += '<span>'+result.insert_data.giveaway_tag+'</span>';
            giveaway_new_user += '</a></div>';

						$('.tot-balance').html('$'+result.updated_balance);
						(result.giveaway_data[0].is_multiple_entries == 1) ? $('.getanother_ticket_div').html('<lable class="get_another_ticket_ga_btn cmn_btn">Get Another Entry</lable>') : '';
						$('.edit_ga_tag_div').show();
						$('#ga_plyrtag').val(giveaway_tag);
						
						var socket_data = {
							'lobby_id' : lobby_id,
							'giveaway_new_user' : giveaway_new_user,
							'ga_creator' : result.lobby_data[0].user_id,
							'ga_fee_clctor_arr' : result.ga_fee_clctor_arr,
							'ga_amnt_clctor_arr' : result.ga_amnt_clctor_arr,
							'tg_tbl': 'left',
							'event' : 'giveaway',
							'on' : 'join_giveaway_data'
						};
						socket.emit('sendrealtimedata', socket_data);
					} else {
						window.location.reload();
					}
				},
			});
		});
		$('.procced_to_buy_multi_entries .submit_entries').click(function() {
			var giveaway_amount = $('#giveaway_amount').val(), giveaway_id = $('#giveaway_id').val(), lobby_id = $('#lobby_id').val(), another_entries = $('.procced_to_buy_multi_entries input[name="another_entries"]').val();
			var postData = {
				'another_entries' : another_entries,
				'giveaway_amount': giveaway_amount,
      	'giveaway_id' : giveaway_id,
      	'lobby_id' : lobby_id,
			};
			$.ajax({
				url: base_url+'giveaway/join_giveaway',
				type: "post",
				data: postData,
				beforeSend: function() { loader_show(); } ,
				success: function(result) {
					var result = $.parseJSON(result);
					loader_hide();
					if (result.inserted != undefined && result.inserted != '' && result.inserted != false) {
						var user_id = $('#user_id').val();
						$('.procced_to_buy_multi_entries').hide();
						$('.tot-balance').html('$'+result.updated_balance);
						var socket_data = {
							'lobby_id' : lobby_id,
							'user_id' : user_id,
							'ga_creator' : result.lobby_data[0].user_id,
							'another_entries' : result.another_entries,
							'ga_fee_clctor_arr' : result.ga_fee_clctor_arr,
							'ga_amnt_clctor_arr' : result.ga_amnt_clctor_arr,
							'event' : 'giveaway',
							'on' : 'another_entries_giveaway_data'
						};
						socket.emit('sendrealtimedata', socket_data);
					} else {
						window.location.reload();
					}
				},
			});
		});
		$('.join_giveaway_modal .confirm_giveaway').click(function() {
			var giveaway_amount = $('#giveaway_amount').val(), giveaway_cal_method = $('input[name="global_gafee_calc_method"]').val(), giveaway_fee = $('input[name="global_gafee"]').val();
			//var ga_fee = (giveaway_cal_method == 1) ? parseFloat(giveaway_fee).toFixed(2) : parseFloat(giveaway_amount*giveaway_fee/100).toFixed(2);
			var giveaway_tag = $('.join_giveaway_modal input[name="giveaway_tag"]').val();
			$('.join_giveaway_modal .giveaway_error').hide().find('.alert').text('');
			if (giveaway_tag == '') {
				$('.join_giveaway_modal .giveaway_error').show().find('.alert').text('Please Enter Giveaway tag');
				return false;
			}
			var senddata = '<div class="col-sm-12"><h4 class="text-left">$'+giveaway_amount+' will be deducted from your wallet.</h4><input type="hidden" name="giveaway_tag" value="'+giveaway_tag+'"></div>';
			senddata += '<div class="col-sm-12"><h3><a class="button yes cnfrm_join">OK</a><a class="button no">Cancel</a></h3></div>';
			var modaltitle = 'Are you sure you want to join ?';
			var add_class_in_modal_data = 'sure_to_join_modal';
	    var data = {
	      'add_class_in_modal_data' : add_class_in_modal_data,
	      'modal_title': modaltitle,
	      'senddata' : senddata,
	      'tag' : 'commonmodalshow'
	    }
	    commongashow(data);
			return false;
		});
		$('.sure_to_join_modal .cnfrm_join').click(function() {
			var giveaway_amount = $('#giveaway_amount').val(), giveaway_cal_method = $('input[name="global_gafee_calc_method"]').val(), giveaway_fee = $('input[name="global_gafee"]').val();
			//var ga_fee = (giveaway_cal_method == 1) ? parseFloat(giveaway_fee).toFixed(2) : parseFloat(giveaway_amount*giveaway_fee/100).toFixed(2);
			var giveaway_tag = $('.sure_to_join_modal input[name="giveaway_tag"]').val();
			var senddata = '<div class="col-sm-12"><input type="hidden" name="giveaway_tag" value="'+giveaway_tag+'"><h3><a class="button yes submit_giveaway">Accept</a><a class="button no">Decline</a></h3></div>';
			var modaltitle = '<h1 class="clearfix text-center">All Entries Are Final !!</h1>';
			var add_class_in_modal_data = 'procced_to_join';
	    var data = {
	      'add_class_in_modal_data' : add_class_in_modal_data,
	      'modal_title': modaltitle,
	      'senddata' : senddata,
	      'tag' : 'confirm_modal'
	    }
	    commongashow(data);
			return false;
		});
		$('.ga_buy_more_entries .confirm_giveaway').click(function() {
			var giveaway_amount = $('#giveaway_amount').val(), giveaway_cal_method = $('input[name="global_gafee_calc_method"]').val(), giveaway_fee = $('input[name="global_gafee"]').val();
			var entries = $('.ga_buy_more_entries .number_toopr').val();
			var total_giveaway_amount = parseFloat(giveaway_amount * entries).toFixed(2);
			// var ga_fee = (giveaway_cal_method == 1) ? parseFloat(giveaway_fee * entries).toFixed(2) : parseFloat(entries * (giveaway_amount*giveaway_fee/100)).toFixed(2);
			var giveaway_tag = $('.join_giveaway_modal input[name="giveaway_tag"]').val();

			var senddata = '<div class="col-sm-12"><h4 class="text-left">$'+total_giveaway_amount+' will be deducted from your wallet.</h4></div>';
			senddata += '<input type="hidden" name="another_entries" value="'+entries+'">';
			senddata += '<div class="col-sm-12"><h3><a class="button yes submit_entries">OK</a><a class="button no">Cancel</a></h3></div>';
			var modaltitle = 'Are you sure you want to join ?';
			var add_class_in_modal_data = 'procced_to_buy_multi_entries';
		  var data = {
		    'add_class_in_modal_data' : add_class_in_modal_data,
		    'modal_title': modaltitle,
		    'senddata' : senddata,
		    'tag' : 'commonmodalshow'
		  }
		  commongashow(data);
			return false;
		});
		$('.start_shuffle, .stop_shuffle').click(function() {
			$('.giveaway_start_shuffle, .giveaway_stop_shuffle').hide();
			var action = $(this).attr('action');
			var random_winner = Math.floor(Math.random() * $(".ga_tables .ga_users_div .ga_users").length);
			var fireworks_duration_time = $('#fireworks_duration_time').val();
			var socket_data = {
				'lobby_id' : lobby_id,
				'random_winner' : random_winner,
				'fireworks_duration_time' : fireworks_duration_time,
				'event' : 'giveaway',
				'action' : action,
				'on':'realtime_shuffle'
			};
			socket.emit('sendrealtimedata', socket_data);
			if (action == 'start') {
				$('.modal').css('z-index',9999);
				$('.start_button').css('z-index',9999).find('.shuffle_confirmation').attr('action','stop').text('Stop');
			}
	  });
		$('.number_opr_div .sub, .number_opr_div .add').click(function () {
			if (this.className == 'add cmn_btn') {
				$(this).prev().val(+$(this).prev().val() + (($(this).prev().attr('max') == $(this).prev().val()) ? 0: 1));
			} else if (this.className == 'sub cmn_btn') {
				($(this).next().val() > 1) ? $(this).next().val(+$(this).next().val() - 1) : '';
			}
		});
		$('.modal.custmodal .close, .modal.custmodal .no').click(function () {
			$('.modal.custmodal').hide();
		});
	}
	function giveaway_shuffle(random_winner = '') {
		if (random_winner != '') {
			var random = random_winner.replace(/\'/g, "");
			$(".ga_tables .ga_users_div .ga_users").removeClass('white_shuffle_ga').eq(random).addClass('white_shuffle_ga');
		} else {
			// var ga_userlist = $(".ga_tables .ga_users_div .ga_users"), i;
			// // var n = Math.floor(ga_userlist.length / 2);
			// // var n = Math.floor(ga_userlist.length*95/100);
			// var n = Math.floor(2);
			// // $(".ga_tables .ga_users_div .ga_users").removeClass('white_shuffle_ga');
			// var pastele, curele, pastele_clone, curele_clone;
			// while (n) {
		 //    i = Math.floor(Math.random() * ga_userlist.length);
		 //    if (i in ga_userlist) {
		 //    	curele = $(".ga_tables .ga_users_div .ga_users").eq(i);
		 //    	if (pastele != null && pastele != '') {
		 //    		curele_clone = curele.clone();
		 //    		pastele_clone = pastele.clone();
		 //    		curele.replaceWith(pastele_clone);
		 //    		pastele.replaceWith(curele_clone);
		 //    	}
		 //    	pastele = curele;
		 //    	// $(".ga_tables .ga_users_div .ga_users").eq(i).addClass('white_shuffle_ga');
		 //      delete ga_userlist[i];
		 //      n--;
		 //    }
		 //  }
		 // shuffling_effect();
		}
	}
	function giveaway_winner(winner_arr) {
		var postData = {
			'user_id' : winner_arr.user_id,
			'giveaway_id' : winner_arr.giveaway_id,
			'lobby_id' : winner_arr.lobby_id,
			'giveaway_tag' : winner_arr.giveaway_tag,
		};
		$.ajax({
			url: base_url+'giveaway/giveaway_winner',
			type: "post",
			data: postData,
			// beforeSend: function() { loader_show(); } ,
			success: function(result) {
				// loader_hide();
				var result = $.parseJSON(result);
				// if (result.inserted != undefined && result.inserted != '') {
					
				// }
			},
		});
	}
	function send_message_to_giveaway_grp(arr){
		var socket_data, postData;
		if (arr.formData != undefined && arr.formData !='') {
			var xhr = new XMLHttpRequest();
			xhr.open('POST',base_url+'giveaway/send_message_to_giveaway_grp',true);
			xhr.responseType = 'json';
			xhr.onreadystatechange = function (srcData) {
				srcData = this.response;
				if (srcData !='' && srcData != null) {
					var data = {
						'msg_data': srcData.ga_get_chat,
						'name' : arr.fan_tag,
						'tag': 'send_socket_msg_ga_grp'
					};
					commongashow(data);
					$('.attachfile_div input[type="file"]').val('');
					arr.formData.delete(arr.lobby_id);
				}
			};
			xhr.send(arr.formData);
		} else {
			postData = {
				'lobby_id': arr.lobby_id,
				'giveaway_id' : arr.giveaway_id,
				'message': arr.message,
				'user_id': arr.user_id
			};
			$.ajax({
				url : base_url +'giveaway/send_message_to_giveaway_grp',
				type: "post",
				data: postData,
				success: function(srcData){
					srcData = $.parseJSON(srcData);
					var data = {
						'msg_data': srcData.ga_get_chat,
						'name' : arr.fan_tag,
						'tag': 'send_socket_msg_ga_grp'
					};
					commongashow(data);
					$('.commonchatbox_chatbox_main_row .cmn_grp_msgbox_message-input .emojionearea-editor').html('');
					$('.attachfile_div input[type="file"]').val('');
				}
			});
		}
	}
	if (typeof socket != 'undefined') {
		socket.on('getrealtimedata',function(getdata){
			var lobby_id = getdata.data.lobby_id;
			var winlocation = window.location.href.split('#')[0].toLowerCase();
			var getloc = (base_url+'giveaway/'+lobby_id).toLowerCase();
			if (getdata.data.event == 'giveaway') {
				if (getloc == winlocation) {
					var user_id = $('#user_id').val();

					if (getdata.data.on == 'join_giveaway_data') {
						(user_id == getdata.data.ga_creator) ? $('.ga_table .start_button').html('<a class="cmn_btn shuffle_confirmation">Start</a>') : '';
						$('.ga_tables').append(getdata.data.giveaway_new_user);
						$('input[name="remaining_items"]').val($('input[name="remaining_items"]').val() - 1);
						$('.total_registred_plyr span').text(parseInt($('.total_registred_plyr span').text()) + 1);

						if (remaining_items == 0) {
							$('.getanother_ticket_div .get_another_ticket_ga_btn').remove();
							$('.ga_buy_more_entries').hide().find('.modal_data').html('');
						}
						if (user_id == getdata.data.ga_fee_clctor_arr.user_id)
							$('.tot-balance').html('$'+parseFloat(getdata.data.ga_fee_clctor_arr.total_balance).toFixed(2));
						else if (user_id == getdata.data.ga_amnt_clctor_arr.user_id)
							$('.tot-balance').html('$'+parseFloat(getdata.data.ga_amnt_clctor_arr.total_balance).toFixed(2));
					}

					if (getdata.data.on == 'another_entries_giveaway_data') {
						var remaining_items = $('input[name="remaining_items"]').val() - getdata.data.another_entries;
						$('input[name="remaining_items"]').val(remaining_items);
						$('.total_registred_plyr span').text(parseInt($('.total_registred_plyr span').text()) + parseInt(getdata.data.another_entries));
						if (remaining_items == 0) {
							$('.getanother_ticket_div .get_another_ticket_ga_btn').remove();
							$('.ga_buy_more_entries').hide().find('.modal_data').html('');
						}
						for (var i = getdata.data.another_entries - 1; i >= 0; i--) {
							// var left_length = $('.ga_tables.left .ga_users_div').length, right_length = $('.ga_tables.right .ga_users_div').length;
							// var tg_tbl = (left_length == right_length || left_length < right_length) ? 'left' : 'right';
							// var giveaway_new_user = $('.ga_users_div[user_id="'+getdata.data.user_id+'"]').eq(0).clone().removeClass().addClass(tg_tbl+'_table text-'+tg_tbl+' ga_users_div');
							// $('.ga_tables.'+tg_tbl).append(giveaway_new_user);
							var random_color = '#'+ ('000000' + Math.floor(Math.random()*16777215).toString(16)).slice(-6);
							var clone_ele = $('.ga_users_div[user_id="'+getdata.data.user_id+'"]').eq(0).clone();
							clone_ele.find('a').css({'background-color': random_color});
							var giveaway_new_user = clone_ele.removeClass().addClass('left_table text-left ga_users_div');
							$('.ga_tables').append(giveaway_new_user);

						}
						if (user_id == getdata.data.ga_fee_clctor_arr.user_id)
							$('.tot-balance').html('$'+parseFloat(getdata.data.ga_fee_clctor_arr.total_balance).toFixed(2));
						else if (user_id == getdata.data.ga_amnt_clctor_arr.user_id)
							$('.tot-balance').html('$'+parseFloat(getdata.data.ga_amnt_clctor_arr.total_balance).toFixed(2));
					}

					if (getdata.data.on == 'realtime_shuffle') {
						$('body').append('<div class="clearfix btn_disable_overlay"> </div>');
						if (getdata.data.action == 'start') {
							var shuffling_miniseconds = 20000;
							var intrvl_loop = $('.ga_table .ga_tables').rdmGrid({columns: 7});
							intrvl = setInterval(() => {intrvl_loop.trigger();}, 700);
						} else {
							var shuffling_miniseconds = 500;
							clearTimeout(timout);
							clearInterval(intrvl);
						}
				  	timout = setTimeout(function() { 
				  		clearInterval(intrvl);
				  		
				  		giveaway_shuffle("'"+getdata.data.random_winner+"'");
				  		$('.modal').css('z-index',5);
				  		$('.start_button').css('z-index',0).find('.shuffle_confirmation').attr('action','start').text('Start');
				  		var giveaway_id = $('#giveaway_id').val(), winner_user = $('.ga_tables .ga_users_div').eq(getdata.data.random_winner).attr('user_id');
				  		var giveaway_tag = $('div[user_id="'+winner_user+'"] a').eq(0).text();
				  		var winner_arr = {
				  			'user_id' : winner_user,
				  			'giveaway_id' : giveaway_id,
				  			'giveaway_tag' : giveaway_tag,
				  			'lobby_id' : lobby_id
				  		}
				  		giveaway_winner(winner_arr);
				  		var winner_tile_css = {'animation': 'ga_winner_animation 1.2s alternate infinite ease-in','z-index' : '1'};
				  		var winner_div = $('.ga_tables .ga_users_div .ga_users.white_shuffle_ga').closest('.ga_users_div').css(winner_tile_css).clone().removeAttr('style');
				  		var fireworks_blast_content = '<div class="fireworks_blast"><div class="clearfix winner_tile"><div class="winner_div"></div><div class="winner_label"><h1>You Won !!</h1></div></div></div>';

				  		$('body').append(fireworks_blast_content);
				  		$('.fireworks_blast .winner_div').css({'display':'none', 'position':'relative'}).append(winner_div).slideDown("slow");

				  		$('.banner').show().animate({'font-size':'3em'},1000);
				  		$('.fireworks_blast').fireworks().children('h1').animate({'font-size':'70px'},1000).animate({'font-size':'36px'},1000);
				  		($('.fireworks_audio_file').length && $('.fireworks_audio_file') != undefined) ? $('.fireworks_audio_file')[0].play() : '';
							setTimeout(function () {
								$('.fireworks_blast').fireworks('destroy');
								($('.fireworks_audio_file').length && $('.fireworks_audio_file') != undefined) ? $('.fireworks_audio_file').trigger("pause") : '';
								$('.fireworks_blast, .btn_disable_overlay').remove();
							}, getdata.data.fireworks_duration_time);
				  	}, shuffling_miniseconds);
				  }

				  if (getdata.data.on == 'ga_stream_action') {
				  	if (getdata.data.default_stream_showstatus == 0) {
				  		$('.ga_default_stream_main_div').hide().find('.ga_default_stream').removeAttr('src');
				  		$('.cemara_action').find('img').attr('src',base_url+'assets/frontend/images/ga_live_stream_off.png');
				  	} else {
				  		$('.ga_default_stream_main_div').show().find('.ga_default_stream').attr('src','https://player.twitch.tv/?channel='+getdata.data.default_stream+'&parent='+ $(location).attr('hostname'))
				  		$('.cemara_action').find('img').attr('src',base_url+'assets/frontend/images/ga_live_stream_on.png');
				  	}
				  }

					if (getdata.data.on == 'send_message_in_giveaway_grp') {
						var crtr = $('input[name="ga_crtr"]').val();
						if (user_id == crtr) {
							var lbyctr = 1;
						}
						var direction = 'left', download_attr = 'download', clss = 'enable';
						if (user_id == getdata.data.session_id) {
							direction = 'right', download_attr = '', clss = 'disable';
						}
						var get_msg_bx_height = $('.commonchatbox_msg_bx').height(), context_menu = '';
						var cn_clss = (lbyctr || getdata.data.is_admin == 1) ? "del_opt" : '';
						if (lbyctr || getdata.data.is_admin == 1){
							context_menu = '<div class="dropdown clearfix contmenu_'+getdata.data.msg_data.message_id+' contextMenu"><ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu" style="display:block;">';
							context_menu += '<li><a class="remove_msg" onclick="send_msg_delete('+getdata.data.msg_data.message_id+','+getdata.data.msg_data.user_id+')">Remove Msg</a></li>';
							if (direction == 'left') {
								context_menu += '<li><a class="remove_userid" onclick="del_usr_from_chat('+getdata.data.msg_data.user_id+');" >Remove User</a></li><li><a class="mute_user" onclick="mute_usr_from_chat('+getdata.data.msg_data.user_id+');">Time Out User</a></li>';
							}
							context_menu += '</ul></div>';
						}
						var msg = getdata.data.msg_data.message;
						if (getdata.data.msg_data.message_type == 'file') {
							msg = '<a href="'+base_url+'upload/all_images/'+getdata.data.msg_data.attachment+'" class="download_attachment lobby_grpmsg_attach_link '+clss+'" '+download_attr+'><i class="fa fa-download '+clss+'"></i><img class="attachment_image img img-thumbnail" src="'+base_url+'upload/all_images/'+getdata.data.msg_data.attachment+'" class="lobby_grpmsg_attach" alt="pic" width="100px"></a>';
						} else if (getdata.data.msg_data.message_type == 'tipmessage') {
							var tipicon = '<img src="'+base_url+'upload/stream_setting/'+getdata.data.msg_data.attachment+'" alt="tipimg" class="tipiconimg">';
							msg = (user_id == getdata.data.session_id) ? tipicon+getdata.data.msg_data.message : getdata.data.msg_data.message+tipicon;
						}
						var img = (getdata.data.is_admin == 1) ? '<img class="chat_image pull-'+direction+'" width="20" height="20" src="'+base_url+'upload/admin_dashboard/Chevron_3_Twitch_72x72.png">' : '';
						var append_data = '<div data-mid="'+getdata.data.msg_data.message_id+'" data-cid="'+getdata.data.msg_data.id+'" class="message-row"><div class="col-md-12 col-sm-12 col-xs-12 message-box '+cn_clss+'" tgcontmenu="'+getdata.data.msg_data.message_id+'"><div class="message_header">'+img+'<small class="list-group-item-heading text-muted text-primary pull-'+direction+'">'+getdata.data.name+'</small></div><div class="message_row_container in pull-'+direction+'"><p class="list-group-item-text"></p><p>'+msg+'</p>'+context_menu+'<p></p></div></div></div>';
						$('.commonchatbox_chatbox_main_row #message-inner').append(append_data).find('.no_messages').remove();
						
						$("body").on("contextmenu", ".message-box.del_opt", function(e) {
						  var tgcontmenu = $(this).attr('tgcontmenu'), contextMenu = $(".contextMenu.contmenu_"+tgcontmenu);
						  $(".contextMenu").hide();
						  contextMenu.css({ display: "block", width: 'auto', position: 'absolute' });
						  return false;
						});
						$('html').click(function() { $(".contextMenu").hide(); });
						($("#message-inner").length) ? $("#message-inner").scrollTop($("#message-inner")[0].scrollHeight) : '';
						// var data = {
						// 	'lobby_id':lobby_id,
						// 	'audio_file':getdata.data.getmytip_sound,
						// 	'event': 'msg_sound_play'
						// };
						// commongaappend(data);
					}

				  if (getdata.data.on == 'page_refresh') {
				  	window.location.reload();
				  }
					commongiveawayfunction();
				}
			}


			// ======== For chat box ========= //
			var get_lobbyid_forchatbox_loc = (base_url+'giveaway/'+getdata.data.id).toLowerCase();
			if (get_lobbyid_forchatbox_loc == winlocation) {
				if (getdata.data.on == 'delete_msg_from_lobby') {
					$('.message-row[data-mid="'+getdata.data.msg_id+'"]').remove();
				}
				if (getdata.data.on == 'del_usr_from_looby_chat') {
					($('.lobbymsgbox_userid_'+getdata.data.user_id).length || $('.msgbox_userid_'+getdata.data.user_id).length) ? $('.msgbox_userid_'+getdata.data.user_id, '').remove() : '';
					($('.msg_usr_'+getdata.data.user_id).length) ? $('.msg_usr_'+getdata.data.user_id).remove() : '';
				}
				if (getdata.data.on == 'mute_usr_from_looby_chat') {
	      	var user_id = $('input[name="user_id"]').val();
	      	if (user_id == getdata.data.user_id) {
	      		$('#message-container .lobby_grp_msgbox, #message-container .grp_msgbox').addClass('disable').find('textarea').attr('readonly','');
	      		var append_mute_timer = 'You can chat after <span class="mutetimer" data-seconds-left="'+getdata.data.mutediff+'"></span> Seconds';
	      		$('.mute_div').addClass('mutetimer_div').html(append_mute_timer);
	      		$('.commonchatbox_msg_bx .mutetimer').startTimer({
	      			onComplete: function(element){
						    element.addClass('is-complete');
					    	if ($('.commonchatbox_msg_bx .mutetimer_div').length) {
					    		$('.commonchatbox_msg_bx .mute_div.mutetimer_div').removeClass('mutetimer_div');
					    		$('.commonchatbox_msg_bx .mute_div').html('');
					    		$('.commonchatbox_msg_bx .grp_msgbox').removeClass('disable');
					    		$('.commonchatbox_msg_bx .grp_msgbox textarea').removeAttr('readonly');
					    	}
					    }
						});
	      	}
				}
			}
			// ======== chat box ========= //
		});
	}
}(jQuery));