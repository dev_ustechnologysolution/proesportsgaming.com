/*
rdmGrid
Version: 1.0.0
License: https://github.com/patrick-hlt/rdm-grid/blob/master/LICENSE
*/
$.fn.rdmGrid = function(a) {
    function b(h) {
        let m, o, p = h.length;
        for (let q = p - 1; 0 < q; --q) m = Math.floor(Math.random() * (q + 1)), o = h[q], h[q] = h[m], h[m] = o
    }
    const c = $.extend({
            animationSpeed: 350,
            breakPoint: 800,
            btns: 'rg_btn',
            columns: 3,
            fadeInSpeed: 350,
            initialShuffle: !0,
            responsive: !0
        }, a),
        d = this,
        f = $(this);
    let g;
    if (f.css({
            position: 'relative',
            'box-sizing': 'border-box'
        }).children().css({
            display: 'none',
            position: 'absolute',
            'box-sizing': 'border-box'
        }), 1 === f.length) g = $('.' + c.btns).filter('[data-rg=' + $(f[0]).attr('id') + ']');
    else {
        let h = [];
        f.each(function() {
            h.push($(this).attr('id'))
        }), g = $('.' + c.btns).filter(function() {
            if (-1 !== h.indexOf($(this).attr('data-rg'))) return this
        })
    }
    return g.click(function(h) {
        h.preventDefault(), d.trigger($('#' + $(this).attr('data-rg')))
    }), !0 === c.responsive && $(window).width() < c.breakPoint && (c.columns = 1), d.trigger = function(h, m = !1) {
        var o;
        h instanceof jQuery ? o = h : 'string' == typeof h ? o = $('#' + h) : h ? console.error('ERROR in jQuery plugin \'rdmGrid\' inside \'trigger()\': Can\'t detect type of element.') : o = $(this);
        const p = o.children(),
            q = o.innerWidth(),
            r = q / c.columns;
        let s = 0,
            t = p.length,
            u = t / c.columns,
            v = [];
        o.children().css({
            width: r
        }), (!0 === m && !0 === c.initialShuffle || !1 === m) && b(p), $(p).each(function() {
            $(this).height('initial')
        });
        let w = 0;
        for (let y = 0; y < u; ++y)
            for (let z = 0; z < c.columns; ++z) $(p[w]).attr({
                'rg-row': y,
                'rg-col': z
            }), w++;
        for (let y = 0; y < u; ++y) {
            let z = [],
                A = [];
            for (let C = 0; C < t; ++C) $(p[C]).attr('rg-row') == y && (z.push(p[C]), A.push($(p[C]).outerHeight(!0)));
            let B = Math.max.apply(null, A);
            $(z).each(function() {
                $(this).height(B + 'px')
            }), v.push(s), s += B
        }!0 === m ? o.height(s) : o.animate({
            height: s
        }, c.animationSpeed), $(p).each(function() {
            !0 === m ? $(this).css({
                left: $(this).attr('rg-col') * r,
                top: v[$(this).attr('rg-row')]
            }).fadeIn(c.fadeInSpeed) : $(this).animate({
                left: $(this).attr('rg-col') * r,
                top: v[$(this).attr('rg-row')]
            }, c.animationSpeed);
        })
    }, f.each(function() {
        d.trigger($(this), !0)
    }), d
};