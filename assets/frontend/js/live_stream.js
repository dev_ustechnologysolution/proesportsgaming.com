function stream_launch(stream_arr) {
	if (flvjs.isSupported() && stream_arr.stream_key != undefined) {
		// var url2 = stream_arr.base_url.replace(/\/$/, '');
		var url2 = protocol+server_name;
		var streaming_port = '8100';
		(server_name == 'localhost') ? streaming_port = '8000': '';
		var videoElement = $('#'+stream_arr.random_id+' .obs_streaming')[0];
		var stream_url = url2+':8100/live/'+stream_arr.stream_key+'.flv'
		var flvPlayer = flvjs.createPlayer({
			"type": 'flv',
			"url": stream_url,
			"isLive": true,
		});
		flvPlayer.attachMediaElement(videoElement);
		$(videoElement).show();
		// videojs('videojs-flvjs-player', {
		//     techOrder: ['html5', 'flvjs'],
		//     flvjs: {
		//     	mediaDataSource: {
		//     		isLive: true,
		//         	cors: true,
		//         	withCredentials: false,
		//         	controls: false
		//       	},
		//       	// config: {},
		//     },
		// });
		flvPlayer.load();
		($(videoElement).attr('autoplay')) ? flvPlayer.play() : '';
	} else {
	  	$('#'+stream_arr.random_id+' .stream_error').html('Your browser is not supported FLV player').show();
	}
}	