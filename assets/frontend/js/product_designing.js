$(function (){
    'use strict';
    $('.customizeble_product').click(function () {
        // var image_url = $('.prdct_img_ul .prdctimg.active img').attr('src');
        // (image_url != '') ? $('.product_cust_design .customize_prdct_img img').attr('src',image_url) : '';
        $('.customize_product_image').show();
    });
    $('#set_text_prdct').keyup(function() {
        $('.product_cust_design .custm_text').text($(this).val());
    })
    $('#font_size_sldr').change(function() {
        $('.product_cust_design .custm_text').css('font-size',$(this).val()+'px');
    })
    $('.simple-color-picker.prdct_text_color').colorpicker().on('changeColor', function (e) {
      $('.product_cust_design .custm_text').css('color', e.color.toHex())
    });
    $('.product_cust_design .custm_text').draggable({
        cursor: "pointer",
        stop: function( event, ui ) {}
    })
    $('.product_cust_design .save_customized_tag').click(function() {
      var customizable_tag = $('.product_cust_design input[name="customizable_tag"]').val();
      $('.product_cust_design .custom_tag_error').hide();
      if (customizable_tag == undefined || customizable_tag == '') {
        $('.product_cust_design .custom_tag_error').show();
        return false;
      } 
      var get_exist_custom_tag = $('.custom_tag_variation').val().split('optlbl_')[1];
      var custom_tag_variation = $('.custom_tag_variation').val().replace(get_exist_custom_tag, customizable_tag);
      $('.custom_tag_variation').val(custom_tag_variation);
      $('.customize_product_image').hide();
    });
    $('.product_cust_design .save_customized_img').click(function() {
        function hiddenClone(element){
          var clone = element.cloneNode(true);
          var style = clone.style;
          style.position = 'relative';
          style.display = 'block';
          style.top = window.innerHeight + 'px';
          style.left = 0;
          document.body.appendChild(clone);
          return clone;
        }
        var offScreen = document.querySelector(".customize_prdct_img");;
        var clone = hiddenClone(offScreen);
        html2canvas(clone).then(function(canvas) {
          var data = canvas.toDataURL('image/jpeg');
          save_img(data);
        });
        function save_img(data) {
            $('.product_main_img.show.zoom').attr('href',data).find('img').attr('src',data);
            $('.customize_product_image').hide();
            $('.add-to-cart_form input[name="product_image_base64"]').remove();
            $('.customize_prdct_img').last().hide();
            $('.add-to-cart_form').append($('<input type="hidden" name="product_image_base64" value="'+data+'">'));
        }
    })
});