// Copyright (c) 2015, Fujana Solutions - Moritz Maleck. All rights reserved.
// For licensing, see LICENSE.md
function getBaseUrl() {
	var str = window.location.href;
	var n = str.indexOf("admin");
	return str.substr(0, n);
	}
var b_url=getBaseUrl();
CKEDITOR.plugins.add( 'imageuploader', {
    init: function( editor ) {
        editor.config.filebrowserBrowseUrl = b_url+'ckeditor/plugins/imageuploader/imgbrowser.php';
    }
});
