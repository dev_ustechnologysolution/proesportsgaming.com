<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Chat extends My_Controller {
    function __construct() {
        parent::__construct();
        $this->check_user_page_access();
    }
    public function chat_box() {
        $data['user_to']=$_GET['user_to'];
        $data['active']='chat_box';
        $data['banner_data'] = $this->General_model->tab_banner();
        $content=$this->load->view('chat/chat_box',$data,true);
        $this->render($content);
    }
    public function friends_chat_box() {
        $data['user_to']=$_GET['user_to'];
        $data['active']='chat_box';
        $data['banner_data'] = $this->General_model->tab_banner();
        $content=$this->load->view('chat/friends_chat_box',$data,true);
        $this->render($content);
    }
    public function group_chat_box() {
        $data_ajska = array(
            'accepterid' => $_GET['accepterid'],
            'challengerid' => $_GET['challengerid'],
            'groupdata' => $_GET['groupdata'],
            'agentonline' => $_GET['agentonline'],
            'status' => $_GET['status'],
            'userto' => $_GET['userto'],
            'user' => $_GET['user'],
            'ci' => $_GET['ci'],
            'gamename' => $_GET['gamename']
        );
        $data['active']='group_chat_box';
        $data['banner_data'] = $this->General_model->tab_banner();
        $content=$this->load->view('chat/group_chat_box',$data,true);
        $this->render($content);
    }
}