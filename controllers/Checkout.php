<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Checkout extends My_Controller {
    public function __construct() {
    	parent::__construct();
    	$this->load->model('My_model');
        $this->load->model('General_model');
        $this->load->model('Product_model');
        $this->load->model('User_model');
        $this->load->model('Cart_model');
        $this->load->model('Paypal_payment_model');
        $this->check_cover_page_access();
    }
    // Checkout page
    public function index() {
        $data['category_list'] = $this->General_model->view_all_data('product_categories_tbl','id','asc');
        $data['user_detail'] = $this->User_model->user_detail()[0];
        $data['country_list'] = $this->User_model->country_list();
        $data['state_list'] = $this->User_model->state_list($data['user_detail']['country']);
        $data['cart_item'] = $this->session->userdata('cart_item_list');
        $shipping = 0;
        if (!empty($data['cart_item'])) {
        	$shipping = $data['cart_item'][0]['int_fee'];
        }
        if ((!empty($user_detail) && $user_detail['countryname'] != 'United States') || (!empty($this->session->userdata('shippaddress')) && $this->session->userdata('shippaddress')['country'] != 'United States')) {
	      $shipping_fee = $shipping;
	    }
        $data['shipping_fee'] = number_format((float)$shipping_fee, 2, '.', '');
        $content = $this->load->view('store/checkout.tpl.php',$data,true);
        $this->render($content);
    }
    public function login() {
    	$email = $this->input->post('email', TRUE);
		$password = md5($this->input->post('password', TRUE));
		$data_arr = $this->User_model->authenticate($email,$password);
		if ($this->input->post('check_as_guest') == 'on') {
			$randomNum = substr(str_shuffle("0123456789abcdefghijklmnopqrstvwxyz"), 0, 11);
			$this->session->set_userdata('guest_user',$randomNum);
			redirect(base_url().'checkout');
		} else {
			$changeactive_flag = $this->User_model->changeactive_flag($data_arr);
			if (isset($data_arr) && is_array($data_arr) && $data_arr['is_active'] == '0') {
				$this->User_model->log_this_login($data_arr);
				$this->session->unset_userdata('login_att');
				redirect(base_url().'checkout');
				exit();
			} else if (isset($data_arr) && is_array($data_arr) && $data_arr['is_active'] == '1') {
				$data['err']="";
				if($email!=''){	
					$checkbanned = $this->User_model->checkbanned($data_arr);
					$data['err'] ='<h5 style="margin-left: 255px;color: red;">'.$checkbanned.'</h5>';
				}
	        	$content=$this->load->view('login/login.php',$data,true);
	        	$this->render($content);
			} else { 
	        	$data['err']="";
	        	if ($email!='') {
	        		$data['err'] ='<h5 style="margin-left: 255px;color: red;">Invalid username or password</h5>';
	        	}
				if ($this->session->userdata['login_att'] == '') {
					$this->session->set_userdata('login_att','1');
				} else {
					$user_att = $this->session->userdata['login_att'] + 1;
					$this->session->set_userdata('login_att',$user_att);
				}
				if ($this->session->userdata['login_att'] > 7) {
					$data['err'] ='<h5 style="margin-left: 255px;color: red;">Your Account Has Been Blocked </h5>';
					$user_data	=	array(
						'is_active'	=>	'1',
					);
					$this->General_model->update_data('user',$user_data,array('email'=>$email));
				}
				$content=$this->load->view('login/login.php',$data,true);
				$this->render($content);
	        }
	    }
    }
    public function contact_bill_info() {
    	if ($_POST['email'] !='' && $_POST['name'] !='' && $_POST['phone'] !='') {
    		$order_usrinfo = array(
    			'email' => $_POST['email'],
    			'name' => $_POST['name'],
    			'phone' => $_POST['phone']
    		);
    		$this->session->set_userdata('order_usrinfo',$order_usrinfo);
    	}
    	if ($_POST['address'] !='' && $_POST['country'] !='' && $_POST['state'] !='' && $_POST['zip_code'] !='') {
    		$billaddress = $shippaddress = array(
    			'address' => $_POST['address'],
    			'country' => $_POST['country'],
    			'state' => $_POST['state'],
    			'countrysortname' => $_POST['bill_countrysortname'],
    			'zip_code' => $_POST['zip_code']
    		);
    		$this->session->set_userdata('billaddress',$billaddress);
    		if ($_POST['ship_to_diffadd_check'] == 'on') {
	    		$shippaddress = array(
	    			'address' => $_POST['shipping_address'],
	    			'country' => $_POST['shipping_country'],
	    			'state' => $_POST['shipping_state'],
	    			'countrysortname' => $_POST['shipp_countrysortname'],
	    			'zip_code' => $_POST['shipping_zip_code'],
	    		);
    		}
    		$this->session->set_userdata('shippaddress',$shippaddress);
    	}
    	redirect(base_url().'checkout');
    }
    public function checkoutpay() {
    	if (!empty($_POST)) {
    		$user_id = ($this->session->userdata('user_id') !='') ? $this->session->userdata('user_id') : 0;
	    	$shipping_fee = $_POST['shipping_fee'];
	    	$cart_item = $this->session->userdata('cart_item_list');
	    	$int = array_column($cart_item,'int_fee');
	    	$prdct_prc = array_column($cart_item,'price');
	    	$SUBTOTAL = array_sum($int)+array_sum($prdct_prc);
	    	$total = number_format((float)($shipping_fee)+($SUBTOTAL), 2, '.', '');
	    	$order_calc = array(
				'shipping_fee' => $shipping_fee,
				'subtotal' => $SUBTOTAL,
				'total' => $total
			);
			$this->session->set_userdata('order_calc',$order_calc);

			// Amount calculation
			$pymnt_opt = array(
				'user_id' => $user_id,
				'shipping_fee' => $shipping_fee,
				'cart_item' => $cart_item,
				'int' => $int,
				'prdct_prc' => $prdct_prc,
				'SUBTOTAL' => $SUBTOTAL,
				'total' => $total
			);
			if (in_array('from_wallet', $_POST['check_paypal']) && !in_array('pay_with_paypal', $_POST['check_paypal'])) {
				if ($total > $this->session->userdata('total_balance')) {
					$this->session->set_flashdata('message_err', 'Insufficient balance to checkout');
					redirect(base_url().'checkout');
					exit();
				}
				$pymnt_opt['payment_method'] = 'wallet';
				$this->wallet_checkout($pymnt_opt);
			} else if (in_array('from_wallet', $_POST['check_paypal']) && in_array('pay_with_paypal', $_POST['check_paypal'])) {
   				$rvtotal = $total - $this->session->userdata('total_balance');
   				if ($rvtotal>0) {
   					$pymnt_opt['payment_method'] = ($total == $rvtotal) ? 'paypal' : 'paypal&wallet';
   					$total = $rvtotal;
   					$pymnt_opt['total'] = $total;
   					$pymnt_opt['fromwallet'] = $this->session->userdata('total_balance');
		   			$this->paypal_checkout($pymnt_opt);
   				} else {
   					$pymnt_opt['payment_method'] = 'wallet';
					$this->wallet_checkout($pymnt_opt);
   				}
   			} else if (!in_array('from_wallet', $_POST['check_paypal']) && in_array('pay_with_paypal', $_POST['check_paypal'])) {
   				$pymnt_opt['fromwallet'] = 0;
   				$pymnt_opt['payment_method'] = 'paypal';
	   			$this->paypal_checkout($pymnt_opt);
   			}
    	}
    }
    function wallet_checkout($wallet_opt) {
    	$user_id = $wallet_opt['user_id'];
    	$grand_total = $wallet_opt['total'];
    	$payment_method = $wallet_opt['payment_method'];
    	if ($user_id != 0) {
        	$rem_bal = $this->session->userdata('total_balance') - $grand_total;
        	$update_data = array(
        		'total_balance' => number_format((float) $rem_bal, 2, '.', '')
        	);
            $this->General_model->update_data('user_detail', $update_data, array('user_id' => $user_id));
        }

    	$plc_ckordr = array(
			'user_id' => $user_id,
			'payment_method' => $payment_method
		);
    	$this->place_ckout_order($plc_ckordr);
    }
    function paypal_checkout($paypl_opt) {
		$fromwallet = $paypl_opt['fromwallet'];
		$payment_method = $paypl_opt['payment_method'];
		$user_id = $paypl_opt['user_id'];
    	$cart_item = $paypl_opt['cart_item'];
    	$shipping_fee = $paypl_opt['shipping_fee'];
    	$int = $paypl_opt['int'];
    	$prdct_prc = $paypl_opt['prdct_prc'];
		$SUBTOTAL = $paypl_opt['SUBTOTAL'];
		$total = $paypl_opt['total'];
    	if (isset($_SESSION['order_usrinfo']) && !empty($_SESSION['order_usrinfo'])) {
			$data['shiptoname'] = $_SESSION['order_usrinfo']['name'];
			$data['shiptophonenum'] = $_SESSION['order_usrinfo']['phone'];
		}
		if (isset($_SESSION['shippaddress']) && !empty($_SESSION['shippaddress'])) {
            $data['shiptostreet'] = $_SESSION['shippaddress']['address'];
            $data['shiptozip'] = $_SESSION['shippaddress']['zip_code'];
            $data['shiptostate'] = $_SESSION['shippaddress']['state'];
            $data['shiptocountrycode'] = $_SESSION['shippaddress']['countrysortname'];
		}
		foreach ($cart_item as $key => $ci) {
			$set_cartitem[] = array(
                'name' => $ci['product_name'],
                'amt' => $ci['price'],
                'number' => $ci['product_id'],
                'qty' => $ci['prdct_qntity'],
                'itemurl' => base_url().'Store/view_product/'.$ci['product_id']
			);
		}
		$data['cartitem'] = $set_cartitem;
        $data['subscribe_amount'] = $total;        
        $payer_detail = $this->User_model->user_detail();
        $data['payer_email'] = $payer_detail[0]['email'];
       	$data['returnurl'] = $data['cancelurl'] = base_url().'checkout/get_payment_data/';
       	$data['custom'] = 'userid='.$user_id.',fromwallet='.$fromwallet.',payment_method='.$payment_method;
       	$data['title'] = 'Checkout Cart.';
       	 $data['notifyurl'] = '';
        $data['billingtype'] = 'MerchantInitiatedBilling';
       	$response = $this->Paypal_payment_model->Set_express_checkout($data);
       	if($response['redirect'] != ''){
       		redirect($response['redirect']);
			exit();
       	} else {
       		$this->error($response['errors']);
       	}
    }
    public function get_payment_data() {
    	if ($_GET['token']) {
            $token = $_GET['token'];
            $result = $this->Paypal_payment_model->Get_express_checkout_details($token);
	    	if ($result['status'] == 1 && $result['data']['ACK'] == 'Success') {
	    		$custom_data = explode(',', $result['data']['CUSTOM']);
	    		$user_id = explode('=',$custom_data[0])[1];
	    		$wallet_amount = explode('=',$custom_data[1])[1];
	    		$payment_method = explode('=',$custom_data[2])[1];
	    		$plc_ckordr = array(
	    			'payment_method' => $payment_method,
	    			'total' => $wallet_amount,
	    			'user_id' => $user_id
	    		);
            	$this->wallet_checkout($plc_ckordr);
	    	}
    	}
    }
    function place_ckout_order($plc_ckordr) {
    	$payment_method = $plc_ckordr['payment_method'];
    	$user_id = $plc_ckordr['user_id'];
        $comps = explode(' ', microtime());
        $transaction_id = 'order'.sprintf('%d%03d', $comps[1], $comps[0] * 1000);
        $shippaddress = $this->session->userdata('shippaddress')['address'].', '.$this->session->userdata('shippaddress')['country'].', '.$this->session->userdata('shippaddress')['zip_code'];
        $billaddress = $this->session->userdata('billaddress')['address'].', '.$this->session->userdata('billaddress')['country'].', '.$this->session->userdata('billaddress')['zip_code'];
        if (!empty($this->session->userdata('order_calc'))) {
        	$subtotal = $this->session->userdata('order_calc')['subtotal'];
        	$shipping_fee = $this->session->userdata('order_calc')['shipping_fee'];
        	$grand_total = $this->session->userdata('order_calc')['total'];
        }
        $insert_order = array(
            'transaction_id' => $transaction_id,
            'sold_by' => 2,
            'user_id' => $user_id,
            'shipping_address_id' => $shippaddress,
            'billing_address_id' => $billaddress,
            'sub_total' => $subtotal,
            'tax' => $shipping_fee,
            'grand_total' => $grand_total,
            'payment_method' => $payment_method,
            'order_status' => 'placed',
        );
        $order = $this->General_model->insert_data('orders_tbl', $insert_order);
        if ($order) {
            foreach ($this->session->userdata('cart_item_list') as $key => $itmlst) {
	        	$sel_variation = !empty($itmlst['sel_variation']) ? explode(', ', $itmlst['sel_variation']) : '';
	        	if (isset($sel_variation) && $sel_variation != '') {
	        		foreach ($sel_variation as $k => $sv) {
	        			$sv = explode('&', $sv);
	    				$var_title = end(explode('_', $sv[0]));
	        			$var_opt = end(explode('_', $sv[1]));
	        			$variation[] = $var_title.'_'.$var_opt;
	        		}
	        	}
				$product_attributes = implode(',', $variation);

                $insert_order_items = array(
                    'order_id' => $order,
                    'product_id' => $itmlst['product_id'],
                    'product_name' => $itmlst['product_name'],
                    'product_image' => $itmlst['select_img'],
                    'qty' => $itmlst['prdct_qntity'],
                    'amount' => $itmlst['price'],
                    'product_attributes' => $product_attributes
                );
                $insertitems = $this->General_model->insert_data('orders_items_tbl', $insert_order_items);
            }
            if ($insertitems) {
				$this->session->unset_userdata('shippaddress');
				$this->session->unset_userdata('billaddress');
				$this->session->unset_userdata('cart_item_list');
                $this->session->set_flashdata('message_succ', 'Order placed successfull');
                redirect(base_url().'Store');
            } else {
                $this->session->set_flashdata('message_err', 'Unable to Order place');
                redirect(base_url().'Store/cart');
            }
        } else {
            $this->session->set_flashdata('message_err', 'Unable to Order place');
            redirect(base_url().'Store/cart');
        }
    }
}