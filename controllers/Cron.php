<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Cron extends My_Controller
{
    function __construct()
    {
        parent::__construct();
		$this->load->model('General_model');
		$this->load->model('User_model');
        $this->target_dir=BASEPATH.'../upload/game/';
		
    }
    public function index(){
		$where = array(
			'status' =>  '1',
		);
		
		$gameListDp = $this->General_model->view_data('game',$where);
		
		foreach($gameListDp as $gameList){
			if($gameList['game_timer'] != '' && $gameList['game_timer']  != "0000-00-00 00:00:00" ){
				if(strtotime($gameList['game_timer']) < time()){
					echo $gameList['price'];
					$user_data_where = array(
						'user_id'	=> $gameList['user_id'],
					);
					$userListDp = $this->General_model->view_data('user_detail',$user_data_where);
					foreach($userListDp as $userList){
						$user_where = array(
							'user_id'	=> $userList['user_id'],
						);
						$update_user = array(
							'total_balance'	=> $gameList['price'] + $userList['total_balance'],
						);
						$this->General_model->update_data('user_detail',$update_user,$user_where);
						echo $this->db->last_query();
					}
					$update_game = array(
						'status'	=> '0',
					);
					$update_where =	array(
						'id'		=> $gameList['id']
					);
					$this->General_model->update_data('game',$update_game,$update_where);					
					
				} else {
					
				}
			}			
		}
    }
	
	
	function game_won_lose(){
		$list = $this->General_model->url_info();
		
		if(!empty($list)){
			foreach($list as $lis) {
				
				
				$listuser 	= explode(',',$lis['bet_user_id']);
				$liststatus = explode(',',$lis['v_status']);
				$listvideo 	= explode(',',$lis['v_video_id']);
				
				if($listuser[0] != '' && $listuser[1] != ''){
					
					if($liststatus[0] == '1' && $liststatus[1]  == '1'){
						echo 'Both Same';
					} else  if($liststatus[0] == '0' && $liststatus[1]  == '0'){
						echo 'Both Same';
					} else {
						
						if($liststatus[0] == '1'){
							$user_id_win	= $listuser[0];
							$video_id		= $listvideo[0];
							$user_id_lose 	= $listuser[1];
						} else {
							$user_id_win 	= $listuser[1];
							$video_id		= $listvideo[1];
							$user_id_lose	= $listuser[0];
						}
							
						$dt=array(
							'status'	=> 3,
							'video_id'	=> $video_id,
						);
						$this->General_model->update_data('game',$dt,array('id'=>$lis['game_tbl_id']));						
						$arr_bet_id = $this->General_model->view_data('game',array('id'=>$lis['game_tbl_id']));						
						$arr_bet_data = $this->General_model->view_data('bet',array('id'=>$arr_bet_id[0]['bet_id']));
						$data	=	array('winner_id'=>$user_id_win,'loser_id'=>$user_id_lose,'win_lose_deta'=>date('Y-m-d H:i:s',time()));
						$r		=	$this->General_model->update_data('bet',$data,array('id'=>$arr_bet_id[0]['bet_id']));

$user_data_where = array(
							'user_id'	=> $user_id_win,
						);
						$userListDp = $this->General_model->view_data('user_detail',$user_data_where);
						foreach($userListDp as $userList){
							$user_where = array(
								'user_id'	=> $userList['user_id'],
							);
							$update_user = array(
								'total_balance'	=> $arr_bet_id[0]['price'] + $userList['total_balance'],
							);
							$this->General_model->update_data('user_detail',$update_user,$user_where);
						}





					}
					
				}
			}
		}	
		
	}
	
	
	
	public function winlist_cron(){
		$user_list = $this->User_model->user_view_admin();
		if(!empty($user_list)){
			foreach($user_list as $user_li){
				$won_sum[]	= '';
				$lost_sum[]	= '';
				$withdrawal_sum[]	= '';
				
				$win_amount = '';
				$lost_amount = '';
				$with_amount = '';
				
				$won_array = $this->User_model->get_all_won_amt($user_li['id']);
				if(!empty($won_array)){
					foreach($won_array as $won_arr){
						$won_sum[]	= $won_arr['price'];
					}
					$win_amount = array_sum($won_sum);
					unset($won_sum);
				}
				
				$lost_array = $this->User_model->get_all_lost_amt($user_li['id']);
				if(!empty($lost_array)){
					foreach($lost_array as $lost_arr){
						$lost_sum[]	= $lost_arr['price'];
					}
					$lost_amount =  array_sum($lost_sum);
					unset($lost_sum);
				}
				
				$withdrawal_array = $this->User_model->get_all_withdrawal_amt($user_li['id']);
				if(!empty($withdrawal_array)){
					foreach($withdrawal_array as $withdrawal_arr){
						$withdrawal_sum[]	= $withdrawal_arr['amount_paid'];
					}
					$with_amount = array_sum($withdrawal_sum);
					unset($withdrawal_sum);
				}
					
				$data =array(
					'won_amt' 			=> $win_amount,
					'lost_amt' 			=> $lost_amount,
					'withdrawal_amt' 	=> $with_amount,
				);
				
				$data_where = array(
					'user_id'	=> $user_li['id'],
				);
				
				$this->General_model->update_data('user_detail',$data,$data_where);

				
			}
			//print_r($data); die;
		}
	}
	
}