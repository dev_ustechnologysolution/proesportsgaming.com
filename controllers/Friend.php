<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Friend extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('User_model');
		$this->target_dir=BASEPATH.'../upload/profile_img/';
	}
	function index() {
		$this->check_user_page_access();
        $data['friendlist']=$this->General_model->friendlist($_SESSION['user_id']);
        $data['pendingrequest_friendlist_count'] = count($this->General_model->pendingrequest_friendlist($_SESSION['user_id']));
        $data['banner_data'] = $this->General_model->tab_banner();
		$content=$this->load->view('friend/friendlist.tpl.php',$data,true);
		$this->render($content);
	}

	function find_friendlist() {
		$this->check_user_page_access();
        $data['friendlist'] = $this->General_model->find_friendlist($_SESSION['user_id']);
		$arr_sent_friend_request = array_values(array_unique(array_column($data['friendlist'], 'friend_id')));
		$data['sent_friend_request'] = $this->General_model->remove_element($arr_sent_friend_request,'0_3');
		$data['banner_data'] = $this->General_model->tab_banner();
		$content=$this->load->view('friend/find_friendlist.tpl.php',$data,true);
		$this->render($content);
	}

	function pendingrequest_friendlist() {
		$this->check_user_page_access();
        $data['pendingrequest_friendlist']=$this->General_model->pendingrequest_friendlist($_SESSION['user_id']);
        $data['pendingrequest_friendlist_count'] = count($data['pendingrequest_friendlist']);
        $data['banner_data'] = $this->General_model->tab_banner();
		$content=$this->load->view('friend/pendingrequest_friendlist.tpl.php',$data,true);
		$this->render($content);
	}
	function friend_profile() {
		$this->check_user_page_access();
		$user_id = $_GET['user_id'];
		$data['friend_profile']=$this->General_model->friend_profile($user_id);
		$data['banner_data'] = $this->General_model->tab_banner();
		$content=$this->load->view('friend/friendprofile.tpl.php',$data,true);
		$this->render($content);
	}
	function add_friend() {
		$this->check_user_page_access();
		$friend_id = $_POST['user_to'];
		$user_id = $_SESSION['user_id'];
		$condition = 'id =' .$this->session->userdata('user_id');
		$sender_mail = $this->General_model->view_single_row_single_column('user', $condition, 'email');
		$condition = 'user_id =' .$this->session->userdata('user_id');
		$sender_mail_name = $this->General_model->view_single_row_single_column('user_detail', $condition, 'name');
		$check_mail_categories = $this->User_model->check_mail_categories($friend_id);
		$images_array = '';
		$mail = 'You have new friend request from "'.$sender_mail_name['name'].'"';
		foreach ($check_mail_categories as $key => $value) {
			if ($value['friends_mail']=='yes') {
				$sub = 'Friend request';
				$send_personal_mail=$this->User_model->send_mail_to_friend($sender_mail['email'],$value['email'],$mail,$sub);
			}
		}

		$mail_data = array(
			'mail'=>$mail,
			'attachment'=>$images_array,
			'status'=>'1'
		);	
		$r=$this->General_model->insert_data('mail_data',$mail_data);

		$data_out = array(
			'user_id'=>$_SESSION['user_id'],
			'user_to'=>$friend_id,
			'user_from'=>$_SESSION['user_id'],
			'direction'=>'OUT',
			'mail_type'=>'friends_mail',
			'status'=>'0',
			'date'=>date('Y-m-d h:i:s'),
			'message_id'=>$r
		);

		$data_in = array(
			'user_id'=>$friend_id,
			'user_to'=>$_SESSION['user_id'],
			'user_from'=>$_SESSION['user_id'],
			'direction'=>'IN',
			'mail_type'=>'friends_mail',
			'status'=>'0',
			'date'=>date('Y-m-d h:i:s'),
			'message_id'=>$r
		);

		$r.=$this->General_model->insert_data('mail_conversation',$data_out);
		$r.=$this->General_model->insert_data('mail_conversation',$data_in);		

		$data = array(
			'user_id' => $user_id,
			'friend_id' => $friend_id,
			'request' =>'2',
			'created_at' =>date('Y-m-d')
		);
		$data_2 = array(
			'user_id' => $friend_id,
			'friend_id' => $user_id,
			'request' =>'1',
			'created_at' =>date('Y-m-d')
		);
		$result = $this->General_model->insert_data('friend_list',$data);
		$result .= $this->General_model->insert_data('friend_list',$data_2);
		if ($result>0) {
			$this->session->set_flashdata('message_succ', "Friend Request Sent");
			redirect(base_url().'friend/find_friendlist');
		}
	}
	function accept_the_request($sender,$receiver) {
		$accept_request = array(
			'request' => 0,
		);
		$update_data = $this->General_model->update_data('friend_list',$accept_request,array('friend_id'=>$receiver, 'user_id'=>$sender));
		$update_data .= $this->General_model->update_data('friend_list',$accept_request,array('friend_id'=>$sender, 'user_id'=>$receiver));
		if ($update_data>0) {
	 		$this->session->set_flashdata('message_succ','Friend request Accepted');
			redirect('friend');
		}
	}
	function remove_the_request($sender,$receiver) {
		$delete_data=$this->General_model->delete_data('friend_list',array('friend_id'=>$receiver, 'user_id'=>$sender));
	 	$delete_data.=$this->General_model->delete_data('friend_list',array('friend_id'=>$sender, 'user_id'=>$receiver));
	 	if ($delete_data>0) {
	 		$this->session->set_flashdata('message_succ','Friend request Declined');
	 		redirect('friend/pendingrequest_friendlist/');
	 	}
	}
	function search_friend() {
		$this->check_user_page_access();
		$srch_plyr = $this->input->post('srch_plyr');
		$result = $this->General_model->find_friendlist($_SESSION['user_id'],$srch_plyr);
		$arr_sent_friend_request = array_values(array_unique(array_column($result, 'friend_id')));
		$sent_friend_request = $this->General_model->remove_element($arr_sent_friend_request,'0_3');
		if ($result) {
			foreach ($result as $key => $val)  {
				if ($val->user_id != 0 ) {
					
					if (in_array($val->user_id.'_1', $sent_friend_request)) {
						$btn = '<span class="request_sent"> Request Sent <i class="fa fa-check"></i></span>';
					} else if (in_array($val->user_id.'_0', $sent_friend_request)) {
						$btn = '<span class="request_sent"> Already Friend </span>';
					} else {
						$btn = '<input type="submit" class="add_friend_button" value="Add Friend" onclick="return confirm('."'".'Are you sure you want to add '.$name.' as a Friend in your Friend List ? ?'."'".')">';
					}
					$name = $val->display_name_status == 1 ? $val->display_name : $val->name;
					$srch_plyr_list[] = '<li><div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
				          <img src="'.base_url().'/upload/profile_img/'.$val->image.'" alt="">
				        </div>
				        <div class="col-lg-3 col-sm-6 col-md-3 bid-info">
				          <h3>'.$name.'</h3>                                                
				            <span class="postby"><a href=""><a href="'.base_url().'friend/friend_profile/params?user_id='.$val->user_id.'" class="view_profile">Profile</a></span> 
				        </div>
				        <div class="col-lg-4 col-sm-3 col-md-2 bid-info team_info"><p class="font_big">Esports Team</p><h3>'.$val->team_name.'</h3></div>
				        <div class="col-lg-3 col-sm-5 col-md-3 primum-play play-n-rating">
				        <form role="form" action="'.base_url().'friend/add_friend/" name="addToFriendForm" class="add-to-friend-form has-validation-callback" method="POST" enctype="multipart/form-data">
				        	'.$btn.'
				        	<textarea style="display: none;" name="mail">'.$val->name.' sent you friend request on '.base_url().'.</textarea>
				            <input type="hidden" name="user_to" value="'.$val->user_id.'">
				            </form>
				        </div>
				    </li>';
				}
			}
		} else {
			$srch_plyr_list = '<h5 class="no_payers_availble" style="color: #FF5000;"> Friend Not Available</h5>';
		}
		$res['srch_plyr_list'] = $srch_plyr_list;
		echo json_encode($res);
		exit();
	}

	function search_pending_request() {
		$this->check_user_page_access();
		$srch_pending_plyr = $this->input->post('srch_pending_plyr');
		$result = $this->General_model->pendingrequest_friendlist($_SESSION['user_id'],$srch_pending_plyr);
        foreach ($result as $key => $value) {
        	$name = $value['name'];
        	$team_name = '<h3>'.$value['team_name'].'</h3>';
        	if ($value['display_name_status'] == 1) {
        		$name = $value['display_name'];
        	}
			$srch_pending_plyr_list[] = '<li><div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
                  <img src="'.base_url().'/upload/profile_img/'.$value['image'].'" alt="">
                </div>
                <div class="col-lg-3 col-sm-6 col-md-3 bid-info">
				<h3>'.$name.'</h3>											
                    <span class="postby"><a href=""><a href="'.base_url().'friend/friend_profile/params?user_id='.$value['friend_id'].'" class="view_profile">Profile</a></span> 
                </div>
                <div class="col-lg-4 col-sm-3 col-md-2 bid-info team_info"><p class="font_big">Esports Team</p><h3>'.$team_name.'</h3></div>
                <div class="col-lg-3 col-sm-5 col-md-3 primum-play play-n-rating">
                <a class="add_friend_button" href="'.base_url().'friend/accept_the_request/'.$_SESSION["user_id"].'/'.$value['friend_id'].'" onclick="return confirm('."'".'Are you sure want to add ?'."'".')"> Accept</a>
                  <a class="add_friend_button" onclick="return confirm('."'".'Are you sure want to Remove ?'."'".')" href="'.base_url().'friend/remove_the_request/'.$_SESSION["user_id"].'/'.$value['friend_id'].'" style=" margin-left:10px;"> Remove</a>
                    
                </div>
            </li>';
		}
		$res['srch_pending_plyr_list'] = $srch_pending_plyr_list;
		echo json_encode($res);
	}
	function search_my_friend()
    {
		$this->check_user_page_access();
		$srch_plyr = $this->input->post('srch_plyr');
		$res = $this->General_model->friendlist($_SESSION['user_id'],$srch_plyr);
        foreach ($res as $key => $value) {
        	$name = $value['name'];
        	$team_name = '<h3>'.$value['team_name'].'</h3>';
        	if ($value['display_name_status'] == 1) {
        		$name = $value['display_name'];
        	}
			$srch_plyr_list[] = '<li class="frind_row_li" id="frind_row'.$value['friend_id'].'"><div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
				<img src="'.base_url().'/upload/profile_img/'.$value['image'].'" alt="">
                </div>
                <div class="col-lg-3 col-sm-6 col-md-3 bid-info">
                  <h3>'.$name.'</h3>                                                
                    <span class="postby"><a href="'.base_url().'friend/friend_profile/params?user_id='.$value['user_id'].'" class="view_profile">Profile</a><a href="'.base_url().'mail/?friend_id='.$value['friend_id'].'"><i class="fa fa-envelope-o"></i></a></span> 
                </div>
                <div class="col-lg-4 col-sm-3 col-md-2 bid-info team_info"><p class="font_big">Esports Team</p><h3>'.$team_name.'</h3><p class="friends_chat"><a target="_blank" href="'.base_url().'chat/friends_chat_box/?friend_id='.$value['friend_id'].'"><i class="fa fa-comments-o"></i></a></p>
                </div>
                <div class="col-lg-3 col-sm-5 col-md-3 primum-play play-n-rating">
                  <a href="">Challenge friend</a>
                  <span class="glyphicon glyphicon-minus" onclick="remove_friend('.$value['user_id'].','.$value['friend_id'].');"></span>
                    
                </div>
            </li>';
		}
		$res['srch_plyr_list'] = $srch_plyr_list;
		echo json_encode($res);
	}
	public function remove_friend() {
		$this->check_user_page_access();
		$res['result']=$this->General_model->delete_data('friend_list',array('user_id'=>$_POST['user_id'], 'friend_id'=>$_POST['friend_id'], 'request'=>0));
		$res['result'].=$this->General_model->delete_data('friend_list',array('friend_id'=>$_POST['user_id'], 'user_id'=>$_POST['friend_id'], 'request'=>0));
		$this->session->set_flashdata('message_succ', "Friend Deleted.");
		echo json_encode($res);
		exit();
	}

}

