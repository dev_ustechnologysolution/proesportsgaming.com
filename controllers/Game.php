<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Game extends My_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation'));
        $this->target_dir=BASEPATH.'../upload/game/';
        $this->load->model('User_model');	
     }
	
    public function index(){
    	$this->check_user_page_access();
		$data['active']='game_list';
        $data['list']=$this->General_model->view_data('game',array('user_id'=>$this->session->userdata('user_id')));
        $content=$this->load->view('dashboard/newgame.tpl.php',$data,true);
        $this->render($content);
    }
	
    public function newgame() {
		if($this->session->userdata('user_id') == ''){
            header('location:'.base_url().'login');
            exit;
        }
		$data['active']='new_game';
		$data['banner_data'] = $this->General_model->tab_banner();
		$content=$this->load->view('game/newgame.tpl.php',$data,true);
        $this->render($content);
    }
	
    public function gameAdd(){
    	if($this->session->userdata('user_id') == ''){
            header('location:'.base_url().'login');
            exit;
        }
		$data['active']='game';
		$data['list1']=$this->General_model->playing_game_list();
		$data['list']=$this->General_model->open_challenge_game_list();
		$data['challenge_personal_profile'] = $this->General_model->friend_profile($_GET['personal_challenge_friend_id']);
		$total_game_count	=	count($data['list']) + count($data['list1']);
		if($total_game_count < 2){
			
		} else {
			$this->session->set_flashdata('message_err',"Already Reached Limit of creating or joining more than 2 games");
			header('location:'.base_url().'dashboard');
		}
		if ($_SESSION['total_balance'] <= 0) {
            $this->session->set_flashdata('message_err', "You do not have enough credits to place a challenge. Please Proceed to Purchase tab for more.");
            header('location:' . base_url() . 'points/buypoints');
            exit();
        }
        $data['game_category']=$this->General_model->game_system_list();
        // $data['game_category']=$this->General_model->view_all_data('admin_game_category','id','asc');
        $data['banner_data'] = $this->General_model->tab_banner();
        $content=$this->load->view('game/game_create.tpl.php',$data,true);
        $this->render($content);
	}
	
    public function gameCreate() {
    	$this->check_user_page_access();
		if(is_null($this->input->post('device_number', TRUE)) || $this->input->post('device_number', TRUE) == ''){
            $this->session->set_flashdata('required_msg',"Please enter PSN/XBOX/PC ID Or Nickname!");
            header('location:'.base_url().'game/gameadd');
        }
        else
        {
        	$points = $this->General_model->view_data('user_detail',array('user_id'=>$_SESSION['user_id']));
			$game_timer = $this->input->post('game_timer', TRUE);
			if($game_timer){
				switch($game_timer){
					case '15':
					case '30':
					case '45':
							$endTime = strtotime("+".$game_timer ." minutes", time());
							$endTime = date('Y-m-d H:i:s', $endTime);
							break;
					case '1':
							$endTime = strtotime("+".$game_timer ." hour", time());
							$endTime = date('Y-m-d H:i:s', $endTime);
							break;
					case 'no':
							$endTime = '';
							break;
				}				
			}
			if (isset($_POST['personal_challenge_friend_id']) && $_POST['personal_challenge_friend_id'] != '') {
				if($_POST['custom_game'] == '') {
					$game_new_data=array(
		                'user_id'			=>$_SESSION['user_id'],
		                'game_category_id'	=>$this->input->post('game_category', TRUE),
		                'game_id'			=>$this->input->post('game_name', TRUE),
		                'sub_game_id'		=>$this->input->post('subgame_name_id', TRUE),
		                'game_type'			=>$this->input->post('game_type', TRUE),
		                'game_timer'		=>$endTime,
		                'device_id'			=>$this->input->post('device_number', TRUE),
		                'image_id'			=>$this->input->post('game_image_id', TRUE),
		                'price'				=>$this->input->post('bet_amount', TRUE),
		                'game_password'		=>$this->input->post('game_password', TRUE),
		                'best_of'			=>$this->input->post('best_of', TRUE),
		                'created_at'		=>date('Y-m-d'),
		                'personal_challenge'=>"yes",
		                'challenge_friend_id'=>$this->input->post('personal_challenge_friend_id', TRUE),
		                'description'		=>$this->input->post('description', TRUE)
	                );
	                $check_mail_categories = $this->User_model->check_mail_categories($_POST['personal_challenge_friend_id']);
					$sender_mail = $this->General_model->view_data('user',array('id'=>$this->session->userdata('user_id')));
					$sender_mail_name = $this->General_model->view_data('user_detail',array('user_id'=>$this->session->userdata('user_id')));
					$images_array = '';
					$mail['mails_data'] = 'You have new challenge from "'.$sender_mail_name[0]['name'].'"';
					$mail['email'] = $sender_mail[0]['email'];
					$mail['sender_mail_name'] = $sender_mail_name[0]['name'];
					foreach ($check_mail_categories as $key => $value) {
						if ($value['challenge_mail']=='yes') {
							$sub = 'Challenge Mail';
							$send_personal_mail=$this->User_model->send_mail_to_friend($sender_mail[0]['email'],$value['email'],$mail,$sub);
						}
					}
					$mail_data = array(
					'mail'=>$mail,
					'attachment'=>$images_array,
					'status'=>'1'
					);	
					$r=$this->General_model->insert_data('mail_data',$mail_data);
			    	$data_out=array(
								'user_id'=>$_SESSION['user_id'],
								'user_to'=>$_POST['personal_challenge_friend_id'],
								'user_from'=>$_SESSION['user_id'],
								'direction'=>'OUT',
								'mail_type'=>'challenge_mail',
								'status'=>'0',
								'date'=>date('Y-m-d h:i:s'),
								'message_id'=>$r
								);	

			    	$data_in=array(
								'user_id'=>$_POST['personal_challenge_friend_id'],
								'user_to'=>$_SESSION['user_id'],
								'user_from'=>$_SESSION['user_id'],
								'direction'=>'IN',
								'mail_type'=>'challenge_mail',
								'status'=>'0',
								'date'=>date('Y-m-d h:i:s'),
								'message_id'=>$r
								);	

			    	$r.=$this->General_model->insert_data('mail_conversation',$data_out);
			    	$r.=$this->General_model->insert_data('mail_conversation',$data_in);
					$percentage = $this->General_model->view_all_data('admin_points_percentage','id','asc');
					$lessthan5_total 		= $percentage[0]['lessthan5'];	
					$morethan5_25_total 	= $percentage[0]['morethan5_25'];	
					$morethan25_50_total 	= $percentage[0]['morethan25_50'];	
					$morethan50_total		= $percentage[0]['morethan50'];	
					if($game_new_data['price'] < 5){
						$price_add = $game_new_data['price'] + $lessthan5_total;
					}else if($game_new_data['price'] >= 5 && $game_new_data['price'] < 25){
						$price_add = $game_new_data['price'] + $morethan5_25_total;
					} else if($game_new_data['price'] >= 25 && $game_new_data['price'] < 50){
						$price_add = $game_new_data['price'] + $morethan25_50_total;
					} else if($game_new_data['price'] >= 50){
						$price_add = $game_new_data['price'] + $morethan50_total;
					}
					$price_add_decimal = number_format((float)$price_add, 2, '.', '');
				
		            if((float)$price_add_decimal > (float)$points[0]['total_balance']) {
		                $this->session->set_flashdata('msg',"You do not have enough credits to place a challenge. Please Proceed to Purchase tab for more.");
		                header('location:'.base_url().'game/gameadd');
		            }
		            else
		            {
		                $rem_points = $points[0]['total_balance'] - $price_add_decimal;
					    $update_data=array(
		                    'total_balance'=> number_format((float)$rem_points, 2, '.', ''),
		                );
						$this->General_model->update_data('user_detail',$update_data, array('user_id'=>$this->session->userdata('user_id')));
						$rdata=$this->General_model->insert_data('game',$game_new_data);	                
						if($rdata)
		                {
		                    $game_new_data['gameImage'] = $this->General_model->view_data('game_image',array('id'=>$this->input->post('game_image_id', TRUE)));
		                    $game_new_data['game_name'] = $this->General_model->view_data('admin_game',array('id'=>$this->input->post('game_name', TRUE)));
		                    $session_data = array(
		                        'game_name'=>$game_new_data['game_name'][0]['game_name'],
		                        'game_image'=>$game_new_data['gameImage'][0]['game_image'],
		                        'game_price'=>$this->input->post('bet_amount', TRUE),
		                        'game_id'=>$this->input->post('game_name', TRUE),
		                        'sgame_id'=>$this->input->post('subgame_name_id', TRUE)
		                        );
		                    $this->session->set_userdata($session_data);
		                    $this->session->set_userdata('message_succ', 'Successfully added');

		                    $bet_data = array(
		                        'game_id' 		=> $this->session->userdata('game_id'),
		                        'game_sub_id' 	=> $this->session->userdata('sgame_id'),
		                        'challenger_id' => $this->session->userdata('user_id')
							);
		                    $bet_id = $this->General_model->insert_data('bet',$bet_data);

		                    $condition = "1 AND `game_id`='".$this->session->userdata('game_id')."' AND (`status`=1 OR `status`=0)";
		                    $arr_game_id = $this->General_model->view_data('game',$condition);
		                    $dt=array(
		                        'payment_status'=>1,
		                        'status'=>1,
		                        'bet_id'=>$bet_id
		                        );
		                    $this->General_model->update_data('game', $dt , array('id'=>$rdata));
		                    header('location:'.base_url().'dashboard');
		                } else {
		                	$this->session->set_userdata('message_err', 'Failure to add');
		                    header('location:'.base_url().'game/gameadd');
		                }
		            }
				}
				else {
					$add_game_category = array(
						'category_name'=>$_POST['game_category'],
						'cat_slug'=>$_POST['game_category'],
						'custom'=>'yes'
					);
					$add_game_category_id = $this->General_model->insert_data('admin_game_category',$add_game_category);

					$game_type='';
			        foreach($_POST['game_type'] as $r=>$w)
			        {
			            $game_type .=$w.'|';
			        }
					$add_admin_game = array(
						'game_name'=>$_POST['game_name'],
						'game_description'=>$_POST['game_name'],
						'game_category_id'=>$add_game_category_id,
						'created_at'=>date('Y-m-d h:i:s'),
						'game_type'=>$game_type,
						'custom'=>'yes'
					);
					$add_admin_game_id = $this->General_model->insert_data('admin_game',$add_admin_game);

					$add_admin_sub_game = array(
						'game_id'=>$add_admin_game_id,
						'sub_game_name'=>$_POST['subgame_name_id'],
						'custom'=>'yes'
					);
					$add_admin_sub_game_id = $this->General_model->insert_data('admin_sub_game',$add_admin_sub_game);

					$img='';
                    if(isset($_FILES["game_image"]["name"]) && $_FILES["game_image"]["name"]!='')
                    {
                        $img=time().basename($_FILES["game_image"]["name"]);
                        $ext = end((explode(".", $img)));
                        if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg' || $ext=='GIF' || $ext=='JPEG' || $ext=='PNG' || $ext=='JPG')
                        {
                            $target_file = $this->target_dir.$img;
                            move_uploaded_file($_FILES["game_image"]["tmp_name"], $target_file);
                            $add_admin_game_image = array(
								'game_id'=>$add_admin_game_id,
								'game_image'=>$img,
								'custom'=>'yes'
							);	
                            $add_admin_game_image_id = $this->General_model->insert_data('game_image',$add_admin_game_image);
                        }
                    }
	                			
					$game_new_data=array(
	                'user_id'			=>$_SESSION['user_id'],
	                'game_category_id'	=>$add_game_category_id,
	                'game_id'			=>$add_admin_game_id,
	                'sub_game_id'		=>$add_admin_sub_game_id,
	                'game_type'			=>$this->input->post('game_type', TRUE),
	                'game_timer'		=>$endTime,
	                'device_id'			=>$this->input->post('device_number', TRUE),
	                'image_id'			=>$add_admin_game_image_id,
	                'price'				=>$this->input->post('bet_amount', TRUE),
	                'best_of'			=>$this->input->post('best_of', TRUE),
	                'game_password'		=>$this->input->post('game_password', TRUE),
	                'created_at'		=>date('Y-m-d'),
	                'description'		=>$this->input->post('description', TRUE),
	                'personal_challenge'=>"yes",
	                'challenge_friend_id'=>$this->input->post('personal_challenge_friend_id', TRUE)
	                );
				
					$check_mail_categories = $this->User_model->check_mail_categories($_POST['personal_challenge_friend_id']);
					$sender_mail = $this->General_model->view_data('user',array('id'=>$this->session->userdata('user_id')));
					$sender_mail_name = $this->General_model->view_data('user_detail',array('user_id'=>$this->session->userdata('user_id')));
					$images_array = '';
					$mail = 'You have new challenge from "'.$sender_mail_name[0]['name'].'"';
					foreach ($check_mail_categories as $key => $value) {
						if ($value['challenge_mail']=='yes') {
							$sub = 'Challenge Mail';
							$send_personal_mail=$this->User_model->send_mail_to_friend($sender_mail[0]['email'],$value['email'],$mail,$sub);
						}
					}
					$mail_data = array(
					'mail'=>$mail,
					'attachment'=>$images_array,
					'status'=>'1'
					);	
					$r=$this->General_model->insert_data('mail_data',$mail_data);

			    	$data_out=array(
								'user_id'=>$_SESSION['user_id'],
								'user_to'=>$_POST['personal_challenge_friend_id'],
								'user_from'=>$_SESSION['user_id'],
								'direction'=>'OUT',
								'mail_type'=>'challenge_mail',
								'status'=>'0',
								'date'=>date('Y-m-d h:i:s'),
								'message_id'=>$r
								);	

			    	$data_in=array(
								'user_id'=>$_POST['personal_challenge_friend_id'],
								'user_to'=>$_SESSION['user_id'],
								'user_from'=>$_SESSION['user_id'],
								'direction'=>'IN',
								'mail_type'=>'challenge_mail',
								'status'=>'0',
								'date'=>date('Y-m-d h:i:s'),
								'message_id'=>$r
								);	

			    	$r.=$this->General_model->insert_data('mail_conversation',$data_out);
			    	$r.=$this->General_model->insert_data('mail_conversation',$data_in);
					$percentage = $this->General_model->view_all_data('admin_points_percentage','id','asc');
					$lessthan5_total 		= $percentage[0]['lessthan5'];	
					$morethan5_25_total 	= $percentage[0]['morethan5_25'];	
					$morethan25_50_total 	= $percentage[0]['morethan25_50'];	
					$morethan50_total		= $percentage[0]['morethan50'];	
					
					if($game_new_data['price'] < 5) {
						$price_add = $game_new_data['price'] + $lessthan5_total;
					} else if($game_new_data['price'] >= 5 && $game_new_data['price'] < 25) {
						$price_add = $game_new_data['price'] + $morethan5_25_total;
					} else if($game_new_data['price'] >= 25 && $game_new_data['price'] < 50) {
						$price_add = $game_new_data['price'] + $morethan25_50_total;
					} else if($game_new_data['price'] >= 50) {
						$price_add = $game_new_data['price'] + $morethan50_total;
					}
					$price_add_decimal = number_format((float)$price_add, 2, '.', '');
				
		            if((float)$price_add_decimal > (float)$points[0]['total_balance']){
		                $this->session->set_flashdata('msg',"You do not have enough credits to place a challenge. Please Proceed to Purchase tab for more.");
		                header('location:'.base_url().'game/gameadd');
		            }
		            else
		            {
		                $rem_points = $points[0]['total_balance'] - $price_add_decimal;
					    $update_data=array(
		                    'total_balance'=> number_format((float)$rem_points, 2, '.', ''),
		                );
						$this->General_model->update_data('user_detail',$update_data, array('user_id'=>$this->session->userdata('user_id')));
						$rdata=$this->General_model->insert_data('game',$game_new_data);	                
						if($rdata)
		                {
		                    $game_new_data['gameImage'] = $this->General_model->view_data('game_image',array('id'=>$add_admin_game_image_id));
		                    $game_new_data['game_name'] = $this->General_model->view_data('admin_game',array('id'=>$add_admin_game_id));
		                    $session_data = array(
		                        'game_name'=>$game_new_data['game_name'][0]['game_name'],
		                        'game_image'=>$game_new_data['gameImage'][0]['game_image'],
		                        'game_price'=>$this->input->post('bet_amount', TRUE),
		                        'game_id'=>$add_admin_game_id,
		                        'sgame_id'=>$add_admin_sub_game_id
		                        );
		                    $this->session->set_userdata($session_data);
		                    $this->session->set_userdata('message_succ', 'Successfully added');

		                    $bet_data = array(
		                        'game_id' 		=> $this->session->userdata('game_id'),
		                        'game_sub_id' 	=> $this->session->userdata('sgame_id'),
		                        'challenger_id' => $this->session->userdata('user_id'),
							);
		                    $bet_id = $this->General_model->insert_data('bet',$bet_data);
		                    $condition = "1 AND `game_id`='".$this->session->userdata('game_id')."' AND (`status`=1 OR `status`=0)";
		                    $arr_game_id = $this->General_model->view_data('game',$condition);
		                    $dt=array(
		                        'payment_status'=>1,
		                        'status'=>1,
		                        'bet_id'=>$bet_id
		                        );
		                    $this->General_model->update_data('game', $dt, array('id'=>$rdata));
		                    header('location:'.base_url().'dashboard');
		                }
		                else
		                {
		                	$this->session->set_userdata('message_err', 'Failure to add');
		                    header('location:'.base_url().'game/gameadd');
		                }
		            }
	            }
	        }
			else
			{
				$data=array(
                'user_id'			=>$_SESSION['user_id'],
                'game_category_id'	=>$this->input->post('game_category', TRUE),
                'game_id'			=>$this->input->post('game_name', TRUE),
                'sub_game_id'		=>$this->input->post('subgame_name_id', TRUE),
                'game_type'			=>$this->input->post('game_type', TRUE),
                'game_timer'		=>$endTime,
                'device_id'			=>$this->input->post('device_number', TRUE),
                'image_id'			=>$this->input->post('game_image_id', TRUE),
                'price'				=>$this->input->post('bet_amount', TRUE),
                'best_of'			=>$this->input->post('best_of', TRUE),
                'game_password'		=>$this->input->post('game_password', TRUE),
                'created_at'		=>date('Y-m-d'),
                'description'		=>$this->input->post('description', TRUE)
                );
			
				$percentage = $this->General_model->view_all_data('admin_points_percentage','id','asc');                        
				$lessthan5_total 		= $percentage[0]['lessthan5'];	
				$morethan5_25_total 	= $percentage[0]['morethan5_25'];	
				$morethan25_50_total 	= $percentage[0]['morethan25_50'];	
				$morethan50_total		= $percentage[0]['morethan50'];	
				
				if($data['price'] < 5){
					$price_add = $data['price'] + $lessthan5_total;
				}else if($data['price'] >= 5 && $data['price'] < 25){
					$price_add = $data['price'] + $morethan5_25_total;
				} else if($data['price'] >= 25 && $data['price'] < 50){
					$price_add = $data['price'] + $morethan25_50_total;
				} else if($data['price'] >= 50){
					$price_add = $data['price'] + $morethan50_total;
				}
				$price_add_decimal = number_format((float)$price_add, 2, '.', '');
			
	            if((float)$price_add_decimal > (float)$points[0]['total_balance'])
	            {
	                $this->session->set_flashdata('msg',"You do not have enough credits to place a challenge. Please Proceed to Purchase tab for more.");
	                header('location:'.base_url().'game/gameadd');
	            }
	            else
	            {
	                $rem_points = $points[0]['total_balance'] - $price_add_decimal;
				    $update_data=array(
	                    'total_balance'=> number_format((float)$rem_points, 2, '.', ''),
	                );
					$this->General_model->update_data('user_detail',$update_data, array('user_id'=>$this->session->userdata('user_id')));
	                $r=$this->General_model->insert_data('game',$data);
	                if($r)
	                {
	                    $data['gameImage'] = $this->General_model->view_data('game_image',array('id'=>$this->input->post('game_image_id', TRUE)));
	                    $data['game_name'] = $this->General_model->view_data('admin_game',array('id'=>$this->input->post('game_name', TRUE)));
	                    $session_data = array(
	                        'game_name'=>$data['game_name'][0]['game_name'],
	                        'game_image'=>$data['gameImage'][0]['game_image'],
	                        'game_price'=>$this->input->post('bet_amount', TRUE),
	                        'game_id'=>$this->input->post('game_name', TRUE),
	                        'sgame_id'=>$this->input->post('subgame_name_id', TRUE)
	                        );
	                    $this->session->set_userdata($session_data);
	                    $this->session->set_userdata('message_succ', 'Successfully added');

	                    $bet_data = array(
	                        'game_id' 		=> $this->session->userdata('game_id'),
	                        'game_sub_id' 	=> $this->session->userdata('sgame_id'),
	                        'challenger_id' => $this->session->userdata('user_id')
						);
	                    $bet_id = $this->General_model->insert_data('bet',$bet_data);

	                    $condition = "1 AND `game_id`='".$this->session->userdata('game_id')."' AND (`status`=1 OR `status`=0)";
	                    $arr_game_id = $this->General_model->view_data('game',$condition);
	                    $dt=array(
	                        'payment_status'=>1,
	                        'status'=>1,
	                        'bet_id'=>$bet_id
	                        );
	                    $this->General_model->update_data('game', $dt , array('id'=>$r));
	                    header('location:'.base_url().'dashboard');
	                }
	                else
	                {
	                    $this->session->set_userdata('message_err', 'Failure to add');
	                    header('location:'.base_url().'game/gameadd');
	                }
            	}
            }
    	}
    }
    //load edit page
    public function gameurlEdit($id='-1', $status) {
    	$this->check_user_page_access();
    	$data['banner_data'] = $this->General_model->tab_banner();
		if($status == 'won'){
			$status_result = '1';
		} else if($status == 'lose'){
			$status_result = '0';
		} else if($status == 'admin'){
			$status_result = '2';
		}
		
		$check_user_won_lose_info	=	$this->General_model->check_user_won_lose_info($id);
		
		
		if(!empty($check_user_won_lose_info)){
			
			$listuser 	= explode(',',$check_user_won_lose_info->bet_user_id);
			$liststatus = explode(',',$check_user_won_lose_info->v_status);
			$listvideo 	= explode(',',$check_user_won_lose_info->v_video_id);

			
			if(count($listuser) == 2){
				
			} else {

				if($listuser[0] != ''){
					if($liststatus[0] == '1' && $status_result == '1' ){ // 1- won 2-won
						$this->session->set_flashdata('message_err', 'Error Player has already reported Won.');
						$this->session->set_flashdata('message_err_1', 'won_'.$id);
						header('location:'.base_url().'dashboard');
						//error message
					} else if($liststatus[0] == '0' && $status_result == '0' ){ // both lose
						$this->session->set_flashdata('message_err', 'Error Player has already reported Loss.');
						$this->session->set_flashdata('message_err_1', 'lose_'.$id);
						header('location:'.base_url().'dashboard');
						
						//echo 'Both Same';
					} else if($liststatus[0] == '2' && $status_result == '0' ){ // 1- admin 2-lose
						
					} else if($liststatus[0] == '2' && $status_result == '1' ){ // 1- admin 2-won
						
					} else {
						
					}
						
				}
			}		
		}
		
        $data['active']='game_edit';
        $data['game']=$this->General_model->view_data('game',array('id'=>$id));
		
		if($status == 'lose') {			
		
			$check_user_won_lose_info	=	$this->General_model->check_user_won_lose_info($id);
				
			if(!empty($check_user_won_lose_info)){
				
				$listuser 	= explode(',',$check_user_won_lose_info->bet_user_id);
				$liststatus = explode(',',$check_user_won_lose_info->v_status);
				$listvideo 	= explode(',',$check_user_won_lose_info->v_video_id);
				if($liststatus[0] == '0' && $status_result == '0' ){ } else {
					if(count($listuser) == 1){
					
						$data=array(
							'game_tbl_id'	=>	$id,
							'game_id'		=>	$data['game'][0]['game_id'],
							'game_sub_id'	=>	$data['game'][0]['sub_game_id'],
							'video_url'		=>	'',
							'user_id'		=>	$this->session->userdata('user_id'),
							'created_at'	=>	date('Y-m-d'),
							'status'		=>	$status_result,
						);
						
						$r=$this->General_model->insert_data('video',$data);
						if($r){
							
							$check_user_won_lose_info	=	$this->General_model->check_user_won_lose_info($id);
							
							if(!empty($check_user_won_lose_info)){
									
								$listuser 	= explode(',',$check_user_won_lose_info->bet_user_id);
								$liststatus = explode(',',$check_user_won_lose_info->v_status);
								$listvideo 	= explode(',',$check_user_won_lose_info->v_video_id);
								if(count($listuser) == 2){	
									if($listuser[0] != '' && $listuser[1] != ''){
										
										if($liststatus[0] == '0' && $liststatus[1]  == '1'){
											
											$user_id_win 	= $listuser[1];
											$video_id		= $listvideo[1];
											$user_id_lose	= $listuser[0];
											
											$dt = array(
												'status'	=> 3,
												'video_id'	=> $video_id,
											);
											$this->General_model->update_data('game',$dt,array('id'=>$check_user_won_lose_info->game_tbl_id));						
											$arr_bet_id = $this->General_model->view_data('game',array('id'=>$check_user_won_lose_info->game_tbl_id));						
											$arr_bet_data = $this->General_model->view_data('bet',array('id'=>$arr_bet_id[0]['bet_id']));
											$data	=	array('winner_id'=>$user_id_win,'loser_id'=>$user_id_lose,'win_lose_deta'=>date('Y-m-d H:i:s',time()));
											$r		=	$this->General_model->update_data('bet',$data,array('id'=>$arr_bet_id[0]['bet_id']));
											
											
											$user_data_where = array(
												'user_id'	=> $user_id_win,
											);
											$userListDp = $this->General_model->view_data('user_detail',$user_data_where);
											foreach($userListDp as $userList){
												$user_where = array(
													'user_id'	=> $userList['user_id'],
												);
												$update_user = array(
													'total_balance'	=>  $userList['total_balance'] + (2*$arr_bet_id[0]['price']),
												);
												$this->General_model->update_data('user_detail',$update_user,$user_where);
											}
											
										} else  if($liststatus[0] == '1' && $liststatus[1]  == '0'){
											
											$user_id_win	= $listuser[0];
											$video_id		= $listvideo[0];
											$user_id_lose 	= $listuser[1];
											
											$dt = array(
												'status'	=> 3,
												'video_id'	=> $video_id,
											);
											$this->General_model->update_data('game',$dt,array('id'=>$check_user_won_lose_info->game_tbl_id));						
											$arr_bet_id = $this->General_model->view_data('game',array('id'=>$check_user_won_lose_info->game_tbl_id));						
											$arr_bet_data = $this->General_model->view_data('bet',array('id'=>$arr_bet_id[0]['bet_id']));
											$data	=	array('winner_id'=>$user_id_win,'loser_id'=>$user_id_lose,'win_lose_deta'=>date('Y-m-d H:i:s',time()));
											$r	=	$this->General_model->update_data('bet',$data,array('id'=>$arr_bet_id[0]['bet_id']));
											
											$user_data_where = array(
												'user_id'	=> $user_id_win,
											);
											$userListDp = $this->General_model->view_data('user_detail',$user_data_where);
											foreach($userListDp as $userList){
												$user_where = array(
													'user_id'	=> $userList['user_id'],
												);
												$update_user = array(
													'total_balance'	=>  $userList['total_balance'] + (2*$arr_bet_id[0]['price']),
												);
												$this->General_model->update_data('user_detail',$update_user,$user_where);
											}								
										} 					
									}
								}
							}
							
							$this->session->set_userdata('message_succ', 'Successfully Updated');
							header('location:'.base_url().'dashboard');
						}
					}
				}
			} else {
				$data=array(
					'game_tbl_id'	=>	$id,
					'game_id'		=>	$data['game'][0]['game_id'],
					'game_sub_id'	=>	$data['game'][0]['sub_game_id'],
					'video_url'		=>	'',
					'user_id'		=>	$this->session->userdata('user_id'),
					'created_at'	=>	date('Y-m-d'),
					'status'		=>	$status_result,
				);
				
				$r=$this->General_model->insert_data('video',$data);
				if($r){
					$this->session->set_userdata('message_succ', 'Successfully Updated');
					header('location:'.base_url().'dashboard');
				}
			}
		} else if($status == 'admin') {
			$data['game_cate']=$this->General_model->view_data('admin_game_category',array('id'=>$data['game'][0]['game_category_id']));
			$data['game_data']=$this->General_model->view_data('admin_game',array('id'=>$data['game'][0]['game_id']));
			$data['game_subname']=$this->General_model->view_data('admin_sub_game',array('game_id'=>$data['game'][0]['game_id']));
			$data['game_img']=$this->General_model->view_data('game_image',array('game_id'=>$data['game'][0]['game_id'],'id'=>$data['game'][0]['image_id']));
			$data['game_url']=$this->General_model->view_data('video',array('game_tbl_id'=>$id,'user_id'=>$this->session->userdata('user_id')));
			// echo '<pre>';
			// print_r($data['game_url']);exit;
			$data['gameid']=$id;
			$content=$this->load->view('game/game_edit1.tpl.php',$data,true);
			$this->render($content);
		} else if($status == 'won') {
			// echo '<pre>';
			// print_r($data['game']);exit;
			$data['game_cate']=$this->General_model->view_data('admin_game_category',array('id'=>$data['game'][0]['game_category_id']));
			$data['game_data']=$this->General_model->view_data('admin_game',array('id'=>$data['game'][0]['game_id']));
			$data['game_subname']=$this->General_model->view_data('admin_sub_game',array('game_id'=>$data['game'][0]['game_id']));
			$data['game_img']=$this->General_model->view_data('game_image',array('game_id'=>$data['game'][0]['game_id'],'id'=>$data['game'][0]['image_id']));
			$data['game_url']=$this->General_model->view_data('video',array('game_tbl_id'=>$id,'user_id'=>$this->session->userdata('user_id')));
			// echo '<pre>';
			// print_r($data['game_url']);exit;
			$data['gameid']=$id;
			$content=$this->load->view('game/game_edit.tpl.php',$data,true);
			$this->render($content);
		}
    }
    //edit
    public function gameUpdate() {
		$this->check_user_page_access();
		$data['active']='game_edit';
        $img=$this->input->post('old_image', TRUE);
        if(isset($_FILES["image"]["name"]) && $_FILES["image"]["name"]!='')
        {
            $img=time().basename($_FILES["image"]["name"]);
            $target_file = $this->target_dir.$img;
            if(move_uploaded_file($_FILES["image"]["tmp_name"], $target_file))
            {
                if(file_exists($this->target_dir.$this->input->post('old_image', TRUE)))
                {
                    unlink($this->target_dir.$this->input->post('old_image', TRUE));
                }
            }
        }
        $data=array(
            'name'=>$this->input->post('name', TRUE),
            'device_id'=>$this->input->post('device_number', TRUE),
            'description'=>$this->input->post('description', TRUE),
            'image'=>$img,
            'price'=>$this->input->post('price', TRUE),
            'updated_at'=>date('Y-m-d'),
            );
        $r=$this->General_model->update_data('game',$data,array('id'=>$this->input->post('id', TRUE)));
        if($r)
        {
            $this->session->set_userdata('message_succ', 'Update successfull');
            header('location:'.base_url().'game/gameEdit/'.$this->input->post('id', TRUE));
        }
        else
        {
            $this->session->set_userdata('message_err', 'Failure to update');
            header('location:'.base_url().'game/gameEdit/'.$this->input->post('id', TRUE));
        }
    }
    //delete
    public function delete($id){
    	$this->check_user_page_access();
		$r=$this->General_model->delete_data('game',array('id'=>$id));
        if($r) {
            $this->session->set_userdata('message_succ', 'Delete Successfull');
            header('location:'.base_url().'dashboard');
        } else {
            $this->session->set_userdata('message_err', 'Failure to delete');
            header('location:'.base_url().'dashboard');
        }
    }
	public function gamePassword($id) {
		$this->check_user_page_access();
		$data['list']=$this->General_model->challenger_game_details($id);
		$content=$this->load->view('game/game_password_page.tpl.php',$data,true);
        $this->render($content);
	}
	public function gamePasswordAuth() {
		$this->check_user_page_access();
		$get_row=$this->General_model->challenger_game_details($_POST['game_id']);
		if ($get_row[0]['game_password'] == $_POST['game_password']) {
			$res="yes";
			$_SESSION['current_game_pw'] = $_POST['game_password'];
		}
		else
		{
			$res="no";
		}
		echo $res;
	}
    public function gameDetails($id) {
    	$this->check_user_page_access();
    	$data['list']=$this->General_model->challenger_game_details($id);
    	if (isset($data['list'][0]['game_password'])) {
    		if ($data['list'][0]['game_password'] != $_SESSION['current_game_pw']) {
            	header('location:'.base_url().'home/gamePwAuth/'.$data['list'][0]['id']);
			}
    	}
        $data['user']=$this->General_model->view_data('user',array('id'=>$this->session->userdata('user_id')));
        $points = $this->General_model->view_data('user_detail',array('user_id'=>$this->session->userdata('user_id')));
        $data['total_balance'] = $points[0]['total_balance'];
        $content=$this->load->view('game/game_details_page.tpl.php',$data,true);
        $this->render($content);
    }
	
	public function duePayment($id) {
    	$this->check_user_page_access();
		$data['list']=$this->General_model->view_data('game',array('id'=>$id));
        $session_data   = array(
            'game_name'=>$data['list'][0]['name'],
            'game_image'=>$data['list'][0]['image'],
            'game_price'=>$data['list'][0]['price'],
            'game_id'=>$data['list'][0]['id'],
            );
        $this->session->set_userdata($session_data);
        header('location:'.base_url().'payment');
    }
	
    public function insertVideo(){
    	$this->check_user_page_access();
		if(array_key_exists("url",$_REQUEST)){
			if($this->input->post('url') == ''){
				$this->form_validation->set_rules('accept_risk', 'Please Check Your I Accept Risk Checked', 'required');
			} else {
				$this->form_validation->set_rules('url', 'url', 'required');
			}
		} else {
			$this->session->set_flashdata('message_video_error', 'Please add the video URL ');
			$this->session->set_flashdata('message_checkbox_error', 'Please Check I Accept Risk in Checkbox ');
			header('location:'.base_url().'game/gameurlEdit/'.$this->input->post('game_tbl_id'));
		}
		
		//$this->form_validation->set_rules('url[]', 'url', 'required');
		
		if($this->input->post('game_select') == 'won'){
			$status_result = '1';
		} else if($this->input->post('game_select') == 'lose'){
			$status_result = '0';
		} else if($this->input->post('game_select') == 'admin'){
			$status_result = '2';
		}
		
		
		if ($this->form_validation->run() == true) {
			
			if($this->General_model->get_num_rows('video',array('game_id'=>$this->uri->segment(3),'user_id'=>$this->session->userdata('user_id'),'game_tbl_id'=>$this->input->post('game_tbl_id')))){
				$data=array(
					'video_url'		=>	$this->input->post('url', TRUE),		
					'video_url1'	=>	$this->input->post('url1', TRUE),	
					'detail_admin'	=>	$this->input->post('details_admin', TRUE),
					'status'		=>	$status_result,
				);
				$r=$this->General_model->update_data('video',$data,array('game_id'=>$this->uri->segment(3),'user_id'=>$this->session->userdata('user_id'),'game_tbl_id'=>$this->input->post('game_tbl_id')));
				if($r) {
					$this->session->set_userdata('message_succ', 'Successfully Updated');
					//header('location:'.base_url().'game/gameurlEdit/'.$this->input->post('game_tbl_id').'/'.$this->input->post('game_select'));
					header('location:'.base_url().'dashboard');
				} else {
					$this->session->set_userdata('message_err', 'Failure to update');
					header('location:'.base_url().'game/gameurlEdit/'.$this->input->post('game_tbl_id').'/'.$this->input->post('game_select'));
				}
			} else {
				
				$data=array(
					'game_tbl_id'	=>	$this->input->post('game_tbl_id'),
					'game_id'		=>	$this->uri->segment(3),
					'game_sub_id'	=>	$this->input->post('sub_id', TRUE),
					'video_url'		=>	$this->input->post('url', TRUE),
					'video_url1'	=>	$this->input->post('url1', TRUE),
					'detail_admin'	=>	$this->input->post('details_admin', TRUE),
					'user_id'		=>	$this->session->userdata('user_id'),
					'created_at'	=>	date('Y-m-d'),
					'status'		=>	$status_result,
				);
					
				$r=$this->General_model->insert_data('video',$data);
				if($r) {
					
					$check_user_won_lose_info	=	$this->General_model->check_user_won_lose_info($this->input->post('game_tbl_id'));
		
					if(!empty($check_user_won_lose_info)){
						
						$listuser 	= explode(',',$check_user_won_lose_info->bet_user_id);
						$liststatus = explode(',',$check_user_won_lose_info->v_status);
						$listvideo 	= explode(',',$check_user_won_lose_info->v_video_id);
						if(count($listuser) == '2'){
						if($listuser[0] != '' && $listuser[1] != ''){
							
							if($liststatus[0] == '0' && $liststatus[1]  == '1'){
								
								$user_id_win 	= $listuser[1];
								$video_id		= $listvideo[1];
								$user_id_lose	= $listuser[0];
								
								$dt = array(
									'status'	=> 3,
									'video_id'	=> $video_id,
								);
								$this->General_model->update_data('game',$dt,array('id'=>$check_user_won_lose_info->game_tbl_id));						
								$arr_bet_id = $this->General_model->view_data('game',array('id'=>$check_user_won_lose_info->game_tbl_id));						
								$arr_bet_data = $this->General_model->view_data('bet',array('id'=>$arr_bet_id[0]['bet_id']));
								$data	=	array('winner_id'=>$user_id_win,'loser_id'=>$user_id_lose,'win_lose_deta'=>date('Y-m-d H:i:s',time()));
								$r		=	$this->General_model->update_data('bet',$data,array('id'=>$arr_bet_id[0]['bet_id']));
								
								
								$user_data_where = array(
									'user_id'	=> $user_id_win,
								);
								
								$userListDp = $this->General_model->view_data('user_detail',$user_data_where);
								foreach($userListDp as $userList){
									$user_where = array(
										'user_id'	=> $userList['user_id'],
									);
									$update_user = array(
										'total_balance'	=>  $userList['total_balance'] + (2*$arr_bet_id[0]['price']),
									);
									$this->General_model->update_data('user_detail',$update_user,$user_where);
																			
								
								}
								$user_data_whe = array(
									'id'	=> $user_id_win,
								);
								$userList_Details = $this->General_model->view_data('user',$user_data_whe);									
								$this->winner_email($userList_Details[0]['email']);
								
							} else  if($liststatus[0] == '1' && $liststatus[1]  == '0'){
								
								$user_id_win	= $listuser[0];
								$video_id		= $listvideo[0];
								$user_id_lose 	= $listuser[1];
								
								$dt = array(
									'status'	=> 3,
									'video_id'	=> $video_id,
								);
								$this->General_model->update_data('game',$dt,array('id'=>$check_user_won_lose_info->game_tbl_id));						
								$arr_bet_id = $this->General_model->view_data('game',array('id'=>$check_user_won_lose_info->game_tbl_id));						
								$arr_bet_data = $this->General_model->view_data('bet',array('id'=>$arr_bet_id[0]['bet_id']));
								$data	=	array('winner_id'=>$user_id_win,'loser_id'=>$user_id_lose,'win_lose_deta'=>date('Y-m-d H:i:s',time()));
								$r		=	$this->General_model->update_data('bet',$data,array('id'=>$arr_bet_id[0]['bet_id']));
								
								$user_data_where = array(
									'user_id'	=> $user_id_win,
								);
								$userListDp = $this->General_model->view_data('user_detail',$user_data_where);
								foreach($userListDp as $userList){
									$user_where = array(
										'user_id'	=> $userList['user_id'],
									);
									$update_user = array(
										'total_balance'	=>  $userList['total_balance'] + (2*$arr_bet_id[0]['price']),
									);
									$this->General_model->update_data('user_detail',$update_user,$user_where);
								}	
								$user_data_whe = array(
									'id'	=> $user_id_win,
								);
								$userList_Details = $this->General_model->view_data('user',$user_data_whe);									
								$this->winner_email($userList_Details[0]['email']);
							} else {
								//echo 'Both Same';
							} 					
						}
						}
					
					}
					
					$this->session->set_userdata('message_succ', 'Successfully added');
					//header('location:'.base_url().'game/gameurlEdit/'.$this->input->post('game_tbl_id'));
					header('location:'.base_url().'dashboard');
				} else {
					//$this->session->set_userdata('message_err', 'Failure to add');
					header('location:'.base_url().'game/gameurlEdit/'.$this->input->post('game_tbl_id').'/'.$this->input->post('game_select'));
				}
			}
		} else {
			if($this->input->post('url', TRUE) != '' && $this->input->post('accept_risk', TRUE) != ''){
				$this->session->set_flashdata('message_video_error', 'Please add the video URL ');
				$this->session->set_flashdata('message_checkbox_error', 'Please Check I Accept Risk in Checkbox ');				
			} else if($this->input->post('url', TRUE) != '' ) {
				$this->session->set_flashdata('message_video_error', 'Please add the video URL ');
			} else if($this->input->post('accept_risk', TRUE) != ''){
				$this->session->set_flashdata('message_checkbox_error', 'Please Check I Accept Risk in Checkbox ');
			}			
			header('location:'.base_url().'game/gameurlEdit/'.$this->input->post('game_tbl_id').'/'.$this->input->post('game_select'));
		}
    }
	
    public function playgame() {
    	$this->check_user_page_access();
		$data['dt']=1;
        $content=$this->load->view('play/playgame.tpl.php',$data,true);
        $this->render($content);
    }
    // for getting game from databse using ajax
    public function get_game(){
    	$this->check_user_page_access();
		$cat_id=$this->input->post('game_category_id');
        $res=$this->General_model->get_game_view_data('admin_game',array('game_category_id'=>$cat_id,'play_status'=>0,'custom'=>null,'is_category_active'=>1));
		echo json_encode($res);
    }

	public function search_matches(){
	    $system = $this->input->post('system');
		$game_name = $this->input->post('game_name');
	    $sub_game_name = $this->input->post('sub_game_name');
	    $game_size_var = $this->input->post('game_size');
	  	$srch_term = $this->input->post('srch_term');
	  	
  		if ($system != '' && $game_size_var == '') {
			$this->db->select('ag.id,ag.game_name');
			$this->db->from('game g');
			$this->db->join('admin_game ag','ag.id = g.game_id');
			$this->db->where("g.payment_status='1' AND g.status='1'");
			$this->db->where("g.game_category_id = '".$system."'");
			$this->db->group_by('ag.game_name');
			$this->db->limit(0,5);
			$query_game = $this->db->get();

			foreach ($query_game->result() as $key => $val) {
	        	$game_list[] = '<option class="game_list_front_side" value="'.$val->id.'">'.$val->game_name.'</option>';
	        	$game_list_id[] = $val->id;
	        	$game_list_name[] = $val->game_name;
	        }
		}

		if ($game_size_var != '' && $system == '') {
			$this->db->select('ag.id,ag.game_name');
			$this->db->from('game g');
			$this->db->join('admin_game ag','ag.id = g.game_id');
			$this->db->where("g.payment_status='1' AND g.status='1'");
			$this->db->where("g.game_type = '".$game_size_var."'");
			$this->db->group_by('ag.game_name');
			$this->db->limit(0,5);
	        $query_game = $this->db->get();
            foreach ($query_game->result() as $key => $val) {
	        	$game_list[] = '<option class="game_list_front_side" value="'.$val->id.'">'.$val->game_name.'</option>';
	        	$game_list_id[] = $val->id;
	        	$game_list_name[] = $val->game_name;
	        }
		}

		if ($game_size_var != '' && $system != '') {
			$this->db->select('ag.id,ag.game_name');
			$this->db->from('game g');
			$this->db->join('admin_game ag','ag.id = g.game_id');
			$this->db->where("g.payment_status='1' AND g.status='1'");
			$this->db->where("g.game_category_id = '".$system."'");
			$this->db->where("g.game_type = '".$game_size_var."'");
			$this->db->group_by('ag.game_name');
			$this->db->limit(0,5);
        	$query_game = $this->db->get();
            
            foreach ($query_game->result() as $key => $val) {
	        	$game_list[] = '<option class="game_list_front_side" value="'.$val->id.'">'.$val->game_name.'</option>';
	        	$game_list_id[] = $val->id;
	        	$game_list_name[] = $val->game_name;
	        }
		}

		if ($game_name != '') {
			$this->db->select('asg.id as asgid, asg.*');
			$this->db->from('admin_sub_game asg');
			$this->db->join('game g','g.sub_game_id=asg.id');
			$this->db->where("g.game_id = '".$game_name."'");
			$this->db->group_by('asg.sub_game_name');
			$this->db->limit(0,5);
        	$query_sub_game = $this->db->get();
        
            foreach ($query_sub_game->result() as $key => $val) {
	        	$sub_game_list[] = '<option class="subgame_list_front_side" value="'.$val->id.'">'.$val->sub_game_name.'</option>';	
	        	$sub_game_list_id[] = $val->id;
	        	$sub_game_list_name[] = $val->sub_game_name;
			}
		}

  		$this->db->select('agc.*, asg.sub_game_name,g.best_of,g.game_password, g.id,g.game_category_id,g.price,g.game_timer,g.description,g.device_id,g.game_type,g.created_at,g.game_id,gi.game_image,ag.game_name,ud.name,g.sub_game_id');
		$this->db->from('game g');
		$this->db->join('user_detail ud','g.user_id=ud.user_id');
		$this->db->join('game_image gi','gi.id=g.image_id');
		$this->db->join('admin_game ag','ag.id=g.game_id');
		$this->db->join('admin_game_category agc','agc.id = ag.game_category_id');
		$this->db->join('admin_sub_game asg','asg.id = g.sub_game_id');
		$this->db->where("g.payment_status='1' AND g.status='1'");
		if ($system != '') {
    		$this->db->where("agc.id = '".$system."'");
    	}
    	if ($game_name != '') {
    		$this->db->where("ag.id = '".$game_name."'");
    	}
    	if ($game_name != '') {
    		$this->db->where("asg.game_id = '".$game_name."'");
    	}
    	if ($game_size_var != '') {
    		$this->db->where("g.game_type = '".$game_size_var."'");
    	}
    	if ($srch_term != '') {
    		$this->db->where("g.payment_status='1' AND g.status='1' AND (g.device_id LIKE '%".$srch_term."%' OR  ag.game_name LIKE '%".$srch_term."%')");
    	}

		$this->db->limit(0,5);
    	$query = $this->db->get();
    	// echo "<pre>";
    	// print_r($this->db->last_query());
    	// exit();
    	$res = $query->result();

    	foreach ($res as $key => $value) {
    		if($value->game_category_id == 1) {
                $game_icon = 'XBOX.png';
            } else if($value->game_category_id == 2) {
                $game_icon = 'PS4.png';
            } else if($value->game_category_id == 3) {
                $game_icon = 'PC.png';
            }else {
                $game_icon = '';
            }
	  		$res['new_time'][$key] = 0;
	  	  	if($value->game_timer != '' && $value->game_timer != "0000-00-00 00:00:00") {
	  	  		$diff = strtotime($value->game_timer)- time();
	  	  		if(time() < strtotime($value->game_timer)) {
	  	  			$new_time[] = '<script type="application/javascript"> var myCountdown1 = new Countdown({
	  	  				time: '.$diff.', width:120, height:45, rangeHi:"minute", style:"flip" }); </script>';
	  	  			$res['new_time'][$key] = $diff;
	  	  		}
	  	  	}
	  	  	$search_game[] = '<li>
            	<div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
                	<img src="'.base_url().'/upload/game/'.$value->game_image.'" alt="">
                </div>
                <div class="col-lg-3 col-sm-6 col-md-3 bid-info">
                	<h3>'.$value->game_name.'</h3>
                    <p>'.$value->sub_game_name.'</p>
                    <span class="postby">Tag: '.$value->device_id.'</span> 
                </div>
                <div class="col-lg-2 col-sm-3 col-md-2 uplod-day">
                   <h4 class="custom_timer">'.$new_time.'</h4>
                    <p style="margin-top: 5px;"> Gamesize: --'.$value->game_type."V".$value->game_type.'--</p>
                </div>
                <div class="col-lg-2 col-sm-4 col-md-2 divPrice">
                	<div class="game-price premium">
                    	<h3>'.CURRENCY_SYMBOL.$value->price.'</h3>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-5 col-md-3 primum-play play-n-rating">
                	<a href="'.base_url().'home/gameAuth/'.$value->id.'">play</a>
                    
                </div>
            </li>';

            if ($value->game_password != '') {
            	$res['id'][$key] = base_url().'home/gamePwAuth/'.$value->id;
            } else {
            	$res['id'][$key] = base_url().'home/gameAuth/'.$value->id;
            }
		   	
		   	$res['game_image'][$key] = '<img src="'.base_url().'upload/game/'.$value->game_image.'" alt="">';
		  	$res['sub_game_name'][$key] = $value->sub_game_name;
		  	$res['game_icon'][$key] = base_url().'upload/game/'.$game_icon;
		  	$res['device_id'][$key] = $value->device_id;
		  	$res['game_name'][$key] = $value->game_name;
		  	$res['game_type'][$key] = $value->game_type;
		  	$res['game_password'][$key] = $value->game_password;
		  	$res['price'][$key] = CURRENCY_SYMBOL.$value->price;
		  	$res['best_of'][$key] = $value->best_of;
	  	
	  }
	  
	  $res['game_list_id'] = $game_list_id;
	  $res['game_list_name'] = $game_list_name;
	  $res['game_list'] = $game_list;

	  $res['subgame_list_id'] = $sub_game_list_id;
	  $res['subgame_list_name'] = $sub_game_list_name;
	  $res['sub_game_list'] = $sub_game_list;
	  
	  $res['search_game'] = $search_game;
	  echo json_encode($res);
	}

    public function search_xbox_game(){
    	$game_name = $this->input->post('game_name');
        $sub_game_name = $this->input->post('sub_game_name');
        $game_size_var = $this->input->post('game_size');
        $system = $this->input->post('system');
      	$srch_term = $this->input->post('srch_term');
        	$this->db->select('agc.*, asg.sub_game_name,g.game_password,g.id,g.game_category_id,g.price,g.game_timer,g.description,g.device_id,g.game_type,g.created_at,g.game_id,gi.game_image,ag.game_name,ud.name,g.sub_game_id');
			$this->db->from('game g');
			$this->db->join('user_detail ud','g.user_id=ud.user_id');
			$this->db->join('game_image gi','gi.id=g.image_id');
			$this->db->join('admin_game ag','ag.id=g.game_id');
			$this->db->join('admin_game_category agc','agc.id = ag.game_category_id');
			$this->db->join('admin_sub_game asg','g.sub_game_id = asg.id');
			$this->db->where("g.payment_status='1' AND g.status='1'");
			if ($system != '') {
        	$this->db->where("agc.category_id = '".$system."'");
        	}
        	if ($game_name != '') {
        	$this->db->where("ag.game_name = '".$game_name."'");
        	}
        	if ($sub_game_name != '') {
        	$this->db->where("asg.sub_game_name = '".$sub_game_name."'");
        	}
        	if ($game_size_var != '') {
        	$this->db->where("g.game_type = '".$game_size_var."'");
        	}
        	if ($srch_term != '') {
        	$this->db->where("g.payment_status='1' AND g.status='1' AND g.device_id LIKE '%".$srch_term."%' OR g.payment_status='1' AND g.status='1' AND ag.game_name LIKE '%".$srch_term."%'" );
        	}


			$this->db->limit(0,5);
        	$query = $this->db->get();
        	$res = $query->result();
        	
       	// $res=$this->General_model->search_game_view_data('admin_game',array('game_type' => $game_size, 'game_name' => $game_name,'game_category_id'=>$cat_id,'play_status'=>0));
      foreach ($res as $key => $value) {
      	
      	if($value->game_timer != '' && $value->game_timer != "0000-00-00 00:00:00"){ $diff = strtotime($value->game_timer)- time(); if(time() < strtotime($value->game_timer)){   
      		$new_time[] = '<script type="application/javascript">
            							var myCountdown1 = new Countdown({
            									time: '.$diff.', // 86400 seconds = 1 day
            									width:120, 
            									height:45,  
            									rangeHi:"minute",
            									style:"flip"	// <- no comma on last item!
            								});
            								</script>';
            								}}
      	 $search_game[] = '<li>
                	<div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
                    	<img src="'.base_url().'/upload/game/'.$value->game_image.'" alt="">
                    </div>
                    <div class="col-lg-3 col-sm-6 col-md-3 bid-info">
                    	<h3>'.$value->game_name.'</h3>
                        <p>'.$value->sub_game_name.'</p>
                        <span class="postby">Tag: '.$value->device_id.'</span> 
                    </div>
                    <div class="col-lg-2 col-sm-3 col-md-2 uplod-day">
                       <h4>'.$new_time.'</h4>
                        <p style="margin-top: 5px;"> Gamesize: --'.$value->game_type."V".$value->game_type.'--</p>
                    </div>
                    <div class="col-lg-2 col-sm-4 col-md-2 divPrice">
                    	<div class="game-price premium">
                        	<h3>'.CURRENCY_SYMBOL.$value->price.'</h3>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-5 col-md-3 primum-play play-n-rating">
                    	<a href="'.base_url().'home/gameAuth/'.$value->id.'">play</a>
                        
                    </div>
                </li>';	


      }

       $res['search_game'] = $search_game;
       echo json_encode($res);
    }


     public function search_playstore_game()
     {
        $cat_id = $this->input->post('game_category_id');
        $game_name = $this->input->post('game_name');
        $sub_game_name = $this->input->post('sub_game_name');
        $game_size_var = $this->input->post('game_size');
       
        
      	$srch_term = $this->input->post('srch_term');

        	$this->db->select('asg.sub_game_name,g.game_password,g.id,g.game_category_id,g.price,g.game_timer,g.description,g.device_id,g.game_type,g.created_at,g.game_id,gi.game_image,ag.game_name,ud.name,g.sub_game_id');
			$this->db->from('game g');
			$this->db->join('user_detail ud','g.user_id=ud.user_id');
			$this->db->join('game_image gi','gi.id=g.image_id');
			$this->db->join('admin_game ag','ag.id=g.game_id');
			$this->db->join('admin_sub_game asg','g.sub_game_id = asg.id');
			$this->db->where("g.payment_status='1' AND g.status='1'");
			$this->db->where('g.game_category_id', '2');
        	if ($cat_id != '') {
        	$this->db->where('g.game_category_id', $cat_id);
        	}
        	if ($game_name != '') {
        	$this->db->where("ag.game_name = '".$game_name."'");
        	}
        	if ($sub_game_name != '') {
        	$this->db->where("asg.sub_game_name = '".$sub_game_name."'");
        	}
        	if ($game_size_var != '') {
        	$this->db->where("g.game_type = '".$game_size_var."'");
        	}
			if ($srch_term != '') {
		    $this->db->where("g.payment_status='1' AND g.status='1' AND g.game_category_id = '2' AND g.device_id LIKE '%".$srch_term."%' OR g.payment_status='1' AND g.status='1' AND g.game_category_id = '2' AND ag.game_name LIKE '%".$srch_term."%'" );
		    }      	        	
			$this->db->limit(0,5);
        	$query = $this->db->get();
        	$res = $query->result();
        	
    
      foreach ($res as $key => $value) {
      	
      	if($value->game_timer != '' && $value->game_timer != "0000-00-00 00:00:00"){ $diff = strtotime($value->game_timer)- time(); if(time() < strtotime($value->game_timer)){   
      		$new_time[] = '<script type="application/javascript">
            							var myCountdown1 = new Countdown({
            									time: '.$diff.', // 86400 seconds = 1 day
            									width:120, 
            									height:45,  
            									rangeHi:"minute",
            									style:"flip"	// <- no comma on last item!
            								});
            								</script>';
            								}}
      	 $search_game[] = '<li>
                	<div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
                    	<img src="'.base_url().'/upload/game/'.$value->game_image.'" alt="">
                    </div>
                    <div class="col-lg-3 col-sm-6 col-md-3 bid-info">
                    	<h3>'.$value->game_name.'</h3>
                        <p>'.$value->sub_game_name.'</p>
                        <span class="postby">Tag: '.$value->device_id.'</span> 
                    </div>
                    <div class="col-lg-2 col-sm-3 col-md-2 uplod-day">
                       <h4>'.$new_time.'</h4>
                        <p style="margin-top: 5px;"> Gamesize: --'.$value->game_type."V".$value->game_type.'--</p>
                    </div>
                    <div class="col-lg-2 col-sm-4 col-md-2 divPrice">
                    	<div class="game-price premium">
                        	<h3>'.CURRENCY_SYMBOL.$value->price.'</h3>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-5 col-md-3 primum-play play-n-rating">
                    	<a href="'.base_url().'home/gameAuth/'.$value->id.'">play</a>
                        
                    </div>
                </li>';	


      }

       $res['search_game'] = $search_game;
       echo json_encode($res);
    }

    //for getting sub game name
    public function get_subgame(){
    	$this->check_user_page_access();
		$gm_id=$this->input->post('ajax_game_id');
        //for get sub game name
        $res[0]=$this->General_model->view_data('admin_sub_game',array('game_id'=>$gm_id,'status'=>0));
        //for game description
        $res[1]=$this->General_model->view_data('admin_game',array('id'=>$gm_id));
        //get game image
        $res[2]=$this->General_model->view_data('game_image',array('game_id'=>$gm_id));
        echo json_encode($res);
    }

    //for getting sub game name
    public function playerslost(){
    	$this->check_user_page_access();
		$data['active']='game';

        $lostGames = $this->General_model->lost_games();
        // echo '<pre>';
        // print_r($lostGames); 
        // echo '</pre>';
        // exit(0);
        $data['lost_games'] = $lostGames;
        
        $content=$this->load->view('game/game_player_lost.php',$data,true);
        $this->render($content);
    }

    public function challenge_payment(){
    	$this->check_user_page_access();
		$accepterTag	=	$this->input->post('accepterTag');
		$game_id		=	$this->input->post('game_id');
		$data = array(
	    	'personal_challenge'=>'no',
			'accepter_device_id'=> $accepterTag
		);
		$this->General_model->update_data('game', $data, array('id'=>$game_id));
		$get_the_bet_detail = $this->General_model->view_data('game',array('id'=>$game_id))[0];
		$bet_detail = $this->General_model->view_data('bet',array('id'=>$get_the_bet_detail['bet_id'],'is_accept'=>0));

		if (!empty($bet_detail)) {
            $points = $this->General_model->view_data('user_detail',array('user_id'=>$_SESSION['user_id']));
			$percentage = $this->General_model->view_all_data('admin_points_percentage','id','asc');                        
			$lessthan5_total 		= $percentage[0]['lessthan5'];	
			$morethan5_25_total 	= $percentage[0]['morethan5_25'];	
			$morethan25_50_total 	= $percentage[0]['morethan25_50'];	
			$morethan50_total		= $percentage[0]['morethan50'];	

			$price_total = $this->session->userdata('game_price');
			if ($price_total < 5) {
				$price_add = $price_total + $lessthan5_total;
			} else if ($price_total >= 5 && $price_total < 25) {
				$price_add = $price_total + $morethan5_25_total;
			} else if ($price_total >= 25 && $price_total < 50) {
				$price_add = $price_total + $morethan25_50_total;
			} else if($price_total >= 50) {
				$price_add = $price_total + $morethan50_total;
			}
			$price_add_decimal = number_format((float)$price_add, 2, '.', '');
			
            $rem_points = $points[0]['total_balance'] - $price_add_decimal;
            $update_data=array(
                'total_balance'=> number_format((float)$rem_points, 2, '.', ''),
			);

            $this->General_model->update_data('user_detail',$update_data, array('user_id'=>$this->session->userdata('user_id')));
            $get_challenger_id = $bet_detail[0]['challenger_id'];
			$get_challengerdata = $this->General_model->view_data('user',array('id'=>$get_challenger_id));

			$to =	$get_challengerdata[0]['email'];

			$subject = "Game Acception ";

			$message ='

			<html>

			<body>
			<table cellpadding="0" cellspacing="0" broder="0" width="600">';
			$message .='<tr>
					<td><img src="'.base_url().'/assets/frontend/images/email/header.png" style="width:100%;"></td>
				</tr>';
			$message .='<tr>
					<td>
						<p> Hello </p>
						<p> Your Game has been Accepted by someone. Please use link below.</p>

						<a href='.base_url().'dashboard/>Click here</a>
						<p>Thanks,</p>
					</td>
				</tr>';
				$message .='<tr>
					<td><img src="'.base_url().'/assets/frontend/images/email/footer.png" style="width:100%;"></td>
				</tr>';
				$message .='</table>
			
			</body>

			</html>

			';
			
			
			
			$headers = "MIME-Version: 1.0" . "\r\n";

			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

			$headers .= 'From: <noreply@ProEsportsGaming.com>' . "\r\n";
			$user_name = 'User';
			// mail($to,$subject,$message,$headers);
			$re = $this->General_model->sendMail($to,$user_name,$subject,$message);
			
			
            $bt_data = array(
	            'accepter_id' => $this->session->userdata('user_id'),
	            'bet_status' => 1,
	            'is_accept' => 1
	        );
            $this->General_model->update_data('bet',$bt_data,array('id' => $get_the_bet_detail['bet_id']));
            $bt_dat = array(
            	'status' => 2
            );
            $this->General_model->update_data('game',$bt_dat,array('id' => $game_id));
			
			$images_array = '';

			$sender_mail_name = $this->General_model->view_data('user_detail',array('user_id'=>$_SESSION['user_id']));

			$mail = 'Your Game is Accepted by "'.$sender_mail_name[0]['name'].'".';

			$mail_data = array(
					'mail'=>$mail,
					'attachment'=>$images_array,
					'status'=>'1'
					);	
			$r=$this->General_model->insert_data('mail_data',$mail_data);

			$data_out=array(
				'user_id'=>$_SESSION['user_id'],
				'user_to'=>$get_challenger_id,
				'user_from'=>$_SESSION['user_id'],
				'direction'=>'OUT',
				'mail_type'=>'challenge_mail',
				'status'=>'0',
				'date'=>date('Y-m-d h:i:s'),
				'message_id'=>$r
			);	
			$data_in=array(
				'user_id'=>$get_challenger_id,
				'user_to'=>$_SESSION['user_id'],
				'user_from'=>$_SESSION['user_id'],
				'direction'=>'IN',
				'mail_type'=>'challenge_mail',
				'status'=>'0',
				'date'=>date('Y-m-d h:i:s'),
				'message_id'=>$r
			);	
			$r.=$this->General_model->insert_data('mail_conversation',$data_out);
			$r.=$this->General_model->insert_data('mail_conversation',$data_in);


            // update `admin_sub_game` table to `status` 0 changed on 26.07.2016
            $this->General_model->update_data('admin_sub_game',array('status'=>0),array('game_id'=>$this->session->userdata('game_id')));
            // update `admin_game` table to `play_status` 0 changed on 26.07.2016
            $this->General_model->update_data('admin_game',array('play_status'=>0),array('id'=>$this->session->userdata('game_id')));
            $data['dt']='Payment Successful';
            $content=$this->load->view('payment/payment_success.tpl.php',$data,true);
            $this->render($content);
			// redirect('payment/payment_success','refresh');
        }
    }

	public function winner_email(){
		$this->check_user_page_access();
		$to =	$get_challengerdata[0]['email'];
		$subject = "You are won game";
		$message ='<html>
			<body>
			<table cellpadding="0" cellspacing="0" broder="0" width="600">';
				// <tr>
				// 	<td><img src="'.base_url().'/assets/frontend/images/email/header.png" style="width:100%;"></td>
				// </tr>
				$message .='<tr>
					<td>
						<p> Hello </p>
						<p> You are Won the Game. Please this below link</p>
						<a href='.base_url().'dashboard/>Click here</a>
						<p>Thanks,</p>
					</td>
				</tr>';
				// <tr>
				// 	<td><img src="'.base_url().'/assets/frontend/images/email/footer.png" style="width:100%;"></td>
				// </tr>
				$message .='</table>
			</body>
			</html>';
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			$headers .= 'From: <noreply@ProEsportsGaming.com>' . "\r\n";
			$$user_name = 'User';
			$this->General_model->sendMail($to,$user_name,$subject,$message);
			// mail($to,$subject,$message,$headers);
	}

	
}   