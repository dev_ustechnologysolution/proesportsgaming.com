<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Home extends My_Controller
{
    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $cover_data = $this->General_model->view_all_data('cover','id','asc');
        $data['challangetDp']=$this->General_model->xbox_game_list();
        $data['game_system_list']=$this->General_model->game_system_list();
        $data['playstore_game']=$this->General_model->playstore_game_list();
        $data['systemList']= $this->General_model->matches_system_list();
        $data['custsysList']= $this->General_model->matches_custsyslist_list();
        $data['gamenameList']= $this->General_model->matches_game_name_list();
        $data['subgamenameList']= $this->General_model->matches_sub_game_name_list();
        $data['gameSize']= $this->General_model->matches_game_size();

        $data['lobby_list']=$this->General_model->lobby_list();

        $data['banner_data']=$this->General_model->banner_order();
        $data['dt']=$this->General_model->view_all_data('flow_of_work','id','asc');
        $data['heading_dt']=$this->General_model->view_all_data('heading','id','asc');
        $data['video_dt'] = $this->General_model->getvideos('',1);
        // $lock_video     =   $this->General_model->uploaded_videos();        
        // $remaining_video_count = 8 - count($lock_video);
        // $unlock_video   =   $this->General_model->unlock_uploaded_videos($remaining_video_count);
        // $data['video_dt']   =   array_merge($lock_video,$unlock_video);
        if ($cover_data[0]['status'] == '1') {
            if (isset($_SESSION['cover_page_password'])) {
        		$content=$this->load->view('home/home.tpl.php',$data,true);
                $this->render($content);
            } else {
                header('location:'.base_url().'Home/cover_page');
            }
        } else {
            $content=$this->load->view('home/home.tpl.php',$data,true);
            $this->render($content);
        }
    }
    public function php_ini(){
        $data = $this->General_model->view_all_data('lobby','id','asc');
        foreach ($data as $key => $d) {
            $dt = array(
                'custom_name' => $d['id']
            );
            $lb = $this->General_model->update_data('lobby', $dt, array(
                'id' => $d['id']
            ));
        }
        phpinfo();
        exit();
    }
    public function cover_page() {
        $cover_data = $this->General_model->view_all_data('cover','id','asc');
        if ($cover_data[0]['status'] == '1') {
            if (!isset($_SESSION['cover_page_password'])) {
                if (!isset($_POST['password'])) {
                    $data['cover_page_data']=$this->General_model->view_all_data('cover','id','desc');
                    $this->load->view('cover/coverpage.tpl.php',$data);   
                }
                else{
                    $data['cover_password']=$this->General_model->view_all_data('cover','id','desc');
                    if ($data['cover_password'][0]['password'] == $_POST['password'])
                    {
                        $_SESSION['cover_page_password'] = $_POST['password'];
                        redirect(base_url());
                    }
                    else
                    {
                        $_SESSION['cover_page_password_error'] = 'Password Incorrect';
                        redirect(base_url().'Home/cover_page');
                    }
                }
            }
            else{
                header('location:'.base_url());
            }
        }
        else{
            header('location:'.base_url());
        }
    }
    public function gameAuth($id) {
        $data['list']=$this->General_model->view_data('game',array('id'=>$id));
        // echo '<pre>';
        // print_r($data['list']);exit;
        if($data['list'][0]['user_id']!=$this->session->userdata('user_id')) {
            if($this->session->userdata('user_id')!='')
            {
                redirect(base_url().'game/gameDetails/'.$id);
            }
            else{
                redirect(base_url().'login');
            }
        }
        else {
            $this->session->set_flashdata('err', '<p style="color: #FF0000;">You can not play your own posted game</p>');
            header('location:'.base_url().'home');
        }
    }
    public function gamePwAuth($id) {
        $data['list']=$this->General_model->view_data('game',array('id'=>$id));
        if($data['list'][0]['user_id']!=$this->session->userdata('user_id')) {
            if($this->session->userdata('user_id')!=''){
                redirect(base_url().'game/gamePassword/'.$id);
            }
            else{
                redirect(base_url().'login');
            }
        }
        else {
            $this->session->set_flashdata('err', '<p style="color: #FF0000;">You can not play your own posted game</p>');
            header('location:'.base_url().'home');
        }
    }
    
    public function placeChallenge($id) {
        if($this->session->userdata('user_id')=='')
        {
            redirect(base_url().'login');
        }
        else{
            $data['game_dat']=$this->General_model->view_data('admin_game',array('id'=>$id));
            $data['game_cat']=$this->General_model->view_data('admin_game_category',array('id'=>$data['game_dat'][0]['game_category_id']));
            $data['subgame']=$this->General_model->view_data('admin_sub_game',array('game_id'=>$id));
            $data['game_img']=$this->General_model->view_data('game_image',array('game_id'=>$id));
            // echo '<pre>';
            // print_r($data['game_img']);exit;
            //redirect(base_url().'game/gameAdd/'.$id);
            $content=$this->load->view('game/game_create.tpl.php',$data,true);
            $this->render($content);
        }
    }
    //game
    public function game()
    {
        $data['game']=$this->General_model->view_all_data('game','id','desc');
        $this->load->view('game/game.tpl.php',$data);
    }
    public function game_select()
    {
        //$this->load->view('game/game.tpl.php');
        $data=array('name'=>$this->input->post('name', TRUE));
        if($this->input->post('name', TRUE)!='')
        {
            $r=$this->General_model->insert_data('cre_contact',$data);
        }
    }

}