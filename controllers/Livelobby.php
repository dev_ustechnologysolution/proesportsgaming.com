<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH . '../application/controllers/My_Controller.php';
class Livelobby extends My_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('My_model');
        $this->load->model('General_model');
        $this->load->library('pagination');
        $this->load->library(array(
            'form_validation'
        ));
        $this->target_dir = BASEPATH . '../upload/game/';
        $this->target_lbygrp_file = BASEPATH.'../upload/lobby_pics_msg/';
        $this->target_banner_dir = BASEPATH.'../upload/banner/';
        $this->pageLimit  = 20;
        $this->check_cover_page_access();
    }
    function _remap($method,$args) {
        if (method_exists($this, $method)) {
            if (count($args) < 2) {
                $this->$method($args[0]);
            } else {
                redirect(base_url());
            }
        } else {
            $this->index($method,$args);
        }
    }
    public function index($name) {
        // $name = str_replace('account', '', $name);
        $this->db->select('l.id');
        $this->db->from('lobby l');
        $this->db->join('group_bet gb','gb.lobby_id = l.id');
        $this->db->where('l.status',1);
        $this->db->where('gb.bet_status',1);
        $this->db->where('l.custom_name',strtolower($name));
        $id = $this->db->get()->row()->id;
        $view_lby_det = $this->General_model->view_data('lobby', array(
            'id' => $id,
            'status' => 1
        ));
        if (count($view_lby_det)>0) {
            $view_data = $this->General_model->view_data('lobby_fans', array(
                'lobby_id' => $id,
                'user_id' => $this->session->userdata('user_id')
            ));
            if (count($view_data) > 0) {
                $isLobbyEvent = $this->General_model->view_data('lobby',array('is_event'=>'1','id' => $id,'status' => 1));
               
                $query = $this->db->query('select count(id) as total from events_users where ((user_id ='.$this->session->userdata('user_id').' and is_gift = 0) or(is_gift = 1 and gift_to ='.$this->session->userdata('user_id').')) and type = 1 and lobby_id = '.$id);
                $is_event_lobby = $query->result_array();


                $isLobbySpectate = $this->General_model->view_data('lobby',array('is_spectator'=>'1','id' => $id,'status' => 1));
                $spectate_query = $this->db->query('select count(id) as total from events_users where ((user_id ='.$this->session->userdata('user_id').' and is_gift = 0) or(is_gift = 1 and gift_to ='.$this->session->userdata('user_id').')) and type = 2 and lobby_id = '.$id);
                $is_spectate_lobby = $spectate_query->result_array();
                $view = 0;
                if($isLobbySpectate[0]['is_spectator'] == 0 && $isLobbyEvent[0]['is_event'] == 0){
                    $view = 1;
                } else if($is_spectate_lobby[0]['total'] > 0 && $isLobbySpectate[0]['is_spectator'] == 1){
                    $view = 1;
                } 
                else if($is_event_lobby[0]['total'] > 0 && $isLobbyEvent[0]['is_event'] == 1){
                    $view = 1;
                } else if($view_lby_det[0]['user_id'] == $this->session->userdata('user_id')){
                    $view = 1;
                } 
                else {
                    $view = 0;
                }
                
                if($view == 1) {

                    $data = $this->footer_choose_table_reset($id);

                    $data['get_my_fan_tag']            = $this->General_model->get_my_fan_tag($id, $_SESSION['user_id']);
                    $data['wtnglist']                  = $this->wtnglist($id);
                    $data['subscribedlist']            = $this->General_model->subscribedlist($_SESSION['user_id']);
                    $data['game_category']             = $this->General_model->game_system_list();
                    $data['cust_game_cat']             = $this->General_model->matches_custsyslist_list();
                    $data['game_imgs']                 = $this->General_model->lobby_game_imgs($id);
                    $data['fanscount']                 = $this->General_model->fanscount($id);
                    $data['get_grp_for_me']            = $this->General_model->get_grp_for_me($id, $_SESSION['user_id']);
                    $data['get_play_status_lobby_bet'] = $this->General_model->get_play_status_lobby_bet($id);
                    $data['get_freport']               = $this->General_model->get_first_report_dtime($id);
                    $data['lobby_bet']                 = $this->General_model->lobby_bet_detail($id);
                    $data['lobby_ads']                 = $this->General_model->get_ads_detail('lobby_ads',1,'img','');
                    $data['check_bet_reported_for_me'] = $this->General_model->check_bet_reported_for_me($id);
                    $data['lobby_chat']                = $this->General_model->lobby_chat($id);
                    $data['check_reported']            = $this->General_model->check_bet_reported($id);
                    $data['get_stream_channels']       = $this->General_model->get_stream_channels($id);
                    $data['lobby_creator']             = $this->General_model->lobby_creator($id);
                    $data['lobby_creator_detail']      = $this->General_model->get_my_fan_tag($id, $data['lobby_creator']->user_id);
                    $data['defult_chat']               = $this->General_model->get_defult_lobby_chat($id);
                    
                    $data['stream_chat_expire']        = $this->General_model->stream_chat_expire($data['lobby_game_bet_left_grp'],$data['lobby_game_bet_right_grp']);

                    $data['game_name']                 = $this->General_model->matches_game_name_list($data['lobby_bet'][0]['game_category_id']);
                    $data['sub_game_name']             = $this->General_model->matches_sub_game_name_list($data['lobby_bet'][0]['game_id']);
                    $points                            = $this->General_model->view_data('user_detail', array(
                        'user_id' => $this->session->userdata('user_id')
                    ));
                    $data['total_balance']             = $points[0]['total_balance'];
                    $data['current_is_admin']             = $points[0]['is_admin'];

                    $data['is_full_play'] = 'no';
                    if ($data['lobby_bet'][0]['is_full_play'] == 'yes') {
                        $data['is_full_play'] = 'yes';
                    }
                    $content = $this->load->view('lobby/view_lobby.tpl.php', $data, true);
                    $this->render($content);
                } else { 
                    $event_data['lobby_data'] = $this->General_model->view_data('admin_game', array(
                        'id' => $view_lby_det[0]['game_id']));
                        $view_lby_det[0]['game_name'] =  $event_data['lobby_data'][0]['game_name']; 
                   
                    $event_data['basic_details'] = array('is_gift' => 0,'lobby_details' => $view_lby_det[0],'user_id' => $this->session->userdata('user_id'));
                    $content = $this->load->view('lobby/view_events_lobby.tpl.php', $event_data, true);
                    $this->render($content);
                }
            } else {
                redirect(base_url().'livelobby/addFan/'.$id);
            }
        } else {
            redirect(base_url());
            exit();
        }        
    }

    public function live_lobby_list($type = '', $page = 0) {    
        $data['challangetDp']=$this->General_model->xbox_game_list();
        $data['game_system_list']=$this->General_model->game_system_list();
        $data['custsysList']= $this->General_model->matches_custsyslist_list();
        $data['playstore_game']=$this->General_model->playstore_game_list();
        $data['systemList']= $this->General_model->matches_system_list();
        $data['gamenameList']= $this->General_model->matches_game_name_list();
        $data['subgamenameList']= $this->General_model->matches_sub_game_name_list();
        $data['gameSize']=$this->General_model->matches_game_size();
        $data['lobby_list']=$this->General_model->lobby_list();
        $data['banner_data']=$this->General_model->tab_banner();
        $data['dt']=$this->General_model->view_all_data('flow_of_work','id','asc');
        $data['heading_dt']=$this->General_model->view_all_data('heading','id','asc');
        $content = $this->load->view('lobby/lobby_list.tpl.php', $data, true);
        $this->render($content);
    }
    public function addFan($id) 
    {
        $this->check_user_page_access();
        $view_data = $this->General_model->view_data('lobby_fans', array(
            'lobby_id' => $id,
            'user_id' => $this->session->userdata('user_id')
        ));
        $lobby_detail = $this->General_model->lobby_details($id);

        $isLobbyEvent = $this->General_model->view_data('lobby',array('is_event'=>'1','id' => $id,'status' => 1));
               
        $query = $this->db->query('select count(id) as total from events_users where ((user_id ='.$this->session->userdata('user_id').' and is_gift = 0) or(is_gift = 1 and gift_to ='.$this->session->userdata('user_id').')) and type = 1 and lobby_id = '.$id);
        $is_event_lobby = $query->result_array();

        $isLobbySpectate = $this->General_model->view_data('lobby',array('is_spectator'=>'1','id' => $id,'status' => 1));

        $spectate_query = $this->db->query('select count(id) as total from events_users where ((user_id ='.$this->session->userdata('user_id').' and is_gift = 0) or(is_gift = 1 and gift_to ='.$this->session->userdata('user_id').')) and type = 2 and lobby_id = '.$id);

        $is_spectate_lobby = $spectate_query->result_array();

        if($_GET['is_gift'] == 1){
            $event_data['basic_details'] = array('is_gift' => 1,'lobby_details' => $lobby_detail[0],'user_id' => $this->session->userdata('user_id'));
            $content = $this->load->view('lobby/view_events_lobby.tpl.php', $event_data, true);
            $this->render($content);
        }  else {
            
            if (count($view_data) == '0') {
                if($isLobbySpectate[0]['is_spectator'] == 0 && $isLobbyEvent[0]['is_event'] == 0){
                    $view = 1;
                } else if($is_spectate_lobby[0]['total'] > 0 && $isLobbySpectate[0]['is_spectator'] == 1){
                    $view = 1;
                } 
                else if($is_event_lobby[0]['total'] > 0 && $isLobbyEvent[0]['is_event'] == 1){
                    $view = 1;
                } 
                else {
                    $view = 0;
                }
                if($view == 1){
                
                    $data['list'] = $lobby_detail;           
                    $content      = $this->load->view('lobby/lobby_addFan.tpl.php', $data, true);
                    $this->render($content);
                } else {                
                    $event_data['basic_details'] = array('is_gift' => 0,'lobby_details' => $lobby_detail[0],'user_id' => $this->session->userdata('user_id'));
                    $content = $this->load->view('lobby/view_events_lobby.tpl.php', $event_data, true);
                    $this->render($content);
                }
            } else {
                $view_lobby = $this->General_model->view_single_row('lobby', array('id' => $id),'*');
                redirect(base_url().'livelobby/'.$view_lobby['custom_name']);
            }
        }
    }
    public function insertFan() {
        $this->check_user_page_access();
        $session_data = array(
            'lobby_id' => $_POST['lobby_id']
        );
        $this->session->set_userdata($session_data);
        $get_fan_lby_exist = $this->General_model->view_single_row('lobby_fans',array('user_id' => $this->session->userdata('user_id'),'lobby_id' => $_POST['lobby_id']),'');
        $view_lobby = $this->General_model->view_single_row('lobby', array('id' => $_POST['lobby_id']),'*');
        if (count($get_fan_lby_exist) == 0) {
            $fan_data = array(
                'user_id' => $this->session->userdata('user_id'),
                'lobby_id' => $_POST['lobby_id'],
                'fan_tag' => $_POST['fan_device_id'],
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s', time())
            );
            $r = $this->General_model->insert_data('lobby_fans', $fan_data);
            if ($r) {
                redirect(base_url().'livelobby/'.$view_lobby['custom_name']);
            }
        } else {
            redirect(base_url().'livelobby/'.$view_lobby['custom_name']);
        }
    }
    public function lobbyAdd() {
        if ($this->session->userdata('user_id') == '') {
            header('location:' . base_url() . 'login');
            exit;
        }
        $data['active']                     = 'game';
        $data['list1']                      = $this->General_model->playing_game_list();
        $data['list']                       = $this->General_model->open_challenge_game_list();
        $data['challenge_personal_profile'] = $this->General_model->friend_profile($_GET['personal_challenge_friend_id']);
        $data['get_created_lobby'] = $this->General_model->created_lobbys($this->session->userdata('user_id'));
        $total_game_count                   = count($data['get_created_lobby']);
        if ($total_game_count >= 1) {
            $this->session->set_flashdata('message_err', 'Already Reached the Limit of creating A lobby <br> Please Leave your Existing game in My Challenges Tab. Than Join a New Game.');
            header('location:' . base_url() . 'dashboard');
            exit();
        }
        $data['game_category'] = $this->General_model->game_system_list();
        $data['cust_game_cat'] = $this->General_model->matches_custsyslist_list();
        $data['banner_data']   = $this->General_model->tab_banner();
        $content               = $this->load->view('lobby/lobby_create.tpl.php', $data, true);
        $this->render($content);
    }
    public function lobbyCreate() {
        $this->check_user_page_access();
        if (is_null($this->input->post('device_number', TRUE)) || $this->input->post('device_number', TRUE) == '') {
            $this->session->set_flashdata('required_msg', "Please enter PSN/XBOX/PC ID  Or Nickname!");
            header('location:' . base_url() . 'livelobby/lobbyadd');
        } else {
            $get_user_detail = $this->User_model->get_user_detail($_SESSION['user_id']);
            $bet_amount = number_format($this->input->post('bet_amount'),2,".","");
            $data   = array(
                'user_id' => $_SESSION['user_id'],
                'game_category_id' => $this->input->post('game_category', TRUE),
                'game_id' => $this->input->post('game_name', TRUE),
                'sub_game_id' => $this->input->post('subgame_name_id', TRUE),
                'game_type' => $this->input->post('game_type', TRUE),
                'device_id' => $this->input->post('device_number', TRUE),
                'image_id' => $this->input->post('game_image_id', TRUE),
                'price' => $bet_amount,
                'lobby_password' => $this->input->post('lobby_password', TRUE),
                'created_at' => date('Y-m-d'),
                'description' => $this->input->post('description', TRUE)
            ); 
            $game_id = $this->input->post('game_name', TRUE);
            $sgame_id = $this->input->post('subgame_name_id', TRUE);
            if ($_POST['personal_challenge_friend_id'] == 1) {
                $add_game_category_id = $_POST['game_category'];
                $game_type='';
                foreach($_POST['game_type'] as $r=>$w) {
                    $game_type .=$w.'|';
                }
                $add_admin_game = array(
                    'game_name'=>$_POST['game_name'],
                    'game_description'=>$_POST['game_name'],
                    'game_category_id'=>$add_game_category_id,
                    'created_at'=>date('Y-m-d h:i:s'),
                    'game_type'=>$game_type,
                    'custom'=>'yes'
                );

                $add_admin_game_id = $this->General_model->insert_data('admin_game',$add_admin_game);

                $add_admin_sub_game = array(
                    'game_id'=>$add_admin_game_id,
                    'sub_game_name'=>$_POST['subgame_name_id'],
                    'custom'=>'yes'
                );
                $add_admin_sub_game_id = $this->General_model->insert_data('admin_sub_game',$add_admin_sub_game);

                $img = '';
                if(isset($_FILES["game_image"]["name"]) && $_FILES["game_image"]["name"]!='') {
                    $img=time().basename($_FILES["game_image"]["name"]);
                    $ext = end((explode(".", $img)));
                    if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg' || $ext=='GIF' || $ext=='JPEG' || $ext=='PNG' || $ext=='JPG') {
                        $target_file = $this->target_dir.$img;
                        move_uploaded_file($_FILES["game_image"]["tmp_name"], $target_file);
                        $add_admin_game_image = array(
                            'game_id'=>$add_admin_game_id,
                            'game_image'=>$img,
                            'custom'=>'yes'
                        );  
                        $add_admin_game_image_id = $this->General_model->insert_data('game_image',$add_admin_game_image);
                    }
                }
                $game_id = $add_admin_game_id;
                $sgame_id = $add_admin_sub_game_id;
                
                $data   = array(
                    'user_id' => $_SESSION['user_id'],
                    'game_category_id' => $add_game_category_id,
                    'game_id' => $add_admin_game_id,
                    'sub_game_id' => $add_admin_sub_game_id,
                    'game_type' => $this->input->post('game_type', TRUE),
                    'device_id' => $this->input->post('device_number', TRUE),
                    'image_id' => $add_admin_game_image_id,
                    'price' => $bet_amount,
                    'lobby_password' => $this->input->post('lobby_password', TRUE),
                    'created_at' => date('Y-m-d'),
                    'is_custom' => 'yes',
                    'description' => $this->input->post('description', TRUE)
                );    
            }
            $lobby_creat_fee = $this->General_model->view_all_data('admin_lobby_fee', 'id', 'asc');
            $fee_for_creating_game = $lobby_creat_fee[0]['fee_for_creating_game'];
            $price_add_decimal = number_format((float) $fee_for_creating_game, 2, '.', '');
            if ((float) $price_add_decimal > (float) $get_user_detail->total_balance) {
                $this->session->set_flashdata('msg', "You do not have enough credits to place a challenge. Please Proceed to Purchase tab for more.");
                header('location:' . base_url() . 'points/buypoints');
            } else {
                $rem_points  = $get_user_detail->total_balance - $price_add_decimal;
                $update_data = array(
                    'total_balance' => number_format((float) $rem_points, 2, '.', '')
                );
                $this->General_model->update_data('user_detail', $update_data, array(
                    'user_id' => $this->session->userdata('user_id')
                ));
                $query  = $this->db->query('SELECT max(lobby_order) as current_lobby_order FROM lobby');
                $current_lobby_order = $query->result();
                $lobby_order = $current_lobby_order[0]->current_lobby_order + 1;
                $data['lobby_order'] = $lobby_order;
                $r = $this->General_model->insert_data('lobby', $data);
                if ($r) {
                    $data['gameImage'] = $this->General_model->view_data('game_image', array(
                        'id' => $this->input->post('game_image_id', TRUE)
                    ));
                    $data['game_name'] = $this->General_model->view_data('admin_game', array(
                        'id' => $this->input->post('game_name', TRUE)
                    ));
                    $session_data      = array(
                        'game_name' => $data['game_name'][0]['game_name'],
                        'game_image' => $data['gameImage'][0]['game_image'],
                        'game_price' => $bet_amount,
                        'game_id' => $game_id,
                        'sgame_id' => $sgame_id,
                    );
                    $this->session->set_userdata($session_data);
                    $this->session->set_userdata('message_succ', 'Successfully added');
                    
                    $bet_data = array(
                        'lobby_id' => $r,
                        'game_id' => $this->session->userdata('game_id'),
                        'game_sub_id' => $this->session->userdata('sgame_id'),
                        'bet_status' => 1,
                        'is_active' => 1,
                        'challenger_table' => 'LEFT',
                        'accepter_table' => 'RIGHT'
                    );
                    $bet_id   = $this->General_model->insert_data('group_bet', $bet_data);
                    
                    $condition   = "1 AND `game_id`='" . $this->session->userdata('game_id') . "' AND (`status`=1 OR `status`=0)";
                    $arr_game_id = $this->General_model->view_data('lobby', $condition);
                    $dt          = array(
                        'payment_status' => 1,
                        'custom_name' => 'account'.$get_user_detail->custom_name,
                        'status' => 1
                    );
                    $lb = $this->General_model->update_data('lobby', $dt, array(
                        'id' => $r
                    ));

                    $lobby_fee   = array(
                        'user_id'       => $this->session->userdata('user_id'),
                        'lobby_id'      => $r,
                        'group_bet_id'  => $bet_id,
                        'fee'           => $price_add_decimal,
                        'fee_type'      => 'create',
                        'status'        => '1',
                        'created'       => date('Y-m-d H:i:s',time()),
                    );
                    $this->General_model->insert_data('lobby_fee_colloected',$lobby_fee);

                    if ($lb) {
                        $default_stream = $this->General_model->get_stream_setting($_SESSION['user_id'],'','default_stream')[0];
                        $lby_grp_data = array(
                            'group_bet_id' => $bet_id,
                            'table_cat' => 'LEFT',
                            'user_id' => $_SESSION['user_id'],
                            'status' => 1,
                            'is_creator' => 'yes',
                            'stream_status' => ($default_stream->default_stream_channel != '') ? 'enable' : 'disable',
                            'stream_channel' => ($default_stream->default_stream_channel != '') ? $default_stream->default_stream_channel : '',
                            'created_at' => date('Y-m-d H:i:s', time())
                        );
                        $is_waiting = 1;
                        $is_waiting_table = 'LEFT';
                        if ($get_user_detail->is_18 == 1) {
                            $is_waiting = 0;
                            $is_waiting_table = NULL;
                            $lby_insert   = $this->General_model->insert_data('lobby_group', $lby_grp_data);
                        }
                        
                        $lby_fan_data = array(
                            'user_id' => $_SESSION['user_id'],
                            'lobby_id' => $r,
                            'fan_tag' => $this->input->post('device_number', TRUE),
                            'status' => 1,
                            'is_waiting' => $is_waiting,
                            'waiting_table' => $is_waiting_table,
                            'created_at' => date('Y-m-d H:i:s', time())
                        );
                        $this->General_model->insert_data('lobby_fans', $lby_fan_data);
                    }
                    redirect(base_url().'livelobby/account'.$get_user_detail->custom_name);
                } else {
                    $this->session->set_userdata('message_err', 'Failure to add');
                    redirect(base_url().'livelobby/lobbyadd');
                }
            }
        }
    }
    public function watchLobby($id) {
        if ($this->session->userdata('user_id') != '') {
            if(isset($_GET['is_gift'])){
                redirect(base_url() . 'livelobby/addFan/' . $id.'?is_gift='.$_GET['is_gift']);
            } else {
                redirect(base_url().'livelobby/addFan/'.$id);
            }
        } else {
            redirect(base_url() . 'login');
        }
    }
    public function page($page_id) {
        $data['dt']       = 1;
        // get all game under category xbox
        $offset           = 5;
        $start            = ($page_id * $offset) - $offset;
        $condition        = "1 AND game_category_id='1' LIMIT " . $start . "," . $offset;
        $data['gameList'] = $this->General_model->admin_Matches_list($condition);
        //$data['gameList']=$this->General_model->view_data('admin_game',$condition);
        // GET TOTAL NO OF ROWS
        
        $tot_game        = $this->General_model->get_num_rows('game', array(
            'game_category_id' => 1
        ));
        // CALCULATE NO OF PAGES
        $tot_page        = ceil($tot_game / $offset);
        $data['totGame'] = $tot_game;
        $data['totPage'] = $tot_page;
        // GET ALL CATEGORY LIST
        
        $data['catList'] = $this->General_model->xbox_game_list();
        $content         = $this->load->view('matches/matches.tpl.php', $data, true);
        $this->render($content);
    }
    public function view_fanslist($id) {
        $this->check_user_page_access();
        $data['view_lby_det'] = $this->General_model->view_data('lobby', array(
            'id' => $id,
            'status' => 1
        ));
        if(count($data['view_lby_det'])>0){
            $view_data = $this->General_model->view_data('lobby_fans', array(
                'lobby_id' => $id,
                'user_id' => $this->session->userdata('user_id')
            ));
            if (count($view_data) > 0) {
                $options             = array(
                'select' => 'lf.user_id, lf.id,lf.fan_tag,lf.lobby_id,lf.status,ud.name,ud.image,l.status as lobby_status',
                'table' => 'lobby_fans lf',
                'join' => array(
                'user_detail ud' => 'ud.user_id = lf.user_id',
                'lobby l' => 'l.id = lf.lobby_id',
                ),
                    'where' => array(
                        'lf.lobby_id' => $id,
                        'l.status' => 1
                    )
                );
                $data['view_lobby_fans_list'] = $this->General_model->commonGet($options);

                $options             = array(
                'select' => 'ud.name,ud.user_id',
                'table' => 'user_detail ud',
                'where' => array(
                    'ud.user_id' => $_SESSION['user_id']
                ),
                'single' => true
                );
                $data['my_data'] = $this->General_model->commonGet($options);

                $data['fans_id_list'] = $this->General_model->view_data('lobby_fans', array(
                    'lobby_id' => $id
                ));
                $data['ordinal_lobby_id'] = $this->General_model->ordinal($id);
                $content = $this->load->view('lobby/view_lobby_fanslist.tpl.php', $data, true);
                $this->render($content);
            } else {
                redirect(base_url().'livelobby/addFan/'.$id);
            }
        } else {
            redirect(base_url());
            exit();
        }
    }
    public function lobby_fan_search() {
        $this->check_user_page_access();
        $fans_id = $_POST['fans_id'];
        $fan_tag = $_POST['fan_tag'];
        $srch_fan = $_POST['srch_fan'];
        $lobby_id = $_POST['lobby_id'];
        $this->db->select('lf.user_id, lf.id,lf.fan_tag,lf.lobby_id,lf.status,ud.name,ud.image,l.status as lobby_status');
        $this->db->from('lobby_fans lf');
        $this->db->join('user_detail ud','ud.user_id = lf.user_id');
        $this->db->join('lobby l','l.id = lf.lobby_id');
        if (isset($fans_id) && $fans_id != '') {
            $this->db->where('lf.id',$fans_id);
        }
        if (isset($fan_tag) && $fan_tag != '') {
            $this->db->where('lf.fan_tag',$fan_tag);
        }
        if (isset($srch_fan) && $srch_fan != '') {
           $this->db->where('lf.id LIKE "%'.$srch_fan.'%" OR lf.fan_tag LIKE "%'.$srch_fan.'%"');
        }
        $this->db->where('lf.lobby_id',$lobby_id);
        $this->db->where('l.status',1);
        $this->db->order_by('lf.id','desc');
        $query=$this->db->get();
        $data['view_lobby_fans_list'] = $query->result_array();
        echo json_encode($data);
        exit();
    }
    public function view_fansprofile() {
        $id = $_GET['lobby_id'];
        $fans_id = $_GET['fans_id'];
        $this->check_user_page_access();
        $data['view_lby_det'] = $this->General_model->view_data('lobby', array(
            'id' => $id,
            'status' => 1
        ));
        if(count($data['view_lby_det'])>0){
            $view_data = $this->General_model->view_data('lobby_fans', array(
                'lobby_id' => $id,
                'user_id' => $this->session->userdata('user_id')
            ));
            if (count($view_data) > 0) {
                $checkcountry = $this->General_model->view_data('user_detail', array('user_id' => $fans_id));
                if (is_numeric($checkcountry[0]['country'])) {
                    $options             = array(
                    'select' => 'lf.id,lf.fan_tag,lf.lobby_id,lf.status,ud.*,u.*,ud.image,l.status as lobby_status, c.sortname as country, s.statename as state',
                    'table' => 'lobby_fans lf',
                    'join' => array(
                    'user_detail ud' => 'ud.user_id = lf.user_id',
                    'user u' => 'u.id = lf.user_id',
                    'lobby l' => 'l.id = lf.lobby_id',
                    'country c' => 'c.country_id = ud.country',
                    'state s' => 's.country_id = c.country_id',
                    ),
                        'where' => array(
                            'lf.lobby_id' => $id,
                            'lf.user_id' => $fans_id, 
                            'l.status' => 1
                        )
                    );
                } else {
                    $options             = array(
                    'select' => 'lf.id,lf.fan_tag,lf.lobby_id,lf.status,ud.*,u.*,ud.image,l.status as lobby_status',
                    'table' => 'lobby_fans lf',
                    'join' => array(
                    'user_detail ud' => 'ud.user_id = lf.user_id',
                    'user u' => 'u.id = lf.user_id',
                    'lobby l' => 'l.id = lf.lobby_id',
                    ),
                        'where' => array(
                            'lf.lobby_id' => $id,
                            'lf.user_id' => $fans_id, 
                            'l.status' => 1
                        )
                    );
                }
                $data['view_lobby_fan_profile'] = $this->General_model->commonGet($options);
                $data['ordinal_lobby_id'] = $this->General_model->ordinal($id);
                 
                $content = $this->load->view('lobby/view_fansprofile.tpl.php', $data, true);
                $this->render($content);
            } else {
                redirect(base_url().'livelobby/addFan/'.$id);
            }
        } else {
                redirect(base_url());
                exit();
        }
    }
    public function lobby_join() {
        $this->check_user_page_access();
        $join_lobby_data          = array(
            'group_bet_id' => $_POST['group_bet_id'],
            'table_cat' => $_POST['table_cat'],
            'user_id' => $this->session->userdata('user_id'),
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s', time())
        );
        $join_lobby_data_inserted = $this->General_model->insert_data('lobby_group', $join_lobby_data);
        $res['get_join_detail']   = $this->General_model->get_join_detail($_POST['group_bet_id'], $_POST['table_cat']);
        if (isset($join_lobby_data_inserted)) {
            echo json_encode($res);
        }
    }
    public function change_lobby_details() {
        $this->check_user_page_access();
        if (isset($_POST) && !empty($_POST['lobby_id']) && !empty($_POST['system']) && !empty($_POST['gamename']) && !empty($_POST['subgamename']) && !empty($_POST['game_type']) && !empty($_POST['game_amount']) && !empty($_POST['password_option']) && !empty($_POST['description']) && !empty($_POST['group_bet_id'])) {
            $game_id = $_POST['gamename'];
            $sgame_id = $_POST['subgamename'];
            if ($_POST['custom_lby'] == 'on') {
                $game_category_id = $_POST['system'];
                $game_type='';
                foreach($_POST['game_type'] as $r=>$w) {
                    $game_type .=$w.'|';
                }
                $add_admin_game = array(
                    'game_name'=>$_POST['gamename'],
                    'game_description'=>$_POST['gamename'],
                    'game_category_id'=>$game_category_id,
                    'created_at'=>date('Y-m-d h:i:s'),
                    'game_type'=>$game_type,
                    'custom'=>'yes'
                );
                $game_id = $this->General_model->insert_data('admin_game',$add_admin_game);
                $add_admin_sub_game = array(
                    'game_id'=>$game_id,
                    'sub_game_name'=>$_POST['subgamename'],
                    'custom'=>'yes'
                );
                $sgame_id = $this->General_model->insert_data('admin_sub_game',$add_admin_sub_game);
                $game_image_id = $_POST['game_image_id'];
                $img = '';
                if($_FILES["game_image_id"]["name"][0]!='') {
                    $img=time().basename($_FILES["game_image_id"]["name"][0]);
                    $ext = end((explode(".", $img)));
                    if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg' || $ext=='GIF' || $ext=='JPEG' || $ext=='PNG' || $ext=='JPG') {
                        $target_file = $this->target_dir.$img;
                        move_uploaded_file($_FILES["game_image_id"]["tmp_name"][0], $target_file);
                        $add_admin_game_image = array(
                            'game_id'=>$game_id,
                            'game_image'=>$img,
                            'custom'=>'yes'
                        );  
                        $game_image_id = $this->General_model->insert_data('game_image',$add_admin_game_image);
                    }
                } else {
                    $upgid = array(
                        'game_id' => $game_id
                    );
                    $ugrpbetwhere = array(
                        'id' => $game_image_id
                    );
                    $this->General_model->update_data('game_image', $upgid, $ugrpbetwhere);
                }
                $update_row = array(
                    'game_category_id' => $_POST['system'],
                    'game_id' => $game_id,
                    'sub_game_id' => $sgame_id,
                    'game_type' => $_POST['game_type'],
                    'image_id' => $game_image_id,
                    'price' => $_POST['game_amount'],
                    'lobby_password' => $_POST['lobby_password'],
                    'description' => $_POST['description']
                );
            } else {
                $update_row = array(
                    'game_category_id' => $_POST['system'],
                    'game_id' => $game_id,
                    'sub_game_id' => $sgame_id,
                    'game_type' => $_POST['game_type'],
                    'image_id' => $_POST['game_image_id'],
                    'price' => $_POST['game_amount'],
                    'lobby_password' => $_POST['lobby_password'],
                    'description' => $_POST['description']
                );
            }
            if ($update_row) {
                $get_lobby_info = $this->General_model->view_data('lobby',array('id'=>$_POST['lobby_id']));
                $this->reset_lobby_balance($get_lobby_info,$_POST['lobby_id'],$_POST['group_bet_id'],$_POST['game_type']);
                $update_data_where = array('id' => $_POST['lobby_id']);
                $res['update'] = $this->General_model->update_data('lobby', $update_row, $update_data_where);
                if ($res['update']) {
                    $update_game_id = array(
                        'game_id' => $game_id,
                        'game_sub_id' => $sgame_id
                    );
                    $update_grpbet_where = array(
                        'id' => $_POST['group_bet_id']
                    );
                    $res['update_grpbet'] = $this->General_model->update_data('group_bet', $update_game_id, $update_grpbet_where);
                }
            }
        }
        echo json_encode($res);
        exit();
    }
    public function change_event() {
        $this->check_user_page_access();
        if (!empty($_POST)) {
            $user_id = $_SESSION['user_id'];
            $lobby_id = $_POST['lobby_id'];
            $is_event = $_POST['change_lobby_event'];
            $is_spector = $_POST['change_lobby_spector'];
            $lobby_event_amount = ($_POST['lobby_event_amount'])?$_POST['lobby_event_amount']:'1';
            $lobby_spector_amount = ($_POST['lobby_spector_amount'])?$_POST['lobby_spector_amount']:'0';

            $event_fee = ($lobby_event_amount)?$lobby_event_amount * 10/100:0;
            $event_image = ($_POST['event_old_img'] != '' && $_POST['event_old_img'] != undefined)?$_POST['event_old_img']:'';

            if(!empty($_FILES['event_image'])) {
                $event_img = time().'_event_img_'.basename($_FILES["event_image"]["name"]);
                $target_file = $this->target_banner_dir.$event_img;
                if(move_uploaded_file($_FILES["event_image"]["tmp_name"], $target_file)) {
                    if(file_exists($this->target_banner_dir.$event_image)) {
                        unlink($this->target_banner_dir.$event_image);
                    }
                    $event_image = $event_img;
                }
            }

            $update_data = array(
                'is_event' => $is_event,
                'is_spectator' => $is_spector,
                'event_price' => $lobby_event_amount,
                'event_creator' => $user_id,
                'event_image' => $event_image,
                'event_fee' => number_format((float)$event_fee, 2, '.', ''),
                'spectate_price' => $lobby_spector_amount,
                'event_creator' => $this->session->userdata('user_id')
            );
            $update_data_where = array(
                'id' => $lobby_id
            );
            $r = $this->General_model->update_data('lobby',$update_data,$update_data_where);
            $error = '';
            if ($r) {
                $this->session->set_flashdata('message_succ', 'Event Updated');
            } else {
                $error = 'Event not Updated';
                $this->session->set_flashdata('message_err', $error);
            }
            $res['error'] = $error;
            $res['lobby_custom_name'] = $_POST['lobby_custom_name'];
            echo json_encode($res);
            exit();
        }
    }
    public function change_lobby_tag() {
        $this->check_user_page_access();
        $data = $this->General_model->lobby_creator($_POST['lobby_id']);
        if ($_POST['lobby_id'] != '') {
            $lobby_id = $_POST['lobby_id'];
            $lobby_tag = $_POST['lobby_tag'];
            if ($data->user_id == $_SESSION['user_id']) {
                $update_data_wherelby = array('id' => $lobby_id, 'user_id' => $_SESSION['user_id']);
                $updatelby = array('device_id'=>$_POST['lobby_tag']);
                $this->General_model->update_data('lobby', $updatelby, $update_data_wherelby);
            }
            $update_data_where = array('lobby_id' => $lobby_id, 'user_id' => $_SESSION['user_id']);
            $update_row = array('fan_tag' => $lobby_tag);
            $res['update'] = $this->General_model->update_data('lobby_fans', $update_row, $update_data_where);
            if ($res) {
                echo json_encode($res);
                exit();
            }
        }
    }
    public function send_msg_delete() {
        $this->check_user_page_access();
        if ($_POST['msg_id'] != '' && $_POST['plyr_id']) {
            $msg_id = $_POST['msg_id'];
            $plyr_id = $_POST['plyr_id'];
            $del_msg = $this->General_model->delete_data('xwb_messages', array(
                'id' => $msg_id
            ));
            if ($del_msg){
                $del_conv = $this->General_model->delete_data('lobby_group_conversation', array(
                    'message_id' => $msg_id
                )); 
            }
            $res['message_id'] = $msg_id;
            echo json_encode($res);
            exit();
        }
    }
    public function del_usr_from_chat() {
        $this->check_user_page_access();
        if ($_POST['plyr_id'] != '' && $_POST['lobby_id']) {
            $plyr_id = $_POST['plyr_id'];
            $lobby_id = $_POST['lobby_id'];
            $update_data = array(
                'status' => 0
            );
            $update_usr_status = $this->General_model->update_data('lobby_fans', $update_data, array(
                'user_id' => $plyr_id,
                'lobby_id' => $lobby_id
            ));
            if ($update_usr_status) {
                $res['lobby_id'] = $lobby_id;
                $res['user_id'] = $plyr_id;
                echo json_encode($res);
                exit();
            }
        }
    }
    public function mute_usr_from_chat() {
        $this->check_user_page_access();
        if ($_POST['plyr_id'] != '' && $_POST['lobby_id']) {
            $plyr_id = $_POST['plyr_id'];
            $lobby_id = $_POST['lobby_id'];
            $mute_time = date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." +1 minutes"));
            $update_data = array(
                'mute_time' => $mute_time
            );
            $update_usr_status = $this->General_model->update_data('lobby_fans', $update_data, array(
                'user_id' => $plyr_id,
                'lobby_id' => $lobby_id
            ));
            if ($update_usr_status) {
                $res['mutediff'] = strtotime($mute_time) - time();
                $res['lobby_id'] = $lobby_id;
                $res['user_id'] = $plyr_id;
                echo json_encode($res);
                exit();
            }
        }
    }    
    public function challenge_payment() {
        $this->check_user_page_access();
        $group_bet_id = $_POST['group_bet_id'];
            
        $lobby_fee = $this->General_model->view_all_data('admin_lobby_fee', 'id', 'asc');
        $fee_per_game = $lobby_fee[0]['fee_per_game'];
        $join_fee = number_format((float)$fee_per_game, 2, '.', '');

        $udata = $this->General_model->view_data('user_detail', array(
            'user_id' => $_SESSION['user_id']
        ));

        $srcData['display_name'] = $this->General_model->get_my_fan_tag($_POST['lobby_id'],$_SESSION['user_id'])->fan_tag;
        $srcData['user_id'] = $_SESSION['user_id'];

        $final_fee = $_POST['tax_price']*$join_fee/100;
        $fee = $final_fee + $_POST['tax_price'];
        $rem_points  = $udata[0]['total_balance'] - $fee;
        $update_data = array(
            'total_balance' => number_format((float) $rem_points, 2, '.', '')
        );
        $this->General_model->update_data('user_detail', $update_data, array(
            'user_id' => $this->session->userdata('user_id')
        ));
        
        $lobby_payment_update_data = array(
            'group_bet_id' => $group_bet_id,
            'user_id' => $_SESSION['user_id'],
            'status' => 1
        );
        $r = $this->General_model->update_data('lobby_group', array('play_status' => 1), $lobby_payment_update_data);
        if ($r) {
            $options             = array(
                'select' => 'lg.*,ud.total_balance',
                'table' => 'lobby_group lg',
                'join' => array(
                    'user_detail ud' => 'ud.user_id = lg.user_id'
                ),
                'where' => array(
                    'lg.group_bet_id' => $group_bet_id,
                    'lg.user_id' => $_SESSION['user_id'],
                    'lg.play_status' => 1,
                    'lg.status' => 1
                )
            );
            $srcData['lobby_payment_update_data'] = $this->General_model->commonGet($options);
            $playedgrp_data                = $this->General_model->view_data('lobby_group', array(
                'group_bet_id' => $_POST['group_bet_id'],
                'play_status' => 1
            ));
            $lobby_bet                     = $this->General_model->lobby_bet_detail($_POST['lobby_id']);
            $srcData['get_lobby_size']     = $lobby_bet[0]['game_type'] * 2;

            $lobby_fee   = array(
                'user_id'       => $this->session->userdata('user_id'),
                'lobby_id'      => $_POST['lobby_id'],
                'group_bet_id'  => $group_bet_id,
                'fee'           => $final_fee,
                'fee_type'      => 'join',
                'status'        => '1',
                'created'       => date('Y-m-d H:i:s',time()),
            );
            $this->General_model->insert_data('lobby_fee_colloected',$lobby_fee);

            $srcData['is_full_play'] = 'no';
            if (count($playedgrp_data) == $srcData['get_lobby_size']) {
                $full_played_update = array(
                    'is_full_play' => 'yes'
                );
                $this->General_model->update_data('group_bet', $full_played_update, array(
                    'id' => $group_bet_id
                ));
                $srcData['is_full_play'] = 'yes';
            }
            echo json_encode($srcData);
            exit();
        }
    }
    public function group_bet_report() {
        $this->check_user_page_access();
        $group_bet_id             = $_POST['group_bet_id'];
        $lobby_report_update_data = array(
            'group_bet_id' => $group_bet_id,
            'user_id' => $_SESSION['user_id'],
            'status' => 1
        );
        $r                        = $this->General_model->update_data('lobby_group', array(
            'reported' => 1
        ), $lobby_report_update_data);
    }
    public function get_playedgrp_data() {
        $group_bet_id                  = $_POST['group_bet_id'];
        $lobby_id                      = $_POST['lobby_id'];
        $srcData['get_playedgrp_data'] = $this->General_model->view_data('lobby_group', array(
            'group_bet_id' => $_POST['group_bet_id'],
            'play_status' => 1
        ));
        $lobby_bet                     = $this->General_model->lobby_bet_detail($lobby_id);
        $srcData['get_lobby_size']     = $lobby_bet[0]['game_type'] * 2;
        $srcData['lobby_creator']      = $this->General_model->lobby_creator($lobby_id);
        $srcData['get_lefttop_played_id']  = $this->General_model->get_lefttop_played_id($group_bet_id, $lobby_id);
        $srcData['get_righttop_played_id'] = $this->General_model->get_righttop_played_id($group_bet_id, $lobby_id);
        
        $srcData['is_full_play']             = 'no';
        if ($lobby_bet[0]['is_full_play'] == 'yes') {
            $srcData['is_full_play']         = 'yes';
        }
        $srcData['user_id']                  = $_SESSION['user_id'];
        $srcData['get_playedgrp_data_count'] = count($playedgrp_data);
        echo json_encode($srcData);
        exit();
    }
    public function report_lobbybet() {
        $lobby_id        = $_POST['lobby_id'];
        $group_bet_id    = $_POST['group_bet_id'];
        $user_id         = $_POST['user_id'];
        $video           = $_POST['video'];
        $accept_risk     = $_POST['accept_risk'];
        $win_lose_status = $_POST['win_lose_status'];
        $explain_admin   = $_POST['explain_admin'];
        $data = $this->General_model->lobby_report_timer();
        $fivmininc = date('Y-m-d H:i:s',strtotime("+".$data->hours." hours +".$data->minutes." minutes",time()));
        $get_my_table = $this->General_model->get_my_lobby_table($lobby_id,$group_bet_id,$user_id);
        $check_lobby_group_user_won_lose = $this->General_model->check_lobby_group_user_won_lose($group_bet_id);
        if (!empty($check_lobby_group_user_won_lose)) {
            $fivmininc = date('Y-m-d H:i:s', time());
        }
        $this->db->where('group_bet_id', $group_bet_id)->where('user_id', $user_id)->where('status', 1);
        $this->db->update('lobby_group', array(
            'reported' => 1,
            'win_lose_status' => $win_lose_status,
            'updated_at' => $fivmininc
        ));
        if (isset($group_bet_id)) {
            if ($win_lose_status == 'win') {
                $status_result = '1';
            } else if ($win_lose_status == 'lose') {
                $status_result = '0';
            } else if ($win_lose_status == 'admin') {
                $status_result = '2';
            }
            $lobby_bet = $this->General_model->lobby_bet_detail($lobby_id);
            $video_id = $check_lobby_group_user_won_lose->video_id;
            if ($win_lose_status == 'lose') {
                if (!empty($check_lobby_group_user_won_lose)) {
                    if ($status_result == '0' && $check_lobby_group_user_won_lose->win_lose_status == 'win') {
                        $winner_table = $check_lobby_group_user_won_lose->table_cat;
                        $loser_table  = 'RIGHT';
                        if ($winner_table == 'RIGHT') {
                            $loser_table = 'LEFT';
                        }
                        $dt_data = array(
                            'bet_status' => 3,
                            'winner_table' => $winner_table,
                            'loser_table' => $loser_table,
                            'video_id' => $video_id,
                            'win_lose_deta' => date('Y-m-d H:i:s', time()),
                            'is_active' => 0
                        );
                        $this->General_model->update_data('group_bet', $dt_data, array(
                            'id' => $group_bet_id
                        ));
                        $lobby_bet_winner_list = $this->General_model->view_data('lobby_group', array(
                            'table_cat' => $winner_table,
                            'group_bet_id' => $group_bet_id,
                            'status' => 1
                        ));
                        foreach ($lobby_bet_winner_list as $key => $list) {
                            $user_data_where = array(
                                'user_id' => $list['user_id']
                            );
                            $userListDp      = $this->General_model->view_data('user_detail', $user_data_where);
                            foreach ($userListDp as $key => $value) {
                                $update_user = array(
                                    'total_balance' => $value['total_balance'] + (2 * $lobby_bet[0]['price'])
                                );
                                $this->General_model->update_data('user_detail', $update_user, $user_data_where);
                            }
                        }
                        
                        $this->db->select('gb.*');
                        $this->db->from('group_bet gb');
                        $this->db->where('gb.id', $group_bet_id);
                        $this->db->where('gb.lobby_id', $lobby_id);
                        $this->db->where('gb.bet_status', 3);
                        $query               = $this->db->get();
                        $get_duplicate_data  = $query->row();
                        $get_data            = array(
                            'lobby_id' => $get_duplicate_data->lobby_id,
                            'game_id' => $get_duplicate_data->game_id,
                            'game_sub_id' => $get_duplicate_data->game_sub_id,
                            'challenger_table' => $get_duplicate_data->challenger_table,
                            'accepter_table' => $get_duplicate_data->accepter_table,
                            'winner_table' => '',
                            'loser_table' => '',
                            'bet_status' => 1,
                            'challenge_date' => $get_duplicate_data->challenge_date,
                            'accepte_date' => '0000-00-00 00:00:00',
                            'video_id' => '',
                            'is_active' => 1
                        );
                        $group_bet_insert_id = $this->General_model->insert_data('group_bet', $get_data);
                        $options             = array(
                            'select' => 'lg.*,ud.total_balance',
                            'table' => 'user_detail ud',
                            'join' => array(
                                'lobby_group lg' => 'lg.user_id = ud.user_id'
                            ),
                            'where' => array(
                                'lg.group_bet_id' => $group_bet_id,
                                'lg.status' => 1
                            )
                        );
                        $lobby_bet_users     = $this->General_model->commonGet($options);
                        
                        foreach ($lobby_bet_users as $key => $lbusers) {
                            if ($lbusers->total_balance >= $lobby_bet[0]['price'] && $lbusers->total_balance != '0.00' && $lbusers->total_balance != '0') {
                                $join_lobby_data = array(
                                    'group_bet_id' => $group_bet_insert_id,
                                    'table_cat' => $lbusers->table_cat,
                                    'user_id' => $lbusers->user_id,
                                    'is_creator' => $lbusers->is_creator,
                                    'is_controller' => $lbusers->is_controller,
                                    'stream_status' => $lbusers->stream_status,
                                    'stream_channel' => $lbusers->stream_channel,
                                    'status' => 1,
                                    'created_at' => date('Y-m-d H:i:s', time())
                                );
                                $this->General_model->insert_data('lobby_group', $join_lobby_data);
                            }
                        }
                    }
                    $ref_id = 1;
                } else {
                    $ref_id = 0;
                }
                $data = array(
                    'lobby_id' => $lobby_id,
                    'grp_bet_id' => $group_bet_id,
                    'video_url' => $video . '&&' . $user_id,
                    'user_id' => $user_id,
                    'table_cat' => $get_my_table,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'status' => $status_result
                );
                $r    = $this->General_model->insert_data('lobby_video', $data);
            } else if ($win_lose_status == 'admin') {
                if (!empty($check_lobby_group_user_won_lose)) {
                    if ($status_result == '2' && $check_lobby_group_user_won_lose->win_lose_status == 'lose' || $check_lobby_group_user_won_lose->win_lose_status == 'win') {
                        $dt = array(
                            'bet_status' => 2,
                            'video_id' => '',
                            'is_active' => 0
                        );
                        
                        $this->General_model->update_data('group_bet', $dt, array(
                            'id' => $group_bet_id
                        ));
                        $this->db->select('gb.*');
                        $this->db->from('group_bet gb');
                        $this->db->where('gb.id', $group_bet_id);
                        $this->db->where('gb.lobby_id', $lobby_id);
                        $this->db->where('gb.bet_status', 2);
                        $this->db->where('gb.is_active', 0);
                        $query              = $this->db->get();
                        $get_duplicate_data = $query->row();
                        
                        $get_data            = array(
                            'lobby_id' => $get_duplicate_data->lobby_id,
                            'game_id' => $get_duplicate_data->game_id,
                            'game_sub_id' => $get_duplicate_data->game_sub_id,
                            'challenger_table' => $get_duplicate_data->challenger_table,
                            'accepter_table' => $get_duplicate_data->accepter_table,
                            'winner_table' => '',
                            'loser_table' => '',
                            'bet_status' => 1,
                            'challenge_date' => $get_duplicate_data->challenge_date,
                            'accepte_date' => '0000-00-00 00:00:00',
                            'video_id' => '',
                            'is_active' => 1
                        );
                        $group_bet_insert_id = $this->General_model->insert_data('group_bet', $get_data);
                        $options             = array(
                            'select' => 'lg.*,ud.total_balance',
                            'table' => 'user_detail ud',
                            'join' => array(
                                'lobby_group lg' => 'lg.user_id = ud.user_id'
                            ),
                            'where' => array(
                                'lg.group_bet_id' => $group_bet_id,
                                'lg.status' => 1
                            )
                        );
                        $lobby_bet_users     = $this->General_model->commonGet($options);
                        
                        foreach ($lobby_bet_users as $key => $lbusers) {
                            if ($lbusers->total_balance >= $lobby_bet[0]['price'] && $lbusers->total_balance != '0.00' && $lbusers->total_balance != '0') {
                                $join_lobby_data = array(
                                    'group_bet_id' => $group_bet_insert_id,
                                    'table_cat' => $lbusers->table_cat,
                                    'user_id' => $lbusers->user_id,
                                    'is_creator' => $lbusers->is_creator,
                                    'is_controller' => $lbusers->is_controller,
                                    'stream_status' => $lbusers->stream_status,
                                    'stream_channel' => $lbusers->stream_channel,
                                    'status' => 1,
                                    'created_at' => date('Y-m-d H:i:s', time())
                                );
                                $this->General_model->insert_data('lobby_group', $join_lobby_data);
                            }
                        }
                    }
                    $ref_id = 1;
                } else {
                    $ref_id = 0;
                }
                $data = array(
                    'lobby_id' => $lobby_id,
                    'grp_bet_id' => $group_bet_id,
                    'video_url' => $video . '&&' . $user_id,
                    'detail_admin' => $explain_admin,
                    'user_id' => $user_id,
                    'table_cat' => $get_my_table,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'status' => $status_result
                );
                $r    = $this->General_model->insert_data('lobby_video', $data);
            } else if ($win_lose_status == 'win') {
                if (!empty($check_lobby_group_user_won_lose)) {
                    if ($status_result == '1' && $check_lobby_group_user_won_lose->win_lose_status == 'lose') {
                        $winner_table = $check_lobby_group_user_won_lose->table_cat;
                        $loser_table  = 'RIGHT';
                        if ($winner_table == 'RIGHT') {
                            $loser_table = 'LEFT';
                        }
                        $dt = array(
                            'bet_status' => 3,
                            'winner_table' => $loser_table,
                            'loser_table' => $winner_table,
                            'video_id' => '',
                            'is_active' => 0
                        );
                        $this->General_model->update_data('group_bet', $dt, array(
                            'id' => $group_bet_id
                        ));
                        $lobby_bet_winner_list = $this->General_model->view_data('lobby_group', array(
                            'table_cat' => $loser_table,
                            'group_bet_id' => $group_bet_id,
                            'status' => 1
                        ));
                        foreach ($lobby_bet_winner_list as $key => $list) {
                            $user_data_where = array(
                                'user_id' => $list['user_id']
                            );
                            $userListDp      = $this->General_model->view_data('user_detail', $user_data_where);
                            foreach ($userListDp as $key => $value) {
                                $update_user = array(
                                    'total_balance' => $value['total_balance'] + (2 * $lobby_bet[0]['price'])
                                );
                                $this->General_model->update_data('user_detail', $update_user, $user_data_where);
                            }
                        }
                        
                        $this->db->select('gb.*');
                        $this->db->from('group_bet gb');
                        $this->db->where('gb.id', $group_bet_id);
                        $this->db->where('gb.lobby_id', $lobby_id);
                        $this->db->where('gb.bet_status', 3);
                        $query              = $this->db->get();
                        $get_duplicate_data = $query->row();
                        
                        $get_data            = array(
                            'lobby_id' => $get_duplicate_data->lobby_id,
                            'game_id' => $get_duplicate_data->game_id,
                            'game_sub_id' => $get_duplicate_data->game_sub_id,
                            'challenger_table' => $get_duplicate_data->challenger_table,
                            'accepter_table' => $get_duplicate_data->accepter_table,
                            'winner_table' => '',
                            'loser_table' => '',
                            'bet_status' => 1,
                            'challenge_date' => $get_duplicate_data->challenge_date,
                            'accepte_date' => '0000-00-00 00:00:00',
                            'video_id' => '',
                            'is_active' => 1
                        );
                        $group_bet_insert_id = $this->General_model->insert_data('group_bet', $get_data);
                        $options             = array(
                            'select' => 'lg.*,ud.total_balance',
                            'table' => 'user_detail ud',
                            'join' => array(
                                'lobby_group lg' => 'lg.user_id = ud.user_id'
                            ),
                            'where' => array(
                                'lg.group_bet_id' => $group_bet_id,
                                'lg.status' => 1
                            )
                        );
                        $lobby_bet_users     = $this->General_model->commonGet($options);
                        
                        foreach ($lobby_bet_users as $key => $lbusers) {
                            if ($lbusers->total_balance >= $lobby_bet[0]['price'] && $lbusers->total_balance != '0.00' && $lbusers->total_balance != '0') {
                                $join_lobby_data = array(
                                    'group_bet_id' => $group_bet_insert_id,
                                    'table_cat' => $lbusers->table_cat,
                                    'user_id' => $lbusers->user_id,
                                    'is_creator' => $lbusers->is_creator,
                                    'is_controller' => $lbusers->is_controller,
                                    'stream_status' => $lbusers->stream_status,
                                    'stream_channel' => $lbusers->stream_channel,                                    
                                    'status' => 1,
                                    'created_at' => date('Y-m-d H:i:s', time())
                                );
                                $this->General_model->insert_data('lobby_group', $join_lobby_data);
                            }
                        }
                    }
                    $ref_id = 1;
                } else {
                    $ref_id = 0;
                }
                $data = array(
                    'lobby_id' => $lobby_id,
                    'grp_bet_id' => $group_bet_id,
                    'video_url' => $video.'&&'.$user_id,
                    'user_id' => $user_id,
                    'table_cat' => $get_my_table,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'status' => $status_result
                );
                $r    = $this->General_model->insert_data('lobby_video', $data);
            }
        }
        
        $this->db->select('lg.*, gb.lobby_id, ud.name, ud.user_id, l.custom_name');
        $this->db->from('lobby_group lg');
        $this->db->join('user_detail ud', 'ud.user_id=lg.user_id');
        $this->db->join('group_bet gb', 'gb.id=lg.group_bet_id');
        $this->db->join('lobby l', 'l.id=gb.lobby_id');
        $this->db->where('lg.group_bet_id', $group_bet_id);
        $this->db->where('lg.user_id', $user_id);
        $this->db->where('gb.lobby_id', $lobby_id);
        $query                      = $this->db->get();
        $data['get_updated_detail'] = $query->row();
        $data['get_duplicate_data'] = $get_duplicate_data;
        $data['ref_id']             = $ref_id;
        $data['lobby_id']           = $lobby_id;
        echo json_encode($data);
        exit();
    }
    public function report_timer_expire() {
        $lobby_id     = $_POST['lobby_id'];
        $group_bet_id = $_POST['group_bet_id'];
        $lobby_bet = $this->General_model->lobby_bet_detail($lobby_id);
        $check_lobby_group_user_won_lose = $this->General_model->check_lobby_group_user_won_lose($group_bet_id);
        $count_game_bet = $this->General_model->view_data('group_bet', array(
            'id' => $group_bet_id,
            'bet_status' => 2
        ));
        if (count($count_game_bet) == 0) {
            if (isset($lobby_id) && $lobby_id != '' && isset($group_bet_id) && $group_bet_id != '') {
                if ($check_lobby_group_user_won_lose->win_lose_status == 'win') {
                    $winner_table = $check_lobby_group_user_won_lose->table_cat;
                    $loser_table  = 'RIGHT';
                    if ($winner_table == 'RIGHT') {
                        $loser_table = 'LEFT';
                    }
                    $dt = array(
                        'bet_status' => 3,
                        'winner_table' => $winner_table,
                        'loser_table' => $loser_table,
                        'video_id' => $video_id,
                        'win_lose_deta' => date('Y-m-d H:i:s', time()),
                        'is_active' => 0
                    );
                    $this->General_model->update_data('group_bet', $dt, array(
                        'id' => $group_bet_id
                    ));
                    $lobby_bet_winner_list = $this->General_model->view_data('lobby_group', array(
                        'table_cat' => $winner_table,
                        'group_bet_id' => $group_bet_id,
                        'status' => 1
                    ));
                    foreach ($lobby_bet_winner_list as $key => $list) {
                        $user_data_where = array(
                            'user_id' => $list['user_id']
                        );
                        $userListDp      = $this->General_model->view_data('user_detail', $user_data_where);
                        foreach ($userListDp as $key => $value) {
                            $update_user = array(
                                'total_balance' => $value['total_balance'] + (2 * $lobby_bet[0]['price'])
                            );
                            $this->General_model->update_data('user_detail', $update_user, $user_data_where);
                        }
                    }
                    $this->db->select('gb.*');
                    $this->db->from('group_bet gb');
                    $this->db->where('gb.id', $group_bet_id);
                    $this->db->where('gb.lobby_id', $lobby_id);
                    $this->db->where('gb.bet_status', 3);
                    $query              = $this->db->get();
                    $get_duplicate_data = $query->row();
                    
                    $get_data            = array(
                        'lobby_id' => $get_duplicate_data->lobby_id,
                        'game_id' => $get_duplicate_data->game_id,
                        'game_sub_id' => $get_duplicate_data->game_sub_id,
                        'challenger_table' => $get_duplicate_data->challenger_table,
                        'accepter_table' => $get_duplicate_data->accepter_table,
                        'winner_table' => '',
                        'loser_table' => '',
                        'bet_status' => 1,
                        'challenge_date' => $get_duplicate_data->challenge_date,
                        'accepte_date' => '0000-00-00 00:00:00',
                        'video_id' => '',
                        'is_active' => 1
                    );
                    $group_bet_insert_id = $this->General_model->insert_data('group_bet', $get_data);
                    $options             = array(
                        'select' => 'lg.*,ud.total_balance',
                        'table' => 'user_detail ud',
                        'join' => array(
                            'lobby_group lg' => 'lg.user_id = ud.user_id'
                        ),
                        'where' => array(
                            'lg.group_bet_id' => $group_bet_id,
                            'lg.status' => 1
                        )
                    );
                    $lobby_bet_users     = $this->General_model->commonGet($options);
                    foreach ($lobby_bet_users as $key => $lbusers) {
                        if ($lbusers->total_balance >= $lobby_bet[0]['price'] && $lbusers->total_balance != '0.00' && $lbusers->total_balance != '0') {
                            $join_lobby_data = array(
                                'group_bet_id' => $group_bet_insert_id,
                                'table_cat' => $lbusers->table_cat,
                                'user_id' => $lbusers->user_id,
                                'is_creator' => $lbusers->is_creator,
                                'is_controller' => $lbusers->is_controller,
                                'stream_status' => $lbusers->stream_status,
                                'stream_channel' => $lbusers->stream_channel,                                    
                                'status' => 1,
                                'created_at' => date('Y-m-d H:i:s', time())
                            );
                            $this->General_model->insert_data('lobby_group', $join_lobby_data);
                        }
                    }            
                } elseif ($check_lobby_group_user_won_lose->win_lose_status == 'lose') {
                    $winner_table = $check_lobby_group_user_won_lose->table_cat;
                    $loser_table  = 'RIGHT';
                    if ($winner_table == 'RIGHT') {
                        $loser_table = 'LEFT';
                    }
                    $dt = array(
                        'bet_status' => 3,
                        'winner_table' => $loser_table,
                        'loser_table' => $winner_table,
                        'video_id' => '',
                        'is_active' => 0
                    );
                    $this->General_model->update_data('group_bet', $dt, array(
                        'id' => $group_bet_id
                    ));
                    $lobby_bet_winner_list = $this->General_model->view_data('lobby_group', array(
                        'table_cat' => $loser_table,
                        'group_bet_id' => $group_bet_id,
                        'status' => 1
                    ));
                    foreach ($lobby_bet_winner_list as $key => $list) {
                        $user_data_where = array(
                            'user_id' => $list['user_id']
                        );
                        $userListDp      = $this->General_model->view_data('user_detail', $user_data_where);
                        foreach ($userListDp as $key => $value) {
                            $update_user = array(
                                'total_balance' => $value['total_balance'] + (2 * $lobby_bet[0]['price'])
                            );
                            $this->General_model->update_data('user_detail', $update_user, $user_data_where);
                        }
                    }
                    
                    $this->db->select('gb.*');
                    $this->db->from('group_bet gb');
                    $this->db->where('gb.id', $group_bet_id);
                    $this->db->where('gb.lobby_id', $lobby_id);
                    $this->db->where('gb.bet_status', 3);
                    $query              = $this->db->get();
                    $get_duplicate_data = $query->row();
                    
                    $get_data            = array(
                        'lobby_id' => $get_duplicate_data->lobby_id,
                        'game_id' => $get_duplicate_data->game_id,
                        'game_sub_id' => $get_duplicate_data->game_sub_id,
                        'challenger_table' => $get_duplicate_data->challenger_table,
                        'accepter_table' => $get_duplicate_data->accepter_table,
                        'winner_table' => '',
                        'loser_table' => '',
                        'bet_status' => 1,
                        'challenge_date' => $get_duplicate_data->challenge_date,
                        'accepte_date' => '0000-00-00 00:00:00',
                        'video_id' => '',
                        'is_active' => 1
                    );
                    $group_bet_insert_id = $this->General_model->insert_data('group_bet', $get_data);
                    $options             = array(
                        'select' => 'lg.*,ud.total_balance',
                        'table' => 'user_detail ud',
                        'join' => array(
                            'lobby_group lg' => 'lg.user_id = ud.user_id'
                        ),
                        'where' => array(
                            'lg.group_bet_id' => $group_bet_id,
                            'lg.status' => 1
                        )
                    );
                    $lobby_bet_users     = $this->General_model->commonGet($options);
                    
                    foreach ($lobby_bet_users as $key => $lbusers) {
                        if ($lbusers->total_balance >= $lobby_bet[0]['price'] && $lbusers->total_balance != '0.00' && $lbusers->total_balance != '0') {
                            $join_lobby_data = array(
                                'group_bet_id' => $group_bet_insert_id,
                                'table_cat' => $lbusers->table_cat,
                                'user_id' => $lbusers->user_id,
                                'is_creator' => $lbusers->is_creator,
                                'is_controller' => $lbusers->is_controller,
                                'stream_status' => $lbusers->stream_status,
                                'stream_channel' => $lbusers->stream_channel,                                    
                                'status' => 1,
                                'created_at' => date('Y-m-d H:i:s', time())
                            );
                            $this->General_model->insert_data('lobby_group', $join_lobby_data);
                        }
                    }
                } elseif ($check_lobby_group_user_won_lose->win_lose_status == 'admin') {
                    $dt =  array(
                        'bet_status' => 2,
                        'video_id' => '',
                        'is_active' => 0
                    );
                    $dt_whr = array(
                        'id' => $group_bet_id,
                        'lobby_id' => $lobby_id,
                        'bet_status' => 1
                    );
                    $this->General_model->update_data('group_bet', $dt, $dt_whr);

                    $this->db->select('gb.*');
                    $this->db->from('group_bet gb');
                    $this->db->where('gb.id', $group_bet_id);
                    $this->db->where('gb.lobby_id', $lobby_id);
                    $this->db->where('gb.bet_status', 2);
                    $this->db->where('gb.is_active', 0);
                    $this->db->order_by('gb.id');
                    $query               = $this->db->get();
                    $query_data          = $query->row();
                    $get_data            = array(
                        'lobby_id' => $query_data->lobby_id,
                        'game_id' => $query_data->game_id,
                        'game_sub_id' => $query_data->game_sub_id,
                        'challenger_table' => $query_data->challenger_table,
                        'accepter_table' => $query_data->accepter_table,
                        'winner_table' => '',
                        'loser_table' => '',
                        'bet_status' => 1,
                        'challenge_date' => $query_data->challenge_date,
                        'accepte_date' => '0000-00-00 00:00:00',
                        'video_id' => '',
                        'is_active' => 1
                    );
                    $group_bet_insert_id = $this->General_model->insert_data('group_bet', $get_data);
                    $options             = array(
                        'select' => 'lg.*,ud.total_balance',
                        'table' => 'user_detail ud',
                        'join' => array(
                            'lobby_group lg' => 'lg.user_id = ud.user_id'
                        ),
                        'where' => array(
                            'lg.group_bet_id' => $group_bet_id,
                            'lg.status' => 1
                        )
                    );
                    $lobby_bet_users     = $this->General_model->commonGet($options);
                    
                    foreach ($lobby_bet_users as $key => $lbusers) {
                        if ($lbusers->total_balance >= $lobby_bet[0]['price'] && $lbusers->total_balance != '0.00' && $lbusers->total_balance != '0') {
                            $join_lobby_data = array(
                                'group_bet_id' => $group_bet_insert_id,
                                'table_cat' => $lbusers->table_cat,
                                'user_id' => $lbusers->user_id,
                                'is_creator' => $lbusers->is_creator,
                                'is_controller' => $lbusers->is_controller,
                                'stream_status' => $lbusers->stream_status,
                                'stream_channel' => $lbusers->stream_channel,                            
                                'status' => 1,
                                'created_at' => date('Y-m-d H:i:s', time())
                            );
                            $this->General_model->insert_data('lobby_group', $join_lobby_data);
                        }
                    }
                    $res['get_duplicate_data'] = $query_data;
                }
            }
        }
        $livelobby_id = $this->General_model->view_single_row_single_column('lobby', array('id' => $lobby_id,'status' => 1), 'custom_name')['custom_name'];
        $res['lobby_id']       = $livelobby_id;
        $res['count_game_bet'] = count($count_game_bet);
        echo json_encode($res);
        exit();
    }
    // to Get report timer
    public function getGetReportTimer() {
        $livelobby_id = $this->General_model->view_single_row_single_column('lobby', array('custom_name' => $_POST['lobby_id'],'status' => 1), 'id')['id'];

        $this->db->select('lg.updated_at, gb.*');
        $this->db->from('lobby_group lg');
        $this->db->join('group_bet gb','gb.id=lg.group_bet_id');
        $this->db->where('gb.lobby_id',$livelobby_id);
        $this->db->where('lg.group_bet_id',$_POST['group_bet_id']);
        $this->db->where('lg.play_status',1);
        $this->db->where('lg.status',1);
        $this->db->order_by('lg.updated_at',"ASC");
        $this->db->where('lg.updated_at !=','0000-00-00 00:00:00');
        $query = $this->db->get();
        $srcData['get_timer'] = $query->row();
        $srcData['get_remaining_timer'] = strtotime($srcData['get_timer']->updated_at) - time() + rand(0,10); 
        $srcData['usertable_uid'] = $_SESSION['user_id'];
        $srcData['ref_id'] = $_POST['ref_id'];
        $srcData['lobby_id'] = $_POST['lobby_id'];
        echo json_encode($srcData);
        exit();
    }
    public function send_message_to_lobby_grp() {
        $this->check_user_page_access();
        if (isset($_POST)) {
            $message = $this->General_model->replace_url_to_anchor_tag($_POST['message']);
            if (isset($_FILES['file']) && $_FILES['file'] != '' && $_POST['lobby_id'] != '' && $_POST['user_id'] != '') {
                for($i=0;$i<count($_FILES["file"]["name"]);$i++) {
                    if(isset($_FILES["file"]["name"][$i]) && $_FILES["file"]["name"][$i]!='') {
                        $img=time().basename($_FILES["file"]["name"][$i]);
                        $ext = end((explode(".", $img)));
                        if($ext=='jpeg' || $ext=='png' || $ext=='jpg'|| $ext=='JPEG' || $ext=='PNG' || $ext=='JPG') {
                            $target_file = $this->target_lbygrp_file.$img;
                            move_uploaded_file($_FILES["file"]["tmp_name"][$i], $target_file);
                            $files_name[] = $img;
                        }
                    }
                }
                $files_name = implode(',', $files_name);
                $insert_data = array(
                    'attachment' => $files_name,
                    'message_type' => 'file'
                );
                $message_id  = $this->General_model->insert_data('xwb_messages', $insert_data);
            } else {
                $insert_data = array(
                    'message' => $message,
                    'message_type' => 'message'
                );
                $message_id  = $this->General_model->insert_data('xwb_messages', $insert_data);
            }
            if ($message_id) {
                $lobby_insert_data           = array(
                    'lobby_id' => $_POST['lobby_id'],
                    'user_id' => $_POST['user_id'],
                    'message_id' => $message_id,
                    'status' => 1,
                    'date' => date('Y-m-d H:i:s', time())
                );
                $lobby_group_conversation_id = $this->General_model->insert_data('lobby_group_conversation', $lobby_insert_data);
                if ($lobby_group_conversation_id) {
                    $options                   = array(
                        'select' => 'lgp.*,xm.message,xm.attachment,xm.message_type,ud.name,ud.is_admin',
                        'table' => 'lobby_group_conversation lgp',
                        'join' => array(
                            'xwb_messages xm' => 'xm.id = lgp.message_id',
                            'user_detail ud' => 'ud.user_id = lgp.user_id'
                        ),
                        'where' => array(
                            'lgp.id' => $lobby_group_conversation_id,
                            'lgp.status' => 1
                        ),
                        'single' => true
                    );
                    $srcData['lobby_get_chat'] = $this->General_model->commonGet($options);
                    
                    $fan_tag = $this->General_model->view_single_row('lobby_fans', array('lobby_id' => $_POST['lobby_id'],'user_id' => $_POST['user_id']),'fan_tag');
                    $srcData['fan_tag'] = $fan_tag['fan_tag'];
                    $srcData['session_id']     = $_SESSION['user_id'];
                    $srcData['is_admin']     = $_SESSION['is_admin'];
                    echo json_encode($srcData);
                    exit();
                }
            }
            
        }
    }
    public function auth_lobby_password() {
        $this->check_user_page_access();
        if (isset($_POST)) {
            $lobby_id     = $_POST['lobby_id'];
            $lobby_password = $_POST['lobby_password'];
            $user_id      = $_SESSION['user_id'];

            $options      = array(
                'select' => 'l.*',
                'table' => 'lobby l',
                'where' => array(
                    'l.id' => $lobby_id,
                    'l.lobby_password' => $lobby_password
                )
            );
            $res['auth_pw'] = $this->General_model->commonGet($options);
            if (count($res['auth_pw']) == 0) {
                $res['password_auth'] = 'auth_failed';
                $_SESSION['lobby_password'] = $lobby_password;
            }
            else{
                $res['password_auth'] = 'auth_success';
            }
            $res['user_id'] = $user_id;
            echo json_encode($res);
            exit();
        }
    }
    public function change_game_size() {
        $this->check_user_page_access();
        if (isset($_POST)) {
            $lobby_id     = $_POST['lobby_id'];
            $group_bet_id = $_POST['group_bet_id'];
            $game_size = $_POST['game_size'];
            $res['group_bet_details'] = $this->General_model->view_data('group_bet',array('id'=>$group_bet_id));
            $get_lobby_info = $this->General_model->view_data('lobby',array('id'=>$lobby_id));
            $update_data = array(
                'game_type' => $game_size
            );
            $update_where = array(
                'id'=>$lobby_id
            );
            $game_size_update = $this->General_model->update_data('lobby',$update_data,$update_where);
            if ($game_size_update) {
                $this->reset_lobby_balance($res['group_bet_details'],$_POST['lobby_id'],$_POST['group_bet_id'],$_POST['game_size']);
            }
            $res['refresh'] = 'yes';
            echo json_encode($res);
            exit();
        }
    }
    public function get_lbydt_on_leave() {

        $srcData = $this->footer_choose_table_reset($_POST['lobby_id']);

        $user_id = $_POST['user_id'];
        $this->db->select('lg.*, ud.name, ud.user_id, ud.total_balance');
        $this->db->from('lobby_group lg');
        $this->db->join('user_detail ud','ud.user_id=lg.user_id');
        $this->db->join('group_bet gb','gb.id=lg.group_bet_id');
        $this->db->where('lg.group_bet_id',$_POST['group_bet_id']);
        $this->db->where('gb.lobby_id',$_POST['lobby_id']);
        $this->db->where('gb.is_active',1);
        $query = $this->db->get();
        $srcData['get_detail'] = $query->result_array();

        $this->db->select('l.game_type, l.price');
        $this->db->from('lobby l');
        $this->db->where('l.id',$_POST['lobby_id']);
        $query = $this->db->get();
        $srcData['lobby_game_type']  = $query->result_array();
        $lobb_price = $srcData['lobby_game_type'][0]['price'];

        $this->db->select('lg.*, ud.name, ud.user_id');
        $this->db->from('lobby_group lg');
        $this->db->join('group_bet gb','gb.id=lg.group_bet_id');
        $this->db->join('user_detail ud','ud.user_id=lg.user_id');
        $this->db->where('gb.lobby_id',$_POST['lobby_id']);
        $this->db->where('lg.table_cat','LEFT');
        $this->db->where('lg.group_bet_id',$_POST['group_bet_id']);
        $this->db->where('gb.bet_status',1);
        $lobby_game_bet_left_grp=$this->db->get()->result_array();

        $this->db->select('lg.*, ud.name, ud.user_id');
        $this->db->from('lobby_group lg');
        $this->db->join('group_bet gb','gb.id=lg.group_bet_id');
        $this->db->join('user_detail ud','ud.user_id=lg.user_id');
        $this->db->where('gb.lobby_id',$_POST['lobby_id']);
        $this->db->where('lg.table_cat','RIGHT');
        $this->db->where('lg.group_bet_id',$_POST['group_bet_id']);
        $this->db->where('gb.bet_status',1);
        $lobby_game_bet_right_grp=$this->db->get()->result_array();

        if ($user_id == $_SESSION['streamer_chat_id_set']) {
                unset($_SESSION['lobby_id']);
                unset($_SESSION['streamer_name_set']);
                unset($_SESSION['streamer_chat_id_set']);
                unset($_SESSION['streamer_channel_set']);   
        }

        $del_data       = $this->General_model->delete_data('lobby_video', array(
            'grp_bet_id' => $_POST['lobby_id'],
            'lobby_id' => $_POST['group_bet_id'],
        ));


        $this->db->select('lg.*, ud.name, ud.user_id, ud.total_balance');
        $this->db->from('lobby_group lg');
        $this->db->join('user_detail ud','ud.user_id=lg.user_id');
        $this->db->join('group_bet gb','gb.id=lg.group_bet_id');
        $this->db->where('lg.group_bet_id',$_POST['group_bet_id']);
        $this->db->where('gb.lobby_id',$_POST['lobby_id']);
        $this->db->where('gb.is_active',1);
        $query = $this->db->get();
        $srcData['get_updated_detail'] = $query->result_array();

        $srcData['is_full_play']             = 'no';
        if ($srcData['lobby_bet_detail'][0]['is_full_play'] == 'yes') {
            $srcData['is_full_play']         = 'yes';
        }

        $srcData['post_userid'] = $user_id;
        $srcData['lobby_creator'] = $this->General_model->lobby_creator($_POST['lobby_id']);
        $srcData['usertable_uid'] = $_SESSION['user_id'];
        $srcData['csrf_key'] = $this->security->get_csrf_hash();
        echo json_encode($srcData);
        exit();    
    }
    public function leave_lobby() {
        $this->check_user_page_access();
        if (isset($_POST)) {
            $leave_from_waitinlist = $_POST['leave_from_waitinlist'];
            $lobby_id     = $_POST['lobby_id'];
            $group_bet_id = $_POST['group_bet_id'];
            $user_id      = $_POST['leave_id'];
            $creator      = $_POST['creator'];
            $controller   = $_POST['controller'];
            $autoloss     = $_POST['autoloss'];

            $res['lobby_leaved_users'] = $this->General_model->get_grp_for_me($lobby_id,$user_id,'','single');
            
            $res['user_id'] = $user_id;
            $group_bet_details = $this->General_model->view_data('group_bet',array('id'=>$group_bet_id));
            $get_lobby_info = $this->General_model->view_data('lobby',array('id'=>$lobby_id));
            $get_price = $get_lobby_info[0]['price'];
            
            $lobby_creat_fee = $this->General_model->view_all_data('admin_lobby_fee', 'id', 'asc');
            $play_fee = $lobby_creat_fee[0]['fee_per_game'];
            $final_fee = $get_price*$play_fee/100;

            // lobby banner disable for leave user
             $q_update_data = array(
                'is_banner' => '0',
            );                
            $q_update_data_where = array(
                'user_id' => $user_id,
                'group_bet_id' => $group_bet_id,
            );
            $this->General_model->update_data('lobby_group', $q_update_data, $q_update_data_where);

            if ($autoloss == 'yes') {
                if ($creator == 'yes' || $controller == 'yes') {
                    if ($leave_from_waitinlist == 'no') {
                        $winner_table = 'LEFT';
                        $loser_table = 'RIGHT';
                        if ($res['lobby_leaved_users']->table_cat == 'LEFT') {
                            $winner_table = 'RIGHT';
                            $loser_table = 'LEFT';
                        }
                        $dt_data = array(
                            'winner_table' => $winner_table,
                            'loser_table' => $loser_table,
                            'win_lose_deta' => date('Y-m-d H:i:s', time())
                        );
                        $this->General_model->update_data('group_bet', $dt_data, array(
                            'id' => $group_bet_id
                        ));
                        $options = array(
                            'select' => 'lg.user_id, lg.play_status',
                            'table' => 'group_bet gb',
                            'join' => array('lobby_group lg' => 'lg.group_bet_id = gb.id'),
                            'where' => array('lg.group_bet_id'=>$group_bet_id,'lg.table_cat'=>$winner_table,'gb.bet_status' => 1)
                        );
                        $lobby_bet_get_grp = $this->General_model->commonGet($options);
                        $price_add = $get_price*2;
                    } else {
                        $options = array(
                            'select' => 'lg.user_id, lg.play_status',
                            'table' => 'group_bet gb',
                            'join' => array('lobby_group lg' => 'lg.group_bet_id = gb.id'),
                            'where' => array('lg.group_bet_id'=>$group_bet_id,'gb.bet_status' => 1)
                        );
                        $lobby_bet_get_grp = $this->General_model->commonGet($options);
                        $price_add = $get_price;
                        $cbalance = $this->General_model->view_data('user_detail',array('user_id'=>$user_id));
                        $crbalance = $cbalance[0]['total_balance'] - number_format((float)$price_add, 2, '.', '');
                        $update_crdata = array(
                            'total_balance' => number_format((float)$crbalance, 2, '.', ''),
                        );
                        $this->General_model->update_data('user_detail',$update_crdata,array('user_id'=>$user_id));
                    }
                    $price_add_decimal = number_format((float)$price_add, 2, '.', '');
                    foreach ($lobby_bet_get_grp as $key => $lbgp) {
                        $get_balance = $this->General_model->view_data('user_detail',array('user_id'=>$lbgp->user_id));
                        $update_total_balance = $get_balance[0]['total_balance'];
                        if ($lbgp->play_status == 1) {
                            $update_total_balance = $get_balance[0]['total_balance'] + $price_add_decimal;
                            $update_data = array(
                                'total_balance' => number_format((float)$update_total_balance, 2, '.', ''),
                            );
                            $this->General_model->update_data('user_detail',$update_data,array('user_id'=>$lbgp->user_id));
                        }
                        if ($lbgp->user_id == $user_id) {
                            $res['leaved_total_balance'] = number_format((float)$update_total_balance, 2, '.', '');
                        }
                    }
                }
            } else {
                if ($leave_from_waitinlist == 'no' || ($creator == 'yes' && $leave_from_waitinlist == 'yes')) {
                    $lobby_bet_get_grp = $this->General_model->get_join_group_data($lobby_id,$group_bet_id,'');
                    $price_add = $final_tip_fee + $get_price;
                    $price_add_decimal = number_format((float)$price_add, 2, '.', '');
                    foreach ($lobby_bet_get_grp as $key => $lbgp) {
                        $get_balance = $this->General_model->view_data('user_detail',array('user_id'=>$lbgp['user_id']));
                        $update_total_balance = $get_balance[0]['total_balance'];
                        if ($lbgp['play_status'] == 1) {
                            $update_total_balance = $get_balance[0]['total_balance'] + $price_add_decimal;
                            $update_data = array(
                                'total_balance' => number_format((float)$update_total_balance, 2, '.', ''),
                            );
                            $this->General_model->update_data('user_detail',$update_data,array('user_id'=>$lbgp['user_id']));

                            $del_data       = $this->General_model->delete_data('lobby_fee_colloected', array(
                                'group_bet_id' => $group_bet_id,
                                'lobby_id' => $lobby_id,
                                'user_id' => $user_id
                            ));
                        }
                        $q_update_data = array(
                            'play_status' => 0,
                            'reported' => 0,
                            'win_lose_status' => null,
                            'updated_at' => '0000-00-00 00:00:00',
                        );                
                        $q_update_data_where = array(
                            'user_id' => $lbgp['user_id'],
                            'group_bet_id' => $group_bet_id,
                        );
                        $q = $this->General_model->update_data('lobby_group', $q_update_data, $q_update_data_where);
                    }
                } else if ($creator == 'no' && $leave_from_waitinlist == 'yes') {
                    $this->change_user_waitinglist($lobby_id,$user_id,null,0,'disable',NULL);
                }
            }

            $get_balance = $this->General_model->view_data('user_detail',array('user_id'=>$user_id));
            $update_total_balance = $get_balance[0]['total_balance'];
            $res['leaved_total_balance'] = number_format((float)$update_total_balance, 2, '.', '');                       
            if ($creator == 'no') {
                if ($controller == 'yes') {
                    if ($autoloss == 'yes') {
                        $this->General_model->duplicate_lobby_insert($lobby_id,$group_bet_id,$user_id,$res['lobby_leaved_users']->table_cat);
                        $del_data       = $this->General_model->delete_data('lobby_group', array(
                            'group_bet_id' => $group_bet_id,
                            'user_id' => $user_id
                        ));
                        $res['refresh'] = 'yes';
                    }
                } else {
                    $del_data       = $this->General_model->delete_data('lobby_group', array(
                        'group_bet_id' => $group_bet_id,
                        'user_id' => $user_id
                    ));
                    $res['refresh'] = 'no';
                }
            } else {
                $data=array(
                    'bet_status'=> 0,
                );
                $update = $this->General_model->update_data('group_bet',$data,array('id'=>$group_bet_id));
                if ($update) {
                    $res['refresh'] = 'yes';
                    $update_data = array(
                        'status' => '0'
                    );
                    $update_data_where = array(
                        'id' => $lobby_id
                    );
                    $this->General_model->update_data('lobby', $update_data, $update_data_where);
                }
            }
            echo json_encode($res);
            exit();            
        }
    }
    public function join_lobby_group(){
        $this->check_user_page_access();
        if (!empty($_POST['lobby_id']) && !empty($_POST['group_bet_id']) && !empty($_POST['table_cat']) && !empty($_POST['price_add_decimal'])) {

            $lobby_id = $_POST['lobby_id'];
            $group_bet_id = $_POST['group_bet_id'];
            $table_cat = $_POST['table_cat'];
            $price_add_decimal = $_POST['price_add_decimal'];

            $user_id = $_POST['dragged_userid'] != '' ? $_POST['dragged_userid'] : $_SESSION['user_id'];
            $data['joined_lobby_list'] = $this->General_model->get_my_lobby_list($user_id,'no');
            $get_lobbylist_where_fan = $this->General_model->waitinglist($lobby_id,'',$user_id,'',1);
            $data['session_user_id'] = $_SESSION['user_id'];
            $data['join_user_id'] = $user_id;
            $total_game_count = count($data['joined_lobby_list']);
            if ($total_game_count < 6) {
                foreach ($get_lobbylist_where_fan as $key => $glwf) {
                    if ($glwf->user_id != $glwf->creator_id) {
                        $this->change_user_waitinglist($glwf->lobby_id,$glwf->user_id,null,0,'','');
                    }
                }
            }

            $creator_id = $this->General_model->lobby_creator($lobby_id);
            $get_join_detail = $this->General_model->get_join_detail($group_bet_id,'',$user_id);
            $mywaiting_details = $this->General_model->waitinglist($lobby_id,'',$user_id,'','single');

            $get_defaultstream = $this->General_model->get_stream_setting($user_id,'','default_stream')[0];

            $is_creator = 'no';
            if ($creator_id->user_id == $user_id) {
                $is_creator = 'yes';
            }

            $stream_status = $mywaiting_details->stream_status;
            $stream_channel = $mywaiting_details->stream_channel;

            if (!empty($get_defaultstream) && $get_defaultstream->default_stream_channel != '') {
                $stream_status = 'enable';
                $stream_channel = $get_defaultstream->default_stream_channel;
                $data['get_defaultstream'] = 1;
            }
            $join_lobby_data = array(
                'group_bet_id' => $group_bet_id,
                'table_cat' => $table_cat,
                'is_creator' => $is_creator,
                'user_id' => $user_id,
                'stream_status' => $stream_status,
                'stream_channel' => $stream_channel,
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s',time())
            );

            // player moved vice-versa table
            if (count($get_join_detail) != 0) {
                if ($get_join_detail[0]['is_creator'] == 'yes') {
                    $data['creator_changed_table'] = 'yes';
                }
                $data['change_user_vice_versa'] = 'yes';
                // $del_data = $this->General_model->delete_data('lobby_group', array(
                //     'group_bet_id' => $group_bet_id,
                //     'user_id' => $user_id
                // ));

                $update_user_data = array(
                    // 'group_bet_id' => $group_bet_id,
                    'table_cat' => $table_cat,
                    // 'user_id' => $user_id,
                    // 'reported' => $get_join_detail[0]['reported'],
                    // 'play_status' => $get_join_detail[0]['play_status'],
                    // 'win_lose_status' => $get_join_detail[0]['win_lose_status'],
                    // 'stream_status' => $get_join_detail[0]['stream_status'],
                    // 'stream_channel' => $get_join_detail[0]['stream_channel'],
                    // 'is_controller' => $get_join_detail[0]['is_controller'],
                    // 'is_creator' => $get_join_detail[0]['is_creator'],
                    // 'is_banner' => $get_join_detail[0]['is_banner'],
                    // 'status' => 1,
                    // 'created_at' => date('Y-m-d H:i:s',time())
                );
                $total_game_count = 0;

                $update_data_where = array(
                    'user_id' => $user_id,
                    'group_bet_id' => $group_bet_id,
                );
                $add_user = $this->General_model->update_data('lobby_group', $update_user_data, $update_data_where);
                // $add_user = $this->db->insert('lobby_group', $join_lobby_data);
            } else {
                $add_user = $this->db->insert('lobby_group', $join_lobby_data);
            }

            if ($total_game_count > 5) {
                $data['reached_limit'] = 'yes';
                if ($data['session_user_id'] == $data['join_user_id']) {
                    $this->session->set_flashdata('message_err', 'Already Reached the Limit of joining A Lobby list <br> Please Leave your Existing game in My Challenges Tab. Than Join a New Game.');
                }
                echo json_encode($data);
                exit();
            }

            $get_user_balance = $this->General_model->view_data('user_detail', array(
                'user_id' => $user_id
            ));

            if ((float) $price_add_decimal > (float) $get_user_balance[0]['total_balance']) {
                $data['balance'] = 'less';
                echo json_encode($data);
                exit();
            }

            
            $data['get_join_detail'] = $this->General_model->get_join_detail($group_bet_id,'',$user_id);

            $data['reached_limit'] = 'no';

            if (isset($add_user)) {
                $this->change_user_waitinglist($lobby_id,$user_id,null,0,'','');
                echo json_encode($data);
                exit();
            }
        }
    }
    public function senduser_to_waitinglist() {
        $this->check_user_page_access();
        if (!empty($_POST['lobby_id']) && !empty($_POST['group_bet_id']) && !empty($_POST['table_cat']) && !empty($_POST['price_add_decimal']) && !empty($_POST['user_id'])) {

            $lobby_id = $_POST['lobby_id'];
            $group_bet_id = $_POST['group_bet_id'];
            $table_cat = $_POST['table_cat'];
            $user_id = $_POST['user_id'];

            $price_add_decimal = $_POST['price_add_decimal'];
            $lobby_creat_fee = $this->General_model->view_all_data('admin_lobby_fee', 'id', 'asc');
            $play_fee = $lobby_creat_fee[0]['fee_per_game'];
            $final_fee = $price_add_decimal*$play_fee/100;
            $price_add_decimal = $price_add_decimal + $final_fee;

            $get_join_detail = $this->General_model->get_join_detail($group_bet_id,'',$user_id);
            $senduser_balance = $this->General_model->view_data('user_detail', array(
                'user_id' => $user_id
            ));
            $user_id_balance = $senduser_balance[0]['total_balance'];
            $this->change_user_waitinglist($lobby_id,$user_id,$table_cat,1,'','');
            if ($get_join_detail[0]['play_status'] == 1) {
                $user_id_balance = $user_id_balance + $price_add_decimal;
                $update_data = array(
                    'total_balance' => $user_id_balance
                );
                $update_data_where = array(
                    'user_id' => $user_id
                );
                $update_to = $this->General_model->update_data('user_detail', $update_data, $update_data_where);
            } else if ($get_join_detail[0]['is_creator'] == 'yes') {
                $this->change_user_waitinglist($lobby_id,$user_id,$table_cat,1,$get_join_detail[0]['stream_status'],$get_join_detail[0]['stream_channel']);
                $creator_in_waitinglist = 'yes';
            }

            $this->General_model->delete_data('lobby_group', array(
              'group_bet_id' => $group_bet_id,
              'user_id' => $user_id,
            ));


            $srcData = $this->footer_choose_table_reset($lobby_id);
            // get spectator user
            $get_ticket_arr = array(
                'lobby_id' => $lobby_id,
                'user_id' => $user_id,
                'type' => 2,
                'is_gift' => 0
            );
            $srcData['get_ticket_detail'] = $this->General_model->get_ticket_detail($get_ticket_arr);
            
            $srcData['user_id_balance'] = $user_id_balance;
            $srcData['creator_in_waitinglist'] = $creator_in_waitinglist;
            $srcData['is_full_play'] = 'no';
            if ($srcData['lobby_bet_detail'][0]['is_full_play'] == 'yes') {
                $srcData['is_full_play'] = 'yes';
            }
            $srcData['get_lobby_detail'] = $this->General_model->lobby_details($lobby_id);
            $srcData['get_fan_detail'] = $this->General_model->get_my_fan_tag($lobby_id,$user_id);
            echo json_encode($srcData);
            exit();
      }
    }
    public function get_lobby_singleuser(){
        $this->check_user_page_access();

        if (!empty($_POST['lobby_id']) && !empty($_POST['group_bet_id']) && !empty($_POST['table_cat']) && !empty($_POST['user_id'])) {        

            $group_bet_id = $_POST['group_bet_id'];
            $lobby_id = $_POST['lobby_id'];
            $user_id = $_POST['user_id'];
            $table_cat = $_POST['table_cat'];

            $srcData = $this->footer_choose_table_reset($lobby_id);
            $srcData['get_detail'] = $this->General_model->get_join_detail($group_bet_id,$table_cat,$user_id);
            $srcData['fan_tag'] = $this->General_model->get_my_fan_tag($lobby_id,$user_id)->fan_tag;
            $srcData['get_all_detail'] = $this->General_model->get_join_group_data($lobby_id,$group_bet_id,'');
            $srcData['subscribedlist'] = $this->General_model->subscribedlist($user_id);

            $count_query = count($srcData['get_all_detail']);
            $srcData['get_lobby_group_play'] = $count_query;
            $srcData['creator_id'] = $this->General_model->lobby_creator($lobby_id);
            $srcData['lobby_creator']  = $srcData['creator_id']->user_id;
            $srcData['is_full_play'] = 'no';
            if ($srcData['lobby_bet_detail'][0]['is_full_play'] == 'yes') {
                $srcData['is_full_play'] = 'yes';
            }
            $srcData['post_userid'] = $user_id;
            $srcData['usertable_uid'] = $_SESSION['user_id'];
            // get spectator user
            $get_ticket_arr = array(
                'lobby_id' => $lobby_id,
                'user_id' => $user_id,
                'type' => 2,
                'is_gift' => 0
            );
            $srcData['get_ticket_detail'] = $this->General_model->get_ticket_detail($get_ticket_arr);
            echo json_encode($srcData);
            exit();
        }
    }
    public function get_change_stream_status() {
        $res['update'] = 'yes';
        $user_id = $_POST['user_id'];
        if ($user_id == $_SESSION['streamer_chat_id_set']) {
            unset($_SESSION['lobby_id']);
            unset($_SESSION['streamer_name_set']);
            unset($_SESSION['streamer_chat_id_set']);
            unset($_SESSION['streamer_channel_set']);
            $res['user_id'] = $user_id;
        }
        echo json_encode($res);
        exit();     
    }
    public function change_stream_status() {
        $this->check_user_page_access();
        if (isset($_POST) && isset($_POST['user_id']) && $_POST['user_id'] !='' && isset($_POST['lobby_id']) && $_POST['lobby_id'] !='' && isset($_POST['group_bet_id']) && $_POST['group_bet_id'] !='') {
            $res['lobby_id'] = $_POST['lobby_id'];
            $res['streamer_details'] = $this->General_model->get_join_detail($_POST['group_bet_id'],'','');
            $user_id = $_POST['user_id'];

            $enable_arr = array('stream_status' => 'enable','stream_channel' => $_POST['channel_name']);
            $disable_arr = array('stream_status' => 'disable','stream_channel' => '');

            $update_data = $_POST['channel_name'] !='' ? $enable_arr : $disable_arr;
            $res['status_update_data'] = $_POST['channel_name'] !='' ? 'enable' : 'disable';

            if (isset($_POST['save_stream_check']) && $_POST['save_stream_check'] == 1 && $res['status_update_data'] == 'enable') {

                $get_defaultstream = $this->General_model->get_stream_setting($_POST['user_id'],'','default_stream')[0];
                if (empty($get_defaultstream)) {
                    $insert_data = array(
                        'user_id' => $_SESSION['user_id'],
                        'default_stream_channel' => $_POST['channel_name'],
                        'key_option' => 'default_stream',
                    );
                    $this->General_model->insert_data('stream_settings', $insert_data);
                } else {
                    $update_defaultstream_row = array(
                        'default_stream_channel' => $_POST['channel_name']
                    );
                    $update_defaultstream_where = array('user_id' => $_SESSION['user_id'], 'key_option' => 'default_stream');
                    $this->General_model->update_data('stream_settings', $update_defaultstream_row, $update_defaultstream_where);
                }

                $save_stream_data = array(
                    'stream_channel' => $_POST['channel_name'],
                );
                $save_stream_where = array(
                    'group_bet_id' => $_POST['group_bet_id'],
                    'user_id' => $this->session->userdata('user_id')
                );
                $update = $this->General_model->update_data('lobby_group', $save_stream_data, $save_stream_where);
            }
            
            if (!empty($res['streamer_details'])) {
                $update_data['is_banner'] = 0;
                $update_data_where = array(
                    'group_bet_id' => $_POST['group_bet_id'],
                    'user_id' => $this->session->userdata('user_id')
                );
                $update = $this->General_model->update_data('lobby_group', $update_data, $update_data_where);
                $res['streamer_details'] = array_merge($res['streamer_details'][0],$update_data);
            } else {
                $update_data_where = array(
                    'lobby_id' => $res['lobby_id'],
                    'user_id' => $this->session->userdata('user_id')
                );
                $update = $this->General_model->update_data('lobby_fans', $update_data, $update_data_where);
                $res['stream_update_as_fan'] = 'yes';
            }
            $res['subscribedlist'] = $this->General_model->subscribedlist($_SESSION['user_id']);
            $res['fan_detail'] = $this->General_model->get_my_fan_tag($_POST['lobby_id'],$_SESSION['user_id']);
            echo json_encode($res);
            exit();
        }
    }
    public function change_streamchat_status() {
        $this->check_user_page_access();
        if ($_POST['user_id'] !='' && $_POST['lobby_id'] !='' && $_POST['group_bet_id'] !='' && $_POST['check'] !='' && $_POST['t_streamerchat'] !='' && $_POST['streamer_name'] !='' && $_POST['stream_channel'] !='') 
        {
            if ($_POST['check'] == 'check') 
            { 
                $_SESSION['lobby_id'] = $_POST['lobby_id'];                
                $_SESSION['streamer_name_set'] = $_POST['streamer_name'];
                $_SESSION['streamer_chat_id_set'] = $_POST['t_streamerchat'];
                $_SESSION['streamer_channel_set'] = $_POST['stream_channel'];
               
                $lobby_creator  = $this->General_model->lobby_creator($_POST['lobby_id']);
                $res['defult_creator_name'] = $lobby_creator->stream_channel;
                $_SESSION['defult_chat_arr'][] = array(
                    'lobby_id' => $_POST['lobby_id'],
                    'streamer_name_set' => $_POST['streamer_name'],
                    'streamer_chat_id_set' => $_POST['t_streamerchat'],
                    'streamer_channel_set' => $_POST['stream_channel'],
                    'defult_creator_name' => $lobby_creator->stream_channel,
                ); 
                $res['lobby_id'] = $_SESSION['lobby_id'];
                $res['streamer_name_set'] = $_SESSION['streamer_name_set'];
                $res['streamer_chat_id_set'] = $_SESSION['streamer_chat_id_set'];
                $res['streamer_channel_set'] = $_SESSION['streamer_channel_set'];
                $res['steam_chat_set'] = 'on';
            } else {                
                foreach($_SESSION['defult_chat_arr'] as $k => $v){
                    if (($key = array_search($_SESSION['lobby_id'], $v)) !== false) {
                        unset($_SESSION['defult_chat_arr'][$k]);
                    }
                }
                unset($_SESSION['lobby_id']);
                unset($_SESSION['streamer_name_set']);
                unset($_SESSION['streamer_chat_id_set']);
                unset($_SESSION['streamer_channel_set']);
                $res['steam_chat_set'] = 'off';
            }
        }
        echo json_encode($res);
        exit();
    }
    public function send_live_stream_show_status() {
        $this->check_user_page_access();
        if ($_POST['user_id'] !='' && $_POST['lobby_id'] !='' && $_POST['group_bet_id'] !='' && $_POST['check'] !='' && $_POST['t_streamerchat'] !='' && $_POST['streamer_name'] !='' && $_POST['stream_channel'] !='') {
            if ($_POST['check'] == 'check') {
                if (isset($_SESSION['stream_store'])) {
                    $stream_store[] = array(
                        'lobby_id' => $_POST['lobby_id'],
                        'streamer_name_set' => $_POST['streamer_name'],
                        'streamer_chat_id_set' => $_POST['t_streamerchat'],
                        'streamer_channel_set' => $_POST['stream_channel'],
                    );
                    $_SESSION['stream_store'] = array_merge($_SESSION['stream_store'], $stream_store);
                } else {
                    $stream_store = array(
                        'lobby_id' => $_POST['lobby_id'],
                        'streamer_name_set' => $_POST['streamer_name'],
                        'streamer_chat_id_set' => $_POST['t_streamerchat'],
                        'streamer_channel_set' => $_POST['stream_channel'],
                    );
                    $_SESSION['stream_store'][] = $stream_store;
                }
                $get_detail = $this->General_model->view_data('user_detail', array(
                    'user_id' => $_POST['user_id']
                ));
                $res['getstreamer_detail'] = $get_detail[0];
                $res['lobby_id'] = $_POST['lobby_id'];
                $res['streamer_name_set'] = $_POST['streamer_name'];
                $res['streamer_chat_id_set'] = $_POST['t_streamerchat'];
                $res['streamer_channel_set'] = $_POST['stream_channel'];
                $res['steam_chat_set'] = 'on';
                $res['subscribedlist'] = $this->General_model->subscribedlist($_POST['user_id']);
            } else {
                foreach ($_SESSION['stream_store'] as $key => $ss) {
                    if ($_POST['t_streamerchat'] != $ss['streamer_chat_id_set']) {
                        $stream_store[] = array(
                            'lobby_id' => $ss['lobby_id'],
                            'streamer_name_set' => $ss['streamer_name_set'],
                            'streamer_chat_id_set' => $ss['streamer_chat_id_set'],
                            'streamer_channel_set' => $_POST['streamer_channel_set'],  
                        );
                    }
                }
                $_SESSION['stream_store'] = $stream_store;
                $res['lobby_id'] = $_POST['lobby_id'];
                $res['streamer_name_set'] = $_POST['streamer_name'];
                $res['streamer_chat_id_set'] = $_POST['t_streamerchat'];
                $res['streamer_channel_set'] = $_POST['stream_channel'];
                $res['steam_chat_set'] = 'off';
            }
        }
        $res['table_cat'] = $_POST['table_cat'];
        $res['disable_class'] = $_POST['disable_class'];
        echo json_encode($res);
        exit();
    }
    public function send_tip_to_streamer() {
        $this->check_user_page_access();
        if (isset($_POST) && isset($_POST['amount']) && $_POST['amount'] !='' && isset($_POST['from']) && $_POST['from'] !='' && isset($_POST['lobby_id']) && $_POST['lobby_id'] !='' && isset($_POST['group_bet_id']) && $_POST['group_bet_id'] !='' && isset($_POST['to']) && $_POST['to'] !='') {
            $res['lobby_id'] = $_POST['lobby_id'];
            
            $tip_amount = number_format($_POST['amount'],2,".","");
            
            $get_current_user_balance = $this->General_model->view_data('user_detail', array(
                'user_id' => $_SESSION['user_id']
            ));

            $res['from_total_balance'] = $get_current_user_balance[0]['total_balance'] - $tip_amount;
            
            $res['balance_status'] = '';
            if ($tip_amount>$get_current_user_balance[0]['total_balance']) {
                $res['balance_status'] = 'less';
                echo json_encode($res);
                exit();
            } else {
                $get_streamer_balance = $this->General_model->view_data('user_detail', array(
                    'user_id' => $_POST['to']
                ));
                $res['to_total_balance'] = $get_streamer_balance[0]['total_balance'] + $tip_amount;
                
                $update_data = array(
                        'total_balance' => $res['to_total_balance']
                    );
                $update_data_where = array(
                    'user_id' => $_POST['to']
                );
                $update_to = $this->General_model->update_data('user_detail', $update_data, $update_data_where);

                $update_data = array(
                        'total_balance' => $res['from_total_balance']
                    );
                $update_data_where = array(
                    'user_id' => $_POST['from']
                );
                $update_from = $this->General_model->update_data('user_detail', $update_data, $update_data_where);

                if ($update_to && $update_from) {
                    $res['user_to'] = $_POST['to'];
                    $res['user_from'] = $_POST['from'];
                    echo json_encode($res);
                    exit();
                }
            }
            
        }
    }   
    public function send_tip_to_team() {
       $this->check_user_page_access();
        if (isset($_POST) && $_POST['amount'] !='' && $_POST['from'] !='' && $_POST['lobby_id'] !='' && $_POST['group_bet_id'] !='' && $_POST['to_usr_arr'] !='') {
            $lobby_id = $_POST['lobby_id'];
            $group_bet_id = $_POST['group_bet_id'];
            $to_users = $_POST['to_usr_arr'];
            $res['lobby_id'] = $lobby_id;
            $tip_amount = number_format($_POST['amount'],2,".","");
            
            $get_current_user_balance = $this->General_model->view_data('user_detail', array(
                'user_id' => $_SESSION['user_id']
            ));
            $res['balance_status'] = '';
            if ($tip_amount > $get_current_user_balance[0]['total_balance']) {
                $res['balance_status'] = 'less';
                echo json_encode($res);
                exit();
            } else {
                $deduct_balance = 0;
                $tipsentuser = [];
                $condition = 'user_id =' .$this->session->userdata('user_id');
                //get Defualt tip Image.
                $arr_getmytipicon = $this->General_model->view_single_row_single_column('stream_settings', array('user_id' => $_SESSION['user_id'],'key_option' => 'tip_icon'), 'tip_selicon_img')['tip_selicon_img'];

                //get Defualt tip sound.
                $res['getmytip_sound'] = $this->General_model->view_single_row_single_column('stream_settings', array('user_id' => $_SESSION['user_id'],'key_option' => 'tip_sound'), 'selsound_audio')['selsound_audio'];

                $getmytipicon = ($arr_getmytipicon == '') ? 'tip.png' : $arr_getmytipicon;
                
                // $getmytipicon = $this->General_model->getmytipicon($_SESSION['user_id'],'');
                foreach ($to_users as $key => $guser) {
                    $tipsentuser[] = '<b>'.$this->General_model->get_my_fan_tag($lobby_id,$guser)->fan_tag.'</b>';
                    $get_streamer_balance = $this->General_model->view_data('user_detail', array(
                        'user_id' => $guser
                    ));
                    $lobby_fee = $this->General_model->view_all_data('admin_lobby_fee', 'id', 'asc');
                    $tip_fess_amount = $lobby_fee[0]['tip_fees'];
                    $tip_fees = number_format((float)$tip_fess_amount, 2, '.', '');
                    $final_tip_fee = $tip_amount*$tip_fees/100;
                    $final_tip_fee = number_format((float)$final_tip_fee, 2, '.', '');
                    $deduct_balance = $deduct_balance + $tip_amount;
                    $update_balance = $get_streamer_balance[0]['total_balance'] + $tip_amount - $final_tip_fee;
                    $update_balance = number_format((float)$update_balance, 2, '.', '');
                    $update_data = array(
                        'total_balance' => $update_balance
                    );
                    $update_data_where = array(
                        'user_id' => $guser
                    );
                    $update_to = $this->General_model->update_data('user_detail', $update_data, $update_data_where);

                    $tip_history   = array(
                        'user_from' => $_SESSION['user_id'],
                        'user_to' => $guser,
                        'tip_amt' => $tip_amount,
                        'tip_type' => 'single_user_tip',
                        'lobby_id' => $lobby_id
                    );
                    $inserttip = $this->General_model->insert_data('tip_history',$tip_history);

                }
                if ($inserttip) {
                    $tipsentuser = implode(', ', $tipsentuser);
                    $insert_data = array(
                        'message' => 'Sent Tip to '.$tipsentuser.' <span class="tipamount">$ '.$tip_amount.'</span>',
                        'attachment' => $getmytipicon,
                        'message_type' => 'tipmessage'
                    );
                    $message_id = $this->General_model->insert_data('xwb_messages', $insert_data);
                    if ($message_id) {
                        $lobby_insert_data           = array(
                            'lobby_id' => $lobby_id,
                            'user_id' => $_SESSION['user_id'],
                            'message_id' => $message_id,
                            'status' => 1,
                            'date' => date('Y-m-d H:i:s', time())
                        );
                        $lobby_group_conversation_id = $this->General_model->insert_data('lobby_group_conversation', $lobby_insert_data);
                        if ($lobby_group_conversation_id) {
                            $options                   = array(
                                'select' => 'lgp.*,xm.message,xm.attachment,xm.message_type,ud.name',
                                'table' => 'lobby_group_conversation lgp',
                                'join' => array(
                                    'xwb_messages xm' => 'xm.id = lgp.message_id',
                                    'user_detail ud' => 'ud.user_id = lgp.user_id'
                                ),
                                'where' => array(
                                    'lgp.id' => $lobby_group_conversation_id,
                                    'lgp.status' => 1
                                ),
                                'single' => true
                            );
                            $res['lobby_get_chat'] = $this->General_model->commonGet($options);
                        }
                    }
                }

                $getbalance = $this->General_model->view_data('user_detail', array(
                    'user_id' => $_SESSION['user_id']
                ));
                $update_balance = $getbalance[0]['total_balance'] - $deduct_balance;
                $update_balance = number_format((float)$update_balance, 2, '.', '');
                $update_data = array(
                    'total_balance' => $update_balance
                );
                $update_data_where = array(
                    'user_id' => $_SESSION['user_id']
                );
                $update_to = $this->General_model->update_data('user_detail', $update_data, $update_data_where);

                $res['fan_tag'] = $this->General_model->get_my_fan_tag($lobby_id,$_SESSION['user_id'])->fan_tag;
                $res['session_id'] = $_SESSION['user_id'];

                $res['get_grp_with_balance']  = $this->General_model->get_join_group_data($lobby_id, $group_bet_id);
                $res['sender_id'] = $_SESSION['user_id'];
                $res['update_balance'] = $update_balance;
                echo json_encode($res);
                exit();
            }
        }
    }
    public function change_pass_control() {
        $this->check_user_page_access();
        if(isset($_POST['user_id']) && $_POST['user_id'] != '' && isset($_POST['lobby_id']) && $_POST['lobby_id'] != '' && isset($_POST['group_bet_id']) && $_POST['group_bet_id'] != '' && isset($_POST['pass_to']) && $_POST['pass_to'] != '') {
            $lobby_id = $_POST['lobby_id'];
            $group_bet_id = $_POST['group_bet_id'];
            $pass_to = $_POST['pass_to'];
            $pass_to_created_lobbys = $this->General_model->created_lobbys($pass_to);
            $res['refresh'] == 'no';
            if (!empty($pass_to_created_lobbys)) {
                $res['refresh'] = 'yes';
                $this->session->set_flashdata('message_err', 'This player Already have Live Lobby <br> Unable to pass control.');
                echo json_encode($res);
                exit();
            }
            $get_pass_to_detail = $this->General_model->view_data('user_detail', array(
                'user_id' => $pass_to
            ))[0];

            $amount = number_format($_POST['amount'],2,".","");
            $data['lobby_game_bet_left_grp']   = $this->General_model->lobby_game_bet_left_grp($lobby_id);
            $data['lobby_game_bet_right_grp']  = $this->General_model->lobby_game_bet_right_grp($lobby_id);
            $group_users = array_merge($data['lobby_game_bet_left_grp'],$data['lobby_game_bet_right_grp']);
            foreach ($group_users as $key => $guser) {
               if ($guser['play_status'] == 1) {
                    $get_streamer_balance = $this->General_model->view_data('user_detail', array(
                        'user_id' => $guser['user_id']
                    ));
                    $update_balance = $get_streamer_balance[0]['total_balance'] + $amount;
                    $update_data = array(
                        'total_balance' => $update_balance
                    );
                    $update_data_where = array(
                        'user_id' => $guser['user_id']
                    );
                    $update_to = $this->General_model->update_data('user_detail', $update_data, $update_data_where);
                }
                $update_dt = array(
                    'play_status' => 0,
                    'reported' => 0,
                    'win_lose_status' => NULL,
                    'reported' => 0,
                    'updated_at' => '0000-00-00 00:00:00',
                );
                $update_dt_where = array(
                    'user_id' => $guser['user_id'],
                    'group_bet_id' => $group_bet_id,
                );
                $this->General_model->update_data('lobby_group', $update_dt, $update_dt_where);
            }
            $pass_to_arr = array(
                'is_creator' => 'yes',
            );
            $update_pass_where = array(
                'user_id' => $pass_to,
                'group_bet_id' => $group_bet_id,
            );
            $this->General_model->update_data('lobby_group', $pass_to_arr, $update_pass_where);

            $pass_from =  array(
                'is_creator' => 'no',
            );
            $update_passfrom_where = array(
                'user_id' => $_SESSION['user_id'],
                'group_bet_id' => $group_bet_id,
            );
            $this->General_model->update_data('lobby_group', $pass_from, $update_passfrom_where);
            
            $get_fan_tag = $this->General_model->get_my_fan_tag($lobby_id, $pass_to);
            $update_data_wherelby = array('id' => $lobby_id);
            $updatelby = array('device_id' => $get_fan_tag->fan_tag, 'user_id' => $pass_to, 'custom_name' => 'account'.$get_pass_to_detail['custom_name']);
            $this->General_model->update_data('lobby', $updatelby, $update_data_wherelby);
            $res['lobby_id'] = $lobby_id;
            $res['new_lobby_id'] = 'account'.$get_pass_to_detail['custom_name'];
            $res['group_bet_id'] = $group_bet_id;
            $res['pass_from'] = $_SESSION['user_id'];
            $res['pass_to'] = $pass_to;
            echo json_encode($res);
            exit();
       }
    }
    public function select_head_table() {
          $this->check_user_page_access();
          if($_POST['lobby_id'] != '' && $_POST['group_bet_id'] != '' && $_POST['user_id'] != '' && $_POST['table_cat'] != '') {
                $lobby_id = $_POST['lobby_id'];
                $group_bet_id = $_POST['group_bet_id'];
                $table_cat = $_POST['table_cat'];
                $head_id = $_POST['user_id'];

                $res['get_play_status_lobby_bet'] = $this->General_model->get_play_status_lobby_bet($lobby_id);
                $res['lobby_bet'] = $this->General_model->lobby_bet_detail($lobby_id);
                $res['ready_up'] = 0;
                if ($res['lobby_bet'][0]['game_type']*2 == count($res['get_play_status_lobby_bet']) || $res['lobby_bet'][0]['is_full_play'] == 'yes') {
                    $res['ready_up'] = 1;
                }
                $update_is_controller =  array(
                    'is_controller' => 'no',
                );
                $update_passfrom_where = array(
                    'table_cat' => $table_cat,
                    'group_bet_id' => $group_bet_id,
                );
                $update = $this->General_model->update_data('lobby_group', $update_is_controller, $update_passfrom_where);
                if ($head_id != 0) {
                    $update_is_controller =  array(
                    'is_controller' => 'yes',
                    );
                    $update_passfrom_where = array(
                        'user_id' => $head_id,
                        'group_bet_id' => $group_bet_id,
                    );
                    $this->General_model->update_data('lobby_group', $update_is_controller, $update_passfrom_where);
                }
                $res['lobby_id'] = $lobby_id;
                $res['group_bet_id'] = $group_bet_id;
                $res['head_id'] = $head_id;
                $res['table_cat'] = $table_cat;
                echo json_encode($res);
                exit();                

            }
    }
    public function addUserToEvent() 
    {
        $this->check_user_page_access();

        $redirect_flag = $_POST['is_gift'];
        $type = ($_POST['is_event_flag'] == 1) ? 1 : 2 ;                         
        $price = ($type == 1) ? $_POST['event_price'] : $_POST['spectate_price'];
        

        $lobby_data = $this->General_model->view_data('lobby',array('id'=>$_POST['lobby_id']));
        $calculate_fee = ($type == 1) ? $lobby_data[0]['event_fee'] : $lobby_data[0]['spectator_fee'];

        // $update_total_balance = 0;

        // if($_POST['lobby_creator'] != 0){

        //     $get_balance = $this->General_model->view_data('user_detail',array('user_id'=>$_POST['lobby_creator']));
        //     $update_total_balance = $get_balance[0]['total_balance'];
        // }
       
        if(($_POST['gift_ticket_status'] == 'on') && ($_POST['is_gift'] == 0)){
                   
            $reduce_amount = $_POST['total_balance']  - ($price * 2);
            // $reduce_fee = ($price * 2)*($calculate_fee * 2)/100;
            $reduce_fee = ($calculate_fee * 2);

            $added_amount = $update_total_balance  + ($price * 2) - $reduce_fee;
            
        } else {
            
            $reduce_amount = $_POST['total_balance'] - $price;
            $reduce_fee = $calculate_fee;
            $added_amount = $update_total_balance  + $price - $reduce_fee;                           
        }
        $current_user_update = array(
            'total_balance' =>  $reduce_amount,
        );
        $current_user_where = array(
            'user_id'   =>  $_POST['user_id'],
        );
        $this->General_model->update_data('user_detail',$current_user_update,$current_user_where);
        
        // if($_POST['lobby_creator'] != 0){
        //     $this->General_model->update_data('user_detail',array('total_balance' => $added_amount),array('user_id'   =>  $_POST['lobby_creator']));
        // }

        if($lobby_data[0]['event_fee_creator']){

            $get_fee_col_user = $this->General_model->view_data('user_detail',array('account_no'=>$lobby_data[0]['event_fee_creator']));
            $update_fee_balance = $get_fee_col_user[0]['total_balance'];
            $fee_user_update = array(
                 'total_balance' =>  $update_fee_balance + $added_amount,
            );
            $this->General_model->update_data('user_detail',$fee_user_update,array('user_id' => $get_fee_col_user[0]['user_id']));
        }

        $lobby_fee   = array(
            'user_id'       => $this->session->userdata('user_id'),
            'lobby_id'      => $_POST['lobby_id'],
            'group_bet_id'  => 0,
            'fee'           => $reduce_fee,
            'fee_type'      => 'event_fee',
            'status'        => '1',
            'created'       => date('Y-m-d H:i:s',time()),
        );
        $this->General_model->insert_data('lobby_fee_colloected',$lobby_fee);



        if ($_POST['is_gift'] == 1) {
            $data_history1   = array(
                'user_id'       => $_POST['user_id'],
                'lobby_id'      => $_POST['lobby_id'],           
                'is_gift'       => 1,           
                'gift_to'       => $_POST['gift_to'],           
                'created'       => date('Y-m-d H:i:s',time()),
                'type'          => $type
            );
            $this->General_model->insert_data('events_users',$data_history1);  
        } else {  
            if ($_POST['gift_ticket_account'] != '') {
                 $data_history   = array(
                    array(
                        'user_id'       => $_POST['user_id'],
                        'lobby_id'      => $_POST['lobby_id'],           
                        'is_gift'       => 0,           
                        'gift_to'       => 0,           
                        'created'       => date('Y-m-d H:i:s',time()),
                        'type'          => $type
                    ),
                    array(
                        'user_id'       => $_POST['user_id'],
                        'lobby_id'      => $_POST['lobby_id'],           
                        'is_gift'       => 1,           
                        'gift_to'       => $_POST['gift_to'],           
                        'created'       => date('Y-m-d H:i:s',time()),
                        'type'          => $type
                    )
                );
            }  else {
                $data_history   = array(
                    array(
                        'user_id'       => $_POST['user_id'],
                        'lobby_id'      => $_POST['lobby_id'],           
                        'is_gift'       => 0,           
                        'gift_to'       => 0,           
                        'created'       => date('Y-m-d H:i:s',time()),
                        'type'          => $type
                    )                    
                );
            }
            $this->db->insert_batch('events_users',$data_history); 
        }
        $view_lobby = $this->General_model->view_single_row('lobby', array('id' => $_POST['lobby_id']),'*');
        if ($redirect_flag == 0) {
            redirect(base_url() . 'livelobby/'.$view_lobby['custom_name']);
        } else {
            redirect(base_url());
        }
    }
    public function check_account() {
        $account_no = $_REQUEST['acc_no'];
        $lobby_id = $_REQUEST['lobby_id'];
        $type = $_REQUEST['type'];
        $get_user_by_account_no = $this->General_model->view_single_row('user_detail','account_no',$account_no);
        $view_lby_det = $this->General_model->view_data('lobby', array(
            'id' => $lobby_id,
            'status' => 1
        ));
        if(count($get_user_by_account_no) > 0){

            if($get_user_by_account_no['user_id'] == $view_lby_det[0]['user_id']) {

                $result['msg'] = "This user is the creator of the lobby.so they don't need to ticket for entering the lobby.";
                $result['user_id'] = $get_user_by_account_no['user_id'];
                $result['status'] = 1;
                $result['btn_flg'] = 0;

            } else {
                 // $query = $this->db->query('select count(id) as total from events_users where lobby_id = '.$lobby_id.' and (user_id ='.$get_user_by_account_no['user_id'].' and or gift_to ='.$get_user_by_account_no['user_id']);
                $query = $this->db->query('select count(id) as total from events_users where ((user_id ='.$get_user_by_account_no['user_id'].' and is_gift = 0) or(is_gift = 1 and gift_to ='.$get_user_by_account_no['user_id'].')) and type = "'.$type.'" and lobby_id = '.$lobby_id);
                $is_event_lobby = $query->result_array();
                if($get_user_by_account_no['display_name'] == ''){
                    $name = $get_user_by_account_no['name'];
                } else {
                    $name = $get_user_by_account_no['display_name'];
                }
                $img = base_url().'upload/profile_img/'.$get_user_by_account_no['image'];
            
                $result['user_data'] = '<div class="row user_div"><div class="col-lg-3"></div>
                                        <div class="col-lg-6 friend_profile_img" style="text-align:center;border: 2px solid rgb(255, 110, 45);">
                                        <h4 class="mt12 orange" style="padding: 6px 0px 9px 0px;border-radius: 4px;">Account User Details</h4>
                                            <img src="'.$img.'"  id="k"/>
                                            <h4 class="mt12">'.ucfirst($name).'</h4>
                                            <h4 class="mt12">Acc #'.$get_user_by_account_no['account_no'].'</h4>
                                        </div>
                                    
                                    <div class="text-center col-lg-3">
                                        
                                    </div></div>';
       
                if($is_event_lobby[0]['total'] > 0){
                    $result['msg'] = 'This Account User has been Already a Gifted Ticket.';
                    $result['user_id'] = $get_user_by_account_no['user_id'];
                    $result['status'] = 1;
                    $result['btn_flg'] = 0;
                    

                } else {
                    $result['msg'] = 'This is Valid Account No.';
                    $result['user_id'] = $get_user_by_account_no['user_id'];
                    $result['status'] = 1;
                    $result['btn_flg'] = 1;
                }
            }
           
        } else {
            $result['user_id'] = '';
            $result['status'] = 0;
            $result['msg'] = 'Please Enter valid Account No.';
            $result['user_data'] = '';
            $result['btn_flg'] = 0;
        }
        echo json_encode($result);
    }
    public function reset_lobby_balance($get_lobby_info,$lobby_id,$group_bet_id,$game_size) {
        $get_price = $get_lobby_info[0]['price'];
        $lobby_creat_fee = $this->General_model->view_all_data('admin_lobby_fee', 'id', 'asc');
        $join_fee = $lobby_creat_fee[0]['fee_per_game'];
        $final_fee = $get_price*$join_fee/100;
        $price_add = $final_fee + $get_price;
        $price_add_decimal = number_format((float)$price_add, 2, '.', '');
        $options = array(
        'select' => 'lg.user_id, lg.play_status, lg.is_creator, lg.table_cat',
        'table' => 'group_bet gb',
        'join' => array('lobby_group lg' => 'lg.group_bet_id = gb.id'),
        'where' => array('lg.group_bet_id'=>$group_bet_id,'gb.bet_status' => 1)
        );
        $lobby_bet_get_grp = $this->General_model->commonGet($options);
        foreach ($lobby_bet_get_grp as $key => $lbgp) {
            if ($lbgp->play_status == 1) {
                $get_balance = $this->General_model->view_data('user_detail',array('user_id'=>$lbgp->user_id));
                $update_total_balance = $get_balance[0]['total_balance'] + $price_add_decimal;
                $update_data = array(
                    'total_balance' =>  $update_total_balance,
                );
                $this->General_model->update_data('user_detail',$update_data,array('user_id'=>$lbgp->user_id));
            }
            $q_update_data = array(
                'play_status' => 0,
                'reported' => 0,
                'win_lose_status' => '',
                'updated_at' => '0000-00-00 00:00:00',
            );                
            $q_update_data_where = array(
                'user_id' => $lbgp->user_id,
                'group_bet_id' => $group_bet_id,
            );
            $this->General_model->update_data('lobby_group', $q_update_data, $q_update_data_where);
        }
        for ($i=0; $i < count($lobby_bet_get_grp); $i++) { 
            if ($game_size*2 <= $i) {
                $this->change_user_waitinglist($lobby_id,$lobby_bet_get_grp[$i]->user_id,$lobby_bet_get_grp[$i]->table_cat,1,'','');
                if ($lobby_bet_get_grp[$i]->is_creator == 'yes') {
                    $update_creator = 'yes';
                }
                $this->General_model->delete_data('lobby_group', array(
                    'group_bet_id' => $group_bet_id,
                    'user_id' => $lobby_bet_get_grp[$i]->user_id,
                ));
            }
            if ($update_creator == 'yes') {
                $pass_to_arr = array(
                    'is_creator' => 'yes',
                );
                $update_pass_where = array(
                    'user_id' => $lobby_bet_get_grp[0]->user_id,
                    'group_bet_id' => $group_bet_id,
                );
                $this->General_model->update_data('lobby_group', $pass_to_arr, $update_pass_where);
            }
        }
        $del_data = $this->General_model->delete_data('lobby_video', array(
            'grp_bet_id' => $group_bet_id,
            'lobby_id' => $lobby_id,
        ));
    }
    public function change_user_waitinglist($lobby_id,$user_id,$waiting_table,$option,$stream_status='',$stream_channel='') {
        $lfupdatedata = array(
            'is_waiting' => $option,
            'waiting_table' => $waiting_table,
        );
        if ($stream_status != '') {
            $lfupdatedata = array(
                'is_waiting' => $option,
                'waiting_table' => $waiting_table,
                'stream_channel' => $stream_channel,
                'stream_status' => $stream_status,
            );
        }
        $lfupdatedata_where = array(
            'lobby_id' => $lobby_id,
            'user_id' => $user_id,
        );
        $this->General_model->update_data('lobby_fans', $lfupdatedata, $lfupdatedata_where);
    }
    public function footer_choose_table_reset($lobby_id) {
        $this->check_user_page_access();
        $srcData['get_default_img'] = $this->General_model->get_user_img();
        $srcData['lobby_bet_detail'] = $this->General_model->lobby_bet_detail($lobby_id);
        $srcData['lobby_game_bet_left_grp'] = $this->General_model->lobby_game_bet_left_grp($lobby_id);
        $srcData['lobby_game_bet_right_grp'] = $this->General_model->lobby_game_bet_right_grp($lobby_id);

        if ($srcData['lobby_bet_detail'][0]['game_type']*2 != count($srcData['lobby_game_bet_left_grp'])+count($srcData['lobby_game_bet_right_grp'])) {
            if ($srcData['lobby_bet_detail'][0]['game_type'] == count($srcData['lobby_game_bet_left_grp'])) {
                $srcData['full_table'] = 'LEFT';
                $srcData['remaining_table'] = 'RIGHT';
            }
            if ($srcData['lobby_bet_detail'][0]['game_type'] == count($srcData['lobby_game_bet_right_grp'])) {
                    $srcData['full_table'] = 'RIGHT';
                    $srcData['remaining_table'] = 'LEFT';
            }
        } else {
          $srcData['remaining_table'] = 'FULL';
          $srcData['full_table'] = 'ALL';
        }
        return $srcData;
    }
    protected function wtnglist($lobby_id) {
        $this->check_user_page_access();
        $data['leftwaitinglist']           = $this->General_model->waitinglist($lobby_id,'LEFT');
        $data['rightwaitinglist']          = $this->General_model->waitinglist($lobby_id,'RIGHT');
        $data['fanslist']                  = $this->General_model->waitinglist($lobby_id);

        $data['wlfanarr'] = []; $data['leftstreamdetail'] = []; $data['rightstreamdetail'] = [];
        foreach ($data['fanslist'] as $key => $fl) {
            $is_joined = $this->General_model->get_grp_for_me($lobby_id,$fl->user_id,'','single');
            $streamuserlist = '';
            if ($fl->is_waiting == 1) {
                $data['wlfanarr'][] = $fl->user_id;
                $streamuserlist = $this->General_model->waitinglist($lobby_id,$fl->table_cat,$fl->user_id,1,'single');
            } else if (count($is_joined) == 1) {
                $data['wlfanarr'][] = $fl->user_id;
                $streamuserlist = $is_joined;
            }
            if (strtolower($streamuserlist->table_cat) == 'left') {
                $data['leftstreamdetail'][] = $streamuserlist;
            } else {
                $data['rightstreamdetail'][] = $streamuserlist;
            }
        }
        // get my membership 
        $data['get_my_plan'] = $this->General_model->view_data('player_membership_subscriptions', array(
            'user_id' => $_SESSION['user_id'],
            'payment_status' => 'Active'
        ));
        // get spectator list
        $get_ticket_arr = array(
            'lobby_id' => $lobby_id,
            'type' => 2,
            'is_gift' => 0
        );
        $spectator_list_arr                = $this->General_model->get_ticket_detail($get_ticket_arr);
        $data['spectator_list']            = array_column($spectator_list_arr, 'ticket_user_id');
        return $data;
    }
    public function joinwaitinglist() {
        $this->check_user_page_access();
        if (!empty($_POST)) {
            $user_id = $_SESSION['user_id'];
            // $res['joined_lobby_list'] = $this->General_model->get_my_lobby_list($user_id);
            // $total_game_count = count($res['joined_lobby_list']);
            // if ($total_game_count >= 1) {
            //     $res['reached_limit'] = 'yes';
                // $this->session->set_flashdata('message_err', 'Already Reached the Limit of creating or joining A Lobby/waiting list <br> Please Leave your Existing game in My Challenges Tab. Than Join a New Game.');
            //     $this->session->set_flashdata('message_err', 'Already Reached the Limit of creating A lobby<br> Please Leave your Existing game in My Challenges Tab. Than Join a New Game.');
            //     echo json_encode($res);
            //     exit();
            // }

            $lobby_bet_detail = $this->General_model->lobby_creator($_POST['lobby_id']);
            $lobbyamount = number_format($lobby_bet_detail->price,2,".","");

            $get_current_user_balance = $this->General_model->view_data('user_detail', array(
                'user_id' => $user_id
            ));
            $res['balance'] = 'full';
            if ($lobbyamount > $get_current_user_balance[0]['total_balance']) {
                $res['balance'] = 'less';
                $this->session->set_flashdata('message_err', 'You do not have enough credits to join the waiting list. Please Proceed to Purchase tab for more.');
                echo json_encode($res);
                exit();
            } 
            $res['lobby_id'] = $_POST['lobby_id'];
            $res['user_id'] = $user_id;
            $res['waiting_table'] = $_POST['table'];
            $option = '1';
            $change_status = $this->change_user_waitinglist($res['lobby_id'],$res['user_id'],$res['waiting_table'],$option,'','');
            $res['get_my_fan_tag'] = $this->General_model->get_my_fan_tag($res['lobby_id'],$res['user_id']);
            $get_ticket_arr = array(
                'lobby_id' => $res['lobby_id'],
                'user_id' => $res['user_id'],
                'type' => 2,
                'is_gift' => 0
            );
            $res['get_ticket_detail'] = $this->General_model->get_ticket_detail($get_ticket_arr);
            echo json_encode($res);
            exit();
        }
    }

    public function search_lobby() {
            $system = $this->input->post('system');
            $game_name = $this->input->post('game_name');
            $sub_game_name = $this->input->post('sub_game_name');
            $game_size_var = $this->input->post('game_size');
            $srch_term = $this->input->post('srch_term');
                
            if ($system != '' && $game_size_var == '') {
                $this->db->select('ag.id,ag.game_name');
                $this->db->from('lobby l');
                $this->db->join('admin_game ag','ag.id = l.game_id');
                $this->db->where("l.payment_status='1' AND l.status='1'");
                $this->db->where("l.game_category_id = '".$system."'");
                $this->db->group_by('ag.game_name');
                $this->db->limit(0,5);
                $query_game = $this->db->get();
                foreach ($query_game->result() as $key => $val) {
                    $game_list[] = '<option class="game_list_front_side" value="'.$val->id.'">'.$val->game_name.'</option>';
                    $game_list_id[] = $val->id;
                    $game_list_name[] = $val->game_name;
                }
            }
            if ($game_size_var != '' && $system == '') {
                $this->db->select('ag.id,ag.game_name');
                $this->db->from('lobby l');
                $this->db->join('admin_game ag','ag.id = l.game_id');
                $this->db->where("l.payment_status='1' AND l.status='1'");
                $this->db->where("l.game_type = '".$game_size_var."'");
                $this->db->group_by('ag.game_name');
                $this->db->limit(0,5);
                $query_game = $this->db->get();
                foreach ($query_game->result() as $key => $val) {
                    $game_list[] = '<option class="game_list_front_side" value="'.$val->id.'">'.$val->game_name.'</option>';
                    $game_list_id[] = $val->id;
                    $game_list_name[] = $val->game_name;
                }
            }

            if ($game_size_var != '' && $system != '') {
                $this->db->select('ag.id,ag.game_name');
                $this->db->from('lobby l');
                $this->db->join('admin_game ag','ag.id = l.game_id');
                $this->db->where("l.payment_status='1' AND l.status='1'");
                $this->db->where("l.game_category_id = '".$system."'");
                $this->db->where("l.game_type = '".$game_size_var."'");
                $this->db->group_by('ag.game_name');
                $this->db->limit(0,5);
                $query_game = $this->db->get();
                foreach ($query_game->result() as $key => $val) {
                    $game_list[] = '<option class="game_list_front_side" value="'.$val->id.'">'.$val->game_name.'</option>';
                    $game_list_id[] = $val->id;
                    $game_list_name[] = $val->game_name;
                }
            }

            if ($game_name != '') {
                $this->db->select('asg.id as asgid, asg.*');
                $this->db->from('admin_sub_game asg');
                $this->db->join('lobby l','l.sub_game_id=asg.id');
                $this->db->where("l.game_id = '".$game_name."'");
                $this->db->group_by('asg.sub_game_name');
                $this->db->limit(0,5);
                $query_sub_game = $this->db->get();
                foreach ($query_sub_game->result() as $key => $val) {
                    $sub_game_list[] = '<option class="subgame_list_front_side" value="'.$val->id.'">'.$val->sub_game_name.'</option>'; 
                    $sub_game_list_id[] = $val->id;
                    $sub_game_list_name[] = $val->sub_game_name;
                }
            }
            $this->db->select('agc.*, asg.sub_game_name, l.is_event, l.id,l.game_category_id,l.price,l.description,l.device_id,l.game_type,l.created_at,l.game_id,gi.game_image,ag.game_name,ud.name,l.sub_game_id');
            $this->db->from('lobby l');
            $this->db->join('user_detail ud','l.user_id=ud.user_id');
            $this->db->join('game_image gi','gi.id=l.image_id');
            $this->db->join('admin_game ag','ag.id=l.game_id');
            $this->db->join('admin_game_category agc','agc.id = ag.game_category_id');
            $this->db->join('admin_sub_game asg','asg.id = l.sub_game_id');
            $this->db->where("l.payment_status='1' AND l.status='1'");
            if ($system != '') {
                $this->db->where("agc.id = '".$system."'");
            }
            if ($game_name != '') {
                $this->db->where("asg.game_id = '".$game_name."'");
                // $this->db->where("l.game_id = '".$game_name."'");
            }
            if ($sub_game_name != '') {
                $this->db->where("asg.id = '".$sub_game_name."'");
            }
            if ($game_size_var != '') {
                $this->db->where("l.game_type = '".$game_size_var."'");
            }
            if ($srch_term != '') {
                $this->db->where("l.payment_status='1' AND l.status='1' AND (l.device_id LIKE '%".$srch_term."%' OR ag.game_name LIKE '%".$srch_term."%')" );
            }

            $this->db->limit(0,5);
            $query = $this->db->get();
            $res = $query->result();
            foreach ($res as $key => $value) {
                if($value->game_category_id == 1) {
                    $game_icon = 'XBOX.png';
                } else if($value->game_category_id == 2) {
                    $game_icon = 'PS4.png';
                } else if($value->game_category_id == 3) {
                    $game_icon = 'PC.png';
                }else {
                    $game_icon = '';
                }
                
                $search_game[] = '<li>
                            <div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
                                <img src="'.base_url().'/upload/game/'.$value->game_image.'" alt="">
                            </div>
                            <div class="col-lg-3 col-sm-6 col-md-3 bid-info">
                                <h3>'.$value->game_name.'</h3>
                                <p>'.$value->sub_game_name.'</p>
                                <span class="postby">Tag: '.$value->device_id.'</span> 
                            </div>
                            <div class="col-lg-2 col-sm-3 col-md-2 uplod-day">
                                <p style="margin-top: 5px;"> Gamesize: --'.$value->game_type."V".$value->game_type.'--</p>
                            </div>
                            <div class="col-lg-2 col-sm-4 col-md-2 divPrice">
                                <div class="game-price premium">
                                    <h3>'.CURRENCY_SYMBOL.$value->price.'</h3>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-5 col-md-3 primum-play play-n-rating">
                                <a href="'.base_url().'livelobby/gameAuth/'.$value->id.'">play</a>
                            </div>
                        </li>
                        ';  
                $res['id'][$key] = base_url().'/livelobby/watchLobby/'.$value->id;
                $res['game_image'][$key] = '<img src="'.base_url().'upload/game/'.$value->game_image.'" alt="">';
                $res['is_event'][$key] = $value->is_event;
                $res['sub_game_name'][$key] = $value->sub_game_name;
                $res['game_icon'][$key] = base_url().'upload/game/'.$game_icon;
                $res['device_id'][$key] = $value->device_id;
                $res['game_name'][$key] = $value->game_name;
                $res['game_type'][$key] = $value->game_type;
                $res['price'][$key] = CURRENCY_SYMBOL.number_format((float) $value->price, 2 , '.', '');
            }
            $res['game_list_id'] = $game_list_id;
            $res['game_list_name'] = $game_list_name;
            $res['game_list'] = $game_list;

            $res['subgame_list_id'] = $sub_game_list_id;
            $res['subgame_list_name'] = $sub_game_list_name;
            $res['sub_game_list'] = $sub_game_list;

            $res['search_game'] = $search_game;
            echo json_encode($res);
    }
}