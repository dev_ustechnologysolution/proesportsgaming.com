<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Login extends My_Controller 
{
	function __construct(){
		parent::__construct();
		$this->load->model('User_model');
		$this->load->model('General_model');
		$this->target_dir=BASEPATH.'../upload/profile_img/';
		$this->check_cover_page_access();
	}

	//login 
	public function index() {
		$email = $this->input->post('email', TRUE);
		$password = md5($this->input->post('password', TRUE));
		$data_arr = $this->User_model->authenticate($email,$password);
		$changeactive_flag = $this->User_model->changeactive_flag($data_arr);
		if (isset($data_arr) && $data_arr !='' && $data_arr['ban_option'] == 0) {
			$this->User_model->log_this_login($data_arr);
			$this->session->unset_userdata('login_att');
			redirect(base_url().'dashboard');
			exit;
		} else if (isset($data_arr) && $data_arr !='' && $data_arr['ban_option'] > 0) {
			$data['err']="";
			if ($email != '') {
				$checkbanned = $this->User_model->checkbanned($data_arr);
				$data['err'] ='<h5 style="margin-left: 255px;color: red;">'.$checkbanned.'</h5>';
        	}
			
			$content=$this->load->view('login/login.php',$data,true);
			$this->render($content);
		} else { 
        	$data['err']="";
        	if ($email!='') {
        		$data['err'] ='<h5 style="margin-left: 255px;color: red;">Invalid username or password</h5>';
        	}
			if ($this->session->userdata['login_att'] == '') {
				$this->session->set_userdata('login_att','1');
			} else {
				$user_att = $this->session->userdata['login_att'] + 1;
				$this->session->set_userdata('login_att',$user_att);
			}
			if ($this->session->userdata['login_att'] > 7) {
				$data['err'] ='<h5 style="margin-left: 255px;color: red;">Your Account Has Been Blocked </h5>';
				$user_data	=	array(
					'is_active'	=>	'1',
				);
				$this->General_model->update_data('user',$user_data,array('email'=>$email));
			}
			$content=$this->load->view('login/login.php',$data,true);
			$this->render($content);
        }
	}
	
	//logout
	function logout() {
		$this->User_model->logout_this_login();
		header('location:'.base_url());
    }
}
