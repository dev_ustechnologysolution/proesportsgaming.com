<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include BASEPATH.'../application/controllers/My_Controller.php';

class Mail extends My_Controller 

{

  	function __construct() {

	parent::__construct();

	$this->load->model('User_model');

	$this->target_dir=BASEPATH.'../upload/mail/';

	}

	function index() {
		$this->check_user_page_access();
		$data['all_mail']=$this->get_all_mails($_SESSION['user_id']);
        $data['friendlist']=$this->General_model->friendlist($_SESSION['user_id']);
        // $data['unread_msg']=$this->General_model->count_unread_msg($_SESSION['user_id']);
        $data['banner_data'] = $this->General_model->tab_banner();
        $content=$this->load->view('mail/mail.tpl.php',$data,true);
		$this->render($content);
	}
	
	function get_all_mails($user_id) {
		$result['all_mails'] = $this->User_model->get_all_mails($user_id);
		foreach ($result['all_mails'] as $key => $value) {
			$mail_time_sub = strtotime(date("Y-m-d h:i:s"))-strtotime($value['date']);
			$mail_month = floor($mail_time_sub / 2592000 % 7);
			$mail_days = floor($mail_time_sub / 86400 % 7);
            $mail_hours = floor($mail_time_sub / 3600 % 24);
            $mail_mins = floor($mail_time_sub/ 60 % 60);
            $mail_secs = floor($mail_time_sub % 60);  
            if ($mail_month >= 1) {}
            else
            {
	            if($mail_days == '00'){
	          	if($mail_hours == '00'){
	              if($mail_mins == '00'){
	              		$mail_time = sprintf('%02d sec', $mail_secs);
	                  }
	              else{
	                        $mail_time = sprintf('%02d mins %02d sec', $mail_mins, $mail_secs);
	                  }
	                 }
	              else{
	                     $mail_time = sprintf('%02d hrs %02d mins %02d sec', $mail_hours, $mail_mins, $mail_secs);
	                 }
	             }
	             else{
	                  $mail_time = sprintf('%02d days %02d hrs %02d mins %02d sec', $mail_days, $mail_hours, $mail_mins, $mail_secs);
	             }
	    $attachment=explode(',', $value['attachment']);

	    $get_all_mails[] = '<div id="main_div_'.$value['id'].'"><div class="unread_msg_div" id="'.$value['id'].'">';
        if ($value['mail_status'] == '1') {
		    $get_all_mails[] .= 'Unread';
        }
	    $get_all_mails[] .= '</div><div class="mails_panel panel panel-default" id="'.$value['id'].'"><div class="panel-heading"><h4 class="panel-title view_mail" onclick="fortest()" data-toggle="collapse" data-parent="#accordion" id="'.$value['id'].'" href="#collapse'.$value['id'].'"><a><span class="mail_heading">From '.$value['name'].'</span></a><span class="float_right">'.$mail_time.' ago <a class="trash_mail" id="'.$value['id'].'"><i class="fa fa-trash-o"></i></a></span></h4></div><div id="collapse'.$value['id'].'" class="panel-collapse collapse"><div class="panel-body"><div class="recieved_row"><div class="mail_body"><div class="">'.$value['mail'].'</div>';
        $get_all_mails[] .= '</div></div></div>';
        $get_all_mails[] .= '<div class="panel-body panel-body_attachment_div mail_attachment">';
        if ($value['attachment'] != '' || $value['attachment'] != null) {
        	foreach ($attachment as $key => $v) {
	        	$get_all_mails[] .=  '<a href="'.base_url().'upload/mail/'.$v.'" class="download_attachment" download=""><i class="fa fa-download"></i><img class="attachment_image img img-thumbnail" src="'.base_url().'upload/mail/'.$v.'"></a>';
	        }
        }
        $get_all_mails[] .= '</div><div class="panel-body"><div class="sent_row"></div><div class="reply_box"><form role="form" action="'.base_url().'mail/reply_mail/" class="send_mail_form reply_without_ajax_'.$value['id'].' reply-form" name="myForm" method="POST" enctype="multipart/form-data"><textarea class="" rows="10" name="reply_data"></textarea><input type="hidden" name="user_to" value="'.$value['user_to'].'"><input type="hidden" name="mail_id" value="'.$value['message_id'].'"> <div class="file-upload file-upload_div" id="upload_div_'.$value['id'].'"><div class="file-select"><div class="file-select-button" id="fileName">Choose File</div><div class="file-select-name" id="noFileselect_'.$value['id'].'">No file chosen...</div><input type="file" name="reply_file[]"  data-file="hasfile_0" class="chooseFile" id="'.$value['id'].'"></div><div class="image_output row"></div></div><div class="reply_button"><input type="submit" id="reply" value="Reply" name="reply"><input type="reset" value="Cancel"></div></form></div></div></div></div></div>';
        }
     	
     }
       return $get_all_mails;
	}
	function get_mail($user_id)
    {
		$this->check_user_page_access();
		$result['mails'] = $this->User_model->get_mails($user_id);
    	$result['user_id'] = $user_id;
    	foreach ($result['mails'] as $key => $value) {
    		$mail_time_sub = strtotime(date("Y-m-d h:i:s"))-strtotime($value['date']);
			$mail_month = floor($mail_time_sub / 2592000 % 7);
    		$mail_days = floor($mail_time_sub / 86400 % 7);
            $mail_hours = floor($mail_time_sub / 3600 % 24);
            $mail_mins = floor($mail_time_sub/ 60 % 60);
            $mail_secs = floor($mail_time_sub % 60);  
            if($mail_month == '00'){
    		if($mail_days == '00'){
          	if($mail_hours == '00'){
              if($mail_mins == '00'){
              		$mail_time = sprintf('%02d sec', $mail_secs);
                  }
              else{
                        $mail_time = sprintf('%02d mins %02d sec', $mail_mins, $mail_secs);
                  }
                 }
              else{
                     $mail_time = sprintf('%02d hrs %02d mins %02d sec', $mail_hours, $mail_mins, $mail_secs);
                 }
             }
             else{
                  $mail_time = sprintf('%02d days %02d hrs %02d mins %02d sec', $mail_days, $mail_hours, $mail_mins, $mail_secs);
             }
             }
             else{
                  $mail_time = sprintf('%02d months %02d days %02d hrs %02d mins %02d sec', $mail_month, $mail_days, $mail_hours, $mail_mins, $mail_secs);
             }
        $result['mail_time'][] = $mail_time; 
        
        }

        echo json_encode($result);
        exit();
	}

	function send_mail()
    {
		$this->check_user_page_access();
		$sender_mail=$this->General_model->view_data('user',array('id'=>$this->session->userdata('user_id')));
		$check_mail_categories = $this->User_model->check_mail_categories($_POST['user_to']);
		foreach ($check_mail_categories as $key => $value) {
			if ($value['friends_mail']=='yes') {
				$sub = "Friends mail";
				$send_personal_mail=$this->User_model->send_mail_to_friend($sender_mail[0]['email'],$value['email'],$_POST['mail'],$sub);
			}
		}
		$images_array = '';
		$images = array();
    	foreach ($_FILES["file"]['name'] as $key => $image) 
    	{
    	if ($image!='') {
	    	  $_FILES['file[]']['name'] = $files['name'][$key];
	    	  $images[] = time().basename($image);
	    	  $file[$key]=$images[$key];
			  $file_type[$key] = end((explode(".", $file[$key])));
			  if($file_type[$key]=='gif' || $file_type[$key]=='jpeg' || $file_type[$key]=='png' || $file_type[$key]=='jpg' || $file_type[$key]=='GIF' || $file_type[$key]=='JPEG' || $file_type[$key]=='PNG' || $file_type[$key]=='JPG')
			  {
				$target_file = $this->target_dir.$file[$key];
				move_uploaded_file($_FILES["file"]["tmp_name"][$key], $target_file);
			  }
			  $images_array = implode(',',$images);
	  		}
		}
		$mail_data = array(
					'mail'=>$_POST['mail'],
					'attachment'=>$images_array,
					'status'=>'1'
					);	
		$r=$this->General_model->insert_data('mail_data',$mail_data);

    	$data_out=array(
					'user_id'=>$_SESSION['user_id'],
					'user_to'=>$_POST['user_to'],
					'user_from'=>$_SESSION['user_id'],
					'direction'=>'OUT',
					'mail_type'=>'friends_mail',
					'status'=>'0',
					'date'=>date('Y-m-d h:i:s'),
					'message_id'=>$r
					);	

    	$data_in=array(
					'user_id'=>$_POST['user_to'],
					'user_to'=>$_SESSION['user_id'],
					'user_from'=>$_SESSION['user_id'],
					'direction'=>'IN',
					'mail_type'=>'friends_mail',
					'status'=>'0',
					'date'=>date('Y-m-d h:i:s'),
					'message_id'=>$r
					);	

    	$r.=$this->General_model->insert_data('mail_conversation',$data_out);
    	$r.=$this->General_model->insert_data('mail_conversation',$data_in);

		if($r)
		{
 			redirect(base_url().'mail');		
		}
	}

	function send_mail_to_all()
    {
		$this->check_user_page_access();
		$sender_mail=$this->General_model->view_data('user',array('id'=>$this->session->userdata('user_id')));
    	$user_to = explode(',', $_POST['user_to']);
    	$images_array = '';
		$images = array();
    	foreach ($_FILES["file"]['name'] as $key => $image) 
    	{
    	if ($image!='') {
	    	  $_FILES['file[]']['name'] = $files['name'][$key];
	    	  $images[] = time().basename($image);
	    	  $file[$key]=$images[$key];
			  $file_type[$key] = end((explode(".", $file[$key])));
			  if($file_type[$key]=='gif' || $file_type[$key]=='jpeg' || $file_type[$key]=='png' || $file_type[$key]=='jpg' || $file_type[$key]=='GIF' || $file_type[$key]=='JPEG' || $file_type[$key]=='PNG' || $file_type[$key]=='JPG')
			  {
				$target_file = $this->target_dir.$file[$key];
				move_uploaded_file($_FILES["file"]["tmp_name"][$key], $target_file);
			  }
			  $images_array = implode(',',$images);
	  		}
  		}
		$mail_data = array(
					'mail'=>$_POST['mail'],
					'attachment'=>$images_array,
					'status'=>'0'
					);	
		$r=$this->General_model->insert_data('mail_data',$mail_data);
    	foreach ($user_to as $key => $val) {
		$check_mail_categories = $this->User_model->check_mail_categories($val);
			foreach ($check_mail_categories as $key => $value) {
				if ($value['friends_mail']=='yes') {
					$sub = "Friends mail";
					$send_personal_mail=$this->User_model->send_mail_to_friend($sender_mail[0]['email'],$value['email'],$_POST['mail'],$sub);
				}
			}
				if (!isset($mssg_id)) {
					$mssg_id = $r;
				}
			$data_out=array(
						'user_id'=>$_SESSION['user_id'],
						'user_to'=>$val,
						'user_from'=>$_SESSION['user_id'],
						'direction'=>'OUT',
						'mail_type'=>'friends_mail',
						'status'=>'0',
						'date'=>date('Y-m-d h:i:s'),
						'message_id'=>$mssg_id
						);	

	    	$data_in=array(
						'user_id'=>$val,
						'user_to'=>$_SESSION['user_id'],
						'user_from'=>$_SESSION['user_id'],
						'direction'=>'IN',
						'mail_type'=>'friends_mail',
						'status'=>'0',
						'date'=>date('Y-m-d h:i:s'),
						'message_id'=>$mssg_id
						);	

	    	$r.=$this->General_model->insert_data('mail_conversation',$data_out);
	    	$r.=$this->General_model->insert_data('mail_conversation',$data_in);
	    	}
			if($r)
			{
	 			redirect(base_url().'mail');		
			}
    	exit();
	}
	function reply_mail()
    {
    	$this->check_user_page_access();
		$sender_mail=$this->General_model->view_data('user',array('id'=>$this->session->userdata('user_id')));
		$check_mail_categories = $this->User_model->check_mail_categories($_POST['user_to']);
		foreach ($check_mail_categories as $key => $value) {
			if ($value['friends_mail']=='yes') {
				$sub = 'You have Reply mail';
				$send_personal_mail=$this->User_model->send_mail_to_friend($sender_mail[0]['email'],$value['email'],$_POST['reply_data'],$sub);
			}
		}


		$images_array = '';
		$images = array();
    	foreach ($_FILES["reply_file"]['name'] as $key => $image) 
    	{
    	if ($image!='') {
	    	  $_FILES['reply_file[]']['name'] = $files['name'][$key];
	    	  $images[] = time().basename($image);
	    	  $file[$key]=$images[$key];
			  $file_type[$key] = end((explode(".", $file[$key])));
			  if($file_type[$key]=='gif' || $file_type[$key]=='jpeg' || $file_type[$key]=='png' || $file_type[$key]=='jpg' || $file_type[$key]=='GIF' || $file_type[$key]=='JPEG' || $file_type[$key]=='PNG' || $file_type[$key]=='JPG'){
				$target_file = $this->target_dir.$file[$key];
				move_uploaded_file($_FILES["reply_file"]["tmp_name"][$key], $target_file);
			  }
			  $images_array = implode(',',$images);
	  		}
  		}		

		$mail_data = array(
					'mail'=>$_POST['reply_data'],
					'attachment'=>$images_array,
					'status'=>'0'
					);	
		$r=$this->General_model->insert_data('mail_data',$mail_data);

    	$data_out=array(
					'user_id'=>$_SESSION['user_id'],
					'user_to'=>$_POST['user_to'],
					'user_from'=>$_SESSION['user_id'],
					'direction'=>'OUT',
					'mail_type'=>'friends_mail',
					'reply_massege_id'=>$r,
					'is_reply'=>'yes',
					'status'=>'0',
					'date'=>date('Y-m-d h:i:s'),
					'message_id'=>$r
					);	

    	$data_in=array(
					'user_id'=>$_POST['user_to'],
					'user_to'=>$_SESSION['user_id'],
					'user_from'=>$_SESSION['user_id'],
					'direction'=>'IN',
					'mail_type'=>'friends_mail',
					'reply_massege_id'=>$r,
					'is_reply'=>'yes',
					'status'=>'0',
					'date'=>date('Y-m-d h:i:s'),
					'message_id'=>$r
					);	

    	$r.=$this->General_model->insert_data('mail_conversation',$data_out);
    	$r.=$this->General_model->insert_data('mail_conversation',$data_in);

		if($r)
		{
 			redirect(base_url().'mail');		
		}
	}
	function search_friends_mail()
    {
		$this->check_user_page_access();
		$search_friends_mail = $this->input->post('search_friends_mail');

		$this->db->select('u.*, ud.*, fl.*');
        $this->db->from('user u');
        $this->db->join('user_detail ud','ud.user_id=u.id');
        $this->db->join('friend_list fl','fl.friend_id=ud.user_id');
		$this->db->where('fl.user_id',$_SESSION['user_id']);
		$this->db->where('fl.request','0');
        if ($search_friends_mail != '' || $search_friends_mail != null) {
        	if (is_numeric($search_friends_mail)) {
        	$this->db->where("fl.friend_id LIKE '%".$search_friends_mail."%'");
        	} 
        	else {
			$this->db->where("ud.name LIKE '%".$search_friends_mail."%' OR u.email LIKE '%".$search_friends_mail."%' AND fl.user_id = '".$_SESSION['user_id']."' AND fl.request = 0");
        	}
		}
		$query = $this->db->get();
        $res = $query->result();
        foreach ($res as $key => $value) {
        	$unread_msg_count=$this->General_model->count_unread_msg($value->friend_id);
	    	$search_friends_list_mail[] = '<div class="single_friend_in_list"><input type="checkbox" class="select_mail" name="friends[]" id="id_'.$value->friend_id.'" value="'.$value->friend_id.'"> <a href=""><i class="fa fa-envelope-o"></i> <span class="unread_msg">'.count($unread_msg_count).'</span>'.$value->name.'</a></div>';
		}
		$res['search_friends_list_mail'] = $search_friends_list_mail;
		echo json_encode($res);
	}
	function delete_single_mail($id)
	{
		$this->check_user_page_access();
		$view_data = $this->General_model->view_data('mail_conversation',array('id'=>$id));
		$data['id'] = $view_data[0]['id'];
		$view_images = $this->General_model->view_data('mail_data',array('id'=>$view_data[0]['message_id']));
		$data['result']=$this->General_model->delete_data('mail_data',array('id'=>$view_data[0]['message_id']));
		$data['result'].=$this->General_model->delete_data('mail_conversation',array('message_id'=>$view_data[0]['message_id']));
        foreach ($view_images as $key => $value) {
        	$images = explode(',', $value['attachment']);
        	unlink(BASEPATH.'../upload/mail/'.$images[$key]);
        	unlink(BASEPATH.'../upload/challenge_mail/'.$images[$key]);
        }
		echo json_encode($data);
        exit();
	}
	function read_single_mail($id)
	{
		$this->check_user_page_access();
		$view_data = $this->General_model->view_data('mail_conversation',array('id'=>$id));
		$update_data=array(
					'status'=> 0,
				    );
		$this->General_model->update_data('mail_data',$update_data, array('id'=>$view_data[0]['message_id']));
		exit();
	}
	function delete_all_mail($mail_cat,$id)
	{
        $this->check_user_page_access();
		$view_data =$this->General_model->view_data('mail_conversation', array('user_to'=>$id), array('mail_type'=>$mail_cat), array('direction'=>'IN'));
		$delete_mail =$this->General_model->delete_data('mail_conversation', array('user_to'=>$id, 'mail_type'=>$mail_cat, 'direction'=>'IN'));
		foreach ($view_data as $key => $value) {
        	$view_images = $this->General_model->view_data('mail_data',array('id'=>$value['message_id']));
        	$delete_mail .=$this->General_model->delete_data('mail_data',array('id'=>$value['message_id']));
        }
        foreach ($view_images as $key => $value) {
        	$images = explode(',', $value['attachment']);
        	unlink(BASEPATH.'../upload/mail/'.$images[$key]);
        	unlink(BASEPATH.'../upload/challenge_mail/'.$images[$key]);
        }
        exit();
	}
}


