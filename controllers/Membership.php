<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';

class Membership extends My_Controller{
	public function __construct(){
        parent::__construct();       
        $this->load->model('Paypal_payment_model');
    }
    function getMembershipPlan(){
        $this->check_user_page_access();
    	$condition = array('user_id' => $this->session->userdata('user_id'),'payment_status' => 'Active');
    	$user_plan_data = $this->General_model->view_data('player_membership_subscriptions',$condition);
    	if ($user_plan_data[0]['plan_id'] != '') {
            $data['active_plan'] = $user_plan_data[0]['plan_id'];
            $data['transaction_id'] = $user_plan_data[0]['transaction_id']; 
        } else {
            $data['active_plan'] = '0'; 
            $data['transaction_id'] = '0';  
        }
        $data['plan']=$this->General_model->view_all_data('membership','id','desc');
        $content=$this->load->view('membership/membershiplist.tpl.php',$data,true);
        $this->render($content);
    }
    function activeMembershipPlan(){
    	if ($_POST['membership_subscribe_plan'] !='' && $_POST['membership_subscribe_amount'] !='') {
            if (!empty($_POST['membership_payment_id']) && !empty($_POST['membership_subscribed_plan'])) {
                $_SESSION['membership_payment_id'] = $_POST['membership_payment_id'];
                $_SESSION['membership_subscribed_plan'] = $_POST['membership_subscribed_plan'];
            } else {
                unset($_SESSION['membership_payment_id']);
                unset($_SESSION['membership_subscribed_plan']);
            }

            $data['subscribe_amount'] = number_format(($_POST['membership_subscribe_amount'] + $_POST['fees']),2,".","");                     
            $data['subscribe_plan'] = $_POST['membership_subscribe_plan'];            
            $payer_detail = $this->User_model->user_detail();
            $data['payer_email'] = $payer_detail[0]['email'];
           	$data['returnurl'] = $data['cancelurl'] = base_url().'membership/GetPaymentDetails';
           	$data['custom'] = 'subscripltion_plan&'.$_POST['membership_subscribe_plan'].'&title&'.$_POST['title'].'&time_duration&'.$_POST['time_duration'];
           	$data['title'] = $_POST['title'].' Membership Plan.';
           	$data['notifyurl'] = '';
            $data['billingtype'] = 'RecurringPayments';
           	$response = $this->Paypal_payment_model->Set_express_checkout($data);
           	if($response['redirect'] != ''){
           		header("Location:".$response['redirect']);
				exit();
           	} else {
           		$this->error($response['errors']);
           	}          
        }
    }
    function GetPaymentDetails($token=''){
    	if ($_GET['token']) {
            $token = $_GET['token'];
        } 

    	$result = $this->Paypal_payment_model->Get_express_checkout_details($token);
    	
    	if($result['status'] == 1)
    	{
    		$PayPalResult = $result['data'];

	        $implode_custom_data = explode('&', $PayPalResult['CUSTOM']);
	        	        
	        $plan_description = $implode_custom_data[3].' Membership Plan.';
	        
            $billing_duration = $implode_custom_data[5];
	        $response = $this->Paypal_payment_model->CreateRecurringPaymentsProfile($PayPalResult,'Admin',$plan_description,$billing_duration);
	        
	        if($response['status'] == 0) {	           
	            $this->error($response['data']);
	        } else {
	            $this->savePlayerMembership($response['data']);
	        } 
    	} else {
    		$this->error($result['data']);
    	}
    }    
    function savePlayerMembership($getdetails)
    {
    	$condition = array('user_id' => $this->session->userdata('user_id'),'payment_status' => 'Active');
    	$user_plan_data = $this->General_model->view_data('player_membership_subscriptions',$condition);
    	if (count($user_plan_data) > 0) {    	
            $data['membership_payment_id'] = $user_plan_data[0]['transaction_id'];
            $data['membership_subscription_opt'] = 'Unbscribe';
            $data['redirect'] = 'false';
            $this->unsubscribeMembershipplan($data); 
        }

    	$getdetails[]= $this->paypal_pro->GetExpressCheckoutDetails($_GET['token']); 
    	  	
        $implode_custom_data = explode('&', $getdetails[0]['CUSTOM']);
        $plan = $implode_custom_data[1];
        $player_subscriptions_data = array(
            'token_id' => $getdetails[0]['TOKEN'],
            'transaction_id' => $getdetails['PROFILEID'],
            'user_id' => $this->session->userdata('user_id'),
            'plan_id' => $plan,            
            'validity' => '',
            'valid_from' => $getdetails['REQUESTDATA']['PROFILESTARTDATE'],
            'valid_to' => '',
            'amount' => $getdetails[0]['AMT'],
            'currency_code' => $getdetails[0]['CURRENCYCODE'],
            'payer_email' => $getdetails['REQUESTDATA']['EMAIL'],
            'create_at' => date('Y-m-d H:i:s', time()),
            'update_at' => date('Y-m-d H:i:s', time()),
            'payment_status' => 'Active',
            'payment_date' => $getdetails[0]['TIMESTAMP'] 
        );        
        $r = $this->General_model->insert_data('player_membership_subscriptions', $player_subscriptions_data);
       
        if ($r) {
            $data['getdetails'] = $getdetails;
            $data['plan'] = $implode_custom_data[3];            
            $data['subscribe_amount'] = $getdetails['REQUESTDATA']['AMT'];
            $content=$this->load->view('membership/membership_success.tpl.php',$data,true);
            $this->render($content);
        }
    }
    function inactiveMembershipPlan(){
    	if (!empty($_POST)) {
    		$data = $_POST;
    		$this->unsubscribeMembershipplan($data);           
        }
    }
    function unsubscribeMembershipplan($data){
    	$payment_id = $data['membership_payment_id'];            
        $PayPalResponseResult = $this->Paypal_payment_model->get_recurring_payments_profile_details($payment_id);
          
        $subscription_opt = $data['membership_subscription_opt'];
        if ($subscription_opt == 'Unbscribe') {
            $subscription_opt = 'Cancel';
        }
      
        if($PayPalResponseResult['status'] == 1){
        	$PayPalResult = $PayPalResponseResult['data'];
        	
        	if ($PayPalResult['STATUS'] != 'Cancelled') {
           	 	$this->Paypal_payment_model->Manage_recurring_payments_profile_status($payment_id,$subscription_opt);
            }
            $this->General_model->update_data('player_membership_subscriptions',array('payment_status' => 'Inactive','payment_date' => date('Y-m-d H:i:s', time())), array('transaction_id' => $payment_id));
            if(isset($data['redirect']))
            {
            	return 1;
            } else {
            	redirect(base_url().'Membership/getMembershipPlan');
            }
            
        } else {
        	$this->error($PayPalResponseResult['data']);
        }
    }
    function error($errors){
        $content=$this->load->view('membership/membership_error.tpl.php',true);
        $this->render($content);
    }
    function getMembershipPlanById(){
        $id = $_POST['id'];
        $data = $this->General_model->view_single_row('membership','id',$id);
        echo json_encode($data);
        exit();
    }
}

