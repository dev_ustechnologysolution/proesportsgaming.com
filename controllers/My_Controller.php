<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class My_Controller extends CI_Controller {

	public $footer_data;  //decleare as a global variable
	public $footer_logo;
	public $footer_link;
	public $header_menu;
	public $footer_menu;
	public $footer_cs;
	function __construct() {

		parent::__construct();
		$this->load->model('General_model');
		$this->load->library('breadcrumbs');
        $this->load->library('gameimage');
        $this->load->library('pagination');
        $this->load->helper('my_common_helper');
		$this->footer_content();
		$this->menu_mgmt();

		if($this->session->userdata('user_id')){
			$userData = $this->General_model->view_data('user_detail',array('user_id'=>$this->session->userdata('user_id')));
			$this->session->set_userdata('total_balance', $userData[0]['total_balance']);

		}

		
	}

	

	//load view

	public function render($content) {
		$view_data=array('content' =>$content);
		$this->load->view('layout',$view_data);
	}
	

	// login status check

	protected function check_user_page_access() {
		if($this->session->userdata('user_id')=='') {
		    header('location:'.base_url());
            exit;
        }
	}
	
	protected function check_cover_page_access(){
		$cover_data = $this->General_model->view_all_data('cover','id','asc');
        if ($cover_data[0]['status'] == '1') {
            if (!isset($_SESSION['cover_page_password'])) {
        		header('location:'.base_url().'Home/cover_page');
            }
        }
	}	

	public function footer_content() {			//for showing footer content globally

		$this->footer_data=$this->General_model->view_all_data('footer_content','id','asc');
		
		$this->footer_logo=$this->General_model->view_all_data('logo','id','asc');

		$this->footer_link=$this->General_model->view_all_data('footer_link','id','asc');

		$this->footer_menu=$this->General_model->view_data('menu',array('position'=>2,'status'=>'active'));

		$this->footer_cs=$this->General_model->view_all_data('footer_customer_service','id','asc');
	}

	public function menu_mgmt() {
		
		$this->header_menu=$this->General_model->view_data_by_order('menu',array('position'=>1,'status'=>'active'),'menu_order','desc');

		// echo '<pre>';
		// print_r($this->header_menu);
		// exit();

	}	

}

