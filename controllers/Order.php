<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Order extends My_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Product_model');
        $this->load->model('User_model');
        $this->load->model('Cart_model');
        $this->check_cover_page_access();
        $this->check_user_page_access();
    }
    function index() {
        $data['banner_data'] = $this->General_model->tab_banner();
        $arr = array(
            'user_id' => $this->session->userdata('user_id')
        );
        $data['order_list'] = $this->Product_model->get_my_product($arr);
        // echo "<pre>";
        // print_r($data['order_list']);
        // exit();
        $content = $this->load->view('order/order.tpl.php',$data,true);
        $this->render($content);
    }
    function order_items() {
        if (!empty($_POST)) {
            $id = $_POST['id'];
            $arr = array(
                'user_id' => $this->session->userdata('user_id'),
                'id' => $id
            );
            $order_items = $this->Product_model->get_my_product($arr);
            $plist = '<div class="common_product_list col-sm-12"><ul class="c_product_list">';
            $plist .= '<li>
                <div class="col-sm-1"><lable>Image</lable></div>
                <div class="col-sm-4"><lable>Product Name</lable></div>
                <div class="col-sm-2"><lable>Product Option</lable></div>
                <div class="col-sm-2"><lable>Quantity</lable></div>
                <div class="col-sm-3"><lable>Gross total</lable></div>
            </li>';
            foreach ($order_items as $key => $oi) {
                $attr_div = '';
                if (!empty($oi->product_attributes)) {
                    $att = '';
                    $product_attributes = explode(',', $oi->product_attributes);
                    foreach ($product_attributes as $key => $pa) {
                        $att = explode('_', $pa);
                        $attr_div .= '<div>
                            <h4>
                                <span class="var_title">'.$att[0].'</span> : <span> '.$att[1].'</span>
                            </h4>
                        </div>';
                    }
                }
                $plist .= '<li class="c_product_list_li">
                    <div class="col-md-1 c_prdct_img">
                        <img src="'.base_url().'upload/products/'.$oi->product_image.'" alt="'.$oi->product_image.'" class="img img-responsive">
                    </div>
                    <div class="col-md-4 c_prdct_name">
                        <a href="'.base_url().'Store/view_product/'.$oi->product_id.'">
                            <div>'.$oi->product_name.'</div>
                        </a>
                    </div>
                    <div class="col-md-2 c_slct_varation">'.$attr_div.'</div>
                    <div class="col-md-2 c_prdct_item select_prdct_qnt">
                        <div>'.$oi->qty.'</div>
                    </div>
                    <div class="col-md-3 c_prdct_price">
                        <div>'.$oi->amount.'</div>
                    </div>
                </li>';
            }
            $plist .= '</ul></div>';
            $res['order_item'] = $plist;
            echo json_encode($res);
            exit();
        }
    }
    function cancel_order() {
        if (!empty($_POST)) {
            $update_data = array(
                'order_status' => 'canceled'
            );
            $ordr_cancel = $this->General_model->update_data('orders_tbl', $update_data, array('user_id' => $this->session->userdata('user_id'),'id' => $_POST['order_id']));
            if ($ordr_cancel) {
                $this->session->set_flashdata('message_succ', 'Order canceled successfully');
            } else {
                $this->session->set_flashdata('message_err', 'Unable to cancel Order');
            }
            $res['redirect'] = base_url().'order';
            echo json_encode($res);
            exit();
        }
    }
}