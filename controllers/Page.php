<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include BASEPATH.'../application/controllers/My_Controller.php';

class Page extends My_Controller 

{

	

	public function __construct()

    {

	parent::__construct();

	// $this->check_user_page_access();

    }

//page loading

	public function pageLink($link='')
	{
		try {
			$menu_data=$this->General_model->view_data('menu',array('link'=>$link));
			if($menu_data[0]['type'] == 'dynamic'){	
				if(strtolower($link) == 'home')	{
					header('location:'.base_url());
				} 
				else if(strtolower($link) == 'world_ranking'){
					header('location:'.base_url().'Ranking');
				}	
				else if(strtolower($link) == 'store'){
					header('location:'.base_url().'Store');
				}	
				else if(strtolower($link) == 'live_lobby'){

					header('location:'.base_url().'Live_lobby');
				}	
				else if(strtolower($link) == 'matches'){					
				  header('location:'.base_url().'Matches/index');					
				} else {
					header('location:'.base_url());
				}		
				
			} else {
				$data['page']=$this->General_model->view_data('menu',array('link'=>$link));
				$content=$this->load->view('page/page.tpl.php',$data,true);
				$this->render($content);
			}
		} catch(Exception $e) {
			echo "<pre>"; print_r($e);
		}
		
	}

}

