<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Points extends My_Controller {

	public function __construct() {
		parent::__construct();
		$this->check_user_page_access();
		$this->load->library('paypal_lib');
		$this->load->library('user_agent');
		$this->load->helper('url');
        $this->load->helper('file');
	}

	public function index() {
		// echo 'index';exit
		// $data['news'] = $this->news_model->get_news();
	}

	public function view($slug = NULL) {
		// $data['news_item'] = $this->news_model->get_news($slug);
	}

	public function history(){
		// echo 'test';exit
		// $data['active']='points';
		$points = 0;

		$points = $this->General_model->view_data('user_detail',array('user_id'=>$this->session->userdata('user_id')));

		$data['total_balance'] = $points[0]['total_balance'];

		$dt = $this->General_model->view_data('payment',array('user_id'=>$this->session->userdata('user_id'),'package_id'=>0, 'payment_status'=>'completed'));
		$tempArray = [];
		foreach($dt as $point){
			$temp = array(
					'payer_email' => $point['payer_email'],
					'payment_status' => $point['payment_status'],
					'amount' => $point['amount'],
					'payment_date' => $point['payment_date'],
					'points' => $point['points'],
					'point_title' => 'INDIVIDUAL PURCHASE',
					'point_description' => ''
			);
			array_push($tempArray, $temp);
		}

		// $data['directBuy'] = $dt;


		$this->db->select('*');
		$this->db->from('payment');
		$this->db->join('admin_packages', 'payment.package_id = admin_packages.id');
		$this->db->where('payment.user_id', $this->session->userdata('user_id'));
		$this->db->where('payment.payment_status', 'completed');
		$query = $this->db->get();


		foreach( $query->result_array() as $point){
			 array_push($tempArray,$point);
		}
		usort($tempArray, function($a, $b) {
				$ad = new DateTime($a['payment_date']);
				$bd = new DateTime($b['payment_date']);

				if ($ad == $bd) {
					return 0;
				}

				return $ad > $bd ? -1 : 1;
		});

		$data['paymentsHistory'] = $tempArray;

		$data['banner_data'] = $this->General_model->tab_banner();
		$content=$this->load->view('points/history.php',$data,true);
		$this->render($content);
	}

	public function buypoints() {
		$data['active'] = 'points';
		$data['admin_points_percentage'] = $this->General_model->view_all_data('admin_points_percentage','id','asc');
		$data['packages']=$this->General_model->view_all_data('admin_packages','id','asc');	
		
		$fee_type = 'manual_purchase_fee';
		$data['manual_points_fee']=$this->General_model->view_data('admin_fee_master',array('fee_type'=>$fee_type));

		$data['banner_data'] = $this->General_model->tab_banner();
		$content=$this->load->view('points/buypoints.php',$data,true);
		$this->render($content);
	}

	public function withdrawal() {
		$points = 0;
		$points = $this->General_model->view_data('user_detail',array('user_id'=>$this->session->userdata('user_id')));
		$get_past_withdrawal_data = $this->General_model->get_past_withdrawal_data();
		$twentyfour_hourpls = strtotime("+24 hours", strtotime($get_past_withdrawal_data));
		$data['request_pending'] = 0;
		if (strtotime("now") < $twentyfour_hourpls) {
			$data['request_pending'] = 1;
		}
		$data['total_balance'] = $points[0]['total_balance'];
		$data['active'] = 'points';
		$data['withdraw_request'] = $this->General_model->view_data_by_order('withdraw_request',array('user_id'=>$this->session->userdata('user_id')), 'req_date', 'desc');
		$data['banner_data'] = $this->General_model->tab_banner();
		$content=$this->load->view('points/withdrawal.php',$data,true);
		$this->render($content);
	}

	public function payment($game_id,$points,$package_id){
		// echo $this->input->post('amount', TRUE); exit;
		$order="orderID".time();
		$data['info']=$order;

		if($game_id == 'dpayment'){
			if ($_POST['amount'] < 10) {
				$this->session->set_flashdata('min_msg', 'The Miniumum Amount is $10');
				redirect(base_url().'points/buypoints');
			}
			$fee_type = 'manual_purchase_fee';
			$manual_purchase_fee = $this->General_model->view_data('admin_fee_master',array('fee_type'=>$fee_type));
			$is_exist_in_given_range = 0;
			foreach ($manual_purchase_fee as $key => $mpf) {
				$range = explode('-', $mpf['fee_range']);
				if ($_POST['amount'] >= $range[0] && $_POST['amount'] <= $range[1]) {
					$fee_calc_type = $mpf['fee_calc_type'];
					$fee_value = $mpf['fee_value'];
					if ($mpf['fee_calc_type'] == '%') {
						$amount_tobe_paid = $_POST['amount'] + (($_POST['amount'] * $mpf['fee_value'])/100);
						$fee_lbl = $mpf['fee_value'].$mpf['fee_calc_type'];
						$collected_fees = ($_POST['amount'] * $mpf['fee_value'])/100;
					} else {
						$fee_lbl = $mpf['fee_calc_type'].$mpf['fee_value'];
						$collected_fees = $mpf['fee_value'];
						$amount_tobe_paid = $_POST['amount'] + $mpf['fee_value'];
					}
					$is_exist_in_given_range = 1;
				}
			}
			if ($is_exist_in_given_range == 0) {
				$this->session->set_flashdata('min_msg', 'Please eneter valid amount');
				redirect(base_url().'points/buypoints');
			}
			$data['selectedPackage']['amount'] = $_POST['amount'];
			$data['custom']['points'] = $_POST['amount'];
			$data['selectedPackage']['fee_calc_type'] = $fee_calc_type;
			$data['selectedPackage']['fee_value'] = $fee_value;
			$data['custom']['amount_tobe_added'] = $_POST['amount'];
			$data['custom']['package_id'] = $package_id;
			$data['selectedPackage']['point_title'] = 'Individual purchase';
			$data['selectedPackage']['point_description'] = $fee_lbl .' Fee amount will be added';
			$data['custom']['amount'] = $amount_tobe_paid;
			$data['selectedPackage']['amount_tobe_paid'] = $amount_tobe_paid;
			$data['custom']['collected_fees'] = $collected_fees;
		} else {
			$data['custom']['points'] = $_POST['amount'];
			$data['custom']['package_id'] = $package_id;
			$dt = $this->General_model->view_data('admin_packages',array('id'=>$game_id));
			$data['selectedPackage'] = $dt[0];
			// $data['custom']['collected_fees'] = ($data['selectedPackage']['amount'] * $data['selectedPackage']['percentage'])/100;
			$data['custom']['collected_fees'] = $data['selectedPackage']['percentage'];
			$data['custom']['amount'] = $data['selectedPackage']['amount']+$data['custom']['collected_fees'];
			$data['selectedPackage']['amount_tobe_paid'] = $data['custom']['amount'];
		}

		$data['collected_fees'] = $data['custom']['collected_fees'];
		$data['custom'] = json_encode($data['custom']);
		
		$data['banner_data'] = $this->General_model->tab_banner();
		$content = $this->load->view('points/payment.tpl.php',$data,true);
		$this->render($content);
	}

	public function payment_paypal(){
		//Set variables for paypal form
		$returnURL 	= $_REQUEST['return']; //payment success url
		$cancelURL 	= $_REQUEST['cancel_return'];  //payment cancel url
		$notifyURL 	= $_REQUEST['notify_url']; //ipn url
		$item_name 	= $_REQUEST['invoice'];
		$amount    	= $_REQUEST['amount'];
		$custom    	= $_REQUEST['custom'];
		$paypal_url = $_REQUEST['paypal_url'];
		$userID 	= $this->session->userdata('user_id'); //current user id
		$logo 		= base_url().'assets/frontend/images/logo.png';

		$paydata=array(
			'user_id'		=>	$this->session->userdata('user_id'),
			'game_id'		=> 	'',
			'subgame_id'	=>	'',
			'order_id'		=>	$item_name,
			'payment_date'	=>	date('Y-m-d H:i:s'),
			'payment_status'=>	'due'
		);
		$this->General_model->insert_data('payment',$paydata);

		$paydata['amount'] = $amount;
		$this->General_model->paypal_fallback_data(json_encode($paydata), 'Purchase Package Paypal Checkout');

		$this->paypal_lib->add_field('return', $returnURL);
		$this->paypal_lib->add_field('cancel_return', $cancelURL);
		$this->paypal_lib->add_field('notify_url', $notifyURL);
		$this->paypal_lib->add_field('item_name', $item_name);
		$this->paypal_lib->add_field('custom', $custom);
		$this->paypal_lib->add_field('user_id',  $userID);
		$this->paypal_lib->add_field('amount',  $amount);		
		$this->paypal_lib->image($logo);
		$this->paypal_lib->paypal_url = $paypal_url;

		$this->paypal_lib->paypal_auto_form();
		$paypalInfo	= $this->input->post();
		$paypalURL = $this->paypal_lib->paypal_url;		
		$result	= $this->paypal_lib->curlPost($paypalURL,$paypalInfo);
	}
	
	public function payment_ipn(){
		//paypal return transaction details array
		$paypalInfo	= $this->input->post();
		$data['user_id'] = $paypalInfo['custom'];
		$data['product_id']	= $paypalInfo["item_number"];
		$data['txn_id']	= $paypalInfo["txn_id"];
		$data['payment_gross'] = $paypalInfo["payment_gross"];
		$data['currency_code'] = $paypalInfo["mc_currency"];
		$data['payer_email'] = $paypalInfo["payer_email"];
		$data['payment_status']	= $paypalInfo["payment_status"];

		$paypalURL = $this->paypal_lib->paypal_url;
		$result	= $this->paypal_lib->curlPost($paypalURL,$paypalInfo);

		$this->General_model->paypal_fallback_data(json_encode($result), 'Notify url Data after Paypal payment');

		
		//check whether the payment is verified
		if(preg_match("/VERIFIED/i",$result)){
		    //insert the transaction data into the database
			$this->product->insertTransaction($data);
		}
		
	}
	
	public function payment_success() {
		$custom = json_decode($_REQUEST['custom']);

		$this->General_model->paypal_fallback_data(json_encode($_REQUEST), 'Successful Payment');

		$userData = $this->General_model->view_data('user_detail',array('user_id'=>$_SESSION['user_id']));
		if(property_exists($custom,'amount_tobe_added')){
			// echo 'Individual purchase';
			$amount_to_add = $custom->amount_tobe_added;
		} else {
			// echo 'Package _REQUESThase';
			$amount_to_add = $custom->amount-$custom->collected_fees;
		}
		// print_r(((13 * $custom->percentage) / 100));
		// echo $custom->percentage;
		$totPoints = $userData[0]['total_points'] + $custom->points;
		$totBalance = $userData[0]['total_balance'] + $amount_to_add;
		// echo 'actual amount : '.$_REQUEST['mc_gross'];
		// echo 'amount to add : ';print_r($amount_to_add);
		// exit;
		$updatedPoints = array(
			'updated_time' => date('Y-m-d H:i:s'),
			'total_balance' => $totBalance,
			'total_points' => $totPoints
		);
		$this->General_model->update_data('user_detail',$updatedPoints , array('user_id'=>$_SESSION['user_id']));
		// print_r($totPoints);exit(0);
		//$item_no          = 	$_REQUEST['item_number'];
		$item_transaction   = 	$_REQUEST['txn_id']; // Paypal transaction ID
		$item_price         = 	$_REQUEST['mc_gross']; // Paypal received amount
		$item_currency      = 	$_REQUEST['mc_currency'];
		$payer_email      	= 	$_REQUEST['payer_email'];
		$payment_date      	= 	$_REQUEST['payment_date'];
		$points           	= 	$custom->points;
		$package_id         = 	$custom->package_id;
		//update data when challenger pay payment
		$dat=array(
			'transaction_id'	=>	$item_transaction,
			'amount'			=>	$item_price,
			'currency'			=>	$item_currency,
			'payment_date'		=>	date('Y-m-d H:i:s',strtotime($payment_date)),
			'payer_email'		=>	$payer_email,
			'payment_status'	=>	'completed',
			'fees_percentage'   => 	$custom->fees_percentage,
			'collected_fees'    => 	$custom->collected_fees,
			'points' 			=> 	$points,
			'package_id' 		=> 	$package_id
		);
		
		$this->General_model->update_data('payment',$dat,array('order_id' => $this->input->post('item_name',TRUE)));
		$data['dt']='Payment Successful';
		$data['banner_data'] = $this->General_model->tab_banner();
		$content=$this->load->view('payment/payment_success.tpl.php',$data,true);
		$this->render($content);
	}

	public function withdrawl_request($value='') {
		$get_past_withdrawal_data = $this->General_model->get_past_withdrawal_data();
		$withdraw_points = number_format($_POST['withdraw_points'],2,".","");
		$twentyfour_hourpls = strtotime("+24 hours", strtotime($get_past_withdrawal_data));
		if (strtotime("now") < $twentyfour_hourpls) {
			$this->session->set_userdata('message_succ', 'You can request after 24 hour from your last request');
			redirect(base_url().'points/withdrawal');
			exit();
		}
		$points = $this->General_model->view_data('user_detail',array('user_id'=>$this->session->userdata('user_id')));
		$totBalance = $points[0]['total_balance'];
		
		if(is_null($this->input->post('withdraw_points', TRUE)) || $this->input->post('withdraw_points', TRUE) < 20){
			$this->session->set_flashdata('message_err', 'Amount should not exceed $20!');
			header('location:'.base_url().'points/withdrawal');
		} else if ($totBalance <= 20) {
			$this->session->set_flashdata('message_err', 'Account balance should be greated than <b>$20</b> for withdrawal request!');
			header('location:'.base_url().'points/withdrawal');
		} else if($_POST['withdraw_points'] > $totBalance) {
			$this->session->set_flashdata('message_err', 'Insufficient Balance. Account you try to transaction from does not have enough balance.Required balance is $'.$withdraw_points.' and you have $'.$totBalance.'');
			header('location:'.base_url().'points/withdrawal');
		} else {
			
			$points = $this->General_model->view_data('user_detail',array('user_id'=>$this->session->userdata('user_id')));
			$paydata=array(
				'user_id'			=>	$this->session->userdata('user_id'),
				'amount_requested'	=> 	$withdraw_points,
				'req_date'			=>	date('Y-m-d h:i:s'),
				'created_at'		=>	date('Y-m-d h:i:s')
			);
			$this->General_model->insert_data('withdraw_request',$paydata);
			$points = $this->General_model->view_data('user_detail',array('user_id'=>$this->session->userdata('user_id')));

			$data['total_balance'] = number_format($points[0]['total_balance'],2,".","");
			$this->session->set_flashdata('message_succ', 'Successfully Requested Withdrawal amount of '.$withdraw_points);
			redirect(base_url().'points/withdrawal');
		}
	}
	public function getUserEvents()
	{
		
		$user_id = $this->session->userdata('user_id');
		
		$data['live_events'] = $this->getGiftEventsData(array('eu.user_id' => $user_id,'eu.is_gift' => 0));

		$data['gift_to'] = $this->getGiftEventsData(array('eu.gift_to' => $user_id,'eu.is_gift' => 1));
		$data['gift_giver'] = $this->getGiftEventsData(array('eu.user_id' => $user_id,'eu.is_gift' => 1));

		$data['tournaments'] = array();
		
		$content=$this->load->view('points/myevents.tpl.php',$data,true);
		$this->render($content);
	}
	public function getGiftEventsData($where)
	{
		$this->db->select('ag.game_name, ag.game_description as description,l.spectate_price,l.event_price, gi.game_image, l.is_event,eu.type, eu.lobby_id,eu.is_gift,eu.gift_to,eu.user_id, asg.sub_game_name, l.game_type');
		$this->db->from('lobby l');
		$this->db->join('events_users eu', 'eu.lobby_id = l.id');
		$this->db->join('admin_game ag', 'ag.id = l.game_id');
		$this->db->join('admin_sub_game asg','l.sub_game_id = asg.id');
		$this->db->join('game_image gi','gi.id=l.image_id');
		// $this->db->where('l.is_event', 1);
		$this->db->where($where);
		$this->db->order_by("eu.created","desc");
		// $this->db->group_by('ag.id',ASC);
		// return $this->db->last_query(); die;
		return $this->db->get()->result_array();
	}
	public function updateTicket()
	{
		$lobby_id = $_REQUEST['id'];
		$isLobby = $this->General_model->view_data('lobby',array('id' => $lobby_id,'status' => 1));
		$calculate_fee = ($isLobby[0]['is_event'] == 1) ? $isLobby[0]['event_fee'] : $isLobby[0]['spectator_fee'];

		$event_price = number_format($isLobby[0]['event_price'],2,".","");

		$spectate_price = number_format($isLobby[0]['spectate_price'],2,".","");

		$deduct_price = $event_price - $spectate_price;
		// $event_fee = $deduct_price * $calculate_fee/100; 
		$event_fee = $deduct_price - $calculate_fee; 

		// if($isLobby[0]['event_creator'] != 0){
  //           $this->General_model->update_data('user_detail',array('total_balance' => $deduct_price-$event_fee),array('user_id'   =>  $isLobby[0]['event_creator']));
  //       }
        
        if($lobby_data[0]['event_fee_creator']){

            $get_fee_col_user = $this->General_model->view_data('user_detail',array('account_no'=>$lobby_data[0]['event_fee_creator']));
            $update_fee_balance = $get_fee_col_user[0]['total_balance'];

            $fee_user_update = array(
                 'total_balance' => $deduct_price-$event_fee,
            );
            $this->General_model->update_data('user_detail',$fee_user_update,array('user_id' => $get_fee_col_user[0]['user_id']));
        }
        $lobby_fee   = array(
            'user_id'       => $this->session->userdata('user_id'),
            'lobby_id'      => $lobby_id,
            'group_bet_id'  => 0,
            'fee'           => $event_fee,
            'fee_type'      => 'event_fee',
            'status'        => '1',
            'created'       => date('Y-m-d H:i:s',time()),
        );
        $this->General_model->insert_data('lobby_fee_colloected',$lobby_fee);

		$user_details = $this->General_model->view_data('user_detail',array('user_id'=>$this->session->userdata('user_id')));
		
		$update_user = array('total_balance' => $user_details[0]['total_balance'] - $deduct_price);
		
        $this->General_model->update_data('user_detail', $update_user, array('user_id'=>$this->session->userdata('user_id')));

        $this->db->where('lobby_id',$lobby_id);
        $where = '((user_id ='.$this->session->userdata('user_id').' and is_gift = 0) or(is_gift = 1 and gift_to ='.$this->session->userdata('user_id').')) and type = 2';
        $this->db->where($where);
        $this->db->update('events_users',array('type' => 1,'is_upgrade'=>1,'updated_at' => date('Y-m-d h:i:s')));
        $res['refresh'] = 'yes';
        $res['lobby_custom_name'] = $isLobby[0]['custom_name'];
        
        echo json_encode($res);
        exit();
	}
}
