<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include BASEPATH.'../application/controllers/My_Controller.php';

class Ps4 extends My_Controller {

    public function __construct() {

        parent::__construct();
        // $this->check_user_page_access();
        $this->load->model('My_model');
        $this->load->library('pagination');
        $this->pageLimit=20;
    }

    //page loading

    public function index($type='',$page=0) {
        
        $data['dt']=1;
        // get all game under category xbox
        
        $start = 0;
        $offset = 5;

        /*

        $condition = "1 AND game_category_id='2' LIMIT ".$start.",".$offset;
        $data['gameList']=$this->General_model->admin_playstore_game_list($condition);
        */
        $page_one=$page;
          
        if($type!='' && $type=='game')
        {
           $page=0;
        }
        else if($type!='' && $type=='challange')
        {
           $page_one=0;
        }
        /****************Ps4 game list*****************/
        
        $conditions='';
        $data['gameListDpPoser']='';
        if(isset($_POST['game_id']) && $_POST['game_id']!='')
        {
            $conditions=" AND ag.id='".$_POST['game_id']."'";
            $data['gameListDpPoser']=$_POST['game_id'];
        }

        $data['gameListDp']= $this->My_model->getPcTabGame(-1,$page_one,$category_id=2);
        $data['gameList']= $this->My_model->getPcTabGame($this->pageLimit,$page_one,$category_id=2,$conditions);
        if(!isset($data['gameList']['tot']))
        {
            if($page_one>=$data['gameList']['tot'] && $page_one!=0)
            {
                $page_one=$data['gameList']['tot'] - $this->pageLimit;
                if($page_one<0)
                    $page_one=0;
                header('location:'.base_url().'Ps4/index/game/'.$page_one);
                exit;
            }
        }
        paggingInitialization($this,
            array('base_url'=>base_url().'Ps4/index/game',
                'total_row'=>$data['gameList']['tot'],
                'per_page'=>$this->pageLimit,
                'uri_segment'=>4,
                'next_link'=>'<i class="fa fa-chevron-right"></i>',
                'prev_link'=>'<i class="fa fa-chevron-left"></i>'
            )
        );

        $data['paging_one']= $this->pagination->create_links();
       
        /****************End Ps4 game list*****************/

        /****************Ps4 challange list****************/
        
     
        $conditions='';
        $data['challangeDpPoser']='';
        $data['challangeSubGame']='';
        if(isset($_POST['challange_id']) && $_POST['challange_id']!='')
        {
            $conditions=" AND g.id='".$_POST['challange_id']."'";
            $data['challangeDpPoser']=$_POST['challange_id'];
        }

        if(isset($_POST['sub_game-name']) && $_POST['sub_game-name']!='')
        {
            $conditions=" AND asg.sub_game_name='".$_POST['sub_game-name']."'";
            $data['challangeSubGame']=$_POST['sub_game-name'];
        }

        if(isset($_POST['game_type']) && $_POST['game_type']!='')
        {
            $conditions=" AND g.game_type='".$_POST['game_type']."'";
            $data['challangeGameSize']=$_POST['game_type'];
        }

        if(isset($_POST['challange_id']) && $_POST['challange_id']!='' && isset($_POST['sub_game-name']) && $_POST['sub_game-name']!='')
        {
            $conditions=" AND g.id='".$_POST['challange_id']."' AND asg.sub_game_name='".$_POST['sub_game-name']."'";
            $data['challangeDpPoser']=$_POST['challange_id'];
            $data['challangeSubGame']=$_POST['sub_game-name'];
        }

        if(isset($_POST['sub_game-name']) && $_POST['sub_game-name']!='' && isset($_POST['game_type']) && $_POST['game_type']!='')
        {
            $conditions=" AND g.game_type='".$_POST['game_type']."' AND asg.sub_game_name='".$_POST['sub_game-name']."'";
            $data['challangeSubGame']=$_POST['sub_game-name'];
            $data['challangeGameSize']=$_POST['game_type'];
        }        

        if(isset($_POST['challange_id']) && $_POST['challange_id']!=''  && isset($_POST['game_type']) && $_POST['game_type']!='')
        {
            $conditions=" AND g.id='".$_POST['challange_id']."' AND g.game_type='".$_POST['game_type']."'";
            $data['challangeDpPoser']=$_POST['challange_id'];
            $data['challangeGameSize']=$_POST['game_type'];
        }

        if(isset($_POST['challange_id']) && $_POST['challange_id']!='' && isset($_POST['sub_game-name']) && $_POST['sub_game-name']!='' && isset($_POST['game_type']) && $_POST['game_type']!='')
        {
            $conditions=" AND g.id='".$_POST['challange_id']."' AND asg.sub_game_name='".$_POST['sub_game-name']."' AND g.game_type='".$_POST['game_type']."'";
            $data['challangeDpPoser']=$_POST['challange_id'];
            $data['challangeSubGame']=$_POST['sub_game-name'];
            $data['challangeGameSize']=$_POST['game_type'];
        }        
        if (isset($_POST['srch-term'])) {

            $conditions= "AND g.id LIKE '%".$_POST['srch-term']."%' OR g.payment_status='1' AND g.status='1' AND g.game_category_id='2' AND g.device_id LIKE '%".$_POST['srch-term']."%'"; 
            $data['challangeSearchTerms']=$_POST['srch-term'];

            // $data['playstore_game']=$this->My_model->getPcTabGameChallange($main_conditions);           
        }

        $data['challangetDp']= $this->My_model->getPcTabGameChallange(-1,$page,$category_id=2);
        $data['playstore_game']=$this->My_model->getPcTabGameChallange($this->pageLimit,$page,$category_id=2,$conditions);


        if(!isset($data['playstore_game']['tot']))
        {
            if($page>=$data['playstore_game']['tot'] && $page!=0)
            {
                $page=$data['playstore_game']['tot']-$this->pageLimit;
                if($page<0)
                    $page=0;
                header('location:'.base_url().'Ps4/index/challange/'.$page);
                exit;
            }
        }
        paggingInitialization($this,
            array('base_url'=>base_url().'Ps4/index/challange/',
                'total_row'=>$data['playstore_game']['tot'],
                'per_page'=>$this->pageLimit,
                'uri_segment'=>4,
                'next_link'=>'<i class="fa fa-chevron-right"></i>',
                'prev_link'=>'<i class="fa fa-chevron-left"></i>'
            )
        );
        
      $data['paging_two']= $this->pagination->create_links();
        /****************End Ps4 challange list****************/



        // GET TOTAL NO OF ROWS
        $tot_game = $this->General_model->get_num_rows('game',array('game_category_id'=>2));
        
        // CALCULATE NO OF PAGES
        $tot_page = ceil($tot_game/$offset);
        
        $data['totGame'] = $tot_game;
        $data['totPage'] = $tot_page;
        
        // GET ALL CATEGORY LIST
        $data['catList'] = $this->General_model->view_all_data('admin_game_category','id','ASC');
        
        $content=$this->load->view('ps4/ps4.tpl.php',$data,true);
        $this->render($content);

    }
    
    // pagination function
    public function page($page_id) {
        $data['dt']=1;
        // get all game under category xbox
        
        $offset = 5;
        $start = ($page_id*$offset)-$offset;
        
        $condition = "1 AND game_category_id='2' LIMIT ".$start.",".$offset;
        $data['gameList']=$this->General_model->admin_playstore_game_list($condition);
        
        // GET TOTAL NO OF ROWS
        $tot_game = $this->General_model->get_num_rows('game',array('game_category_id'=>2));
        
        // CALCULATE NO OF PAGES
        $tot_page = ceil($tot_game/$offset);
        
        $data['totGame'] = $tot_game;
        $data['totPage'] = $tot_page;
        
        // GET ALL CATEGORY LIST
        $data['catList'] = $this->General_model->view_all_data('admin_game_category','id','ASC');
        
        $content=$this->load->view('ps4/ps4.tpl.php',$data,true);

        $this->render($content);
    }

}

