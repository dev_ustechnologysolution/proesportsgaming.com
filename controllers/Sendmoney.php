<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include BASEPATH.'../application/controllers/My_Controller.php';

class Sendmoney extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->check_user_page_access();
		$this->load->library(array('form_validation'));
		$this->load->model('General_model');
		$this->target_dir=BASEPATH.'../upload/profile_img/';
	}

	public function index() {
		
		$data['dt']=1;
		$data['list']=$this->General_model->open_challenge_game_list();
		$data['list1']=$this->General_model->playing_game_list();
		for($i=0; $i<count($data['list1']); $i++) {
			$data['video_status'][$i]=$this->General_model->get_num_rows('video', array('game_tbl_id'=>$data['list1'][$i]['id'],'game_id'=>$data['list1'][$i]['game_id'],'user_id'=>$this->session->userdata('user_id')));
		}
		$data['list2']=$this->General_model->winning_game_list();
		$data['list3']=$this->General_model->played_game_list();
		$data['list11']=$this->General_model->view_data('game',array('user_id'=>$this->session->userdata('user_id'),'status'=>1,'payment_status'=>1));

		$data['list4']=$this->General_model->accepter_game_list();

		//Old Game List for Accepter
		$data['list6']=$this->General_model->accepter_old_game_list();
		
		//Old Played Game List
		$data['list5']=$this->General_model->view_data('game',array('user_id'=>$this->session->userdata('user_id'),'status'=>3));
		$data['banner_data'] = $this->General_model->tab_banner();
		$content=$this->load->view('sendmoney/index.tpl.php',$data,true);
		$this->render($content);	

	}

	public function money_request(){
		$this->form_validation->set_rules('transfer_amt', 'Enter Amt', 'required|numeric');
		$this->form_validation->set_rules('account_number', 'Enter the Person Account Number', 'required|numeric');
		if ($this->form_validation->run() == true || $this->form_validation->run() == 1) {
			$transfer_amt 	= $this->input->post('transfer_amt', TRUE);
			$account_number	= $this->input->post('account_number', TRUE);

			$to_id = $this->General_model->view_single_row('user_detail','account_no',$account_number)['user_id'];
			$user_where = array('is_active' => 0);
			$user_details = $this->General_model->view_data('user',$user_where);
			if(!empty($user_details)){
				foreach($user_details as $user_det){
					$get_all_user_id[]	= $user_det['id'];
				}
				if($this->session->userdata('account_no') == $account_number){
					$this->session->set_flashdata('message_err', 'You have entered Account number is your own account number. Please try another account no. ');
					redirect(base_url().'/sendmoney');
				} else {

					if(in_array($to_id,$get_all_user_id)){
						if($this->session->userdata('total_balance') > $transfer_amt) {
							$get_user_details_where	=	array(
								'account_no'	=> $account_number,
							);
							
							$get_user_details	=	$this->General_model->view_data('user_detail',$get_user_details_where);
							$data_history	= array(
								'user_id' 		=> $this->session->userdata('user_id'),
								'trade_id' 		=> $get_user_details[0]['user_id'],
								'transfer_amt' 	=> $transfer_amt,
								'transfer_type' => 'firend_to_friend',
								'status' 		=> '1',
								'created' 		=> date('Y-m-d H:i:s',time()),
							);
							// echo "<pre>"; print_r($data_history);die;
							$this->General_model->insert_data('trade_history',$data_history);
							
							$reduce_amount = $this->session->userdata('total_balance') - $transfer_amt;
							
							$current_user_update = array(
								'total_balance'	=>	$reduce_amount,
							);
							$current_user_where = array(
								'user_id'	=>	$this->session->userdata('user_id'),
							);
							
							$this->General_model->update_data('user_detail',$current_user_update,$current_user_where);
							
							
							
							$update_amount = $get_user_details[0]['total_balance'] + $transfer_amt;
							
							$trade_user_update = array(
								'total_balance'	=>	$update_amount,
							);
							$trade_user_where = array(
								'account_no'	=>	$account_number,
							);
							
							$this->General_model->update_data('user_detail',$trade_user_update,$trade_user_where);
							
							$this->session->set_flashdata('message_succ', 'Amount Transfer Successfully');
							redirect(base_url().'sendmoney');
						}	
					} else {
						$this->session->set_flashdata('message_err', 'Enter the Correct Account Number');
						redirect(base_url().'sendmoney');
					}
				}
			}
		} else {
			$this->session->set_flashdata('message_err', 'Enter the Correct Account Number and Amt');
			redirect(base_url().'sendmoney');
		}
	}
	function get_payer_details()
	{
		$account_no = $_POST['account_to'];
		$user_data	= $this->General_model->view_data('user_detail',array('account_no'=>$account_no));
		
		if(count($user_data) > 0) 
		{
			if($user_data[0]['display_name'] == ''){
				$name = $user_data[0]['name'];
			} else {
				$name = $user_data[0]['display_name'];
			}
			$img = base_url().'upload/profile_img/'.$user_data[0]['image'];
		
			echo $data = '<div class="row user_div"><div class="col-lg-12">
				<div class="friend_profile_img" style="text-align:center;">
					<img src="'.$img.'"  id="k"/>
				</div>
			</div>
			<div class="text-center">
				<p class="mt12">'.$name.'</p>
			</div>
			<div class="text-center">
				<label class="white no-padding">Account NO #'.$user_data[0]['account_no'].'</label>
			</div>
		</div>';
		} else {
			echo $data = '<div class="row user_div"><div class="col-lg-12">
				
			
			<div class="text-center">
				<h3 class="mt12">Acc # '.$account_no.' Not Exits. Please Enter Valid Account No.</h3>
			</div>
			
		</div>';
		}
			
	}

}

