<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Signup extends My_Controller 
{
  function __construct()
    {
		parent::__construct();
		$this->load->model('User_model');
		$this->target_dir=BASEPATH.'../upload/profile_img/';
		$this->check_cover_page_access();
	}
	public function index() {
		$data['dt']=1;
		$data['country_list'] = $this->User_model->country_list();
		$content=$this->load->view('login/registration.tpl.php',$data,true);
		$this->render($content);
	}

	public function register() {
		$data['msg']='';
		$user=array(
			'email'=>$this->input->post('email', TRUE),
			'password'=>md5($this->input->post('password', TRUE)),
		);
		
		if($this->input->post('email', TRUE)!='') {
			if($this->User_model->check_mail('user',array('email'=>$this->input->post('email', TRUE)))) {
				$data['msg']='Your email already exist';
			} else {
				$img = $this->General_model->get_user_img();
				if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
					$img=time().basename($_FILES['image']['name']);
					$ext = end((explode(".", $img)));
					
					if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg') {
						$target_file=$this->target_dir.$img;
						move_uploaded_file($_FILES['image']['tmp_name'], $target_file);
					} else {
						$this->session->set_flashdata('msg','This is not a image file, unable to signup');	
						redirect(base_url().'signup');						
					}
				}
				$r=$this->General_model->insert_data('user',$user); //insert user login details after image validation
				$friends_mail = 'no';
				$team_mail = 'no';
				$challenge_mail = 'no';
				if (isset($_POST['friends_mail'])) {
					$friends_mail = 'yes';
				}
				if (isset($_POST['team_mail'])) {
					$team_mail = 'yes';
				}
				if (isset($_POST['challenge_mail'])) {
					$challenge_mail = 'yes';
				}

				$mail_cate_data=array(
					'friends_mail'=>$friends_mail,
					'team_mail'=>$team_mail,
					'challenge_mail'=>$challenge_mail,
					'user_id'=>$r
				 );
				
				$insert_mail = $this->General_model->insert_data('mail_categories',$mail_cate_data);

				$full_name= $this->input->post('name', TRUE)." ".$this->input->post('lname', TRUE);
				$date_of_birth = $this->input->post('date', TRUE);
				$query = $this->db->query("SELECT MAX(account_no) as account_no FROM user_detail");

				$row = $query->row();
				$display_name_status = 0;
				if($this->input->post('team_name') != ''){
					$display_name_status = 1;
				}
				$user_detail=array(
					'name'=>$full_name,
					'number'=>$this->input->post('number', TRUE),
					'location'=>$this->input->post('location', TRUE),
					'dob'=>$date_of_birth,
					'image'=>$img,
					'state'=>$this->input->post('state', TRUE),
					'country'=>$this->input->post('country', TRUE),
					'zip_code'=>$this->input->post('zip_code', TRUE),
					'display_name'=>$this->input->post('display_name', TRUE),
					'team_name'=>$this->input->post('team_name', TRUE),
					'is_18'=>$this->input->post('is_18', TRUE),
					'user_id'=>$r,
					'display_name_status'=>$display_name_status,
					'account_no' => $row->account_no + 1,
					'custom_name' => $row->account_no + 1,
					);	
				
				if($this->General_model->insert_data('user_detail',$user_detail) && isset($insert_mail)) {
					$data['msg']='Register Successfully';
					$this->session->set_flashdata('message_succ', 'Login with your user details');
					redirect(base_url().'login');
				} else {
					$data['msg']='Register Unsuccessfull';					
				}
			}
			$content=$this->load->view('login/registration.tpl.php',$data,true);
			$this->render($content);
			// $this->session->set_flashdata('message_err', 'User already exist');
			// redirect(base_url().'signup');
		}	
	}
	public function updateuser(){
		$user_data = $this->General_model->view_all_data('user_detail','id','asc');
		$count = 1;
		foreach ($user_data as $key => $value) {
			$data = $this->User_model->over_eighteen($value['user_id']);
			if($data){
				$count++;
			}
		}
		echo $count;
	}
	public function select_country()
	{
			 $rowCount = $this->General_model->view_data('state',array('country_id'=>$_POST['country_id'],'status'=>'0'));
			 if($rowCount > 0){
			        echo '<option value="">Select State</option>';
			        foreach ($rowCount as $key => $value) {
			            echo '<option value="'.$value['stateid'].'">'.$value['statename'].'</option>';
			        	}
			        }
			    else{		
			        echo '<option value="">State not available</option>';
			}
	}	

	public function check_email()
	{
		$us_email=$_POST['uemail'];

		$res=$this->General_model->get_num_rows('user',array('email'=>$us_email)); 
		
		if($res>0)
		{
		
			echo "E-mail already Exists";
		}
		
	}		
}
