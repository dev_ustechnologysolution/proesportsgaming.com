<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Store extends My_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('My_model');
        $this->load->model('General_model');
        $this->load->model('Product_model');
        $this->load->model('User_model');
        $this->load->model('Cart_model');
        $this->load->library('pagination');
        $this->pageLimit = 20;
        $this->countItem = 10;
        $this->check_cover_page_access();
        $this->category_list = $this->category_list();
    }
    //page loading
    function index() {
        $data['banner_data'] = $this->General_model->tab_banner();
        $data['itemperpage'] = $this->countItem;
        if (isset($_GET['search_product'])) {
            if (!empty($_POST)) {
                $main_cat = $data['selected_main_cat'] = $_POST['main_category'];
                $main_sub_cat = $data['selected_main_sub_cat'] = $_POST['main_sub_cat'];
                $product_srch_term = $data['inputed_product_srch_term'] = $_POST['product_srch-term'];
                if ($main_cat !='') {
                    $arr = array( 'main_cat' => $main_cat );
                    $all_sub_cat = $this->Product_model->getSubCarArr($arr);
                    $all_sub_cat_arr = explode(', ', $all_sub_cat->cate_id_arr);
                }
                $store_ads_list = $this->General_model->get_ads_detail('store_ads',100,'img','');
                $list_arr = array(
                    'all_sub_cat_arr' => $all_sub_cat_arr,
                    'main_cat' => $main_cat,
                    'main_sub_cat' => $main_sub_cat,
                    'product_srch_term' => $product_srch_term,
                    'limit' => $data['itemperpage'],
                );
                $data['list'] = $this->Product_model->SearchProductArr($list_arr);
                $get_last_arr = array(
                    'all_sub_cat_arr' => $all_sub_cat_arr,
                    'main_cat' => $main_cat,
                    'main_sub_cat' => $main_sub_cat,
                    'product_srch_term' => $product_srch_term,
                    'get_last_id' => 'yes',
                    'limit' => 1,
                );
                $data['get_last_id'] = $this->Product_model->SearchProductArr($get_last_arr)[0];
                $data['count_product'] = $data['get_last_id']['count'].' Result to display';
            }
        } else {
            $get_last_arr = array(
                'get_last_id' => 'yes',
                'limit' => 1,
            );
            $data['get_last_id'] = $this->Product_model->SearchProductArr($get_last_arr)[0];
            $list_arr = array(
                'limit' => $data['itemperpage'],
            );
            $data['list'] = $this->Product_model->SearchProductArr($list_arr);
        }
        $data['category_list'] = $this->category_list;
        $data['store_ads_list'] = $this->General_model->get_ads_detail('store_ads',count($data['list']),'img','');
        $data['category_list_arr'] = array_column($data['category_list'], 'parent_id');
        $content = $this->load->view('store/store.tpl.php',$data,true);
        $this->render($content);
    }
    // My Cart Item
    function category_list() {
        $category_list = $this->General_model->view_all_data('product_categories_tbl','id','asc');
        return $category_list;
    }
    // product search
    function search_product() {
        if (!empty($_POST)) {
            $main_cat = $_POST['main_category'];
            $main_sub_cat = $_POST['main_sub_cat'];
            $product_srch_term = $_POST['product_srch_term'];
            $last_item = $_POST['allcount'];
            if ($main_cat !='') {
                $arr = array( 'main_cat' => $main_cat );
                $all_sub_cat = $this->Product_model->getSubCarArr($arr);
                $all_sub_cat_arr = explode(',', $all_sub_cat->cate_id_arr);
            }
            $store_ads_list = $this->General_model->get_ads_detail('store_ads',100,'img','');
            $arr1 = array(
                'all_sub_cat_arr' => $all_sub_cat_arr,
                'main_cat' => $main_cat,
                'main_sub_cat' => $main_sub_cat,
                'product_srch_term' => $product_srch_term,
                'last_id' => $last_item,
                'limit' => $this->countItem,
            );
            $res_arr = $this->Product_model->SearchProductArr($arr1);
            if (!empty($res_arr)) {
                $i = 0;
                $data['last_id'] = $last_item;
                foreach ($res_arr as $key => $res) {
                    $data['last_id'] = $res['id'];
                    $product_id = $res['id'];
                    $arr = array( 'product_id' => $product_id );
                    $img = $this->Product_model->ProductImg($arr)->image;

                    if ($key % 5 == 0) { 
                        $result .='<li class="ads row">
                            <div class="add_listing_div">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <a class="ads_container_desciption_div" href="'.$store_ads_list[$i]->sponsered_by_logo_link.'" target="_blank">
                                       <div class="ads_img_div col-sm-4">
                                        <img src="'.base_url().'upload/ads_master_upload/'.$store_ads_list[$i]->upload_sponsered_by_logo.'" alt="'.$store_ads_list[$i]->upload_sponsered_by_logo.'" class="img img-responsive">
                                       </div>
                                       <div class="ads_content_div col-sm-8">
                                        <div class="ads_desciption">'.$store_ads_list[$i]->sponsered_by_logo_description.'</div>
                                      </div>
                                    </a>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                        </li>'; 
                        $i++;
                    }

                    $result .= '<li class="product_'.$product_id.'">
                        <div class="main_product">
                            <div class="product_img_dv"><img src="'.base_url().'upload/products/'.$img.'" alt="'.$img.'" class="img img-responsive"></div>
                            <div class="product_title_dv">
                                <label class="title">'.$res['name'].'</label>
                            </div>
                            <div class="product_view_dv">
                                <a class="view_product" href="'.base_url().'Store/view_product/'.$product_id.'"> View Item </a>
                            </div>
                        </div>
                    </li>';
                }
            }
            $data['result'] = $result;
            echo json_encode($data);
            exit();
        }
    }
    function search_category() {
        if (!empty($_POST)) {
            $main_cat = $_POST['main_category'];
            $res['suboption'] = '<option value="">Please select a Sub Category</option>';
            if ($main_cat !='') {
                $arr = array( 'main_cat' => $main_cat );
                $all_sub_cat = $this->Product_model->getSubCarArr($arr);
                $all_sub_cat_arr = explode(',', $all_sub_cat->cate_id_arr);
                $all_subcat = explode(',', $all_sub_cat->subcate_name_arr);
                foreach ($all_sub_cat_arr as $key => $asc) {
                    $res['suboption'] .= '<option value="'.trim($asc).'">'.$all_subcat[$key].'</option>';
                }
            }
            echo json_encode($res);
            exit();
        }
    }
    function view_product($id) {
        $data['category_list'] = $this->category_list;
        $data['product_id'] = $id;
        $arr = array( 'product_id' => $id );
        $data['product_detail'] = $this->Product_model->productDetail($arr);
        $data['prdct_img_arr'] = array_unique(array_column($data['product_detail'],'image'));

        $arr = array( 'product_id' => $id, 'get' => 'get_imgs');
        $data['default_sel_img'] = $this->Product_model->ProductImg($arr)->image;

        $arr = array('product_id' => $id);
        $product_attribute_data = $this->Product_model->attributes_list($arr);
        $result = array();
        foreach($product_attribute_data as $attribute) {
            $result[$attribute->title.'_'.$attribute->type][]= $attribute; 
        }
        $data['attribute_list'] = $result;
        $data['product_ads'] = $this->General_model->get_ads_detail('product_ads',1,'',$data['product_id'])[0];
        $content = $this->load->view('store/single_product.tpl.php',$data,true);
        $this->render($content);
    }
    function add_product_cart() {
      if (!empty($_POST)) {
        if (isset($_POST['add_to_cart'])) {
          $product_id = $_POST['product_id'];
          $_POST['sel_variation'] = array_flip($_POST['sel_variation']);
          ksort($_POST['sel_variation']);
          $_POST['sel_variation'] = array_flip($_POST['sel_variation']);
          $_POST['sel_variation'] = implode(', ', $_POST['sel_variation']);
          $_POST['price'] = number_format((float) $_POST['price']+$_POST['product_fee'], 2, '.', '');
          foreach ($_SESSION['cart_item_list'] as $key => $cil) {
            if ($cil['product_id'] == $product_id && $cil['sel_variation'] == $_POST['sel_variation']) {
              unset($_SESSION['cart_item_list'][$key]);
              $cil['prdct_qntity'] = $cil['prdct_qntity'] + $_POST['prdct_qntity'];
              $newsession = $cil;
            }
          }
          if (isset($newsession) && !empty($newsession)) {
            $_SESSION['cart_item_list'][] = $newsession;
          } else {
            $_SESSION['cart_item_list'][] = $_POST;
          }
          $this->session->set_flashdata('message_succ', 'Item added into cart');
          redirect(base_url().'store/cart');
        } else if (isset($_POST['add_to_wishlist'])) {

        }
      }
    }
    // Cart page
    function cart() {
      $data['category_list'] = $this->category_list;
      $data['cart_item'] = $_SESSION['cart_item_list'];
      $data['user_detail'] = $this->User_model->user_detail()[0];
      $content = $this->load->view('store/cart.tpl.php',$data,true);
      $this->render($content);
    }
    function get_img_selvar() {
        if (!empty($_POST)) {
            if (!empty($_POST['sel_variation']) && !empty($_POST['sel_variation_ids'])) {
                $sel_variation = $_POST['sel_variation'];
                $sel_variation_ids = $_POST['sel_variation_ids'];
                foreach ($sel_variation as $key => $sv) {
                    $sv = explode('&', $sv)[0];
                    $attribute = explode('_', $sv)[1];
                    if (strtolower($attribute) == 'color') {
                        $selvid = explode('&', $_POST['sel_variation_ids'][$key]);
                        $attribute_id = explode('_', $selvid[0])[1];
                        $option_id = explode('_', $selvid[1])[1];
                        $attr = array(
                           'attribute_id' => $attribute_id,
                           'option_id' => $option_id
                        );
                        $get_img = $this->Cart_model->get_attribute_img($attr);
                    }
                }
                $data['img'] = ($get_img->option_image !='')?base_url().'upload/products/'.$get_img->option_image : '';
                $data['selected_img'] = $get_img->option_image;
                echo json_encode($data);
                exit();
            }
        }
    }
    function remove_product_cart($id) {
      unset($_SESSION['cart_item_list'][$id]);
      $this->session->set_flashdata('message_succ', 'Removed Item from cart');
      redirect(base_url().'store/cart');
    }
    // Cart Update
    function cart_update() {
      $cart_item = $_SESSION['cart_item_list'];
      $new_arr = []; $i = 0;
      foreach ($cart_item as $key => $ci) {
        if (in_array($ci['product_id'], $_POST['product_id'])) {
          $ci['prdct_qntity'] = $_POST['prdct_qntity'][$i];
          $new_arr[] = $ci;
        }
        $i++;
      }
      $this->session->set_userdata('cart_item_list',$new_arr);
      $this->session->set_flashdata('message_succ', 'Cart updated');
      redirect(base_url().'store/cart');
    }
}