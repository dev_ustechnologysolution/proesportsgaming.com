<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Streamsetting extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->check_user_page_access();
		$this->load->library(array('form_validation'));
		$this->load->model('General_model');
		$this->target_dir=BASEPATH.'../upload/stream_setting/';
	}
	public function index() {
		$data['banner_data'] = $this->General_model->tab_banner();
		$data['get_tipicon'] = $this->General_model->get_stream_setting($_SESSION['user_id'],'','tip_icon');
		$data['get_tipsound'] = $this->General_model->get_stream_setting($_SESSION['user_id'],'','tip_sound');
		$data['get_subsound'] = $this->General_model->get_stream_setting($_SESSION['user_id'],'','sub_sound');
		$data['get_defaultstream'] = $this->General_model->get_stream_setting($_SESSION['user_id'],'','default_stream');
		$data['get_user_detail'] = $this->User_model->get_user_detail($this->session->userdata('user_id'));
		$data['created_lobbys'] = $this->General_model->created_lobbys($this->session->userdata('user_id'));
		$content=$this->load->view('streamsetting/streamsetting.tpl.php',$data,true);
		$this->render($content);
	}
	public function add_data() {
		if ($_GET['add_tipimg']) {
			$img = '';
	        if(isset($_FILES["new_tip_img"]["name"]) && $_FILES["new_tip_img"]["name"]!='') {
	        	$file_name = str_replace(',','',$_FILES['new_tip_img']['name']);
		      	$file_size = $_FILES['new_tip_img']['size'];
		      	$file_tmp = $_FILES['new_tip_img']['tmp_name'];
		      	$file_type = $_FILES['new_tip_img']['type'];
		      	$file_name = str_replace('_-image-ini-file_','',$file_name);
	            $img = time().'_-image-ini-file_'.basename($file_name);
	            $ext = strtolower(end(explode('.',$img)));
	            $extensions= array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
	            $errors = '';
	      	    if(in_array($ext,$extensions)=== false){
			       $errors = "extension not allowed, please choose a JPEG or PNG or GIF file.";
			    }
			    if($errors == ''){
			    	$target_file = $this->target_dir.$img;
			    	$get_tipicon = $this->General_model->get_stream_setting($_SESSION['user_id'],'','tip_icon');
		       		if (move_uploaded_file($file_tmp, $target_file)) {
				    	if (count($get_tipicon) == 0) {
		       				$insert_data = array(
					            'user_id' => $_SESSION['user_id'],
					            'tip_icon_imgs' => $img,
								'key_option' => 'tip_icon',
					        );
					        $data_inserted = $this->General_model->insert_data('stream_settings', $insert_data);
				    	} else {
					    	$tipicon_arr = $img;
					    	if (count($get_tipicon[0]->tip_icon_imgs) != 0) {
						    	$tipicon_arr = explode(',', $get_tipicon[0]->tip_icon_imgs);
						    	array_push($tipicon_arr, $img);
						    	$tipicon_arr = implode(',', $tipicon_arr);
					    	}
				            $update_data_where = array('user_id' => $_SESSION['user_id'], 'key_option' => 'tip_icon');
				            $update_row = array(
				                'tip_icon_imgs' => $tipicon_arr
				            );
				            $update = $this->General_model->update_data('stream_settings', $update_row, $update_data_where);			    	
				    	}
			    	}
	                $this->session->set_flashdata('message_succ', 'Tip icon added');
			    } else {
			    	$this->session->set_flashdata('message_err', $errors);
			    }
			    redirect(base_url().'Streamsetting');
	        }
		} else if ($_GET['add_sound']) {
			$file = '';	
			$key_check = $_GET['add_sound'];
			$msg_var = ($key_check == 'tip_sound') ? 'Tip' : (($key_check == 'sub_sound')? 'Subcription' : '' );
	        if(isset($_FILES["new_audio"]["name"]) && $_FILES["new_audio"]["name"]!='') {
	        	$file_name = str_replace(',','',$_FILES['new_audio']['name']);
		      	$file_size = $_FILES['new_audio']['size'];
		      	$file_tmp = $_FILES['new_audio']['tmp_name'];
		      	$file_type = $_FILES['new_audio']['type'];
		      	$file_name = str_replace('_-audio-ini-mp3_','',$file_name);
	            $file = time().'_-audio-ini-mp3_'.basename($file_name);
	            $ext = strtolower(end(explode('.',$file)));
	            $extensions = array("wav","mp3","wma","ogg","WAV","MP3","WMA","OGG");
	            $errors = '';
	      	    if(in_array($ext,$extensions)=== false){
			       $errors = "extension not allowed, please choose a MP3 or OGG or WMA file.";
			    }
			    if($errors == ''){
			    	$target_file = $this->target_dir.$file;
			    	if ($key_check == 'tipsound') {
						
					}
			    	$getsound = $this->General_model->get_stream_setting($_SESSION['user_id'],'',$key_check);
		       		if (move_uploaded_file($file_tmp, $target_file)) {
				    	if (count($getsound) == 0) {
		       				$insert_data = array(
					            'user_id' => $_SESSION['user_id'],
					            'sound_audios' => $file,
								'key_option' => $key_check,
					        );
					        $data_inserted = $this->General_model->insert_data('stream_settings', $insert_data);
					        if (!$data_inserted) {
					        	echo "error to insert";
					        }
				    	} else {
					    	$tipsound_arr = $file;
					    	if (count($getsound[0]->sound_audios) != 0) {
						    	$tipsound_arr = explode(',', $getsound[0]->sound_audios);
						    	array_push($tipsound_arr, $file);
						    	$tipsound_arr = implode(',', $tipsound_arr);
					    	}
				            $update_data_where = array('user_id' => $_SESSION['user_id'], 'key_option' => $key_check);
				            $update_row = array(
				                'sound_audios' => $tipsound_arr
				            );
				            $update = $this->General_model->update_data('stream_settings', $update_row, $update_data_where);
				            if (!$update) {
					        	echo "error to update";
					        }
				        }
			    	}
	                $this->session->set_flashdata('message_succ', $msg_var.' Sound added');
			    } else {
			    	$this->session->set_flashdata('message_err', $errors);
			    }
			    header('location:' . base_url().'Streamsetting');
	        }
		}
	}
	public function change_file_name() {
		$key_type = $_GET['type'];
		if (!empty($_POST)) {
			$file_pattern = ($key_type == 'tip_icon')?'_-image-ini-file_':'';
			$file_pattern = ($key_type == 'tip_sound' || $key_type == 'sub_sound')?'_-audio-ini-mp3_':$file_pattern;

			$old_file_name = $_POST['old_file_name'];
			$edit_file_name = $_POST['edit_file_name'];

			$arr = array(
				'key_type' => $key_type,
				'user_id' => $this->session->userdata('user_id'),
				'file_name' => $old_file_name
			);
			$get_data = $this->General_model->get_chat_file($arr);
			if (!empty($get_data)) {
				$ext = strtolower(end(explode('.',$old_file_name)));
				$edit_file_name = str_replace(' ', '-', $edit_file_name);
				$edit_file_name = preg_replace('/[^A-Za-z0-9\-]/', '', $edit_file_name);
				$new_file_name = time().$file_pattern.$edit_file_name.'.'.$ext;
				$update_data_where = array(
					'user_id' => $this->session->userdata('user_id'),
					'key_option' => $key_type
				);
				if ($key_type == 'tip_icon') {
					$files_arr = str_replace($old_file_name,$new_file_name,$get_data[0]->tip_icon_imgs);
					$update_row['tip_icon_imgs'] = $files_arr;
					if ($old_file_name == $get_data[0]->tip_selicon_img) {
						$update_row['tip_selicon_img'] = $new_file_name;
					}
				} else if ($key_type == 'tip_sound' || $key_type == 'sub_sound') {
					$files_arr = str_replace($old_file_name,$new_file_name,$get_data[0]->sound_audios);
					$update_row['sound_audios'] = $files_arr;
					if ($old_file_name == $get_data[0]->selsound_audio) {
						$update_row['selsound_audio'] = $new_file_name;
					}
				}
				$update = $this->General_model->update_data('stream_settings', $update_row, $update_data_where);
				
	            if ($update && file_exists($this->target_dir.$old_file_name)) {
	            	$file_updated = rename($this->target_dir.$old_file_name,$this->target_dir.$new_file_name);
	            	if ($file_updated) {
	            		$this->session->set_flashdata('message_succ', 'file name updated');
	            	} else {
	            		$this->session->set_flashdata('message_err', 'file name not updated');
	            	}
	            } else {
	            	$this->session->set_flashdata('message_err', 'file name not updated');
	            }
	            redirect(base_url().'Streamsetting');
			}
		}
	}
	public function update_settings() {
		if (!empty($_POST)) {
			$key_option = $_GET['update_data'];
			$msg_var = ($key_option == 'tip_icon') ? 'Tip icon' : (($key_option == 'tip_sound')? 'Tip sound' : (($key_option == 'sub_sound')? 'Subcription sound' : '' ));
			$update_data_where = array(
				'user_id' => $_SESSION['user_id'],
				'key_option' => $key_option
			);
			if ($key_option == 'tip_icon') {
				$update_row['tip_selicon_img'] = $_POST['seltipicon'];
			} else if ($key_option == 'tip_sound' || $key_option == 'sub_sound') {
				$update_row['selsound_audio'] = $_POST['selsound'];
			}
            $update = $this->General_model->update_data('stream_settings', $update_row, $update_data_where);
            if ($update) {
            	$this->session->set_flashdata('message_succ', $msg_var.' Updated');
            } else {
            	$this->session->set_flashdata('message_err', $msg_var.' Not Updated');
            }
            header('location:' . base_url().'Streamsetting');
		}
	}	
	public function get_default_stream($user_id) {
		$res['get_defaultstream'] = $this->General_model->get_stream_setting($user_id,'','default_stream')[0];
		$res['count_get_defaultstream'] = count($res['get_defaultstream']);
		// call from ajax
		if ($_POST['data'] == 'json') {
			echo json_encode($res);
		} else {
			return json_encode($res);
		}
	}
	public function update_default_stream() {
		if (!empty($_POST)) {
			$data = $this->get_default_stream($_SESSION['user_id']);
			$data = json_decode($data);
			if (empty($data->get_defaultstream)) {
				$insert_data = array(
		            'user_id' => $_SESSION['user_id'],
		            'default_stream_channel' => ($_POST['default_stream'] != '') ? $_POST['default_stream'] : null,
					'key_option' => 'default_stream',
		        );
		        $inserted = $this->General_model->insert_data('stream_settings', $insert_data);
		        $r = ($inserted) ? $this->session->set_flashdata('message_succ', 'Default Stream Inserted') : $this->session->set_flashdata('message_succ', 'Error to Insert Default Stream') ;
			} else {
				$update_data_where = array('user_id' => $_SESSION['user_id'], 'key_option' => 'default_stream');
	            $update_row = array(
	                'default_stream_channel' => ($_POST['default_stream'] != '') ? $_POST['default_stream'] : null
	            );
	            $update = $this->General_model->update_data('stream_settings', $update_row, $update_data_where);
	            if ($update) {
	            	$this->session->set_flashdata('message_succ', 'Default Stream Updated');
	            } else {
	            	$this->session->set_flashdata('message_succ', 'Error to Updated Default Stream');
	            }
			}
			redirect(base_url().'Streamsetting');
		}
	}
	public function delete_data() {
		if ((isset($_GET['sound']) || isset($_GET['img'])) && isset($_GET['delete_file'])) {
			$key_option = ($_GET['sound']) ? $_GET['sound'] : $_GET['img'];
			$delete_file = $_GET['delete_file'];
			$msg_var = ($key_option == 'tip_icon') ? 'Tip icon' : (($key_option == 'tip_sound')? 'Tip sound' : (($key_option == 'sub_sound')? 'Subcription sound' : '' ));
			$update_data_where = array('user_id' => $_SESSION['user_id'], 'key_option' => $key_option);
			$get_files = $this->General_model->get_stream_setting($_SESSION['user_id'],'',$key_option);
			
			if ($key_option == 'tip_icon') {
				$files_arr = explode(',', $get_files[0]->tip_icon_imgs);
				if (($key = array_search($delete_file, $files_arr)) !== false) {
				    unset($files_arr[$key]);
				    unlink($this->target_dir.$delete_file);
				}
				$files_array = implode(',', $files_arr);
				$update_row['tip_icon_imgs'] = $files_array;
			   
			    if ($get_files[0]->tip_selicon_img == $delete_file) {
			        $update_row = array(
			        	'tip_selicon_img' => null,
			            'tip_icon_imgs' => implode(',', $files_arr)
			        );
				} else if (count($files_arr) == 0) {
					$update_row = array(
			            'tip_icon_imgs' => null
			        );
				}
			} else if ($key_option == 'tip_sound' || $key_option == 'sub_sound') {
				$files_arr = explode(',', $get_files[0]->sound_audios);
				if (($key = array_search($delete_file, $files_arr)) !== false) {
				    unset($files_arr[$key]);
				    unlink($this->target_dir.$delete_file);
				}
				$files_array = implode(',', $files_arr);
				$update_row['sound_audios'] = $files_array;

				if ($get_files[0]->selsound_audio == $delete_file) {
			        $update_row = array(
			        	'selsound_audio' => null,
			            'sound_audios' => implode(',', $files_arr)
			        );
				} else if (count($files_arr) == 0) {
					$update_row = array(
			            'sound_audios' => null
			        );
				}
			}
	        $update = $this->General_model->update_data('stream_settings', $update_row, $update_data_where);
	        if ($update) {
	        	$this->session->set_flashdata('message_succ', $msg_var.' Deleted');
	        } else {
	        	$this->session->set_flashdata('message_succ', $msg_var.' Not Deleted');
	        }
	        header('location:' . base_url().'Streamsetting');
		}
	}
	public function update_lobby_url() {
		$lobby_id = $_POST['lobby_id'];
		$custom_name = $_POST['custom_name'];
		$update_row = array('custom_name' => strtolower($custom_name));
		if (!empty($lobby_id) && !empty($custom_name)) {
			$update_data_where = array('user_id' => $_SESSION['user_id'], 'id' => $lobby_id);
			$update = $this->General_model->update_data('lobby', $update_row, $update_data_where);
		} else {
			$update_data_where = array('user_id' => $_SESSION['user_id']);           
            $update = $this->General_model->update_data('user_detail', $update_row, $update_data_where);
		}
		($update) ? $this->session->set_flashdata('message_succ', 'Link Updated') : $this->session->set_flashdata('message_succ', 'Error to link Updated') ;
            redirect(base_url().'Streamsetting');
	}
	public function check_lobby_name() {
		$lobby_name = $_POST['lobby_name'];
		$res['count_lby'] = 0;
		if (!empty($lobby_name)) {
			$this->db->select('l.*');
			$this->db->from('lobby l');
			$this->db->join('group_bet gb','gb.lobby_id = l.id');
	    	$this->db->where('l.status',1);
	    	$this->db->where('gb.bet_status',1);
	    	$this->db->where('l.custom_name',strtolower($lobby_name));
	    	$res['view_data'] = $this->db->get()->result();
	        $res['count_lby'] = count($res['view_data']);
		}
		if ($res['count_lby'] && $res['count_lby'] != 0) {
			$res['error'] = 'Lobby is exist with this name. Please try another name..';
		} else if (strpos($lobby_name, 'account') !== false) {
			$res['error'] = "Can't use Account keyword. Please try another name..";
		} else {
			$res['error'] = '';
		}
		echo json_encode($res);
		exit();
	}
	public function check_default_user_link() {
		$default_link_name = $_POST['default_link_name'];
		$res['count_lby'] = 0;
		if (!empty($default_link_name)) {
			$this->db->select('ud.*');
			$this->db->from('user_detail ud');
			$this->db->where('ud.custom_name',strtolower($default_link_name));
	    	$user_custom_detail = $this->db->get()->result();
	    	$this->db->select('l.*');
			$this->db->from('lobby l');
			$this->db->join('group_bet gb','gb.lobby_id = l.id');
	    	$this->db->where('l.status',1);
	    	$this->db->where('gb.bet_status',1);
	    	$this->db->where('l.custom_name',strtolower($default_link_name));
	    	$lobby_exist_detail = $this->db->get()->result();
	    	$res['view_data'] = array_merge($user_custom_detail,$lobby_exist_detail);
	        $res['count_lby'] = count($res['view_data']);
		}
		if ($res['count_lby'] && $res['count_lby'] != 0) {
			$res['error'] = 'User or live lobby is exist with this name. Please try another name..';
		} else if (strpos($lobby_name, 'account') !== false) {
			$res['error'] = "Can't use Account keyword. Please try another name..";
		} else {
			$res['error'] = '';
		}
		echo json_encode($res);
		exit();
	}
}

