<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include BASEPATH.'../application/controllers/My_Controller.php';

class Trade extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->check_user_page_access();
		$this->load->library(array('form_validation'));
		$this->load->model('General_model');
		$this->target_dir=BASEPATH.'../upload/profile_img/';
	}

	public function tradeHistory() {
		$data['banner_data'] = $this->General_model->tab_banner();
		$data['my_data'] = $this->User_model->user_detail()[0];
		$data['get_sent_history'] = $this->General_model->get_mytradelist($_SESSION['user_id'],'sent','created');
		$data['get_receive_history'] = $this->General_model->get_mytradelist($_SESSION['user_id'],'receive','created');
		$data['trade_data'] = $this->General_model->array_merge_custom($data['get_sent_history'],$data['get_receive_history'],'id');
		$content=$this->load->view('trade/tradelist.tpl.php',$data,true);
		$this->render($content);
	}
	public function tipHistory(){
		$data['banner_data'] = $this->General_model->tab_banner();
		$data['my_data'] = $this->User_model->user_detail()[0];
		$data['tip_send'] = $this->General_model->get_tip_history($_SESSION['user_id'],'th.user_from','th.user_to');
		$data['tip_receive'] = $this->General_model->get_tip_history($_SESSION['user_id'],'th.user_to','th.user_from');
		$content=$this->load->view('trade/tiphistorylist.tpl.php',$data,true);
		$this->render($content);

	}

}

