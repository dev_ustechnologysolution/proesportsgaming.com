<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
// require_once(FCPATH.'/vendor/autoload.php');

class User extends My_Controller 
{

  	function __construct()
    {
		parent::__construct();
		$this->load->model('User_model');
		$this->target_dir=BASEPATH.'../upload/profile_img/';
	}
	//profile view
	function myprofile()
    {
		$this->check_user_page_access();
		$user_id = $_SESSION['user_id'];
		$data['friend_profile']=$this->General_model->friend_profile($user_id);
		$data['banner_data'] = $this->General_model->tab_banner();
		$content=$this->load->view('user/myprofile.tpl.php',$data,true);
		$this->render($content);
	}
	function profile()
    {
		$this->check_user_page_access();
		$data['country_list'] = $this->User_model->country_list();
		$data['user_detail']=$this->User_model->user_detail();	
		$data['mail_categories_fun']=$this->User_model->mail_categories_fun();
		$data['banner_data'] = $this->General_model->tab_banner();
		$content=$this->load->view('user/profile.tpl.php',$data,true);
		$this->render($content);
	}

	//profile edit
	function profile_edit()
    {
		$this->check_user_page_access();
		$img = $this->input->post('old_image', TRUE) != '' ? $this->input->post('old_image', TRUE) : $this->General_model->get_user_img();

		if(isset($_FILES["image"]["name"]) && $_FILES["image"]["name"]!='') {
			$img=time().basename($_FILES["image"]["name"]);
			$ext = end((explode(".", $img)));
		
			if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg') {
				$target_file = $this->target_dir.$img;
		        if(move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
					if(file_exists($this->target_dir.$this->input->post('old_image', TRUE))) {
						unlink($this->target_dir.$this->input->post('old_image', TRUE));
					}
				}
			} else {
				$this->session->set_flashdata('msg', 'This is not a image file');	
				redirect(base_url().'user/profile');
			}
		}
		//update email and password into user table
		$email=array('email'=>$this->input->post('email', TRUE));
		$this->General_model->update_data('user',$email,array('id'=>$_SESSION['user_id']));
		if($this->input->post('password', TRUE)!='')//checking password is emplty or not..
		{
			$password=array('password'=>md5($this->input->post('password', TRUE)));
			$this->General_model->update_data('user',$password,array('id'=>$_SESSION['user_id']));
		}
		$friends_mail = 'no';
		$team_mail = 'no';
		$challenge_mail = 'no';
		if (isset($_POST['friends_mail'])) {
			$friends_mail = 'yes';
		}
		if (isset($_POST['team_mail'])) {
			$team_mail = 'yes';
		}
		if (isset($_POST['challenge_mail'])) {
			$challenge_mail = 'yes';
		}

		$select_mail_categories=$this->General_model->view_data('mail_categories',array('user_id'=>$_SESSION['user_id']));
		
		//update user info into user detail tables..
		$mail_cate_data=array(

			'user_id'=>$_SESSION['user_id'],

			'friends_mail'=>$friends_mail,

			'team_mail'=>$team_mail,


			'challenge_mail'=>$challenge_mail

			 );
			if ($this->input->post('display_name_status') == 'on') {
				$display_name=$this->input->post('display_name', TRUE);
				$display_name_status=1;
				$_SESSION['display_name'] = $display_name;
			} else {
				$display_name='';
				$display_name_status=0;
				if ($_SESSION['display_name']) {
					unset($_SESSION['display_name']);
				}
			}
			$data=array(
				'name'=>$this->input->post('name', TRUE),
				'display_name'=>$display_name,
				'display_name_status'=>$display_name_status,
				'number'=>$this->input->post('number', TRUE),
				'team_name'=>$this->input->post('team_name', TRUE),
				'location'=>$this->input->post('location', TRUE),			
				'country'=>$this->input->post('country', TRUE),
				'state'=>$this->input->post('state', TRUE),
				'zip_code'=>$this->input->post('zip_code', TRUE),
				'taxid'=>$this->input->post('taxid', TRUE),
				'image'=>$img
			 );

		$r=$this->General_model->update_data('user_detail',$data,array('user_id'=>$_SESSION['user_id']));
		
		if (count($select_mail_categories)==0) {
			$r.=$this->General_model->insert_data('mail_categories',$mail_cate_data);
		} else
		{
			$r.=$this->General_model->update_data('mail_categories',$mail_cate_data,array('user_id'=>$_SESSION['user_id']));
		} 
		if($r)
		{
			$session_data = array(
								'user_name'=>$this->input->post('name', TRUE),
								'user_image'=>$img,
								'message_succ'=>'<h5 style="margin-left: 250px;color: #FF5000;">Update Successful</h5>'
							);
            $this->session->set_userdata($session_data);
			header('location:'.base_url().'user/profile');
		}
    }
	//view change password
 	function changePassword()
    {
		$this->check_user_page_access();
		$data['data']='';
		$content=$this->load->view('user/change_password.tpl.php',$data,true);
		$this->render($content);
	}

	//change password
	function change_password()
    {
		$this->check_user_page_access();
		//check new_password and confirm_password
		if($this->input->post('new_password', TRUE)==$this->input->post('confirm_password', TRUE))
		{
			//check current password
			$r=$this->User_model->check_password('user',array('id'=>$_SESSION['user_id'],'password'=>md5($this->input->post('current_password', TRUE))));
			if($r==1)
			{
				$data=array('password'=>md5($this->input->post('new_password', TRUE)),);
				$r=$this->General_model->update_data('user',$data,array('id'=>$_SESSION['user_id']));

				if($r)
				{
					echo '<div class="box box-success">
							<div class="box-header with-border">
								<h3 class="box-title">Password changed successfully</h3>
								<div class="box-tools pull-right">
									<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
								</div>
							</div>
						</div>';
			  	}
			} else {
				echo '<div class="box box-danger">
                		<div class="box-header with-border">
                  			<h3 class="box-title">Current password is not correct</h3>
							<div class="box-tools pull-right">
								<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
							</div>
						</div>
					  </div>';
			}
		} else {
			echo '<div class="box box-danger">
					<div class="box-header with-border">
					<h3 class="box-title">New password and confirm password not match</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
					</div>
					</div>
				</div>';
		}
	}
	//forgot password
	function forgot_password()
    {
		$view_data=array();
		if($this->input->post('email', TRUE)!='')
		{
			$user=$this->General_model->view_data('user',array('email'=>$this->input->post('email', TRUE)));
			if(isset($user)&& count($user)>0)
			{
				$user_id=$user[0]['id'];
				$user_name=$this->General_model->view_data('user_detail',array('user_id'=>$user_id));
				$user_name=$user_name[0]['name'];

				$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
				$password = substr(str_shuffle($chars), 0, 6);    		    		
				$email = $this->input->post('email', TRUE);
				$token = array('token'=>$password);

				$this->General_model->update_data('user',$token , array('email'=>$email));			
				$data = array('password'=>md5($password));
				if($this->General_model->update_data('user',$data,array('email'=>$this->input->post('email', TRUE))))
					$to =$email;
					$header = base_url().'/assets/frontend/images/email/header.png';
				$subject = "Recovery Password";
				$message ='<html><body><table cellpadding="0" cellspacing="0" broder="0" width="600">';
				$message .='<tr><td><img src="'.$header.'" style="width:100%;"></td></tr>';
				$message .='<tr><td><p> Hello '.$user_name.'</p><p>Please click on the below link to reset your password.</p><a href='.base_url().'user/reset_password/'.$password.'>Click here</a><p>Thanks,</p></td></tr>';
				$message .='<tr><td><img src="'.base_url().'/assets/frontend/images/email/footer.png" style="width:100%;"></td></tr>';
				$message .='</table></body></html>';

				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers .= 'From: <noreply@ProEsportsGaming.com>' . "\r\n";
				if(mail($to,$subject,$message,$headers)){
					$view_data['err']='<span style="color:green">please check your mail</span>';
				} else {
					$re = $this->General_model->sendMail($to,$user_name,$subject,$message);
					if($re == 1){
						$view_data['err']='<span style="color:green">please check your mail</span>';
					} else {

						$view_data['err']='<span style="color:red">mail not send </span>';
					}
				}
			} else {
				$view_data['err']='<span style="color:red">Give proper email</span>';
			}
		}
		$content=$this->load->view('login/forgot_pass',$view_data,true);
		$this->render($content);
	}
	
	function sendMail1($to,$subject,$message)
	{
		$this->load->library('email');
		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'smtp.sendgrid.net',
			'smtp_port' => 587,
			'smtp_user' => 'archita@aspireedge.com', // change it to yours
			'smtp_pass' => 'archita123', // change it to yours
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'wordwrap' => TRUE,
		);
		$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		$this->email->from('noreply@ProEsportsGaming.com'); // change it to yours
		$this->email->to($to);// change it to yours
		$this->email->subject($subject);
		$this->email->message($message);
		if($this->email->send()) {
			$result = 1;
		} else {
			$result = 0;
		//  show_error($this->email->print_debugger());
		}
		return $result;
	}

	//reset password
	function reset_password($token='')
    {
		$view_data=array();
		$user = $this->General_model->view_data('user',array('token'=>$token));
		
		if(isset($user)&& count($user)>0)
		{
		
			if($this->input->post('new_password', TRUE)!='')
			{
				if($this->input->post('new_password', TRUE)==$this->input->post('confirm_password', TRUE))
				{
					$email= $user[0]['email'];
					$data=array('token'=>'','password'=>md5($this->input->post('new_password', TRUE)));
					if($this->General_model->update_data('user',$data,array('email'=>$email)))
					header('location:'.base_url('login'));
				} else {
					$view_data['err']='<span style="color:red">Passwords not matching </span>';
					$this->load->view('login/reset_pass',$view_data);
				}
			}
		} else {
			header('location:'.base_url());
		}
	    $this->load->view('login/reset_pass',$view_data);
	}
}

