<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include BASEPATH.'../application/controllers/My_Controller.php';

class Video extends My_Controller {

	function __construct() {

		parent::__construct();

		$this->check_user_page_access();

		$this->load->model('User_model');

		$this->load->model('Video_model');

		$this->target_dir=BASEPATH.'../upload/profile_img/';

	}
	
	public function index() {


		$this->check_user_page_access();

		$data['active']='game_list';

		$data['list']=$this->General_model->video_detail();

		$content=$this->load->view('video/list.tpl.php',$data,true);

		$this->render($content);

	}

	

	public function insertVideo()

	{

		$this->check_user_page_access();

		$data=array(

		'game_id'=>$this->uri->segment(3),

		'video_url'=>$this->input->post('url', TRUE),

		'user_id'=>$this->session->userdata('user_id'),

		'created_at'=>date('Y-m-d'),

		 );

		$r=$this->General_model->insert_data('video',$data);

		if($r) {

			$this->session->set_userdata('message_succ', 'Successfully added');	

			header('location:'.base_url().'game/gameEdit/'.$this->uri->segment(3));

		} else {

			$this->session->set_userdata('message_err', 'Failure to add');	

			header('location:'.base_url().'game/gameEdit/');

		}	

	}


	public function addVideo() {

		$this->check_user_page_access();

		$data['active']='addVideo';

		$data['game']=$this->General_model->view_all_data('game','id','desc');

		$content=$this->load->view('video/add.tpl.php',$data,true);

		$this->render($content);

	}

	public function editVideo($id='-1') {

		$this->check_user_page_access();

		$data['active']='editVideo';

		$data['game']=$this->General_model->view_all_data('game','id','desc');

		$data['video']=$this->General_model->view_data('video',array('id'=>$id));

		$content=$this->load->view('video/edit.tpl.php',$data,true);

		$this->render($content);

	}


	public function videoUpdate()

	{

		$this->check_user_page_access();

		$data['active']='videoUpdate';

		$data=array(

		'game_id'=>$this->input->post('game_id', TRUE),

		'video_url'=>$this->input->post('video_url', TRUE),

		'user_id'=>$this->session->userdata('user_id'),

		'updated_at'=>date('Y-m-d'),

		 );

		 $r=$this->General_model->update_data('video',$data,array('id'=>$this->input->post('id', TRUE)));

		if($r)

			{

			$this->session->set_userdata('message_succ', 'Update successfull');	

			header('location:'.base_url().'video/editVideo/'.$this->input->post('id', TRUE));

			}

			else

			{

			$this->session->set_userdata('message_err', 'Failure to update');	

			header('location:'.base_url().'video/editVideo/'.$this->input->post('id', TRUE));

			}

		

	}

	//delete

	public function delete($id)

	{

		$this->check_user_page_access();

		

		 $r=$this->General_model->delete_data('game',array('id'=>$id));

		if($r)

		{

		$this->session->set_userdata('message_succ', 'Delete Successfull');	

		header('location:'.base_url().'admin/game/game_list/');

		}

		else

		{

		$this->session->set_userdata('message_err', 'Failure to delete');	

		header('location:'.base_url().'admin/game/game_list/');

		}

	}	

}

