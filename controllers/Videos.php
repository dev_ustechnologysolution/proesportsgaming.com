<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/My_Controller.php';
class Videos extends My_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->check_user_page_access();
        $this->load->model('My_model');
        $this->load->model('General_model');
        $this->load->library('pagination');
        $this->pageLimit=20;
        $this->check_cover_page_access();
    }
//page loading
    public function index() {
        $this->check_user_page_access();
        $data['banner_data'] = $this->General_model->tab_banner();
        $data['video_dt'] = $this->General_model->getvideos();
        $content=$this->load->view('videos/videolist.tpl.php',$data,true);
        $this->render($content);
    }
}