<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Attributes extends My_Controller 
{
  	function __construct()
    {
		parent::__construct();		
    }
	public function index() {
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Attributes List', 'admin/attributes');
		$data['active']='attributes_list';
		$data['page_name']='Attributes List';
		$data['list']=$this->General_model->view_all_data('attributes_tbl','id','desc');
		$content=$this->load->view('admin/attributes/attributes_list.tpl.php',$data,true);
		$this->render($content);
	}
	
	public function attributeAdd() 
	{
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Attributes List', 'admin/attributes');
		$this->breadcrumbs->push('Add Attribute', 'admin/attributes/attributeAdd');
		$data['active']='attributes_list';
		$id = $_REQUEST['id'];
		$data['attributes_data']=$this->General_model->view_data('attributes_tbl',array('id'=>$id));
		$data['attr_id'] = $id;
		$data['page_name']='Add Attribute';		
		$content=$this->load->view('admin/attributes/attributes.tpl.php',$data,true);
		$this->render($content);
	}
	public function attributeEdit() 
	{
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Attributes List', 'admin/attributes');
		$this->breadcrumbs->push('Edit Attribute', 'admin/attributes/attributeEdit');
		$id = $_REQUEST['id'];
		$data['attributes_data']=$this->General_model->view_data('attributes_tbl',array('id'=>$id));
		$option_data = $this->General_model->view_data('product_attributes_opt_tbl',array('attr_id'=>$id));
		$data['option_data']= $option_data;		
		$data['active']='attributes_list';
		$data['page_name']='Attribute Edit';
		if(empty($option_data)) {
			redirect(base_url().'admin/attributes/attributeAdd/'.$this->input->post('id', TRUE));
		} else {
			$content=$this->load->view('admin/attributes/attributes_edit.tpl.php',$data,true);
			$this->render($content);
		}
	}
	
	public function attributeInsert() 
	{
		try {
			$this->check_user_page_access();
			if($_POST['type'] == 'attribute'){

				$query = $this->db->get_where('attributes_tbl', array('title' => strtolower(trim($_POST['title'])))); 

                if ($query->num_rows() == 0 )
                {
                    $data = array(
						'type' => 3,
						'title' => strtolower(trim($_POST['title'])),
						'status' => 1,
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s')
					);	
					$attr_id = $this->General_model->insert_data('attributes_tbl',$data);
					if($attr_id){
						$this->session->set_userdata('message_succ', 'Insert successfull');
						redirect(base_url().'admin/attributes');
					} else {
						$this->session->set_userdata('message_err', 'Failure to update');	
						redirect(base_url().'admin/attributes');
					}
                } else {
                	$this->session->set_userdata('message_err', 'Given Attribute is existing.Please Enter another attribute.');	
					redirect(base_url().'admin/attributes');
                }
				
			} else {
				if(!empty($_POST['option_label'][0])){
					foreach ($_POST['option_label'] as $key => $value) {
						$option_data[] = array(
							'attr_id' => $_REQUEST["attr_id"],
							'option_label' => $value,
							'created_at' => date('Y-m-d H:i:s'),
							'updated_at' => date('Y-m-d H:i:s')
						);	
					}					
				 	$result = $this->db->insert_batch('product_attributes_opt_tbl',$option_data);
				 	if($result){
						$this->session->set_userdata('message_succ', 'Insert successfull');
						redirect(base_url().'admin/attributes');
					} else {
						$this->session->set_userdata('message_err', 'Failure to update');	
						redirect(base_url().'admin/attributes');
					}		
				}
			
			}
			
		} catch (Exception $e) { 
	        $this->session->set_userdata('message_err', $e->getMessage());
	        redirect(base_url().'admin/attributes');
	    }
	}
	
	public function attributeUpdate() 
	{
		try {
			$this->check_user_page_access();		
			$id = $this->input->post('id', TRUE);
			$attribute_data = $this->General_model->view_data('product_attributes_opt_tbl',array('attr_id'=>$id));
			$list_option_id = array_column($attribute_data, 'option_id');
			$insert_data_arr = array_diff($list_option_id,$_POST['option_data']);
			if(!empty($insert_data_arr)){
				$this->db->where_in('option_id', $insert_data_arr);
				$this->db->delete('product_attributes_opt_tbl');
			}
					
			$data = array(
				'type' => 3,
				'title' => trim($_POST['title']),
				'updated_at' => date('Y-m-d H:i:s')
			);
			
			$r = $this->General_model->update_data('attributes_tbl',$data,array('id'=>$id));
			if($r){
				$insert_data = $update_data = array();
				if(!empty($_POST['option_label'][0])){
					foreach($_POST['option_label'] as $key => $value){						
						foreach ($_POST['option_data'] as $k => $v) {							
							if($k == $key)
							{
								$update_data[] = array(
									'option_id' => $v,
									'option_label' => $value,									
									'updated_at' => date('Y-m-d H:i:s')
								);
							} 
						}	
					}
					$insert_data_arr = array_diff_key($_POST['option_label'],$_POST['option_data']);
					
					if(!empty($insert_data_arr)){
						foreach ($insert_data_arr as $label) {
							 $insert_data[] = array(
										'attr_id' => $id,
										'option_label' => $label,
										'created_at' => date('Y-m-d H:i:s'),
										'updated_at' => date('Y-m-d H:i:s')
									);
						}
						$this->db->insert_batch('product_attributes_opt_tbl',$insert_data);	 
					}
					if(!empty($update_data)) {
						$this->db->update_batch('product_attributes_opt_tbl',$update_data, 'option_id'); 
					}					
				}
				$this->session->set_userdata('message_succ', 'Update successfull');
				redirect(base_url().'admin/attributes');
			}else{
				$this->session->set_userdata('message_err', 'Failure to update');	
				redirect(base_url().'admin/attributes/attributeEdit/'.$this->input->post('id', TRUE));
			}
		} catch (Exception $e) { 
	        $this->session->set_userdata('message_err', $e->getMessage());
	        redirect(base_url().'admin/attributes/attributeEdit/'.$this->input->post('id', TRUE));
	    }
	}
	public function delete() 
	{
		$id = $_GET['id'];
		$this->check_user_page_access();
		$r=$this->General_model->delete_data('attributes_tbl',array('id'=> $id));
		if ($r) {
			$this->session->set_userdata('message_succ', 'Delete Successfull');	
			header('location:'.base_url().'admin/attributes');
		} else {
			$this->session->set_userdata('message_err', 'Failure to delete');	
			header('location:'.base_url().'admin/attributes');
		}
	}
	public function updateIsStatus()
	{
		$id 	= $_REQUEST['id'];
		$type 	= $_REQUEST['type'];
		$status = ($type == 'check') ? 1 : 0;
		$data=array(
			'status'=>$status,
		);
		$this->General_model->update_data('attributes_tbl',$data,array('id'=>$id));		
	}
	
}

