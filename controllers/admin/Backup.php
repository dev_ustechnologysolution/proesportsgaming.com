<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include BASEPATH.'../application/controllers/admin/My_Controller.php';

class Backup extends My_Controller 

{

  	function __construct()
    {

		parent::__construct();

	}

    public function index(){
    	$this->check_user_page_access();

		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('Site Backup', 'admin/Backup');


		$data['active']='site_backup';

		$data['page_name']='Site Backup';
		

		$data['backup_cat']='';

		$content=$this->load->view('admin/site_backup/site_backup.tpl.php',$data,true);

		$this->render($content);


    }
	public function file_backup()
	{
		$this->check_user_page_access();
		$this->load->library('zip');
		$file_name='backup['.date('d-m-Y-H-i-s').'].zip';
		$this->zip->read_dir($_SERVER['DOCUMENT_ROOT'],TRUE);
		$this->zip->download($file_name);
		exit();	
	}
	public function db_backup()
	{
		$this->check_user_page_access();
	 	$this->load->dbutil();
		$prefs = array(
			'format' => 'sql',
			'filename' => 'db_backup.sql'
		);
		$backup = & $this->dbutil->backup($prefs);
		$db_name = 'db_backup-on-' . date("Y-m-d-H-i-s") . '.sql';
		$save = $_SERVER['DOCUMENT_ROOT'].FCPATH.'/assets/dwnld/' . $db_name;
		$this->load->helper('file');
		write_file($save, $backup);
		$this->load->helper('download');
		force_download($db_name, $backup);
	}
	public function db_backup_cron()
	{
		$main_site_url = base_url();
		if($main_site_url == 'https://proesportsgaming.com/') {
			$db_saved_path = $_SERVER['DOCUMENT_ROOT'].'/database-proesportsgaming-live-website/';
		} else {
			$db_saved_path = $_SERVER['DOCUMENT_ROOT'].'/database-proesportsgaming-test-website/';
		}
	 	$this->load->dbutil();
		$prefs = array(
			'format' => 'sql',
			'filename' => 'db_backup.sql'
		);
		$backup = $this->dbutil->backup($prefs);
		$db_name = 'db_backup-on-' . date("Y-m-d-H-i-s") . '.sql';
		$save = $db_saved_path . $db_name;
		$this->load->helper('file');
		$res = write_file($save, $backup);
		if ($res) {
			echo "Database exported";
			exit();
		} else {
			echo $save;
		}
	}
}