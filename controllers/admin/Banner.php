<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';

class Banner extends My_Controller {
	function __construct() {
		parent::__construct();
		$this->target_dir=BASEPATH.'../upload/banner/';
	}

	public function index() {
		$this->check_user_page_access();
		$data['active_tab'] = (isset($_GET['type'])) ? 'banner' : $data['active_tab'] = 'lobby_event';
		
		$this->breadcrumbs->push('Home', 'admin/dashboard');
		$this->breadcrumbs->push('Banner List', 'admin/banner');
		$data['active'] = 'banner_list';
		$data['page_name'] = 'Banner List';
		$data['list'] = $this->General_model->view_all_data('banner','banner_order','asc');
		$data['lobby_status'] = $this->General_model->view_single_row('site_settings','option_title','live_lobbyy_banner');
		// $query = $this->db->query('SELECT l.lobby_banner_order,l.device_id,l.event_start_date,lg.user_id,lg.is_creator,l.event_image,l.is_spectator,l.is_event,GROUP_CONCAT(lg.stream_channel,"*_*+",lg.is_banner) as channel, l.game_id,gb.lobby_id,ag.game_name,ag.game_description FROM `lobby_group` as lg inner join group_bet as gb on gb.id = lg.`group_bet_id` and gb.bet_status = 1 inner JOIN lobby as l on l.id = gb.lobby_id and l.status = 1 inner join admin_game as ag on ag.id = l.game_id where lg.stream_channel IS NOT NULL and lg.stream_status = "enable" group by(gb.lobby_id) order by l.lobby_banner_order asc');
		// $data['live_lobby_data'] = $query->result();
		$live_lobby_data = $this->Lobby_model->get_lobby_banner_admin();

		$data['event_lobby_data'] = array_map(function($i){ return $i; },array_filter($live_lobby_data,function($item) { return $item->is_event == 1 && $item->channel != '*_*+0'; } ));
		$data['live_lobby_data'] = array_map(function($i){ return $i; },array_filter($live_lobby_data,function($item) { return $item->is_event == 0 && $item->channel != '*_*+0'; } ));
		$data['streamer_off_lobby_data'] = array_map(function($i){ return $i; },array_filter($live_lobby_data,function($item) { return $item->channel == '*_*+0'; } ));
		$content = $this->load->view('admin/banner/banner_list.tpl.php',$data,true);
		$this->render($content);
	}
	public function lobbyBannerEdit() {
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Banner List', 'admin/banner');
		$this->breadcrumbs->push('Edit Live Stream Banner', 'admin/banner/lobbyBannerEdit');
		$id = $_REQUEST['id'];
		$data['live_lobby_name'] = $_REQUEST['name'];
		$query  = $this->db->query('SELECT l.game_id,gb.lobby_id,lg.id,lg.user_id,lg.is_banner,lg.stream_channel,lg.table_cat,ud.display_name,ud.name FROM `lobby_group` as lg inner join group_bet as gb on gb.id = lg.`group_bet_id` and gb.bet_status = 1 inner JOIN lobby as l on l.id = gb.lobby_id and l.status = 1 inner join user_detail as ud on ud.user_id = lg.user_id where lg.stream_channel IS NOT NULL and lg.stream_status = "enable" and gb.lobby_id  ='.$id);
		$data['live_lobby_stream_data'] = $query->result();
		$data['active']='banner_list';
		$data['page_name']='Banner List';
	
		$content=$this->load->view('admin/banner/lobby_banner_list.tpl.php',$data,true);
		$this->render($content);
	}
	public function bannerAdd() {
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Banner List', 'admin/banner');
		$this->breadcrumbs->push('Add Banner', 'admin/banner/bannerAdd');
		$data['active']='banner';
		$data['page_name']='Add Banner';
		$content=$this->load->view('admin/banner/banner.tpl.php',$data,true);
		$this->render($content);
	}
	public function bannerInsert() {
		$this->check_user_page_access();
		$query  = $this->db->query('SELECT max(banner_order) as current_banner_order FROM banner');
        $current_banner_order = $query->result();
        $new_banner_order = $current_banner_order[0]->current_banner_order + 1;
		$img = '';
		$bname = $_POST['bname'] != 'undefined' ? $_POST['bname'] : '' ;
		$site_url = $_POST['site_url'] != 'undefined' ? $_POST['site_url'] : '' ;
		$content = $_POST['content'] != 'undefined' ? $_POST['content'] : '' ;
		$button1_title = $_POST['button1_title'] != 'undefined' ? $_POST['button1_title'] : '';
		$button1_link = $_POST['button1_link'] != 'undefined' ? $_POST['button1_link'] : '';
		$button2_title = $_POST['button2_title'] != 'undefined' ? $_POST['button2_title'] : '';
		$button2_link = $_POST['button2_link'] != 'undefined' ? $_POST['button2_link'] : '';
		$banner_youtube_link = $_POST['banner_youtube_link'] != 'undefined' ? $_POST['banner_youtube_link'] : '';
		$banner_youtube_link_duration = $_POST['banner_youtube_link_duration'] != 'undefined' ? $_POST['banner_youtube_link_duration'] : '';
		$data = array(
			'banner_name' => $bname,
			'banner_order' => $new_banner_order,
			'site_url' => $site_url,
			'banner_content' => $content,
			'banner_image' => $img,
			'button1_title' => $button1_title,
			'button1_link' => $button1_link,
			'button2_title' => $button2_title,
			'button2_link' => $button2_link,
		);

		if ($_POST['selected_banner'] == 'upload_image_banner') {
			if(isset($_FILES["image_banner"]["name"]) && $_FILES["image_banner"]["name"]!='') {
				$img=time().basename($_FILES["image_banner"]["name"]);
				$ext = end((explode(".", $img)));
				if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg') {
					$target_file = $this->target_dir.$img;
			        move_uploaded_file($_FILES["image_banner"]["tmp_name"], $target_file);
			    } else {
			    	$this->session->set_userdata('message_err', 'This is not a video file');
					redirect(base_url().'admin/banner/bannerAdd');
			    }
			}
		} else if ($_POST['selected_banner'] == 'youtube_link') {
			$data['banner_youtube_link'] = $banner_youtube_link;
			$data['banner_youtube_link_duration'] = $banner_youtube_link_duration;
		} else {
			$vid = '';
			if(isset($_FILES["video_banner"]["name"]) && $_FILES["video_banner"]["name"]!='') {
				$vid=time().basename($_FILES["video_banner"]["name"]);
				$ext_vid = end((explode(".", $vid)));
				if($ext_vid=='mp4' || $ext_vid=='avi' || $ext_vid=='mov' || $ext_vid=='3gp') {
					$target_file = $this->target_dir.$vid;
			        move_uploaded_file($_FILES["video_banner"]["tmp_name"], $target_file);
			    } else {
			    	$this->session->set_flashdata('message_err', 'This is not a video file, Please upload mp4/mov/avi');
					$res['url'] = base_url().'admin/banner/bannerAdd';
					echo json_encode($res);
					exit();
			    }
			}
			$data['banner_video_duration'] = $_POST['video_duration'];
			$data['banner_video'] = $vid;
		}
		$r = $this->General_model->insert_data('banner',$data);
		if($r) {
			$this->session->set_userdata('message_succ', 'Successfully added');
			$res['url'] = base_url().'admin/banner';
		} else {
			$this->session->set_userdata('message_err', 'Failure to add');	
			$res['url'] = base_url().'admin/banner/bannerAdd';
		}
		echo json_encode($res);
		exit();
	}
	//load edit page
	public function bannerEdit($id='-1') {
		$this->check_user_page_access();
		// add breadcrumbs
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Banner List', 'admin/banner');
		$this->breadcrumbs->push('Edit Banner', 'admin/banner/bannerEdit');
		$data['active']='banner_edit';
		$data['page_name']='Edit Page';
		$data['banner_data']=$this->General_model->view_data('banner',array('id'=>$id));
		$content=$this->load->view('admin/banner/banner_edit.tpl.php',$data,true);
		$this->render($content);
	}
	//edit 
	public function bannerUpdate() {
		$this->check_user_page_access();
		$data['active']='game_edit';
		$old_img = ($_POST['old_image_banner'] != '') ? $_POST['old_image_banner'] : '';
		$old_vid = ($_POST['old_video_banner'] != '') ? $_POST['old_video_banner'] : '';
		$old_youtube_link = ($_POST['old_youtube_link'] != '') ? $_POST['old_youtube_link'] : '';
		$duration = ($_POST['old_video_duration'] != '') ? $_POST['old_video_duration'] : '';

		$bname = $_POST['bname'] != 'undefined' ? $_POST['bname'] : '' ;
		$site_url = $_POST['site_url'] != 'undefined' ? $_POST['site_url'] : '' ;
		$content = $_POST['content'] != 'undefined' ? $_POST['content'] : '' ;
		$banner_order = $_POST['banner_order'] != 'undefined' ? $_POST['banner_order'] : '' ;
		$id = $_POST['id'];
		$button1_title = $_POST['button1_title'] != 'undefined' ? $_POST['button1_title'] : '';
		$button1_link = $_POST['button1_link'] != 'undefined' ? $_POST['button1_link'] : '';
		$button2_title = $_POST['button2_title'] != 'undefined' ? $_POST['button2_title'] : '';
		$button2_link = $_POST['button2_link'] != 'undefined' ? $_POST['button2_link'] : '';
		$banner_youtube_link = $_POST['banner_youtube_link'] != 'undefined' ? $_POST['banner_youtube_link'] : '';
		$banner_youtube_link_duration = $_POST['banner_youtube_link_duration'] != 'undefined' ? $_POST['banner_youtube_link_duration'] : '';

		$data = array(
			'banner_name' => $bname,
			'site_url' => $site_url,
			'banner_order' => $banner_order,
			'banner_content' => $content,
			'banner_video' => $old_vid,
			'banner_image' => $old_img,
			'banner_video_duration' => '',
			'button1_title' => $button1_title,
			'button1_link' => $button1_link,
			'button2_title' => $button2_title,
			'button2_link' => $button2_link,
		);
		if ($_POST['selected_banner'] == 'edit_banner_image') {
			if(isset($_FILES["image_banner"]["name"]) && $_FILES["image_banner"]["name"]!='') {
				$img=time().basename($_FILES["image_banner"]["name"]);
				$ext = end((explode(".", $img)));
				if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg') {
					$target_file = $this->target_dir.$img;
			        if(move_uploaded_file($_FILES["image_banner"]["tmp_name"], $target_file)) {
						if(file_exists($this->target_dir.$old_img)) {
							unlink($this->target_dir.$old_img);
						}
						if(file_exists($this->target_dir.$old_vid)) {
							unlink($this->target_dir.$old_vid);
						}						
					}
				}
			}
			$data['banner_video'] = '';
			$data['banner_video_duration'] = '';
			$data['banner_youtube_link'] = '';
			$data['banner_image'] = $img;
		} else if ($_POST['selected_banner'] == 'youtube_link') {
			$data['banner_video'] = '';
			$data['banner_video_duration'] = '';
			$data['banner_image'] = '';
			$data['banner_youtube_link'] = $banner_youtube_link;
			$data['banner_youtube_link_duration'] = $banner_youtube_link_duration;
		} else {
			if(isset($_FILES["file"]["name"]) && $_FILES["file"]["name"]!='') {
				$vid = time().basename($_FILES["file"]["name"]);
				$ext_vid = end((explode(".", $vid)));
				if($ext_vid =='mp4' || $ext_vid == 'avi' || $ext_vid == 'mov' || $ext_vid == '3gp') {
					$target_file = $this->target_dir.$vid;
					if(move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
						if(file_exists($this->target_dir.$old_vid)) {
							unlink($this->target_dir.$old_vid);
						}
						if(file_exists($this->target_dir.$old_img)) {
							unlink($this->target_dir.$old_img);
						}
					}
		    	} else {
		    		$this->session->set_flashdata('message_err', 'This is not a video file, Please upload mp4/mov/avi');
					$res['url'] = base_url().'admin/banner/bannerEdit/'.$id;
					echo json_encode($res);
					exit();
		    	}
		    } else {
		    	$vid = $old_vid;
		    }
		    $data['banner_youtube_link'] = '';
		    $data['banner_image'] = '';
			$data['banner_video'] = $vid; 
			$data['banner_video_duration'] = $duration;
		}
		$r = $this->General_model->update_data('banner',$data,array('id'=>$id));
		if ($r) {
			$this->session->set_userdata('message_succ', 'Update successfull');
		} else {
			$this->session->set_userdata('message_err', 'Failure to update');
		}
		$res['url'] = base_url().'admin/banner/bannerEdit/'.$id;
		echo json_encode($res);
		exit();
	}

	//delete
	public function delete($id) {
		$this->check_user_page_access();
		$r=$this->General_model->delete_data('banner',array('id'=>$id));
		if ($r) {
			$this->session->set_userdata('message_succ', 'Delete Successfull');	
			header('location:'.base_url().'admin/banner?type=banner');
		} else {
			$this->session->set_userdata('message_err', 'Failure to delete');	
			header('location:'.base_url().'admin/banner?type=banner');
		}
	}
	public function ChangeBannerOrder(){
			$id = $_REQUEST['id'];
			$user_data	= $this->General_model->view_data('banner',array('id'=>$id));
			$data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Banner Order   '.$user_data[0]['banner_order'].'</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="change_banner_order" name="change_banner_order" method="POST">
						<div class="box-body">
							<div class="form-group">
								<label>Change Banner Order </label>
								<input type="hidden" id="banner_id" class="form-control" name="banner_id" value="'.$id.'" required>
								<input type="text" id="new_order" class="form-control" name="new_order" value="" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
		echo $data_value;
	}
	public function UpdateBannerOrder(){
		$banner_id 	= $_REQUEST['banner_id'];
		$new_order 	= $_REQUEST['new_order'];
		$banner_data = $this->General_model->view_data('banner',array('id'=>$banner_id));
		if($banner_data[0]['banner_order'] != $new_order){
			$this->General_model->update_data('banner',array('banner_order' => $banner_data[0]['banner_order']),array('banner_order'=>$new_order));
		} 
		$data=array(
			'banner_order'=>$new_order,
		);
		$this->General_model->update_data('banner',$data,array('id'=>$banner_id));
	}
	public function tab_banner() {
		$this->check_user_page_access();
		// add breadcrumbs
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Tab Banner List', 'admin/banner/tab_banner');
		$data['active'] = 'tab_banner_list';
		$data['page_name'] = 'Tab Banner List';
		$data['list'] = $this->General_model->view_all_data('tab_banner','banner_order','asc');
		$content = $this->load->view('admin/banner/tab_banner_list.tpl.php',$data,true);
		$this->render($content);
	}
	public function tab_bannerAdd() {
		$this->check_user_page_access();
		// add breadcrumbs
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Tab Banner List', 'admin/tab_banner');
		$this->breadcrumbs->push('Add Tab Banner', 'admin/banner/tab_bannerAdd');
		$data['active'] = 'tab_banner';
		$data['page_name'] = 'Add Tab Banner';
		$content = $this->load->view('admin/banner/tab_banner.tpl.php',$data,true);
		$this->render($content);
	}
	public function tab_bannerInsert() {
		$this->check_user_page_access();
		$img = '';
		if ($_POST['selected_banner'] == 'upload_image_banner') {
			if(isset($_FILES["image_banner"]["name"]) && $_FILES["image_banner"]["name"]!='') {
				$img=time().basename($_FILES["image_banner"]["name"]);
				$ext = end((explode(".", $img)));
				if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg') {
					$target_file = $this->target_dir.$img;
			        move_uploaded_file($_FILES["image_banner"]["tmp_name"], $target_file);
			    } else {
			    	$this->session->set_flashdata('msg', 'This is not a image file');	
					redirect(base_url().'admin/banner/tab_bannerAdd');
			    }
			}
			$data=array(
				'banner_name'=>$this->input->post('bname', TRUE),
				'banner_order'=>$this->input->post('banner_order', TRUE),
				'site_url'=>$this->input->post('site_url', TRUE),
				'banner_content'=>$this->input->post('content', TRUE),
				'banner_image'=>$img
			);
		} else {
			$vid='';
			if(isset($_FILES["video_banner"]["name"]) && $_FILES["video_banner"]["name"]!='') {
				$vid=time().basename($_FILES["video_banner"]["name"]);
				$ext_vid = end((explode(".", $vid)));
				if($ext_vid=='wmv' || $ext_vid=='mp4' || $ext_vid=='avi' || $ext_vid=='mov' || $ext_vid=='3gp') {
					$target_file = $this->target_dir.$vid;
			        move_uploaded_file($_FILES["video_banner"]["tmp_name"], $target_file);
			    } else {
			    	$this->session->set_flashdata('msg', 'This is not a video file');	
					redirect(base_url().'admin/banner/tab_bannerAdd');
			    }
			}

			$data=array(
				'banner_name'=>$this->input->post('bname', TRUE),
				'banner_order'=>$this->input->post('banner_order', TRUE),
				'site_url'=>$this->input->post('site_url', TRUE),
				'banner_content'=>$this->input->post('content', TRUE),
				'banner_video_duration'=>$this->input->post('video_duration', TRUE),
				'banner_video'=>$vid
			);
		}
		$r = $this->General_model->insert_data('tab_banner',$data);
		if($r) {
			$this->session->set_userdata('message_succ', 'Successfully added');	
			redirect(base_url().'admin/banner/tab_banner');
		} else {
			$this->session->set_userdata('message_err', 'Failure to add');	
			redirect(base_url().'admin/banner/tab_bannerAdd');
		}
		//redirect(base_url().'admin/banner');
	}
	//load edit page

	public function tab_bannerEdit($id='-1') {
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
		$this->breadcrumbs->push('Tab Banner List', 'admin/banner/tab_banner');
		$this->breadcrumbs->push('Edit Tab Banner', 'admin/banner/tab_bannerEdit');
		$data['active']='tab_banner_edit';
		$data['page_name']='Edit Tab Banner';
		$data['banner_data']=$this->General_model->view_data('tab_banner',array('id'=>$id));
		$content=$this->load->view('admin/banner/tab_banner_edit.tpl.php',$data,true);
		$this->render($content);
	}

	//edit 
	public function tab_bannerUpdate() {
		$this->check_user_page_access();
		$old_img = $this->input->post('old_image_banner', TRUE);
		$old_vid = $this->input->post('old_video_banner', TRUE);
		$duration = $this->input->post('old_video_duration', TRUE);
		if ($_POST['selected_banner'] == 'edit_banner_image') {
			if (isset($_FILES["image_banner"]["name"]) && $_FILES["image_banner"]["name"]!='') {
				$img = time().basename($_FILES["image_banner"]["name"]);
				$ext = end((explode(".", $img)));
				if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg') {
					$target_file = $this->target_dir.$img;
					if (move_uploaded_file($_FILES["image_banner"]["tmp_name"], $target_file)) {
						if (file_exists($this->target_dir.$this->input->post('old_image_banner', TRUE))) {
							unlink($this->target_dir.$this->input->post('old_image_banner', TRUE));
						}
						if (file_exists($this->target_dir.$this->input->post('old_video_banner', TRUE))) {
							unlink($this->target_dir.$this->input->post('old_video_banner', TRUE));
						}
					}
				}
			} else {
				$img = $this->input->post('old_image_banner', TRUE);;
			}
			$data=array(
				'site_url'=>$this->input->post('site_url', TRUE),
				'banner_name'=>$this->input->post('bname', TRUE),
				'banner_order'=>$this->input->post('banner_order', TRUE),
				'banner_content'=>$this->input->post('content', TRUE),
				'banner_video'=>'',
				'banner_video_duration'=>'',
				'banner_image'=>$img
			);
		} else {
			if (isset($_FILES["video_banner"]["name"]) && $_FILES["video_banner"]["name"]!='') {
				$vid = time().basename($_FILES["video_banner"]["name"]);
				$ext_vid = end((explode(".", $vid)));
				if ($ext_vid=='wmv' || $ext_vid=='mp4' || $ext_vid=='avi' || $ext_vid=='mov' || $ext_vid=='3gp') {
					$target_file = $this->target_dir.$vid;
					if (move_uploaded_file($_FILES["video_banner"]["tmp_name"], $target_file)) {
						if (file_exists($this->target_dir.$this->input->post('old_video_banner', TRUE))) {
							unlink($this->target_dir.$this->input->post('old_video_banner', TRUE));
						}
						if (file_exists($this->target_dir.$this->input->post('old_image_banner', TRUE))) {
							unlink($this->target_dir.$this->input->post('old_image_banner', TRUE));
						}
					}
				} else {
					$this->session->set_flashdata('msg', 'This is not a video file');
					redirect(base_url().'admin/banner/tab_bannerEdit/'.$this->input->post('id', TRUE));
				}
			} else {
				$vid = $this->input->post('old_video_banner', TRUE);;
			}
			$data=array(
				'banner_order'=>$this->input->post('banner_order', TRUE),
				'banner_name'=>$this->input->post('bname', TRUE),
				'site_url'=>$this->input->post('site_url', TRUE),
				'banner_content'=>$this->input->post('content', TRUE),
				'banner_video_duration'=>$duration,
				'banner_video'=>$vid,
				'banner_image'=>'',
			);
		}
		$r = $this->General_model->update_data('tab_banner',$data,array('id'=>$this->input->post('id', TRUE)));
		($r) ? $this->session->set_userdata('message_succ', 'Update successfull') : $this->session->set_userdata('message_err', 'Failure to update');
		redirect(base_url().'admin/banner/tab_bannerEdit/'.$this->input->post('id', TRUE));
	}

	public function tab_bannerdelete($id) {
		$this->check_user_page_access();
		$r = $this->General_model->delete_data('tab_banner',array('id'=>$id));
		($r) ? $this->session->set_userdata('message_succ', 'Delete Successfull') : $this->session->set_userdata('message_err', 'Failure to delete');
		redirect(base_url().'admin/banner/tab_banner');
	}

	public function ChangeTabBannerOrder(){
			$id = $_REQUEST['id'];
			$user_data	= $this->General_model->view_data('tab_banner',array('id'=>$id));
			$data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Banner Order   '.$user_data[0]['banner_order'].'</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="change_banner_order" name="change_banner_order" method="POST">
						<div class="box-body">
							<div class="form-group">
								<label>Change Banner Order </label>
								<input type="hidden" id="banner_id" class="form-control" name="banner_id" value="'.$id.'" required>
								<input type="text" id="new_order" class="form-control" name="new_order" value="" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
		echo $data_value;
	}
	
	public function UpdateTabBannerOrder(){
		$banner_id 	= $_REQUEST['banner_id'];
		$new_order 	= $_REQUEST['new_order'];

		$data=array(
			'banner_order'=>$new_order,
		);
		$this->General_model->update_data('tab_banner',$data,array('id'=>$banner_id));
	}

	public function chat_banner()

	{

		$this->check_user_page_access();

		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('Chat Banner List', 'admin/banner/chat_banner');



		$data['active']='chat_banner_list';

		$data['page_name']='Chat Banner List';

		

		$data['list']=$this->General_model->view_all_data('chat_banner','banner_order','asc');

		$content=$this->load->view('admin/banner/chat_banner_list.tpl.php',$data,true);

		$this->render($content);

	}


	public function chat_bannerAdd()

	{

		$this->check_user_page_access();

		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('Chat Banner List', 'admin/banner/chat_banner');
			
		$this->breadcrumbs->push('Add Chat Banner', 'admin/banner/chat_bannerAdd');

		$data['active']='chat_banner';
		$data['page_name']='Add Chat Banner';

		$content=$this->load->view('admin/banner/chat_banner.tpl.php',$data,true);

		$this->render($content);

	}

	

	public function chat_bannerInsert()
	{

		$this->check_user_page_access();

		$img='';

		if ($_POST['selected_banner'] == 'upload_image_banner') {

			if(isset($_FILES["image_banner"]["name"]) && $_FILES["image_banner"]["name"]!='')

			{

				$img=time().basename($_FILES["image_banner"]["name"]);

				$ext = end((explode(".", $img)));
				
				if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg')
				{

					$target_file = $this->target_dir.$img;

			        move_uploaded_file($_FILES["image_banner"]["tmp_name"], $target_file);
			    }
			    else
			    {
			    	$this->session->set_flashdata('msg', 'This is not a image file');	
					redirect(base_url().'admin/banner/chat_bannerAdd');
			    }

			}
		
		$data=array(

		'site_url'=>$this->input->post('site_url', TRUE),

		'banner_name'=>$this->input->post('bname', TRUE),

		'banner_order'=>$this->input->post('banner_order', TRUE),

		'banner_content'=>$this->input->post('content', TRUE),

		'banner_image'=>$img

		);
		}
		else
		{
			$vid='';

			if(isset($_FILES["video_banner"]["name"]) && $_FILES["video_banner"]["name"]!='')

			{

				$vid=time().basename($_FILES["video_banner"]["name"]);

				$ext_vid = end((explode(".", $vid)));

			if($ext_vid=='wmv' || $ext_vid=='mp4' || $ext_vid=='avi' || $ext_vid=='mov' || $ext_vid=='3gp')
				{

					$target_file = $this->target_dir.$vid;

			        move_uploaded_file($_FILES["video_banner"]["tmp_name"], $target_file);
			    }
			    else
			    {
			    	$this->session->set_flashdata('msg', 'This is not a video file');	
					redirect(base_url().'admin/banner/tab_bannerAdd');
			    }

			}

		$data=array(

		'site_url'=>$this->input->post('site_url', TRUE),

		'banner_name'=>$this->input->post('bname', TRUE),

		'banner_order'=>$this->input->post('banner_order', TRUE),

		'banner_content'=>$this->input->post('content', TRUE),
		
		'banner_video_duration'=>$this->input->post('video_duration', TRUE),

		'banner_video'=>$vid

		);

		}


		$r=$this->General_model->insert_data('chat_banner',$data);

		if($r)

		{

			$this->session->set_userdata('message_succ', 'Successfully added');	

			redirect(base_url().'admin/banner/chat_banner');

		}

		else

		{

			$this->session->set_userdata('message_err', 'Failure to add');	

			redirect(base_url().'admin/banner/chat_bannerAdd');

		}

		//redirect(base_url().'admin/banner');
	}

	//load edit page

	public function chat_bannerEdit($id='-1')
	{

		$this->check_user_page_access();

		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('Chat Banner List', 'admin/banner/chat_banner');
			
		$this->breadcrumbs->push('Edit Chat Banner', 'admin/banner/chat_bannerEdit');



		$data['active']='chat_banner_edit';

		$data['page_name']='Edit Chat Banner';


		$data['banner_data']=$this->General_model->view_data('chat_banner',array('id'=>$id));

		$content=$this->load->view('admin/banner/chat_banner_edit.tpl.php',$data,true);

		$this->render($content);

	}

	//edit 

	public function chat_bannerUpdate()
	{
		
		$this->check_user_page_access();

		$old_img = $this->input->post('old_image_banner', TRUE);

		$old_vid = $this->input->post('old_video_banner', TRUE);

		$duration = $this->input->post('old_video_duration', TRUE);

		if ($_POST['selected_banner'] == 'edit_banner_image')
		{
			if(isset($_FILES["image_banner"]["name"]) && $_FILES["image_banner"]["name"]!='')
			{
				$img=time().basename($_FILES["image_banner"]["name"]);
				$ext = end((explode(".", $img)));			
				if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg')
				{
					$target_file = $this->target_dir.$img;
			        if(move_uploaded_file($_FILES["image_banner"]["tmp_name"], $target_file))
					{
						if(file_exists($this->target_dir.$this->input->post('old_image_banner', TRUE)))
						{
							unlink($this->target_dir.$this->input->post('old_image_banner', TRUE));

						}

						if(file_exists($this->target_dir.$this->input->post('old_video_banner', TRUE)))

						{

							unlink($this->target_dir.$this->input->post('old_video_banner', TRUE));

						}						

					}
				}

			}
			else
			{
				$img = $this->input->post('old_image_banner', TRUE);;
			}
			
			$data=array(

				'banner_name'=>$this->input->post('bname', TRUE),

				'banner_order'=>$this->input->post('banner_order', TRUE),

				'site_url'=>$this->input->post('site_url', TRUE),

				'banner_content'=>$this->input->post('content', TRUE),

				'banner_video'=>'',

				'banner_video_duration'=>'',

				'banner_image'=>$img

			);

		}
	
		else

		{

			if(isset($_FILES["video_banner"]["name"]) && $_FILES["video_banner"]["name"]!='')

			{

				$vid=time().basename($_FILES["video_banner"]["name"]);

				$ext_vid = end((explode(".", $vid)));

				if($ext_vid=='wmv' || $ext_vid=='mp4' || $ext_vid=='avi' || $ext_vid=='mov' || $ext_vid=='3gp')
				
					{

						$target_file = $this->target_dir.$vid;

			        	
			        	  if(move_uploaded_file($_FILES["video_banner"]["tmp_name"], $target_file))

							{

								if(file_exists($this->target_dir.$this->input->post('old_video_banner', TRUE)))

								{

									unlink($this->target_dir.$this->input->post('old_video_banner', TRUE));

								}

								if(file_exists($this->target_dir.$this->input->post('old_image_banner', TRUE)))

								{

									unlink($this->target_dir.$this->input->post('old_image_banner', TRUE));

								}

							}

			    	}
			    else
			    	{
			    	
			    		$this->session->set_flashdata('msg', 'This is not a video file');	
					
						redirect(base_url().'admin/banner/tab_bannerEdit/'.$this->input->post('id', TRUE));
			    	}

			}

			else
			{
				$vid = $this->input->post('old_video_banner', TRUE);;
			}
			

			$data=array(

				'banner_name'=>$this->input->post('bname', TRUE),

				'banner_content'=>$this->input->post('content', TRUE),
				
				'banner_video_duration'=>$duration,

				'site_url'=>$this->input->post('site_url', TRUE),

				'banner_video'=>$vid,

				'banner_image'=>'',

			);

		}

		

		 $r=$this->General_model->update_data('chat_banner',$data,array('id'=>$this->input->post('id', TRUE)));

		if($r)

		{

			$this->session->set_userdata('message_succ', 'Update successfull');	

			redirect(base_url().'admin/banner/chat_bannerEdit/'.$this->input->post('id', TRUE));

		}

		else

		{

			$this->session->set_userdata('message_err', 'Failure to update');	

			redirect(base_url().'admin/banner/chat_bannerEdit/'.$this->input->post('id', TRUE));

		}	

	}

	public function chat_bannerdelete($id)
	{

		$this->check_user_page_access();

		$r=$this->General_model->delete_data('chat_banner',array('id'=>$id));

		if($r)
		{

			$this->session->set_userdata('message_succ', 'Delete Successfull');	

			header('location:'.base_url().'admin/banner/chat_banner');

		}
		else
		{

			$this->session->set_userdata('message_err', 'Failure to delete');	

			header('location:'.base_url().'admin/banner/chat_banner');

		}
	}

	public function ChangeChatBannerOrder(){
			$id = $_REQUEST['id'];
			$user_data	= $this->General_model->view_data('chat_banner',array('id'=>$id));
			$data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Banner Order   '.$user_data[0]['banner_order'].'</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="change_banner_order" name="change_banner_order" method="POST">
						<div class="box-body">
							<div class="form-group">
								<label>Change Banner Order </label>
								<input type="hidden" id="banner_id" class="form-control" name="banner_id" value="'.$id.'" required>
								<input type="text" id="new_order" class="form-control" name="new_order" value="" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
						</div>
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
		echo $data_value;
	}
	public function UpdateChatBannerOrder()
	{
		$banner_id 	= $_REQUEST['banner_id'];
		$new_order 	= $_REQUEST['new_order'];

		$data=array(
			'banner_order'=>$new_order,
		);
		$this->General_model->update_data('chat_banner',$data,array('id'=>$banner_id));
	}
	public function updateSiteSettings(){
		$type = $_REQUEST['type'];
		$this->General_model->update_data('site_settings',array('option_value' => $type),array('option_title'=>'live_lobbyy_banner'));
	}
	public function updateIsSpectator(){
		$update_arr = ($_REQUEST['update_type'] == 'spectator') ? array('is_spectator' => $_REQUEST['type']) : array('is_event' => $_REQUEST['type']);
		$data = $this->General_model->update_data('lobby',$update_arr,array('id'=>$_REQUEST['id']));
	}

	public function getEventFee(){
		$global_event_fees=$this->General_model->view_single_row('site_settings','option_title','global_event_fees');
		$arr_result = explode("_", $global_event_fees['option_value']);
		$type_1 = $type_2 = '';
		($arr_result[0] == 1) ? $type_1 = 'selected' : $type_2 = 'selected';
		// Set Global Event Fee
		// <option '.$type_1.' value="1">Fix Amount</option>
		$data_value = '<div class="modal-header" style="border: none;">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title text-center" style="font-weight:bold;">Set Global</h4>
			</div>
			<div class="modal-body nav-tabs-custom">
				<ul class="nav nav-tabs" role="tablist">
		      <li class="active"><a role="tab" data-toggle="tab">Esports Fee</a></li>
		    </ul>
		    <div class="tab-content">
					<div class="row tab-pane active">
						<form action="'.base_url("admin/banner/updateIsEventFee").'" id="change_event_fee" name="change_event_fee" method="POST">
							<div class="box-body">						      
								<div class="form-group row">
									<label class="col-sm-2">Type </label>	
									<div class="col-sm-10">								
									   <select name="type" class="form-control">
									   <option '.$type_2.' value="2">Percentage</option>
									   </select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-2">Fee </label>
									<div class="col-sm-10">
									   <input required type="text" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46"  class="form-control event_fee" name="event_fee" value="'.$arr_result[1].'">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-2"></label>	
									<div class="col-sm-10"><span style="color:red;"> [ This fee will be calculate on new events ] </span></div>
								</div>
							</div>
							<div class="box-footer">
								<input class="btn btn-primary" type="submit" value="Submit" >
								<button type="button" class="btn btn-info pull-right" data-dismiss="modal">Close</button>
							</div>
						</form>
					</div>
				</div>		
			</div>';
		echo $data_value;
	}
	public function updateIsEventFee(){
		$insert_data = array('option_value' => $_REQUEST['type'].'_'.$_REQUEST['event_fee']);
		$this->General_model->update_data('site_settings',$insert_data,array('option_title' => 'global_event_fees'));
		// $query  = $this->db->query('SELECT l.id,l.is_event,l.event_price FROM `lobby_group` as lg inner join group_bet as gb on gb.id = lg.`group_bet_id` and gb.bet_status = 1 inner JOIN lobby as l on l.id = gb.lobby_id and l.status = 1 inner join admin_game as ag on ag.id = l.game_id where lg.stream_channel IS NOT NULL and l.is_event = 1 and lg.stream_status = "enable" group by(gb.lobby_id) order by l.lobby_order ASC' );
		
		// $isEventLobby = $query->result();
		// if(count($isEventLobby) > 0){
		// 	foreach($isEventLobby as $k => $event){
		// 		$event_fee = ($_REQUEST['type'] == 1) ? $_REQUEST['event_fee'] : $event->event_price * $_REQUEST['event_fee'] /100; 
		// 		$updateArray[] = array(
		// 			'id' => $event->id,
		// 			'event_fee' => $event_fee
		// 		);
		// 	}
		// 	$this->db->update_batch('lobby',$updateArray, 'id'); 
		// }
		$this->session->set_userdata('message_succ', 'Global Event Fee updated successfully.');
		redirect(base_url().'admin/banner');
	}
	public function getEventPrice() {
		$id = $_REQUEST['id'];
		$type = $_REQUEST['type'];
		$lobby_data	= $this->General_model->view_data('lobby',array('id'=>$id));
		$title = ($_REQUEST['type'] == 'event_price') ? 'Update Event Details' :'Update Spectate Price';
		$subtitle = ($_REQUEST['type'] == 'event_price') ? 'Event Price' :'Spectate Price';
		$price = ($_REQUEST['type'] == 'event_price') ? $lobby_data[0]['event_price'] : $lobby_data[0]['spectate_price'];
		$fee = ($_REQUEST['type'] == 'event_price') ? $lobby_data[0]['event_fee'] : $lobby_data[0]['spectator_fee'];
		$event_title = ($lobby_data[0]['event_title'] != '') ? $lobby_data[0]['event_title'] : '';
		$event_acc = ($lobby_data[0]['event_creator'] != '') ? $lobby_data[0]['event_creator'] : '';
		$event_description = ($lobby_data[0]['event_description'] != '') ? $lobby_data[0]['event_description'] : '';
		$event_start_date =  ($lobby_data[0]['event_start_date'] != '') ? $lobby_data[0]['event_start_date'] : '';
		$event_end_date =  ($lobby_data[0]['event_end_date'] != '') ? $lobby_data[0]['event_end_date'] : '';
		$acc_credit = $acc_credit = number_format((float) ($price - $fee), 2, '.', '');

		$global_event_fees = $this->General_model->view_single_row('site_settings','option_title','global_event_fees');
		$arr_result = explode("_", $global_event_fees['option_value']);
		$calctype = $arr_result[0];
		$calcval = $arr_result[1];
		$event_price = ($lobby_data[0]['event_price'] !='') ? $lobby_data[0]['event_price'] : 0;
		($lobby_data[0]['is_event_image'] == 1) ? $check = 'checked' : '';
		if ($_REQUEST['type'] == 'event_price') {
			$class = '';
			$img = '<lable class="col-sm-2"><b>Image Preview:</b></lable><div class="col-sm-10"><img style="border: 1px solid grey;border-radius: 12px;"width="200" src="'.base_url().'upload/banner/'.$lobby_data[0]['event_image'].'"></div>
			';
			if($lobby_data[0]['event_image'] == ''){
				$class = 'required';
				$img = '';
			}
			$data = '
			<div class="form-group row">
						<lable class="col-sm-2"><b>Image</b></lable>
							<div class="col-sm-10">
						       <input '.$class.' type="file" id="event_image" class="form-control" name="event_image">
							</div>
						</div>
						<div id="previewImage" class="form-group row" style="margin-top: 10px;">'.$img.'</div><div class="form-group row">
			<lable class="col-sm-2"><b>Image View</b></lable>			
			<div class="pull-left col-sm-10 banner_page">
        <label class="switch"><input type="checkbox" name="is_event_image"  '.$check.'><span class="slider round"></span></label>
      </div>
			</div>';
		}
		$data_value = '<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">'.$title.'</h4>
		</div>
		<div class="modal-body">
			<div class="row">
				<form enctype="multipart/form-data" action="'.base_url("admin/banner/updateIsEventPrice").'" id="change_event_price" name="change_event_price" method="POST">
					<div class="box-body">
						<input type="hidden" id="lobby_id" class="form-control" name="lobby_id" value="'.$id.'" required>
						<input type="hidden" id="type" class="form-control" name="type" value="'.$lobby_data[0]['is_event'].'" required>
						<input type="hidden" class="form-control" id="price_type" name="price_type" value="'.$_REQUEST['type'].'" required>	
						<input type="hidden" class="form-control" name="old_event_banner" value="'.$lobby_data[0]['event_image'].'" required>
						<input type="hidden" class="form-control" id="old_spectator_price" value="'.$lobby_data[0]['spectate_price'].'" required>
						<input type="hidden" id="old_event_price" class="form-control" value="'.$event_price.'" required>
						<input type="hidden" class="calctype" value="'.$calctype.'">
						<input type="hidden" class="calcval" value="'.$calcval.'">
						<div class="form-group row">
							<label class="col-sm-2">'.$subtitle.' ($) </label>	
							<div class="col-sm-10">
								<input required type="text" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46"  id="event_price" class="form-control" name="event_price" value="'.number_format((float) $price, 2, '.', '').'" placeholder="Enter Price">
								<p style="color:red;" class="err_msg"></p>
							</div>
						</div>';
						if ($type == 'event_price') {
							$formula = ($calctype == 2) ? '<p>[Calculation Formula : (Price * '.$calcval.')/100 = $.fee amount per Ticket.]</p>' : '';
							$data_value .= '
							<div class="form-group row">
							    <label class="col-sm-2">Event Title</label>
							    <div class="col-sm-10">
							    	<input required type="text" name="event_title" class="form-control" value="'.$event_title.'" placeholder="Enter title">
							    </div>
							</div>
							<div class="form-group row">
							    <label class="col-sm-2">Event description</label>
							    <div class="col-sm-10">
							    <textarea required name="event_description" rows="3" class="form-control" placeholder="Enter description">'.$event_description.'</textarea>
							    </div>
							</div>
							<div class="form-group row">
								<div class="col-sm-6 padd0">
							    <label class="col-sm-4">Event start date</label>
							    <div class="col-sm-8">
							    	<input required type="text" name="event_start_date" class="form-control eve_datetimepicker" value="'.$event_start_date.'" placeholder="Enter start date">
							    </div>
							  </div>
								<div class="col-sm-6 padd0">
								    <label class="col-sm-4">Event end date</label>
								    <div class="col-sm-8">
								    	<input required type="text" name="event_end_date" class="form-control eve_datetimepicker" value="'.$event_end_date.'" placeholder="Enter end date">
								    </div>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2">Only date</label>
                <div class="col-md-10" style="margin-bottom: 5px;">
                  <span>
                    <label style=""> <span class="pull-left"><label class="cntnr"><input type="checkbox" class="onlydate" id="onlydate" value=""><span class="chckmrk"></span></label></span></label>
                  </span>
                </div>
              </div>
							<div class="form-group row">
							  <label class="col-sm-2">Esports Fee ($) </label>	
						    <div class="col-sm-10">
						    	<input required type="text" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46"  class="form-control event_fee" name="event_fee" value="'.number_format((float) $fee, 2, '.', '').'" >
						    	'.$formula.'
						    	<p style="color:red;" class="fee_err_msg"></p>
						    </div>
							</div>
							<div class="form-group row">
						    <label class="col-sm-2">Esports Acct# </label>
						    <div class="col-sm-10">
						    	<input required type="number" class="form-control" value="0" readonly>
						    </div>
						  </div>
							<div class="form-group row">
						    <label class="col-sm-2">Prize Acct# </label>
						    <div class="col-sm-10">
						    	<input required type="text" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46"   class="form-control" name="event_acc" value="'.$event_acc.'" >
						    </div>
						  </div>
							<div class="form-group row">
						    <label class="col-sm-2">Prize Amt ($) </label>
						    <div class="col-sm-10">
						    <input type="number" name="acc_price_credited" required class="form-control" value="'.$acc_credit.'" readonly>
						    </div>
						  </div>';
						}
					$data_value .= $data.'</div>
					<div class="box-footer">
						<input class="btn btn-primary" type="submit" value="Submit" >
						<button type="button" class="btn btn-info pull-right" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>';
		echo $data_value;
	}
	public function updateIsBanner() {
		$id = $_REQUEST['id'];
		$type = $_REQUEST['type'];		
		$is_banner = ($type == 'check') ? 1 : 0;
		$data = array(
			'is_banner'=>$is_banner,
		);
		$this->General_model->update_data('lobby_group',$data,array('id'=>$id));		
	}
	public function is_banner_flag_all() {
		$this->check_user_page_access();
		if (!empty($_POST)) {
			$is_banner = ($_POST['submit'] == 'on') ? 1 : 0;
			$is_event_arr = (isset($_POST['is_event_arr']) && $_POST['is_event_arr'] !='') ? implode(', ', json_decode($_POST['is_event_arr']))  : '';
			$res = $this->db->set('is_banner',$is_banner)->where('is_banner !=',NULL)->where_not_in('user_id',$is_event_arr)->update('lobby_group');
			($res) ? $this->session->set_userdata('message_succ', 'All live stream banners are '.strtoupper($_POST['submit'])) : $this->session->set_userdata('message_err', 'Unable to update the stream banners.') ;
				redirect(base_url().'admin/banner/');
		}
	}
	public function updateIsEventPrice() {
		if (!empty($_POST)) {
			if ($_FILES["event_image"]["name"] != '') {
				$event_img = time().'_event_img_'.basename($_FILES["event_image"]["name"]);
				$target_file = $this->target_dir.$event_img;
				if (move_uploaded_file($_FILES["event_image"]["tmp_name"], $target_file)) {
					if (file_exists($this->target_dir.$this->input->post('old_event_banner', TRUE))) {
						unlink($this->target_dir.$this->input->post('old_event_banner', TRUE));
					}
				}
			} else {
				$event_img = $this->input->post('old_event_banner', TRUE);
			}
			$data['is_event_image'] = ($_REQUEST['is_event_image'] == 'on') ? 1 : 0;

			$type = $_REQUEST['type'];
			$price_type = $_REQUEST['price_type'];
			$data['is_event'] = ($type == '1') ? 1 : 0;
			$data['event_image'] = $event_img;
			
			if ($price_type == 'event_price') {
				$acc_no = trim($_REQUEST['event_acc']);
				// $user_data = $this->General_model->view_data('user_detail',array('account_no'=>$acc_no));
				$data['event_title'] = trim($_REQUEST['event_acc']);
				$data['event_title'] = $_REQUEST['event_title'];
				$data['event_description'] = $_REQUEST['event_description'];
				$data['event_creator'] = $acc_no;
				if (!empty($_POST['event_start_date']) && !empty($_POST['event_end_date'])) {
					$update_start = date('Y-m-d\TH:i:sP',strtotime($_POST['event_start_date']));
					$update_end = date('Y-m-d\TH:i:sP',strtotime($_POST['event_end_date']));
					$arr = array(
						'id' => $_POST['lobby_id'],
						'event_type' => 'lobby_event',
					);
					$getevedetail = $this->Tournaments_model->event_start_end_date($arr)[0];
					if ($getevedetail->timezone != '') {
						date_default_timezone_set($getevedetail->timezone);
						$update_start = date('Y-m-d\TH:i:sP',strtotime($update_start));
						$update_end = date('Y-m-d\TH:i:sP',strtotime($update_end));
					}
			    $data['event_start_date'] = $update_start;
			    $data['event_end_date'] = $update_end;
				}
				$data['event_price'] = $_REQUEST['event_price'];
				$data['event_fee'] = $_REQUEST['event_fee'];
				$data['event_fee_creator'] = trim($_REQUEST['event_acc']);
			} else {
				$data['spectate_price'] = $_REQUEST['event_price'];	
				$data['spectator_fee'] = $_REQUEST['event_fee'];		
			}		
			$this->General_model->update_data('lobby',$data,array('id'=>$_REQUEST['lobby_id']));
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	public function getEventsUsers() {
		$this->breadcrumbs->push('Home', 'admin/dashboard');
		$this->breadcrumbs->push('Banner List', 'admin/banner');
		$this->breadcrumbs->push('Event Users', 'admin/banner/getEventsUsers');
		$id = $_REQUEST['id'];		
		$query  = $this->db->query('SELECT l.event_price,l.event_fee,l.spectator_fee,l.event_fee_creator,ud.account_no,ud.name,ud.display_name,u.email,ud.team_name,eu.user_id,eu.is_gift,eu.gift_to,eu.created from events_users as eu inner join user_detail as ud on ud.user_id = eu.user_id inner join user as u on u.id = ud.user_id inner join lobby as l on l.id=eu.lobby_id where eu.lobby_id ='.$id.' and eu.type = 1 order by eu.created desc');
		$data['events_users'] = $query->result();
		$data['active']='banner_list';
		$data['page_name']='Event User List';
		$data['lobby_id'] = $id;
	
		$content=$this->load->view('admin/banner/events_users.tpl.php',$data,true);
		$this->render($content);
	}
	public function getSpectateUsers() {
		$this->breadcrumbs->push('Home', 'admin/dashboard');
		$this->breadcrumbs->push('Banner List', 'admin/banner');
		$this->breadcrumbs->push('Spectate Users', 'admin/banner/getSpectateUsers');

		$id = $_REQUEST['id'];		
		$query  = $this->db->query('SELECT l.spectate_price,ud.account_no,ud.name,ud.display_name,u.email,ud.team_name,eu.user_id,eu.is_gift,eu.gift_to,eu.created from events_users as eu inner join user_detail as ud on ud.user_id = eu.user_id inner join user as u on u.id = ud.user_id inner join lobby as l on l.id=eu.lobby_id where eu.lobby_id ='.$id.' and eu.type = 2 order by eu.created desc');
		$data['events_users'] = $query->result();
		$data['active']='banner_list';
		$data['page_name']='Spectate User List';
		$data['lobby_id'] = $id;
	
		$content=$this->load->view('admin/banner/spectate_users.tpl.php',$data,true);
		$this->render($content);
	}
	public function ChangeLobbyOrder()
	{
		$id = $_REQUEST['id'];
		$lobby_data	= $this->General_model->view_data('lobby',array('id'=>$id));
		$data_value = '<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Lobby Order   '.$lobby_data[0]['lobby_banner_order'].'</h4>
		</div>
		<div class="modal-body">
			<div class="row">
				<form action="#" id="change_lobby_order" name="change_lobby_order" method="POST">
					<div class="box-body">
						<div class="form-group">
							<label>Change Lobby Order </label>
							<input type="hidden" id="lobby_id" class="form-control" name="lobby_id" value="'.$id.'" required>
							<input type="text" id="new_order" class="form-control" name="new_order" value="" required>
						</div>
					</div>
					<div class="box-footer">
						<input class="btn btn-primary" type="submit" value="Submit" >
						<button type="button" class="btn btn-info pull-right" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>';
		echo $data_value;
	}
	public function UpdateLobbyOrder(){
		$lobby_id 	= $_REQUEST['lobby_id'];
		$new_order 	= $_REQUEST['new_order'];
		$lobby_data = $this->General_model->view_data('lobby',array('id'=>$this->input->post('lobby_id', TRUE)));
		
		if ($lobby_data[0]['lobby_banner_order'] != $_REQUEST['new_order']) {
			$this->General_model->update_data('lobby',array('lobby_banner_order' => $lobby_data[0]['lobby_banner_order']),array('lobby_banner_order'=>$_REQUEST['new_order']));
		} 
		$data['lobby_banner_order'] = $new_order;
		$this->General_model->update_data('lobby',$data,array('id'=>$lobby_id));
	}
	
	public function eventUserDelete(){
		 
		$lobby_id 	= $_REQUEST['lobby_id'];		
		$type = $_REQUEST['type'];
			if($type == 1){
				$where = array('lobby_id'=>$lobby_id,'type' => 1);
			} else {
				$where = array('lobby_id'=>$lobby_id,'type' => 2);
			}
		
			$r=$this->General_model->delete_data('events_users',$where);

			if($r) 
			{
				$this->session->set_userdata('message_succ', 'Reset Successful');	
				$data_value = true;
			} else {
				$this->session->set_userdata('message_err', 'Fail to reset');	
				$data_value = false;
			}
			redirect(base_url('admin/banner'));
		
	}

	public function reset_selected_users(){
		$user_ids = explode(',', $_POST['selected_users']);
		$lobby_id 	= $_REQUEST['lobby_id'];

		// $lobby_data = $this->General_model->view_data('lobby',array('id'=>$lobby_id));
		// foreach ($user_ids as $value) {
		// 	$this->db->where('user_id', $value);
		// 	$this->db->set('total_balance', 'total_balance + '.$lobby_data[0]['event_price'], FALSE);
		// 	$this->db->update('user_detail');
		// }
		$this->db->where('lobby_id', $lobby_id);
		$this->db->where_in('user_id', $user_ids);
		$r = $this->db->delete('events_users');
		if($r) 
		{
			$this->session->set_userdata('message_succ', 'Reset Successful');	
			$data_value = true;
		} else {
			$this->session->set_userdata('message_err', 'Fail to reset');	
			$data_value = false; 
		}
		redirect(base_url().'admin/banner/getEventsUsers?id='.$lobby_id);
	}

	public function refund_selected_users(){
		$user_ids = explode(',', $_POST['selected_users_rf']);
		$lobby_id 	= $_REQUEST['lobby_id'];

		$lobby_data = $this->General_model->view_data('lobby',array('id'=>$lobby_id));
		foreach ($user_ids as $value) {
			$this->db->where('user_id', $value);
			$this->db->set('total_balance', 'total_balance + '.$lobby_data[0]['event_price'], FALSE);
			$this->db->update('user_detail');

			$this->db->where('user_id', $lobby_data[0]['user_id']);
			$this->db->set('total_balance', 'total_balance - '.($lobby_data[0]['event_price'] - $lobby_data[0]['event_fee']), FALSE);
			$this->db->update('user_detail');
		}
		$this->db->where('lobby_id', $lobby_id);
		$this->db->where_in('user_id', $user_ids);
		$r = $this->db->delete('events_users');
		if($r) 
		{
			$this->session->set_userdata('message_succ', 'Refund Successful');	
			$data_value = true;
		} else {
			$this->session->set_userdata('message_err', 'Fail to reset');	
			$data_value = false; 
		}
		redirect(base_url().'admin/banner/getEventsUsers?id='.$lobby_id);
	}

	public function reset_selected_spec(){
		$user_ids = explode(',', $_POST['selected_spec']);
		$lobby_id 	= $_REQUEST['lobby_id'];

		// $lobby_data = $this->General_model->view_data('lobby',array('id'=>$lobby_id));
		// foreach ($user_ids as $value) {
		// 	$this->db->where('user_id', $value);
		// 	$this->db->set('total_balance', 'total_balance + '.$lobby_data[0]['spectate_price'], FALSE);
		// 	$this->db->update('user_detail');
		// }
		$this->db->where('lobby_id', $lobby_id);
		$this->db->where_in('user_id', $user_ids);
		$r = $this->db->delete('events_users');
		if($r) 
		{
			$this->session->set_userdata('message_succ', 'Reset Successful');	
			$data_value = true;
		} else {
			$this->session->set_userdata('message_err', 'Fail to reset');	
			$data_value = false; 
		}
		redirect(base_url().'admin/banner/getSpectateUsers?id='.$lobby_id);
	}

	public function refund_selected_spec(){
		$user_ids = explode(',', $_POST['selected_spec_rf']);
		$lobby_id 	= $_REQUEST['lobby_id'];

		$lobby_data = $this->General_model->view_data('lobby',array('id'=>$lobby_id));
		foreach ($user_ids as $value) {
			$this->db->where('user_id', $value);
			$this->db->set('total_balance', 'total_balance + '.$lobby_data[0]['spectate_price'], FALSE);
			$this->db->update('user_detail');

			$this->db->where('user_id', $lobby_data[0]['user_id']);
			$this->db->set('total_balance', 'total_balance - '.($lobby_data[0]['spectate_price'] - $lobby_data[0]['spectator_fee']), FALSE);
			$this->db->update('user_detail');
		}
		$this->db->where('lobby_id', $lobby_id);
		$this->db->where_in('user_id', $user_ids);
		$r = $this->db->delete('events_users');
		if($r) 
		{
			$this->session->set_userdata('message_succ', 'Refund Successful');	
			$data_value = true;
		} else {
			$this->session->set_userdata('message_err', 'Fail to reset');	
			$data_value = false; 
		}
		redirect(base_url().'admin/banner/getSpectateUsers?id='.$lobby_id);
	}

}

