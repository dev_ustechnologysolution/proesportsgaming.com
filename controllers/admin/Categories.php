<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Categories extends My_Controller 
{
  	function __construct()
    {
		parent::__construct();
		$this->load->model('Product_model');
    }
	public function index() {
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Categories List', 'admin/categories');
		$data['active']='categories_list';
		$data['page_name']='Categories List';
		$data['list']=$this->General_model->view_all_data('product_categories_tbl','id','asc');		
		$content=$this->load->view('admin/categories/categories_list.tpl.php',$data,true);
		$this->render($content);
	}
	
	public function categoriesAdd() 
	{
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Categories List', 'admin/categories');
		$this->breadcrumbs->push('Add Category', 'admin/categories/categoriesAdd');
		$data['active']='categories_list';
		$data['type'] = $_REQUEST['type'];
		$data['page_name']='Add Category';
		$data['category_list'] = $this->Product_model->getCategories();
		$content=$this->load->view('admin/categories/categories.tpl.php',$data,true);
		$this->render($content);
	}
	public function categoriesEdit() 
	{
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Categories List', 'admin/categories');
		$this->breadcrumbs->push('Edit Categories', 'admin/categories/categoriesEdit');
		$id = $_REQUEST['id'];
		$data['type'] = $_REQUEST['type'];
		$data['category_data']=$this->General_model->view_data('product_categories_tbl',array('id'=>$id));
		$data['category_list'] = $this->Product_model->getCategories();
		$data['active']='categories_list';
		$data['page_name']='Category Edit';
	
		$content=$this->load->view('admin/categories/categories.tpl.php',$data,true);
		$this->render($content);
	}
	
	public function categoriesInsert() 
	{
		
		$this->check_user_page_access();
		$query = $this->db->query("SELECT MAX(sort_num) as sort_num FROM product_categories_tbl");
		$row = $query->row();
		$data = array(
			'parent_id' => (isset($_POST['parent_id'])) ? $_POST['parent_id'] : 0,
			'name' => trim($_REQUEST['name']),
			'status' => 1,
			'sort_num' => $row->sort_num +1,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		);
		
		$r = $this->General_model->insert_data('product_categories_tbl',$data);
		
		if($r){
			$this->session->set_userdata('message_succ', 'Category Insert successfull');
			redirect(base_url().'admin/categories');
		}else{
			$this->session->set_userdata('message_err', 'Failure to update');	
			redirect(base_url().'admin/categories');
		}
	}
	public function categoriesUpdate() 
	{
		$this->check_user_page_access();		
		$id = $this->input->post('id', TRUE);
		$data = array(
			'parent_id' => $_POST['parent_id'],
			'name' => trim($_POST['name']),
			'updated_at' => date('Y-m-d H:i:s')
		);
		
		$r = $this->General_model->update_data('product_categories_tbl',$data,array('id'=>$id));
		if($r){
			$this->session->set_userdata('message_succ', 'Update successfull');
			redirect(base_url().'admin/categories');
		}else{
			$this->session->set_userdata('message_err', 'Failure to update');	
			redirect(base_url().'admin/categories/categoriesEdit/'.$this->input->post('id', TRUE));
		}
	}
	public function delete() 
	{
		$id = $_GET['id'];
		$this->check_user_page_access();
		$r=$this->General_model->delete_data('product_categories_tbl',array('id'=> $id));
		if ($r) {
			$this->session->set_userdata('message_succ', 'Delete Successfull');	
			header('location:'.base_url().'admin/categories');
		} else {
			$this->session->set_userdata('message_err', 'Failure to delete');	
			header('location:'.base_url().'admin/categories');
		}
	}
	public function updateIsStatus()
	{
		$id 	= $_REQUEST['id'];
		$type 	= $_REQUEST['type'];
		$status = ($type == 'check') ? 1 : 0;
		$data=array(
			'status'=>$status,
		);
		$this->General_model->update_data('product_categories_tbl',$data,array('id'=>$id));		
	}
	public function ChangeCategoryOrder()
	{
		$id = $_REQUEST['id'];
		$cat_data	= $this->General_model->view_data('product_categories_tbl',array('id'=>$id));
		$data_value = '<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Lobby Order   '.$cat_data[0]['sort_num'].'</h4>
		</div>
		<div class="modal-body">
			<div class="row">
				<form action="#" id="change_lobby_order" name="change_lobby_order" method="POST">
					<div class="box-body">
						<div class="form-group">
							<label>Change Lobby Order </label>
							<input type="hidden" id="lobby_id" class="form-control" name="cat_id" value="'.$id.'" required>
							<input type="text" id="new_order" class="form-control" name="new_order" value="" required>
						</div>
					</div>
					<div class="box-footer">
						<input class="btn btn-primary" type="submit" value="Submit" >
						<button type="button" class="btn btn-info pull-right" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>';
		echo $data_value;
	}
	public function UpdateCatOrder(){
		$cat_id 	= $_REQUEST['cat_id'];
		$new_order 	= $_REQUEST['new_order'];

		$cat_data = $this->General_model->view_data('product_categories_tbl',array('id'=>$this->input->post('cat_id', TRUE)));

		
		if($cat_data[0]['sort_num'] != $_REQUEST['new_order']){
			
			$this->General_model->update_data('product_categories_tbl',array('sort_num' => $cat_data[0]['sort_num']),array('sort_num'=>$_REQUEST['new_order']));
		} 

		$data=array(
			'sort_num'=>$new_order,
		);
		$this->General_model->update_data('product_categories_tbl',$data,array('id'=>$cat_id));
	}
	
}

