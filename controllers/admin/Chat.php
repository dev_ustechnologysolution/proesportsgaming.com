<?php



defined('BASEPATH') OR exit('No direct script access allowed');



include BASEPATH.'../application/controllers/admin/My_Controller.php';

// require_once('PHPExcel.php');

class Chat extends My_Controller 



{



  	function __construct()



    {



	parent::__construct();





	}



	public function index()

	{

		// $this->check_user_page_access();

		$this->breadcrumbs->push('Home', 'admin/dashboard');

		$this->breadcrumbs->push('Customer Service Agent List', 'admin/chat');

        $this->check_user_page_access();

		$data['active']='customer_service_agent_list';

		$data['page_name']='Customer Service Agent List';

		$content=$this->load->view('admin/chat/customer_service_chat',$data,true);

		$this->render($content);

	}

	



	public function admin_csConsole()

	{

		$this->check_user_page_access();



		$this->breadcrumbs->push('Home', 'admin/dashboard');



        $this->breadcrumbs->push('Admin chat console', 'admin/chat/admin_csConsole');



		$data['active']='admin_csConsole';



		$data['page_name']='Admin chat console';



		$content=$this->load->view('admin/chat/admin_chat_console',$data,true);

		$this->render($content);



	}



	public function chat_with_cust()

	{

		$this->check_user_page_access();



		$this->breadcrumbs->push('Home', 'admin/dashboard');



		$this->breadcrumbs->push('Customer Service Agent List', 'admin/chat');



        $this->breadcrumbs->push('Chat With Customer', 'admin/chat/chat_with_cust');



		$data['active']='customer_service_agent_list';


		$data['page_name']='Chat With Customer';



		$content=$this->load->view('admin/chat/customer_service_chat_box',$data,true);

		$this->render($content);



	}



	public function cust_chat_history()

	{

		$this->check_user_page_access();



		$this->breadcrumbs->push('Home', 'admin/dashboard');

		$this->breadcrumbs->push('Customer Service Agent List', 'admin/chat');

        $this->breadcrumbs->push('Customer Chat History', 'admin/chat/cust_chat_history');



		$data['active']='customer_service_agent_list';



		$data['page_name']='Customer Chat History';



		$content=$this->load->view('admin/chat/customer_chat_history',$data,true);

		$this->render($content);



	}



	

	public function excel_export()

	{



		$this->load->library('excel');



		$object = new PHPExcel();

		$object->setActiveSheetIndex(0);

		$table_columns = array('Sl No.', 'Customer Service Online',	'Agent Type', 	'Notes', 'E-Mail', 'Ph.#', 'Game Category', 'Login Time', 'Logout Time',	'Total Time');

		$column = 0;

		foreach ($table_columns as $field) {

	 		$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);

			$column++;

		}



		$CI =& get_instance();

        $result = $CI->chatsocket->getConversationCustHistAdminside();

        $excel_row = 2;

		if ($result['status'] == true) 

		{

		$conversation = $result['users_conversation'];

		foreach ($conversation as $key => $value) 

		{

		$user_to = $value['user_to'];

		$offline = 'offline';

		$last_login = $value['last_login'];

		$last_logout = $value['last_logout'];

			if ($last_logout != '0000-00-00 00:00:00')

			{



			   if (strtotime($last_logout) < strtotime($last_login)) {

			    $total_time = 'online';

			    } 

			    else{

			        $newtime = strtotime($last_logout)- strtotime($last_login);

			        $days = floor($newtime / 86400 % 7);

			        $hours = floor($newtime / 3600 % 24);

			        $mins = floor($newtime/ 60 % 60);

			        $secs = floor($newtime % 60);

			      if($days == '00'){



			      if ($hours == '00') {



			          if ($mins == '00') {

			          

			               $total_time = sprintf('%02d sec', $secs);



			              }

			          else

			              {

			           		$total_time = sprintf('%02d mins %02d sec', $mins, $secs);

			              }

			             }

			             else

			             {

			                 $total_time = sprintf('%02d hrs %02d mins %02d sec', $hours, $mins, $secs);

			             }

			         }

			    }



			   }

			   if ($value['notes'] == '') {

			   	$notesvalue = '   ';

			   }

			   else

			   {

			   	$notesvalue = $value['notes'];

			   }

			    $object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row,$value['user_to']);

	        	$object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row,$value['display_name']);

	        	$object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$value['agent_type']);

	        	$object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$notesvalue);

	        	$object->getActiveSheet()->setCellValueByColumnAndRow(4,$excel_row,$value['email']);

	        	$object->getActiveSheet()->setCellValueByColumnAndRow(5,$excel_row,$value['phone']);

	        	$object->getActiveSheet()->setCellValueByColumnAndRow(6,$excel_row,$value['game_category']);

	        	$object->getActiveSheet()->setCellValueByColumnAndRow(7,$excel_row,$last_login);

	        	$object->getActiveSheet()->setCellValueByColumnAndRow(8,$excel_row,$last_logout);

	        	$object->getActiveSheet()->setCellValueByColumnAndRow(9,$excel_row,$total_time);



				$excel_row++;

			}



		

		$object_writer = PHPExcel_IOFactory::createWriter($object,'Excel5');

		header('Content-Type: application/vnd.ms-excel');

		header('Content-Disposition: attachment;filename="History.xls"');



		$object_writer->save('php://output');



	}

}

	public function export_all_data()

	{

		$this->load->library('excel');

		$object = new PHPExcel();

		$object->setActiveSheetIndex(0);

		$table_columns = array('Sl No.', 'Customer Service Online',	'Agent Type', 'E-Mail', 'Ph.#', 'Game Category', 'Login Time', 'Logout Time',	'Total Time');

		$column = 0;

		foreach ($table_columns as $field) {

	 		$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);

			$column++;

		}

		$CI =& get_instance();

        $result = $CI->chatsocket->getAgentHist();

        $excel_row = 2;

		if ($result['status'] == true) 

		{

		$agent_data = $result['agent_data'];

		foreach ($agent_data as $key => $value) 

			{

				    $object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row,$value->id);

		        	$object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row,$value->name);

		        	$object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$value->agent_type);

		        	$object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$value->email);

		        	$object->getActiveSheet()->setCellValueByColumnAndRow(4,$excel_row,$value->phone);

		        	$object->getActiveSheet()->setCellValueByColumnAndRow(5,$excel_row,$value->game_category);

		        	$object->getActiveSheet()->setCellValueByColumnAndRow(6,$excel_row,$value->login_time);

		        	$object->getActiveSheet()->setCellValueByColumnAndRow(7,$excel_row,$value->logout_time);

		        	$object->getActiveSheet()->setCellValueByColumnAndRow(8,$excel_row,$value->total_time);

					$excel_row++;

			}

		$object_writer = PHPExcel_IOFactory::createWriter($object,'Excel5');

		header('Content-Type: application/vnd.ms-excel');

		header('Content-Disposition: attachment;filename="History.xls"');

		$object_writer->save('php://output');

		}

	}

	

	public function single_agent_history_export()

	{

				

		$this->load->library('excel');



		$object = new PHPExcel();

		$object->setActiveSheetIndex(0);

		$table_columns = array('Sl. No.', 'Login Time', 'Logout Time',	'Total Time');

		$column = 0;

		foreach ($table_columns as $field) {

	 		$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);

			$column++;

		}



		$CI =& get_instance();

		$agent_id = $_GET['agent_id'];

        $result = $CI->chatsocket->getSingleAgentHist($agent_id);

        $excel_row = 2;

		if ($result['status'] == true) 

		{

		$agent_data = $result['agent_data'];

		$i = 1;

		foreach ($agent_data as $key => $value)

		{ 

			    $object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row,$i);

	        	$object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row,$value->login_time);

	        	$object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$value->logout_time);

	        	$object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$value->total_time);	        	

				$excel_row++;

				$i++;

		}



		$object_writer = PHPExcel_IOFactory::createWriter($object,'Excel5');

		header('Content-Type: application/vnd.ms-excel');

		header('Content-Disposition: attachment;filename="History.xls"');



		$object_writer->save('php://output');



		}

	}

	

	public function security_password_data($id){

		echo '<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>

			</div>

			<div class="modal-body">

				<div class="row">

					<form action="#" id="security_id" name="security_id" method="POST" >

						<div class="box-body">

							<div class="form-group">

								<label for="name">Admin Password</label>

								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>

								<input type="hidden" id="userid" class="form-control" name="userid" value="'.$id.'" required>

							</div>

						</div>

						<div class="box-footer">

							<input class="btn btn-primary" type="submit" value="Submit" >

						</div>

					</form>

				</div>

			</div>			

			<div class="modal-footer">

				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		

			</div>';

	}

		public	function reset_all_agent_data(){

		$security_password 	= $_REQUEST['security_password'];

		$userid 			= $_REQUEST['userid'];

		$security_password_db = $this->General_model->view_all_data('admin_security_password','id','asc');

		if($security_password == $security_password_db[0]['password']){

			$CI =& get_instance();

			$CI->chatsocket->resetAllHistory();

			$data_value = '1';

		}

		else

		{

		$data_value = '<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>

			</div>

			<div class="modal-body">

				<div class="row">

					<form action="#" id="security_id" name="security_id" method="POST" >

						<div class="box-body">

							<div class="form-group">

								<label for="name">Admin Password</label>

								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>

								<input type="hidden" id="userid" class="form-control" name="userid" value="'.$userid.'" required>

							</div>

						</div>

						<div class="box-footer">

							<input class="btn btn-primary" type="submit" value="Submit" >

						</div>

					</form>

				</div>

			</div>			

			<div class="modal-footer">

				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		

			</div>';

		}

		echo $data_value;

		$result['data_value'] = $data_value;

		return $result;

	}



public function delete_agent_data(){

		$security_password 	= $_REQUEST['security_password'];

		$userid 			= $_REQUEST['userid'];

		$security_password_db = $this->General_model->view_all_data('admin_security_password','id','asc');

		if($security_password == $security_password_db[0]['password']){

			$CI =& get_instance();

			$id = $_REQUEST['userid'];

			$CI->chatsocket->deleteAgentData($id);

			$data_value = '1';

		}

		else

		{

		$data_value = '<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>

			</div>

			<div class="modal-body">

				<div class="row">

					<form action="#" id="security_id" name="security_id" method="POST" >

						<div class="box-body">

							<div class="form-group">

								<label for="name">Admin Password</label>

								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>

								<input type="hidden" id="userid" class="form-control" name="userid" value="'.$userid.'" required>

							</div>

						</div>

						<div class="box-footer">

							<input class="btn btn-primary" type="submit" value="Submit" >

						</div>

					</form>

				</div>

			</div>			

			<div class="modal-footer">

				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		

			</div>';

		}

		echo $data_value;

		$result['data_value'] = $data_value;

		return $result;

	}









	public function get_msg_conversation()

	{

		$this->check_user_page_access();



		$this->breadcrumbs->push('Home', 'admin/dashboard');



        $this->breadcrumbs->push('Customer Service Massege Box', 'admin/chat/customer_service_chat_box');



		

		$data['active']='admin';



		$data['page_name']='Customer Service Chat Box';

		$content=$this->load->view('admin/chat/customer_service_chat_box',$data,true);

		$this->render($content);



	}

	public function create_new_agent()

	{

		$this->check_user_page_access();



		$this->breadcrumbs->push('Home', 'admin/dashboard');



		$this->breadcrumbs->push('Customer Service Agent List', 'admin/chat');



        $this->breadcrumbs->push('Create Admin User', 'create_new_agent');



		

		$data['active']='customer_service_agent_list';



		$data['page_name']='Create Admin User';



		if (isset($_POST['submit'])) {

			$pwd = md5($_POST['pwd']);



			if ($_POST['agent_type'] == 'Admin') {

			$agent_category_no = '3';

			$roll_id = '1';

			}

			if($_POST['agent_type'] == 'Tech Specialist')

			{

			$agent_category_no = '2';

			}

			if($_POST['agent_type'] == 'Customer Service')

			{

			$agent_category_no = '1';

			}

			$roll_id = '2';

			$last_login = date("Y-m-d h:i:s");

			$data_insert_cre = array(

				'email' => $_POST['email'],

				'password' => $pwd,

				'role_id' => $roll_id,

				'last_login' => $last_login

            );



            $data_insert_detail = array(

            	'name' => $_POST['full_name'],

				'agent_type' => $_POST['agent_type'],

				'phone' => $_POST['phone_num'],

				'address' => $_POST['location'],

				'notes' => $_POST['notes'],

				'agent_category_no' => $agent_category_no,

				'game_category' => $_POST['game_category']

            );



       $this->General_model->insert_multi_data($data_insert_cre,$data_insert_detail);

		

          

			redirect(base_url().'admin/chat');

          }

		else

		{

			$content=$this->load->view('admin/chat/agent/create',$data,true);

			$this->render($content);

		}





	}



	public function logout()

	{

		$this->check_user_page_access();

		

		unset($_SESSION["admin_user_agent_id"]);

		redirect(base_url().'admin/chat');

		$this->render($content);



	}





    public function chat_box()

    {

        $this->check_user_page_access();

        $data['user_to']=$_GET['user_to'];

        $data['active']='chat_box';

        // $data['list']=$this->General_model->view_data('game',array('user_id'=>$this->session->userdata('user_id')));

        $content=$this->load->view('admin/chat/chat_box',$data,true);

        $this->render($content);

        // $this->load->view('admin_to_customer_chat_box');

    }

}



