<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH . '../application/controllers/admin/My_Controller.php';
class Chatads extends My_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('My_model');
        $this->load->model('General_model');
        $this->load->helper('file');
        $this->target_upload_commercial_dir     = BASEPATH . '../upload/ads_commercial/';
        $this->target_add_banner_dir            = BASEPATH . '../upload/ads_banner/';
        $this->target_add_sponsered_by_logo_dir = BASEPATH . '../upload/ads_sponsered_by_logo/';
        $this->target_upload_ten_sec_intro_dir  = BASEPATH . '../upload/ads_ten_sec_intro/';
    }
    public function index() {
        $this->check_user_page_access();
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('CHAT Ads', 'admin/chatads/');
        $data['active']    = 'chat_ads';
        $data['page_name'] = 'CHAT Ads';
        $data['ads_list']  = $this->My_model->chatads_data();
        $content           = $this->load->view('admin/chatads/chatadslist', $data, true);
        $this->render($content);
    }
    public function pvpads() {
        $this->check_user_page_access();
        if ($_GET['create'] == 'pvp') {
            $this->breadcrumbs->push('Home', 'admin/dashboard');
            $this->breadcrumbs->push('Ads List', 'admin/chatads');
            $data['active']    = 'chat_ads';
            $data['page_name'] = 'Create PvP Ads';
            $ads_cat           = 'pvp';
            $this->breadcrumbs->push('Create PvP Ads', 'admin/chatads/create_pvpads');
        } else {
            $this->breadcrumbs->push('Home', 'admin/dashboard');
            $this->breadcrumbs->push('Ads List', 'admin/chatads');
            $data['active']    = 'chat_ads';
            $data['page_name'] = 'Create Cust. Service Ads';
            $this->breadcrumbs->push('Create Cust. Service Ads', 'admin/chatads/create_pvpads');
            $ads_cat = 'custads';
        }
        $data['ads_cat'] = $ads_cat;
        $content         = $this->load->view('admin/chatads/pvpads/create', $data, true);
        $this->render($content);
    }
    public function create_pvpads() {
        $this->check_user_page_access();
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Ads List', 'admin/chatads');
        $this->breadcrumbs->push('Create Pvpads', 'admin/chatads/create_pvpads');
        $data['active']    = 'chat_ads';
        $data['page_name'] = 'Create Pvpads';
        $ads_banner_img    = '';
        $youtube_embeded_link = $this->General_model->getYoutubeEmebedUrl($_POST['youtube_link'],1)['url'];
        $autoskipp_time = ($_POST['autoskipp_time'] != '')? $_POST['autoskipp_time'] : 10;
        if (isset($_FILES["upload_banner"]["name"]) && $_FILES["upload_banner"]["name"] != '') {
            $ads_banner_img = time() . basename($_FILES["upload_banner"]["name"]);
            $ext            = end((explode(".", $ads_banner_img)));
            if ($ext == 'gif' || $ext == 'jpeg' || $ext == 'png' || $ext == 'jpg') {
                $target_file = $this->target_add_banner_dir . $ads_banner_img;
                move_uploaded_file($_FILES["upload_banner"]["tmp_name"], $target_file);
            } else {
                $this->session->set_flashdata('msg', 'This is not a image file');
            }
        }
        $upload_sponsered_by_logo = '';
        if (isset($_FILES["upload_sponsered_by_logo"]["name"]) && $_FILES["upload_sponsered_by_logo"]["name"] != '') {
            $upload_sponsered_by_logo = time() . basename($_FILES["upload_sponsered_by_logo"]["name"]);
            $ext_a                    = end((explode(".", $upload_sponsered_by_logo)));
            if ($ext_a == 'gif' || $ext_a == 'jpeg' || $ext_a == 'png' || $ext_a == 'jpg') {
                $target_file_log = $this->target_add_sponsered_by_logo_dir . $upload_sponsered_by_logo;
                move_uploaded_file($_FILES["upload_sponsered_by_logo"]["tmp_name"], $target_file_log);
            } else {
                $this->session->set_flashdata('msg', 'This is not a image file');
            }
        }
        $upload_ten_sec_intro = '';
        if (isset($_FILES["upload_ten_sec_intro"]["name"]) && $_FILES["upload_ten_sec_intro"]["name"] != '') {
            $upload_ten_sec_intro = time() . basename($_FILES["upload_ten_sec_intro"]["name"]);
            $ext_vid              = end((explode(".", $upload_ten_sec_intro)));
            if ($ext_vid == 'wmv' || $ext_vid == 'mp4' || $ext_vid == 'avi' || $ext_vid == 'mov' || $ext_vid == '3gp') {
                $target_upload_ten_sec_intro = $this->target_upload_ten_sec_intro_dir . $upload_ten_sec_intro;
                move_uploaded_file($_FILES["upload_ten_sec_intro"]["tmp_name"], $target_upload_ten_sec_intro);
            } else {
                $this->session->set_flashdata('msg', 'This is not a Video file');
            }
        }
        $upload_commercial = '';
        if (isset($_FILES["upload_commercial"]["name"]) && $_FILES["upload_commercial"]["name"] != '') {
            $upload_commercial = time() . basename($_FILES["upload_commercial"]["name"]);
            $ext_ads_vid       = end((explode(".", $upload_commercial)));
            if ($ext_ads_vid == 'wmv' || $ext_ads_vid == 'mp4' || $ext_ads_vid == 'avi' || $ext_ads_vid == 'mov' || $ext_ads_vid == '3gp') {
                $target_upload_commercial = $this->target_upload_commercial_dir . $upload_commercial;
                move_uploaded_file($_FILES["upload_commercial"]["tmp_name"], $target_upload_commercial);
            } else {
                $this->session->set_flashdata('msg', 'This is not a Video file');
            }
        }
        if (isset($_POST['submit'])) {
            $data_insert = array(
                'youtube_link_title' => $_POST['youtube_link_title'],
                'youtube_link' => $youtube_embeded_link,
                'upload_banner_title' => $_POST['upload_banner_title'],
                'upload_banner' => $ads_banner_img,
                'banner_link_title' => $_POST['banner_link_title'],
                'banner_link' => $_POST['banner_link'],
                'sponsered_by_logo_title' => $_POST['sponsered_by_logo_title'],
                'sponsered_by_logo_link' => $_POST['sponsered_by_logo_link'],
                'upload_sponsered_by_logo' => $upload_sponsered_by_logo,
                'ten_sec_intro_title' => $_POST['ten_sec_intro_title'],
                'upload_ten_sec_intro' => $upload_ten_sec_intro,
                'commercial_title' => $_POST['commercial_title'],
                'upload_commercial' => $upload_commercial,
                'ads_category' => $_POST['ads_category'],
                'autoskipp_time' => $autoskipp_time,
                'created_date' => date('Y-m-d H:i:s')
            );
            $this->General_model->insert_data('chatads', $data_insert);
            redirect(base_url() . 'admin/chatads');
        } else {
            $content = $this->load->view('admin/chatads/chatadslist', $data, true);
            $this->render($content);
        }
    }
    public function edit_pvpads() {
        $this->check_user_page_access();
        $ads_banner_img = '';
        $autoskipp_time = ($_POST['autoskipp_time'] != '')? $_POST['autoskipp_time'] : 10;
        if (isset($_FILES["upload_banner"]["name"])) {
            if ($_FILES["upload_banner"]["name"] != '') {
                $ads_banner_img = time() . basename($_FILES["upload_banner"]["name"]);
                $ext_a          = end((explode(".", $ads_banner_img)));
                if ($ext_a == 'gif' || $ext_a == 'jpeg' || $ext_a == 'png' || $ext_a == 'jpg') {
                    $target_file = $this->target_add_banner_dir . $ads_banner_img;
                    if (move_uploaded_file($_FILES["upload_banner"]["tmp_name"], $target_file)) {
                        if (file_exists($this->target_add_banner_dir . $_POST['upload_banner_image'])) {
                            unlink($this->target_add_banner_dir . $_POST['upload_banner_image']);
                        }
                    }
                } else {
                    $this->session->set_flashdata('msg', 'This is not a image file');
                    // redirect(base_url().'admin/chatads/chatadslist');
                }
            } else {
                $ads_banner_img = $_POST['upload_banner_image'];
            }
        }
        if (isset($_FILES["upload_sponsered_by_logo"]["name"])) {
            if ($_FILES["upload_sponsered_by_logo"]["name"] != '') {
                $upload_sponsered_by_logo = time() . basename($_FILES["upload_sponsered_by_logo"]["name"]);
                $ext                      = end((explode(".", $upload_sponsered_by_logo)));
                if ($ext == 'gif' || $ext == 'jpeg' || $ext == 'png' || $ext == 'jpg') {
                    $target_file_log = $this->target_add_sponsered_by_logo_dir . $upload_sponsered_by_logo;
                    if (move_uploaded_file($_FILES["upload_sponsered_by_logo"]["tmp_name"], $target_file_log)) {
                        if (file_exists($this->target_add_sponsered_by_logo_dir . $_POST['upload_sponsered_by_logo_image'])) {
                            unlink($this->target_add_sponsered_by_logo_dir . $_POST['upload_sponsered_by_logo_image']);
                        }
                    }
                } else {
                    $this->session->set_flashdata('msg', 'This is not a image file');
                }
            } else {
                $upload_sponsered_by_logo = $_POST['upload_sponsered_by_logo_image'];
            }
        }
        $upload_ten_sec_intro = '';
        if (isset($_FILES["upload_ten_sec_intro"]["name"])) {
            if ($_FILES["upload_ten_sec_intro"]["name"] != '') {
                $upload_ten_sec_intro = time() . basename($_FILES["upload_ten_sec_intro"]["name"]);
                $ext_vid              = end((explode(".", $upload_ten_sec_intro)));
                if ($ext_vid == 'wmv' || $ext_vid == 'mp4' || $ext_vid == 'avi' || $ext_vid == 'mov' || $ext_vid == '3gp') {
                    $target_upload_ten_sec_intro = $this->target_upload_ten_sec_intro_dir . $upload_ten_sec_intro;
                    if (move_uploaded_file($_FILES["upload_ten_sec_intro"]["tmp_name"], $target_upload_ten_sec_intro)) {
                        if (file_exists($this->target_upload_ten_sec_intro_dir . $_POST['upload_ten_sec_intro_vid'])) {
                            unlink($this->target_upload_ten_sec_intro_dir . $_POST['upload_ten_sec_intro_vid']);
                        }
                    }
                } else {
                    $this->session->set_flashdata('msg', 'This is not a Video file');
                }
            } else {
                $upload_ten_sec_intro = $_POST['upload_ten_sec_intro_vid'];
            }
        }
        $upload_commercial = '';
        if (isset($_FILES["upload_commercial"]["name"])) {
            if ($_FILES["upload_commercial"]["name"] != '') {
                $upload_commercial = time() . basename($_FILES["upload_commercial"]["name"]);
                $ext_ads_vid       = end((explode(".", $upload_commercial)));
                if ($ext_ads_vid == 'wmv' || $ext_ads_vid == 'mp4' || $ext_ads_vid == 'avi' || $ext_ads_vid == 'mov' || $ext_ads_vid == '3gp') {
                    $target_upload_commercial = $this->target_upload_commercial_dir . $upload_commercial;
                    if (move_uploaded_file($_FILES["upload_commercial"]["tmp_name"], $target_upload_commercial)) {
                        if (file_exists($this->target_upload_commercial_dir . $_POST['upload_commercial_vid'])) {
                            unlink($this->target_upload_commercial_dir . $_POST['upload_commercial_vid']);
                        }
                    }
                } else {
                    $this->session->set_flashdata('msg', 'This is not a Video file');
                }
            } else {
                $upload_commercial = $_POST['upload_commercial_vid'];
            }
        }
        if (isset($_POST['submit'])) {
        	$youtube_embeded_link = $this->General_model->getYoutubeEmebedUrl($_POST['youtube_link'],1)['url'];
            $id          = $_POST['id'];
            $data_update = array(
                'youtube_link_title' => $_POST['youtube_link_title'],
                'youtube_link' => $youtube_embeded_link,
                'upload_banner_title' => $_POST['upload_banner_title'],
                'upload_banner' => $ads_banner_img,
                'banner_link_title' => $_POST['banner_link_title'],
                'banner_link' => $_POST['banner_link'],
                'sponsered_by_logo_title' => $_POST['sponsered_by_logo_title'],
                'sponsered_by_logo_link' => $_POST['sponsered_by_logo_link'],
                'upload_sponsered_by_logo' => $upload_sponsered_by_logo,
                'ten_sec_intro_title' => $_POST['ten_sec_intro_title'],
                'upload_ten_sec_intro' => $upload_ten_sec_intro,
                'commercial_title' => $_POST['commercial_title'],
                'upload_commercial' => $upload_commercial,
                'ads_category' => $_POST['ads_category'],
                'autoskipp_time' => $autoskipp_time,
                'created_date' => date('Y-m-d H:i:s')
            );
            $this->General_model->update_data('chatads', $data_update, array(
                'id' => $id
            ));
            redirect(base_url() . 'admin/chatads');
        } else {
            $id                     = $_GET['id'];
            $data['ads_single_row'] = $this->My_model->chatads_single_data($id);
            $ads_cate               = 'Cust. Service Ads';
            if ($data['ads_single_row']->ads_category == 'pvp') {
                $ads_cate = 'PvP Ads';
            }
            $this->breadcrumbs->push('Home', 'admin/dashboard');
            $this->breadcrumbs->push('Ads List', 'admin/chatads');
            $this->breadcrumbs->push('Edit ' . $ads_cate, 'admin/chatads/edit_pvpads');
            $data['active']    = 'chat_ads';
            $data['page_name'] = 'Edit ' . $ads_cate;
            $content           = $this->load->view('admin/chatads/pvpads/edit', $data, true);
            $this->render($content);
        }
    }
    public function delete_ads_data() {
        $security_password    = $_REQUEST['security_password'];
        $userid               = $_REQUEST['userid'];
        $security_password_db = $this->General_model->view_all_data('admin_security_password', 'id', 'asc');
        if ($security_password == $security_password_db[0]['password']) {
            $id         = $_REQUEST['userid'];
            $data_value = '1';
            $this->db->select('*');
            $this->db->from('chatads');
            $this->db->where('id', $id);
            $query    = $this->db->get();
            $ads_data = $query->result();
            unlink($this->target_upload_commercial_dir . $ads_data['0']->upload_commercial);
            unlink($this->target_add_banner_dir . $ads_data['0']->upload_banner);
            unlink($this->target_add_sponsered_by_logo_dir . $ads_data['0']->upload_sponsered_by_logo);
            unlink($this->target_upload_ten_sec_intro_dir . $ads_data['0']->upload_ten_sec_intro);
            // unlink("uploads/".$group_picture);
            $this->db->delete('chatads', array(
                'id' => $id
            ));
        } else {
            $data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>

			</div>

			<div class="modal-body">

				<div class="row">

					<form action="#" id="security_id" name="security_id" method="POST" >

						<div class="box-body">

							<div class="form-group">

								<label for="name">Admin Password</label>

								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>

								<input type="hidden" id="userid" class="form-control" name="userid" value="' . $userid . '" required>

							</div>

						</div>

						<div class="box-footer">

							<input class="btn btn-primary" type="submit" value="Submit" >

						</div>

					</form>

				</div>

			</div>			

			<div class="modal-footer">

				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		

			</div>';
        }
        echo $data_value;
        $result['data_value'] = $data_value;
        return $result;
    }
}
?>

