<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include BASEPATH.'../application/controllers/admin/My_Controller.php';

class Customer_service extends My_Controller 

{

  	function __construct()

    {

	parent::__construct();

    $this->target_dir=BASEPATH.'../upload/banner/';

	}



	public function index()

	{

		$this->check_user_page_access();

		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('Customer Service List', 'admin/customer_service_page');


		$data['active']='customer_service_page';

		$data['page_name']='Customer Service';
		

		$data['list']=$this->General_model->view_all_data('footer_customer_service','id','asc');

		$content=$this->load->view('admin/customer_service/customer_service_page.tpl.php',$data,true);

		$this->render($content);

	}

	//edit 

	public function change_to_deactive() {

		$data=array(

			'status'=>'deactive'

		 );

		$this->General_model->update_data('footer_customer_service',$data,array('id'=>$_POST['cs_status_id']));
	}

	public function change_to_active() {

		$data=array(

			'status'=>'active'

		 );

		$this->General_model->update_data('footer_customer_service',$data,array('id'=>$_POST['csstatus_id']));
	}
	
}

