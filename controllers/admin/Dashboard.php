<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Dashboard extends My_Controller 
{
	function __construct() {
		parent::__construct();
		$this->load->model('Admin_user_model');
	} 
	 
	public function index() {
		$this->check_user_page_access();
		$this->render(null);
	}
}
