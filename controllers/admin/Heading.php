<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include BASEPATH.'../application/controllers/admin/My_Controller.php';

class Heading extends My_Controller 

{

  	function __construct()

    {

	parent::__construct();

    $this->target_dir=BASEPATH.'../upload/banner/';

	}



	public function index()

	{

		$this->check_user_page_access();

		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('Heading List', 'admin/heading');



		$data['active']='heading_list';

		$data['page_name']='Heading List';

		

		$data['list']=$this->General_model->view_all_data('heading','id','asc');

		$content=$this->load->view('admin/heading/heading_list.tpl.php',$data,true);

		$this->render($content);

	}

	//load edit page

	public function headingEdit($id='-1')

	{

		$this->check_user_page_access();

		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('Heading List', 'admin/heading');

		$this->breadcrumbs->push('Edit Heading', 'admin/heading/headingEdit');



		$data['active']='heading_edit';

		$data['page_name']='Edit Page';



		$data['heading_data']=$this->General_model->view_data('heading',array('id'=>$id));

		$content=$this->load->view('admin/heading/heading_edit.tpl.php',$data,true);

		$this->render($content);

	}

	//edit 

	public function headingUpdate()

	{

		$this->check_user_page_access();

		//$data['active']='game_edit';
		//print_r($_POST);exit;
		$data=array(

			'heading1'=>$this->input->post('name', TRUE),

			'heading2'=>$this->input->post('name1', TRUE),

			'heading3'=>$this->input->post('name2', TRUE),

			'heading4'=>$this->input->post('name3', TRUE)
		 );

		 $r=$this->General_model->update_data('heading',$data,array('id'=>$this->input->post('id', TRUE)));

		if($r)

		{

			$this->session->set_userdata('message_succ', 'Update successfull');	

			redirect(base_url().'admin/heading/headingEdit/'.$this->input->post('id', TRUE));

		}

		else

		{

			$this->session->set_userdata('message_err', 'Failure to update');	

			redirect(base_url().'admin/heading/headingEdit/'.$this->input->post('id', TRUE));

		}	

	}
	
}

