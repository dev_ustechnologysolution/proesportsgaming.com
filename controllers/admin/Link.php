<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include BASEPATH.'../application/controllers/admin/My_Controller.php';

class Link extends My_Controller 

{

  	function __construct()

    {

	parent::__construct();

    $this->target_dir=BASEPATH.'../upload/banner/';

	}



	public function index()

	{

		$this->check_user_page_access();

		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('Link List', 'admin/link');



		$data['active']='link_list';

		$data['page_name']='Link List';

		

		$data['list']=$this->General_model->view_all_data('footer_link','id','asc');

		$content=$this->load->view('admin/link/link_list.tpl.php',$data,true);

		$this->render($content);

	}

	//load edit page

	public function linkEdit($id='-1')

	{

		$this->check_user_page_access();

		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('Link List', 'admin/link');

		$this->breadcrumbs->push('Edit Link', 'admin/link/linkEdit');



		$data['active']='link_edit';

		$data['page_name']='Edit Page';



		$data['link_data']=$this->General_model->view_data('footer_link',array('id'=>$id));

		$content=$this->load->view('admin/link/link_edit.tpl.php',$data,true);

		$this->render($content);

	}

	//edit 

	public function linkUpdate()

	{

		$this->check_user_page_access();

		//$data['active']='game_edit';

		$data=array(

			'class'=>$this->input->post('footer_class', TRUE),

			'link'=>$this->input->post('footer_link', TRUE)
		 );

		 $r=$this->General_model->update_data('footer_link',$data,array('id'=>$this->input->post('id', TRUE)));

		if($r)

		{

			$this->session->set_userdata('message_succ', 'Update successfull');	

			redirect(base_url().'admin/link/linkEdit/'.$this->input->post('id', TRUE));

		}

		else

		{

			$this->session->set_userdata('message_err', 'Failure to update');	

			redirect(base_url().'admin/link/linkEdit/'.$this->input->post('id', TRUE));

		}	

	}
	
}

