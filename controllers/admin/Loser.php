<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Loser extends My_Controller 
{
	function __construct()
    {
	parent::__construct();
	$this->load->model('User_model');
    $this->target_dir=BASEPATH.'../upload/usr/';
	}

	public function index(){
		$this->check_user_page_access();
		// add breadcrumbs
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Loser List', 'loser_list');
		$data['active']='loser_list';
		$data['page_name']='Loser List';

		$data['list']=$this->User_model->loser_result();

/*		 echo "<pre>";
		print_r($data['list']);exit;*/
		$content=$this->load->view('admin/result/loser_list.tpl.php',$data,true);
		$this->render($content);
	}
}