<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';

class Membership extends My_Controller 
{

  	function __construct() {
		parent::__construct();
	    $this->target_dir=BASEPATH.'../upload/membership/';
	}
	public function index(){

		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Pro Esports Membership List', 'admin/membership');

		$data['active']='pro_esports_membership';
		$data['page_name']='Pro Esports Membership List';

		$data['list']=$this->General_model->view_all_data('membership','id','desc');
		$condition = array('payment_status' => 'Active');
    	$data['plyer_list'] = $this->General_model->view_data('player_membership_subscriptions',$condition);
		$content=$this->load->view('admin/membership/membership_list.tpl.php',$data,true);

		$this->render($content);
	}
	public function addMembership() {
		$this->check_user_page_access();

		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Pro Esports Membership List', 'admin/membership');
        $this->breadcrumbs->push('Add Membership', 'admin/membership/addMembership');

		$data['active']='pro_esports_membership';
		$data['page_name']='Membership Add';

		$content=$this->load->view('admin/membership/membership_create.tpl.php',$data,true);
		$this->render($content);
	}
	public function create(){
		$this->check_user_page_access();
		
		if(isset($_FILES["image"]["name"]) && $_FILES["image"]["name"]!='')
		{
			$img = time().basename($_FILES["image"]["name"]);
			$ext = end((explode(".", $img)));
			
			if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg')
			{
				$target_file = $this->target_dir.$img;
		        move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
		    }
		    else
		    {
		    	$this->session->set_flashdata('msg', 'This is not a image file');	
				redirect(base_url().'admin/membership/bannerAdd');
		    }

		}
		$data = array(
			'title'=>$this->input->post('title',TRUE),
			'status'=>'1',
			'description' => $this->input->post('description',TRUE),
			'amount' => $this->input->post('amount',TRUE),
			'fees' => $this->input->post('fees',TRUE),
			'image' => $img,
			'terms_condition' => $this->input->post('terms_condition',TRUE),
			'start_date' => date('Y-m-d h:i'),
			'expire_date' => date('Y-m-d h:i'),
			'time_duration' => $this->input->post('time_duration',TRUE)
		 );
		
		$r=$this->General_model->insert_data('membership',$data);

		if($r)
		{
			$this->session->set_userdata('message_succ', 'Successfull Added');
			redirect(base_url().'admin/membership');
		} else {
			$this->session->set_userdata('message_err', 'Failure to Create');
			redirect(base_url().'admin/membership');
		}
	}
	public function membershipEdit($id='-1'){
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Pro Esports Membership List', 'admin/membership');
		$this->breadcrumbs->push('Edit Pro Esports Membership', 'admin/membership/membershipEdit');

		$data['active']='pro_esports_membership';
		$data['page_name']='Edit Pro Esports Membership';

		$data['membership_data']=$this->General_model->view_data('membership',array('id'=>$id));

		$content=$this->load->view('admin/membership/membership_edit.tpl.php',$data,true);
		$this->render($content);
	}
	public function membershipUpdate(){
		$this->check_user_page_access();		
		$trim = strip_tags($_REQUEST['editor1']);
    	$trim=str_replace([" ","\n","\t","&ndash;","&rsquo;","&#39;","&quot;","&nbsp;"], '', $trim);

    	$totalCharacter = strlen(trim($trim));
    	
		if($this->input->post('type',TRUE) == 'static' && $totalCharacter == 0 ){
			$this->session->set_userdata('message_err', 'Please Enter Content');	

			redirect(base_url().'admin/membership/membershipEdit/'.$this->input->post('id', TRUE));
		}

		$data['active']='pro_esports_membership';
		if((isset($_FILES["image"]["name"])) && ($_FILES["image"]["name"] != ''))
		{

			$img=time().basename($_FILES["image"]["name"]);
			$ext = end((explode(".", $img)));
		
			if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg')
			{
				$target_file = $this->target_dir.$img;
		        if(move_uploaded_file($_FILES["image"]["tmp_name"], $target_file))
				{
					if(file_exists($this->target_dir.$this->input->post('old_image', TRUE)))
					{
						unlink($this->target_dir.$this->input->post('old_image', TRUE));
					}
				}
			}

		} else {
			$img = $this->input->post('image', TRUE);;
		}

		$data=array(
			'title'=>$this->input->post('title',TRUE),
			'status'=>'1',
			'description' => $this->input->post('description',TRUE),
			'amount' => $this->input->post('amount',TRUE),
			'fees' => $this->input->post('fees',TRUE),
			'terms_condition' => $this->input->post('terms_condition',TRUE),
			'start_date' => date('Y-m-d h:i'),
			'expire_date' => date('Y-m-d h:i'),
			'time_duration' => $this->input->post('time_duration',TRUE),
			'image' => $img
		 );

		$r=$this->General_model->update_data('membership',$data,array('id'=>$this->input->post('id', TRUE)));
		if($r)
		{
			$this->session->set_userdata('message_succ', 'Update successfull');
			redirect(base_url().'admin/membership/membershipEdit/'.$this->input->post('id', TRUE));
		} else
		{
			$this->session->set_userdata('message_err', 'Failure to update');
			redirect(base_url().'admin/membership/membershipEdit/'.$this->input->post('id', TRUE));
		}	
	}
	public function delete_data($id){
		$result = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="security_password_id" name="security_password_id" method="POST" >
						<div class="box-body">
							<div class="form-group">
								<label for="name">Admin Password</label>
								<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
								<input type="hidden" id="membership_id" class="form-control" name="membership_id" value="'.$id.'" required>
							</div>
						</div>
						<div class="box-footer">
							<input class="btn btn-primary" type="submit" value="Submit" >
							<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>	
						</div>
					</form>
				</div>
			</div>';
			echo $result;
	}
	public function delete(){
		
		$security_password 	= $_REQUEST['security_password'];
		$membership_id 			= $_REQUEST['membership_id'];
			
		$security_password_db = $this->General_model->view_all_data('admin_security_password','id','asc');
		
		if($security_password == $security_password_db[0]['password'])
		{
			$r=$this->General_model->delete_data('membership',array('id'=>$membership_id));

			if($r) 
			{
				$this->session->set_userdata('message_succ', 'Delete Successfull');	
				$data_value = true;
			} else {
				$this->session->set_userdata('message_err', 'Failure to delete');	
				$data_value = false;
			}
		} else {
			$data_value['pass_err'] = 'Password Error';
			$data_value = '<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Security Password</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<form action="#" id="security_password_id" name="security_password_id" method="POST" >
						<div class="form-group" style="margin: 0 12px 15px;"><label style="color:red;">'.$data_value['pass_err'].'</label></div>
							<div class="box-body">
								<div class="form-group">
									<label for="name">Admin Password</label>
									<input type="password" id="security_password" class="form-control" name="security_password" value="" data-validation="required" required>
									<input type="hidden" id="membership_id" class="form-control" name="membership_id" value="'.$membership_id.'" required>
								</div>
							</div>
							<div class="box-footer">
								<input class="btn btn-primary" type="submit" value="Submit" >
								<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>	
							</div>
						</form>
					</div>
				</div>';
				
			}
			echo $data_value; die;
	}
	public function getMembershipPlayer(){
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Players Membership List', 'admin/membership');

		$data['active']='players_membership';
		$data['page_name']='Players Membership List';		

		$condition = array('payment_status' => 'Active');
    	$data['list'] = $this->General_model->view_data('player_membership_subscriptions',$condition);		
		$content=$this->load->view('admin/membership/player_membership_list.tpl.php',$data,true);

		$this->render($content);
	}
	public function membershipHistory(){
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Players Membership List', 'admin/membership');
        $this->breadcrumbs->push('Players History List', 'admin/membership/membershipHistory');

		$data['active']='players_membership';
		$data['page_name']='Players History List';		

		$condition = array('user_id' => $_GET['user_id']);
    	$data['list'] = $this->General_model->view_data('player_membership_subscriptions',$condition);		
		$content=$this->load->view('admin/membership/player_history_list.tpl.php',$data,true);

		$this->render($content);
	}
}

