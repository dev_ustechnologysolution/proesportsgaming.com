<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Orders extends My_Controller 
{
  	function __construct()
    {
		parent::__construct();
		$this->load->model('Product_model');
    }
	public function index() {
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('New Orders', 'admin/orders');
		$data['active']='orders_list';
		$data['page_name']='Orders List';
		$q = $this->db->query('SELECT ot.sub_total,ot.grand_total,ot.tracking_id,ot.id,ot.sold_by,ot.user_id,ot.shipping_address_id,ud.name,ud.account_no,ud.number FROM `orders_tbl` as ot inner JOIN user_detail as ud on ud.user_id = ot.user_id where ot.order_status = "placed"');
		$data['list'] = $q->result();		
		$content=$this->load->view('admin/orders/orders_list.tpl.php',$data,true);
		$this->render($content);
	}
	public function orderItemDetail(){
		$sender_arr = $this->General_model->view_single_row('orders_items_tbl','id',$_GET['id']);
		
		$product_att = explode(',', $sender_arr['product_attributes']);

		$data_value = '<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Product Details</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="#" id="change_product_order" name="change_product_order" method="POST">
						<div class="box-body">
							<div class="form-group col-md-4">
								<div>				
								<img width="250" height="250" src="'.base_url().'upload/products/'.$sender_arr['product_image'].'">
								</div>
							</div>
							<div class="form-group col-md-8">
								<h3 style="margin-top:0px;">'.$sender_arr['product_name'].'</h3>	
								<h3>$'.$sender_arr['amount'].'</h3>';
								if(count($product_att) > 0){
									$data_value .='<h3>Product Attributes</h3>';
								foreach ($product_att as $key => $value) {
									$att = explode('_', $value);
									$data_value .='<div><h4>'.$att[0].' : '.$att[1].'</h4></div>';
								}
							}
								
							$data_value .='</div>
						</div>
						
					</form>
				</div>
			</div>			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>		
			</div>';
		echo $data_value;

	}
	public function savedata()
    {
        $title = $this->input->post('title',true);
       	if($_REQUEST['type'] == 1){
       		$fields = array(
            'tracking_id' => str_replace('<i class="fa fa-pencil fa-fw"></i>','',$title),
          );
       	} else {
       		$fields = array(
            'order_status' => 'completed',
          );
       	}
        
       $this->db->where('id',$_REQUEST['id'])->update('orders_tbl',$fields);
       if($_REQUEST['type'] == 1){
	        echo "Successfully saved";
	    } else {
	    	redirect(base_url().'admin/orders');
	    }
    }
    public function orderdetails()
    {
    	$order_id = $this->input->get('id');
    	$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Order Details', 'admin/orders');
		$data['active']='orders_list';
		$data['page_name']='Orders Details';
		$data['list'] = $this->General_model->view_data('orders_items_tbl',array('order_id' => $order_id));		
		$content=$this->load->view('admin/orders/orders_details.tpl.php',$data,true);
		$this->render($content);
    }
    public function completedOrders(){
    	$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('New Orders', 'admin/orders');
		$data['active']='completedOrders';
		$data['page_name']='Completed Orders List';
		$q = $this->db->query('SELECT ot.sub_total,ot.grand_total,ot.tracking_id,ot.id,ot.sold_by,ot.user_id,ot.shipping_address_id,ud.name,ud.account_no,ud.number FROM `orders_tbl` as ot inner JOIN user_detail as ud on ud.user_id = ot.user_id where ot.order_status = "completed"');
		$data['list'] = $q->result();		
		$content=$this->load->view('admin/orders/complete_orders_list.tpl.php',$data,true);
		$this->render($content);
    }
}

