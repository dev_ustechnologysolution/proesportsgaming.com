<?php



defined('BASEPATH') OR exit('No direct script access allowed');



include BASEPATH.'../application/controllers/admin/My_Controller.php';



class Pvpchat extends My_Controller 



{



  	function __construct()



    {

		parent::__construct();

	}



	public function index()

	{

		$this->check_user_page_access();

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('PVP Chat', 'admin/pvpchat/');

		$data['active']='pvp_chat';

		$data['page_name']='PVP Chat';

		$content=$this->load->view('admin/pvpchat/pvp_chat_list',$data,true);

		$this->render($content);

	}



	public function pvpgroup_adminside_chat_box()

    {

        $this->check_user_page_access();

        $data_ajska['accepterid']=$_GET['accepterid'];

        $data_ajska['challengerid']=$_GET['challengerid'];

        $data_ajska['groupdata']=$_GET['groupdata'];

        $data_ajska['agentonline']=$_GET['agentonline'];

        $data_ajska['status']=$_GET['status'];

        $data_ajska['userto']=$_GET['userto'];

        $data_ajska['user']=$_GET['user'];

        $data_ajska['ci']=$_GET['ci'];

        $data_ajska['gamename']=$_GET['gamename'];

        $data['active']='group_chat_box';

        $content=$this->load->view('admin/pvpchat/group_chat_box',$data,true);

        $this->render($content);

        // $this->load->view('pvpgroup_adminside_chat_box');

    }

}

?>

