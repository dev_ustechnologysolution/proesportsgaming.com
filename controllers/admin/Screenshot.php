<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';
class Screenshot extends My_Controller 
{
  function __construct()
    {
	parent::__construct();
	$this->load->model('User_model');
	$this->target_dir=BASEPATH.'../upload/screenshot/';
	}
	public function index() {

		$this->check_user_page_access();
		// add breadcrumbs
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Screenshot List', 'admin/screenshot');

        $data['active']='screenshot_list';
		$data['page_name']='Screenshot List Page';

		$data['list']=$this->General_model->view_all_data('screenshot','id','desc');
		$content=$this->load->view('admin/screenshot/screenshot_list.tpl.php',$data,true);
		$this->render($content);
	}

	public function addScreenshot() {

		$data['active']='screenshot';
		$data['page_name']='Screenshot Add Page';
		$content=$this->load->view('admin/screenshot/screenshot.tpl.php',$data,true);
		$this->render($content);
	}
	

	public function insertScreenshot()
	{

		$img='';

		if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name']))
		{
			
			$img=time().basename($_FILES['image']['name']);
			$ext = end((explode(".", $img)));

			
			if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg')
			{

				$target_file=$this->target_dir.$img;
				move_uploaded_file($_FILES['image']['tmp_name'], $target_file);
			}
			else
			{
				$this->session->set_flashdata('msg', 'This is not a image file');	
				redirect(base_url().'admin/screenshot/addScreenshot');
				
			}
		}

		 $data=array(
			'game_name'=>$this->input->post('name', TRUE),
			'description'=>$this->input->post('description', TRUE),
			'image'=>$img
			);	

		if($this->General_model->insert_data('screenshot',$data))
		{
			$this->session->set_userdata('message_succ', 'Successfully added');	
			redirect(base_url().'admin/Screenshot');
		}
		else{
			$this->session->set_userdata('message_err', 'Failure to add');	
			redirect(base_url().'admin/Screenshot/addScreenshot');
		}
		
	}	

	//load edit page
	public function screenshotEdit($id='-1')
	{
		$this->check_user_page_access();
		// add breadcrumbs
		$this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Screenshot List', 'admin/screenshot');
		$this->breadcrumbs->push('Edit Screenshot', 'admin/screenshot/screenshotEdit');

		$data['active']='screenshot_edit';
		$data['page_name']='Edit Page';

		$data['edit_data']=$this->General_model->view_data('screenshot',array('id'=>$id));
		$content=$this->load->view('admin/screenshot/screenshot_edit.tpl.php',$data,true);
		$this->render($content);
	}

	public function screenshotUpdate() {

		$img=$this->input->post('old_image', TRUE);

		if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name']))
		{
			
			$img=time().basename($_FILES['image']['name']);
			$ext = end((explode(".", $img)));

			
			if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg')
			{

				$target_file=$this->target_dir.$img;
				if(move_uploaded_file($_FILES["image"]["tmp_name"], $target_file))
				{
					if(file_exists($this->target_dir.$this->input->post('old_image', TRUE)))
					{
						unlink($this->target_dir.$this->input->post('old_image', TRUE));
					}
				}
				
			}
			else
			{
				$this->session->set_flashdata('msg', 'This is not a image file');	
				redirect(base_url().'admin/screenshot/screenshotEdit/'.$this->input->post('id', TRUE));
				
			}
		}

		$data=array(
			'game_name'=>$this->input->post('name', TRUE),
			'description'=>$this->input->post('description', TRUE),
			'image'=>$img
			);	

		if($this->General_model->update_data('screenshot',$data,array('id'=>$this->input->post('id', TRUE))))
		{
			$this->session->set_userdata('message_succ', 'Successfully updated');	
			redirect(base_url().'admin/Screenshot/screenshotEdit/'.$this->input->post('id', TRUE));
		}
		else{
			$this->session->set_userdata('message_err', 'Failure to add');	
			redirect(base_url().'admin/Screenshot/addScreenshot/screenshotEdit/'.$this->input->post('id', TRUE));
		}
	}

	//delete
	public function delete($id)
	{
		$this->check_user_page_access();
		
		 $r=$this->General_model->delete_data('screenshot',array('id'=>$id));
		if($r)
			{
			$this->session->set_userdata('message_succ', 'Delete Successfull');	
			redirect(base_url().'admin/screenshot');
			}
			else
			{
			$this->session->set_userdata('message_err', 'Failure to delete');	
			redirect(base_url().'admin/screenshot');
			}
		
	}
}
