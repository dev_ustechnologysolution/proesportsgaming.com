<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include BASEPATH.'../application/controllers/admin/My_Controller.php';

class Terms extends My_Controller 

{

  	function __construct()

    {

	parent::__construct();

    $this->target_dir=BASEPATH.'../upload/banner/';

	}



	public function index()

	{

		$this->check_user_page_access();

		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

        $this->breadcrumbs->push('Term List', 'admin/terms');



		$data['active']='terms_list';

		$data['page_name']='Term List';

		

		$data['list']=$this->General_model->view_all_data('term','id','asc');

		$content=$this->load->view('admin/term/term_list.tpl.php',$data,true);

		$this->render($content);

	}

	//load edit page

	public function termEdit($id='-1')

	{

		$this->check_user_page_access();

		// add breadcrumbs

		$this->breadcrumbs->push('Home', 'admin/dashboard');

       $this->breadcrumbs->push('Term List', 'admin/terms');

		$this->breadcrumbs->push('Edit Terms', 'admin/terms/termEdit');



		$data['active']='terms_edit';

		$data['page_name']='Edit Page';



		$data['term_data']=$this->General_model->view_data('term',array('id'=>$id));

		$content=$this->load->view('admin/term/term_edit.tpl.php',$data,true);

		$this->render($content);

	}

	//edit 

	public function termUpdate()

	{

		$this->check_user_page_access();

		//$data['active']='game_edit';

		$data=array(

			'content'=>$this->input->post('content', TRUE)
		 );

		 $r=$this->General_model->update_data('term',$data,array('id'=>$this->input->post('id', TRUE)));

		if($r)

		{

			$this->session->set_userdata('message_succ', 'Update successfull');	

			redirect(base_url().'admin/terms/termEdit/'.$this->input->post('id', TRUE));

		}

		else

		{

			$this->session->set_userdata('message_err', 'Failure to update');	

			redirect(base_url().'admin/terms/termEdit/'.$this->input->post('id', TRUE));

		}	

	}
	
}

