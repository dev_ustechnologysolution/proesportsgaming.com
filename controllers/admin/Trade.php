<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH.'../application/controllers/admin/My_Controller.php';

class Trade extends My_Controller 
{
	function __construct() {
        parent::__construct();
        $this->load->model('Admin_user_model');
        $this->load->model('Paypal_payment_model');
    }

    // List all packages
    public function index() {
		$this->check_user_page_access();
		// add breadcrumbs
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Trade History List', 'admin/trade');
        $data['active']='trade_history';
		$data['page_name']='Trade History';
		$query = $this->db->query('SELECT th.*,ud.account_no,ud.name FROM `trade_history` as th inner join user_detail as ud on ud.user_id = th.user_id');
		$data['trade_data'] = $query->result();
        $query1 = $this->db->query('SELECT th.*,ud.account_no,ud.name FROM `tip_history` as th inner join user_detail as ud on ud.user_id = th.user_to  order by created desc');
        $data['tip_data'] = $query1->result();
        
		$content=$this->load->view('admin/trade/trade.tpl.php',$data,true);
		$this->render($content);
    }
    public function tip_history(){
        $this->check_user_page_access();
        // add breadcrumbs
        $this->breadcrumbs->push('Home', 'admin/dashboard');
        $this->breadcrumbs->push('Tip History List', 'admin/trade/tip_history');
        $data['active']='tip_history';
        $data['page_name']='Tip History';        
        $query1 = $this->db->query('SELECT th.*,ud.account_no,ud.name FROM `tip_history` as th inner join user_detail as ud on ud.user_id = th.user_to  order by created desc');
        $data['tip_data'] = $query1->result();
        
        $content=$this->load->view('admin/trade/tip_history.tpl.php',$data,true);
        $this->render($content); 
    }
    public function subscription() {
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
       	$this->breadcrumbs->push('Subscription List', 'admin/trade/subscription');
		$data['active']='subscriber_history';
		$data['page_name']='Subscription List';
        $arr = array(
            'subscribe_status' => 1,
            'is_history' => 0,
        );
        $data['subscription_data'] = $this->General_model->getsubscribedlist($arr);
		$content=$this->load->view('admin/trade/subscription_list.tpl.php',$data,true);
		$this->render($content);		
    }
    public function subscription_history() {
		$this->check_user_page_access();
		$this->breadcrumbs->push('Home', 'admin/dashboard');
       	$this->breadcrumbs->push('Subscription List', 'admin/trade/subscription');
       	$this->breadcrumbs->push('Subscription History', 'admin/trade/subscription_history');
		$data['active']='subscriber_history';
		$data['page_name']='Subscription History List';
        $arr = array(
            'transaction_id' => $_GET['transaction_id'],
            'subscribe_status' => 1,
            'is_history' => 1
        );
        $data['subscription_history_data'] = $this->General_model->getsubscribedlist($arr);
		$content=$this->load->view('admin/trade/subscription_history_list.tpl.php',$data,true);
		$this->render($content);		
    }
    public function unsubscribe($dataid) {
        $this->check_user_page_access();
        $PayPalResponseResult = $this->Paypal_payment_model->get_recurring_payments_profile_details($dataid);
        $subscription_opt = 'Cancel';
        if($PayPalResponseResult['status'] == 1) {
            $PayPalResult = $PayPalResponseResult['data'];
            if ($PayPalResult['STATUS'] != 'Cancelled') {
                $this->Paypal_payment_model->Manage_recurring_payments_profile_status($dataid,$subscription_opt);
            }
            $r = $this->General_model->update_data('paypal_subscriptions', array('subscribe_status' => '0','updated_at' => date('Y-m-d H:i:s', time())), array('transaction_id' => $dataid));
            if ($r) {
                $this->session->set_userdata('message_succ', 'Unsubscribe successfull');
            } else {
                $this->session->set_userdata('message_err', 'Unable to Unsubscribe');
            }
            redirect(base_url().'admin/trade/subscription/');
        } else {
            $this->error($PayPalResponseResult['data']);
        }
    }
    public function dataadd_from_history_to_main_table() {
        $this->check_user_page_access();
        $data['trade_data'] = $this->General_model->view_data('paypal_subscriptions_history', array());
        foreach ($data['trade_data'] as $key => $td) {
            $subs_detail = $this->General_model->view_single_row('paypal_subscriptions', array('subscribe_status' => 1,'is_history' => 0, 'transaction_id' => $td['payment_id']),'*');
            $arr = array(
                'token_id' => $subs_detail['token_id'],
                'transaction_id' => $subs_detail['transaction_id'],
                'user_from' => $subs_detail['user_from'],
                'user_to' => $subs_detail['user_to'],
                'subscribption_plan_id' => $subs_detail['subscribption_plan_id'],
                'payment_method' => $subs_detail['payment_method'],
                'validity' => $subs_detail['validity'],
                'valid_from' => $subs_detail['valid_from'],
                'amount' => $subs_detail['amount'],
                'fee' => $subs_detail['fee'],
                'currency_code' => $subs_detail['currency_code'],
                'payer_email' => $subs_detail['payer_email'],
                'payment_status' => $td['payment_status'],
                'is_history' => 1,
                'subscribe_status' => $subs_detail['subscribe_status'],
                'created_at' => $subs_detail['created_at']
            );
            $r = $this->General_model->insert_data('paypal_subscriptions',$arr);
        }
        echo ($r) ? 'inserted' : 'fail';
    }
}
