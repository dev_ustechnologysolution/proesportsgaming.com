<?php
class Admin_user_model extends CI_Model
{
    public function __construct()
    {
	      parent::__construct();
    }
// user authenticate check
    function authenticate($login, $password)
    {
		$sql = "SELECT * FROM cre_admin_user WHERE
                email = '".$this->db->escape_str($login)."' AND password = '".$this->db->escape_str($password)."' ";
		$query= $this->db->query($sql);
		$result_arr= $query->result_array();
		if( isset($result_arr[0]) )
            return $result_arr[0];
		else
            return false;
    }
    // login session value initialization
    function log_this_login($user_data)
    {
        $sql= "SELECT name,img FROM cre_admin_detail WHERE user_id= '{$user_data['id']}'";
        $query=$this->db->query($sql);
		$row = $query->row();
		$name=$row->name;
		$img=$row->img;
		$this->db->update('cre_admin_user', array('last_login'=>date('Y-m-d H:i:s')),array('id'=>$user_data['id']));
	    $session_data   = array('admin_user_id'=>$user_data['id'],
                            'admin_user_name'=>$name,
							'admin_user_role_id'=>$user_data['role_id'],
							'img'=>$img,
							);

        $_SESSION["timeout"] = time()+ (24 * 60 * 60);
        $this->session->set_userdata($session_data);
    }
    //logout
    function logout_this_login()
    {
        $this->db->update('cre_admin_user', array('last_logout'=>date('Y-m-d H:i:s')),array('id'=>$this->session->userdata('admin_user_id')));
       $session_data   = array('admin_user_id',
                            'admin_user_name',
							'admin_user_role_id',
							'img',
							);
        $this->session->unset_userdata($session_data);
        unset($_SESSION["timeout"]);
    }

	   function user_detail()
    {
    	  $this->db->select('*');
        $this->db->from('cre_admin_user');
        $this->db->join('cre_admin_detail', 'cre_admin_user.id = cre_admin_detail.user_id');
    	  $this->db->where('cre_admin_user.id', $this->session->userdata('admin_user_id'));
        $query = $this->db->get();
	      return $query->result_array();
	}

	public function check_password($table_name,$condition)
    {
	    $this->db->where($condition);
	    $query = $this->db->get($table_name);
		return $query->num_rows();
	}

  public function user_point_list(){
        
        // ap.point_title,
        $this->db->select('p.*,ud.name,ud.user_id');

		$this->db->from('payment p');

		// $this->db->join('admin_packages ap','ap.id=p.package_id');

		$this->db->join('user_detail ud','ud.user_id=p.user_id');

        $this->db->order_by('p.id', 'DESC');

		$query=$this->db->get();

		return $query->result_array();
  }


}
?>
