<?php
class Cart_model extends CI_Model {
	public function __construct() {
		parent::__construct();    
	}
	// My Cart Item
    function cart_item() {
        $cart_item = $this->General_model->view_data('products_cart', array(
            'user_id' => $_SESSION['user_id']
        ));
        return $cart_item;
    }
    function get_attribute_img($attr='') {
        $this->db->select('pat.product_id, pat.attribute_id, pat.option_id, pat.option_image');
        $this->db->from('product_attributes_tbl pat');
        (isset($attr['attribute_id']) && $attr['attribute_id'] != '') ? $this->db->where('pat.attribute_id',$attr['attribute_id']) : '';
        (isset($attr['option_id']) && $attr['option_id'] != '') ? $this->db->where('pat.option_id',$attr['option_id']) : '';
        $res = $this->db->get()->row();
        return $res;
    }
}