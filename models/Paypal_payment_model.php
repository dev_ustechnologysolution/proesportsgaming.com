<?php
class Paypal_payment_model extends CI_Model 
{
	public function __construct(){
		parent::__construct();
		$this->load->model('User_model');
        $this->load->helper('url');
        $this->load->helper('file');
		$config = array(       
            'APIUsername' => $this->paypal_lib->apiusername,   
            'APIPassword' => $this->paypal_lib->apipassword,   
            'APISignature' => $this->paypal_lib->apisignature,  
            'APISubject' => '',                                 
            'APIVersion' => $this->paypal_lib->apiversion      
        );
        $this->load->library('paypal_pro', $config);
	}
    //This method use to call paypal payment method.its redirect to paypal payment gateway and return with url and token.
    function Set_express_checkout($data){

        $res = $PaymentOrderItems = $ShippingOptions = $Payments = array();       
        $SECFields = array(
            'token' => '',                                                                                 
            // A timestamped token, the value of which was returned by a previous SetExpressCheckout call.
            'maxamt' => $data['subscribe_amount'],
            // The expected maximum total amount the order will be, including S&H and sales tax.
            'returnurl' => $data['returnurl'],
            // Required.  URL to which the customer will be returned after returning from PayPal.  2048 char max.
            'cancelurl' => $data['cancelurl'],
            // Required.  URL to which the customer will be returned if they cancel payment on PayPal's site.
            'callback' => '',                                                                                   // URL to which the callback request from PayPal is sent.  Must start with https:// for production.
            'callbacktimeout' => '6', 
            // An override for you to request more or less time to be able to process the callback request and response.  Acceptable range for override is 1-6 seconds.  If you specify greater than 6 PayPal will use default value of 3 seconds.
            'callbackversion' => '',  
            // The version of the Instant Update API you're using.  The default is the current version.                         
            'reqconfirmshipping' => '',
            // The value 1 indicates that you require that the customer's shipping address is Confirmed with PayPal.  This overrides anything in the account profile.  Possible values are 1 or 0.
            'noshipping' => '',                                                                                // The value 1 indiciates that on the PayPal pages, no shipping address fields should be displayed.  Maybe 1 or 0.
            'allownote' => '',                                                                                  // The value 1 indiciates that the customer may enter a note to the merchant on the PayPal page during checkout.  The note is returned in the GetExpresscheckoutDetails response and the DoExpressCheckoutPayment response.  Must be 1 or 0.
            'addroverride' => '',
            // The value 1 indiciates that the PayPal pages should display the shipping address set by you in the SetExpressCheckout request, not the shipping address on file with PayPal.  This does not allow the customer to edit the address here.  Must be 1 or 0.
            'localecode' => '',                                                                                 // Locale of pages displayed by PayPal during checkout.  Should be a 2 character country code.  You can retrive the country code by passing the country name into the class' GetCountryCode() function.
            'pagestyle' => '', 
            // Sets the Custom Payment Page Style for payment pages associated with this button/link.  
            'hdrimg' => '',
            // URL for the image displayed as the header during checkout.  Max size of 750x90.  Should be stored on an https: server or you'll get a warning message in the browser.
            'hdrbordercolor' => '',
            // Sets the border color around the header of the payment page.  The border is a 2-pixel permiter around the header space.  Default is black.  
            'hdrbackcolor' => '', 
            // Sets the background color for the header of the payment page.  Default is white.  
            'payflowcolor' => '',
            // Sets the background color for the payment page.  Default is white.
            'skipdetails' => '',
            // This is a custom field not included in the PayPal documentation.  It's used to specify whether you want to skip the GetExpressCheckoutDetails part of checkout or not.  See PayPal docs for more info.
            'email' => $data['payer_email'], 
            // Email address of the buyer as entered during checkout.  PayPal uses this value to pre-fill the PayPal sign-in page.  127 char max.
            'solutiontype' => '',
            // Type of checkout flow.  Must be Sole (express checkout for auctions) or Mark (normal express checkout)
            'landingpage' => '',
            // Type of PayPal page to display.  Can be Billing or Login.  If billing it shows a full credit card form.  If Login it just shows the login screen.
            'channeltype' => '',
            // Type of channel.  Must be Merchant (non-auction seller) or eBayItem (eBay auction)
            'giropaysuccessurl' => '', 
            // The URL on the merchant site to redirect to after a successful giropay payment.  Only use this field if you are using giropay or bank transfer payment methods in Germany.
            'giropaycancelurl' => '',
            // The URL on the merchant site to redirect to after a canceled giropay payment.  Only use this field if you are using giropay or bank transfer methods in Germany.
            'banktxnpendingurl' => '',
            // The URL on the merchant site to transfer to after a bank transfter payment.  Use this field only if you are using giropay or bank transfer methods in Germany.
            'brandname' => '',
            // A label that overrides the business name in the PayPal account on the PayPal hosted checkout pages.  127 char max.
            'customerservicenumber' => '',
            // Merchant Customer Service number displayed on the PayPal Review page. 16 char max.
            'giftmessageenable' => '',
            // Enable gift message widget on the PayPal Review page. Allowable values are 0 and 1
            'giftreceiptenable' => '', 
            // Enable gift receipt widget on the PayPal Review page. Allowable values are 0 and 1
            'giftwrapenable' => '',
            // Enable gift wrap widget on the PayPal Review page.  Allowable values are 0 and 1.
            'giftwrapname' => '',
            // Label for the gift wrap option such as "Box with ribbon".  25 char max.
            'giftwrapamount' => '',
            // Amount charged for gift-wrap service.
            'buyeremailoptionenable' => '',
            // Enable buyer email opt-in on the PayPal Review page. Allowable values are 0 and 1
            'surveyquestion' => '',
            // Text for the survey question on the PayPal Review page. If the survey question is present, at least 2 survey answer options need to be present.  50 char max.
            'surveyenable' => '',
            // Enable survey functionality. Allowable values are 0 and 1
            'totaltype' => '',
            // Enables display of "estimated total" instead of "total" in the cart review area.  Values are:  Total, EstimatedTotal
            'notetobuyer' => '',
            // Displays a note to buyers in the cart review area below the total amount.  Use the note to tell buyers about items in the cart, such as your return policy or that the total excludes shipping and handling.  127 char max.                          
            'buyerid' => '', 
            // The unique identifier provided by eBay for this buyer. The value may or may not be the same as the username. In the case of eBay, it is different. 255 char max.
            'buyerusername' => '',
            // The user name of the user at the marketplaces site.
            'buyerregistrationdate' => '',
            // Date when the user registered with the marketplace.
            'allowpushfunding' => ''
            // Whether the merchant can accept push funding.  0 = Merchant can accept push funding : 1 = Merchant cannot accept push funding.           
        );
            
        // Basic array of survey choices.  Nothing but the values should go in here.  
        $SurveyChoices = array('choices1', 'choices2', 'choices3', 'etc');
        
        // You can now utlize parallel payments (split payments) within Express Checkout.
        // Here we'll gather all the payment data for each payment included in this checkout 
        // and pass them into a $Payments array.  
        
        // Keep in mind that each payment will ahve its own set of OrderItems
        // so don't get confused along the way.
        
        $Payment = array(
            'amt' => $data['subscribe_amount'],                            
            // Required.  The total cost of the transaction to the customer.  If shipping cost and tax charges are known, include them in this value.  If not, this value should be the current sub-total of the order.
            'currencycode' => '',                   
            // A three-character currency code.  Default is USD.
            'itemamt' => '',                        
            // Required if you specify itemized L_AMT fields. Sum of cost of all items in this order.  
            'shippingamt' => '',                    
            // Total shipping costs for this order.  If you specify SHIPPINGAMT you mut also specify a value for ITEMAMT.
            'shipdiscamt' => '',                
            // Shipping discount for this order, specified as a negative number.
            'insuranceoptionoffered' => '',         
            // If true, the insurance drop-down on the PayPal review page displays the string 'Yes' and the insurance amount.  If true, the total shipping insurance for this order must be a positive number.
            'handlingamt' => '',                    
            // Total handling costs for this order.  If you specify HANDLINGAMT you mut also specify a value for ITEMAMT.
            'taxamt' => '',                         
            // Required if you specify itemized L_TAXAMT fields.  Sum of all tax items in this order. 
            'desc' => '',                           
            // Description of items on the order.  127 char max.
            'custom' => $data['custom'],                         
            // Free-form field for your own use.  256 char max.
            'invnum' => '',                         
            // Your own invoice or tracking number.  127 char max.
            'notifyurl' => $data['notifyurl'],                      
            // URL for receiving Instant Payment Notifications
            'shiptoname' => (isset($data['shiptoname']) && $data['shiptoname'] !='') ? $data['shiptoname'] : '',                     
            // Required if shipping is included.  Person's name associated with this address.  32 char max.
            'shiptostreet' => (isset($data['shippaddress']) && $data['shippaddress'] !='') ? $data['shippaddress'] : '',                   
            // Required if shipping is included.  First street address.  100 char max.
            'shiptostreet2' => '',                  
            // Second street address.  100 char max.
            'shiptocity' => '',                     
            // Required if shipping is included.  Name of city.  40 char max.
            'shiptostate' => (isset($data['shiptostate']) && $data['shiptostate'] !='') ? $data['shiptostate'] : '',             // Required if shipping is included.  Name of state or province.  40 char max.
            'shiptozip' => (isset($data['shiptozip']) && $data['shiptozip'] !='') ? $data['shiptozip'] : '',               // Required if shipping is included.  Postal code of shipping address.  20 char max.
            'shiptocountrycode' => (isset($data['shiptocountrycode']) && $data['shiptocountrycode'] !='') ? $data['shiptocountrycode'] : '',              // Required if shipping is included.  Country code of shipping address.  2 char max.
            'shiptophonenum' => (isset($data['shiptophonenum']) && $data['shiptophonenum'] !='') ? $data['shiptophonenum'] : '', // Phone number for shipping address.  20 char max.
            'notetext' => '',                       
            // Note to the merchant.  255 char max.  
            'allowedpaymentmethod' => '',           
            // The payment method type.  Specify the value InstantPaymentOnly.
            'allowpushfunding' => '',               
            // Whether the merchant can accept push funding:  0 - Merchant can accept push funding.  1 - Merchant cannot accept push funding.  This will override the setting in the merchant's PayPal account.
            'paymentaction' => '',                  
            // How you want to obtain the payment.  When implementing parallel payments, this field is required and must be set to Order. 
            'paymentrequestid' => '',               
            // A unique identifier of the specific payment request, which is required for parallel payments. 
            'sellerid' => '',                       
            // The unique non-changing identifier for the seller at the marketplace site.  This ID is not displayed.
            'sellerusername' => '',                 
            // The current name of the seller or business at the marketplace site.  This name may be shown to the buyer.
            'sellerpaypalaccountid' => ''           
            // A unique identifier for the merchant.  For parallel payments, this field is required and must contain the Payer ID or the email address of the merchant.
        );
            
        // For order items you populate a nested array with multiple $Item arrays.  
        // Normally you'll be looping through cart items to populate the $Item array
        // Then push it into the $OrderItems array at the end of each loop for an entire 
        // collection of all items in $OrderItems.
        $Item = array(
            'name' => '',                               // Item name. 127 char max.
            'desc' => '',                               // Item description. 127 char max.
            'amt' => '',                                // Cost of item.
            'number' => '',                             // Item number.  127 char max.
            'qty' => '',                                // Item qty on order.  Any positive integer.
            'taxamt' => '',                             // Item sales tax
            'itemurl' => '',                            // URL for the item.
            'itemweightvalue' => '',                    // The weight value of the item.
            'itemweightunit' => '',                     // The weight unit of the item.
            'itemheightvalue' => '',                    // The height value of the item.
            'itemheightunit' => '',                     // The height unit of the item.
            'itemwidthvalue' => '',                     // The width value of the item.
            'itemwidthunit' => '',                      // The width unit of the item.
            'itemlengthvalue' => '',                    // The length value of the item.
            'itemlengthunit' => '',                     // The length unit of the item.
            'itemurl' => '',                            // URL for the item.
            'itemcategory' => '',                       // Must be one of the following values:  Digital, Physical
            'ebayitemnumber' => '',                     // Auction item number.  
            'ebayitemauctiontxnid' => '',               // Auction transaction ID number.  
            'ebayitemorderid' => '',                    // Auction order ID number.
            'ebayitemcartid' => ''                      // The unique identifier provided by eBay for this order from the buyer. These parameters must be ordered sequentially beginning with 0 (for example L_EBAYITEMCARTID0, L_EBAYITEMCARTID1). Character length: 255 single-byte characters
        );
        if (isset($data['cartitem']) && !empty($data['cartitem'])) {
            foreach ($data['cartitem'] as $key => $itm) {
                $Item[] = array(
                    'name' => $itm['name'],
                    'amt' => $itm['amt'],
                    'number' => $itm['number'],
                    'qty' => $itm['qty'],
                    'itemurl' => $itm['itemurl'],
                );
            }
        }
        array_push($PaymentOrderItems, $Item);
            
        // Now we've got our OrderItems for this individual payment, 
        // so we'll load them into the $Payment array
        $Payment['order_items'] = $PaymentOrderItems;
        
        // Now we add the current $Payment array into the $Payments array collection
        array_push($Payments, $Payment);
        
        $BuyerDetails = array(
            'buyerid' => '',                // The unique identifier provided by eBay for this buyer.  The value may or may not be the same as the username.  In the case of eBay, it is different.  Char max 255.
            'buyerusername' => '',          // The username of the marketplace site.
            'buyerregistrationdate' => ''   // The registration of the buyer with the marketplace.
        );
                                    
        // For shipping options we create an array of all shipping choices similar to how order items works.
      
        $Option = array(
            'l_shippingoptionisdefault' => '',              // Shipping option.  Required if specifying the Callback URL.  true or false.  Must be only 1 default!
            'l_shippingoptionname' => '',                   // Shipping option name.  Required if specifying the Callback URL.  50 character max.
            'l_shippingoptionlabel' => '',                  // Shipping option label.  Required if specifying the Callback URL.  50 character max.
            'l_shippingoptionamount' => ''                  // Shipping option amount.  Required if specifying the Callback URL.  
        );
        array_push($ShippingOptions, $Option);
                
        // For billing agreements we create an array similar to working with 
        // payments, order items, and shipping options. 
        $BillingAgreements = array();
        $Item = array(
          'l_billingtype' => $data['billingtype'],                            // Required.  Type of billing agreement.  For recurring payments it must be RecurringPayments.  You can specify up to ten billing agreements.  For reference transactions, this field must be either:  MerchantInitiatedBilling, or MerchantInitiatedBillingSingleSource
          'l_billingagreementdescription' => $data['title'],            // Required for recurring payments.  Description of goods or services associated with the billing agreement.  
          'l_paymenttype' => '',                            // Specifies the type of PayPal payment you require for the billing agreement.  Any or IntantOnly
          'l_billingagreementcustom' => ''                  // Custom annotation field for your own use.  256 char max.
        );
        array_push($BillingAgreements, $Item);
            
        $PayPalRequestData = array(
            'SECFields' => $SECFields, 
            'SurveyChoices' => $SurveyChoices, 
            'Payments' => $Payments, 
            'BuyerDetails' => $BuyerDetails, 
            'ShippingOptions' => $ShippingOptions, 
            'BillingAgreements' => $BillingAgreements
        );
        $PayPalResult = $this->paypal_pro->SetExpressCheckout($PayPalRequestData);
        if (!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK'])) {
            $res['errors'] = array('Errors'=>$PayPalResult['ERRORS']);
        } else {
            $res['redirect'] = $PayPalResult['REDIRECTURL'];
            $res['data'] = $PayPalResult;
        }
        return $res;
    }
	//This method use to get deatils of checkout by token.
    function Get_express_checkout_details($token){
        $result = array();
        $PayPalResult = $this->paypal_pro->GetExpressCheckoutDetails($token);
        if (!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK'])) {
            $errors = array('Errors'=>$PayPalResult['ERRORS']);
            $result['status'] = 0;
            $result['data'] = $errors;            
        } else {
            $result['status'] = 1;
            $result['data'] = $PayPalResult;
        }
        return $result;
    }
    //This method use to create recurring payment profile
    function CreateRecurringPaymentsProfile($PayPalResult,$subscribername,$plan_description,$billing_duration){
        $CRPPFields = array(
            'token' => $PayPalResult['TOKEN'],
            // Token returned from PayPal SetExpressCheckout.  Can also use token returned from SetCustomerBillingAgreement.
        );
                        
        $ProfileDetails = array(
            'subscribername' => $subscribername, 
            // Full name of the person receiving the product or service paid for by the recurring payment.  32 char max.
            'profilestartdate' => $PayPalResult['TIMESTAMP'],                   
            // Required.  The date when the billing for this profiile begins.  Must be a valid date in UTC/GMT format.
            'profilereference' => ''                   
            // The merchant's own unique invoice number or reference ID.  127 char max.
        );
                        
        $ScheduleDetails = array(
            'desc' => $plan_description, 
            // Required.  Description of the recurring payment.  This field must match the corresponding billing agreement description included in SetExpressCheckout.
            'maxfailedpayments' => '',  
            // The number of scheduled payment periods that can fail before the profile is automatically suspended.  
            'autobilloutamt' => 'AddToNextBilling'  
            // This field indiciates whether you would like PayPal to automatically bill the outstanding balance amount in the next billing cycle.  Values can be: NoAutoBill or AddToNextBilling
        );
                        
        $BillingPeriod = array(
            'trialbillingperiod' => '', 
            'trialbillingfrequency' => '', 
            'trialtotalbillingcycles' => '', 
            'trialamt' => '', 
            'billingperiod' => $billing_duration,                      
            // Required.  Unit for billing during this subscription period.  One of the following: Day, Week, SemiMonth, Month, Year
            'billingfrequency' => 1,                   
            // Required.  Number of billing periods that make up one billing cycle.  The combination of billing freq. and billing period must be less than or equal to one year. 
            'totalbillingcycles' => '0',                 
            // the number of billing cycles for the payment period (regular or trial).  For trial period it must be greater than 0.  For regular payments 0 means indefinite...until canceled.  
            'amt' => $PayPalResult['AMT'],                                
            // Required.  Billing amount for each billing cycle during the payment period.  This does not include shipping and tax. 
            'currencycode' => $PayPalResult['CURRENCYCODE'],                       
            // Required.  Three-letter currency code.
            'shippingamt' => '',                        
            // Shipping amount for each billing cycle during the payment period.
            'taxamt' => ''                              
            // Tax amount for each billing cycle during the payment period.
        );
                        
        $ActivationDetails = array(
            'initamt' => '',                            
            // Initial non-recurring payment amount due immediatly upon profile creation.  Use an initial amount for enrolment or set-up fees.
            'failedinitamtaction' => 'CancelOnFailure',                
            // By default, PayPal will suspend the pending profile in the event that the initial payment fails.  You can override this.  Values are: ContinueOnFailure or CancelOnFailure
        );
                        
        $CCDetails = array(
            'creditcardtype' => '',                   
            // Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
            'acct' => '',                               
            // Required.  Credit card number.  No spaces or punctuation.  
            'expdate' => '',                          
            // Required.  Credit card expiration date.  Format is MMYYYY
            'cvv2' => '',                                
            // Requirements determined by your PayPal account settings.  Security digits for credit card.
            'startdate' => '',                          
            // Month and year that Maestro or Solo card was issued.  MMYYYY
            'issuenumber' => ''                         
            // Issue number of Maestro or Solo card.  Two numeric digits max.
        );
                        
        $PayerInfo = array(
            'email' => $PayPalResult['EMAIL'],            // Email address of payer.
            'payerid' => $PayPalResult['PAYERID'],        // Unique PayPal customer ID for payer.
            'payerstatus' => $PayPalResult['PAYERSTATUS'],// Status of payer.  Values are verified or unverified
            'business' => ''                              // Payer's business name.
        );
                        
        $PayerName = array(
            'salutation' => '',                         // Payer's salutation.  20 char max.
            'firstname' => $PayPalResult['FIRSTNAME'],  // Payer's first name.  25 char max.
            'middlename' => '',                         // Payer's middle name.  25 char max.
            'lastname' => $PayPalResult['LASTNAME'],    // Payer's last name.  25 char max.
            'suffix' => ''                              // Payer's suffix.  12 char max.
        );
        $BillingAddress = array(
            'street' => '',                         // Required.  First street address.
            'street2' => '',                        // Second street address.
            'city' => '',                           // Required.  Name of City.
            'state' => '',                          // Required. Name of State or Province.
            'countrycode' => '',                    // Required.  Country code.
            'zip' => '',                            // Required.  Postal code of payer.
            'phonenum' => ''                        // Phone Number of payer.  20 char max.
        );
                            
        $ShippingAddress = array(
            'shiptoname' => '',                     
            // Required if shipping is included.  Person's name associated with this address.  32 char max.
            'shiptostreet' => '',                  
            // Required if shipping is included.  First street address.  100 char max.
            'shiptostreet2' => '',                 
            // Second street address.  100 char max.
            'shiptocity' => '',                    
            // Required if shipping is included.  Name of city.  40 char max.
            'shiptostate' => '',                   
            // Required if shipping is included.  Name of state or province.  40 char max.
            'shiptozip' => '',                      
            // Required if shipping is included.  Postal code of shipping address.  20 char max.
            'shiptocountry' => '',                   
            // Required if shipping is included.  Country code of shipping address.  2 char max.
            'shiptophonenum' => ''                  
            // Phone number for shipping address.  20 char max.
        );

        $PayPalRequestData = array(
            'CRPPFields' => $CRPPFields, 
            'ProfileDetails' => $ProfileDetails, 
            'ScheduleDetails' => $ScheduleDetails, 
            'BillingPeriod' => $BillingPeriod, 
            'ActivationDetails' => $ActivationDetails, 
            'CCDetails' => $CCDetails, 
            'PayerInfo' => $PayerInfo, 
            'PayerName' => $PayerName, 
            'BillingAddress' => $BillingAddress, 
            'ShippingAddress' => $ShippingAddress
        ); 
        
        $result = $this->paypal_pro->CreateRecurringPaymentsProfile($PayPalRequestData);
        $final_result = array();   
        if(!$this->paypal_pro->APICallSuccessful($result['ACK'])) {
            $errors = array('Errors'=>$result['ERRORS']);
            $final_result['status'] = 0;
            $final_result['data'] = $errors;
        } else {
            $final_result['status'] = 1;
            $final_result['data'] = $result;
        } 
        return $final_result;
    }
    //This method use to stop recurring payment method
	function Manage_recurring_payments_profile_status($payment_id,$subscription_opt) 
	{
        $MRPPSFields = array(
        'profileid' => $payment_id, // Required. Recurring payments profile ID returned from 
        'action' => $subscription_opt,//Required.The action to be performed.Mest be: Cancel, Suspend, Reactivate
        );
                        
        $PayPalRequestData = array('MRPPSFields' => $MRPPSFields);
        
        $PayPalResult = $this->paypal_pro->ManageRecurringPaymentsProfileStatus($PayPalRequestData);
        if (!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK'])) {
            $errors = array('Errors'=>$PayPalResult['ERRORS']);
        }
    }
    //This method use to get details of specific payment history by payment id
    function get_recurring_payments_profile_details($payment_id) 
    {
        $result = array();
        $GRPPDFields = array(
            'profileid' => $payment_id
        );
        $PayPalRequestData = array('GRPPDFields' => $GRPPDFields);        
        $PayPalResult = $this->paypal_pro->GetRecurringPaymentsProfileDetails($PayPalRequestData);        
        if(!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK'])) {
            $result['status'] = 0;
            $errors = array('Errors'=>$PayPalResult['ERRORS']);
            $result['data'] = $errors;
        } else {
            $result['status'] = 1;
            $result['data'] =  $PayPalResult;            
        }
        return $result;
    }    
}


 