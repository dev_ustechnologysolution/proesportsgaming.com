<?php
class Product_model extends CI_Model 
{
	public function __construct(){
		parent::__construct();    
	}
    function getCategories(){
        $results = $this->db
                ->select('id,parent_id,name')
                ->order_by('id','asc')
                ->get('product_categories_tbl')
                ->result_array();
        return $results;
    }
    function getCategoryByParent($parent_id){
        $results = $this->db
                ->select('id,parent_id,name')
                ->where('parent_id',$parent_id)
                ->order_by('id','asc')
                ->get('product_categories_tbl')
                ->result_array();
        return $results;
    }
    function getCategoryTree($idField='id', $parentIdField='parent_id', $childrenField='children') {
        $results = $this->getCategories();
        $hierarchy = array(); 
        $itemReferences = array(); 

        foreach ( $results as $item ) {
            $id       = $item[$idField];
            $parentId = $item[$parentIdField];
            if (isset($itemReferences[$parentId])) { 
                $itemReferences[$parentId][$childrenField][$id] = $item; 
                $itemReferences[$id] =& $itemReferences[$parentId][$childrenField][$id]; 
            } elseif (!$parentId || !isset($hierarchy[$parentId])) { 
                $hierarchy[$id] = $item;
                $itemReferences[$id] =& $hierarchy[$id];
            }
        }
        unset($results, $item, $id, $parentId);
        
        foreach ( $hierarchy as $id => &$item ) {
            $parentId = $item[$parentIdField];
            if ( isset($itemReferences[$parentId] ) ) { 
                $itemReferences[$parentId][$childrenField][$id] = $item; 
                unset($hierarchy[$id]); 
            }
        }
        unset($itemReferences, $id, $item, $parentId);
        return $hierarchy;
    }
    function getSubCarArr($arr) {
        $this->db->select('GROUP_CONCAT(pct.name SEPARATOR ", ") AS subcate_name_arr, GROUP_CONCAT(pct.id SEPARATOR ", ") AS cate_id_arr');
        $this->db->from('product_categories_tbl pct');
        ($arr['main_cat'] != '') ? $this->db->where('pct.parent_id',$arr['main_cat']) : '';
        $this->db->where('pct.parent_id !=',0);
        return $this->db->get()->row();
    }
    function SearchProductArr($arr) {
        ($arr['get_last_id'] == 'yes') ? $this->db->select('MAX(pt.id) as last_id, COUNT(pt.id) as count') : $this->db->select('pt.*');
        $this->db->from('products_tbl pt');
        if ($arr['main_cat'] != '') {
            $where = "FIND_IN_SET('".$arr['main_cat']."', pt.category_id)";
        }
        if ($arr['main_sub_cat'] != '') {
            $where .= " AND FIND_IN_SET('".$arr['main_sub_cat']."', pt.category_id)";
        }
        (isset($where) && $where !='') ? $this->db->where($where) : '';
        ($arr['product_srch_term'] !='') ? $this->db->where("pt.name LIKE '%".$arr['product_srch_term']."%'") : '';
        ($arr['last_id'] != '') ? $this->db->where('pt.id >'.$arr['last_id']) : '';
        ($arr['limit'] != 0) ? $this->db->limit($arr['limit']) : '';
        $this->db->order_by('id','asc');
        $res_arr = $this->db->get()->result_array();
        return $res_arr;
    }
    function ProductImg($arr) {
        $this->db->select('pi.image, pi.id as image_id');
        $this->db->from('product_images pi');
        $this->db->where('pi.product_id',$arr['product_id']);
        $this->db->where('pi.is_default',1);
        $resrow = $this->db->get()->row();
        return $resrow;
    }
    function productDetail($arr) {
        $this->db->select('pt.*, pi.is_default, pi.image, pi.id as image_id');
        $this->db->from('products_tbl pt');
        $this->db->join('product_images pi', 'pi.product_id = pt.id');
        $this->db->where('pt.id',$arr['product_id']);
        $prdct_dtl = $this->db->get()->result();
        return $prdct_dtl;
    }
    function attributes_list($arr='') {
        $this->db->select('at.title,at.type,paot.attr_id,paot.option_label,paot.option_id');
        $this->db->from('product_attributes_tbl pat');
        $this->db->join('attributes_tbl at', 'at.id = pat.attribute_id');
        $this->db->join('product_attributes_opt_tbl paot', 'paot.option_id = pat.option_id');
        $this->db->where('at.status',1);
        ($arr['product_id'] != '') ? $this->db->where('pat.product_id',$arr['product_id']) : '';
        $attributes_list = $this->db->get()->result();
        return $attributes_list;
    }
    function get_my_product($arr=''){
        $select = 'ot.id, ot.transaction_id, ot.sold_by, ot.tracking_id, ot.user_id, ot.shipping_address_id, ot.billing_address_id, ot.sub_total, ot.tax, ot.grand_total, ot.payment_method, ot.order_status, ot.created_at';
        $select .= ($arr['id'] != '') ? ', oit.order_id as order_id, oit.product_id, oit.product_name, oit.product_image, oit.qty, oit.amount, oit.product_attributes' : ', COUNT(oit.order_id) as count_order';
        $this->db->select($select);
        $this->db->from('orders_tbl ot');
        $this->db->join('orders_items_tbl oit', 'oit.order_id = ot.id');
        // $this->db->join('products_tbl pt', 'pt.id = oit.product_id');
        ($arr['id'] != '') ? $this->db->where('oit.order_id',$arr['id']) : $this->db->group_by('oit.order_id');
        ($arr['user_id'] !='') ? $this->db->where('ot.user_id',$arr['user_id']) : '';
        // $this->db->group_by('oit.order_id');
        $res = $this->db->get()->result();
        return $res;
    }
}


 