<?php

class User_model extends CI_Model

{

    public function __construct() {
    	parent::__construct();
        $this->over_eighteen($_SESSION['user_id']);
    }

// user authenticate check

    function authenticate($login, $password) {
		$sql = "SELECT * FROM user WHERE email = '".$this->db->escape_str($login)."' AND password = '".$this->db->escape_str($password)."' ";
		$query= $this->db->query($sql);
		$result_arr = $query->result_array();
		if( isset($result_arr[0]) )
            return $result_arr[0];
		else
            return false;
    }

    // login session value initialization

    function log_this_login($user_data) {
        $row = $this->get_user_detail($user_data['id']);

		$name			= $row->name;
		$last_logout	= $row->last_logout;
		$img			= $row->image;
        $account_no		= $row->account_no;
        $is_admin       = $row->is_admin;
        $is_18          = $row->is_18;

		$this->db->update('user', array('last_login'=>date('Y-m-d H:i:s'), 'is_active_for_chat' => '1','logged_in'=>'1'),array('id'=>$user_data['id'])); 
        $session_data   = array('user_id'=>$user_data['id'],
            'last_logout' => $last_logout,
            'user_name' => $name,
            'user_image' => $img,
            'account_no' => $account_no,
            'is_admin' => $is_admin,
            'is_18'    => $is_18
        );

        $this->session->set_userdata($session_data);
        $_SESSION["timeout"] = time() + (24 * 60 * 60);
        $this->over_eighteen($user_data['id']);

    }
    function over_eighteen($user_id) {
        $user_detail = $this->get_user_detail($user_id);
        $age = date_diff(date_create($user_detail->dob), date_create('today'))->y;
        $data['is_18'] = 0;
        if ($age >= 18) {
            $update_data=array(
                'is_18' => 1,
                'updated_time' => date('Y-m-d H:i:s')
            );
            $this->db->update('user_detail', $update_data, array('user_id'=>$user_id));
            $data['is_18'] = 1;
        }
        if ($_SESSION['user_id'] == $user_id) {
            $this->session->set_userdata('is_18',$data['is_18']);
        }
        return $data;
    }

    //logout
    function logout_this_login() {
        $this->db->update('user', array('last_logout'=>date('Y-m-d H:i:s'),'logged_in'=>'0','is_active_for_chat'=>'0'),array('id'=>$this->session->userdata('user_id')));
        $session_data   = array('user_id','user_name','user_image','billaddress','shippaddress','guest_user');
        $this->session->unset_userdata($session_data);
        // $this->session->userdata('guest_user')
        unset($_SESSION["timeout"]);
    }

    function changeactive_flag($data_arr) {
        if($data_arr['is_active'] == 1){
            $getstrtime = (strtotime($data_arr['ban_time']) - strtotime(date('Y-m-d H:i:s')))/3600;
            $getstrtime = round($getstrtime);
            if($getstrtime <= 0) {
                $user_data = array(                 
                    'is_active' => 0,
                    'ban_option' => 0,
                    'ban_time' => date("Y-m-d H:i:s")
                );
                $this->General_model->update_data('user',$user_data,array('id'=>$data_arr['id']));
            }
            return $getstrtime;
        }
    }

    function checkbanned($data_arr) {
        $getstrtime = (strtotime($data_arr['ban_time']) - strtotime(date('Y-m-d H:i:s')))/3600;
        $daydiff = round($getstrtime/24);
        $getstrtime = round($getstrtime);
        if($getstrtime >= 48) {
            $ban_message = 'Your Account Has Been Banned for '.$daydiff.' Days.';
        } else if ($data_arr['ban_option'] == 1) {
            $ban_message = 'Your Account Has Been Banned.';
        } else {
            $ban_message = 'Your Account Has Been Banned for '.$getstrtime.' Hours';
        }   
        return $ban_message;
    }

	function user_detail() {
    	$this->db->select('*');
        $this->db->from('user');
        $this->db->join('user_detail', 'user.id = user_detail.user_id');
        // $this->db->join('country c', 'c.country_id = user_detail.country');
        // $this->db->join('state s', 's.stateid = user_detail.state');
        $this->db->where('user.id', $this->session->userdata('user_id'));
        $query = $this->db->get()->result_array();
        if (is_numeric($query[0]['country']) && $query[0]['country'] != '') {
            $this->db->select('*');
            $this->db->from('user');
            $this->db->join('user_detail', 'user.id = user_detail.user_id');
            $this->db->join('country c', 'c.country_id = user_detail.country');
            $this->db->join('state s', 's.stateid = user_detail.state');
            $this->db->where('user.id', $this->session->userdata('user_id'));
            $query = $this->db->get()->result_array();
        }   
        return $query;
	}

    function mail_categories_fun() {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->join('user_detail', 'user.id = user_detail.user_id');
        $this->db->join('mail_categories mc', 'mc.user_id = user_detail.user_id');
        $this->db->where('user.id', $this->session->userdata('user_id'));
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_user_detail($user_id) {
        $this->db->select('ud.is_18,ud.custom_name,ud.name,ud.display_name,ud.is_admin,ud.account_no,ud.image,u.last_logout,ud.dob,ud.total_balance,ud.location,ud.state,ud.country,ud.zip_code');
        $this->db->from('user_detail ud');
        $this->db->join('user u','u.id=ud.user_id');
        $this->db->where('ud.user_id',$user_id);
        $row = $this->db->get()->row();
        return $row;
    }

	public function check_password($table_name,$condition) {
	    $this->db->where($condition); 
	    $query = $this->db->get($table_name);
		return $query->num_rows();
	}

	function user_view_admin() {
		$this->db->select('c.countryname,ud.account_no,ud.is_admin,s.statename,ud.display_name,ud.team_name,u.id,u.email,ud.name,ud.location,ud.taxid,ud.number,ud.is_18,ud.total_balance,ud.won_amt,ud.lost_amt,ud.withdrawal_amt,u.is_active,u.last_login,ud.state,ud.country,ud.zip_code');

	    $this->db->from('user u');

	    $this->db->join('user_detail ud', 'u.id = ud.user_id');
	    $this->db->join('country c', 'ud.country = c.country_id OR ud.country = c.sortname');
	    $this->db->join('state s', 'ud.state = s.stateid');

	    // $this->db->where_in('u.is_active',array(0,1));

	    $this->db->order_by('ud.account_no','asc');
	    
        $query = $this->db->get();
        $data = array();
        if($query !== FALSE && $query->num_rows() > 0){
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
        }

        return $data;
		// return $query->result_array();
	}

    function active_all_users()
    {
        $this->db->select('u.id,u.email,ud.name,ud.location,ud.taxid,ud.number,ud.total_balance,ud.won_amt,ud.lost_amt,ud.withdrawal_amt,u.is_active,u.last_login,ud.state,ud.country,ud.zip_code');

        $this->db->from('user u');

        $this->db->join('user_detail ud', 'u.id = ud.user_id');

        $this->db->where_in('u.logged_in',1);

        $this->db->order_by('u.id','desc');
        
        $query = $this->db->get();
        
        return count($query->result_array());
    }
    public function totalbalance()
    {
        $this->db->select('ud.total_balance');

        $this->db->from('user u');

        $this->db->join('user_detail ud', 'u.id = ud.user_id');

        $query = $this->db->get();
        $totalbalance = '';
        $last_entry_date = '';
        foreach ($query->result_array() as $key => $value) {
            $totalbalance += $value['total_balance'];
        }

        $this->db->select('MAX(ud.updated_time) as updated_time');
        $this->db->from('user_detail ud');
        $last_entry_date = $this->db->get()->row();
        
        $this->db->select('MAX(am.date) as date');
        $this->db->from('admin_amount am');
        $que = $this->db->get()->row();
        
        $this->db->select('am.total_amount');
        $this->db->from('admin_amount am');
        $this->db->where('date',$que->date);
        $amnt = $this->db->get()->row();
        $total_balance = $amnt->total_amount;

        if (strtotime($last_entry_date->updated_time)>strtotime($que->date)) {
         $total_balance = $totalbalance;
        }
        $total_bal['total_balance'] = $total_balance;
        $total_bal['que_date'] = $que->date;        
        return $total_bal;
    }
    public function originaltotalbalance()
    {
        $this->db->select('ud.total_balance');

        $this->db->from('user u');

        $this->db->join('user_detail ud', 'u.id = ud.user_id');

        $query = $this->db->get();
        $totalbalance = '';
        $last_entry_date = '';
        foreach ($query->result_array() as $key => $value) {
            $totalbalance += $value['total_balance'];
        }
        
        return $totalbalance;
    }
    function totalfees()
    {
        $this->db->select('wr.collected_fees');
        $this->db->from('withdraw_request wr');
        $this->db->where('wr.status','1');
        $totalcollectedwithdrawfees = $this->db->get();
        $totalcollectedfees = '';
        foreach ($totalcollectedwithdrawfees->result_array() as $key => $value) {
            $totalcollectedfees += $value['collected_fees'];
        }
        
        $this->db->select('p.collected_fees');
        $this->db->from('payment p');
        $this->db->where('p.payment_status','completed');
        $totalcollectedpaymentfees = $this->db->get();
        foreach ($totalcollectedpaymentfees->result_array() as $key => $value) {
            $totalcollectedfees += $value['collected_fees'];
        }

        $this->db->select('lfc.fee');
        $this->db->from('lobby_fee_colloected lfc');
        $this->db->where('lfc.status','1');
        $totalcollectedlobbyfees = $this->db->get();
        foreach ($totalcollectedlobbyfees->result_array() as $key => $value) {
            $totalcollectedfees += $value['fee'];
        }

        $this->db->select('MAX(ud.updated_time) as updated_time');
        $this->db->from('user_detail ud');
        $last_entry_date = $this->db->get()->row();
        
        $this->db->select('MAX(am.date) as date');
        $this->db->from('admin_amount am');
        $que = $this->db->get()->row();

        $this->db->select('am.total_fees');
        $this->db->from('admin_amount am');
        $this->db->where('date',$que->date);
        $amnt = $this->db->get()->row();
        $totalcollected_fees = $amnt->total_fees;
        
        if (strtotime($last_entry_date->updated_time)>strtotime($que->date)) {
         $totalcollected_fees = $totalcollectedfees;
        }
        return $totalcollected_fees;
    }

    function originaltotalfees()
    {
        $this->db->select('wr.collected_fees');
        $this->db->from('withdraw_request wr');
        $this->db->where('wr.status','1');
        $totalcollectedwithdrawfees = $this->db->get();
        $totalcollectedfees = '';
        foreach ($totalcollectedwithdrawfees->result_array() as $key => $value) {
            $totalcollectedfees += $value['collected_fees'];
        }
        
        $this->db->select('p.collected_fees');
        $this->db->from('payment p');
        $this->db->where('p.payment_status','completed');
        $totalcollectedpaymentfees = $this->db->get();
        foreach ($totalcollectedpaymentfees->result_array() as $key => $value) {
            $totalcollectedfees += $value['collected_fees'];
        }
        return $totalcollectedfees;
    }    

    function admin_map_video()
    {
        $this->db->select('fc.content');

        $this->db->from('footer_content fc');

        $this->db->where('fc.status',1);

        $query = $this->db->get()->row()->content;

        return $query;
    }

	function user_view()
    {
		$this->db->select('u.id,u.email,ud.name,ud.location,ud.taxid,ud.won_amt,ud.lost_amt,ud.withdrawal_amt,ud.number,ud.total_balance,u.is_active,ud.state,ud.country,ud.zip_code');

	    $this->db->from('user u');

	    $this->db->join('user_detail ud', 'u.id = ud.user_id');

	    $this->db->where('u.is_active',0);

	    $this->db->order_by('u.id','desc');
	    
	    $query = $this->db->get();
		return $query->result_array();
	}

    function user_game_history($user_id)
    {
        $this->db->select('g.device_id, ag.game_name, asg.sub_game_name, g.game_type, b.id, b.loser_id, b.winner_id, b.game_id, b.flag, ud.user_id, ud.name, ud.location, ud.number, g.price, b.win_lose_deta');

        $this->db->from('bet b');

        $this->db->join('admin_game ag', 'b.game_id = ag.id');

        $this->db->join('game g', 'g.bet_id = b.id');

        $this->db->join('user_detail ud', 'g.user_id = ud.user_id');

        $this->db->join('admin_sub_game asg', 'asg.id = g.sub_game_id');

        $this->db->where('b.winner_id !=',0);

        $this->db->where('ud.user_id',$user_id);

        $this->db->order_by('g.status',3);

        $this->db->order_by('g.id','desc');

        $query = $this->db->get();
        
        return $query->result_array();
    }

	function winner_result()
    {
        $this->db->select('b.id,b.winner_id,b.game_id,b.flag,ud.name,ud.location,ud.number,ag.game_name,g.price,g.device_id,b.win_lose_deta');

        $this->db->from('bet b');

        $this->db->join('user_detail ud', 'b.winner_id = ud.user_id');

        $this->db->join('admin_game ag', 'b.game_id = ag.id');

        $this->db->join('game g', 'g.bet_id = b.id');

        $this->db->where('b.is_active',0);

        $this->db->order_by('b.winner_id','desc');

        $query = $this->db->get();
        
        return $query->result_array();
    }

    function loser_result()
    {
        $this->db->select('b.id,b.loser_id,b.game_id,b.flag,ud.name,ud.location,ud.number,ag.game_name,g.price,g.device_id,b.win_lose_deta');

        $this->db->from('bet b');

        $this->db->join('user_detail ud', 'b.loser_id = ud.user_id');

        $this->db->join('admin_game ag', 'b.game_id = ag.id');

        $this->db->join('game g', 'g.bet_id = b.id');

        $this->db->where('b.is_active',0);

        $this->db->order_by('b.loser_id','desc');

        $query = $this->db->get();
        
        return $query->result_array();
    }

    public function payment_history() {

        $this->db->select('ud.name,ag.game_name,ph.receiver_email,ph.amount,ph.currency,ph.payment_date,ph.payment_status');

        $this->db->from('admin_payment_history ph');
        
        $this->db->join('user_detail ud','ud.user_id=ph.user_id');

        $this->db->join('admin_game ag','ag.id=ph.game_id');

        $this->db->where('ph.payment_status','completed');

        $query = $this->db->get();

        return $query->result_array();
    }

     public function user_payment_history() {

        $this->db->select('ud.name,ag.game_name,p.transaction_id,p.payer_email,p.amount,p.currency,p.payment_date,p.payment_status');

        $this->db->from('payment p');
        
        $this->db->join('user_detail ud','ud.user_id=p.user_id');

        $this->db->join('admin_game ag','ag.id=p.game_id');

        //$this->db->join('admin_sub_game asg','asg.id=p.subgame_id');

        $this->db->where('p.payment_status','completed');

        $query = $this->db->get();

        return $query->result_array();
    }

	public function check_mail($table_name,$condition) 
    {   

	    $this->db->where($condition); 

	    $query = $this->db->get($table_name);

		return $query->num_rows();

	}

    public function game_total_amnt() {

        $this->db->select('g.game_id,ag.game_name,p.currency,aph.amount as amnt');

        $this->db->select_sum('p.amount');

        $this->db->from('payment p');

        $this->db->join('game g','g.game_id=p.game_id');

        $this->db->join('admin_game ag','ag.id=g.game_id');

        //$this->db->join('admin_sub_game asg','asg.id=g.sub_game_id');

        $this->db->join('admin_payment_history aph','aph.game_id=p.game_id');

        $this->db->where('p.payment_status','completed');

         $this->db->where('aph.payment_status','completed');

        $this->db->group_by('p.game_id');

        $query = $this->db->get();

        return $query->result_array();
    }

	public function get_all_won_amt($id){
		 $this->db->select('b.id,b.winner_id,b.game_id,b.flag,ud.name,ud.location,ud.number,ag.game_name,g.price,g.device_id');

        $this->db->from('bet b');

        $this->db->join('user_detail ud', 'b.winner_id = '.$id);

        $this->db->join('admin_game ag', 'b.game_id = ag.id');

        $this->db->join('game g', 'g.bet_id = b.id');

        $this->db->where('b.is_active',0);
		
        $this->db->where('ud.user_id',$id);
		
        $this->db->where('b.is_win_cron',0);

        $this->db->order_by('b.winner_id','desc');

        $query = $this->db->get();
        
        return $query->result_array();
		
		
		
		
	}
	
	public function get_all_lost_amt($id){
		$this->db->select('b.id,b.loser_id,b.game_id,b.flag,ud.name,ud.location,ud.number,ag.game_name,g.price,g.device_id');

        $this->db->from('bet b');

        $this->db->join('user_detail ud', 'b.loser_id = '.$id);

        $this->db->join('admin_game ag', 'b.game_id = ag.id');

        $this->db->join('game g', 'g.bet_id = b.id');

        $this->db->where('b.is_active',0);
		
		$this->db->where('ud.user_id',$id);
		
		$this->db->where('b.is_lose_cron',0);
		
        $this->db->order_by('b.loser_id','desc');

        $query = $this->db->get();
        
        return $query->result_array();
	}
	
	public function get_all_withdrawal_amt($id){
		$this->db->select('w.id,w.points,w.amount_requested,w.amount_paid,w.req_date,w.paid_date,w.status,ud.name');

		$this->db->from('withdraw_request w');

		$this->db->join('user_detail ud','w.user_id='.$id);
		
		$this->db->where('w.status','1');
		
		$this->db->where('ud.user_id',$id);
		
		$this->db->where('w.is_with_cron',0);

		$query=$this->db->get();

		return $query->result_array();
	}
	public function get_mails($user_id){

        $this->db->select('md.*, mc.*, ud.name, md.status as mail_status');
        
        $this->db->from('mail_data md');

        $this->db->join('mail_conversation mc','mc.message_id=md.id');

        $this->db->join('user_detail ud','ud.user_id=mc.user_id');
        
        $this->db->where('mc.user_id',$user_id);

        $this->db->where('mc.user_to',$_SESSION['user_id']);

        $this->db->where('mc.direction','OUT');

        $this->db->where('mc.mail_type','friends_mail'); 

        $this->db->where('mc.user_from',$user_id);

        $rows=$this->db->get()->result_array();

        return $rows;
    }
    public function get_challenge_mail($user_id){

        $this->db->select('md.*, mc.*, ud.name, md.status as mail_status');

        $this->db->from('mail_data md');

        $this->db->join('mail_conversation mc','mc.message_id=md.id');

        $this->db->join('user_detail ud','ud.user_id=mc.user_id');
        
        $this->db->where('mc.user_id',$user_id);

        $this->db->where('mc.user_to',$_SESSION['user_id']);

        $this->db->where('mc.direction','OUT');

        $this->db->where('mc.mail_type','challenge_mail'); 

        $this->db->where('mc.user_from',$user_id);

        $rows=$this->db->get()->result_array();

        return $rows;
    }

    public function get_all_mails($user_id){

        $this->db->select('md.*, mc.*, ud.name, md.status as mail_status');

        $this->db->from('mail_data md');

        $this->db->join('mail_conversation mc','mc.message_id=md.id');

        $this->db->join('user_detail ud','ud.user_id=mc.user_to');
        
        $this->db->where('mc.user_id',$user_id);

        $this->db->where('mc.mail_type','friends_mail'); 

        $this->db->where('mc.direction','IN');

        $this->db->order_by('md.id','desc');

        $rows=$this->db->get()->result_array();

        return $rows;
    }

     public function get_challenge_all_mails($user_id){

        $this->db->select('md.*, mc.*, ud.name, md.status as mail_status');
        
        $this->db->from('mail_data md');

        $this->db->join('mail_conversation mc','mc.message_id=md.id');

        $this->db->join('user_detail ud','ud.user_id=mc.user_to');
        
        $this->db->where('mc.user_id',$user_id);

        $this->db->where('mc.mail_type','challenge_mail'); 

        $this->db->where('mc.direction','IN');

        $this->db->order_by('md.id','desc');

        $rows=$this->db->get()->result_array();

        return $rows;
    }

  
    public function get_reply_mails($user_id){
        $this->db->select('*');

        $this->db->from('mail_reply_data mrd');

        $this->db->join('mail_conversation mc','mc.message_id=mrd.id');
        
        $this->db->where('mc.user_id',$user_id);

        $this->db->where('mc.is_reply','yes');

        $this->db->where('mc.direction','OUT');
            
        $query = $this->db->get()->result();

        foreach ($query as $key => $value) {
            $row_data = '<div class="mail_direction">Sent</div><div class="mail_body">'.$value->mail.'</div>'; 
        }
        return $row_data;
    }

    public function check_mail_categories($user_id){
     
        $this->db->select('mcat.*, ud.*, u.*');

        $this->db->from('mail_categories mcat');

        $this->db->join('user u','u.id=mcat.user_id');
        
        $this->db->join('user_detail ud','ud.user_id=mcat.user_id');
        
        $this->db->where('mcat.user_id',$user_id);

        // $this->db->where('mc.direction','OUT');
            
        $query=$this->db->get();

        return $query->result_array();
    }

   public function send_mail_to_friend($sender,$reciever,$mail_data,$sub)
    {
        $msg_data=$this->message_fun($mail_data);
        $this->load->library('email');

        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        // $this->email->from($sender);
        $this->email->from('YouvsTheWorld@ProEsportsGaming.com');
        $this->email->to($reciever);
        $this->email->subject($sub);
        $this->email->message($msg_data);
        $this->email->send();
    }
    public function message_fun($mail_data)
    {
        if (is_array($mail_data)) {
            $message ='<html>
                <body>
                <table cellpadding="0" cellspacing="0" broder="0" width="600">
                    <tr>
                        <td><img src="'.base_url().'/assets/frontend/images/email/header.png" style="width:100%;"></td>
                    </tr>
                    <tr>
                        <td>
                            <p>'.$mail_data['mails_data'].'</p>
                            <a href='.base_url().'mail/>Click here</a>
                            <p>Thanks,</p>
                            <p>from '.$mail_data['sender_mail_name'].'</p>
                        </td>
                    </tr>
                    <tr>
                        <td><img src="'.base_url().'/assets/frontend/images/email/footer.png" style="width:100%;"></td>
                    </tr>
                </table>
                </body>
                </html>';
        }
        else
        {
            $message ='<html>
                <body>
                <table cellpadding="0" cellspacing="0" broder="0" width="600">
                    <tr>
                        <td><img src="'.base_url().'/assets/frontend/images/email/header.png" style="width:100%;"></td>
                    </tr>
                    <tr>
                        <td>
                            <p>'.$mail_data.'</p>
                            <a href='.base_url().'mail/>Click here</a>
                            <p>Thanks,</p>
                        </td>
                    </tr>
                    <tr>
                        <td><img src="'.base_url().'/assets/frontend/images/email/footer.png" style="width:100%;"></td>
                    </tr>
                </table>
                </body>
                </html>';
            }
            return $message;
    }
    public function country_list() {
        $this->db->select('c.*');
        $this->db->from('country c');
        $this->db->where('c.status','0');
        $rows=$this->db->get()->result_array();
        return $rows;
    }
    public function state_list($country_id = '') {
        $this->db->select('*');
        $this->db->from('state s');
        if ($country_id !='') {
            $this->db->join('country c', 'c.country_id = s.country_id');
            $this->db->where('c.country_id',$country_id);
            $this->db->where('c.status','0');
        }
        $result = $this->db->get()->result();
        return $result;
    }
}
