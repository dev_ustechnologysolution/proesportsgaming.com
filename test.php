<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include BASEPATH . '../application/controllers/My_Controller.php';
class Live_lobby extends My_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('My_model');
        $this->load->model('General_model');
        $this->load->library('pagination');
        $this->load->library(array(
            'form_validation'
        ));
        $this->target_dir = BASEPATH . '../upload/game/';
        $this->pageLimit  = 20;
        $this->check_cover_page_access();
    }
    public function index($type = '', $page = 0)
    {
        $data['challangetDp']=$this->General_model->xbox_game_list();
        $data['game_system_list']=$this->General_model->game_system_list();
        $data['custsysList']= $this->General_model->matches_custsyslist_list();
        $data['playstore_game']=$this->General_model->playstore_game_list();
        $data['systemList']= $this->General_model->matches_system_list();
        $data['gamenameList']= $this->General_model->matches_game_name_list();
        $data['subgamenameList']= $this->General_model->matches_sub_game_name_list();
        $data['gameSize']= $this->General_model->matches_game_size();
        $data['lobby_list']=$this->General_model->lobby_list();
        $data['banner_data']=$this->General_model->banner_order();
        $data['dt']=$this->General_model->view_all_data('flow_of_work','id','asc');
        $data['heading_dt']=$this->General_model->view_all_data('heading','id','asc');
        $content = $this->load->view('lobby/lobby_list.tpl.php', $data, true);
        $this->render($content);
    }
    
    public function addFan($id)
    {
        $this->check_user_page_access();
        $view_data = $this->General_model->view_data('lobby_fans', array(
            'lobby_id' => $id,
            'user_id' => $this->session->userdata('user_id')
        ));
        if (count($view_data) == '0') {
            // $data['get_my_lobby_list']          = $this->General_model->get_my_lobby_list();
            // $total_game_count                   = count($data['get_my_lobby_list']);
            // if ($total_game_count >= 1) {
            //     $this->session->set_flashdata('message_err', "Already Reached Limit of creating or joining of 1 Lobby");
            //     header('location:' . base_url() . 'dashboard');
            // }
            $data['list'] = $this->General_model->lobby_details($id);
            $content      = $this->load->view('lobby/lobby_addFan.tpl.php', $data, true);
            $this->render($content);
        } else {
            header('Location:' . base_url() . 'live_lobby/view_lobby/' . $id);
        }
    }
    public function insertFan()
    {
        $this->check_user_page_access();
        $session_data = array(
            'lobby_id' => $_POST['lobby_id']
        );
        $this->session->set_userdata($session_data);
        
        $fan_data = array(
            'user_id' => $this->session->userdata('user_id'),
            'lobby_id' => $_POST['lobby_id'],
            'fan_tag' => $_POST['fan_device_id'],
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s', time())
        );
        $r        = $this->General_model->insert_data('lobby_fans', $fan_data);
        if ($r) {
            header('Location:' . base_url() . 'live_lobby/view_lobby/' . $_POST['lobby_id']);
        }
    }
    
    public function lobbyAdd()
    {
        if ($this->session->userdata('user_id') == '') {
            header('location:' . base_url() . 'login');
            exit;
        }
        $data['active']                     = 'game';
        $data['list1']                      = $this->General_model->playing_game_list();
        $data['list']                       = $this->General_model->open_challenge_game_list();
        $data['challenge_personal_profile'] = $this->General_model->friend_profile($_GET['personal_challenge_friend_id']);
        $data['get_my_lobby_list']          = $this->General_model->get_my_lobby_list();
        $total_game_count                   = count($data['get_my_lobby_list']);
        if ($total_game_count >= 1) {
            $this->session->set_flashdata('message_err', "Already Reached Limit of creating or joining of 1 Lobby <br> Leave game than create a new game");
            header('location:' . base_url() . 'dashboard');
        }
        $data['game_category'] = $this->General_model->game_system_list();
        $data['cust_game_cat'] = $this->General_model->matches_custsyslist_list();
        $data['banner_data']   = $this->General_model->tab_banner();
        $content               = $this->load->view('lobby/lobby_create.tpl.php', $data, true);
        $this->render($content);
    }
    public function lobbyCreate()
    {
        $this->check_user_page_access();
        if (is_null($this->input->post('device_number', TRUE)) || $this->input->post('device_number', TRUE) == '') {
            $this->session->set_flashdata('required_msg', "Please enter PSN/XBOX/PC ID!");
            header('location:' . base_url() . 'live_lobby/lobbyadd');
        } else {
            $points = $this->General_model->view_data('user_detail', array(
                'user_id' => $_SESSION['user_id']
            ));
            $bet_amount = number_format($this->input->post('bet_amount'),2,".","");
            $data   = array(
                'user_id' => $_SESSION['user_id'],
                'game_category_id' => $this->input->post('game_category', TRUE),
                'game_id' => $this->input->post('game_name', TRUE),
                'sub_game_id' => $this->input->post('subgame_name_id', TRUE),
                'game_type' => $this->input->post('game_type', TRUE),
                'device_id' => $this->input->post('device_number', TRUE),
                'image_id' => $this->input->post('game_image_id', TRUE),
                'price' => $bet_amount,
                'lobby_password' => $this->input->post('lobby_password', TRUE),
                'created_at' => date('Y-m-d'),
                'description' => $this->input->post('description', TRUE)
            ); 
            $game_id = $this->input->post('game_name', TRUE);
            $sgame_id = $this->input->post('subgame_name_id', TRUE);
            if ($_POST['personal_challenge_friend_id'] == 1) {
                $add_game_category_id = $_POST['game_category'];
                $game_type='';
                foreach($_POST['game_type'] as $r=>$w) {
                    $game_type .=$w.'|';
                }
                $add_admin_game = array(
                    'game_name'=>$_POST['game_name'],
                    'game_description'=>$_POST['game_name'],
                    'game_category_id'=>$add_game_category_id,
                    'created_at'=>date('Y-m-d h:i:s'),
                    'game_type'=>$game_type,
                    'custom'=>'yes'
                );
                $add_admin_game_id = $this->General_model->insert_data('admin_game',$add_admin_game);

                $add_admin_sub_game = array(
                    'game_id'=>$add_admin_game_id,
                    'sub_game_name'=>$_POST['subgame_name_id'],
                    'custom'=>'yes'
                );
                $add_admin_sub_game_id = $this->General_model->insert_data('admin_sub_game',$add_admin_sub_game);

                $img='';
                if(isset($_FILES["game_image"]["name"]) && $_FILES["game_image"]["name"]!='') {
                    $img=time().basename($_FILES["game_image"]["name"]);
                    $ext = end((explode(".", $img)));
                    if($ext=='gif' || $ext=='jpeg' || $ext=='png' || $ext=='jpg' || $ext=='GIF' || $ext=='JPEG' || $ext=='PNG' || $ext=='JPG') {
                        $target_file = $this->target_dir.$img;
                        move_uploaded_file($_FILES["game_image"]["tmp_name"], $target_file);
                        $add_admin_game_image = array(
                            'game_id'=>$add_admin_game_id,
                            'game_image'=>$img,
                            'custom'=>'yes'
                        );  
                        $add_admin_game_image_id = $this->General_model->insert_data('game_image',$add_admin_game_image);
                    }
                }
                $game_id = $add_admin_game_id;
                $sgame_id = $add_admin_sub_game_id;
                $data   = array(
                    'user_id' => $_SESSION['user_id'],
                    'game_category_id' => $add_game_category_id,
                    'game_id' => $add_admin_game_id,
                    'sub_game_id' => $add_admin_sub_game_id,
                    'game_type' => $this->input->post('game_type', TRUE),
                    'device_id' => $this->input->post('device_number', TRUE),
                    'image_id' => $add_admin_game_image_id,
                    'price' => $bet_amount,
                    'lobby_password' => $this->input->post('lobby_password', TRUE),
                    'created_at' => date('Y-m-d'),
                    'is_custom' => 'yes',
                    'description' => $this->input->post('description', TRUE)
                );    
            }

            $lobby_creat_fee = $this->General_model->view_all_data('admin_lobby_fee', 'id', 'asc');
            $fee_for_creating_game = $lobby_creat_fee[0]['fee_for_creating_game'];
            $price_add_decimal = number_format((float) $fee_for_creating_game, 2, '.', '');
            if ((float) $price_add_decimal > (float) $points[0]['total_balance']) {
                $this->session->set_flashdata('msg', "You do not have enough credits to place a challenge. Please Proceed to Purchase tab for more.");
                header('location:' . base_url() . 'live_lobby/lobbyadd');
            } else {
                $rem_points  = $points[0]['total_balance'] - $price_add_decimal;
                $update_data = array(
                    'total_balance' => number_format((float) $rem_points, 2, '.', '')
                );
                $this->General_model->update_data('user_detail', $update_data, array(
                    'user_id' => $this->session->userdata('user_id')
                ));
                $r = $this->General_model->insert_data('lobby', $data);
                if ($r) {
                    $data['gameImage'] = $this->General_model->view_data('game_image', array(
                        'id' => $this->input->post('game_image_id', TRUE)
                    ));
                    $data['game_name'] = $this->General_model->view_data('admin_game', array(
                        'id' => $this->input->post('game_name', TRUE)
                    ));
                    $session_data      = array(
                        'game_name' => $data['game_name'][0]['game_name'],
                        'game_image' => $data['gameImage'][0]['game_image'],
                        'game_price' => $this->input->post('bet_amount', TRUE),
                        'game_id' => $game_id,
                        'sgame_id' => $sgame_id,
                    );
                    $this->session->set_userdata($session_data);
                    $this->session->set_userdata('message_succ', 'Successfully added');
                    
                    $bet_data = array(
                        'lobby_id' => $r,
                        'game_id' => $this->session->userdata('game_id'),
                        'game_sub_id' => $this->session->userdata('sgame_id'),
                        'bet_status' => 1,
                        'is_active' => 1,
                        'challenger_table' => 'LEFT',
                        'accepter_table' => 'RIGHT'
                    );
                    $bet_id   = $this->General_model->insert_data('group_bet', $bet_data);
                    
                    $condition   = "1 AND `game_id`='" . $this->session->userdata('game_id') . "' AND (`status`=1 OR `status`=0)";
                    $arr_game_id = $this->General_model->view_data('lobby', $condition);
                    $dt          = array(
                        'payment_status' => 1,
                        'status' => 1
                    );
                    $lb          = $this->General_model->update_data('lobby', $dt, array(
                        'id' => $r
                    ));
                    if ($lb) {
                        $lby_grp_data = array(
                            'group_bet_id' => $bet_id,
                            'table_cat' => 'LEFT',
                            'user_id' => $_SESSION['user_id'],
                            'status' => 1,
                            'is_creator' => 'yes',
                            'created_at' => date('Y-m-d H:i:s', time())
                        );
                        $lby_insert   = $this->General_model->insert_data('lobby_group', $lby_grp_data);
                        
                        if ($lby_insert) {
                            $lby_fan_data = array(
                                'user_id' => $_SESSION['user_id'],
                                'lobby_id' => $r,
                                'fan_tag' => $this->input->post('device_number', TRUE),
                                'status' => 1,
                                'created_at' => date('Y-m-d H:i:s', time())
                            );
                            $this->General_model->insert_data('lobby_fans', $lby_fan_data);
                        }
                        
                    }
                    
                    header('location:' . base_url() . 'live_lobby/view_lobby/'.$r);
                } else {
                    $this->session->set_userdata('message_err', 'Failure to add');
                    header('location:' . base_url() . 'live_lobby/lobbyadd');
                }
            }
        }
    }
    
    public function watchLobby($id)
    {
        if ($this->session->userdata('user_id') != '') {
            redirect(base_url() . 'live_lobby/addFan/' . $id);
        } else {
            redirect(base_url() . 'login');
        }
    }
    
    // pagination function
    public function page($page_id)
    {
        $data['dt']       = 1;
        // get all game under category xbox
        $offset           = 5;
        $start            = ($page_id * $offset) - $offset;
        $condition        = "1 AND game_category_id='1' LIMIT " . $start . "," . $offset;
        $data['gameList'] = $this->General_model->admin_Matches_list($condition);
        //$data['gameList']=$this->General_model->view_data('admin_game',$condition);
        // GET TOTAL NO OF ROWS
        
        $tot_game        = $this->General_model->get_num_rows('game', array(
            'game_category_id' => 1
        ));
        // CALCULATE NO OF PAGES
        $tot_page        = ceil($tot_game / $offset);
        $data['totGame'] = $tot_game;
        $data['totPage'] = $tot_page;
        // GET ALL CATEGORY LIST
        
        $data['catList'] = $this->General_model->xbox_game_list();
        $content         = $this->load->view('matches/matches.tpl.php', $data, true);
        $this->render($content);
    }
    
    public function view_lobby($id)
    {
        $this->check_user_page_access();
        $view_lby_det = $this->General_model->view_data('lobby', array(
            'id' => $id,
            'status' => 1
        ));
        if(count($view_lby_det)>0){
            $view_data = $this->General_model->view_data('lobby_fans', array(
                'lobby_id' => $id,
                'user_id' => $this->session->userdata('user_id')
            ));
            if (count($view_data) > 0) {
                $data['lobby_game_bet_left_grp']   = $this->General_model->lobby_game_bet_left_grp($id);
                $data['lobby_game_bet_right_grp']  = $this->General_model->lobby_game_bet_right_grp($id);
                $data['fanscount']                 = $this->General_model->fanscount($id);
                $data['get_grp_for_me']            = $this->General_model->get_grp_for_me($id);
                $data['get_play_status_lobby_bet'] = $this->General_model->get_play_status_lobby_bet($id);
                $data['get_freport']               = $this->General_model->get_first_report_dtime($id);
                $data['lobby_bet']                 = $this->General_model->lobby_bet_detail($id);
                $data['lobby_ads']                 = $this->General_model->get_lobby_ads_detail($id);
                $data['check_bet_reported_for_me'] = $this->General_model->check_bet_reported_for_me($id);
                $data['lobby_chat']                = $this->General_model->lobby_chat($id);
                $data['check_reported']            = $this->General_model->check_bet_reported($id);
                $data['get_two_channel']           = $this->General_model->get_two_stream_channel($id);
                $data['lobby_creator']             = $this->General_model->lobby_creator($id);
                $data['get_my_fan_tag']             = $this->General_model->get_my_fan_tag($id, $_SESSION['user_id']);
                $data['stream_chat_expire']        = $this->General_model->stream_chat_expire($data['lobby_game_bet_left_grp'],$data['lobby_game_bet_right_grp']);
                $points                            = $this->General_model->view_data('user_detail', array(
                    'user_id' => $this->session->userdata('user_id')
                ));
                $data['total_balance']             = $points[0]['total_balance'];
                if ($data['lobby_bet'][0]['game_type'] == count($data['lobby_game_bet_left_grp'])) {
                    $data['full_table']      = 'LEFT';
                    $data['remaining_table'] = 'RIGHT';
                }
                if ($data['lobby_bet'][0]['game_type'] == count($data['lobby_game_bet_right_grp'])) {
                    $data['full_table']      = 'RIGHT';
                    $data['remaining_table'] = 'LEFT';
                }
                if ($data['lobby_bet'][0]['game_type'] == count($data['lobby_game_bet_left_grp']) && $data['lobby_bet'][0]['game_type'] == count($data['lobby_game_bet_right_grp'])) {
                    $data['remaining_table'] = 'FULL';
                }
                $content = $this->load->view('lobby/view_lobby.tpl.php', $data, true);
                $this->render($content);
            } else {
                header('location:' . base_url() . 'live_lobby/addFan/' . $id);
            }
        }
        else{
                header('location:' . base_url());
                exit();
        }
    }
    public function view_fanslist($id)
    {
        $this->check_user_page_access();

        $data['view_lby_det'] = $this->General_model->view_data('lobby', array(
            'id' => $id,
            'status' => 1
        ));
        if(count($data['view_lby_det'])>0){
            $view_data = $this->General_model->view_data('lobby_fans', array(
                'lobby_id' => $id,
                'user_id' => $this->session->userdata('user_id')
            ));
            if (count($view_data) > 0) {
                $options             = array(
                'select' => 'lf.user_id, lf.id,lf.fan_tag,lf.lobby_id,lf.status,ud.name,ud.image,l.status as lobby_status',
                'table' => 'lobby_fans lf',
                'join' => array(
                'user_detail ud' => 'ud.user_id = lf.user_id',
                'lobby l' => 'l.id = lf.lobby_id',
                ),
                    'where' => array(
                        'lf.lobby_id' => $id,
                        'l.status' => 1
                    )
                );
                $data['view_lobby_fans_list'] = $this->General_model->commonGet($options);

                $options             = array(
                'select' => 'ud.name,ud.user_id',
                'table' => 'user_detail ud',
                'where' => array(
                    'ud.user_id' => $_SESSION['user_id']
                ),
                'single' => true
                );
                $data['my_data'] = $this->General_model->commonGet($options);

                $data['fans_id_list'] = $this->General_model->view_data('lobby_fans', array(
                    'lobby_id' => $id
                ));
                $data['ordinal_lobby_id'] = $this->General_model->ordinal($id);
                $content = $this->load->view('lobby/view_lobby_fanslist.tpl.php', $data, true);
                $this->render($content);
            }
             else {
                header('location:' . base_url() . 'live_lobby/addFan/' . $id);
            }
        }
        else{
                header('location:' . base_url());
                exit();
        }
    }
    public function lobby_fan_search(){
        $this->check_user_page_access();
        $fans_id = $_POST['fans_id'];
        $fan_tag = $_POST['fan_tag'];
        $srch_fan = $_POST['srch_fan'];
        $lobby_id = $_POST['lobby_id'];
        $this->db->select('lf.user_id, lf.id,lf.fan_tag,lf.lobby_id,lf.status,ud.name,ud.image,l.status as lobby_status');
        $this->db->from('lobby_fans lf');
        $this->db->join('user_detail ud','ud.user_id = lf.user_id');
        $this->db->join('lobby l','l.id = lf.lobby_id');
        if (isset($fans_id) && $fans_id != '') {
            $this->db->where('lf.id',$fans_id);
        }
        if (isset($fan_tag) && $fan_tag != '') {
            $this->db->where('lf.fan_tag',$fan_tag);
        }
        if (isset($srch_fan) && $srch_fan != '') {
           $this->db->where('lf.id LIKE "%'.$srch_fan.'%" OR lf.fan_tag LIKE "%'.$srch_fan.'%"');
        }
        $this->db->where('lf.lobby_id',$lobby_id);
        $this->db->where('l.status',1);
        $this->db->order_by('lf.id','desc');
        $query=$this->db->get();
        $data['view_lobby_fans_list'] = $query->result_array();
        echo json_encode($data);
        exit();
    }
    public function view_fansprofile($id,$fans_id) {
        $this->check_user_page_access();
        $data['view_lby_det'] = $this->General_model->view_data('lobby', array(
            'id' => $id,
            'status' => 1
        ));
        if(count($data['view_lby_det'])>0){
            $view_data = $this->General_model->view_data('lobby_fans', array(
                'lobby_id' => $id,
                'user_id' => $this->session->userdata('user_id')
            ));
            if (count($view_data) > 0) {
                $checkcountry = $this->General_model->view_data('user_detail', array('user_id' => $fans_id));
                if (is_numeric($checkcountry[0]['country'])) {
                    $options             = array(
                    'select' => 'lf.id,lf.fan_tag,lf.lobby_id,lf.status,ud.*,u.*,ud.image,l.status as lobby_status, c.sortname as country, s.statename as state',
                    'table' => 'lobby_fans lf',
                    'join' => array(
                    'user_detail ud' => 'ud.user_id = lf.user_id',
                    'user u' => 'u.id = lf.user_id',
                    'lobby l' => 'l.id = lf.lobby_id',
                    'country c' => 'c.country_id = ud.country',
                    'state s' => 's.country_id = c.country_id',
                    ),
                        'where' => array(
                            'lf.lobby_id' => $id,
                            'lf.user_id' => $fans_id, 
                            'l.status' => 1
                        )
                    );
                }
                else{
                    $options             = array(
                    'select' => 'lf.id,lf.fan_tag,lf.lobby_id,lf.status,ud.*,u.*,ud.image,l.status as lobby_status',
                    'table' => 'lobby_fans lf',
                    'join' => array(
                    'user_detail ud' => 'ud.user_id = lf.user_id',
                    'user u' => 'u.id = lf.user_id',
                    'lobby l' => 'l.id = lf.lobby_id',
                    ),
                        'where' => array(
                            'lf.lobby_id' => $id,
                            'lf.user_id' => $fans_id, 
                            'l.status' => 1
                        )
                    );
                }
                $data['view_lobby_fan_profile'] = $this->General_model->commonGet($options);
                $data['ordinal_lobby_id'] = $this->General_model->ordinal($id);
                $content = $this->load->view('lobby/view_fansprofile.tpl.php', $data, true);
                $this->render($content);
            }
             else {
                header('location:' . base_url() . 'live_lobby/addFan/' . $id);
            }
        }
        else{
                header('location:' . base_url());
                exit();
        }
    }
    public function lobby_join() {
        $this->check_user_page_access();
        $join_lobby_data          = array(
            'group_bet_id' => $_POST['group_bet_id'],
            'table_cat' => $_POST['table_cat'],
            'user_id' => $this->session->userdata('user_id'),
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s', time())
        );
        $join_lobby_data_inserted = $this->General_model->insert_data('lobby_group', $join_lobby_data);
        $res['get_join_detail']   = $this->General_model->get_join_detail($_POST['group_bet_id'], $_POST['table_cat']);
        if (isset($join_lobby_data_inserted)) {
            echo json_encode($res);
        }
    }
    public function change_lobby_password() {
        $this->check_user_page_access();
        if ($_POST['lobby_id'] != '') {
            $lobby_id = $_POST['lobby_id'];
            $lobby_password = $_POST['lobby_password'];
            $update_data_where = array('id' => $lobby_id);
            $update_row = array(
                'lobby_password' => $lobby_password
            );
            $res['update'] = $this->General_model->update_data('lobby', $update_row, $update_data_where);
            if ($res) {
                echo json_encode($res);
                exit();
            }
        }
    }
    public function change_lobby_tag() {
        $this->check_user_page_access();
        if ($_POST['lobby_id'] != '') {
            $lobby_id = $_POST['lobby_id'];
            $lobby_tag = $_POST['lobby_tag'];
            $update_data_where = array('lobby_id' => $lobby_id, 'user_id' => $_SESSION['user_id']);
            $update_row = array('fan_tag' => $lobby_tag);
            $res['update'] = $this->General_model->update_data('lobby_fans', $update_row, $update_data_where);
            if ($res) {
                echo json_encode($res);
                exit();
            }
        }
    }
    public function challenge_payment() {
        $this->check_user_page_access();
        $group_bet_id = $_POST['group_bet_id'];
        
        $points = $this->General_model->view_data('user_detail', array(
            'user_id' => $_SESSION['user_id']
        ));
        $tax_price = $_POST['tax_price'];
        $rem_points  = $points[0]['total_balance'] - $tax_price;
        $update_data = array(
            'total_balance' => number_format((float) $rem_points, 2, '.', '')
        );
        
        $this->General_model->update_data('user_detail', $update_data, array(
            'user_id' => $this->session->userdata('user_id')
        ));
        
        $lobby_payment_update_data = array(
            'group_bet_id' => $group_bet_id,
            'user_id' => $_SESSION['user_id'],
            'status' => 1
        );
        $r = $this->General_model->update_data('lobby_group', array('play_status' => 1), $lobby_payment_update_data);
        if ($r) {
           $options             = array(
                'select' => 'lg.*,ud.total_balance',
                'table' => 'lobby_group lg',
                'join' => array(
                    'user_detail ud' => 'ud.user_id = lg.user_id'
                ),
                'where' => array(
                    'lg.group_bet_id' => $group_bet_id,
                    'lg.user_id' => $_SESSION['user_id'],
                    'lg.play_status' => 1,
                    'lg.status' => 1
                )
            );

           $srcData['lobby_payment_update_data'] = $this->General_model->commonGet($options);
           echo json_encode($srcData);
           exit();
        }
    }
    public function group_bet_report() {
        $this->check_user_page_access();
        $group_bet_id             = $_POST['group_bet_id'];
        $lobby_report_update_data = array(
            'group_bet_id' => $group_bet_id,
            'user_id' => $_SESSION['user_id'],
            'status' => 1
        );
        $r                        = $this->General_model->update_data('lobby_group', array(
            'reported' => 1
        ), $lobby_report_update_data);
    }
    
    public function get_playedgrp_data() {
        $group_bet_id                  = $_POST['group_bet_id'];
        $lobby_id                      = $_POST['lobby_id'];
        $playedgrp_data                = $this->General_model->view_data('lobby_group', array(
            'group_bet_id' => $_POST['group_bet_id'],
            'play_status' => 1
        ));
        $lobby_bet                     = $this->General_model->lobby_bet_detail($lobby_id);
        $srcData['get_lobby_size']     = $lobby_bet[0]['game_type'] * 2;
        $srcData['get_playedgrp_data'] = $playedgrp_data;
        $srcData['lobby_creator']      = $this->General_model->lobby_creator($lobby_id);
        $srcData['get_lefttop_played_id']  = $this->General_model->get_lefttop_played_id($group_bet_id, $lobby_id);
        $srcData['get_righttop_played_id'] = $this->General_model->get_righttop_played_id($group_bet_id, $lobby_id);
        
        $srcData['user_id']                  = $_SESSION['user_id'];
        $srcData['get_playedgrp_data_count'] = count($playedgrp_data);
        echo json_encode($srcData);
        exit();
    }
    public function report_lobbybet() {
        $lobby_id        = $_POST['lobby_id'];
        $group_bet_id    = $_POST['group_bet_id'];
        $user_id         = $_POST['user_id'];
        $video           = $_POST['video'];
        $accept_risk     = $_POST['accept_risk'];
        $win_lose_status = $_POST['win_lose_status'];
        $explain_admin   = $_POST['explain_admin'];
        $data = $this->General_model->lobby_report_timer();
        $fivmininc = date('Y-m-d H:i:s',strtotime("+".$data->hours." hours +".$data->minutes." minutes",time()));
        $get_my_table = $this->General_model->get_my_lobby_table($lobby_id,$group_bet_id,$user_id);
        $check_lobby_group_user_won_lose = $this->General_model->check_lobby_group_user_won_lose($group_bet_id);
        if (!empty($check_lobby_group_user_won_lose)) {
            $fivmininc = date('Y-m-d H:i:s', time());
        }
        $this->db->where('group_bet_id', $group_bet_id)->where('user_id', $user_id)->where('status', 1);
        $this->db->update('lobby_group', array(
            'reported' => 1,
            'win_lose_status' => $win_lose_status,
            'updated_at' => $fivmininc
        ));
        if (isset($group_bet_id)) {
            if ($win_lose_status == 'win') {
                $status_result = '1';
            } else if ($win_lose_status == 'lose') {
                $status_result = '0';
            } else if ($win_lose_status == 'admin') {
                $status_result = '2';
            }
            $lobby_bet = $this->General_model->lobby_bet_detail($lobby_id);
            $video_id = $check_lobby_group_user_won_lose->video_id;
            if ($win_lose_status == 'lose') {
                if (!empty($check_lobby_group_user_won_lose)) {
                    if ($status_result == '0' && $check_lobby_group_user_won_lose->win_lose_status == 'win') {
                        $winner_table = $check_lobby_group_user_won_lose->table_cat;
                        $loser_table  = 'RIGHT';
                        if ($winner_table == 'RIGHT') {
                            $loser_table = 'LEFT';
                        }
                        $dt_data = array(
                            'bet_status' => 3,
                            'winner_table' => $winner_table,
                            'loser_table' => $loser_table,
                            'video_id' => $video_id,
                            'win_lose_deta' => date('Y-m-d H:i:s', time()),
                            'is_active' => 0
                        );
                        $this->General_model->update_data('group_bet', $dt_data, array(
                            'id' => $group_bet_id
                        ));
                        $lobby_bet_winner_list = $this->General_model->view_data('lobby_group', array(
                            'table_cat' => $winner_table,
                            'group_bet_id' => $group_bet_id,
                            'status' => 1
                        ));
                        foreach ($lobby_bet_winner_list as $key => $list) {
                            $user_data_where = array(
                                'user_id' => $list['user_id']
                            );
                            $userListDp      = $this->General_model->view_data('user_detail', $user_data_where);
                            foreach ($userListDp as $key => $value) {
                                $update_user = array(
                                    'total_balance' => $value['total_balance'] + (2 * $lobby_bet[0]['price'])
                                );
                                $this->General_model->update_data('user_detail', $update_user, $user_data_where);
                            }
                        }
                        
                        $this->db->select('gb.*');
                        $this->db->from('group_bet gb');
                        $this->db->where('gb.id', $group_bet_id);
                        $this->db->where('gb.lobby_id', $lobby_id);
                        $this->db->where('gb.bet_status', 3);
                        $query               = $this->db->get();
                        $get_duplicate_data  = $query->row();
                        $get_data            = array(
                            'lobby_id' => $get_duplicate_data->lobby_id,
                            'game_id' => $get_duplicate_data->game_id,
                            'game_sub_id' => $get_duplicate_data->game_sub_id,
                            'challenger_table' => $get_duplicate_data->challenger_table,
                            'accepter_table' => $get_duplicate_data->accepter_table,
                            'winner_table' => '',
                            'loser_table' => '',
                            'bet_status' => 1,
                            'challenge_date' => $get_duplicate_data->challenge_date,
                            'accepte_date' => '0000-00-00 00:00:00',
                            'video_id' => '',
                            'is_active' => 1
                        );
                        $group_bet_insert_id = $this->General_model->insert_data('group_bet', $get_data);
                        $options             = array(
                            'select' => 'lg.*,ud.total_balance',
                            'table' => 'user_detail ud',
                            'join' => array(
                                'lobby_group lg' => 'lg.user_id = ud.user_id'
                            ),
                            'where' => array(
                                'lg.group_bet_id' => $group_bet_id,
                                'lg.status' => 1
                            )
                        );
                        $lobby_bet_users     = $this->General_model->commonGet($options);
                        
                        foreach ($lobby_bet_users as $key => $lbusers) {
                            if ($lbusers->total_balance >= $lobby_bet[0]['price'] && $lbusers->total_balance != '0.00' && $lbusers->total_balance != '0') {
                                $join_lobby_data = array(
                                    'group_bet_id' => $group_bet_insert_id,
                                    'table_cat' => $lbusers->table_cat,
                                    'user_id' => $lbusers->user_id,
                                    'is_creator' => $lbusers->is_creator,
                                    'stream_status' => $lbusers->stream_status,
                                    'stream_channel' => $lbusers->stream_channel,
                                    'status' => 1,
                                    'created_at' => date('Y-m-d H:i:s', time())
                                );
                                $this->General_model->insert_data('lobby_group', $join_lobby_data);
                            }
                        }
                    }
                    $ref_id = 1;
                } else {
                    $ref_id = 0;
                }
                $data = array(
                    'lobby_id' => $lobby_id,
                    'grp_bet_id' => $group_bet_id,
                    'video_url' => $video . '&&' . $user_id,
                    'user_id' => $user_id,
                    'table_cat' => $get_my_table,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'status' => $status_result
                );
                $r    = $this->General_model->insert_data('lobby_video', $data);
            } else if ($win_lose_status == 'admin') {
                if (!empty($check_lobby_group_user_won_lose)) {
                    if ($status_result == '2' && $check_lobby_group_user_won_lose->win_lose_status == 'lose' || $check_lobby_group_user_won_lose->win_lose_status == 'win') {
                        $dt = array(
                            'bet_status' => 2,
                            'video_id' => '',
                            'is_active' => 0
                        );
                        
                        $this->General_model->update_data('group_bet', $dt, array(
                            'id' => $group_bet_id
                        ));
                        $this->db->select('gb.*');
                        $this->db->from('group_bet gb');
                        $this->db->where('gb.id', $group_bet_id);
                        $this->db->where('gb.lobby_id', $lobby_id);
                        $this->db->where('gb.bet_status', 2);
                        $this->db->where('gb.is_active', 0);
                        $query              = $this->db->get();
                        $get_duplicate_data = $query->row();
                        
                        $get_data            = array(
                            'lobby_id' => $get_duplicate_data->lobby_id,
                            'game_id' => $get_duplicate_data->game_id,
                            'game_sub_id' => $get_duplicate_data->game_sub_id,
                            'challenger_table' => $get_duplicate_data->challenger_table,
                            'accepter_table' => $get_duplicate_data->accepter_table,
                            'winner_table' => '',
                            'loser_table' => '',
                            'bet_status' => 1,
                            'challenge_date' => $get_duplicate_data->challenge_date,
                            'accepte_date' => '0000-00-00 00:00:00',
                            'video_id' => '',
                            'is_active' => 1
                        );
                        $group_bet_insert_id = $this->General_model->insert_data('group_bet', $get_data);
                        $options             = array(
                            'select' => 'lg.*,ud.total_balance',
                            'table' => 'user_detail ud',
                            'join' => array(
                                'lobby_group lg' => 'lg.user_id = ud.user_id'
                            ),
                            'where' => array(
                                'lg.group_bet_id' => $group_bet_id,
                                'lg.status' => 1
                            )
                        );
                        $lobby_bet_users     = $this->General_model->commonGet($options);
                        
                        foreach ($lobby_bet_users as $key => $lbusers) {
                            if ($lbusers->total_balance >= $lobby_bet[0]['price'] && $lbusers->total_balance != '0.00' && $lbusers->total_balance != '0') {
                                $join_lobby_data = array(
                                    'group_bet_id' => $group_bet_insert_id,
                                    'table_cat' => $lbusers->table_cat,
                                    'user_id' => $lbusers->user_id,
                                    'is_creator' => $lbusers->is_creator,
                                    'stream_status' => $lbusers->stream_status,
                                    'stream_channel' => $lbusers->stream_channel,
                                    'status' => 1,
                                    'created_at' => date('Y-m-d H:i:s', time())
                                );
                                $this->General_model->insert_data('lobby_group', $join_lobby_data);
                            }
                        }
                    }
                    $ref_id = 1;
                } else {
                    $ref_id = 0;
                }
                $data = array(
                    'lobby_id' => $lobby_id,
                    'grp_bet_id' => $group_bet_id,
                    'video_url' => $video . '&&' . $user_id,
                    'detail_admin' => $explain_admin,
                    'user_id' => $user_id,
                    'table_cat' => $get_my_table,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'status' => $status_result
                );
                $r    = $this->General_model->insert_data('lobby_video', $data);
            } else if ($win_lose_status == 'win') {
                if (!empty($check_lobby_group_user_won_lose)) {
                    if ($status_result == '1' && $check_lobby_group_user_won_lose->win_lose_status == 'lose') {
                        $winner_table = $check_lobby_group_user_won_lose->table_cat;
                        $loser_table  = 'RIGHT';
                        if ($winner_table == 'RIGHT') {
                            $loser_table = 'LEFT';
                        }
                        $dt = array(
                            'bet_status' => 3,
                            'winner_table' => $loser_table,
                            'loser_table' => $winner_table,
                            'video_id' => '',
                            'is_active' => 0
                        );
                        $this->General_model->update_data('group_bet', $dt, array(
                            'id' => $group_bet_id
                        ));
                        $lobby_bet_winner_list = $this->General_model->view_data('lobby_group', array(
                            'table_cat' => $loser_table,
                            'group_bet_id' => $group_bet_id,
                            'status' => 1
                        ));
                        foreach ($lobby_bet_winner_list as $key => $list) {
                            $user_data_where = array(
                                'user_id' => $list['user_id']
                            );
                            $userListDp      = $this->General_model->view_data('user_detail', $user_data_where);
                            foreach ($userListDp as $key => $value) {
                                $update_user = array(
                                    'total_balance' => $value['total_balance'] + (2 * $lobby_bet[0]['price'])
                                );
                                $this->General_model->update_data('user_detail', $update_user, $user_data_where);
                            }
                        }
                        
                        $this->db->select('gb.*');
                        $this->db->from('group_bet gb');
                        $this->db->where('gb.id', $group_bet_id);
                        $this->db->where('gb.lobby_id', $lobby_id);
                        $this->db->where('gb.bet_status', 3);
                        $query              = $this->db->get();
                        $get_duplicate_data = $query->row();
                        
                        $get_data            = array(
                            'lobby_id' => $get_duplicate_data->lobby_id,
                            'game_id' => $get_duplicate_data->game_id,
                            'game_sub_id' => $get_duplicate_data->game_sub_id,
                            'challenger_table' => $get_duplicate_data->challenger_table,
                            'accepter_table' => $get_duplicate_data->accepter_table,
                            'winner_table' => '',
                            'loser_table' => '',
                            'bet_status' => 1,
                            'challenge_date' => $get_duplicate_data->challenge_date,
                            'accepte_date' => '0000-00-00 00:00:00',
                            'video_id' => '',
                            'is_active' => 1
                        );
                        $group_bet_insert_id = $this->General_model->insert_data('group_bet', $get_data);
                        $options             = array(
                            'select' => 'lg.*,ud.total_balance',
                            'table' => 'user_detail ud',
                            'join' => array(
                                'lobby_group lg' => 'lg.user_id = ud.user_id'
                            ),
                            'where' => array(
                                'lg.group_bet_id' => $group_bet_id,
                                'lg.status' => 1
                            )
                        );
                        $lobby_bet_users     = $this->General_model->commonGet($options);
                        
                        foreach ($lobby_bet_users as $key => $lbusers) {
                            if ($lbusers->total_balance >= $lobby_bet[0]['price'] && $lbusers->total_balance != '0.00' && $lbusers->total_balance != '0') {
                                $join_lobby_data = array(
                                    'group_bet_id' => $group_bet_insert_id,
                                    'table_cat' => $lbusers->table_cat,
                                    'user_id' => $lbusers->user_id,
                                    'is_creator' => $lbusers->is_creator,
                                    'stream_status' => $lbusers->stream_status,
                                    'stream_channel' => $lbusers->stream_channel,                                    
                                    'status' => 1,
                                    'created_at' => date('Y-m-d H:i:s', time())
                                );
                                $this->General_model->insert_data('lobby_group', $join_lobby_data);
                            }
                        }
                    }
                    $ref_id = 1;
                } else {
                    $ref_id = 0;
                }
                $data = array(
                    'lobby_id' => $lobby_id,
                    'grp_bet_id' => $group_bet_id,
                    'video_url' => $video.'&&'.$user_id,
                    'user_id' => $user_id,
                    'table_cat' => $get_my_table,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'status' => $status_result
                );
                $r    = $this->General_model->insert_data('lobby_video', $data);
            }
        }
        
        $this->db->select('lg.*, gb.lobby_id, ud.name, ud.user_id');
        $this->db->from('lobby_group lg');
        $this->db->join('user_detail ud', 'ud.user_id=lg.user_id');
        $this->db->join('group_bet gb', 'gb.id=lg.group_bet_id');
        $this->db->where('lg.group_bet_id', $group_bet_id);
        $this->db->where('lg.user_id', $user_id);
        $this->db->where('gb.lobby_id', $lobby_id);
        $query                      = $this->db->get();
        $data['get_updated_detail'] = $query->row();
        $data['get_duplicate_data'] = $get_duplicate_data;
        $data['ref_id']             = $ref_id;
        $data['lobby_id']           = $lobby_id;
        echo json_encode($data);
        exit();
    }
    public function report_timer_expire()
    {
        $lobby_id     = $_POST['lobby_id'];
        $group_bet_id = $_POST['group_bet_id'];
        $check_lobby_group_user_won_lose = $this->General_model->check_lobby_group_user_won_lose($group_bet_id);
        print_r($check_lobby_group_user_won_lose);
        echo "exit()";
        if (!empty($check_lobby_group_user_won_lose))
        exit();
        if (isset($lobby_id) && $lobby_id != '' && isset($group_bet_id) && $group_bet_id != '') {
            
            $count_game_bet = $this->General_model->view_data('group_bet', array(
                'id' => $group_bet_id,
                'bet_status' => 2
            ));
            
            if (count($count_game_bet) == 0) {
                $this->db->where('id', $group_bet_id)->where('lobby_id', $lobby_id)->where('bet_status', 1);
                $this->db->update('group_bet', array(
                    'bet_status' => 2,
                    'is_active' => 0
                ));
                
                $this->db->select('gb.*');
                $this->db->from('group_bet gb');
                $this->db->where('gb.id', $group_bet_id);
                $this->db->where('gb.lobby_id', $lobby_id);
                $this->db->where('gb.bet_status', 2);
                $this->db->order_by('gb.id');
                $query               = $this->db->get();
                $query_data          = $query->row();
                $get_data            = array(
                    'lobby_id' => $query_data->lobby_id,
                    'game_id' => $query_data->game_id,
                    'game_sub_id' => $query_data->game_sub_id,
                    'challenger_table' => $query_data->challenger_table,
                    'accepter_table' => $query_data->accepter_table,
                    'winner_table' => '',
                    'loser_table' => '',
                    'bet_status' => 1,
                    'challenge_date' => $query_data->challenge_date,
                    'accepte_date' => '0000-00-00 00:00:00',
                    'video_id' => '',
                    'is_active' => 1
                );
                $group_bet_insert_id = $this->General_model->insert_data('group_bet', $get_data);
                $options             = array(
                    'select' => 'lg.*,ud.total_balance',
                    'table' => 'user_detail ud',
                    'join' => array(
                        'lobby_group lg' => 'lg.user_id = ud.user_id'
                    ),
                    'where' => array(
                        'lg.group_bet_id' => $group_bet_id,
                        'lg.status' => 1
                    )
                );
                $lobby_bet_users     = $this->General_model->commonGet($options);
                
                foreach ($lobby_bet_users as $key => $lbusers) {
                    if ($lbusers->total_balance >= $lobby_bet[0]['price'] && $lbusers->total_balance != '0.00' && $lbusers->total_balance != '0') {
                        $join_lobby_data = array(
                            'group_bet_id' => $group_bet_insert_id,
                            'table_cat' => $lbusers->table_cat,
                            'user_id' => $lbusers->user_id,
                            'is_creator' => $lbusers->is_creator,
                            'stream_status' => $lbusers->stream_status,
                            'stream_channel' => $lbusers->stream_channel,                            
                            'status' => 1,
                            'created_at' => date('Y-m-d H:i:s', time())
                        );
                        $this->General_model->insert_data('lobby_group', $join_lobby_data);
                    }
                }
                $res['get_duplicate_data'] = $query_data;
            }
            $res['lobby_id']       = $lobby_id;
            $res['count_game_bet'] = count($count_game_bet);
            echo json_encode($res);
            exit();
            
        }
    }
    public function send_message_to_lobby_grp()
    {
        $this->check_user_page_access();
        if (isset($_POST)) {
            $insert_data = array(
                'message' => $_POST['message'],
                'message_type' => 'message'
            );
            $message_id  = $this->General_model->insert_data('xwb_messages', $insert_data);
            if ($message_id) {
                $lobby_insert_data           = array(
                    'lobby_id' => $_POST['lobby_id'],
                    'user_id' => $_POST['user_id'],
                    'message_id' => $message_id,
                    'status' => 1,
                    'date' => date('Y-m-d H:i:s', time())
                );
                $lobby_group_conversation_id = $this->General_model->insert_data('lobby_group_conversation', $lobby_insert_data);
                if ($lobby_group_conversation_id) {
                    $options                   = array(
                        'select' => 'lgp.*,xm.message,ud.name',
                        'table' => 'lobby_group_conversation lgp',
                        'join' => array(
                            'xwb_messages xm' => 'xm.id = lgp.message_id',
                            'user_detail ud' => 'ud.user_id = lgp.user_id'
                        ),
                        'where' => array(
                            'lgp.id' => $lobby_group_conversation_id,
                            'lgp.status' => 1
                        )
                    );
                    $srcData['lobby_get_chat'] = $this->General_model->commonGet($options);
                    $fan_tag = $this->General_model->view_single_row('lobby_fans', array('lobby_id' => $_POST['lobby_id'],'user_id' => $_POST['user_id']),'fan_tag');
                    $srcData['fan_tag'] = $fan_tag['fan_tag'];
                    $srcData['session_id']     = $_SESSION['user_id'];
                    echo json_encode($srcData);
                    exit();
                }
            }
            
        }
    }
    public function auth_lobby_password()
    {
        $this->check_user_page_access();
        if (isset($_POST)) {
            $lobby_id     = $_POST['lobby_id'];
            $lobby_password = $_POST['lobby_password'];
            $user_id      = $_SESSION['user_id'];

            $options      = array(
                'select' => 'l.*',
                'table' => 'lobby l',
                'where' => array(
                    'l.id' => $lobby_id,
                    'l.lobby_password' => $lobby_password
                )
            );
            $res['auth_pw'] = $this->General_model->commonGet($options);
            if (count($res['auth_pw']) == 0) {
                $res['password_auth'] = 'auth_failed';
                $_SESSION['lobby_password'] = $lobby_password;
            }
            else{
                $res['password_auth'] = 'auth_success';
            }
            $res['user_id'] = $user_id;
            echo json_encode($res);
            exit();
        }
    }
    public function change_game_size()
    {
        $this->check_user_page_access();
        if (isset($_POST)) {
            $lobby_id     = $_POST['lobby_id'];
            $group_bet_id = $_POST['group_bet_id'];
            $game_size = $_POST['game_size'];
            $res['group_bet_details'] = $this->General_model->view_data('group_bet',array('id'=>$group_bet_id));
            $get_lobby_info = $this->General_model->view_data('lobby',array('id'=>$lobby_id));
            $update_data = array(
                'game_type' => $game_size
            );
            $update_where = array(
                'id'=>$lobby_id
            );
            $game_size_update = $this->General_model->update_data('lobby',$update_data,$update_where);
            if ($game_size_update) {
                $get_price = $get_lobby_info[0]['price'];
                $lobby_creat_fee = $this->General_model->view_all_data('admin_lobby_fee', 'id', 'asc');
                $fee_per_game = $lobby_creat_fee[0]['fee_per_game'];
                $price_add = $get_price + $fee_per_game;
                $price_add_decimal = number_format((float)$price_add, 2, '.', '');
                $options = array(
                'select' => 'lg.user_id, lg.play_status, lg.is_creator',
                'table' => 'group_bet gb',
                'join' => array('lobby_group lg' => 'lg.group_bet_id = gb.id'),
                'where' => array('lg.group_bet_id'=>$group_bet_id,'gb.bet_status' => 1)
                );
                $lobby_bet_get_grp = $this->General_model->commonGet($options);
                foreach ($lobby_bet_get_grp as $key => $lbgp) {
                    if ($lbgp->play_status == 1) {
                        $get_balance = $this->General_model->view_data('user_detail',array('user_id'=>$lbgp->user_id));
                        $update_total_balance = $get_balance[0]['total_balance'] + $price_add_decimal;
                        $update_data = array(
                            'total_balance' =>  $update_total_balance,
                        );
                        $this->General_model->update_data('user_detail',$update_data,array('user_id'=>$lbgp->user_id));
                    }
                    $q_update_data = array(
                        'play_status' => 0,
                        'reported' => 0,
                        'win_lose_status' => '',
                        'updated_at' => '0000-00-00 00:00:00',
                    );                
                    $q_update_data_where = array(
                        'user_id' => $lbgp->user_id,
                        'group_bet_id' => $group_bet_id,
                    );
                    $this->General_model->update_data('lobby_group', $q_update_data, $q_update_data_where);
                }
                for ($i=0; $i < count($lobby_bet_get_grp) ; $i++) { 
                    if ($game_size*2 <= $i) {
                        $del_data = $this->General_model->delete_data('lobby_group', array(
                            'group_bet_id' => $group_bet_id,
                            'user_id' => $lobby_bet_get_grp[$i]->user_id
                        ));
                        if ($lobby_bet_get_grp[$i]->is_creator == 'yes') {
                            $update_creator = 'yes';
                        }
                    }
                    if ($update_creator == 'yes') {
                        $pass_to_arr = array(
                            'is_creator' => 'yes',
                        );
                        $update_pass_where = array(
                            'user_id' => $lobby_bet_get_grp[0]->user_id,
                            'group_bet_id' => $group_bet_id,
                        );
                        $this->General_model->update_data('lobby_group', $pass_to_arr, $update_pass_where);
                    }
                }
                $del_data = $this->General_model->delete_data('lobby_video', array(
                    'grp_bet_id' => $group_bet_id,
                    'lobby_id' => $lobby_id,
                ));
            }
            $res['refresh'] = 'yes';
            echo json_encode($res);
            exit();
        }
    }
    public function get_lbydt_on_leave(){
        $user_id = $_POST['user_id'];
        $this->db->select('lg.*, ud.name, ud.user_id, ud.total_balance');
        $this->db->from('lobby_group lg');
        $this->db->join('user_detail ud','ud.user_id=lg.user_id');
        $this->db->join('group_bet gb','gb.id=lg.group_bet_id');
        $this->db->where('lg.group_bet_id',$_POST['group_bet_id']);
        $this->db->where('gb.lobby_id',$_POST['lobby_id']);
        $this->db->where('gb.is_active',1);
        $query = $this->db->get();
        $srcData['get_detail'] = $query->result_array();

        $this->db->select('l.game_type, l.price');
        $this->db->from('lobby l');
        $this->db->where('l.id',$_POST['lobby_id']);
        $query = $this->db->get();
        $srcData['lobby_game_type']  = $query->result_array();
        
        $lobb_price = $srcData['lobby_game_type'][0]['price'];

        $this->db->select('lg.*');
        $this->db->from('lobby_group lg');
        $this->db->join('group_bet gb','gb.id=lg.group_bet_id');
        $this->db->where('lg.group_bet_id',$_POST['group_bet_id']);
        $this->db->where('gb.lobby_id',$_POST['lobby_id']);
        $this->db->where('gb.is_active',1);
        $query = $this->db->get();
        $count_query = count($query->result_array());
        $srcData['get_lobby_group_play'] = $count_query;

        if ($srcData['get_lobby_group_play'] == 0) {
            $update_data = array(
                'status' => 0
            );
            $update_data_where = array(
                'id' => $_POST['lobby_id']
            );
            $update = $this->General_model->update_data('lobby', $update_data, $update_data_where);
            if ($update) {
                $srcData['lobby_updated'] = 1;
            }
        }

        $this->db->select('lg.*, ud.name, ud.user_id');
        $this->db->from('lobby_group lg');
        $this->db->join('group_bet gb','gb.id=lg.group_bet_id');
        $this->db->join('user_detail ud','ud.user_id=lg.user_id');
        $this->db->where('gb.lobby_id',$_POST['lobby_id']);
        $this->db->where('lg.table_cat','LEFT');
        $this->db->where('lg.group_bet_id',$_POST['group_bet_id']);
        $this->db->where('gb.bet_status',1);
        $lobby_game_bet_left_grp=$this->db->get()->result_array();

        $this->db->select('lg.*, ud.name, ud.user_id');
        $this->db->from('lobby_group lg');
        $this->db->join('group_bet gb','gb.id=lg.group_bet_id');
        $this->db->join('user_detail ud','ud.user_id=lg.user_id');
        $this->db->where('gb.lobby_id',$_POST['lobby_id']);
        $this->db->where('lg.table_cat','RIGHT');
        $this->db->where('lg.group_bet_id',$_POST['group_bet_id']);
        $this->db->where('gb.bet_status',1);
        $lobby_game_bet_right_grp=$this->db->get()->result_array();

        if ($user_id == $_SESSION['streamer_chat_id_set']) {
                unset($_SESSION['lobby_id']);
                unset($_SESSION['streamer_name_set']);
                unset($_SESSION['streamer_chat_id_set']);
                unset($_SESSION['streamer_channel_set']);   
        }

        $del_data       = $this->General_model->delete_data('lobby_video', array(
            'grp_bet_id' => $_POST['lobby_id'],
            'lobby_id' => $_POST['group_bet_id'],
        ));
        if ($srcData['lobby_game_type'][0]['game_type']*2 != count($lobby_game_bet_left_grp)+count($lobby_game_bet_right_grp)) {
            if ($srcData['lobby_game_type'][0]['game_type'] == count($lobby_game_bet_left_grp)) {
                $srcData['full_table']='LEFT';
                $srcData['remaining_table'] = 'RIGHT';
            }
            if ($srcData['lobby_game_type'][0]['game_type'] == count($lobby_game_bet_right_grp)) {
                    $srcData['full_table']='RIGHT';
                    $srcData['remaining_table'] = 'LEFT';
            }
        } else {
                $srcData['remaining_table'] = 'FULL';
                $srcData['full_table']='ALL';
        }

        $this->db->select('lg.*, ud.name, ud.user_id, ud.total_balance');
        $this->db->from('lobby_group lg');
        $this->db->join('user_detail ud','ud.user_id=lg.user_id');
        $this->db->join('group_bet gb','gb.id=lg.group_bet_id');
        $this->db->where('lg.group_bet_id',$_POST['group_bet_id']);
        $this->db->where('gb.lobby_id',$_POST['lobby_id']);
        $this->db->where('gb.is_active',1);
        $query = $this->db->get();
        $srcData['get_updated_detail'] = $query->result_array();
        $srcData['lobby_creator'] = $this->General_model->lobby_creator($_POST['lobby_id']);
        $srcData['usertable_uid'] = $_SESSION['user_id'];
        $srcData['csrf_key'] = $this->security->get_csrf_hash();
        echo json_encode($srcData);
        exit();
    }
    public function leave_lobby() {
        $this->check_user_page_access();
        if (isset($_POST)) {
            $lobby_id     = $_POST['lobby_id'];
            $group_bet_id = $_POST['group_bet_id'];
            $user_id      = $_POST['leave_id'];
            $creator      = $_POST['creator'];
            $options      = array(
                'select' => 'lg.*, ud.total_balance',
                'table' => 'lobby_group lg',
                'join' => array(
                    'user_detail ud' => 'ud.user_id = lg.user_id',
                ),
                'where' => array(
                    'lg.group_bet_id' => $_POST['group_bet_id'],
                    'lg.user_id' => $user_id,
                    'lg.status' => 1
                ),
                'single'=>true
            );
            $res['lobby_leaved_users'] = $this->General_model->commonGet($options);
            $res['user_id'] = $user_id;
            $group_bet_details = $this->General_model->view_data('group_bet',array('id'=>$group_bet_id));
            $get_lobby_info = $this->General_model->view_data('lobby',array('id'=>$lobby_id));
            $get_price = $get_lobby_info[0]['price'];

            $lobby_creat_fee = $this->General_model->view_all_data('admin_lobby_fee', 'id', 'asc');
            $fee_per_game = $lobby_creat_fee[0]['fee_per_game'];
            $price_add = $get_price + $fee_per_game;
            $price_add_decimal = number_format((float)$price_add, 2, '.', '');
            $options = array(
                'select' => 'lg.user_id, lg.play_status',
                'table' => 'group_bet gb',
                'join' => array('lobby_group lg' => 'lg.group_bet_id = gb.id'),
                'where' => array('lg.group_bet_id'=>$group_bet_id,'gb.bet_status' => 1)
            );
            $lobby_bet_get_grp = $this->General_model->commonGet($options);
            foreach ($lobby_bet_get_grp as $key => $lbgp) {
                $get_balance = $this->General_model->view_data('user_detail',array('user_id'=>$lbgp->user_id));
                $update_total_balance = $get_balance[0]['total_balance'];
                if ($lbgp->play_status == 1) {
                    $update_total_balance = $get_balance[0]['total_balance'] + $price_add_decimal;
                    $update_data = array(
                        'total_balance' => number_format((float)$update_total_balance, 2, '.', ''),
                    );
                    $this->General_model->update_data('user_detail',$update_data,array('user_id'=>$lbgp->user_id));
                }
                if ($lbgp->user_id == $user_id) {
                    $res['leaved_total_balance'] = number_format((float)$update_total_balance, 2, '.', '');
                }
                $q_update_data = array(
                    'play_status' => 0,
                    'reported' => 0,
                    'win_lose_status' => null,
                    'updated_at' => '0000-00-00 00:00:00',
                );                
                $q_update_data_where = array(
                    'user_id' => $lbgp->user_id,
                    'group_bet_id' => $group_bet_id,
                );
                $q = $this->General_model->update_data('lobby_group', $q_update_data, $q_update_data_where);
            }
            if ($creator == 'no') {
                $del_data       = $this->General_model->delete_data('lobby_group', array(
                    'group_bet_id' => $group_bet_id,
                    'user_id' => $user_id
                ));
                $res['refresh'] = 'no';
            } else {
                $data=array(
                    'bet_status'=> 0,
                );
                $update = $this->General_model->update_data('group_bet',$data,array('id'=>$group_bet_id));
                if ($update) {
                    $res['refresh'] = 'yes';
                    $this->General_model->delete_data('lobby', array('id' => $lobby_id));
                }
            }
            echo json_encode($res);
            exit();
        }
    }
    public function get_change_stream_status() {
        $res['update'] = 'yes';
        $user_id = $_POST['user_id'];
        if ($user_id == $_SESSION['streamer_chat_id_set']) {
            unset($_SESSION['lobby_id']);
            unset($_SESSION['streamer_name_set']);
            unset($_SESSION['streamer_chat_id_set']);
            unset($_SESSION['streamer_channel_set']);
            $res['user_id'] = $user_id;
        }
        echo json_encode($res);
        exit();            
    }
    public function change_stream_status() {
        $this->check_user_page_access();
        if (isset($_POST) && isset($_POST['user_id']) && $_POST['user_id'] !='' && isset($_POST['lobby_id']) && $_POST['lobby_id'] !='' && isset($_POST['group_bet_id']) && $_POST['group_bet_id'] !='') {
            $res['lobby_id'] = $_POST['lobby_id'];
            if (isset($_POST['channel_name']) && $_POST['channel_name'] !='') {
                $res['status_update_data'] = 'enable';
                $update_data = array(
                    'stream_status' => 'enable',
                    'stream_channel' => $_POST['channel_name']
                );
            } else {
                $res['status_update_data'] = 'disable';
                $update_data = array(
                    'stream_status' => 'disable',
                    'stream_channel' => ''
                );
            }
            $update_data_where = array(
                'group_bet_id' => $_POST['group_bet_id'],
                'user_id' => $this->session->userdata('user_id')
            );
            $update = $this->General_model->update_data('lobby_group', $update_data, $update_data_where);
            $leftoptions      = array(
                    'select' => 'lg.*, ud.name, ud.user_id',
                    'table' => 'lobby_group lg',
                    'join' => array(
                        'group_bet gb' => 'gb.id = lg.group_bet_id',
                        'user_detail ud' => 'ud.user_id = lg.user_id',
                    ),
                    'where' => array(
                        'gb.lobby_id' => $_POST['lobby_id'],
                        'lg.table_cat' => 'LEFT',
                        'lg.group_bet_id' => $_POST['group_bet_id'],
                        'gb.bet_status' => 1
                    )
                );
            $res['lobby_game_bet_left_grp'] = count($this->General_model->commonGet($leftoptions));
            $rightoptions      = array(
                    'select' => 'lg.*, ud.name, ud.user_id',
                    'table' => 'lobby_group lg',
                    'join' => array(
                        'group_bet gb' => 'gb.id = lg.group_bet_id',
                        'user_detail ud' => 'ud.user_id = lg.user_id',
                    ),
                    'where' => array(
                        'gb.lobby_id' => $_POST['lobby_id'],
                        'lg.table_cat' => 'RIGHT',
                        'lg.group_bet_id' => $_POST['group_bet_id'],
                        'gb.bet_status' => 1
                    )
                );
            $res['lobby_game_bet_right_grp'] = count($this->General_model->commonGet($rightoptions));
            $streamerdetailsoptions      = array(
                    'select' => 'lg.*, ud.name, ud.user_id',
                    'table' => 'lobby_group lg',
                    'join' => array(
                        'group_bet gb' => 'gb.id = lg.group_bet_id',
                        'user_detail ud' => 'ud.user_id = lg.user_id',
                    ),
                    'where' => array(
                        'gb.lobby_id' => $_POST['lobby_id'],
                        'lg.group_bet_id' => $_POST['group_bet_id'],
                        'lg.user_id' => $_SESSION['user_id'],
                        'gb.bet_status' => 1
                    )
                );
            $res['streamer_details'] = $this->General_model->commonGet($streamerdetailsoptions);
            $res['fan_tag'] = $this->General_model->get_my_fan_tag($_POST['lobby_id'],$_SESSION['user_id']);
            $res['current_user_id'] = $_SESSION['user_id'];
            echo json_encode($res);
            exit();
        }
    }
    public function change_streamchat_status() {
        $this->check_user_page_access();
        if ($_POST['user_id'] !='' && $_POST['lobby_id'] !='' && $_POST['group_bet_id'] !='' && $_POST['check'] !='' && $_POST['t_streamerchat'] !='' && $_POST['streamer_name'] !='' && $_POST['stream_channel'] !='') {
            if ($_POST['check'] == 'check') {
                $_SESSION['lobby_id'] = $_POST['lobby_id'];
                $_SESSION['streamer_name_set'] = $_POST['streamer_name'];
                $_SESSION['streamer_chat_id_set'] = $_POST['t_streamerchat'];
                $_SESSION['streamer_channel_set'] = $_POST['stream_channel'];
                $res['lobby_id'] = $_SESSION['lobby_id'];
                $res['streamer_name_set'] = $_SESSION['streamer_name_set'];
                $res['streamer_chat_id_set'] = $_SESSION['streamer_chat_id_set'];
                $res['streamer_channel_set'] = $_SESSION['streamer_channel_set'];
                $res['steam_chat_set'] = 'on';
            } else {
                unset($_SESSION['lobby_id']);
                unset($_SESSION['streamer_name_set']);
                unset($_SESSION['streamer_chat_id_set']);
                unset($_SESSION['streamer_channel_set']);
                $res['steam_chat_set'] = 'off';
            }
        }
        echo json_encode($res);
        exit();
    }
    public function send_live_stream_show_status() {
        $this->check_user_page_access();
        if ($_POST['user_id'] !='' && $_POST['lobby_id'] !='' && $_POST['group_bet_id'] !='' && $_POST['check'] !='' && $_POST['t_streamerchat'] !='' && $_POST['streamer_name'] !='' && $_POST['stream_channel'] !='') {
            if ($_POST['check'] == 'check') {
                if (isset($_SESSION['stream_store'])) {
                    $stream_store[] = array(
                        'lobby_id' => $_POST['lobby_id'],
                        'streamer_name_set' => $_POST['streamer_name'],
                        'streamer_chat_id_set' => $_POST['t_streamerchat'],
                        'streamer_channel_set' => $_POST['stream_channel'],
                    );
                    $_SESSION['stream_store'] = array_merge($_SESSION['stream_store'], $stream_store);
                } else {
                    $stream_store = array(
                        'lobby_id' => $_POST['lobby_id'],
                        'streamer_name_set' => $_POST['streamer_name'],
                        'streamer_chat_id_set' => $_POST['t_streamerchat'],
                        'streamer_channel_set' => $_POST['stream_channel'],
                    );
                    $_SESSION['stream_store'][] = $stream_store;
                }
                $res['lobby_id'] = $_POST['lobby_id'];
                $res['streamer_name_set'] = $_POST['streamer_name'];
                $res['streamer_chat_id_set'] = $_POST['t_streamerchat'];
                $res['streamer_channel_set'] = $_POST['stream_channel'];
                $res['steam_chat_set'] = 'on';
            } else {
                foreach ($_SESSION['stream_store'] as $key => $ss) {
                    if ($_POST['t_streamerchat'] != $ss['streamer_chat_id_set']) {
                        $stream_store[] = array(
                            'lobby_id' => $ss['lobby_id'],
                            'streamer_name_set' => $ss['streamer_name_set'],
                            'streamer_chat_id_set' => $ss['streamer_chat_id_set'],
                            'streamer_channel_set' => $_POST['streamer_channel_set'],  
                        );
                    }
                }
                $_SESSION['stream_store'] = $stream_store;
                $res['lobby_id'] = $_POST['lobby_id'];
                $res['streamer_name_set'] = $_POST['streamer_name'];
                $res['streamer_chat_id_set'] = $_POST['t_streamerchat'];
                $res['streamer_channel_set'] = $_POST['stream_channel'];
                $res['steam_chat_set'] = 'off';
            }
        }
        $res['table_cat'] = $_POST['table_cat'];
        $res['disable_class'] = $_POST['disable_class'];
        echo json_encode($res);
        exit();
    }
    public function send_tip_to_streamer() {
        $this->check_user_page_access();
        if (isset($_POST) && isset($_POST['amount']) && $_POST['amount'] !='' && isset($_POST['from']) && $_POST['from'] !='' && isset($_POST['lobby_id']) && $_POST['lobby_id'] !='' && isset($_POST['group_bet_id']) && $_POST['group_bet_id'] !='' && isset($_POST['to']) && $_POST['to'] !='') {
            $res['lobby_id'] = $_POST['lobby_id'];
            
            $tip_amount = number_format($_POST['amount'],2,".","");
            
            $get_current_user_balance = $this->General_model->view_data('user_detail', array(
                'user_id' => $_SESSION['user_id']
            ));

            $res['from_total_balance'] = $get_current_user_balance[0]['total_balance'] - $tip_amount;
            
            $res['balance_status'] = '';
            if ($tip_amount>$get_current_user_balance[0]['total_balance']) {
                $res['balance_status'] = 'less';
                echo json_encode($res);
                exit();
            }
            else{
                $get_streamer_balance = $this->General_model->view_data('user_detail', array(
                    'user_id' => $_POST['to']
                ));
                $res['to_total_balance'] = $get_streamer_balance[0]['total_balance'] + $tip_amount;
                
                $update_data = array(
                        'total_balance' => $res['to_total_balance']
                    );
                $update_data_where = array(
                    'user_id' => $_POST['to']
                );
                $update_to = $this->General_model->update_data('user_detail', $update_data, $update_data_where);

                $update_data = array(
                        'total_balance' => $res['from_total_balance']
                    );
                $update_data_where = array(
                    'user_id' => $_POST['from']
                );
                $update_from = $this->General_model->update_data('user_detail', $update_data, $update_data_where);

                if ($update_to && $update_from) {
                    $res['user_to'] = $_POST['to'];
                    $res['user_from'] = $_POST['from'];
                    echo json_encode($res);
                    exit();
                }
            }
            
        }
    }   
    public function send_tip_to_team() {
       $this->check_user_page_access();
        if (isset($_POST) && isset($_POST['amount']) && $_POST['amount'] !='' && isset($_POST['from']) && $_POST['from'] !='' && isset($_POST['lobby_id']) && $_POST['lobby_id'] !='' && isset($_POST['group_bet_id']) && $_POST['group_bet_id'] !='') {
            $lobby_id = $_POST['lobby_id'];
            $group_bet_id = $_POST['group_bet_id'];
            $res['lobby_id'] = $lobby_id;
            $tip_amount = number_format($_POST['amount'],2,".","");
            
            $get_current_user_balance = $this->General_model->view_data('user_detail', array(
                'user_id' => $_SESSION['user_id']
            ));
            $res['balance_status'] = '';
            if ($tip_amount>$get_current_user_balance[0]['total_balance']) {
                $res['balance_status'] = 'less';
                echo json_encode($res);
                exit();
            } else {
                $data['lobby_game_bet_left_grp']   = $this->General_model->lobby_game_bet_left_grp($lobby_id);
                $data['lobby_game_bet_right_grp']  = $this->General_model->lobby_game_bet_right_grp($lobby_id);
                $group_users = array_merge($data['lobby_game_bet_left_grp'],$data['lobby_game_bet_right_grp']);
                $deduct_balance = 0;
                foreach ($group_users as $key => $guser) {
                    $get_streamer_balance = $this->General_model->view_data('user_detail', array(
                        'user_id' => $guser['user_id']
                    ));
                    $deduct_balance = $deduct_balance + $tip_amount;
                    $update_balance = $get_streamer_balance[0]['total_balance'] + $tip_amount;
                    $update_data = array(
                            'total_balance' => $update_balance
                        );
                    $update_data_where = array(
                        'user_id' => $guser['user_id']
                    );
                    $update_to = $this->General_model->update_data('user_detail', $update_data, $update_data_where);
                }
                
                $getbalance = $this->General_model->view_data('user_detail', array(
                    'user_id' => $_SESSION['user_id']
                ));
                $update_balance = $getbalance[0]['total_balance'] - $deduct_balance;
                $update_data = array(
                        'total_balance' => $update_balance
                    );
                $update_data_where = array(
                    'user_id' => $_SESSION['user_id']
                );
                $update_to = $this->General_model->update_data('user_detail', $update_data, $update_data_where);
                $res['get_grp_with_balance']  = $this->General_model->get_join_group_data($lobby_id, $group_bet_id);
                $res['sender_id'] = $_SESSION['user_id'];
                echo json_encode($res);
                exit();
                if (!$update_to) {
                }
            }
        }
    }
    public function change_pass_control() {
       $this->check_user_page_access();
       if(isset($_POST['user_id']) && $_POST['user_id'] != '' && isset($_POST['lobby_id']) && $_POST['lobby_id'] != '' && isset($_POST['group_bet_id']) && $_POST['group_bet_id'] != '' && isset($_POST['pass_to']) && $_POST['pass_to'] != '') {
           $lobby_id = $_POST['lobby_id'];
           $group_bet_id = $_POST['group_bet_id'];
           $pass_to = $_POST['pass_to'];
           $amount = number_format($_POST['amount'],2,".","");
           $data['lobby_game_bet_left_grp']   = $this->General_model->lobby_game_bet_left_grp($lobby_id);
           $data['lobby_game_bet_right_grp']  = $this->General_model->lobby_game_bet_right_grp($lobby_id);
           $group_users = array_merge($data['lobby_game_bet_left_grp'],$data['lobby_game_bet_right_grp']);
           foreach ($group_users as $key => $guser) {
               if ($guser['play_status'] == 1) {
                    $get_streamer_balance = $this->General_model->view_data('user_detail', array(
                        'user_id' => $guser['user_id']
                    ));
                    $update_balance = $get_streamer_balance[0]['total_balance'] + $amount;
                    $update_data = array(
                        'total_balance' => $update_balance
                    );
                    $update_data_where = array(
                        'user_id' => $guser['user_id']
                    );
                    $update_to = $this->General_model->update_data('user_detail', $update_data, $update_data_where);
                }
                $update_dt = array(
                    'play_status' => 0,
                    'reported' => 0,
                    'win_lose_status' => NULL,
                    'reported' => 0,
                    'updated_at' => '0000-00-00 00:00:00',
                );
                $update_dt_where = array(
                    'user_id' => $guser['user_id'],
                    'group_bet_id' => $group_bet_id,
                );
                $this->General_model->update_data('lobby_group', $update_dt, $update_dt_where);
           }
           $pass_to_arr = array(
                'is_creator' => 'yes',
           );
            $update_pass_where = array(
                'user_id' => $pass_to,
                'group_bet_id' => $group_bet_id,
            );
            $this->General_model->update_data('lobby_group', $pass_to_arr, $update_pass_where);
            
            $pass_from =  array(
                'is_creator' => 'no',
            );
            $update_passfrom_where = array(
                'user_id' => $_SESSION['user_id'],
                'group_bet_id' => $group_bet_id,
            );
            $this->General_model->update_data('lobby_group', $pass_from, $update_passfrom_where);
            $res['lobby_id'] = $lobby_id;
            $res['group_bet_id'] = $group_bet_id;
            $res['pass_from'] = $_SESSION['user_id'];
            $res['pass_to'] = $pass_to;
            echo json_encode($res);
            exit();
       }
    }
}