<?php $this->load->view('admin/common/show_message'); ?>
<section class="content box">
   <div class="row">
      <div class="col-md-12">
         <div class="row">
           <div class="agent_navigation pull-right">
           	   <a href="<?php echo base_url(); ?>admin/ads/create_ads/">Add new Ads</a>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 xwb-main-contact-conversation">
               <div class="table-responsive agent_table">
                  <table id="example1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th>Sl. No.</th>
                           <th>Ads type</th>
                           <th>Sponserd By</th>
                           <th>Sponserd Description</th>
                           <th>Sponserd Link</th>
                           <th>Youtube Link</th>
                           <th>Custom Commercial</th>
                           <th>Date</th>
                           <th>Option</th>
                        </tr>
                     </thead>
                     <tbody>
                      <?php 
                      $i = 1;
                      if (isset($ads_list)) {
                        foreach ($ads_list as $value) {
                          $ads_list_row = '<tr>
                            <td>'.$i.'</td>
                            <td><b>'.$value->ads_type.'</b></td>
                            <td>'.$value->sponsered_by_logo_title.'</td>
                            <td>'.$value->sponsered_by_logo_description.'</td>
                            <td style="max-width: 180px !important;overflow: hidden;">'.$value->sponsered_by_logo_link.'</td>
                            <td style="max-width: 180px !important;overflow: hidden;">'.$value->youtube_link.'</td>
                            <td>'.$value->custom_commercial.'</td>
                            <td>'.$value->created_date.'</td>
                            <td><a href="'.base_url().'admin/ads/edit_ads/?id='.$value->id.'"><i class="fa fa-pencil" id="'.$value->id.'"></i></a> &nbsp;&nbsp;&nbsp;&nbsp;<a class="delete-data" action="delete_ads" id="'.$value->id.'"><i class="fa fa-trash-o" id="'.$value->id.'"></i></a></td>
                          </tr>'; 
                          echo $ads_list_row;
                          $i++;
                        } 
                      }
                      ?>
                     </tbody>
                  </table>
               </div>
            </div>
           <div class="modal fade  custom-width" id="modal-1">
            <div class="modal-dialog" style="width: 50%;">
              <div class="modal-content">
                
              </div>
            </div>
          </div>
         </div>
      </div>
   </div>
</section>


