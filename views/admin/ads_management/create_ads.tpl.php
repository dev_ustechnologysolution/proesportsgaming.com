<section class="content background-white box box-primary">
  <div class="row ">
  	<div class="col-md-12">
    	<form action="<?php echo base_url();?>admin/ads/insert_ads/" method="POST" enctype='multipart/form-data' class="add_ads ads_master"  onsubmit="return lobbyadsimage_size_validation()">
    		<!-- product_list -->
    		<?php
    		$hide_item_cls = 'hide';
    		$show_item_cls = 'show';
    		$req_item = '';
    		if ($_GET['select_ads_type'] == 'product_ads') {
    			$hide_item_cls = ''; 
    			$req_item = 'required';
    			?>
    			<div class="form-group">
					<label>Choose Product:</label>
					<select class="form-control select2 select_2" name="select_product_id[]" required multiple>
						<option value="">Select Product</option>
						<?php foreach ($product_list as $key => $pl) { ?>
							<option value="<?php echo $pl['id']; ?>"><?php echo $pl['name']; ?></option>
						<?php } ?>
					</select>
					<input type="hidden" name="select_ads_type" value="product_ads">
				</div>
    		<?php } else { ?>
    			<div class="form-group">
					<label>Choose Ads Category:</label>
					<select class="form-control" name="select_ads_type" required>
						<option value="">Select Ads Category</option>
						<option value="lobby_ads"> Lobby Ads </option>
						<option value="store_ads"> Store Ads </option>
					</select>
				</div>
	    		<div class="form-group">
					<div>
						<label>Choose Ads Type:</label>
					</div>
					<div class="padd0 col-md-6 form-group">
						<label><input type="radio" name="select_type" value="static_dis_ads" checked><i> Static display ads</i></label>
					</div>
					<div class="padd0 col-md-6 form-group">
						<label><input type="radio" name="select_type" value="commercial_ads"><i> Commercial Ads</i></label>
					</div>
				</div>
			<?php } ?>
			<div class="static_dis_ads show">
	    		<div class="form-group">
					<label>Sponsered By Logo Title:</label>
					<input type="text" class="form-control" id="sponsered_by_logo_title" name="sponsered_by_logo_title" placeholder="Enter Sponsered By Logo Title" required>
				</div>
				<div class="form-group">
					<label>Upload Sponsered By Logo: (400px * 300px)</label>
					<label id="sponsered_by_logo_size" style="display:none"></label>
					<input type="file" class="form-control" id="upload_sponsered_by_logo" name="upload_sponsered_by_logo" placeholder="Upload Sponsered By Logo" required accept="image/*">
				</div>
				<div class="form-group">
					<label>Sponsered By Logo Description:</label>
					<textarea class="form-control" id="sponsered_by_logo_description" name="sponsered_by_logo_description" placeholder="Enter Sponsered By Logo Description" required></textarea>
				</div>
				<div class="form-group">
					<label>Sponsered By Logo Link:</label>
					<input type="url" class="form-control" id="sponsered_by_logo_link" name="sponsered_by_logo_link" placeholder="Enter Sponsered By Logo Link" required>
				</div>
			</div>
			<div class="commercial_ads <?php echo $hide_item_cls;  ?>">
				<div class="form-group">
					<label>Select video</label>
					<select name="select_vids_type" class="form-control" <?php echo $req_item; ?>>
						<option value=""> Select Commercial video type</option>
						<option value="youtube_link"> Youtube Link </option>
						<option value="custom_vids"> Upload Custom </option>
					</select>
				</div>
				<div class="form-group youtube_link hide">
					<label>Youtube Link</label>
					<input type="url" class="form-control" placeholder="Enter youtube link" name="youtube_link">
				</div>
				<div class="form-group custom_vids hide">
					<label>Upload Commercial</label>
					<input type="file" class="form-control" accept="video/mp4,video/3gp,video/ogg" name="custom_vids">
				</div>
			</div>
			<div class="form-group">
				<input type="submit" name="submit" class="btn btn-info" value="Submit">
			</div>
		</form>
	</div>
</div>
</section>