<section class="content background-white box box-primary">
  <div class="row">
    <div class="col-md-12"> 
      <form action="<?php echo base_url();?>admin/ads/update_single_data" method="POST" enctype='multipart/form-data' class="add_ads ads_master" onsubmit="return lobbyadsimage_size_validation()">
        <?php
        $hide_item_cls = 'hide';
        $show_item_cls = 'show';
        $req_item = '';
        if ($_GET['select_ads_type'] == 'product_ads') { 
          $hide_item_cls = ''; 
          $req_item = 'required';
          ?>
          <div class="form-group">
            <label>Choose Product:</label>
            <select class="form-control select2 select_2" name="select_product_id[]" required multiple>
              <option value="">Select Product</option>
              <?php
              $product_ids = explode(',', $ads_single_row[0]['product_id']);
              foreach ($product_list as $key => $pl) {
                $selected = '';
                if (in_array($pl['id'],$product_ids)) {
                  $selected = 'selected';
                } ?>
                <option value="<?php echo $pl['id']; ?>" <?php echo $selected; ?>><?php echo $pl['name']; ?></option>
              <?php } ?>
            </select>
            <input type="hidden" name="select_ads_type" value="product_ads">
          </div>
        <?php } else { ?>
          <div class="form-group">
            <label>Choose Ads Type:</label>
            <select class="form-control" name="select_ads_type" required>
              <option value="">Select Ads Type</option>
              <option value="lobby_ads" <?php echo ($ads_single_row[0]['ads_type'] == 'lobby_ads')? 'selected' : ''; ?>> Lobby Ads </option>
              <option value="store_ads" <?php echo ($ads_single_row[0]['ads_type'] == 'store_ads')? 'selected' : ''; ?>> Store Ads </option>
            </select>
          </div>
          <div class="form-group">
            <div>
              <label>Choose Ads Type:</label>
            </div>
            <div class="padd0 col-md-6 form-group">
              <label><input type="radio" name="select_type" value="static_dis_ads" <?php echo ($ads_single_row[0]['upload_sponsered_by_logo'] !='') ? 'checked' : ''; ?>><i> Static display Ads</i></label>
            </div>
            <div class="padd0 col-md-6 form-group">
              <label><input type="radio" name="select_type" value="commercial_ads" <?php echo ($ads_single_row[0]['youtube_link'] !='' || $ads_single_row[0]['custom_commercial'] !='') ? 'checked' : ''; ?>><i> Commercial Ads</i></label>
            </div>
          </div>
        <?php } ?>
        <div class="static_dis_ads <?php echo ($ads_single_row[0]['upload_sponsered_by_logo'] !='') ? 'show' : 'hide'; ?>">
          <div class="form-group">
            <label>Sponsered By Logo Title:</label>
            <input type="text" class="form-control" id="sponsered_by_logo_title" name="sponsered_by_logo_title" <?php echo ($ads_single_row[0]['sponsered_by_logo_title'])? 'value="'.$ads_single_row[0]['sponsered_by_logo_title'].'" required' :''; ?> pattern="[A-Za-z0-9-_ ]+" title="Allow letters, numbers, hyphens, underscores and space Only" placeholder="Enter title">
          </div>
          <div class="form-group">
            <label>Upload Sponsered By Logo: (400px * 300px)</label>
            <label id="sponsered_by_logo_size" style="display:none"></label>
            <input type="file" class="form-control novalidate" id="upload_sponsered_by_logo" name="upload_sponsered_by_logo" value="" accept="image/*">
            <?php echo ($ads_single_row[0]['upload_sponsered_by_logo'])? "<img src='".base_url().'upload/ads_master_upload/'.$ads_single_row[0]['upload_sponsered_by_logo']."' height='auto' width='150' alt='Sponsered By Logo'>" : ''; ?>
            <input type="hidden" class="form-control" name="upload_sponsered_by_logo_image" <?php echo ($ads_single_row[0]['upload_sponsered_by_logo'])? 'value="'.$ads_single_row[0]['upload_sponsered_by_logo'].'"' :''; ?>>
          </div>
          <div class="form-group">
            <label>Sponsered By Logo Description:</label>
            <textarea class="form-control" id="sponsered_by_logo_description" name="sponsered_by_logo_description" placeholder="Enter sponsered by logo description" <?php echo ($ads_single_row[0]['sponsered_by_logo_description'])? 'required' :''; ?>><?php echo ($ads_single_row[0]['sponsered_by_logo_description'])? $ads_single_row[0]['sponsered_by_logo_description'] :''; ?></textarea>
          </div>
          <div class="form-group">
            <label>Sponsered By Logo Link:</label>
            <input type="url" class="form-control" id="sponsered_by_logo_link" placeholder="Enter sponsered by link"  name="sponsered_by_logo_link" <?php echo ($ads_single_row[0]['sponsered_by_logo_link'])? 'value="'.$ads_single_row[0]['sponsered_by_logo_link'].'" required' :''; ?>>
          </div>
        </div>
        <div class="commercial_ads <?php echo ($ads_single_row[0]['youtube_link'] !='' || $ads_single_row[0]['custom_commercial'] !='') ? 'show' : 'hide'; ?>">
          <div class="form-group">
            <label>Select video</label>
            <select name="select_vids_type" class="form-control" <?php echo ($ads_single_row[0]['youtube_link'] !='' || $ads_single_row[0]['custom_commercial'] !='') ? 'required' : ''; ?>>
              <option value=""> Select Commercial video type</option>
              <option value="youtube_link" <?php echo ($ads_single_row[0]['youtube_link'] !='') ? 'selected' : ''; ?>> Youtube Link </option>
              <option value="custom_vids" <?php echo ($ads_single_row[0]['custom_commercial'] !='') ? 'selected' : ''; ?>> Upload Custom </option>
            </select>
          </div>
          <div class="form-group youtube_link <?php echo ($ads_single_row[0]['youtube_link'] !='') ? 'show' : 'hide'; ?>">
            <label>Youtube Link</label>
            <input type="url" class="form-control" placeholder="Enter youtube link" name="youtube_link" <?php echo ($ads_single_row[0]['youtube_link'] !='') ? 'value="'.$ads_single_row[0]['youtube_link'].'" required' : ''; ?>>
          </div>
          <div class="form-group custom_vids <?php echo ($ads_single_row[0]['custom_commercial'] !='') ? 'show' : 'hide'; ?>">
            <label>Upload Commercial</label>
            <input type="file" accept="video/mp4,video/3gp,video/ogg" name="custom_vids" class="form-control <?php echo ($ads_single_row[0]['custom_commercial'] !='') ? 'novalidate' : ''; ?>">
            <br>
            <div>
              <?php echo ($ads_single_row[0]['custom_commercial'] !='') ? '<video muted autoplay controls loop src="'.base_url().'upload/ads_master_upload/'.$ads_single_row[0]['custom_commercial'].'" width="40%"></video> <input type="hidden" name="old_custom_commercial" value="'.$ads_single_row[0]['custom_commercial'].'"> <div>'.$ads_single_row[0]['custom_commercial'].'</div>' : ''; ?>
            </div>
          </div>
        </div>
        <div class="form-group">
          <input type="hidden" name="id" value="<?php echo $edit_id; ?>">
          <input type="submit" name="submit" class="btn btn-info" value="Submit">
        </div>
      </form>
    </div>
  </div>
</section>