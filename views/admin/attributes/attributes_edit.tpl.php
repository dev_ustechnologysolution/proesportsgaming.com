<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php $this->load->view('admin/common/show_message') ?>
                       
                <form  action="<?php echo base_url(); ?>admin/attributes/attributeUpdate" method="post" enctype="multipart/form-data" class="admin_game_edit_form">
                    <input type="hidden" name="id" value="<?php echo $attributes_data[0]['id']; ?>">
                  
                    <?php 

                    if(count($option_data)>0) { 
                        ?>
                     <div class="box box-primary" id="hidden_div"> 
                        <h4 class="optionheader"><b><?php echo "Update ".ucfirst($attributes_data[0]['title'])."" ?></b></h4>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right" for="title">Attribute Title </label>
                            <div class="col-sm-4">
                            <input class="form-control" type="text" name="title" value="<?php echo $attributes_data[0]['title']; ?>">
                        </div>
                        </div>
                        <

                        <?php $k = 1; foreach($option_data as $key => $v){                            
                            ?>
                        <div class="form-group row remove_<?php echo $k; ?>" data-seq="remove_<?php echo $k; ?>">
                                <label class="col-sm-2 text-right" for="name">Main Title </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="option_label[]" value="<?php echo $v['option_label']; ?>"/>
                                    <input type="hidden" class="form-control" name="option_data[]" value="<?php echo $v['option_id']; ?>"/>
                                </div>
                                <?php if($key == 0) { ?>
                                <div class="col-sm-1">
                                    <button class="btn btn-primary add-row" type="button">Add</button>
                                </div> 
                                <?php } else { ?> 
                                <div class="col-sm-1">
                                    <button class="btn btn-danger remove_field" type="button">Remove</button>
                                </div>
                                <?php } ?>                              
                        </div>
                            <?php $k++; }} ?>
                        <div class="input_fields_container"></div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right"></label>
                            <div class="col-sm-4" align="center">
                                <button class="btn btn-primary" type="submit" id="edit_game_update">Submit</button>
                            </div>
                        </div>
                     </div>
                   
                </form>                
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function(){
       
        var x = '<?php echo $k;?>';
        var max_fields_limit = 10;
        $(".add-row").click(function(e){
            e.preventDefault();            
            if(x < max_fields_limit){ 
                x++; 
                $('.input_fields_container').append('<div class="form-group row remove_'+x+'" data-seq="remove_'+x+'"><label class="col-sm-2 text-right">Main Title</label><div class="col-sm-4 "><input type="text" required class="form-control" name="option_label[]" value=""/></div><div class="col-sm-1 "><button class="btn btn-danger remove_field" type="button">Remove</button></div></div>'); 
            } else {
                $('.add-row').attr('disabled','true');
            }
        });
        
        
        // $('.input_fields_container').on("click",".remove_field", function(e){ 
             $(".remove_field").click(function(e){
            e.preventDefault();
            data = $(this).closest('[data-seq]').data('seq'); 
            $('.'+data).remove(); 
            x--;
        });        
    }); 
     function showDiv(divId, element)
        {
            document.getElementById(divId).style.display = element.value == 3 || element.value == 2 ? 'block' : 'none';
        }   
</script>
<style type="text/css">
    .optionheader{
        margin: 30px 6px;
    }
    .pb20{
        padding-bottom: 20px;
    }
    #hidden_div {
        padding-bottom: 20px;
        /*display: none;*/
    }
    .block {
        display: block;
    }
    .none {
        display: none;
    }
</style>
