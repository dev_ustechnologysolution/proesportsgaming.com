<section class="content" style="margin-top:30px;"> 
  <?php $this->load->view('admin/common/show_message') ?>
  <div class="box banner_page">
    <div class="box-header">
    <?php if(count($list) > 0) { 
            foreach ($list as $key => $value) { ?>
              <a href="<?php echo base_url().'admin/attributes/attributeAdd?id='.$value['id']; ?>"><button style="margin:0px 4px;" class="btn pull-right btn-primary btn-xl">Add <?php echo $value['title']; ?></button></a>
            <?php } } ?>    
      <a class="add_attribute pull-right" style="cursor:pointer;margin:0px 4px;font-size: 16px;line-height: 32px;"><i class="fa fa-pencil fa-fw"></i>Add Attribute</a>
    </div>
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Attribute Title</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; foreach($list as $k => $attribute){  ?>          
          <tr>
            <td><?php echo ucfirst($attribute['title']); ?></td>
            <td><label class="myCheckbox"><input <?php echo ($attribute['status'] == 1) ? 'checked' :'';?> id="is_banner<?php echo $k;?>" class="single-checkbox custom-cb" type="checkbox" name="progress" value="<?php echo $attribute['id'];?>"><span></span></label></td> 
            <td>
              <a title="Edit Attribute" href="<?php echo base_url().'admin/attributes/attributeEdit?id='.$attribute['id']; ?>">
                <i class="fa fa-pencil fa-fw"></i>
              </a>
               <a title="Delete Attribute" href="<?php echo base_url().'admin/attributes/delete?id='.$attribute['id']; ?>">
                <i class="fa fa-trash fa-fw"></i>
              </a>
            </td>
          </tr>
          <?php }?>
        </tbody>
      </table>
    </div>
   
  </div>
</section>
<style>
.no_banner{
  width:15px;
  height:15px;
  background:red;
  border-radius:4px;
}
.yes_banner{
  width:15px;
  height:15px;
  background:green;
  border-radius:4px;
}
.myCheckbox input {
   visibility:hidden;
    position: absolute;
    z-index: -9999;
}
.myCheckbox span {
    width: 15px;
    height: 15px;
    display: block;
    background: red;
    border-radius:4px;
}
.myCheckbox input:checked + span {
    background: green;
}
</style>
<div class="modal fade  custom-width" id="modal-1">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content"> 
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" style="color:#111; font-size:19px; font-weight:bold;">Add Attribute</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <form method="post" action="<?php echo base_url('admin/attributes/attributeInsert');?>">
            <input type='hidden' name="type" value="attribute">
            <div class="box-body">
              <div class="form-group">
                <label>Add Attribute</label>
                <input type="text" name="title" required class="form-control">
              </div>
            </div>
            <div class="box-footer">
              <input class="btn btn-primary" type="submit" value="Submit" >
              <button type="button" class="btn btn-info pull-right" data-dismiss="modal">Close</button>   
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).on('click','.add_attribute',function(){
    $('#modal-1').modal('show');     
  })

   $(document).on('click','.lobby_view',function(){
  var id = $(this).attr('id');
  if(id){
    $.ajax({
      url : '<?php echo site_url(); ?>admin/Banner/ChangeLobbyOrder/?id='+id,
      success: function(result) {
        if(result){
          $('.modal-content').html(result);
          $('#modal-1').modal('show');                              

        }
      }
    });
  }
});
$(document).on('submit','#change_lobby_order',function(e){
  e.preventDefault();
  var data = $(this).serialize();
  if(data){
    $.ajax({
      url: "<?php echo site_url(); ?>admin/Banner/UpdateLobbyOrder",
      data: data,
      type: 'POST',
      success: function(result) {
        window.location.href = "<?php echo site_url(); ?>admin/Banner/";       
      }
    });
  }             
});
</script>
<script>
$(document).ready(function(){
    
    var $checkboxes = $('td input[type="checkbox"]');
    
        $('input[type="checkbox"]').click(function()
        {            
            var countCheckedCheckboxes = $checkboxes.filter(':checked').length;            
            var checked_val = $(this).prop("value");
            
            if($(this).prop("checked") == true){             
                
                
                    var type = 'check';
            }
            else if($(this).prop("checked") == false){
                var type = 'uncheck';
            }
            $.ajax({
                url : '<?php echo site_url(); ?>admin/attributes/updateIsStatus?type='+type+'&id='+checked_val,
                success: function(result) {                   
                    // window.location = window.location;                    
                   
                }
            });
            
        });
    });


</script>