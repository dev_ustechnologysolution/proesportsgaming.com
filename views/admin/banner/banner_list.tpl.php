<style>
.none{
  display:none;
}
.block {
  display:block;
}
</style>
<div class="pull-left status_button col-sm-12 banner_page">
  <b>Live Lobby Banner</b> &nbsp;&nbsp;<label class="switch"><input type="checkbox" name="lobby_status" value="lobby_status" <?php if($lobby_status['option_value'] == 'on') { echo 'checked'; } ?> class="lobby_status"><span class="slider round"></span></label>
</div>
<input type="hidden" id="lobby_st" value="<?php echo $lobby_status['option_value']; ?>"> 
<section class="content" style="margin-top:30px;">
  <div class="nav-tabs-custom">

    <ul class="nav nav-tabs" role="tablist">
      <li class="<?php echo ($active_tab == 'lobby') ? 'active' : '';?>"><a href="#javatab" role="tab" data-toggle="tab">Live Stream Banners</a></li>
      <li class="<?php echo ($active_tab == 'banner') ? 'active' : '';?>"><a href="#hometab" role="tab" data-toggle="tab">Banner List</a></li>
      
    </ul>
    <div class="tab-content">
       <?php $this->load->view('admin/common/show_message') ?>
      <div class="tab-pane <?php echo ($active_tab == 'banner') ? 'active' : '';?>" id="hometab">
  <div class="box banner_page1">    
    <div class="box-header">
      <div>
      <!-- <a href="<?php echo base_url().'admin/banner/'; ?>"><button class="btn pull-left btn-primary btn-xl">Main Sliders</button></a><a href="<?php echo base_url().'admin/banner/tab_banner'; ?>"><button class="btn pull-left btn-primary btn-xl">Tab Sliders</button></a>
      <a href="<?php echo base_url().'admin/banner/chat_banner'; ?>"><button class="btn pull-left btn-primary btn-xl">Chat Sliders</button></a> -->
      <a href="<?php echo base_url().'admin/banner/bannerAdd'; ?>"><button class="btn pull-right btn-primary btn-xl">Add Banner</button></a>
    </div>
  </div>
 
  <div class="box-body">
    <table class="table table-bordered table-striped">
      <thead>
        <tr>
          <th style="display: none;"></th>
          <th>Banner Order</th>
          <th>Banner Name</th>
          <th>Banner Content</th>
          <th>Banner Youtube Link</th>
          <th>Banner Image</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php $i=1;
        foreach($list as $value) { ?>
        <?php
        $banner_order = $value['banner_order'];
        $banner_img = $value['banner_image'] != '' ? $value['banner_image'] : 'placeholder.png';
        ?>        
        <tr>
          <td style="display: none;"></td>
          <td><?php echo $value['banner_order']; ?>&nbsp;&nbsp;<a href="#" class="view" id="<?php echo $value['id']?>"><i class="fa fa-pencil fa-fw"></i></a></td>
          <td><?php echo $value['banner_name']?></td>
          <td><?php echo $value['banner_content']?></td>
          <td><?php echo $value['banner_youtube_link']?></td>
          <td><img src="<?php echo base_url().'upload/banner/'. $banner_img; ?>" width="100" height="100"></td>
          <td><a href="<?php echo base_url().'admin/banner/bannerEdit/'.$value['id'];?>"><i class="fa fa-pencil fa-fw"></i></a>|<a href="<?php echo base_url().'admin/banner/delete/'.$value['id'];?>" onclick="return confirm('Are you sure delete ?')"><i class="fa fa-trash-o fa-fw"></i></a></td>
        </tr>
        <?php }?>
      </tbody>
    </table>
  </div>
  </div>
</div>
 <div class="tab-pane <?php echo ($active_tab == 'lobby') ? 'active' : '';?>" id="javatab">
  <div class="box banner_page2">
     <div class="box-header">      
      <button class="btn pull-right btn-primary btn-xl" onclick="return changeEventFee();">Global Event Fee</button>    
      </div>
    <div class="box-body">
      <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th style="display: none;"></th>
            <th>Lobby Strem Order</th>
            <th>Lobby ID</th>
            <th>Lobby Title</th>
            <th>Description</th>
            <th>Creator</th>
            <th>Tag</th>
            <th>Link</th>
            <th>Is Banner</th>
            <th>Is Event</th>
            <th>Event Image</th>
            <th>Action</th>
            <th>Is Spectator</th>
            <th>Spectator</th>
            
          </tr>
        </thead>
        <tbody>
          <?php $i=1; foreach($live_lobby_data as $k => $lobby){


            $creator_arr = $this->General_model->view_single_row('user_detail','user_id',$lobby->user_id);

            $arr = explode(',',$lobby->channel);
            
            $count = 0;
            foreach($arr as $a){
              $sub_arr = explode('*_*+',$a);
              if($sub_arr[1] == 1){
                $count = 1;
              }
            }
            if($count == 0)
            {
              $is_banner_class = 'no_banner';
            } else {
              $is_banner_class = 'yes_banner';
            }
            ?>          
          <tr>
            <td style="display: none;"></td>
            <td><?php echo ($lobby->lobby_order == 0) ? '' : $lobby->lobby_order; ?>&nbsp;&nbsp;<a href="#" class="lobby_view" id="<?php echo $lobby->lobby_id; ?>"><i class="fa fa-pencil fa-fw"></i></a></td>
            <td><?php echo $lobby->lobby_id; ?></td>
            <td><?php echo $lobby->game_name; ?></td>
            <td><?php echo $lobby->game_description; ?></td>


            <td><?php echo ucwords($creator_arr['name']); ?></td>

            <td><?php echo $lobby->device_id?></td>
            <td><a target="_blank" class="btn btn-primary" href="<?php echo base_url('livelobby/watchLobby/'.$lobby->lobby_id); ?>">Live Lobby</td> 
            <td><p class="<?php echo $is_banner_class; ?>"></p></td>
            <td><label class="myCheckbox"><input <?php echo ($lobby->is_event == 1) ? 'checked' :'';?> id="is_event<?php echo $k;?>" class="single-checkbox is_event custom-cb" type="checkbox" name="progress" value="<?php echo $lobby->lobby_id;?>"><span></span></label></td>
           
            <td>
              <a style="cursor: pointer;" title="Update Event Image" onclick="return changeEventprice('<?php echo $lobby->lobby_id; ?>','event_price');">
              <?php ($lobby->event_image == '') ? $img = 'no-image.png' : $img = $lobby->event_image;?><img style="border: 1px solid gray;border-radius: 12px;
"src="<?php echo base_url().'upload/banner/'.$img; ?>" width="125"></a></td>
            <td>
              <a title="Edit Banner" href="<?php echo base_url().'admin/banner/lobbyBannerEdit?id='.$lobby->lobby_id.'&name='.$lobby->game_name;?>"><i class="fa fa-pencil fa-fw"></i></a>
              <a title="Update Event Ticket Amount" onclick="return changeEventprice('<?php echo $lobby->lobby_id; ?>','event_price');"><i class="fa fa-money fa-fw"></i></a>
              <a title="List Event Users" href="<?php echo base_url().'admin/banner/getEventsUsers?id='.$lobby->lobby_id;?>"><i class="fa fa-address-book fa-fw"></i></a>
            </td>
              <td>
                <div class="pull-left col-sm-12 banner_page">
                  <label class="switch"><input type="checkbox" name="is_" value="<?php echo $lobby->lobby_id; ?>" <?php if($lobby->is_spectator == 1) { echo 'checked'; } ?> class="is_spectator"><span class="slider round"></span></label>
                </div>
              </td>
              <td>              
              <a title="Update Spectate Ticket Amount" onclick="return changeEventprice('<?php echo $lobby->lobby_id; ?>','spectate_price');"><i class="fa fa-money fa-fw"></i></a>
              <a title="List Spectate Users" href="<?php echo base_url().'admin/banner/getSpectateUsers?id='.$lobby->lobby_id;?>"><i class="fa fa-address-book fa-fw"></i></a>
            </td>
            
          </tr>
          <?php }?>
        </tbody>
      </table>
    </div>
   
  </div>
</div>
</div>
</section>
<style>
.no_banner{
  width:15px;
  height:15px;
  background:red;
  border-radius:4px;
}
.yes_banner{
  width:15px;
  height:15px;
  background:green;
  border-radius:4px;
}
.myCheckbox input {
   visibility:hidden;
    position: absolute;
    z-index: -9999;
}
.myCheckbox span {
    width: 15px;
    height: 15px;
    display: block;
    background: red;
    border-radius:4px;
}
.myCheckbox input:checked + span {
    background: green;
}
</style>
<div class="modal fade  custom-width" id="modal-1">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">      
    </div>
  </div>
</div>

<script type="text/javascript">

function changeEventFee(){
  $.ajax({
    url : '<?php echo site_url(); ?>admin/Banner/getEventFee/',
    success: function(result) {
      if(result){
        $('.modal-content').html(result);
        $('#modal-1').modal('show');
        $(".err_msg").html('');
      } 
      }     
  });
}

function changeEventprice(id,type){
  $.ajax({
    url : '<?php echo site_url(); ?>admin/Banner/getEventPrice?id='+id+'&type='+type,
    success: function(result) {
      if(result){
        $('.modal-content').html(result);
        $('#modal-1').modal('show');
        $(".err_msg").html('');
        $("#event_price").blur(function(){
          var type = $("#price_type").val();
          var current_price = parseInt($("#event_price").val());
          var event_price = parseInt($("#old_event_price").val());
          var spectator_price = parseInt($("#old_spectator_price").val());
          
          if (type == "event_price") {
            if(current_price > spectator_price){
              $(".err_msg").html(''); 
              var event_fee = current_price * 10 /100;                 
              $(".event_fee").val(event_fee);             
            } else {
              $(".err_msg").html("Specatator Ticket price is "+spectator_price+" you have to enter price is more than spectator price");
              $("#event_price").val('');
            }
          } else {
            if(current_price < event_price){
              $(".err_msg").html('');               
            } else {
              $(".err_msg").html("Event Ticket price is "+event_price+" you have to enter price is less than event price");
              $("#event_price").val('');
            }
          }
        });
        $(".event_fee").blur(function(){ 
          var event_price = $("#event_price").val();
          var event_fee = $(".event_fee").val(); 
          if(parseInt(event_price) > parseInt(event_fee)){
            $(".fee_err_msg").html('');
          } else {
            $(".fee_err_msg").html("Please Enter fee less than price.");
            $(".event_fee").val('');
          }
        });
      }
    }
  });
}
$(".lobby_status").change(function() {
    if(this.checked) {      
      // $('.banner_page1').removeClass('block').addClass('none');       
      // $('.banner_page2').removeClass('none').addClass('block');
      var type = 'on';
  } else {
      // $('.banner_page1').removeClass('none').addClass('block');       
      // $('.banner_page2').removeClass('block').addClass('none');
      var type = 'off';
  }
  $.ajax({
      url : '<?php echo site_url(); ?>admin/Banner/updateSiteSettings?type='+type,
      success: function(result) {
        
      }
    });
});
$(document).ready(function() 
{
   $('.table-bordered').dataTable({
        aLengthMenu: [[25, 50, 75, -1], [25, 50, 75, "All"]],
        iDisplayLength: 25,
        stateSave: true,
    });
   // $('#example2').dataTable({
   //      "aLengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]],
   //      "iDisplayLength": 25,
   //      // "aLengthMenu": [[1, 2, 3, -1], [1, 2, 3, "All"]],
   //      // "iDisplayLength": 1 ,
   //      "stateSave": true,
   //  });
  // var table = $('#example2').DataTable();
  $('.is_spectator').click(function()
  { 
    var checked_val = $(this).prop("value");
    var type = "0"; 

    if($(this).prop("checked") == true)
    {
      var type = "1";  
    }
   
    $.ajax({
      url : '<?php echo site_url(); ?>admin/Banner/updateIsSpectator?id='+checked_val+'&type='+type+'&update_type=spectator',
      success: function(result) {
        window.location.href = "<?php echo site_url(); ?>admin/Banner/";
      }
    });
  });
  $('.is_event').click(function()
  { 
    var checked_val = $(this).prop("value");
    var type = "0";            
    if($(this).prop("checked") == true)
    {
      var type = "1";  
    }
   
    $.ajax({
      url : '<?php echo site_url(); ?>admin/Banner/updateIsSpectator?id='+checked_val+'&type='+type+'update_type=event',
      success: function(result) {
        window.location.href = "<?php echo site_url(); ?>admin/Banner/";
      }
    });
  });
});

// $(document).on('submit','#change_event_price',function(e){
//   e.preventDefault();
//   var data = $(this).serialize();
//   if(data){
//     $.ajax({
//       url: "<?php echo site_url(); ?>admin/Banner/updateIsEventPrice",
//       data: data,
//       type: 'POST',
//       success: function(result) {
//         // window.location.href = "<?php echo site_url(); ?>admin/Banner/";       
//       }
//     });
//   }             
// });
  $(document).on('click','.view',function(){
  var id = $(this).attr('id');
  if(id){
    $.ajax({
      url : '<?php echo site_url(); ?>admin/Banner/ChangeBannerOrder/?id='+id,
      success: function(result) {
        if(result){
          $('.modal-content').html(result);
          $('#modal-1').modal('show');                              

        }
      }
    });
  }
});
   $(document).on('click','.lobby_view',function(){
  var id = $(this).attr('id');
  if(id){
    $.ajax({
      url : '<?php echo site_url(); ?>admin/Banner/ChangeLobbyOrder/?id='+id,
      success: function(result) {
        if(result){
          $('.modal-content').html(result);
          $('#modal-1').modal('show');                              

        }
      }
    });
  }
});

$(document).on('submit','#change_banner_order',function(e){
  e.preventDefault();
  var data = $(this).serialize();
  if(data){
    $.ajax({
      url: "<?php echo site_url(); ?>admin/Banner/UpdateBannerOrder",
      data: data,
      type: 'POST',
      success: function(result) {
        window.location.href = "<?php echo site_url(); ?>admin/Banner?type=banner";       
      }
    });
  }             
});
$(document).on('submit','#change_lobby_order',function(e){
  e.preventDefault();
  var data = $(this).serialize();
  if(data){
    $.ajax({
      url: "<?php echo site_url(); ?>admin/Banner/UpdateLobbyOrder",
      data: data,
      type: 'POST',
      success: function(result) {
        window.location.href = "<?php echo site_url(); ?>admin/Banner/";       
      }
    });
  }             
});

</script>
