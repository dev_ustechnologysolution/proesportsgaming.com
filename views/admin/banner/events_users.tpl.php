
<section class="content">
  <div class="row">
    <div class="col-md-12">
		<?php $this->load->view('admin/common/show_message') ?>
        <div class="box box-primary">
         <button onclick="myFunction('<?php echo $lobby_id; ?>')" class="mt20 btn btn-primary"> <span class="glyphicon glyphicon-refresh"></span> Reset</button> 
            <a href="<?php echo base_url('admin/banner')?>" class="pull-right mt20 btn btn-primary"> <span class="glyphicon glyphicon-chevron-left"></span>Back</a>
            <div class="box-body">
                <p>Event Users list</p>                
                <table id="example1" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Sr No.</th>
                            <th>Acc#</th>
                            <!-- <th>Amount Of Ticket($)</th> -->
                            <th>Total Cost</th>
                            <!-- <th>Pro Fees Collected</th> -->
                            <th>Fee</th>
                            <th>Prize</th>
                            <th>Spectate Collected</th>
                            <th>Fee Collected Acc#</th>
                            <th>User Name</th>
                            <th>Display Name</th>
                            <th>Team Name</th>
                            <th>Email</th>
                            <th>Is Gifted</th>
                            <th>Gifted User</th>
                            <th>Gifted User Acc#</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $i = 1;
                        foreach($events_users as  $user) { 
                          $fee = $user->event_price * $user->event_fee/100;
                          $pay_amount = $user->event_price - $fee;
                          $event_fee_creator = $this->General_model->view_single_row('user_detail','user_id',$user->event_fee_creator);
                          // echo "<pre>"; print_r($user);
                            if($user->is_gift == 1){
                                $is_gift = 'Yes';
                                $gift_user = $this->General_model->view_single_row('user_detail','user_id',$user->gift_to); 
                                $gift_user_name = ucwords($gift_user['name']);
                                $gift_user_acc = $gift_user['account_no'];
                            } else {
                                $is_gift = 'No';
                                $gift_to = '-';
                                $gift_user_name = '';
                                $gift_user_acc = '';
                            }
                           ?>                            
                         <tr>
                            <td><?php echo $i++; ?></td> 
                            <td><?php echo $user->account_no; ?></td> 
                            <td><?php echo $user->event_price; ?></td> 
                            <td><?php echo $fee; ?></td>
                            <td><?php echo $pay_amount; ?></td>
                            <td><?php echo $user->spectator_fee;?></td>
                            <td><?php echo $user->event_fee_creator;?></td>
                            <td><?php echo $user->name;?></td>
                            <td><?php echo $user->display_name;?></td> 
                            <td><?php echo $user->team_name;?></td>
                            <td><?php echo $user->email;?></td>
                            <td><?php echo ($user->is_gift == 1) ? 'Yes' : 'No';?></td>
                            <td><?php echo $gift_user_name; ?></td>
                            <td><?php echo $gift_user_acc; ?></td>
                            <td><?php $date_arr = explode(' ',$user->created); echo $date_arr[0];?></td>                            
                        </tr>
                        <?php  }?> 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
   </div>
</section>
<div class="modal fade  custom-width" id="modal-1">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">
      
    </div>
  </div>
</div>
<style>
    .mt20{
        margin-top: 20px;
    }
</style>
<script type="text/javascript">
    function myFunction(id){
        $.ajax({
            url : '<?php echo site_url(); ?>admin/banner/resetLobbyUser/'+id,
            success: function(result) {
                if(result){
                   $('.modal-content').html(result);
                   $('#modal-1').modal('show');                              
                }
            }
        })
    }
    $(document).on('submit','#security_password_id',function(e){
      e.preventDefault();
      var data = $(this).serialize();
      if(data){
        $.ajax({
          url: "<?php echo site_url(); ?>admin/banner/eventUserDelete",
          data: data,
          type: 'POST',
          success: function(result) {
            console.log(result);
            if(result == true){
              window.location.href = "<?php echo site_url(); ?>admin/banner/";
            } else {
              $('#security_password').val('');
              alert('Password Error');
              return false;
            }
                  
          }
        });
      }             
    })
</script>

