<section class="content">
  <div class="row">
    <div class="col-md-12">
		<?php $this->load->view('admin/common/show_message') ?>
        <div class="box box-primary"> 
            <a href="<?php echo base_url('admin/banner')?>" class="pull-right mt20 btn btn-primary"> <span class="glyphicon glyphicon-chevron-left"></span>Back</a>
            <div class="box-body">
                <p><?php echo $live_lobby_name.' Live Stream List';?></p>
                <form id="form1" method=post action=check.php>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Left Stream channel</th>
                        <th class="border-right">Left Player Name</th>
                        <th>Is Banner</th>
                        <th>Right Stream channel</th>
                        <th class="border-right">Right Player Name</th>
                        <th>Is Banner</th>
                        <th>Link</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php 
                        foreach($live_lobby_stream_data as $k => $stream) {
                            ?>
                            
                         <tr>
                            <?php if($stream->table_cat == 'LEFT'){?>
                       
                            <td><?php echo ($stream->display_name == '') ? $stream->name :$stream->display_name;?></td> 
                            <td class="border-right"><?php echo $stream->stream_channel;?></td>
                            <td><label class="myCheckbox"><input <?php echo ($stream->is_banner == 1) ? 'checked' :'';?> id="is_banner<?php echo $k;?>" class="single-checkbox custom-cb" type="checkbox" name="progress" value="<?php echo $stream->id;?>"><span></span></label></td> 
                            <td></td>
                            <td></td>
                            <td></td>
                            
                        
                        <?php } else { ?>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><?php echo ($stream->display_name == '') ? $stream->name :$stream->display_name;?></td> 
                            <td class="border-right"><?php echo $stream->stream_channel;?></td>
                            <td valign="top"><label class="myCheckbox"><input <?php echo ($stream->is_banner == 1) ? 'checked' :'';?> id="is_banner<?php echo $k;?>" class="single-checkbox custom-cb" type="checkbox" name="progress" value="<?php echo $stream->id;?>"><span></span></label></td> 
                    <?php }?>
                    <td><a target="_blank" class="btn btn-primary" href="<?php echo base_url('livelobby/watchLobby/'.$stream->lobby_id); ?>">Live Lobby</td> 
                   </tr>
                    <?php  }?> 
                    </tbody>
                </table>
                </form>
            </div>
           
        </div>
    </div>
   </div>
</section>
<style>
    .mt20{
        margin-top: 20px;
    }
</style>
<script>
$(document).ready(function(){
    
    var $checkboxes = $('#form1 td input[type="checkbox"]');
    
        $('input[type="checkbox"]').click(function()
        {            
            var countCheckedCheckboxes = $checkboxes.filter(':checked').length;            
            var checked_val = $(this).prop("value");
            
            if($(this).prop("checked") == true){              
                
                if(countCheckedCheckboxes > 2)
                {
                    alert("Please Select only Two") 
                    $(this).prop("checked") == false;
                    return false;
                } else {
                    var type = 'check';
                }
                
            }
            else if($(this).prop("checked") == false){
                
               

                var type = 'uncheck';
               
            }
            $.ajax({
                url : '<?php echo site_url(); ?>admin/Banner/updateIsBanner?type='+type+'&id='+checked_val,
                success: function(result) {                   
                    // window.location = window.location;                    
                   
                }
            });
            
        });
    });


</script>