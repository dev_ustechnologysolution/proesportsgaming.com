
<section class="content">
  <div class="row">
    <div class="col-md-12">
		<?php $this->load->view('admin/common/show_message') ?>
        <div class="box box-primary">
         <!-- <button onclick="myFunction('<?php echo $lobby_id; ?>')" class="mt20 btn btn-primary"> <span class="glyphicon glyphicon-refresh"></span> Reset</button>  -->
            <a href="<?php echo base_url('admin/banner')?>" class="pull-right mt20 btn btn-primary"> <span class="glyphicon glyphicon-chevron-left"></span>Back</a>
            <div class="box-body">
                <p>Spectate Users list</p>                
                <table id="example1" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Sr No.</th>
                            <th>Acc#</th>
                            <th>Amount Of Ticket($)</th>
                            <th>User Name</th>
                            <th>Display Name</th>
                            <th>Team Name</th>
                            <th>Email</th>                           
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $i = 1;
                        foreach($events_users as  $user) { ?>                            
                         <tr>
                            <td><?php echo $i++; ?></td> 
                            <td><?php echo $user->account_no; ?></td> 
                            <td><?php echo $user->spectate_price; ?></td> 
                            <td><?php echo $user->name;?></td>
                            <td><?php echo $user->display_name;?></td> 
                            <td><?php echo $user->team_name;?></td>
                            <td><?php echo $user->email;?></td>                           
                            <td><?php $date_arr = explode(' ',$user->created); echo $date_arr[0];?></td>                            
                        </tr>
                        <?php  }?> 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
   </div>
</section>
<div class="modal fade  custom-width" id="modal-1">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">
      
    </div>
  </div>
</div>
<style>
    .mt20{
        margin-top: 20px;
    }
</style>
<script type="text/javascript">
    function myFunction(id){
        $.ajax({
            url : '<?php echo site_url(); ?>admin/banner/resetLobbyUser/'+id+'type=spectate',
            success: function(result) {
                if(result){
                   $('.modal-content').html(result);
                   $('#modal-1').modal('show');                              
                }
            }
        })
    }
    $(document).on('submit','#security_password_id',function(e){
      e.preventDefault();
      var data = $(this).serialize();
      if(data){
        $.ajax({
          url: "<?php echo site_url(); ?>admin/banner/eventUserDelete",
          data: data,
          type: 'POST',
          success: function(result) {
            console.log(result);
            if(result == true){
              window.location.href = "<?php echo site_url(); ?>admin/banner/";
            } else {
              $('#security_password').val('');
              alert('Password Error');
              return false;
            }
                  
          }
        });
      }             
    })
</script>

