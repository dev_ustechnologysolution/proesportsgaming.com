<section class="content" style="margin-top:30px;">
  
   
  <?php $this->load->view('admin/common/show_message') ?> 
 
  <div class="box banner_page">
    <div class="box-header"> 
     <a href="<?php echo base_url().'admin/categories/categoriesAdd?type=0'; ?>"><button style="margin: 0px 12px;" class="btn pull-right btn-primary btn-xl">Add Sub Category</button></a>     
      <a href="<?php echo base_url().'admin/categories/categoriesAdd?type=1'; ?>"><button class="btn pull-right btn-primary btn-xl">Add Category</button></a>
    </div>
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <!-- <th>Sr.No</th> -->
            <th>Cateory Order</th>
            <th>Parent Category</th>
            <th>Category Title</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; foreach($list as $k => $category){ 
            if($category['parent_id'] == 0){
              $main_category['name'] = 'Main Category';
              $m_category = '';
              $type = 1;
            } else {
              $main_category['name'] = 'Sub Category';
              $m_category = $this->General_model->view_single_row('product_categories_tbl','id',$category['parent_id']); 
              $type = 0;
            }
            
            ?>          
          <tr>
            <!-- <td><?php echo $i++; ?></td> -->
            <td><?php echo ($category['sort_num'] == 0) ? '' : $category['sort_num']; ?>&nbsp;&nbsp;<a href="#" class="lobby_view" id="<?php echo $category['id']; ?>"><i class="fa fa-pencil fa-fw"></i></a></td>
            <td><?php echo $main_category['name']; ?></td>
            
            <td><?php echo ($m_category['name']!= '') ? $m_category['name'].' | '.$category['name'] : $category['name']; ?></td>
             <td><label class="myCheckbox"><input <?php echo ($category['status'] == 1) ? 'checked' :'';?> id="is_banner<?php echo $k;?>" class="single-checkbox custom-cb" type="checkbox" name="progress" value="<?php echo $category['id'];?>"><span></span></label></td> 
            <td>
              <a title="Edit Category" href="<?php echo base_url().'admin/categories/categoriesEdit?type='.$type.'&id='.$category['id']; ?>">
                <i class="fa fa-pencil fa-fw"></i>
              </a>
               <a title="Delete Category" href="<?php echo base_url().'admin/categories/delete?id='.$category['id']; ?>">
                <i class="fa fa-trash fa-fw"></i>
              </a>
            </td>
          </tr>
          <?php }?>
        </tbody>
      </table>
    </div>
   
  </div>
</section>
<style>
.no_banner{
  width:15px;
  height:15px;
  background:red;
  border-radius:4px;
}
.yes_banner{
  width:15px;
  height:15px;
  background:green;
  border-radius:4px;
}
.myCheckbox input {
   visibility:hidden;
    position: absolute;
    z-index: -9999;
}
.myCheckbox span {
    width: 15px;
    height: 15px;
    display: block;
    background: red;
    border-radius:4px;
}
.myCheckbox input:checked + span {
    background: green;
}
</style>
<div class="modal fade  custom-width" id="modal-1">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">      
    </div>
  </div>
</div>

<script type="text/javascript">

   $(document).on('click','.lobby_view',function(){
  var id = $(this).attr('id');
  if(id){
    $.ajax({
      url : '<?php echo site_url(); ?>admin/Categories/ChangeCategoryOrder/?id='+id,
      success: function(result) {
        if(result){
          $('.modal-content').html(result);
          $('#modal-1').modal('show');                              

        }
      }
    });
  }
});
$(document).on('submit','#change_lobby_order',function(e){
  e.preventDefault();
  var data = $(this).serialize();
  if(data){
    $.ajax({
      url: "<?php echo site_url(); ?>admin/Categories/UpdateCatOrder",
      data: data,
      type: 'POST',
      success: function(result) {
        window.location.href = "<?php echo site_url(); ?>admin/Categories/";       
      }
    });
  }             
});
</script>
<script>
$(document).ready(function(){
    
    var $checkboxes = $('td input[type="checkbox"]');
    
        $('input[type="checkbox"]').click(function()
        {            
            var countCheckedCheckboxes = $checkboxes.filter(':checked').length;            
            var checked_val = $(this).prop("value");
            
            if($(this).prop("checked") == true){             
                
                
                    var type = 'check';
            }
            else if($(this).prop("checked") == false){
                var type = 'uncheck';
            }
            $.ajax({
                url : '<?php echo site_url(); ?>admin/categories/updateIsStatus?type='+type+'&id='+checked_val,
                success: function(result) {                   
                    // window.location = window.location;                    
                   
                }
            });
            
        });
    });


</script>