
<section class="content background-white box box-primary">

  <div class="row">

    <div class="col-md-12">
    	
    		<form action="<?php echo base_url();?>admin/chat/create_new_agent" method="POST" enctype='multipart/form-data'>
			  <div class="form-group">
			    <label>Full Name:</label>
			    <input type="text" class="form-control" id="full_name" name="full_name" required placeholder="Enter Name">
			  </div>
			   <div class="form-group">
			    <label>Email address:</label>
			    <input type="email" class="form-control" id="email" name="email" required placeholder="Enter Email">
			  </div>
			   <div class="form-group">
			    <label>Phone Number:</label>
			    <input type="tel" class="form-control" id="phone_num" name="phone_num" required placeholder="Enter Phone Number">
			  </div>
			   <div class="form-group">
			    <label>Location:</label>
			    <input type="text" class="form-control" id="location" name="location" required placeholder="Enter Location">
			  </div>
			   <div class="form-group">
			    <label>Password:</label>
			    <input type="password" class="form-control" id="pwd" name="pwd" required placeholder="Enter Password">
			  </div>
			  <div class="form-group">
			    <label>Agent Type:</label>
			    <div class="row">
				<span class="label-text col-sm-2">Admin</span>
				<input type="radio" class="col-sm-1" name="agent_type" value="Admin" checked><div class="col-sm-9"></div>
				</div>
				<div class="row">
				<span class="label-text col-sm-2">Tech Specialist</span>
				<input type="radio" class="col-sm-1" name="agent_type" value="Tech Specialist"><div class="col-sm-9"></div>
				</div>
				<div class="row">
				<span class="label-text col-sm-2">Customer Service</span>
				<input type="radio" class="col-sm-1" name="agent_type" value="Customer Service"> <div class="col-sm-9"></div>
				</div>
			  </div>
			   <div class="form-group">
			    <label>Notes:</label>
			    <textarea class="form-control" id="notes" name="notes"></textarea>
			  </div>
			  <div class="form-group">
			    <label>Game Category:</label>
			    <div class="row">
				<span class="label-text col-sm-2">Playstation 4</span>
				<input type="radio" class="col-sm-1" name="game_category" value="Playstation 4" checked><div class="col-sm-9"></div>
				</div>
				<div class="row">
				<span class="label-text col-sm-2">Xbox One</span>
				<input type="radio" class="col-sm-1" name="game_category" value="Xbox One"><div class="col-sm-9"></div>
				</div>
				<div class="row">
				<span class="label-text col-sm-2">Pc</span>
				<input type="radio" class="col-sm-1" name="game_category" value="Pc"> <div class="col-sm-9"></div>
				</div>
				<div class="row">
				<span class="label-text col-sm-2">Tournaments</span>
				<input type="radio" class="col-sm-1" name="game_category" value="Tournaments"> <div class="col-sm-9"></div>
				</div>
			  </div>
			 
			  <input type="submit" name="submit" class="btn btn-default" value="Submit">
			</form>

    </div>

  </div>

</section>