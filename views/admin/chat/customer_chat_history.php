<section class="content background-white">
   <div class="row">
      <div class="col-md-12">
         <div class="row">
           <div class="agent_navigation pull-right">
            <form method="POST" action="<?php echo base_url(); ?>admin/chat/export_all_data">
              <input type="reset" name="reser" value="RESET" class="reset_history">
            <input type="submit" name="export" value="DOWNLOAD HISTORY" class="download_history"><a href="" class="" onclick="location.reload()">Refresh</a>
            </form>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 xwb-main-contact-conversation">
               <div class="table-responsive agent_table">
                  <table class="table table-hover" id="example1">
                     <thead>
                        <tr class="active">
                           <th>Sl No.</th>
                           <th>Acc. No.</th>
                           <th>Customer Service Online</th>
                           <th>Agent Type</th>
                           <th>Notes</th>
                           <th>E-Mail</th>
                           <th>Ph.#</th>
                           <th>Game Category</th>
                           <th>Login Time</th>
                           <th>Logout Time</th>
                           <th>Total Time</th>
                        </tr>
                     </thead>
                     <tbody>
                      <?php
                           $CI =& get_instance();
                           $result = $CI->chatsocket->getDistAgentHist();
                           if ($result['status'] == true) {
                               $agent_data = $result['agent_data'];
                               $i = 1;
                               foreach ($agent_data as $key => $value) 
                               {   
                                   $message_page .= '<tr>
                                           <td>'.$i.'</td>
                                           <td style="display: inline-flex;">'. $value->id .'<form method="POST" action="'.base_url().'admin/chat/single_agent_history_export/?agent_id='.$value->agent_id.'">
                                           <input type="hidden" class="unique_agnet_id" value = "'.$value->agent_id.'">
                                           <button type="submit" name="submit" class="btn btn-info" value="'.$value->agent_id.'" style="width: 100%;margin: 0px 0px 0 4px;padding: 0px 1px;">
                                             <i class="fa fa-file"></i></button>
                                              </form></td>
                                           <td>' . $value->name . '</td>
                                           <td>'.$value->agent_type.'</td>
                                           <td>'.$value->notes.'</td>
                                           <td>'.$value->email.'</td>
                                           <td>'.$value->phone.'</td>
                                           <td>'.$value->game_category.'</td>
                                           <td>'.$value->login_time.'</td>
                                           <td>'.$value->logout_time.'</td>
                                           <td>'.$value->total_time.'</td>
                                           </tr>';
                                           $i++;
                               }
                                   echo $message_page;
                               // $message_page .= csListUsers();
                           } 
                           else {
                                   //  $message_page = '<tr>
                                   //   <td colspan="10" align="center"> No Data Available </td>
                                   //   </tr>';
                                   // echo $message_page;
                           }
                           ?>
                     </tbody>
                  </table>
               </div>
            </div>
           <div class="modal fade  custom-width" id="modal-1">
            <div class="modal-dialog" style="width: 50%;">
              <div class="modal-content">
                
              </div>
            </div>
          </div>
          <script> 
            $(document).on('click','.reset_history',function(){
              var id = $('.unique_agnet_id').val();
              if(id){
                $.ajax({
                  url : '<?php echo site_url(); ?>admin/chat/security_password_data/'+id,
                  success: function(result) {
                    if(result){
                      $('.modal-content').html(result);
                      $('#modal-1').modal('show');                              
                    }
                  }
                })
              }
            })
          $(document).on('submit','#security_id',function(e){
            e.preventDefault();
            var data = $(this).serialize();
              $.ajax({
                url: "<?php echo site_url(); ?>admin/chat/reset_all_agent_data",
                data: data,
                type: 'POST',
                success: function(data_value) {
                if(data_value == '1')
                {
                  window.location.href = "<?php echo site_url(); ?>admin/chat/cust_chat_history";
                }
                else
                {
                  alert("Password Error");
                }
              }
              });
          });
          </script> 
         </div>
      </div>
   </div>
</section>


