
<section class="content background-white box box-primary">
  <div class="row">
    <div class="col-md-12"> 
<form action="<?php echo base_url();?>admin/chatads/edit_pvpads" method="POST" enctype='multipart/form-data' class="add_ads" onsubmit="return adsimage_size_validation()">

  <div class="row">
          <div class="form-group col-sm-6">
            <div class="col-sm-1">
              <input type="radio" name="selected_commercial" id="edit_selected_youtube_ads" value="selected_youtube" <?php if($ads_single_row->youtube_link != '') echo 'checked class="checked"'?>>
            </div>
            <div class="col-sm-11">
            <div>
            <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $ads_single_row->id; ?>">
            <label>Youtube Link :</label>
            <input type="url" class="form-control" id="youtube_link" name="youtube_link" placeholder="Enter Youtube Link" value="<?php echo $ads_single_row->youtube_link; ?>" style="display: none;">
            <label id="upload_youtube_lable" style="display: none;">Youtube Link Title:</label>
            <input type="text" class="form-control" id="youtube_link_title" name="youtube_link_title" placeholder="Enter Youtube Link Title" value="<?php echo $ads_single_row->youtube_link_title; ?>" style="display: none;">
          </div>
      </div>
    </div>
      <div class="form-group col-sm-6">
            <div class="col-sm-1">
              <input type="radio" name="selected_commercial" id="edit_selected_commercial_ads" value="selected_commercial"  <?php if($ads_single_row->upload_commercial != '') echo 'checked class="checked"'?>>
            </div>
            <div class="col-sm-11">
              <label>Upload Commercial:</label><a style="float: right; cursor: pointer; display: none;" id="change_upload_commercial">Change Image</a>
              <input type="file" class="form-control" id="upload_commercial" name="upload_commercial" accept="video/mp4,video/3gp,video/ogg" style="display: none;">
              <input type="hidden" class="form-control" name="upload_commercial_vid" id="upload_commercial_vid" value="<?php echo $ads_single_row->upload_commercial; ?>">
              <label style="display: none;" id="upload_commercial_lable">Commercial Title:</label>
              <input type="text" class="form-control" id="commercial_title" name="commercial_title" value="<?php echo $ads_single_row->commercial_title; ?>" style="display: none;">
              <div id="old_commercial_file" style="display: none;"><?php echo $ads_single_row->upload_commercial; ?></div>
           <div>
         </div>
        </div>
      </div>
     </div>
     <div class="form-group">
     <label>Upload Banner Title:</label>
            <input type="text" class="form-control" id="upload_banner_title" name="upload_banner_title" value="<?php echo $ads_single_row->upload_banner_title; ?>" required>
            <label>Upload Banner: (200px * 300px)</label>
            <label id="banner_size" style="display:none"></label>
            <input type="file" class="form-control" id="upload_banner" name="upload_banner" value="<?php echo $ads_single_row->upload_banner; ?>" accept="image/*">
            <input type="hidden" name="upload_banner_image" value="<?php echo $ads_single_row->upload_banner; ?>">          
             <img src="<?php echo base_url().'upload/ads_banner/'.$ads_single_row->upload_banner ?>" height="150" width="150" alt="Banner">
     </div>
   <div class="form-group">
    <label>Banner Link Title:</label>
    <input type="text" class="form-control" id="banner_link_title" name="banner_link_title" value="<?php echo $ads_single_row->banner_link_title; ?>" required>
    <label>Banner Link:</label>
    <input type="text" class="form-control" id="banner_link" name="banner_link" value="<?php echo $ads_single_row->banner_link; ?>" required> 
  </div>
   <div class="form-group">
    <label>Sponsered By Logo Title:</label>
    <input type="text" class="form-control" id="sponsered_by_logo_title" name="sponsered_by_logo_title" value="<?php echo $ads_single_row->sponsered_by_logo_title; ?>" required>
    <label>Sponsered By Logo Link:</label>
    <input type="text" class="form-control" id="sponsered_by_logo_link" name="sponsered_by_logo_link" value="<?php echo $ads_single_row->sponsered_by_logo_link; ?>" required>
    <label>Upload Sponsered By Logo: (120px * 80px)</label>
    <label id="sponsered_by_logo_size" style="display:none"></label>
    <input type="file" class="form-control" id="upload_sponsered_by_logo" name="upload_sponsered_by_logo" value="" accept="image/*">		
    <img src="<?php echo base_url().'upload/ads_sponsered_by_logo/'.$ads_single_row->upload_sponsered_by_logo ?>" height="150" width="150" alt="Sponsered By Logo">
    <input type="hidden" class="form-control" name="upload_sponsered_by_logo_image" value="<?php echo $ads_single_row->upload_sponsered_by_logo; ?>"> 	    
  </div>
   <div class="form-group">
    <label>10 Sec Intro Title:</label>
    <input type="text" class="form-control" id="ten_sec_intro_title" name="ten_sec_intro_title" value="<?php echo $ads_single_row->ten_sec_intro_title; ?>" required>
    <label>Upload 10 Sec Intro:</label>
    <input type="file" class="form-control" id="upload_ten_sec_intro" name="upload_ten_sec_intro" value="<?php echo $ads_single_row->upload_ten_sec_intro; ?>" accept="video/mp4,video/3gp,video/ogg"><span> <?php echo $ads_single_row->upload_ten_sec_intro; ?></span>
    <input type="hidden" class="form-control" name="upload_ten_sec_intro_vid" value="<?php echo $ads_single_row->upload_ten_sec_intro; ?>">
    
  </div>
   
  <div class="form-group">
  <label>ADs Category :</label>
	<div class="row">
	<span class="label-text col-sm-2">PvP Ads</span>
	<input type="radio" class="col-sm-1" <?php if($ads_single_row->ads_category == 'pvp') echo "checked"; ?> name="ads_category" value="pvp" ><div class="col-sm-9"></div>
	</div>
	<div class="row">
	<span class="label-text col-sm-2">Cust Ads</span>
	<input type="radio" class="col-sm-1" <?php if($ads_single_row->ads_category == 'custads') echo "checked"; ?> name="ads_category" value="custads"><div class="col-sm-9"></div>
	</div>
  </div>

    <div class="form-group">
    <label>Autoskipp Time:</label> <input type="number" class="form-control" id="autoskipp_time" name="autoskipp_time" required value="<?php echo $ads_single_row->autoskipp_time;?>">
  </div>
 </div>
  </div>
 
  <input type="submit" name="submit" class="btn btn-default" value="Submit">
</form>

    </div>

  </div>

</section>