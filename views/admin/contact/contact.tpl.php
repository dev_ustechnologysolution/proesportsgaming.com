<section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
			<?php $this->load->view('admin/common/show_message') ?>
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add new contact</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form  action="<?php echo base_url(); ?>admin/contact/add_contact" method="post">
                <div class="box-body">
                 <div class="form-group">
                      <label for="name">Name</label>
                      <input type="text" placeholder="Name" id="name" class="form-control" name="name" required="required">
                    </div>
					 <div class="form-group">
                      <label for="email">Email</label>
                      <input type="email" placeholder="Email" id="email" class="form-control" name="email" required="required">
                    </div>
					<div class="form-group">
                      <label for="phone">Phone</label>
                      <input type="text" placeholder="Phone" id="phone" class="form-control" name="phone" required="required">
                    </div>
                     </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button class="btn btn-primary" type="submit">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

              <!-- Form Element sizes -->
                </div><!--/.col (left) -->
            
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>