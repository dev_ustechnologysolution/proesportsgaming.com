<section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
			<?php $this->load->view('admin/common/show_message') ?>
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit contact</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php if(count($contact)>0){?>
                <form  action="<?php echo base_url(); ?>admin/contact/contact_update" method="post">
                <div class="box-body">
                <div class="form-group">
                 
                      <label for="name">Name</label>
                      <input type="text" placeholder="Name" id="name" class="form-control" name="name" required="required" value="<?php echo $contact[0]['name'];?>">
                    </div>
					 <div class="form-group">
                      <label for="email">Email</label>
                      <input type="email" placeholder="Email" id="email" class="form-control" name="email" required="required" value="<?php echo $contact[0]['email'];?>">
                    </div>
					<div class="form-group">
                      <label for="phone">Phone</label>
                      <input type="text" placeholder="Phone" id="phone" class="form-control" name="phone" required="required" value="<?php echo $contact[0]['phone'];?>">
                    </div>
                     </div><!-- /.box-body -->

                  <div class="box-footer">
                   <input type="hidden" name="id" value="<?php echo $contact[0]['id'];?>">
                   <button class="btn btn-primary" type="submit">Submit</button>
                  </div>
                </form>
                 <?php } else {?>
                 <h4>Data not found</h4>
                 <?php }?>
              </div><!-- /.box -->

              <!-- Form Element sizes -->
                </div><!--/.col (left) -->
            
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>