    <div class="col-sm-12 cover_page_top_div">
      <div class="pull-left status_button">
        <label class="switch"><input type="checkbox" name="cover_page_status" value="cover_page_status" <?php if($list[0]['status'] == '1') { echo 'checked'; } ?> class="cover_page_status"><span class="slider round"></span></label>
      </div>
      <input type="hidden" class="enable_id" value="<?php echo $list[0]['status']; ?>">
      <div class="pull-right">
        <a href="" onclick="location.reload()"> Refresh </a>
      </div>
    </div>

<div class="clearfix"></div>
<section class="content">
  <?php $this->load->view('admin/common/show_message') ?>
  <div class="box box-primary custom_map_video" <?php if($list[0]['status'] == '0') { echo 'style="display: none;"'; } ?>>
        <table id="" class="table table-bordered table-striped">

      <thead>

        <tr>

          <th>Sl No</th>

          <th class="admin_map_th">Cover Password</th>

          <th>Action</th>

        </tr>

      </thead>

      <tbody>
        
         <tr>

          <td><?php echo $list[0]['id']; ?></td>
          <td>
            <?php echo $list[0]['password']; ?>
          </td>
          <td>
            <!-- href="<?php // echo base_url().'admin/content/cover_password_edit/'.$list[0]['id'];?>" -->
            <a id="<?php echo $list[0]['id'];?>" class="edit_pw"><i class="fa fa-pencil fa-fw"></i></a></td>
        </tr>

      </tbody>

    </table>

        </div>

</section>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script> $.validate();</script>
<!-- Modal 1 (Basic)-->
<div class="modal fade  custom-width" id="modal-pw">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">
      
    </div>
  </div>
</div>
<script>
$(document).on('click','.edit_pw',function(e){ 
  e.preventDefault();
  var id = $(this).attr('id');
  //var id = $(this).attr('href');
  if(id){
    $.ajax({
      url : '<?php echo site_url(); ?>admin/content/cover_password_edit/'+id,
      success: function(result) {
        if(result){
          $('.modal-content').html(result);
          $('#modal-pw').modal('show');                              
        }
      }
    })
  }
})

$(document).on('submit','#cover_pw_id',function(e){
  e.preventDefault();
  var data = $(this).serialize();
  // var action_value = $('#action_value').val();
  if(data){
    $.ajax({
      url: "<?php echo site_url(); ?>admin/content/cover_password_change",
      data: data,
     type: 'POST',
     success: function(result) {
        $('.modal-content').html(result);
        $('#modal-1').modal('show');        
     }
    });
  }             
})

$(document).on('submit','#editpw_id',function(e){
  e.preventDefault();
  var data = $(this).serialize();
  if(data){
    $.ajax({
      url: "<?php echo base_url();?>admin/content/update_cover_pw",
      data: data,
      type: 'POST',
      success: function(result) {
        window.location.href = "<?php echo base_url();?>admin/content/cover_password/";        
      }
    });
  }             
});

</script>