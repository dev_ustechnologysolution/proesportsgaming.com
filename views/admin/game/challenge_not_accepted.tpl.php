<section class="content">
  <div class="box">
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th style="display:none"></th>
            <th>Game Name</th>
            <th>Challenger Name</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($challenge as $value){?>
          <tr>
            <td style="display:none"></td>
            <td><?php echo $value['game_name']?></td>
            <td><?php echo $value['name']?></td>
            <td><?php if($value['is_accept']==0) { 
              echo '<span style="color:red">No one accept the challenge</span>';
            }?></td>
          </tr>
          <?php }?>
        </tbody>
      </table>
    </div>
  </div>
</section>