<section class="content">
	<div class="row">
		<div class="col-md-12">
			<?php $this->load->view('admin/common/show_message'); ?>
			<div class="box box-primary">
				<form  action="<?php echo base_url(); ?>admin/game/gameCatUpdate" method="post" enctype="multipart/form-data" class="game_category_name">
					<div class="box-body">
						<div class="form-group">
							<label for="name">Game Catedory Name</label>
							<input type="text" placeholder="Game Catedory Name" id="category_name" class="form-control" name="category_name" required="required" value="<?php echo ($game_cat_detail[0]['category_name']) ? $game_cat_detail[0]['category_name'] : ''; ?>" pattern="[A-Za-z0-9-_ ]+" title="Allow letters, numbers, hyphens, underscores and space Only">
							<label class="alert alert-danger already_exist" style="display: none;"></label>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<input type="hidden" name="old_game_cat_name" value="<?php if(isset($game_cat_detail[0]['category_name'])) echo $game_cat_detail[0]['category_name'];?>" class="old_game_cat_name">
						<input type="hidden" name="id" value="<?php if(isset($game_cat_detail[0]['id'])) echo $game_cat_detail[0]['id'];?>">
						<button class="btn btn-primary" id="game_add" type="submit">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>