<section class="content">
  <?php $this->load->view('admin/common/show_message'); ?>
  <div class="box">
    <div class="box-header">
      <a href="<?php echo base_url().'admin/game/gameCatAdd' ?>">
        <button class="btn btn-primary" style="float:right;margin-right: 7px;">Add Game Category</button>
      </a>
    </div>
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Sl No</th>
            <th>Game Category Name</th>
      			<th>Game Category slug</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; foreach($list as $value) { ?>
          <tr>
            <td><?php echo $i++; ?></td>
            <td><?php echo $value['category_name']?></td>
            <td><?php echo $value['cat_slug'];?></td>
            <td><a href="<?php echo base_url().'admin/game/gameCatEdit/'.$value['id'];?>"><i class="fa fa-pencil fa-fw"></i></a>|<a class="delete-data" id="<?php echo $value['id']; ?>" action="delete_game_category"><i class="fa fa-trash-o fa-fw"></i></a></td>
           </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</section>