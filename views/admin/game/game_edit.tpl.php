<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php $this->load->view('admin/common/show_message') ?>
            <div class="box box-primary">
                <?php if (count($game) > 0) { ?>
                    <form  action="<?php echo base_url(); ?>admin/game/gameUpdate" method="post" enctype="multipart/form-data" class="admin_game_edit_form">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Game Name</label>
                                <input type="text" id="name" class="form-control game-name" name="name" value="<?php if(isset($game[0]['game_name'])) echo $game[0]['game_name'];?>" onkeyup="validate_game()" onkeydown="validate_game()"/>
                                <label class="alert alert-danger already_exist" style="display: none;"></label>
                            </div>
                            <div class="form-group">
                                <label for="description">Game Description</label>
                                <textarea class="form-control" id="description" placeholder="Game Description" name="description" rows="8" cols="8"><?php if(isset($game[0]['game_description'])) echo $game[0]['game_description'];?></textarea>
                            </div>
                            <div class="form-group">
                            <label for="file">Image </label>
                                <label id="image_size" style="display:none"></label>
                                <input type="file" class="form-control" id="file" placeholder="Image" name="image[]"></br>
                                <a href="#" class="add_field_button">+ Add More Images</a>
                                <div class="input_fields_wrap"/>
                            </div></br>
                            <div class="form-group">
                                <?php foreach ($game_image as $value) { ?>
                                    <a href="<?php echo base_url().'admin/game/imageDelete/'.$value['id'] ?>" onclick="return confirm('are you sure delete')"><img src="<?php echo base_url().'upload/game/'.$value['game_image']?>" height="100" width="100">Delete</a>
                                    <input type="hidden" name="image_id[]" value="<?php if(isset($value['id'])) echo $value['id'];?>">
                                <?php } ?>
                            </div>

                            <div class="form-group choose_cate_id">
                                <label for="price">Console</label><br>
                                    <div class="row" style="margin: 5px auto 20px;">
                                        <?php foreach ($cat_name as $value) { ?>
                                        <b class="col-sm-2"><?php echo $value['category_name']; ?> :</b>
                                        <div class="col-sm-1">
                                            <input type="checkbox" name="category[]" value="<?php echo $value['id']; ?>" <?php if(in_array($value['id'],$checked_cat)) echo 'checked'; ?>>
                                            <span class="game_category_loader loader_<?php echo $value['id']; ?>"></span>
                                        </div>
                                        <div class="col-sm-9"><span class="game_is_live_error_<?php echo $value['id']; ?>"></span></div>
                                        <div class="clearfix"></div>
                                        <?php } ?> 
                                    </div>
                                    <div class="row category_is_exist_live_lobby"></div>
                            </div>
                            <div class="form-group">
                                <label for="price">Game Type</label><br/>
                                <?php 
                                $game_type_demo_a = explode('|',$game[0]['game_type']);
                                $game_type_arr = [];
                                foreach ($game_type_demo_a as $key => $gte) {
                                    $game_type_arr[$key+1]=$gte;
                                }
                                $game_type = 16;
                                echo '<div class="row" style="margin: 5px auto 20px;">';
                                for ($x = 1; $x <= $game_type; $x++) { 
                                    echo ($x % 4 == 1) ? '<div class="col-sm-3">' : '';
                                    if (in_array($x, $game_type_arr) ) {
                                        echo '<b>'.$x.'V'.$x.' :</b> <input type="checkbox" name="game_type[]" value="'.$x.'" checked><br/>';   
                                    } else{
                                        echo '<b>'.$x.'V'.$x.' :</b> <input type="checkbox" name="game_type[]" value="'.$x.'" ><br/>';   
                                    }
                                    echo ($x % 4 == 0) ? '</div>' : '';
                                }
                                echo '</div>';
                                ?>
                            </div>
                        </div><!-- /.box-body -->
                            <div class="box-footer">
                                <input type="hidden" name="id" value="<?php if(isset($game[0]['id'])) echo $game[0]['id'];?>">
                                <input type="hidden" name="old_image" value="<?php if(isset($game_image[0]['id'])) echo $game_image[0]['id'];?>">
                                <input type="hidden" name="old_name" value="<?php if(isset($game[0]['game_name'])) echo $game[0]['game_name'];?>" class="old_game_name">
                                <input type="hidden" name="old_image_name" value="<?php if(isset($game_image[0]['game_image'])) echo $game_image[0]['game_image'];?>" class="old_image_name">                            
                                <button class="btn btn-primary" type="submit" id="edit_game_update">Submit</button>
                            </div>
                    </form>
                <?php } else { ?>
                    <h4>Data not found</h4>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
