<section class="content">

	<div class="row">

	

		<div class="col-md-12">

			<?php $this->load->view('admin/common/show_message') ?>

  

			<div class="box box-primary">

				<form  action="<?php echo base_url(); ?>admin/game/gameInsert" method="post">

					<div class="box-body">
						<?php if(!empty($game_name_dt)) { ?>
							<?php foreach($game_name_dt as $game_key=>$game_value) { ?>
							
								<div class="form-group">
									<label for="name"><?php echo $game_key; ?></label>
									<select type="text" class="form-control" name="game_id[]">
										<option value="">--Select a category--</option>
										<?php if(!empty($game_value)){ ?>
										<?php foreach ($game_value as $game_val) { ?>
											<option value="<?php echo $game_val['id']; ?>"><?php echo $game_val['game_name']; ?></option>
										<?php } ?>
										<?php } ?>
									</select>
								</div>							
							<?php } ?>
						<?php } ?>
						
						<div class="form-group">

							<label for="name">Game Sub Name</label>

							<input type="text" id="sub_game_name" name="sub_game_name" placeholder="Sub Game Name" class="form-control" />

						</div>

				

					</div><!-- /.box-body -->



					<div class="box-footer">

						<button class="btn btn-primary" id="submit_subgame" type="submit">Submit</button>

					</div>

				</form>

			</div>

    

        </div>

            

	</div>

	

</section>