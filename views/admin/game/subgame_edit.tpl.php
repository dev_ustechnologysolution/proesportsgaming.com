<section class="content">
	<div class="row">
	
		<div class="col-md-12">
			<?php $this->load->view('admin/common/show_message') ?>
  
			<div class="box box-primary">
				<form  action="<?php echo base_url(); ?>admin/game/subgameUpdate" method="post">
					<div class="box-body">
						<div class="form-group">
							<label for="name">Game Name</label>
							<select type="text" id="subgame" class="form-control" name="game_id" readonly="readonly">
								<option value="">--Select a category--</option>
								<?php foreach ($game_name_dt as $value) { ?>
									<option value="<?php echo $value['id']; ?>" <?php if($value['id']==$subgame_data[0]['game_id']) echo 'selected'; ?>><?php echo $value['game_name']; ?></option>
								<?php } ?>
								
							</select>
						</div>

						<div class="form-group">
							<label for="name">Game Sub Name</label>
							<input type="text" name="sub_game_name" value="<?php if(isset($subgame_data[0]['sub_game_name'])) echo $subgame_data[0]['sub_game_name'];?>" class="form-control" />
						</div>
				
					</div><!-- /.box-body -->

					<div class="box-footer">
						<input type="hidden" name="id" value="<?php if(isset($subgame_data[0]['id'])) echo $subgame_data[0]['id'];?>">
						<button class="btn btn-primary" type="submit">Update</button>
					</div>
				</form>
			</div>
    
        </div>
            
	</div>
	
</section>