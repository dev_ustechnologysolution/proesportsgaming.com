<div class="col-sm-12">
	<div class="pull-right" >
		<a href="<?php echo base_url(); ?>admin/game/withdrawalExcel" id="get_url_2" class="reset_get"><i class="fa fa-print"></i> Export Excel</a> 
	</div>
</div>
<div class="clearfix"></div>
<section class="content">
  <div class="box">
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th style="display:none"></th>
            <th>Sr No</th>
            <th>User Name</th>
            <th>Acct #</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Current Balance($)</th>
            <th>Amount Requested($)</th>
            <th>Request Date</th>
            <th>Payment</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
		<?php 
		$i = 1;
		foreach($withdraw as $value) {
			if($value['status']==0) {
				$after_pay_amount = $value['total_balance'] - $value['amount_paid'];
			} else {
				$after_pay_amount = $value['total_balance'];
			} ?>
          <tr>
            <td style="display:none"></td>
            <td><?php echo $i++;?></td>
            <td><?php echo $value['name'];?></td>
            <td><?php echo $value['account_no'];?></td>
            <td><?php echo $value['user_email'];?></td>
            <td><?php echo $value['number'];?></td>
            <td><?php echo number_format((float)$value['total_balance'], 2, '.', '');?></td>
            <td><?php echo number_format((float)$value['amount_requested'], 2, '.', '');?></td>
            <td><?php echo $value['req_date'];?></td>
            <td><a href="<?php echo base_url().'admin/Payment/withdrawPayment/'.$value['id']; ?>"><button class="btn btn-primary" <?php if($value['status']==1) echo 'disabled="disabled"' ?>>Pay</button></a></td>
            <td><a href="javascript:void(0)" class="delete" id="<?php echo $value['id'];?>"><i class="fa fa-trash-o fa-fw"></i></a></td>
        </tr>
        <?php }?>
        </tbody>
      </table>
    </div>
  </div>
</section>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script> $.validate();</script>
<!-- Modal 1 (Basic)-->
<div class="modal fade  custom-width" id="modal-1">
	<div class="modal-dialog" style="width: 50%;">
		<div class="modal-content">
			
		</div>
	</div>
</div>

<script>
$(document).on('click','.delete',function(e){
	e.preventDefault();
	var id = $(this).attr('id');
	//var id = $(this).attr('href');
	if(id){
		$.ajax({
			url : '<?php echo site_url(); ?>admin/Game/security_pass/'+id,
			success: function(result) {
				if(result){
					$('.modal-content').html(result);
					$('#modal-1').modal('show');
				}
			}
		})
	}
})
$(document).on('submit','#security_pass_with',function(e){
  e.preventDefault();
  var data = $(this).serialize();
  if(data){
    $.ajax({
      url: "<?php echo base_url();?>admin/Game/remove_withdrawal",
      data: data,
      type: 'POST',
      success: function(result) {
      	if(result == 'success'){
      		window.location.href = "<?php echo base_url();?>admin/game/withdrawal";
      	}  else {
      		$('.modal-content').html(result);
        	$('#modal-1').modal('show'); 
        }
      }
    });
  }             
});
$(document).on('click','.reset_get',function(e){
	e.preventDefault();
	var id = $(this).attr('id');
	//var id = $(this).attr('href');
	if(id){
		$.ajax({
			url : '<?php echo site_url(); ?>admin/FrontUser/security_pass/'+id,
			success: function(result) {
				if(result){
					$('.modal-content').html(result);
					$('#modal-1').modal('show');															
				}
			}
		})
	}
})

$(document).on('submit','#security_pass',function(e){
	e.preventDefault();
	var data = $(this).serialize();
	var action_value = $('#action_value').val();
	if(data){
		$.ajax({
			url: "<?php echo site_url(); ?>admin/FrontUser/get_reset_data",
			data: data,
			type: 'POST',
			success: function(result) {
				if(result == 'success'){
					var action_url = $('#'+action_value).attr('href');
					window.location.href = action_url;
					$('#modal-1').modal('hide');
				} else {
					$('.modal-content').html(result);
					$('#modal-1').modal('show');				
				}
			}
		});
	}							
})

</script>
