<section class="content">

  <div class="row">

    <div class="col-md-12">

			<?php $this->load->view('admin/common/show_message') ?>

        <div class="box box-primary">

          <form  action="<?php echo base_url(); ?>admin/heading/headingUpdate" method="post">

            <div class="box-body">

            <div class="form-group">

              <label for="name">Heading1</label>

              <input type="text" class="form-control" name="name" value="<?php if(isset($heading_data[0]['heading1'])) echo $heading_data[0]['heading1'];?>">

            </div>

            <div class="form-group">

              <label for="name">Heading2</label>

              <input type="text" class="form-control" name="name1" value="<?php if(isset($heading_data[0]['heading2'])) echo $heading_data[0]['heading2'];?>">

            </div>

            <div class="form-group">

              <label for="name">Heading3</label>

              <input type="text" class="form-control" name="name2" value="<?php if(isset($heading_data[0]['heading3'])) echo $heading_data[0]['heading3'];?>">

            </div>

            <div class="form-group">

              <label for="name">Heading4</label>

              <input type="text" class="form-control" name="name3" value="<?php if(isset($heading_data[0]['heading4'])) echo $heading_data[0]['heading4'];?>">

            </div>

              </div><!-- /.box-body -->

              <div class="box-footer">

                <input type="hidden" name="id" value="<?php if(isset($heading_data[0]['id'])) echo $heading_data[0]['id'];?>">

                <button class="btn btn-primary" type="submit">Update</button>

              </div>

          </form>

        </div>

      </div>

    </div>

</section>