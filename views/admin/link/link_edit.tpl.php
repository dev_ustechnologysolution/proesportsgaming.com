<section class="content">

  <div class="row">

    <div class="col-md-12">

			<?php $this->load->view('admin/common/show_message') ?>

        <div class="box box-primary">

          <form  action="<?php echo base_url(); ?>admin/link/linkUpdate" method="post">

            <div class="box-body">

              <div class="form-group">

                  <label for="class">Link Class</label>

                  <input type="text" class="form-control" name="footer_class" value="<?php if(isset($link_data[0]['class'])) echo $link_data[0]['class'];?>">

              </div>

              <div class="form-group">

                  <label for="url">Link Url</label>

                  <input type="text" class="form-control" name="footer_link" value="<?php if(isset($link_data[0]['link'])) echo $link_data[0]['link'];?>">

              </div>

              </div><!-- /.box-body -->

              <div class="box-footer">

                <input type="hidden" name="id" value="<?php if(isset($link_data[0]['id'])) echo $link_data[0]['id'];?>">

                <button class="btn btn-primary" type="submit">Update</button>

              </div>

          </form>

        </div>

      </div>

    </div>

</section>