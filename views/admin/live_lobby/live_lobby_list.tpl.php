<section class="content box">
   <div class="row">
      <div class="col-md-12">
         <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 xwb-main-contact-conversation">
               <div class="table-responsive agent_table">
                  <table id="example1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <!-- <th>Sl. No.</th> -->
                           <th>Display Order</th>
                           <th>Creator Acc#</th>
                           <th>Creator Tag</th>
                           <th>Lobby Name</th>
                           <th>Price</th>
                           <th>Lobby Size</th>
                           <th>Fans</th>
                           <th>Status</th>
                           <th>Created Date</th>
                           <th>Option</th>
                        </tr>
                     </thead>
                     <tbody>
                      <?php 
                          $i = 1;
                          if(isset($live_lobby_list) && $live_lobby_list !=''){
                          foreach ($live_lobby_list as $value) {
                            $stream_data = $this->General_model->get_defult_lobby_chat($value->id);
                            $arr_stream_data = array($stream_data);
                            $creator_tag = $this->General_model->get_my_fan_tag($value->id,$value->user_id)->fan_tag;
                            if(count($arr_stream_data) > 0 )
                            {
                                if($arr_stream_data[0]->stream_status == 'enable'){
                                  $stream_img = base_url().'assets/frontend/images/live_stream_off_red.png';
                                } else {
                                  $stream_img = base_url().'assets/frontend/images/live_stream_off_admin.png';
                                }
                            } else {
                                $stream_img = base_url().'assets/frontend/images/live_stream_off_admin.png';;
                            }
                            $fans_count = count($this->General_model->fanscount($value->id));
                            $status = 'active';
                            $checked = 'checked';
                            if ($value->status == '0') {
                              $status = 'deactive';
                              $checked = '';
                            }
                            $list_row = '<tr>
                            <td>'.$value->lobby_order.'<a class="lobby_view" id="'.$value->id.'" >   <i class="fa fa-pencil fa-fw" id="'.$value->id.'"></i></a></td>
                            <td>'.$value->creator_accno.'</td>
                            <td>'.$creator_tag.'</td>
                            <td>'.$value->game_name.'</td>
                            <td>'.$value->price.'</td>
                            <td>'.$value->game_type.'v'.$value->game_type.'</td>
                            <td>'.$fans_count.'</td>';
                            // <td><label class="myCheckbox"><input '.$checked.' id="'.$value->id.'" class="single-checkbox custom-cb edit_lobby" custom_type="'.$value->status.'" type="checkbox" name="progress" value="'.$status.'"><span></span></label></td>
                            $list_row .= '<td><img width="34" src='.$stream_img.'></td>
                            <td>'.$value->created_at.'</td>
                            <td><a class="delete_lobby" id="'.$value->id.'" custom-type="delete"><i class="fa fa-trash-o" id="'.$value->id.'"></i></a></td>
                          </tr>'; 
                           echo $list_row;
                           $i++;

                           } 
                         }
                       ?>
                     </tbody>
                  </table>
               </div>
            </div>
          <div class="modal fade  custom-width" id="modal-1">
            <div class="modal-dialog" style="width: 50%;">
              <div class="modal-content">
                
              </div>
            </div>
          </div>
          <script> 
            $(document).on('click','.delete_lobby',function(){
             var id = $(this).attr('id');

              if(id){
                $.ajax({
                  url : '<?php echo site_url(); ?>admin/chat/security_password_data/'+id,
                  success: function(result) {
                    if(result){
                      $('.modal-content').html(result);
                      $('#modal-1').modal('show');                              
                    }
                  }
                })
              }
            })
             $(document).on('submit','#security_id',function(e){
            e.preventDefault();
            var data = $(this).serialize();
              $.ajax({
                url: "<?php echo site_url(); ?>admin/live_lobby/delete_lobby_data",
                data: data,
                type: 'POST',
                success: function(data_value) {
                if(data_value == '1')
                {
                  window.location.href = "<?php echo site_url(); ?>admin/live_lobby/live_lobby_list/";
                }
                else
                {
                  alert("Password Error");
                }
              }
              });
          });
             $(document).on('click','.edit_lobby',function(){
             var id = $(this).attr('id');
              var custom_type = $(this).attr('custom_type');
              if(custom_type == 0){ var type = 1;}else { type = 0;}
             
              if(id){
                $.ajax({
                  url : '<?php echo site_url(); ?>admin/live_lobby/updateStatusPasswordRequire/',
                  data: {id:id,type:type},
                   type: 'POST',
                  success: function(result) {
                    if(result){
                      $('.modal-content').html(result);
                      $('#modal-1').modal('show');                              
                    }
                  }
                })
              }
            })
             $(document).on('submit','#status_update',function(e){
            e.preventDefault();
            var data = $(this).serialize();
              $.ajax({
                url: "<?php echo site_url(); ?>admin/live_lobby/updateLiveLobbyStatus",
                data: data,
                type: 'POST',
                success: function(data_value) {
                if(data_value == '1')
                {
                  window.location.href = "<?php echo site_url(); ?>admin/live_lobby/live_lobby_list/";
                }
                else
                {
                  alert("Password Error");
                }
              }
              });
          });
             $(document).on('click','.lobby_view',function(){
                var id = $(this).attr('id');
                if(id){
                  $.ajax({
                    url : '<?php echo site_url(); ?>admin/Banner/ChangeLobbyOrder/?id='+id,
                    success: function(result) {
                      if(result){
                        $('.modal-content').html(result);
                        $('#modal-1').modal('show');                              

                      }
                    }
                  });
                }
            });
             $(document).on('submit','#change_lobby_order',function(e){
              e.preventDefault();
              var data = $(this).serialize();
              if(data){
                $.ajax({
                  url: "<?php echo site_url(); ?>admin/Banner/UpdateLobbyOrder",
                  data: data,
                  type: 'POST',
                  success: function(result) {
                    window.location.href = "<?php echo site_url(); ?>admin/live_lobby/live_lobby_list";       
                  }
                });
              }             
            });
          </script> 
         </div>
      </div>
   </div>
</section>


