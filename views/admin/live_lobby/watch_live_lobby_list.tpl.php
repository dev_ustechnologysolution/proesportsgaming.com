<?php //error_reporting(1); ?>
<section class="">
  <div class="box">
    <?php if($this->session->flashdata('status_msg')) { echo $this->session->flashdata('status_msg'); unset($_SESSION['status_msg']); } ?>
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Game Name</th>
            <th>Price</th>
            <th>Left Side Url#1</th>
            <th>Right Side Url#2</th>
            <th>Left Side Tag</th>
            <th>Right Side Tag</th>
			<th>Left Head Status</th>
			<th>Right Head Status</th>
            <th>Status</th>
            <th>Alert</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
		<?php

		if(!empty($list)) {
			foreach($list as $value) { 
	          	$lobby_left_head_tags = $this->General_model->lobby_head_tags($value['lobby_id'],$value['grp_bet_id'],'LEFT');
	          	$lobby_right_head_tags = $this->General_model->lobby_head_tags($value['lobby_id'],$value['grp_bet_id'],'RIGHT');
	          	$lobby_report_status = $this->General_model->lobby_report_status($value['id'],$value['grp_bet_id']); 
	          	?>
	        	
	        	<tr>
		            <td><?php echo $value['game_name']?></td>
		            <td><?php echo CURRENCY_SYMBOL.$value['price']?></td>            			
					<?php
		        		$new_url =  explode(',', $value['video_urls']);
						foreach ($new_url as $key => $val) {
							$urls[] = preg_replace('/&&.*/', '', $val);
						}
						
						// $reported_listtables_cat = $this->General_model->get_report_tables($value['lobby_id'],$value['grp_bet_id']);
						$reported_listtables_cat = explode(', ',trim($value['tables_cat']));
						$listuser 			= explode(',',$value['bet_user_id']);
						$listvideo 			= explode(',',$value['lv_video_id']);
						$listvideostatus	= explode(',',$value['lv_status']);
						$listtables_reported = explode(',',$value['tables_reported']);
						$listtables_win_lose_status = explode(',',$value['tables_win_lose_status']);

						$creator_table_key	= array_search($value['challenger_table'], $reported_listtables_cat);
						$challenger_table_key = array_search($value['accepter_table'], $reported_listtables_cat);
						if ($creator_table_key !== false) {
							$creator_idpre = 1;					
						} else {
							$creator_idpre = 2;
						}
						if ($challenger_table_key !== false) {
							$challenger_idpre = 1;					
						} else {
							$challenger_idpre = 2;
						}
						$video_url_id = 1;
						?>
					<td>				
						<?php  
							$vids_data = $this->General_model->get_lobby_vids_data('LEFT',$value['lobby_id'],$value['grp_bet_id']); 
							$left_url = [];
							$left_url =  explode('&&', $vids_data->video_url);
							if ($left_url[0] !='') {
								echo "<a href=".$vids_data->video_url." target='_blank'>Click to See</a>";
							}
							if ($vids_data->detail_admin !='') {
								echo "<a class='admin_det' data-id=".$vids_data->id."> <i class='fa fa-file-text'></i> </a> ";
							}?>
					</td>
					<td>
						<?php 
							$vids_data = $this->General_model->get_lobby_vids_data('RIGHT',$value['lobby_id'],$value['grp_bet_id']); 
							$righturls = [];
							$right_url =  explode('&&', $vids_data->video_url);
							if ($right_url[0] !='') {
								echo "<a href=".$vids_data->video_url." target='_blank'>Click to See</a>";
							}
							if ($vids_data->detail_admin !='') {
								echo "<a class='admin_det' data-id=".$vids_data->id."> <i class='fa fa-file-text'></i> </a> ";
							}
							?>
					</td>
					<td><?php echo $lobby_left_head_tags;?></td>
		            <td><?php echo $lobby_right_head_tags;?></td>
		            <td><?php 
		            	foreach ($lobby_report_status as $key => $lrs) {
						 	if ($lrs['tables_cat'] == 'LEFT') {
						 		echo $lrs['win_lose_status'];
							}
						}
		             ?> </td>
		            <td>
		            <?php
		            foreach ($lobby_report_status as $key => $lrs) {
					 	if ($lrs['tables_cat'] == 'RIGHT') {
					 		echo $lrs['win_lose_status'];
						}
					} ?> </td>
		            <td>
		              <select name="is_status" onchange="changeLobbyBetStatus(<?php echo $video_url_id; ?>,<?php echo $value['grp_bet_id']; ?>,'<?php echo $value['challenger_table']; ?>',this.value)">
		                <option value="">Select Status</option>
		                <option value="<?php echo $value['challenger_table']; ?>">LEFT</option>
		                <option value="<?php echo $value['accepter_table']; ?>">RIGHT</option>
		              </select>
		            </td>
		            <td>
		            	<?php if($value['group_bet_status']==2) { echo '<p style="color:red">You Have a new game url<p>'; } else { echo '<p style="color:green">You have already view this url</p>'; } ?></td>
		            <td>
		            	<a href="<?php echo base_url().'admin/live_lobby/grpbetURLDelete/'.$value['grp_bet_id'];?>" onclick="return confirm('are you sure delete')"><i class="fa fa-trash-o fa-fw"></i></a>
					</td>
	          	</tr>
		  	<?php unset($urls);?>
		  <?php }
		} ?>
        </tbody>
      </table>
    </div>
  </div>
</section>

<script type="text/javascript">
 function changeLobbyBetStatus(videoid,grp_bet_id,userid,status)
 {
 	var url = "<?php echo base_url().'admin/live_lobby/changeStatus/'?>"+videoid+"/"+grp_bet_id+"/"+userid+"/"+status;
 	window.location.href = url;
}
</script>
<style>
	.modal {     display: none;  position: fixed; z-index: 1; padding-top: 200px; padding-left: 300px; left: 0; top: 0;  width: 100%; height: 100%; overflow: auto;  background-color: rgba(146,146,146,0.4);}
	.modal-content { position: relative; background-color: #fefefe; margin: auto; padding: 0; border: 1px solid #000; width: 80%; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19); -webkit-animation-name: animatetop;  -webkit-animation-duration: 0.4s; animation-name: animatetop; animation-duration: 0.4s }
	@-webkit-keyframes animatetop {     from {top:-300px; opacity:0}      to {top:0; opacity:1} }
	@keyframes animatetop {     from {top:-300px; opacity:0}     to {top:0; opacity:1} }
	.close {     color: white;     float: right;      font-size: 28px;     font-weight: bold; }
	.close:hover, .close:focus {    color: #000;    text-decoration: none;    cursor: pointer; }
	.modal-header { background-color: #1f1f1f; border:none !important; text-align:center;  color :#FFF; }
	.modal-body { padding: 2px 16px; background-color: #1f1f1f; text-align:left; color :#FFF;}
	.modal-footer {background-color: #1f1f1f;}
	.game-act {font-size: 16px;     background: #ff5000;     padding: 5px 20px;    color: #333;    margin: 0px 20px 0px 5px;     border-radius: 5px;    text-decoration: none;    transition: all 0.4s ease-in-out;    text-align: center;    line-height: 50px; }
	</style>
	<div id="myModal" class="modal">
		<div class="modal-content">
			<div class="modal-header">
				<span class="close">&times;</span>	
				<h4>Message</h4>
			</div>
			<div class="modal-body">								
				<div id="add_messg"></div>
			</div>							
		</div>
	</div>

<script>
$(document).on('click','.admin_det',function(){
	var da_id = $(this).attr('data-id');
	if(da_id){
		$.ajax({
			url :"<?php echo base_url().'admin/live_lobby/get_grp_bet_admin_message'?>",
			data : {id : da_id },
			type: "POST",
			success: function (data) {
				$('#myModal').show();
				$('#add_messg').text(data);
			}
		})
	}
})

$(document).on('click','.close',function(e){
	$('#myModal').hide();
});
</script>