<?php $this->load->view('admin/common/login_header'); ?>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="<?php echo base_url(); ?>">
			<img src="<?php echo base_url().'assets/frontend/images/logo.png'; ?>" width="100%" />
			<b><?php //echo ADMIN_TITLE ;?></b>
		</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg"><?php if(isset($err)) echo $err ;?></p>
        <form action="<?php echo base_url().'admin/user/forgot_password'; ?>" method="post">
          <div class="form-group has-feedback">
            <input type="email" class="form-control" placeholder="Email" name="email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          
          <div class="row">
            <div class="col-xs-8">
              
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
            </div><!-- /.col -->
          </div>
        </form>

        
       

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
<?php $this->load->view('admin/common/login_footer'); ?>
    
