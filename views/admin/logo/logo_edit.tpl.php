<section class="content">

  <div class="row">

    <div class="col-md-12">

			<?php $this->load->view('admin/common/show_message') ?>

        <div class="box box-primary">

          <form  action="<?php echo base_url(); ?>admin/logo/logoUpdate" method="post" enctype="multipart/form-data">

            <div class="box-body">

              <div class="form-group">

                    <label for="file">Image</label>

                    <input type="file" class="form-control" id="file" placeholder="Image" name="image">

                </div>

                <div class="form-group">

                  <img src="<?php echo base_url().'upload/logo/'.$logo_data[0]['image']?>" height="150" width="150">

                </div>

              </div><!-- /.box-body -->

              <div class="box-footer">

                <input type="hidden" name="id" value="<?php if(isset($logo_data[0]['id'])) echo $logo_data[0]['id'];?>">

                <input type="hidden" name="old_image" value="<?php if(isset($logo_data[0]['image'])) echo $logo_data[0]['image'];?>">

                <button class="btn btn-primary" type="submit">Update</button>

              </div>

          </form>

        </div>

      </div>

    </div>

</section>