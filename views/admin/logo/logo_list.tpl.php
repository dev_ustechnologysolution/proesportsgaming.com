<section class="content">

  <div class="box">

    <?php $this->load->view('admin/common/show_message') ?>

    <div class="box-body">

      <table id="" class="table table-bordered table-striped">

        <thead>

          <tr>

            <th>Sl No</th>

            <th>Logo Image</th>

            <th>Action</th>

          </tr>

        </thead>

        <tbody>

          <?php $i=1; foreach($list as $value){?>

          <tr>

            <td><?php echo $i++; ?></td>

            <td> <img src="<?php echo base_url().'upload/logo/'.$value['image'] ?>" width="100" height="100"> </td>

            <td><a href="<?php echo base_url().'admin/logo/logoEdit/'.$value['id'];?>"><i class="fa fa-pencil fa-fw"></i></a></td>

           </tr>

          <?php }?>

        </tbody>

      </table>

    </div>

  </div>

</section>