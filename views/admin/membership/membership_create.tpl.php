<section class="content">
  <div class="row">   
    <div class="col-md-12">
      <?php $this->load->view('admin/common/show_message') ?>      
      <div class="box box-primary">
        <form enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/membership/create" method="post">          
      
            <div class="box-body">

              <div class="form-group row">
                <label for="title" class="col-sm-1">Title</label>
                <div class="col-sm-8">
                  <input required type="text" class="form-control" name="title">
                </div>
              </div>

              <div class="form-group row">
                <label for="description" class="col-sm-1">Description</label>
                <div class="col-sm-8">
                  <textarea required name="description" class="form-control" rows="10" cols="80"></textarea>
                </div>
              </div>

              <div class="form-group row">
                <label for="time_duration" class="col-sm-1">Time Duration</label>
                <div class="col-sm-8">
                  <select name="time_duration" class="form-control">
                    <option value="Day">Day</option>
                    <option value="Week">Week</option>
                    <option value="SemiMonth">SemiMonth</option>
                    <option value="Month" selected>Month</option>
                    <option value="Year">Year</option>                    
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label for="image" class="col-sm-1">Image</label>
                <div class="col-sm-8">
                  <input required type="file" class="form-control" name="image" accept="image/*">
                </div>
              </div>

              <div class="form-group row">
                <label for="amount" class="col-sm-1">Amount ($)</label>
                <div class="col-sm-8">
                  <input required type="number" class="form-control" min="1" name="amount">
                </div> 
              </div>

              <div class="form-group row">
                <label for="fees" class="col-sm-1">Fees ($)</label>
                <div class="col-sm-8">
                  <input required type="number" class="form-control" min="0" name="fees">
                 </div> 
              </div>

              <div class="form-group row">
                <label for="terms_condition" class="col-sm-1">Terms & Policy</label>
                <div class="col-sm-8">
                  <textarea name="terms_condition" class="form-control" rows="10" cols="80"></textarea>
                </div>
              </div>
   
            </div>
            <div class="box-footer col-sm-12" align="center"> 
              <div class="col-sm-10">             
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</section>