<section class="content">
  <div class="row">   
    <div class="col-md-12">
      <?php $this->load->view('admin/common/show_message') ?>      
      <div class="box box-primary">
        <form  enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/membership/membershipUpdate" method="post">          
      
            <div class="box-body">

              <div class="form-group row">
                <label for="title" class="col-sm-1">Title</label>
                <div class="col-sm-8">
                  <input required type="text" value="<?php if(isset($membership_data[0]['title'])) echo $membership_data[0]['title'];?>" class="form-control" name="title">
                </div>
              </div>

              <div class="form-group row">
                <label for="description" class="col-sm-1">Description</label>
                <div class="col-sm-8">
                  <textarea required name="description" class="form-control" rows="10" cols="80"><?php if(isset($membership_data[0]['description'])) echo $membership_data[0]['description'];?></textarea>
                </div>
              </div>
              <div class="form-group row">
                <label for="time_duration" class="col-sm-1">Time Duration</label>
                <div class="col-sm-8">
                  <select name="time_duration" class="form-control">
                    <option value="Day" <?php echo ($membership_data[0]['time_duration'] == "Day") ? 'selected' : ''; ?>>Day</option>
                    <option value="Week" <?php echo ($membership_data[0]['time_duration'] == "Week") ? 'selected' : ''; ?>>Week</option>
                    <option value="SemiMonth" <?php echo ($membership_data[0]['time_duration'] == "SemiMonth") ? 'selected' : ''; ?>>SemiMonth</option>
                    <option value="Month" <?php echo ($membership_data[0]['time_duration'] == "Month") ? 'selected' : ''; ?>>Month</option>
                    <option value="Year" <?php echo ($membership_data[0]['time_duration'] == "Year") ? 'selected' : ''; ?> >Year</option>                    
                  </select>
                </div>
              </div>

             
              <div class="form-group row upload-div  <?php if($membership_data[0]['image'] != '') echo 'active' ?>" style="padding-left: 0;">
                <label class="col-sm-1">Image</label>
                <div class="col-sm-8">
                  <input type="hidden" name="selected_banner" id="edit_banner_image" value="edit_banner_image" <?php if($membership_data[0]['image'] != '') echo 'checked class="checked"';?>>
                  <a style="float: right; cursor: pointer; display: none;" class="col-sm-10 browse_new_image">Change Image</a>
                  <input type="file" class="form-control image_banner" id="file" placeholder="Image" name="image" accept="image/*" style="display: none;">
                <input type="hidden" id="old_image" name="image" value="<?php echo $membership_data[0]['image'];?>">
                <div  style="margin-top: 5px; "><img src="<?php echo base_url().'upload/membership/'.$membership_data[0]['image'];?>"  class="old_image_preview" height="150" width="150" style="display: none;">
                </div>
              </div>
                
              </div>
              

              <div class="form-group row">
                <label for="amount" class="col-sm-1">Amount ($)</label>
                <div class="col-sm-8">
                  <input required value="<?php if(isset($membership_data[0]['amount'])) echo $membership_data[0]['amount'];?>" type="number" class="form-control" min="1" name="amount">
                </div> 
              </div>

              <div class="form-group row">
                <label for="fees" class="col-sm-1">Fees ($)</label>
                <div class="col-sm-8">
                  <input required type="number" value="<?php if(isset($membership_data[0]['fees'])) echo $membership_data[0]['fees'];?>" class="form-control" min="0" name="fees">
                 </div> 
              </div>

              <div class="form-group row">
                <label for="terms_condition" class="col-sm-1">Terms & Policy</label>
                <div class="col-sm-8">
                  <textarea name="terms_condition" class="form-control" rows="10" cols="80"><?php if(isset($membership_data[0]['terms_condition'])) echo $membership_data[0]['terms_condition'];?></textarea>
                </div>
              </div>
   
            </div>
            <div class="box-footer col-sm-12" align="center"> 
              <div class="col-sm-10">  
               <input type="hidden" name="id" value="<?php if(isset($membership_data[0]['id'])) echo $membership_data[0]['id'];?>">           
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</section>