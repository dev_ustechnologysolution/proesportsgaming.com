<section class="content">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs" role="tablist">
      <li class="active"><a href="#hometab" role="tab" data-toggle="tab">Membership Plan</a></li>
      <li><a href="#javatab" role="tab" data-toggle="tab">Membership Players</a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="hometab">
        <div class="box">
          <div class="box-header">  
            <a href="<?php echo base_url().'admin/membership/addMembership'; ?>">
              <button class="btn pull-right btn-primary btn-xl">Add Membership</button>
            </a>
          </div>
          <?php $this->load->view('admin/common/show_message') ?>
          <div class="box-body">
            <table  class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Sl No</th>
                  <th>Image</th>
                  <th>Title</th>            
                  <th>Description</th>
                  <th>Amount($)</th>
                  <th>Fees($)</th>
                  <th>Status</th>
                  <th>Created Date</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=1; foreach($list as $value){ ?>
                <tr>
                  <td><?php echo $i++; ?></td>            
                  <td><img src="<?php echo base_url().'upload/membership/'.$value['image'];?>" width="85" height="50"></td>
                  <td><?php echo $value['title']; ?></td>
                  <td><?php echo $value['description']; ?></td>
                  <td><?php echo $value['amount']; ?></td>
                  <td><?php echo $value['fees']; ?></td>
                  <td><?php echo ($value['status'] == 1) ? 'Active' : 'Inactive'; ?></td>
                  <td><?php echo $value['start_date'];?></td>
                  <td>
                    <a href="<?php echo base_url().'admin/membership/membershipEdit/'.$value['id'];?>"> <i class="fa fa-pencil fa-fw"></i></a>|
                    <a href="#" class="delete" onclick="myFunction('<?php echo $value['id']; ?>')"><i class="fa fa-trash-o fa-fw"></i></a>              
                  </td>
                 </tr>
                <?php }?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="tab-pane" id="javatab">
        <div class="box">
          <div class="box-header"></div>
          <div class="box-body">
            <table  class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Sl. No.</th>
                  <th>Trascation Id</th>
                  <th>Plan</th>
                  <th>Player Name</th>
                  <th>Acc#</th>
                  <th>Amount</th>
                  <th>Created Date/Time</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=1; foreach($plyer_list as $value) {
                  $planDetails = $this->General_model->view_single_row('membership','id',$value['plan_id']); 
                  $subscribe_toname = $this->General_model->view_single_row('user_detail','user_id',$value['user_id']); 
                  ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo $value['transaction_id']; ?></td>
                  <td><?php echo $planDetails['title']; ?></td>
                  <td><?php echo ucwords($subscribe_toname['name']);?></td>
                  <td><?php echo $subscribe_toname['account_no'];?></td>
                  <td><?php echo $value['amount'];?></td>
                  <td><?php echo $value['create_at'];?></td>
                  <td><a href="<?php echo base_url(); ?>admin/membership/membershipHistory/?user_id=<?php echo $value['user_id']; ?>">History</a></td>
                 </tr>
                <?php }?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>  
</section>

<div class="modal fade  custom-width" id="modal-1">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">
      
    </div>
  </div>
</div>
<script>
function myFunction(id){
  $.ajax({
      url : '<?php echo site_url(); ?>admin/membership/delete_data/'+id,
      success: function(result) {
        if(result){
          $('.modal-content').html(result);
          $('#modal-1').modal('show');                              
        }
      }
    })
}

$(document).on('submit','#security_password_id',function(e){
  e.preventDefault();
  var data = $(this).serialize();
  if(data){
    $.ajax({
      url: "<?php echo site_url(); ?>admin/membership/delete",
      data: data,
      type: 'POST',
      success: function(result) {
        console.log(result);
        if(result == true){
          window.location.href = "<?php echo site_url(); ?>admin/membership/";
        } else {
          $('#security_password').val('');
          alert('Password Error');
          return false;
        }
              
      }
    });
  }             
})
$(document).ready(function() 
{
   $('.table-bordered').dataTable({
        aLengthMenu: [[25, 50, 75, -1], [25, 50, 75, "All"]],
        iDisplayLength: 25,
        stateSave: true,
    });
    });
</script>