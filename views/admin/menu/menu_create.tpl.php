<section class="content">
  <div class="row">
   
    <div class="col-md-12">
      <?php $this->load->view('admin/common/show_message') ?>
      
      <div class="box box-primary">

        <form  action="<?php echo base_url(); ?>admin/menu/create" method="post">
          
      
            <div class="box-body">

               <div class="form-group">

                  <label for="description">Menu Title</label>

                  <input type="text" class="form-control" name="title">

              </div>

   
              </div>

            </div><!-- /.box-body -->
            <div class="box-footer">
              
              <button type="submit" class="btn btn-primary">Submit</button>
            </div><!-- /.box-footer -->
          

        </form>
      </div>
    </div>
  </div>
</section>