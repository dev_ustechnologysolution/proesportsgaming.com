<section class="content">

  <div class="row">

    <div class="col-md-12">

			<?php $this->load->view('admin/common/show_message') 

        ; 
      ?>

        <div class="box box-primary">

          <form id="f3" action="<?php echo base_url(); ?>admin/menu/menuUpdate" method="post">

            <div class="box-body">

              <div class="form-group">

                  <label for="description">Title</label>

                  <input type="text" class="form-control" name="title" value="<?php if(isset($menu_data[0]['title'])) echo $menu_data[0]['title'];?>">

              </div>
              <div class="form-group">

                  <label for="type">Type</label>
                 

                  <input type="radio" class="one" name="type" value="static" <?php if($menu_data[0]['type'] == 'static') { echo 'checked'; } ?>>Static

                  <input type="radio" class="one" name="type" value="dynamic" <?php if($menu_data[0]['type'] == 'dynamic') { echo 'checked'; };?>>Dynamic

              </div>
              <div class="form-group">

                  <label for="type">Order</label>                 

                  <input required type="number" class="form-control" name="menu_order" value="<?php if(isset($menu_data[0]['menu_order'])) echo $menu_data[0]['menu_order'];?>">

              </div>

              <div class="form-group">

                  <label for="description">Link</label>

                  <input readonly type="text" class="form-control" name="menu_link" value="<?php if(isset($menu_data[0]['link'])) echo $menu_data[0]['link'];?>">

              </div>
              <?php 
              $class='';
                if($menu_data[0]['type'] == 'dynamic'){
                  $class="hide";
                }
              ?>

               <div class="form-group <?php echo $class; ?>" id="content">

                  <label for="description">Content</label>

                  <textarea id="editor" name="editor1" class="ckeditor" rows="10" cols="80"><?php if(isset($menu_data[0]['content'])) echo $menu_data[0]['content'];?></textarea>

              </div>

              <div class="form-group">

                  <label for="description">Menu Position</label>

                  <select type="text" class="form-control" name="menu_position" value="<?php if(isset($menu_data[0]['position'])) echo $menu_data[0]['position'];?>">
                    <option>--Select A Position--</option>
                    <option value="1" <?php echo $menu_data[0]['position'] == '1' ? ' selected="selected"' : '';?>>Header</option>
                    <option value="2" <?php echo $menu_data[0]['position'] == '2' ? ' selected="selected"' : '';?>>Footer</option>
                  </select>

              </div>

              </div><!-- /.box-body -->

              <div class="box-footer">

                <input type="hidden" name="id" value="<?php if(isset($menu_data[0]['id'])) echo $menu_data[0]['id'];?>">

                <button class="btn btn-primary" type="submit">Update</button>

              </div>

          </form>

        </div>

      </div>

    </div>

</section>
<script>
  function bindChangeHandler(cls) {
    $('input.' + cls).change(function () {
        var element = $(this),// get Input element
            name = element.val(), // get Input element name
            is_checked = element.prop('checked'); // get state of radio box

        // return if a deselect triggered the event (may be unnecessary)
        if (!is_checked) return;

        // change class of div-box according to checked radio-box 
        if  (name == 'static') {
            $('#content').removeClass('hide');
            // var editor = CKEDITOR.instances['editor1'];
            // editor.setData('your data');
        } else {
            $('#content').addClass('hide');
        }
    });
}

$(document).ready(function(){
    bindChangeHandler('one');   
});
 // $("#f3").submit( function() {
 //            var messageLength = CKEDITOR.instances['ckeditor'].getData().replace(/<[^>]*>/gi, '').length;
 //            if( !messageLength ) {
 //                alert( 'Please enter a message' );
 //            }
 //        }
</script>