<section class="content">
	<div class="row">
		<div class="col-md-12">
			<?php $this->load->view('admin/common/show_message') ?>
			<div class="box box-primary">
				<form  action="<?php echo base_url(); ?>admin/points/add_package" method="post" enctype="multipart/form-data">
					<div class="box-body">
						<div class="form-group">
							<label for="title">Title</label>
							<input type="text" placeholder="Title" id="title" class="form-control" name="title" required="required">
						</div>
						<div class="form-group">
							<label for="description">Description</label>
							<textarea class="form-control" id="description" placeholder="Description" name="description" rows="8" cols="8"></textarea>
						</div>
						<div class="form-group">
							<label for="amount">Amount ($)</label>
							<input type="text" placeholder="Amount" id="amount" class="form-control" name="amount" required="required">
						</div><!--
						<div class="form-group">
							<label for="points">Points</label>
							<input type="text" placeholder="Points" id="points" class="form-control" name="points" required="required">
						</div> -->
						<div class="form-group">
							<label for="points">Fee ($)</label>
							<input type="text" placeholder="Fee" id="percentage" class="form-control" name="percentage" required="required">
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button class="btn btn-primary" id="point_add" type="submit">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>