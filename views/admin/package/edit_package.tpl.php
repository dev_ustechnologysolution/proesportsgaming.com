<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php $this->load->view('admin/common/show_message') ?>
            <div class="box box-primary">
                <?php if(count($package)>0){?>
                    <form  action="<?php echo base_url(); ?>admin/points/edit_package" method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" placeholder="Title" id="title" class="form-control" name="title" required="required" value='<?php if(isset($package[0]['point_title'])) echo $package[0]['point_title'];?>'>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" id="description" placeholder="Description" name="description" rows="8" cols="8"><?php if(isset($package[0]['point_description'])) echo $package[0]['point_description'];?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="amount">Amount ($)</label>
                                <input type="text" placeholder="Amount" id="amount" class="form-control" name="amount" required="required" value="<?php if(isset($package[0]['amount'])) echo $package[0]['amount'];?>">
                            </div>
							<!--
                            <div class="form-group">
                                <label for="points">Points</label>
                                <input type="text" placeholder="Points" id="points" class="form-control" name="points" required="required" value="<?php if(isset($package[0]['points'])) echo $package[0]['points'];?>">
                            </div> -->
							<div class="form-group">
							<label for="points">Fee ($)</label>
							<input type="text" placeholder="Fee" id="percentage" class="form-control" name="percentage" required="required" value="<?php if(isset($package[0]['percentage'])) echo $package[0]['percentage'];?>">
						</div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <input type="hidden" name="id" value="<?php if(isset($package[0]['id'])) echo $package[0]['id'];?>">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </form>
                <?php } else {?>
                    <h4>Data not found</h4>
                <?php }?>
            </div>
        </div>
    </div>
</section>