<section class="content">
  <div class="box">
      <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th style="display:none"></th>
              <th>Sr no#</th>
                <th>Acct#</th>
                <th>Purchase By</th>
                <th>Title</th>
                <th>Amount</th>
                <th>Purchase Date</th>
                <th>Status</th>
            </tr>
          </thead>
          <tbody>
            <?php $i=1; foreach($points as $value){
              $title = '';
              if ($value['package_id']  != 0) {
                $pdata = $this->General_model->view_data('admin_packages', array('id' => $value['package_id']));
                $title = $pdata[0]['point_title'];
              }
              $payment_status = "<span style='color:green;'> Payment Completed </span>";
              if ($value['payment_status'] == 'due') {
                $payment_status = "<span style='color:red;'> Process Pending </span>";
              }
             ?>
              <tr>
                <td style="display:none"></td>
                <td><?php echo $value['id']; ?></td>
                <td><?php  $account_arr = $this->General_model->view_single_row('user_detail','user_id',$value['user_id']); echo $account_arr['account_no'];  ?></td>
                <td><?php echo $value['name']; ?></td>
                <td><?php echo $title; ?></td>
                <td><?php echo CURRENCY_SYMBOL.($value['amount']);?></td>
                <td><?php echo date('m-d-Y',strtotime($value['payment_date'])); ?></td>
                <td><?php echo $payment_status; ?></td>
              </tr>
            <?php }?>
          </tbody>
        </table>
    </div>
  </div>
</section>
