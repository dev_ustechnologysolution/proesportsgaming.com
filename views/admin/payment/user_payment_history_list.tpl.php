<section class="content">

  <div class="box">

    <div class="box-body">

      <table id="example1" class="table table-bordered table-striped">

        <thead>

          <tr>

            <th style="display:none"></th>

            <th>Sl No</th>

            <th>Player Name</th>

            <th>Game Name</th>

           <!--  <th>Game Sub Name</th> -->

            <th>Transaction Id</th>

            <th>Player Payment Email</th>

            <th>Payment Amount</th>

            <th>Status</th>

            <th>Payment Date</th>

          </tr>

        </thead>

        <tbody>

          <?php $i=1; foreach($user_payment_history_data as $value){?>

          <tr>

            <td style="display:none"></td>

            <td><?php echo $i++; ?></td>

            <td><?php echo $value['name']?></td>

            <td><?php echo $value['game_name']?></td>
            
            <!--<td><?php echo $value['sub_game_name']?></td>-->

            <td><?php echo $value['transaction_id']?></td>

            <td><?php echo $value['payer_email']?></td>

            <td><?php echo $value['amount'].$value['currency']?></td>

            <td><?php echo $value['payment_status']?></td>

            <td><?php echo $value['payment_date']?></td>

           </tr>

          <?php }?>

        </tbody>

      </table>

    </div>

  </div>

</section>