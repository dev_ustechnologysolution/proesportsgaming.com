<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php $this->load->view('admin/common/show_message') ?>
                        
                <form  action="<?php echo base_url(); ?>admin/products/productInsert" method="post" enctype="multipart/form-data" class="admin_game_edit_form">
                    <div class="box box-primary"> 
                        <h4 class="ml20">Product Basic Details</h4>
                        <input type="hidden" name ="type" value="<?php echo $_GET['type'];?>">
                        <div class="box-body">                        
                            <div class="form-row"> 
                            <div class="form-group col-md-6">
                                  <label for="name">Title</label>
                                    <input required type="text" required id="name" class="form-control" name="name" placeholder="Please enter product name" value=""/>
                                </div>        
                                <div class="form-group col-md-3">
                                  <label for="name">Select Main Category</label>
                                      <select required class="form-control" name="category_id" id="category_id"><option value="">Please Select Category</option>
                                        <?php foreach ($category_list as $key => $value) { 
                                         ?>
                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                       <?php } ?>
                                    </select>
                                </div> 
                                <div class="form-group col-md-3">
                                  <label for="sname">Select Sub Category</label>
                                      <select  required class="form-control" name="subcategory_id" id="sub_cat">
                                        <option value="">Please Select Sub-Category</option>
                                    </select>
                                </div> 

                            </div>
                            <div class="form-row">  
                                                    
                                <div class="form-group col-md-12">
                                   <label for="description">Description</label>
                                    <textarea required placeholder="Please enter product Description" class="form-control" rows="6" name="description"> </textarea> 
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="box box-primary"> 
                        <h4 class="ml20">Product Price Details</h4>
                        <div class="box-body">   
                            <div class="form-row">
                                                               
                                <div class="form-group col-md-4">
                                  <label>Price</label>
                                  <input required onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46" type="text" name="price" class="form-control" placeholder="Please Enter Price">
                                </div>
                                <!-- <div class="form-group col-md-4">
                                  <label>Final Price</label>
                                  <input onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46" required type="text" name="final_price" class="form-control" placeholder="Please Enter Price">
                                </div>
                                <div class="form-group col-md-4">
                                  <label>Special Price</label>
                                  <input onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46" required type="text" class="form-control" name="special_price" placeholder="Please Enter Price">
                                </div> -->
                               
                            <!-- </div> -->
                                <div class="form-group col-md-4">
                                  <label>Qty</label>
                                  <input required type="number" name="qty" class="form-control" placeholder="Please Enter Qty">
                                </div>
                                <div class="form-group col-md-4">
                                  <label>Fee</label>
                                  <input onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46" name="fee" required type="text" class="form-control" placeholder="Please Enter Fee">
                                </div>
                                <!-- <div class="form-group col-md-3">
                                  <label>Int Fee</label>
                                  <input onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46" name="int_fee" required type="text" class="form-control" placeholder="Please Enter Int Fee">
                                </div>  -->                              
                            </div>                            
                        </div>
                    </div> 
                    <div class="box box-primary"> 
                        <h4 class="ml20">Product Attributes Details</h4>
                        <div class="box-body">    
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                     <label>Is 18 YRS</label>
                                     <select name="is_18" class="form-control">
                                         <option value="0">No</option>
                                         <option value="1">Yes</option>
                                     </select>                                    
                                </div>
                                <div class="form-group col-md-4">
                                  <?php if($_GET['type'] != 1) { ?>

                                 <label>Upload Game/Music</label>    <small>[ Upload only .zip,.rar,.7zip file ]</small>
                                  <input required type="file" class="form-control" accept=".zip,.rar,.7zip" name="game_file"/> 
                             
                                  <?php } ?>
                                </div>
                                <div class="form-group col-md-4">
                                  
                                </div>
                              </div>
                                <!-- <div class="form-group"> -->
                                <!-- <div class="form-group col-md-4">
                                    <label>Seller Name</label>
                                    <input type="text" class="form-control" required name="seller_name" placeholder="Please Enter Seller Name">
                                </div> -->
                                <?php if($type == 1) { ?>
                                <div class="form-row">
                            <?php if(count($attribute_list) > 0) {
                                foreach($attribute_list as $key => $v) {
                                  $arr_attr = explode('_',$key); ?>
                                    <div class="form-group col-md-12">
                                      <label class="col-sm-12"><?php echo ucfirst($arr_attr[0]).' Turned on/off' ; ?></label>
                                          <?php foreach($v as $v1) { 
                                                if($arr_attr[0] == $v1->title){ ?>
                                                  
                                                  <input name="attr_id[<?php echo $v1->attr_id.'_'.$v1->option_id;?>]" cust-name="<?php echo $arr_attr[0];?>" type="checkbox" id='<?php echo $v1->attr_id.'_'.$v1->option_id; ?>' class='chk-btn' value="<?php echo $v1->option_label; ?>"/>
                                                  <label for='<?php echo $v1->attr_id.'_'.$v1->option_id; ?>'><?php echo $v1->option_label; ?></label>
                                                
                                                    
                                            <?php } }?>
                                                                                 
                                        <?php ?>
                                       </div>
                                   <?php  }
                                }
                            ?>
                            </div>
                             <div class="form-group row" id="dynamic_attr">
                            </div>
                          <?php }  ?>
                            
                          
                           
                        </div>
                    </div>
                    <div class="box box-primary"> 
                        <h4 class="ml20">Product Images Details</h4>
                        <div class="box-body"> 
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label>Upload Product Main Image</label>
                                    <input required type="file" class="upload_file form-control" id="upload_file1" name="main_image" onchange="preview_image('upload_file1','image_preview1');"/>  
                                    <div id="image_preview1"></div>                                  
                                </div>
                                <div class="form-group col-md-8">
                                    <label>Upload Product Gallery</label>
                                    <input required type="file" class="form-control upload_file" id="upload_file" name="gallery_image[]" onchange="preview_image('upload_file','image_preview');" multiple/> 
                                    <div id="image_preview"></div>                       
                                </div>
                                
                            </div>
                        </div>
                        <div class="box-footer" align="center">                               
                            <button class="btn btn-primary" type="submit" id="edit_game_update">Submit</button>
                        </div>
                    </div>    
                </form>
        </div>
    </div>
</section>
<script type="text/javascript">
    $('#category_id').change(function(){
    var category_id = $('#category_id').val();
    if(category_id != '')
    {
     $.ajax({
      url:"<?php echo base_url(); ?>admin/products/fetchSubcat",
      method:"POST",
      data:{category_id:category_id},
      success:function(data)
      {
       $('#sub_cat').html(data);     
      }
     });
    }
    else
    {
     $('#sub_cat').html('<option value="">Select Sub-Category</option>');   
    }
 });

</script>
<style type="text/css">
    .ml20 {
        margin-left: 20px;
        font-weight: bold;
    }
    input.chk-btn {
  display: none;
}
input.chk-btn + label {
  border: 1px solid #fff;
  /*background: ghoswhite;*/
  background: red;
  color: #fff;
  padding: 5px 8px;
  cursor: pointer;
  border-radius: 5px;
}
input.chk-btn:not(:checked) + label:hover {
  box-shadow: 0px 1px 3px;
}
input.chk-btn + label:active,
input.chk-btn:checked + label {
  box-shadow: 0px 0px 3px inset;
  /*background: #eee;*/
  background: green;
  color: #fff;
}
.remove_img{
    display: block;
    color: #fff;
    background: #2c93eb;
    text-align: center;
    padding: 4px 0px;
    border-radius: 0px 0px 12px 12px;
}
</style>
