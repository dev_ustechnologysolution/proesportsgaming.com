<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php $this->load->view('admin/common/show_message') ?>
                        
                <form  action="<?php echo base_url(); ?>admin/products/productUpdate" method="post" enctype="multipart/form-data" class="admin_game_edit_form">
                    <div class="box box-primary"> 
                        <h4 class="ml20">Product Basic Details</h4>
                        <input type="hidden" name="id" value="<?php echo $product_data[0]['id']; ?>">
                        <div class="box-body">                        
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                  <label for="name">Title</label>
                                    <input required type="text" required id="name" class="form-control" name="name" placeholder="Please enter product name" value="<?php echo $product_data[0]['name']; ?>"/>
                                </div>
                                <div class="form-group col-md-3">
                                  <label for="name">Select Main Category</label>

                                      <select required class="form-control" name="category_id" id="category_id"><option value="">Please Select Category</option><?php 
                                    foreach ($category_list as $key => $value) { $arr_category = explode(",", $product_data[0]['category_id']);
                                      if($value['parent_id'] == 0){ 
                                      ?>
                                        <option <?php echo ($arr_category[0] == $value['id']) ? 'selected' : '';?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                            
                                        <?php } } ?>
                                    </select>
                                </div>  
                                <div class="form-group col-md-3">
                                  <label for="name">Select Sub Category</label>

                                      <select required class="form-control" name="subcategory_id" id="sub_cat"><option value="">Please Select Category</option><?php 
                                    foreach ($category_list as $key => $value) { $arr_category = explode(",", $product_data[0]['category_id']);
                                      if($value['parent_id'] == $arr_category[0]){ 
                                      ?>
                                        <option <?php echo ($arr_category[1] == $value['id']) ? 'selected' : '';?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                            
                                        <?php } } ?>
                                    </select>
                                </div>                          
                            </div>
                            <div class="form-row">                           
                                <div class="form-group col-md-12">
                                   <label for="description">Description</label>
                                    <textarea required placeholder="Please enter product Description" class="form-control" rows="6" name="description"><?php echo $product_data[0]['description']; ?> </textarea> 
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="box box-primary"> 
                        <h4 class="ml20">Product Price Details</h4>
                        <div class="box-body">   
                            <div class="form-row">
                                <div class="form-row">  <div class="form-group col-md-4">
                                  <label>Qty</label>
                                  <input required type="number" name="qty" class="form-control" placeholder="Please Enter Qty" value="<?php echo $product_data[0]['qty']; ?>">
                                </div>                             
                                <div class="form-group col-md-4">
                                  <label>Price</label>
                                  <input required onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46" type="text" name="price" class="form-control" value="<?php echo $product_data[0]['price']; ?>" placeholder="Please Enter Price">
                                </div>
                                <div class="form-group col-md-4">
                                  <label>Fee</label>
                                  <input onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46" name="fee" required type="text" class="form-control" placeholder="Please Enter Fee" value="<?php echo $product_data[0]['fee']; ?>">
                                </div>
                               <!--  <div class="form-group col-md-3">
                                  <label>Int Fee</label>
                                  <input onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 44 || event.charCode == 0 || event.charCode == 46" name="int_fee" required type="text" class="form-control" placeholder="Please Enter Int Fee" value="<?php echo $product_data[0]['int_fee']; ?>">
                                </div>  -->                               
                            </div>                                                              
                            </div>                            
                        </div>
                    </div> 
                    <div class="box box-primary"> 
                      <h4 class="ml20">Product Attributes Details</h4>
                      <div class="box-body"> 
                        <div class="form-row">
                          <div class="form-group col-md-4">
                              <label>Is 18 YRS</label>
                               <select name="is_18" class="form-control">
                                   <option <?php echo ($product_data[0]['is_18'] == 0) ? 'selected' : '';?> value="0">No</option>
                                   <option <?php echo ($product_data[0]['is_18'] == 1) ? 'selected' : '';?> value="1">Yes</option>
                               </select>                          
                          </div>
                          <div class="form-group col-md-4">
                           <?php if($product_data[0]['type'] != 1) { ?>
                              <label>Upload Game/Music</label>    <small>[ Upload only .zip,.rar,.7zip file ]</small>
                               <input required type="file" class="form-control" accept=".zip,.rar,.7zip" name="game_file"/>
                            <?php } ?>
                          </div>
                          <div class="form-group col-md-4"></div>
                        </div>
                        <?php if ($product_data[0]['type'] == 1) { ?>
                          <div class="form-row">
                            <?php if(count($attribute_list) > 0) {
                              $arr_selected_options = array_column($product_attribute_data, 'option_id');
                              foreach($attribute_list as $key => $v) { $arr_attr = explode('_',$key); ?> 
                                <div class="form-group col-md-12">
                                  <label><?php echo $arr_attr[0]; ?></label>
                                  <?php foreach($v as $v1) {
                                    if($arr_attr[0] == $v1->title) { ?>
                                      <input cust-name="<?php echo $arr_attr[0];?>" name="attr_id[<?php echo $v1->attr_id.'_'.$v1->option_id;?>]" <?php echo (in_array($v1->option_id, $arr_selected_options)) ? 'checked' : '';  ?> type="checkbox" id='<?php echo $v1->attr_id.'_'.$v1->option_id; ?>' class='chk-btn' value="<?php echo $v1->option_label; ?>"/>
                                      <label for='<?php echo $v1->attr_id.'_'.$v1->option_id; ?>'><?php echo $v1->option_label; ?></label>
                                    <?php } ?>
                                  <?php } ?>
                                </div>
                              <?php } ?>
                            <?php } ?>
                          </div>
                        <?php } ?>
                      </div>
                      <div class="form-group row" id="dynamic_attr">
                        <?php
                        if (count($attribute_list) > 0) {
                          $arr_selected_options = array_column($product_attribute_data, 'option_id');
                          foreach($attribute_list as $key => $v) {
                            $arr_attr = explode('_',$key); ?> 
                            <?php foreach($v as $v1) {
                              if(strtolower($v1->title) == 'color' && (in_array($v1->option_id, $arr_selected_options))){ ?>
                                <div class="form-group col-md-4" id="remove<?php echo $key; ?>">
                                  <label for='<?php echo $v1->attr_id.'_'.$v1->option_id; ?>'><?php echo $v1->option_label; ?></label> 
                                  <input  multiple type="file" id='<?php echo 'img_'.$v1->attr_id.'_'.$v1->option_id; ?>' name="attr_image[<?php echo $v1->attr_id.'_'.$v1->option_id;?>][]" class='form-control' onchange="preview_image('<?php echo 'img_'.$v1->attr_id.'_'.$v1->option_id; ?>','image_<?php echo $v1->option_label; ?>');"/>
                                  <div id="image_<?php echo $v1->option_label; ?>" class="image_<?php echo $v1->option_label; ?>">
                                    <?php 
                                    foreach($product_attribute_data as $option_data) {
                                      if($option_data['option_id'] == $v1->option_id) {
                                        $arr_image = explode(",", $option_data['option_image']);
                                        $j = 1;
                                        $last_key = end(array_keys($arr_image));
                                        foreach($arr_image as $k => $vimg) {
                                          if($vimg != '') {
                                            $result .= $v1->option_id.'_'.$k.','; ?>
                                            <div style="margin:10px;display:inline-block;" id="removeimg_<?php echo $v1->option_id.'_'.$k; ?>">
                                              <input style="vertical-align:top;margin-right:10px;" type="hidden" name="is_defult" class="is_defult">
                                              <span>
                                                <img style="border-radius: 12px 12px 0px 0px;" width="210" height="210" src="<?php echo base_url().'upload/products/'.$vimg; ?>">
                                                <span style="cursor: pointer;" class="remove_img" onclick="remove_image1('removeimg_<?php echo $v1->option_id.'_'.$k; ?>',1,'<?php echo $v1->option_id.'_'.$k; ?>');">
                                                  <i class="fa fa-trash fa-fw"></i>Remove Image
                                                </span>
                                              </span>
                                            </div>
                                          <?php }
                                          $j++;
                                        }
                                      }
                                    } ?>
                                  </div>
                                </div>  
                              <?php } ?>
                            <?php } ?>
                          <?php }
                        } ?>
                        <input type="hidden" id="old_attr_image" name="old_attr_image" value="<?php echo $result; ?>">
                      </div>
                    </div>
                      <div class="box box-primary"> 
                          <h4 class="ml20">Product Images Details</h4>
                          <div class="box-body"> 
                          
                              <div class="form-row">
                                <div class="form-group col-md-6">
                                      <label>Upload Product Main Image</label>
                                      <input type="file" class="upload_file1 form-control" id="upload_file1" name="main_image" onchange="preview_image('upload_file1','image_preview1');"/>  
                                      <div id="image_preview1">
                                        <?php if(count($product_image_data) > 0){ 

                                          foreach ($product_image_data as $key => $single_img) {
                                            if($single_img['is_default'] == 1) { ?>
                                              <div style='margin:10px;display:inline-block' id="removeupload_file1">
                                            <input style='vertical-align:top;margin-right:10px;' type='hidden' name='is_defult' class='is_defult'>
                                           
                                            <img  width='250' src='<?php echo base_url().'upload/products/'.$single_img['image']?>'>
                                
                                          </div>

                                        <?php }
                                      }
                                    } ?>
                                  </div>
                                </div>
                                  
                                  <div class="form-group col-md-6">
                                      <label>Upload Product Gallery</label>
                                       <input  type="file" class="form-control upload_file" id="upload_file" name="gallery_image[]" onchange="preview_image('upload_file','image_preview');" multiple/> 
                                      <div id="image_preview">
                                        <?php 
                                        if(count($product_image_data) > 0){ 

                                          foreach ($product_image_data as $key => $single_img) {
                                            if($single_img['is_default'] == 0) {
                                               $result1 .= $single_img['id'].',';
                                          ?>
                                          <div style='margin:10px;display:inline-block' id="remove_<?php echo $single_img['id'];?>">
                                            <input style='vertical-align:top;margin-right:10px;' type='hidden' name='is_defult' class='is_defult'>
                                            <span>
                                            <img  width='250' src='<?php echo base_url().'upload/products/'.$single_img['image']?>'>
                                            <span style="cursor: pointer;" class="remove_img" onclick="remove_image('remove_<?php echo $single_img['id'];?>',2,'<?php echo $single_img['id'].','; ?>');">
                                  <i class="fa fa-trash fa-fw"></i>Remove Image</span>
                                </span>
                                          </div>

                                        <?php }} }?>
                                        <input type="hidden" name="old_gallery_image" id="old_gallery_image" value="<?php echo $result1; ?>">
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="box-footer" align="center">                               
                              <button class="btn btn-primary" type="submit" id="edit_game_update">Submit</button>
                          </div>
                      </div>    
                </form>
        </div>
    </div>
</section>
<script type="text/javascript">
   $('#category_id').change(function(){
    var category_id = $('#category_id').val();
    if(category_id != '')
    {
     $.ajax({
      url:"<?php echo base_url(); ?>admin/products/fetchSubcat",
      method:"POST",
      data:{category_id:category_id},
      success:function(data)
      {
       $('#sub_cat').html(data);     
      }
     });
    }
    else
    {
     $('#sub_cat').html('<option value="">Select Sub-Category</option>');   
    }
 });
function preview_image() 
{
 var total_file=document.getElementById("upload_file").files.length;
 for(var i=0;i<total_file;i++)
 {
  $('#image_preview').append("<div style='margin:10px;display:inline-block'><input style='vertical-align:top;margin-right:10px;' type='hidden' name='is_defult' class='is_defult'><img  width='250' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
 }
}
function remove_image1(id,type,img_name){
    $('#'+id).remove();
    if(type == 1){
        var str = $('#old_attr_image').val();
        str = str.replace(img_name+',', '');
        $('#old_attr_image').val(str);
    } else {
        var str = $('#old_gallery_image').val();
        str = str.replace(img_name, '');
        $('#old_gallery_image').val(str);
    }
    
   
  }

</script>
<style type="text/css">
.ml20 {
    margin-left: 20px;
    font-weight: bold;
}
input.chk-btn {
display: none;
}
input.chk-btn + label {
  border: 1px solid #fff;
  /*background: ghoswhite;*/
  background: red;
  color: #fff;
  padding: 5px 8px;
  cursor: pointer;
  border-radius: 5px;
}
input.chk-btn:not(:checked) + label:hover {
  box-shadow: 0px 1px 3px;
}
input.chk-btn + label:active,
input.chk-btn:checked + label {
  box-shadow: 0px 0px 3px inset;
  /*background: #eee;*/
  background: green;
  color: #fff;
}
.remove_img{
    display: block;
    color: #fff;
    background: #2c93eb;
    text-align: center;
    padding: 4px 0px;
    border-radius: 0px 0px 12px 12px;
}
</style>