<section class="content" style="margin-top:30px;">
  
   
  <?php $this->load->view('admin/common/show_message') ?> 
 
  <div class="box">
    <div class="box-header"> 
      <a href="<?php echo base_url().'admin/products/productAdd?type=2'; ?>"><button style="margin:0px 10px;" class="btn pull-right btn-primary btn-xl">Add Music</button></a> 

     <a href="<?php echo base_url().'admin/products/productAdd?type=2'; ?>"><button style="margin:0px 10px;" class="btn pull-right btn-primary btn-xl">Add Game</button></a>
          
      <a href="<?php echo base_url().'admin/products/productAdd?type=1'; ?>"><button style="margin:0px 10px;" class="btn pull-right btn-primary btn-xl">Add Item</button></a>

       <button style="margin:0px 6px;" class="btn pull-left btn-primary btn-xl" onclick="return changeProductFee();">Set Global Product Fee</button> 
    </div>
   
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Item.No</th>
            <th>Item</th>
            <th>Item Title</th>
            <th>Description</th>
            <th>Amount</th>
            <th>Fee</th>
            <th>Int Fee</th>
            <th>Action</th>
            <th>18 YRS+</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; foreach($list as $k => $product){ 
            $image_arr = $this->General_model->view_single_row('product_images',array('is_default' => '1','product_id' => $product['id']),'image');
            ?>
          <tr>
            <td><?php echo ($product['product_order'] == 0) ? '' : $product['product_order']; ?>&nbsp;&nbsp;<a href="#" class="lobby_view" id="<?php echo $product['id']; ?>"><i class="fa fa-pencil fa-fw"></i></a></td>
            <td><img width="160" src='<?php echo base_url()."upload/products/".$image_arr["image"];?>'></td>
            <td><b><?php echo strlen($product['name']) > 50 ? substr($product['name'],0,50)."..." : $product['name']; ?></b></td>
            <td><?php echo strlen($product['description']) > 150 ? substr($product['description'],0,150)."..." : $product['description'];?></td>
            <td><?php echo $product['price']; ?></td>
            <td><?php echo $product['fee']; ?></td>
            <td><?php echo $product['int_fee']; ?></td>              
            <td>
              <a title="Edit Product" href="<?php echo base_url().'admin/products/productEdit?id='.$product['id']; ?>">
                <i class="fa fa-pencil fa-fw"></i>
              </a>
              <a title="Update Product Amount" onclick="return changeEventprice('<?php echo $product['id']; ?>','spectate_price');">
                <i class="fa fa-money fa-fw"></i>
              </a>
              <a title="Delete Product"  href="<?php echo base_url().'admin/products/delete?id='.$product['id']; ?>" id="<?php echo $product['id']; ?>" action="delete_product_id"> 
                <i class="fa fa-trash fa-fw"></i>
              </a>
            </td>
            <td><label class="myCheckbox1 text-center" style="width:100%;"><input style="width: 20px;height:20px;" <?php echo ($product['is_18'] == 1) ? 'checked' :'';?>  class="single-checkbox custom-cb is_18" id="<?php echo $product['id']; ?>" type="checkbox" name="progress" value="<?php echo $product['id'];?>"><span></span></label></td> 
          </tr>
          <?php }?>
        </tbody>
      </table>
    </div>
   
  </div>
</section>
<div class="modal fade  custom-width" id="modal-1">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">      
    </div>
  </div>
</div>

<script type="text/javascript">
  function changeEventprice(id,type){
  $.ajax({
      url : '<?php echo site_url(); ?>admin/products/getProductPrice/?id='+id+'&type='+type,
      success: function(result) {
        if(result){
          $('.modal-content').html(result);
          $('#modal-1').modal('show');
        }
      }
    });
}
function changeProductFee(){
  $.ajax({
    url : '<?php echo site_url(); ?>admin/Products/getProductFee/',
    success: function(result) {
      if(result){
        $('.modal-content').html(result);
        $('#modal-1').modal('show');
        $(".err_msg").html('');
      } 
      }     
  });
}

$(document).on('submit','#change_event_price',function(e){
  e.preventDefault();
  var data = $(this).serialize();
  if(data){
    $.ajax({
      url: "<?php echo site_url(); ?>admin/products/productUpdate",
      data: data,
      type: 'POST',
      success: function(result) {
        window.location.href = "<?php echo site_url(); ?>admin/products/";       
      }
    });
  }             
});

   $(document).on('click','.lobby_view',function(){
  var id = $(this).attr('id');
  if(id){
    $.ajax({
      url : '<?php echo site_url(); ?>admin/products/ChangeProductOrder/?id='+id,
      success: function(result) {
        if(result){
          $('.modal-content').html(result);
          $('#modal-1').modal('show');                              

        }
      }
    });
  }
});
$(document).on('submit','#change_product_order',function(e){
  e.preventDefault();
  var data = $(this).serialize();
  if(data){
    $.ajax({
      url: "<?php echo site_url(); ?>admin/products/UpdateProductOrder",
      data: data,
      type: 'POST',
      success: function(result) {
        window.location.href = "<?php echo site_url(); ?>admin/products/";       
      }
    });
  }             
});
$(".is_18").change(function() {
  if(this.checked) {
    var type = '1';
  } else {
    var type = '0';
  }
  var id = this.id;
  $.ajax({
      url : '<?php echo site_url(); ?>admin/products/updateIs18?type='+type+'&id='+id,
      success: function(result) {
        window.location.href = "<?php echo site_url(); ?>admin/products/";
      }
    });
});
</script>
