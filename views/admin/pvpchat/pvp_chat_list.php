<section class="content background-white">
   <div class="row">
      <div class="col-md-12">
         <div class="row">
         <div class="agent_navigation pull-right">
          <a href="" class="" onclick="location.reload()">Refresh</a>
          </div>
            <div class="col-md-12 col-sm-12 col-xs-12 xwb-main-contact-conversation">
               <div class="table-responsive agent_table">
                  <table class="table table-hover" id="example1">
                     <thead>
                        <tr class="active">
                          <th>Sl. no. #</th>
                          <th>Acct. #</th>
                          <th>Player 1</th>
                          <th>Acct. #</th>
                          <th>Player 2</th>
                          <th>Requesting Help</th>
                          <th style="text-align: center;">Open Chat</th>
                          <th>Game Category</th>
                          <th>Status</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                   
                           $CI =& get_instance();

                           $result = $CI->chatsocket->getPvpPlayerlist();

                           // $group_result = $CI->chatsocket->getPvpGroupPlayerlist();
                           $group_result = $data['res_data'];
                           $i = 1;
                               foreach ($result as $key => $value) 
                               {
                                  $game_id = $value->game_id;
                                  $game_name = $value->game_name;
                                  $accepter_id=$value->accepter_id;
                                  $challenger_id=$value->challenger_id;
                                  $grp_result = $CI->chatsocket->getPvpConversationGrouplist($game_id);
                                  $grp_users = implode("|",$grp_result);
                                  $request_for_admin = 'No';
                                    if ($value->is_requested == '1' && $value->is_accepted == '1') {
                                      $request_for_admin = 'No';
                                    }
                                    if ($value->is_requested == '1' && $value->is_accepted == '0') {
                                      $request_for_admin = '<span style="color:#FF5000;">';
                                      $request_for_admin .= 'Yes';
                                      $request_for_admin .= '</span>';
                                    }                                
                                    $group_status = $CI->chatsocket->group_status($game_id);
                                    if (array_sum($group_status) == '0') {
                                      $group_status_res = 'Complete';  
                                    }
                                    else
                                    {
                                      $group_status_res = 'Open';
                                    }
                                     // $message_page .='<tr>
                                     // <td>'.$i.'</td>
                                     // <td>'.$value->challenger_id.'</td>
                                     // <td>'.$value->device_id.'</td>
                                     // <td>'.$value->accepter_id.'</td>
                                     // <td>'.$value->accepter_device_id.'</td>
                                     // <td>'.$request_for_admin.'</td>
                                     // <td align="center" class="open_pop_up_for_pvp_chat_admin_side ' . $offline . '" data-accepterid="'.$accepter_id.'" data-challengerid="'.$challenger_id.'" data-useraa="'.$grp_users.'" data-user = "'.$accepter_id.'|'.$challenger_id.'" data-agentonline="'.$agent_online.'" data-status="' . $offline . '" data-ci="' .$game_id . '" data-gamename="' .$game_name. '"  ><a href="javascript:;"><i class="fa fa-comments" aria-hidden="true"></i></a></td>
                                     // <td>'.$value->game_name.'</td>
                                     // <td>'.$group_status_res.'</td>
                                     // </tr>';
                                    ?>
                                     <tr>
                                       <td><?php echo $i; ?></td>
                                       <td><?php echo $value->challenger_id; ?></td>
                                       <td><?php echo $value->device_id; ?></td>
                                       <td><?php echo $value->accepter_id; ?></td>
                                       <td><?php echo $value->accepter_device_id; ?></td>
                                       <td><?php echo $request_for_admin; ?></td>
                                       <!-- <td align="center" class="open_pop_up_for_pvp_chat_admin_side ' . $offline . '" data-accepterid="'.$accepter_id.'" data-challengerid="'.$challenger_id.'" data-useraa="'.$grp_users.'" data-user = "'.$accepter_id.'|'.$challenger_id.'" data-agentonline="'.$agent_online.'" data-status="' . $offline . '" data-ci="' .$game_id . '" data-gamename="' .$game_name. '"  ><a href="javascript:;"><i class="fa fa-comments" aria-hidden="true"></i></a></td> -->
                                       <td align="center"><a class="link_to_open_chat" onclick='OpenPopupCenter("<?php echo base_url(); ?>admin/pvpchat/pvpgroup_adminside_chat_box/?user_to_from_admin_group=<?php echo $accepter_id; ?>|<?php echo $challenger_id; ?>&accepterid=<?php echo $accepter_id; ?>&challengerid=<?php echo $challenger_id; ?>&useraa=<?php echo $grp_users; ?>&user=<?php echo $accepter_id; ?>|<?php echo $challenger_id; ?>&agentonline=<?php echo $agent_online; ?>&status=<?php echo $offline; ?>&ci=<?php echo $game_id; ?>")'><i class="fa fa-comments" aria-hidden="true"></i></a></td>
                                       <td><?php echo $value->game_name; ?></td>
                                       <td><?php echo $group_status_res; ?></td>
                                       </tr>
                                       <?php
                                     $i++;
                                   }

                               // $message_page .= csListUsers();
                           ?>
                     </tbody>
                  </table>
               </div>
            </div>
           <div class="modal fade  custom-width" id="modal-1">
            <div class="modal-dialog" style="width: 50%;">
              <div class="modal-content">
                
              </div>
            </div>
          </div>
         <script> 
            $(document).on('click','.agent_login_for_chat',function(){
              var id = $(this).attr('id');
              if(id){
                $.ajax({
                  url : '<?php echo site_url(); ?>admin/FrontUser/chat_security_pass/'+id,
                  success: function(result) {
                    if(result){
                      $('.modal-content').html(result);
                      $('#modal-1').modal('show');                              
                    }
                  }
                })
              }
            })
          </script> 
         </div>
      </div>
   </div>
</section>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12 modal" align="center" role="dialog" id="pvp_msgbox_for_admin">
    <?php 
    ob_start();
    $rel_msg = '<div id="message-container_pvp" class="xwb-message-container pvp_msgbox-message-container">
                <div class="chat_logo_div" align="center"><img src="'.base_url().'assets/frontend/images/chat_top.png"></div>
                      <div class="panel panel-info modal-content" align="left">
                         <div class="panel-heading modal-header">
                          <button type="button" class="close close_model col-sm-1" data-dismiss="modal">&times;</button>
                            <h3 class="conversation-name col-sm-11"></h3>
                         </div>
                      </div>
             
               <div class="row overflow_manage">
               <div class="col-sm-10">
               <div id="message-inner" class="message-inner">

               </div>
               </div>
               <div class="col-sm-2 add_adminside_chat banner_on_box">
               </div>
               <div class="col-sm-12 sponsered_by_logo">
               </div>
               </div>
             
               
               <div class="msg-input input-group col-sm-11 msgbox_at_adminside">
                    <textarea id="message-input" class="form-control message-input message-input-admin-side" style="resize:none;" rows="4" name="message-input"></textarea>
                    <input type="hidden" id="csrf_key" class="csrf_key" name="<?php echo $CI->security->get_csrf_token_name(); ?>" value="<?php echo $CI->security->get_csrf_hash(); ?>" />

                    <input type="hidden" name="user_id" class="user_id" id="user_id" value="<?php echo $CI->chatsocket->current_user; ?>">
                   <input name="ci" class="ci" id="ci" value="" type="hidden" />
                  <div class="input-group-btn message_send_div">
                      <button class="btn btn-success" id="send-message-to-pvp-player-from-admin">Send</button>
                    </div>
                </div>
         </div>
         ';
          $out = ob_get_contents();
            ob_end_clean();
            if ($CI->chatsocket->enabled_users!=null) {
                if (!in_array($CI->chatsocket->current_user, $CI->chatsocket->enabled_users)) {
                    $out = '';
                }
            }

            if ($CI->chatsocket->conversation_count == 0) {
                $out = '';
            }

         $rel_msg .= $out;
         $message_page = $rel_msg;
            if ($return) {
                return $message_page;
            } else {
                echo $message_page;
            }
       ?>
   </div>


