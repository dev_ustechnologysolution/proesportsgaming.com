<section class="content">
  <div class="row">
    <div class="col-md-12">
			<?php $this->load->view('admin/common/show_message') ?>
        <div class="box box-primary">
          <?php if(count($edit_data)>0){?>
            <form  action="<?php echo base_url(); ?>admin/screenshot/screenshotUpdate" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="name">Game Name</label>
                  <input type="text" id="name" class="form-control" name="name" value="<?php if(isset($edit_data[0]['game_name']))echo $edit_data[0]['game_name'];?>">
                </div>
               
					      <div class="form-group">
                  <label for="description">Description</label>
                  <textarea class="form-control" id="description" name="description"><?php if(isset($edit_data[0]['description'])) echo $edit_data[0]['description'];?></textarea>
                </div>

                <div class="form-group">
                    <label for="file">Image</label>
                    <input type="file" class="form-control" id="file" name="image">
                </div>

                <div class="form-group">
                  <img src="<?php echo base_url().'upload/screenshot/'.$edit_data[0]['image']?>" height="150" width="150">
                </div>
                <div style="color:red;"><?php echo $this->session->flashdata('msg'); ?></div>
                
              </div><!-- /.box-body -->

              <div class="box-footer">
                <input type="hidden" name="id" value="<?php if(isset($edit_data[0]['id'])) echo $edit_data[0]['id'];?>">
                <input type="hidden" name="old_image" value="<?php if(isset($edit_data[0]['image'])) echo $edit_data[0]['image'];?>">
                <button class="btn btn-primary" type="submit">Update</button>
              </div>
            </form>
          <?php } else {?>
            <h4>Data not found</h4>
          <?php }?>
        </div>
      </div>
    </div>
</section>