<section class="content">
   <div class="box">
      <?php $this->load->view('admin/common/show_message') ?>
      <div class="box-body">
      	 <table id="" class="table table-bordered table-striped">
            <thead>
               <tr>
                  <th>Sl No</th>
                  <th>BACKUP Category </th>
                  <th>Action</th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td>1</td>
                  <td>Site Backup</td>
                  <td><a href="<?php echo base_url();?>admin/Backup/file_backup"><button class="btn pull-left btn-primary btn-xl">Download</button></a></td>
               </tr>
               <tr>
                  <td>2</td>
                  <td>Database Backup</td>
                  <td><a href="<?php echo base_url();?>admin/Backup/db_backup"><button class="btn pull-left btn-primary btn-xl">Download</button></a></td>
               </tr>
            </tbody>
         </table>
      </div>
   </div>
</section>