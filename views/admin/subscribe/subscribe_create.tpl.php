<section class="content">
  <div class="row">   
    <div class="col-md-12">
      <?php $this->load->view('admin/common/show_message') ?>      
      <div class="box box-primary">
        <form enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/subscription/insert_subscription_plan" method="post">
            <div class="box-body">

              <div class="form-group row">
                <label for="title" class="col-sm-2">Title</label>
                <div class="col-sm-10">
                  <input required type="text" class="form-control" name="title" placeholder="Enter Title">
                </div>
              </div>

              <div class="form-group row">
                <label for="description" class="col-sm-2">Description</label>
                <div class="col-sm-10">
                  <textarea name="description" class="form-control" rows="10" cols="80" placeholder="Enter Description"></textarea>
                </div>
              </div>

              <div class="form-group row">
                <label for="image" class="col-sm-2">Image</label>
                <div class="col-sm-10">
                  <input required type="file" class="form-control" name="image" accept="image/*">
                </div>
              </div>

              <div class="form-group row">
                <label for="amount" class="col-sm-2">Amount ($)</label>
                <div class="col-sm-10">
                  <input required type="number" class="form-control" min="1" name="amount" placeholder="Enter Amount">
                </div> 
              </div>

              <div class="form-group row">
                <label for="fees" class="col-sm-2">Fees ($)</label>
                <div class="col-sm-10">
                  <input required type="number" class="form-control" min="0" name="fees" placeholder="Enter Fee">
                 </div> 
              </div>

              <div class="form-group row">
                <label for="terms_condition" class="col-sm-2">Terms & Policy</label>
                <div class="col-sm-10">
                  <textarea name="terms_condition" class="form-control" rows="10" cols="80" placeholder="Enter Terms Condition"></textarea>
                </div>
              </div>
   
            </div>
            <div class="box-footer col-sm-12" align="center"> 
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
      </div>
    </div>
  </div>
</section>