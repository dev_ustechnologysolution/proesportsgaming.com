<section class="content">
  <div class="nav-tabs-custom">
    <div class="box">
      <?php $this->load->view('admin/common/show_message'); ?>
      <div class="box-header">  
        <a href="<?php echo base_url().'admin/subscription/add_subscription_plan'; ?>">
          <button class="btn pull-right btn-primary btn-xl">Add Subscription Plan</button>
        </a>
      </div>
      <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Sl No</th>
              <th>Image</th>
              <th>Title</th>            
              <th>Description</th>
              <th>Amount($)</th>
              <th>Fees($)</th>
              <th>Status</th>
              <th>Created Date</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php $i=1; foreach($list as $value){ ?>
            <tr>
              <td><?php echo $i++; ?></td>            
              <td><img src="<?php echo base_url().'upload/subscription/'.$value['image'];?>" width="85" height="50"></td>
              <td><?php echo $value['title']; ?></td>
              <td><?php echo $value['description']; ?></td>
              <td><?php echo $value['amount']; ?></td>
              <td><?php echo $value['fees']; ?></td>
              <td><?php echo ($value['status'] == 1) ? 'Active' : 'Inactive'; ?></td>
              <td><?php echo $value['start_date'];?></td>
              <td>
                <a href="<?php echo base_url().'admin/subscription/subscription_plan_edit/'.$value['id'];?>"> <i class="fa fa-pencil fa-fw"></i></a>|
                <a class="delete-data" id="<?php echo $value['id']; ?>" action="delete_subscription_plan"><i class="fa fa-trash-o"></i></a>
              </td>
             </tr>
            <?php }?>
          </tbody>
        </table>
      </div>
    </div>
  </div>  
</section>

<div class="modal fade  custom-width" id="modal-1">
  <div class="modal-dialog" style="width: 50%;">
    <div class="modal-content">
      
    </div>
  </div>
</div>