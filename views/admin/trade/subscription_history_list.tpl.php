<section class="content">
  <div class="box">
    <?php $this->load->view('admin/common/show_message'); ?>
    <div class="box-header"></div>
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Sl. No.</th>
            <th>Plan Id</th>
            <th>Amount</th>
            <th>Fee</th>
            <th>Payment Status</th>
            <th>Payment Date</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; foreach($subscription_history_data as $value) {
            ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $value->transaction_id; ?></td>
            <td><?php echo $value->amount; ?></td>
            <td><?php echo $value->fee; ?></td>
            <td><?php echo $value->payment_status; ?></td>
            <td><?php echo $value->created_at; ?></td>
           </tr>
          <?php }?>
        </tbody>
      </table>
    </div>
  </div>
</section>