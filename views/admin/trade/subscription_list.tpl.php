<section class="content">
  <?php $this->load->view('admin/common/show_message'); ?>
  <div class="box">
  <div class="box-header"></div>
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Sl. No.</th>
            <th>Plan Id</th>
            <th>Subscribe from Acc# </th>
            <th>Subscribe to Acc# </th>
            <th>Plan</th>
            <th>Amount</th>
            <th>Fee</th>
            <th>Created Date/Time</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; foreach($subscription_data as $value) {
            $subscribe_fromdetail = $this->General_model->view_single_row('user_detail','user_id',$value->user_from); 
            $subscribe_todetail = $this->General_model->view_single_row('user_detail','user_id',$value->user_to); 
            $plan_detail = $this->General_model->view_single_row('subscription_plans',array('id'=>$value->subscribption_plan_id),'*');
            ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $value->transaction_id; ?></td>
            <td><?php echo $subscribe_fromdetail['account_no']; ?>
            <td><?php echo $subscribe_todetail['account_no']; ?>
            <td><?php echo ucwords($plan_detail['title']);?></td>
            <td><?php echo $value->amount; ?></td>
            <td><?php echo $value->fee; ?></td>
            <td><?php echo $value->created_at;?></td>
            <td><a class="btn btn-primary delete-data plan_unsubscribe" action="unsubscribe_plan" id="<?php echo $value->transaction_id; ?>">Unsubscribe</a> | <a href="<?php echo base_url(); ?>admin/trade/subscription_history/?transaction_id=<?php echo $value->transaction_id; ?>">History</a></td>
           </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <div class="modal fade  custom-width" id="modal-1">
      <div class="modal-dialog" style="width: 50%;">
        <div class="modal-content">
          
        </div>
      </div>
    </div>
  </div>
</section>