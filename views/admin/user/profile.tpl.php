<section class="content">

 <div class="row">

            <div class="col-md-3">



              <!-- Profile Image -->

              <div class="box box-primary">

                <div class="box-body box-profile">

                  <img class="profile-user-img img-responsive img-circle" src="<?php if(isset($user_detail[0]['img'])) echo base_url().'upload/profile_img/'.$user_detail[0]['img']; ?>" alt="User profile picture">

                  <h3 class="profile-username text-center"><?php echo $this->session->userdata('admin_user_name')?></h3>

                 <h6 class="profile-username text-center"><?php echo $this->session->userdata('admin_user_role')?></h6>



                </div><!-- /.box-body -->

              </div><!-- /.box -->



            </div><!-- /.col -->

            <div class="col-md-9">

            <?php $this->load->view('admin/common/show_message'); ?>

           <!-- ajax message-->

            <div id="ajax_msg"></div>

                <!-- ajax message-->

             <div class="nav-tabs-custom">

                <ul class="nav nav-tabs">

                <li class="active"><a href="#my_details" data-toggle="tab">My Details</a></li>

                <li><a href="#Change_Password" data-toggle="tab">Change Password</a></li>

                

                 </ul>

                <div class="tab-content">

                 <div class="active tab-pane" id="my_details">

                    <form class="form-horizontal" action="<?php echo base_url().'admin/user/profile_edit';?>" method="post" enctype="multipart/form-data">

                      <div class="form-group">

                        <label for="inputName" class="col-sm-2 control-label">Name</label>

                        <div class="col-sm-10">

                          <input type="text" class="form-control" id="inputName" placeholder="Name" name="name" value="<?php if(isset($user_detail[0]['name'])) echo $user_detail[0]['name']; ?>">

                        </div>

                      </div>

                      <div class="form-group">

                        <label for="inputEmail" class="col-sm-2 control-label" >Email</label>

                        <div class="col-sm-10">

                          <input type="email" class="form-control" id="inputEmail" placeholder="Email" name="email" value="<?php if(isset($user_detail[0]['email'])) echo $user_detail[0]['email']; ?>" >

                        </div>

                      </div>

                      <div class="form-group">

                        <label for="inputPhone" class="col-sm-2 control-label">Phone</label>

                        <div class="col-sm-10">

                          <input type="text" class="form-control" id="inputPhone" placeholder="Phone" name="phone" value="<?php if(isset($user_detail[0]['phone'])) echo $user_detail[0]['phone']; ?>">

                        </div>

                      </div>

                      <div class="form-group">

                        <label for="inputAddress" class="col-sm-2 control-label">Address</label>

                        <div class="col-sm-10">

                          <textarea class="form-control" id="inputAddress" placeholder="Address" name="address"><?php if(isset($user_detail[0]['address'])) echo $user_detail[0]['address']; ?></textarea>

                        </div>

                      </div>

                      <div class="form-group">

                        <label for="inputImage" class="col-sm-2 control-label">Image</label>

                        <div class="col-sm-10">

                          <input type="file" class="form-control" id="inputImage" placeholder="Image" name="image">

                        </div>

                      </div>

                      

                       <div class="form-group">

                        <div class="col-sm-offset-2 col-sm-10">

                        <input type="hidden" name="old_image" value="<?php if(isset($user_detail[0]['img'])) echo $user_detail[0]['img']; ?>">

                          <button type="submit" class="btn btn-danger">Submit</button>

                        </div>

                      </div>

                    </form>

                  </div><!-- /.tab-pane -->

                  <div class="tab-pane" id="Change_Password">

                    <form class="form-horizontal">

                      <div class="form-group">

                        <label for="current_password" class="col-sm-2 control-label">Current Password</label>

                        <div class="col-sm-10">

                          <input type="password" class="form-control" id="current_password" placeholder="current password" name="current_password">

                        </div>

                      </div>

                      <div class="form-group">

                        <label for="new_password" class="col-sm-2 control-label" >New Password </label>

                        <div class="col-sm-10">

                          <input type="password" class="form-control" id="new_password" placeholder="new password" name="new_password">

                        </div>

                      </div>

                      <div class="form-group">

                        <label for="confirm_password" class="col-sm-2 control-label">Confirm New Password </label>

                        <div class="col-sm-10">

                          <input type="password" class="form-control" id="confirm_password" placeholder="confirm new password" name="confirm_password">

                        </div>

                      </div>

                      <div class="form-group">

                        <div class="col-sm-offset-2 col-sm-10">

                        <input type='hidden' id="base_url" value=<?php echo base_url();?>>

                        <input type="button" id="chang_pass" value="Submit" class="btn btn-danger">

                        </div>

                      </div>

                    </form>

                  </div> <!-- /.tab-pane -->

                  

                  </div><!-- /.tab-content -->

              </div><!-- /.nav-tabs-custom -->

            </div><!-- /.col -->

          </div><!-- /.row -->



        </section>