<section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
			<?php $this->load->view('admin/common/show_message') ?>
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add New Game</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form  action="<?php echo base_url(); ?>admin/game/add_game" method="post" enctype="multipart/form-data" onsubmit="return image_size_validation()">
                <div class="box-body">
                 <div class="form-group">
                      <label for="name">Name</label>
                      <input type="text" placeholder="Name" id="name" class="form-control" name="name" required="required">
                    </div>
                    
                    <div class="form-group">
                      <label for="price">Price</label>
                      <input type="text" placeholder="Price" id="price" class="form-control" name="price" required="required">
                    </div>
					
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="description" placeholder="description" name="description"></textarea>
                     </div>
                      <div class="form-group">
                        <label for="file">Image (190px-112px)</label>
                        <label id="image_size" style="display:none"></label>
                        <input type="file" class="form-control" id="file" placeholder="Image" name="image">
                        </div>
                     </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button class="btn btn-primary" type="submit">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

              <!-- Form Element sizes -->
                </div><!--/.col (left) -->
            
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>