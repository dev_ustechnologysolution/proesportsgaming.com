<div class="col-sm-12">
	<div class="pull-right" >
		<a href="<?php echo base_url(); ?>admin/FrontUser/get_cron_details" id="get_url_1" class="" > Refresh </a> &nbsp;|&nbsp; 
		<a href="<?php echo base_url(); ?>admin/FrontUser/get_reset_all" id="get_url_2" class="reset_get"> Reset All </a> &nbsp;|&nbsp; 
		<a href="<?php echo base_url(); ?>admin/FrontUser/get_reset_won" id="get_url_3" class="reset_get"> Reset Win </a> &nbsp;|&nbsp; 
		<a href="<?php echo base_url(); ?>admin/FrontUser/get_reset_lost" id="get_url_4" class="reset_get"> Reset Lost </a>&nbsp;|&nbsp;
		<a href="<?php echo base_url(); ?>admin/FrontUser/get_reset_with" id="get_url_5" class="reset_get"> Reset Withdrawal </a>&nbsp;|&nbsp;
		<a href="<?php echo base_url(); ?>admin/FrontUser/userExcel" id="get_url_6" class="reset_get"><i class="fa fa-print"></i> Export Excel</a> 
	</div>
</div>
<div class="clearfix"></div>
<section class="content">
  <div class="box">
    <?php $this->load->view('admin/common/show_message') ?>
      <div class="box-body">
	  
        <table id="example" class="table table-bordered table-striped">		
			<thead>
			<tr>
				<td><input type="text" id="search-acc" placeholder="Search Account"></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
            <tr>
              <th style="display:none"></th>
                <th>Acct#</th>
                <th>Name</th>
                <th>Display Name</th>
                <th>Esports Team</th>
                <th>Is 18</th>
                <th>Email</th>
                <th>Phone Number</th>               
                <th>State</th>
                <th>Country</th>                
				<th>Tax ID </th>
                <th>Won Total</th>
                <th>Lost Amt</th>
                <th>Withdrawal Amt</th>
                <th>Balance</th>
                <th>Status</th>
                <th>Last Login</th>
                <th>Action</th>
            </tr>
			</thead>
          <tbody>
            <?php foreach($list as $value){ 
				$img = '';
				if($value['is_admin'] == 1){
					$img = base_url().'upload/admin_dashboard/Chevron_3_Twitch_72x72.png';
				} 
				?>
              <tr>
                <td style="display:none"></td>
			<td><?php echo $value['account_no']?><a href="#" class="account_no" id="<?php echo $value['id']?>"><i class="fa fa-pencil fa-fw"></i></a><?php if($img != ''){ ?><img width="15" height="15" src="<?php echo $img;?>"><?php } ?></td>
                <td><?php echo $value['name']?></td>
                <td><?php echo $value['display_name']?></td>
                <td><?php echo $value['team_name']?></td>
                <td><?php echo ($value['is_18'] == 1) ? '18+' :'Under 18';?></td>
                <td><?php echo $value['email']?></td>
                <td><?php echo $value['number']?></td>                
                <td><?php echo $value['statename']?></td>
                <td><?php echo $value['countryname']?></td>                
				<td>
					<?php  if($value['taxid'] != '') { ?>
							<img src="<?php echo base_url() ?>assets/frontend/images/green.png" width="10" />
						<?php } else { ?>
							<img src="<?php echo base_url() ?>assets/frontend/images/red.png" width="10" />
						<?php }  ?>
				</td>
                <td><?php echo $value['won_amt'] ?></td>
                <td><?php echo $value['lost_amt'] ?></td>
                <td><?php echo $value['withdrawal_amt'] ?></td>
                <td><?php echo $value['total_balance'];?> <a href="#" class="view" id="<?php echo $value['id']?>"><i class="fa fa-pencil fa-fw"></i></a></td>
            
				<td><?php if($value['is_active'] == 0) { echo 'Active'; } else { echo 'Banned'; } ?></td>
				<td><?php echo date('m-d-Y H:i a',strtotime($value['last_login']));?></td>
                <td><a href="<?php echo base_url().'admin/FrontUser/userEdit/'.$value['id'];?>"><i class="fa fa-pencil fa-fw"></i></a><a href="javascript:void(0)" class="delete" id="del_<?php echo $value['id']?>"><i class="fa fa-trash-o fa-fw"></i></a><a href="<?php echo base_url(); ?>admin/FrontUser/exportGameHistory/params?user_id=<?php echo $value['id'];?>"> <i class="fa fa-print"></i></a></td>
              </tr>
            <?php }?>
          </tbody>
		  
        </table>
    </div>
  </div>
</section>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script> $.validate();</script>
<!-- Modal 1 (Basic)-->
<div class="modal fade  custom-width" id="modal-1">
	<div class="modal-dialog" style="width: 50%;">
		<div class="modal-content">
			
		</div>
	</div>
</div>

<script>
$(document).ready(function() 
{
	var table = $('#example').DataTable();
    $('#search-acc').on('keypress keyup', function()
	{	
		table
		.column(1)
		.search(this.value)
		.draw();
	});  
} );

conf = {
	onElementValidate : function(valid, $el, $form, errorMess) {
		if( !valid ) {
			errors.push({el: $el, error: errorMess});
		}
	}
};
lang = {};
$(document).on('submit','#security_update_bal_id',function(e){
	e.preventDefault();
	var data = $(this).serialize();
	if(data){
		$.ajax({
			url: "<?php echo site_url(); ?>admin/FrontUser/ChangeBalance",
			data: data,
			type: 'POST',
			success: function(result) {
				$('.modal-content').html(result);
				$('#modal-1').modal('show');
			}
		});
	}							
})
$(document).on('submit','#account_security_id',function(e){
	e.preventDefault();
	var data = $(this).serialize();
	if(data){
		$.ajax({
			url: "<?php echo site_url(); ?>admin/FrontUser/ChangeAccountNo",
			data: data,
			type: 'POST',
			success: function(result) {
				$('.modal-content').html(result);
				$('#modal-1').modal('show');				
			}
		});
	}							
})
$(document).on('submit','#changebalance_id',function(e){
	e.preventDefault();
	var data = $(this).serialize();
	if(data){
		$.ajax({
			url: "<?php echo site_url(); ?>admin/FrontUser/UpdateBalance",
			data: data,
			type: 'POST',
			success: function(result) {
				window.location.href = "<?php echo site_url(); ?>admin/FrontUser/";
			}
		});
	}							
})
$(document).on('submit','#change_account_no',function(e){
	e.preventDefault();
	var data = $(this).serialize();
	if(data){
		$.ajax({
			url: "<?php echo site_url(); ?>admin/FrontUser/UpdateAccountNo",
			data: data,
			type: 'POST',
			success: function(result) {
				if(result){
					$('.modal-content').html(result);
					$('#modal-1').modal('show');
				} else{
					$('#modal-1').modal('hide');
					window.location.href = "<?php echo site_url(); ?>admin/FrontUser/";				
				}
			}
		});
	}							
})
$(document).on('click','.view',function(){
	var id = $(this).attr('id');
	if(id){
		$.ajax({
			url : '<?php echo site_url(); ?>admin/FrontUser/security_data/'+id,
			success: function(result) {
				if(result){
					$('.modal-content').html(result);
					$('#modal-1').modal('show');															
				}
			}
		})
	}
})
$(document).on('click','.account_no',function(){
	var id = $(this).attr('id');
	if(id){
		$.ajax({
			url : '<?php echo site_url(); ?>admin/FrontUser/account_security_data/'+id,
			success: function(result) {
				if(result){
					$('.modal-content').html(result);
					$('#modal-1').modal('show');															
				}
			}
		})
	}
})

$(document).on('click','.reset_get',function(e){
	e.preventDefault();
	var id = $(this).attr('id');
	//var id = $(this).attr('href');
	if(id){
		$.ajax({
			url : '<?php echo site_url(); ?>admin/FrontUser/security_pass/'+id,
			success: function(result) {
				if(result){
					$('.modal-content').html(result);
					$('#modal-1').modal('show');															
				}
			}
		})
	}
})


$(document).on('submit','#security_pass',function(e){
	e.preventDefault();
	var data = $(this).serialize();
	var action_value = $('#action_value').val();
	if(data){
		$.ajax({
			url: "<?php echo site_url(); ?>admin/FrontUser/get_reset_data",
			data: data,
			type: 'POST',
			success: function(result) {
				if(result == 'success'){
					var action_url = $('#'+action_value).attr('href');
					window.location.href = action_url;
					$('#modal-1').modal('hide');
				} else {
					$('.modal-content').html(result);
					$('#modal-1').modal('show');				
				}
			}
		});
	}							
})


$(document).on('click','.delete',function(){
	var id = $(this).attr('id');
	var idsplit = id.split('_');
	
	if(idsplit[1]){
		$.ajax({
			url : '<?php echo site_url(); ?>admin/FrontUser/delete_data/'+idsplit[1],
			success: function(result) {
				if(result){
					$('.modal-content').html(result);
					$('#modal-1').modal('show');															
				}
			}
		})
	}
})

$(document).on('submit','#security_password_id',function(e){
	e.preventDefault();
	var data = $(this).serialize();
	if(data){
		$.ajax({
			url: "<?php echo site_url(); ?>admin/FrontUser/delete",
			data: data,
			type: 'POST',
			success: function(result) {
				if(result == true){
					window.location.href = "<?php echo site_url(); ?>admin/FrontUser/";
				} else {
					alert('Password Error');
					return false;
				}
							
			}
		});
	}							
})

</script>