<section class="content">

  <div class="row">

    <div class="col-md-12">

			<?php $this->load->view('admin/common/show_message') ?>

        <div class="box box-primary">

          <form  action="<?php echo base_url(); ?>admin/prize/contentUpdate" method="post" enctype="multipart/form-data">

            <div class="box-body">


              <div class="form-group">

                  <label for="description">Content</label>

                  <textarea class="form-control" name="description"><?php if(isset($work_data[0]['description']))echo $work_data[0]['description'];?></textarea>

              </div>

              <div class="form-group">

                <label for="file">Image</label>


                <input type="file" class="form-control" id="file" placeholder="Image" name="image">

              </div>

              <div class="form-group">

                  <img src="<?php echo base_url().'upload/gameflow/'.$work_data[0]['image']?>" height="150" width="150">

                </div>

              </div><!-- /.box-body -->

              <div class="box-footer">

                <input type="hidden" name="id" value="<?php if(isset($work_data[0]['id'])) echo $work_data[0]['id'];?>">

                <input type="hidden" name="old_image" value="<?php if(isset($work_data[0]['image'])) echo $work_data[0]['image'];?>">

                <button class="btn btn-primary" type="submit">Update</button>

              </div>

          </form>

        </div>

      </div>

    </div>

</section>