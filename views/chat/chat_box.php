<section class="body-middle innerpage">
   <div class="container">
      <div class="row chat-box_fronside customer_chatbox_main_row msg_bx_sec">
         <div class="col-md-12 col-sm-12 col-xs-12 " align="center" id="customer_services_msgbox">
            <div id="message-container" class="xwb-message-container">
               <div class="video_box">
                  <video autoplay id="vid1" width="400" muted="true" controls></video>
                  <video autoplay id="vid2" width="400" class="commercial_video" muted="true" controls></video>
                  <a class="skipvidtrigger">skip</a>
               </div>
               <div class="popup-content-wrapper_customer_fontside">
                  <div class="row overflow_manage">
                     <div class="col-sm-10">
                        <div id="message-inner" class="message-inner">
                        </div>
                     </div>
                     <div class="col-sm-2 add_adminside_chat banner_on_box">
                     </div>
                     <div class="col-sm-12 sponsered_by_logo">
                     </div>
                  </div>
                  <div class="msg-input input-group col-sm-11 msgbox_at_adminside">
                     <textarea id="message-input" class="form-control message-input message-input-frontend-side message-input-frontend-side-footer" style="resize:none;" rows="4" name="message-input" ></textarea>
                     <input type="hidden" name="user_id" class="user_id" id="user_id" value="<?php  echo $CI->chatsocket->current_user; ?>"> 
                     <input name="ci" class="ci" id="ci" value="" type="hidden" />
                     <div class="input-group-btn message_send_div">
                        <button class="btn btn-success" id="send-message-to-admin">Send</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>