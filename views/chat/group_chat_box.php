<section class="body-middle innerpage">
  <div class="container">
    <div class="row chat-box_fronside pvp_chatbox_main_row msg_bx_sec">
      <div class="col-md-12 col-sm-12 col-xs-12 " align="center">
        <div id="message-container_pvp" class="xwb-message-container pvp_msgbox-message-container">
          <div class="video_box">
            <video autoplay id="vid1" width="400" muted="true" controls></video>
            <video autoplay id="vid2" width="400" class="pvp_commercial_video" muted="true" controls></video>
            <a class="skipvidtrigger">skip</a>
          </div>
          <div class="popup-content-wrapper popup-content-wrapper_customer_fontside">
<!--             <div class="panel panel-info modal-content" align="left">
               <div class="panel-heading modal-header">
                  <h3 class="conversation-name col-sm-8"></h3><h4 class="col-sm-3 call_admin_link"><input type="hidden" id="calladminvalue" value="1"><span style="display:none" id="groupid"></span><button class="btn btn-success" id="call-admin-for-help"></button>
                  </h4>
               </div>
            </div> -->
            <div class="row overflow_manage">
              <div class="col-sm-10">
                <div id="message-inner" class="message-inner"></div>
              </div>
              <div class="col-sm-2 add_adminside_chat banner_on_box"></div>
              <div class="col-sm-12 sponsered_by_logo"></div>
            </div>
            <div class="msg-input input-group col-sm-11 msgbox_at_adminside pvp_player_chat_msg-input">
              <textarea id="message-input" class="form-control message-input message-input-frontend-side" style="resize:none;" rows="4" name="message-input"></textarea>
              <input type="hidden" id="csrf_key" class="csrf_key" name="<?php // echo $CI->security->get_csrf_token_name(); ?>" value="<?php //echo $CI->security->get_csrf_hash(); ?>" />
              <input type="hidden" name="user_id" class="user_id" id="user_id" value="<?php echo $CI->chatsocket->current_user; ?>">
               <input name="ci" class="ci" id="ci" value="" type="hidden" />
              <div class="input-group-btn message_send_div">
                <button class="btn btn-success" id="send-message-to-pvp-player">Send</button>
              </div>
            </div>
            <div class="col-sm-12 call_admin_link">
              <div class="col-sm-6 group_name"><span style="display:none" id="groupid"></span>
                <h3 class="conversation-name"></h3>
              </div>
              <div class="col-sm-6 call_admin_row" align="right">
                <button class="btn btn-success" id="call-admin-for-help"></button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
