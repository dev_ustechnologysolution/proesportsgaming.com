<!-- emojionearea -->
<script src="<?php echo base_url().'xwb_assets/vendor/mervick/emojionearea/dist/emojionearea.js'; ?>"></script>
<script src= "https://player.twitch.tv/js/embed/v1.js"></script>
<script src="<?php echo base_url().'assets/frontend/';?>js/bootstrap.js"></script>
<script src="<?php echo base_url().'assets/frontend/';?>js/bootstrap-formhelpers-selectbox.js"></script>
<script src="<?php echo base_url().'assets/frontend/';?>js/bootstrap-formhelpers-countries.en_US.js"></script>
<script src="<?php echo base_url().'assets/frontend/';?>js/bootstrap-formhelpers-countries.js"></script>
<script src="<?php echo base_url().'assets/frontend/';?>js/bootstrap-formhelpers-states.en_US.js"></script>
<script src="<?php echo base_url().'assets/frontend/';?>js/bootstrap-formhelpers-states.js"></script>
<script src="<?php echo base_url().'assets/frontend/';?>js/jquery.validate.min.js"></script>
<script src="<?php echo site_url();?>/xwb_assets/js/chatsocket.js"></script>
<script src="<?php  echo base_url().'assets/frontend/';?>js/jquery.simple.timer.js"></script>
<script src="<?php  echo base_url().'assets/frontend/';?>js/zoom-image.js"></script>
<script src="<?php echo base_url().'assets/frontend/';?>js/custom.js"></script>
<script src="<?php echo base_url().'assets/frontend/';?>js/cart.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script src="<?php echo base_url().'assets/frontend/';?>js/real_time.js"></script>
<script>
  $(window).scroll(function () {
    if ($(document).scrollTop() == 0) {
      $('body').removeClass('shrink-header');
    } else {
      $('body').addClass('shrink-header');
    }
    if ($(document).scrollTop() >= 600) {
      $('body').addClass('hide-header');
    } else {
      $('body').removeClass('hide-header');
    }
    if ($(document).scrollTop() + $(window).height() <= $(document).height() - 2000) {
      $('body').removeClass('show-header');
    } else {
      $('body').addClass('show-header');
    }
  });
  $( "#loginDiv" ).click(function() {
		$( "#expandDiv" ).slideToggle();
		$(".pending_review-list").hide();
    $( ".mailnotlist" ).hide();
    $( ".frndrqstlist" ).hide();
	});	
  $( ".pending_review-notification" ).click(function() {
		$( ".pending_review-list" ).slideToggle();
    $( ".mailnotlist" ).hide();
    $( ".frndrqstlist" ).hide();
    $("#expandDiv").hide();
	});
  $( ".friend-request-notification" ).click(function() {
    $( ".frndrqstlist" ).slideToggle();
    $( ".pending_review-list" ).hide();
    $( ".mailnotlist" ).hide();
    $("#expandDiv").hide();
  });  
  $( ".mail-notification" ).click(function() {
    $( ".mailnotlist" ).slideToggle();
    $( ".pending_review-list" ).hide();
    $( ".frndrqstlist" ).hide();
    $("#expandDiv").hide();
  });
</script>
<script>
  $.validate({
    modules : 'file'
  });
</script>
<script src="<?php  echo base_url().'assets/frontend/';?>js/dojo.js"></script>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</body>
</html>