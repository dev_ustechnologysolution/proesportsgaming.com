<?php if ($this->session->flashdata('message_err')!='') { ?>
<div id="myModal" class="modal errmdl">
	<div class="modal-content">
		<div class="modal-header">
			<h2 style="color: red;"> <span class="fa fa-exclamation-triangle"></span> Failure</h2>
			<h4><?php echo $this->session->flashdata('message_err');?></h4>
			<div class="close_model_div"><label class="close"><button class="lobby_btn btn">Okay</button></label></div>
		</div>														
	</div>
</div>
<?php } else if ($this->session->flashdata('message_succ') !='') { ?>
<div id="myModal" class="modal">
	<div class="modal-content">
		<div class="modal-header">
			<h4><?php echo $this->session->flashdata('message_succ'); ?></h4>
			<div class="close_model_div"><label class="close"><button class="lobby_btn btn">Okay</button></label></div>
		</div>														
	</div>
</div>
<?php } else if ($this->session->userdata('message_succ') != '') { ?>
<div id="myModal" class="modal">
	<div class="modal-content">
		<div class="modal-header">
			<h4><?php echo $this->session->userdata('message_succ');?></h4>
			<div class="close_model_div"><label class="close"><button class="lobby_btn btn">Okay</button></label></div>
		</div>														
	</div>
</div>
<?php } else if ($this->session->userdata('message_err') !='') { ?>
<div id="myModal" class="modal errmdl">
	<div class="modal-content">
		<div class="modal-header">
			<h2 style="color: red;"> <span class="fa fa-exclamation-triangle"></span> Failure</h2>
			<h4><?php echo $this->session->userdata('message_err');?></h4>
			<div class="close_model_div"><label class="close"><button class="lobby_btn btn">Okay</button></label></div>
		</div>														
	</div>
</div>
<?php } else if ($this->session->flashdata('message_err_1')!='') { 
$message_err_1 = $this->session->flashdata('message_err_1'); $message_err_exp_1 = explode('_',$message_err_1); ?>
<div id="myModal" class="modal">
	<div class="modal-content">
		<div class="modal-header">
			<span class="close">&times;</span>	
			<h4><?php echo @$_SESSION['message_err'];?></h4>
		</div>
		<div class="modal-body">								
			<h4>Please Choose another Option</h4>
			<?php if($message_err_exp_1[0] == 'won'){ ?>
			<div class="game-action1"><a href="<?php echo base_url().'game/gameurlEdit/'.$message_err_exp_1[1].'/lose/'; ?>" class="game-act"> Lose </a> <a href="<?php echo base_url().'game/gameurlEdit/'.$message_err_exp_1[1].'/admin/'; ?>" class="game-act"> Admin </a></div>
			<?php } else { ?>
			<div class="game-action1"><a href="<?php echo base_url().'game/gameurlEdit/'.$message_err_exp_1[1].'/won/'; ?>" class="game-act"> Won </a> <a href="<?php echo base_url().'game/gameurlEdit/'.$message_err_exp_1[1].'/admin/'; ?>" class="game-act"> Admin </a></div>
			<?php } ?>
		</div>
	</div>
</div>
<?php } ?>

<?php 
	$this->session->set_flashdata('message_err','');
	$this->session->set_flashdata('message_succ','');
	$this->session->set_userdata('message_succ','');
	$this->session->set_userdata('message_err','');
	$this->session->set_flashdata('message_succ','');
	$this->session->set_flashdata('message_err','');
 ?>