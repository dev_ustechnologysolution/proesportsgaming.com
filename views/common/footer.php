<div class="modal custmodal commonmodal" style="display: none;">
    <div class="modal-content">
       <div class="modal-header">
          <span class="close">&times;</span>
          <p class="modal_h1_msg text-left"></p>
          <hr>
          <div class="row modal_data">
          </div>
       </div>
    </div>
 </div>
<footer class="modal-footer">
   <div class="container">
      <div class="row">
         <div class="col-lg-4 col-sm-6 col-md-4">
            <img src="<?php
               echo base_url() . 'assets/frontend/';
               ?>images/logoo.png" alt="logo">
            <p><?php
               echo $this->footer_data[0]['content'];
               ?></p>
            <!--DATA IS coming from my controller-->
         </div>
         <div class="col-lg-2 col-sm-6 col-md-3">
            <h3>USEFUL LINKS</h3>
            <ul>
               <?php
                  foreach ($this->footer_menu as $value) {
                  ?>
               <li><a href="<?php
                  echo base_url() . $value["link"];
                  ?>"><?php
                  echo $value['title'];
                  ?></a></li>
               <?php
                  }
                  ?>
            </ul>
         </div>
         <div class="col-lg-2 col-sm-6 col-md-2">
            <h3>PLATFORMS</h3>
            <ul>
               <li><a href="<?php
                  echo base_url();
                  ?>">PLAY STATION 4</a></li>
               <li><a href="<?php
                  echo base_url();
                  ?>">XBOX 1</a></li>
               <li><a href="<?php
                  echo base_url();
                  ?>">PC</a></li>
            </ul>
         </div>
         <div class="col-lg-2 col-sm-6 col-md-2">
            <?php
               foreach ($this->footer_cs as $value) {
                $cs_status = $value['status'];
              }
              if ($cs_status == 'active') { ?>
            <h3>Customer Service</h3>
            <ul>
               <li>
                  <h5>
                     <?php
                        $CI =& get_instance();
                        $result = $CI->chatsocket->getAdminConversation();
                        if ($result['status'] == true) {
                            $conversation = $result['users_conversation'];
                            
                            foreach ($conversation as $key => $value) {
                                
                                $user_to = $value['user_to'];
                                
                                $offline = 'offline';
                                
                                $cn_id = $value['cn_id'];
                                
                            }
                            
                            if (isset($_SESSION['user_id']) && isset($_SESSION['user_name'])) {
                                
                        ?>
                     <a class="link_to_open_chat" onclick='OpenPopupCenter("<?php
                        echo base_url();
                        ?>chat/chat_box/?user_to_from_customer=<?php
                        echo $user_to;
                        ?>")'>OPEN CHAT</a>
                     <?php
                        }
                        
                        else {
                            
                        ?>
                     <a href="<?php
                        echo base_url();
                        ?>login" class="link_to_open_chat">OPEN CHAT</a>
                     <?php
                        }
                        
                        }
                        
                        else {
                        
                        $list = $result['error'];
                        
                        }
                        
                        ?>
                  </h5>
               </li>
            </ul>
            <?php
               }
               
               else {
                   
                   echo " ";
                   
               }
               
               ?>
         </div>
         <div class="col-lg-2 col-sm-6 col-md-3 seclast">
            <h3>WE ARE SOCIAL</h3>
            <ul class="social-sec">
               <?php
                  foreach ($this->footer_link as $value) {
                  ?>
               <li><a href="<?php
                  echo $value['link'];
                  ?>" target="_blank"><i class="<?php
                  echo $value['class'];
                  ?>" aria-hidden="true" ></i></a></li>
               <?php
                  }
                  ?>
            </ul>
            <div class="xtraLogo">
               <?php
                  foreach ($this->footer_logo as $value) {
                  ?>
               <a href="#"><img src="<?php
                  echo base_url() . 'upload/logo/' . $value['image'];
                  ?>" alt=""></a>
               <?php
                  }
                  ?>
            </div>
         </div>
      </div>
   </div>
   <div class="footer-panel">
      <div class="container">
         <div class="row" style="text-align:center;">
            <p>Pro Esports Gaming would like to give this to the world where dreams can be a place of reality.</p>
         </div>
      </div>
   </div>
  <?php
    $CI =& get_instance();
    if ($CI->chatsocket->current_user != null && $CI->chatsocket->current_user != '') {
      $socketUser = $CI->chatsocket->current_user;
    } else {
      $socketUser = 0;
    }
  ?>
   <!-- jquery -->
   <?php $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
      ?>
   <script src="<?php
      echo $protocol . addhttp($CI->chatsocket->domain_name . ':' . $CI->chatsocket->socket_port . '/socket.io/socket.io.js');
      ?>"></script> 
   <!-- PrettyPhoto -->
   <link rel="stylesheet" href="<?php
      echo base_url('xwb_assets/js/prettyPhoto-3.1.6/css/prettyPhoto.css');
      ?>" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
   <script src="<?php
      echo base_url('xwb_assets/js/prettyPhoto-3.1.6/js/jquery.prettyPhoto.js');
      ?>" type="text/javascript" charset="utf-8"></script>
   <!-- Select 2 -->
   <link rel="stylesheet" href="<?php echo base_url('xwb_assets/js/select2-4.0.3/dist/css/select2.min.css'); ?>" type="text/css" charset="utf-8" />
   <script src="<?php echo base_url('xwb_assets/js/select2-4.0.3/dist/js/select2.full.js'); ?>" type="text/javascript"></script>
   <!-- Bootbox -->
   <script type="text/javascript" src="<?php echo base_url('xwb_assets/js/bootbox.min.js'); ?>"></script>
   <script type="text/javascript">
      if(typeof io != 'undefined'){
        var socket = io.connect( "<?php
          echo addhttp($CI->chatsocket->domain_name . ':' . $CI->chatsocket->socket_port); ?>",{secure: true}); //connect to socket
      }
      var varSCPath = "<?php echo base_url('xwb_assets'); ?>";
      var socketUser = "<?php echo $socketUser; ?>";
      window.formKey = "<?php echo csFormKey(); ?>";
      if(typeof socket != 'undefined'){
        socket.emit( "socket id", { "user": socketUser } ); // pass the user identity to node
      }
    </script>
   <script language="javascript" type="text/javascript">
    function OpenPopupCenter(pageURL) {
      var targetWin = window.open(pageURL);
      targetWin.location = pageURL;
    }
      $(function () {
        $('#myCarousel').carousel({
            interval:2000,
            pause: "false"
        });
        
        $('#playButton').click(function () {
          // function playbanner(id){
            $('#myCarousel').carousel('cycle');
            // var id = $('.playButton').data('id');
            // console.log(id);
            $("#playButton").removeClass('d-inline').addClass('none');
            $("#pauseButton").removeClass('none').addClass('d-inline');
            // $("#pauseButton").removeClass('btnactive');
            // $("#playButton").addClass('btnactive');
        });
      // }
        $('#pauseButton').click(function () {
          // function pausebanner(id){

        
          // var id = $('.playButton').data('id');
          // console.log(id);
            $('#myCarousel').carousel('pause');
            $("#pauseButton").removeClass('d-inline').addClass('none');
            $("#playButton").removeClass('none').addClass('d-inline');
            // $("#pauseButton").addClass('btnactive');
            // $("#playButton").removeClass('btnactive');
          // }
        });
      }); 

      
   </script>
   <!-- Chat Socket Script -->
   
</footer>