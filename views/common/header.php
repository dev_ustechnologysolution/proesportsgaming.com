<!DOCTYPE html>
<html lang="en">
  <head>
  	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-132926196-1');
	</script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
    <meta https-equiv="refresh" content="86400;url=<?php echo base_url(); ?>/login/logout/" />
	<title>PRO ESPORTS GAMING</title>

    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url() ?>assets/frontend/images/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url() ?>assets/frontend/images/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url() ?>assets/frontend/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url() ?>assets/frontend/images/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>assets/frontend/images/favicon/favicon-16x16.png">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script type="text/javascript">base_url='<?php echo base_url(); ?>'</script>
  	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/frontend/';?>css/bootstrap.css">
 	<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/chat.css">
	<link href="https://fonts.googleapis.com/css?family=Squada+One" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<link rel="stylesheet" href="<?php echo base_url().'xwb_assets/vendor/mervick/emojionearea/dist/emojionearea.css'; ?>" type="text/css" />

	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/frontend/';?>css/set1.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/frontend/';?>css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/frontend/';?>css/custom.css">
	<script src="<?php echo base_url().'assets/frontend/js/'; ?>countdown.js" type="text/javascript"></script>
	<script src="<?php echo base_url().'assets/frontend/';?>js/jquery-ui.min.js"></script>
  	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-132926196-1"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	
</head>
  	<body>	
    	<div class="navbar-wrapper header-wrapper">
      		<div class="container">
        		<nav class="navbar navbar-inverse navbar-static-top">
          			<div class="container">
            			<div class="navbar-header col-sm-6">
              				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
              				<a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url().'assets/frontend/';?>images/logo.png" alt="logo"></a>
						</div>
						<div class="login-sec col-sm-6">
						<?php 
						$cartitems = (isset($_SESSION['cart_item_list']) && !empty($_SESSION['cart_item_list'])) ? count($_SESSION['cart_item_list']) : 0;
						$whishlistitems = (isset($_SESSION['whishlist_item_list']) && !empty($_SESSION['whishlist_item_list'])) ? count($_SESSION['whishlist_item_list']) : 0;
						$shopping_icon = '<a href="'.base_url().'store/cart" class="my_cart_basket_icon notification" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="View <br> Cart">
				                <i class="fa fa-shopping-cart"></i><span class="item_count header_pending_review header_pos_abs">'.$cartitems.'</span>
				        </a>';
				        $shopping_icon .= '<a href="'.base_url().'store/whishlist" class="my_cart_basket_icon notification" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="View <br> Whishlist">
				                <i class="fa fa-heart"></i><span class="item_count header_pending_review header_pos_abs">'.$whishlistitems.'</span>
				        </a>';
				        ?>
						<?php if ($this->session->userdata('user_id')=='') { ?>
							<div class="col-sm-10 text-right padd0">
								<ul class="befor-login">
	                    			<li><a href="<?php echo base_url().'login' ?>">login</a></li>
									<li><a href="<?php echo base_url().'signup' ?>">Signup</a></li>
								</ul>
							</div>
						<?php
						echo '<div class="col-sm-2 text-right login_re_div">'.$shopping_icon.'</div>';
						} else { 
							$num1 = $this->session->userdata('total_balance');
							$num1 = number_format((float)$num1, 2, '.', '');
							echo '<div style="color: white;font-size: 20px;" class="notification header_available_balance col-sm-6 text-right padd0"> Available balance : <span class="tot-balance">$'.  $num1. '</span></div>';
							$pending_review = $this->General_model->pending_game_review();
							$pendingrequest_friendlist = count($this->General_model->pendingrequest_friendlist($_SESSION['user_id']));
							$pendingrequest_friendlist_count = count($pendingrequest_friendlist);
							$pending_unread_mail_count = count($this->General_model->get_all_unread_mails($_SESSION['user_id']));
							$pending_mail_count=$pending_unread_mail_count+$pendingrequest_friendlist_count;
							$online_friendlist_count = count($this->General_model->online_friendlist($_SESSION['user_id']));
				 			?>
				 			<div class="col-sm-6 text-right">
				 				<?php echo $shopping_icon; ?>
								<a class="notification mail-notification" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="View <br> Mails">
									<i class="fa fa-envelope" aria-hidden="true"></i>
									<span class="header_pending_review header_pos_abs"><?php echo $pending_mail_count; ?></span>
								</a>				
								<a class="notification pending_review-notification" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="View <br> Notifications">
									<i class="fa fa-bell" aria-hidden="true"></i>
									<span class="header_pending_review header_pos_abs"><?php echo count($pending_review); ?></span>
								</a>
								<a class="notification friend-request-notification" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="View <br> Notifications"> 
									<i class="fa fa-user" aria-hidden="true"></i>
									<span class="header_pending_review header_pos_abs"><?php print_r($online_friendlist_count); ?></span>
								</a>
								<ul class="noticlist pending_review-list">
									<li><h5>Notifications</h5></li>
									<li class="store_li"><a href="<?php echo site_url().'Store';?>"> <img src="<?php echo base_url().'assets/frontend/';?>images/store_icon.png" height="15" width="15" alt="store">&nbsp; Store</a></li>
									<li><h5>Pending Result</h5></li>
									<?php if(!empty($pending_review)) { ?>
										<?php foreach($pending_review as $pend_review) { ?>
										<li>
											<a href="<?php echo site_url().'dashboard'; ?>"><span class="noti-ico"><img src="<?php echo base_url().'assets/frontend/';?>images/noimg1.png" alt="game"></span> <p><?php  echo $pend_review['game_name']; ?></p></a>
										</li>
										<?php } ?>
									<?php } else {
										echo '<li><a><p>No Pending Result <span style="color: #d6d1d1"></span></p></a></li>';
									} ?>					
								</ul>
								<ul class="noticlist frndrqstlist">
									<li><h5>Online Friends</h5></li>
									<?php
									$online_friends = $this->General_model->online_friendlist($_SESSION['user_id']);
									if(!empty($online_friends)) { 
										foreach($online_friends as $olfrnds) { ?>
										<li>
											<a href="<?php echo site_url().'friend/'; ?>"> <p> 
												<?php if (!empty($olfrnds['image']) || $olfrnds['image'] != '') { ?>
													<img src="<?php echo base_url().'upload/profile_img/'.$olfrnds['image']; ?>" class="online_friends_profile_pic"> <?php } else {?>
													<img src="<?php echo base_url().'upload/profile_img/profile_placeholder.jpg'; ?>" class="online_friends_profile_pic"> <?php } ?> <span style="color: #d6d1d1"> <?php  echo $olfrnds['name']; ?></span></p></a>							 
										</li>
										<?php } 
									} else {
										echo '<li><a><p>No Online Friends <span style="color: #d6d1d1"></span></p></a></li>';
									}
									?>					
								</ul>
								<ul class="noticlist mailnotlist">
									<li><h5>Unread mail</h5></li>
									<?php
									$pending_unread_mail = $this->General_model->get_all_unread_mails($_SESSION['user_id']);
									if(!empty($pending_unread_mail)) { ?>
										<?php foreach($pending_unread_mail as $pend_review) { ?>
										<li>
											<a href="<?php echo site_url().'mail/'; ?>"> <p>New Mail from <span style="color: #d6d1d1"> <?php  echo $pend_review['name']; ?></span></p></a>
										</li>
										<?php } ?>
									<?php } else {
										echo '<li><a><p>No mails <span style="color: #d6d1d1"></span></p></a></li>';
									} ?>
									<li><h5>Pending Friend Request</h5></li>
									<?php
									if(!empty($pendingrequest_friendlist)) { ?>
										<?php foreach($pendingrequest_friendlist as $pend_review) { ?>
										<li>
											<a href="<?php echo site_url().'friend/pendingrequest_friendlist/'; ?>"> <p>New Friend Request from <span style="color: #d6d1d1"> <?php  echo $pend_review['name']; ?></span></p></a>
										</li>
										<?php } ?>
									<?php } else {
										echo '<li><a><p>No Pending Request <span style="color: #d6d1d1"></span></p></a></li>';
									}
									?>
								</ul>
								<a id="loginDiv" class="loginexp">
									<span class="userImg">
										<img src="<?php echo base_url().'upload/profile_img/'. $this->session->userdata('user_image');?>" alt="logo">
									</span>
									<span><i class="fa fa-caret-down" aria-hidden="true"></i></span>
								</a>
								<div id="expandDiv">
									<ul>
										<li><a href="<?php echo base_url().'user/myprofile'; ?>">My Profile</a></li>
										<li><a href="<?php echo base_url().'friend'; ?>">Dashboard</a></li>
										<li><a href="<?php echo base_url('game/newgame') ?>">Place Challenge</a></li>
										<li><a href="<?php echo base_url('dashboard') ?>">My Challenges</a></li>
										<li><a href="<?php echo base_url('store/cart') ?>">My Cart</a></li>
										<li><a href="<?php echo base_url('friend') ?>">Friends</a></li>
										<li><a href="<?php echo base_url('mail') ?>">Mail</a></li>
										<li><a href="<?php echo base_url('user/profile') ?>">Settings</a></li>
										<li><a href="<?php echo base_url('login/logout') ?>">Logout</a></li>
									</ul>
								</div>
							</div>
							<?php } ?>
						</div>							
					</div>
				</nav>
			</div>
		</div> 
		<!-- <div class="black archita"></div> -->
		<?php
		$data = $this->router->fetch_class();
		$banner_interval = 15000;
		$method = $this->router->fetch_method();
		$lobby_status = $this->General_model->view_single_row('site_settings','option_title','live_lobbyy_banner');
		$banner_query  = $this->db->query("SELECT GROUP_CONCAT(lg.stream_channel) as channel,gb.lobby_id FROM `lobby_group` as lg inner join group_bet as gb on gb.id = lg.group_bet_id inner join lobby as l on l.id=gb.lobby_id and l.is_game_active = 1 where lg.is_banner = 1 and lg.stream_status = 'enable' and gb.bet_status = 1 group by(gb.lobby_id) order by l.lobby_order asc");
		$stream_banner_data = $banner_query->result();
		$class = '';			
		if ($method == 'view_lobby') { $class = 'mt20'; }
		if(($lobby_status['option_value'] == 'on') && (count($stream_banner_data) > 0)) {
			if($data == "chat" || $data == "Home" || $data == "challenge_mail" || $data == "user" || $data == "page"|| $data == "Store" || $data == "sendmoney" || $data == "points" || $data == "mail" || $data == "game" || $data == "friend" || $data == "dashboard" || $data == "Ranking" || $data == "Matches" || $data == "live_lobby_list" || $data == "Challenge_mail" || $data == 'Streamsetting' || $data == 'trade' || $data == 'Subscribeuser' || $data == 'Membership' || $data == 'membership') {
				if($method != 'gameDetails' && $method != 'forgot_password') { ?>
					<div id="myCarousel" class="carousel slide mb30" data-ride="carousel" >
						<ol class="carousel-indicators">
							<?php 
							$j = 0;
							foreach($stream_banner_data as $k => $v1){  ?>
								<li data-slide-to="<?php echo $j;?>" <?php if($j==0) echo 'class="active"'; ?> data-target="#myCarousel" ></li> 
							<?php $j++; } ?>

						</ol>
						<a id="playButton" type="button" title="Play Banner" class="banner_lock_btn b0 none pull-right">
				          <img width="30" src="<?php echo base_url().'assets/frontend/images/banner_locked.png'; ?>">
				       </a>
				       <a id="pauseButton" type="button" title="Pause Banner" class="banner_lock_btn b0 pull-right">
				          <img width="30" src="<?php echo base_url().'assets/frontend/images/banner_unlocked.png'; ?>">
				       </a>	
						<div class="carousel-inner" role="listbox">
						<?php foreach($stream_banner_data as $t => $v) {
							 $channel = $v->channel;
							 $arr = explode(',',$channel);
							if($arr[1] == '') {
								$class1 = 'col-sm-12';
							} else {
								$class1 = 'col-sm-6';
							} 
							$watchlinklabel = 'Live Stream';
							$single_lobby_data = $this->General_model->lobby_details($v->lobby_id);
							$is_event = $single_lobby_data[0]['is_event'];
							$event_image = '';
							if ($is_event == 1) {
								$watchlinklabel = 'Live Event';
								$event_image = $single_lobby_data[0]['event_image'];
								if($event_image != ''){
									$class1 = 'col-sm-12';
								}
							}
							$is_spectator = $single_lobby_data[0]['is_spectator'];
							?>
							<div class="item <?php if($t==0) echo 'active'; ?>" data-interval="<?php echo $banner_interval;?>">
								<div class="row">   
									<div class="<?php echo $class1; ?>">
										<a>
											<?php if($event_image != '') { ?>
												<img style="width: 100%;margin-bottom: 26px;" src="<?php echo base_url().'upload/banner/'.$event_image;?>">
											<?php } else { ?>
											<iframe src="https://player.twitch.tv/?channel=<?php echo $arr[0];?>" height="500" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
											<?php } ?>

										</a>
										<div class="col-sm-12">
											<div class="text-center" align="center">
												<a href="<?php echo base_url('livelobby/watchLobby/'.$v->lobby_id);?>" class="lobby_slider_button text-center animated-box orange"><?php echo $watchlinklabel; ?></a>
												<?php if($watchlinklabel == 'Live Event' || $is_spectator == 1) { ?>
													<a href="<?php echo base_url('livelobby/watchLobby/'.$v->lobby_id.'?is_gift=1');?>" class="lobby_slider_button text-center animated-box orange">Gift Ticket</a>
												<?php } if($is_spectator == 1){ ?>
													<!-- <a href="<?php echo base_url('livelobby/watchLobby/'.$v->lobby_id.'?is_spectator=1');?>" class="lobby_slider_button text-center animated-box orange">Spectator Ticket</a> -->
												<?php } ?>
											</div>
										</div>
									</div>
									<?php ?>
									<?php if($arr[1] != '' && $is_event == 0){ ?>
									<div class="<?php echo $class1; ?>">
										<a>
											<iframe src="https://player.twitch.tv/?channel=<?php echo $arr[1];?>" height="500" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
										</a>
										<div class="col-sm-12">
											<div class="text-center" align="center">
												<a href="<?php echo base_url('livelobby/watchLobby/'.$v->lobby_id);?>" class="lobby_slider_button text-center animated-box orange"><?php echo $watchlinklabel; ?></a>
												<?php if($watchlinklabel == 'Live Event' || $is_spectator == 1) { ?>
													<a href="<?php echo base_url('livelobby/watchLobby/'.$v->lobby_id.'?is_gift=1');?>" class="lobby_slider_button text-center animated-box orange">Gift Ticket</a>
												<?php } if($is_spectator == 1){ ?>
													<!-- <a href="<?php // echo base_url('livelobby/watchLobby/'.$v->lobby_id.'?is_spectator=1');?>" class="lobby_slider_button text-center animated-box orange">Spectator Ticket</a> -->
												<?php } ?>
											</div>
										</div>
									</div>
								<?php } ?>
								</div>
							</div>
						<?php } ?>									
						</div>
					</div>
				<?php } else {
					$class = 'mt20';
				}
			}  else {
				$class = 'mt20';
			}
		} else  {
			$video_banner = $this->General_model->view_data('videos',array('is_home'=>'1'));	
			if($data == "Home" || $data == "chat" || $data == "challenge_mail" || $data == "user" || $data == "page"|| $data == "Store" || $data == "sendmoney" || $data == "points" || $data == "mail" || $data == "game" || $data == "friend" || $data == "dashboard" || $data == "Ranking" || $data == "live_lobby_list" || $data == "Matches" || $data == "Challenge_mail" || $data == 'Streamsetting' || $data == 'trade' || $data == 'Subscribeuser' || $data == 'Membership' || $data == 'membership'){
				if($method != 'gameDetails' && $method != 'forgot_password'){
					$banner_data =$this->General_model->banner_order();
				}
			} else {
				$banner_data =array();
			}
			
			if(empty($banner_data)){
				$class = 'mt20';
			}

			if(!empty($banner_data)) { ?>
				<div id="myCarousel" class="carousel slide" data-ride="carousel" >
					<ol class="carousel-indicators">
					<?php 
					$i=0; 
					foreach ($banner_data as $value) { ?>
						<li data-target="#myCarousel" data-slide-to="<?php echo $i;?>" <?php if($i==0) echo 'class="active"'; ?>></li>
					<?php $i++; }
					if(count($video_banner) > 0){
						foreach ($video_banner as $value1) { ?>
							<li data-target="#myCarousel" data-slide-to="<?php echo $i;?>"></li>
							<?php $i++;
						}
					}
					?>
					</ol>
					<a id="playButton" type="button" title="Play Banner" class="banner_lock_btn b20 none pull-right">
				        <img width="30" src="<?php echo base_url().'assets/frontend/images/banner_locked.png'; ?>">
			        </a>
			        <a id="pauseButton" type="button" title="Pause Banner" class="banner_lock_btn b20 pull-right">
			          <img width="30" src="<?php echo base_url().'assets/frontend/images/banner_unlocked.png'; ?>">
			        </a>	
					<div class="carousel-inner bs_banner_customize" role="listbox">
						<?php $i=0; foreach ($banner_data as $value) { ?>
						<?php if ($value['banner_video'] != '' && $value['banner_video_duration'] != '') { ?>
							<div  data-interval="<?php echo (isset($value['banner_video_duration'])) ? $value['banner_video_duration'] : '15';?>000" class="item <?php if($i==0) echo 'active'; ?>" id="slider_backvideo">
							<video id="slider_video" width="100%" src="<?php echo base_url().'upload/banner/'.$value['banner_video']?>" muted="muted" loop controls></video>
							<span style="display: none;" id="banner_video_duration"><?php echo $value['banner_video_duration']; ?></span>
						<?php } else if ($value['banner_youtube_link'] != '') { ?>
							<div data-interval="<?php echo (isset($value['banner_youtube_link_duration'])) ? $value['banner_youtube_link_duration'] : '15';?>000" class="item <?php if($i==0) echo 'active'; ?>" id="slider_backvideo">
								<?php $iframe = $this->General_model->getYoutubeEmebedUrl($store_commercial->youtube_link,0);
								echo $iframe['embeded_iframe'];?>
						<?php } else { ?>
							<div class="item <?php if($i==0) echo 'active'; ?>" data-interval="<?php echo $banner_interval; ?>">
								<img src="<?php echo base_url().'upload/banner/'.$value['banner_image'];?>" alt="">
							<?php } ?>
							<div class="container">
								<div class="carousel-caption">
									<h1><?php echo $value['banner_name']; ?></h1>
									<p><?php echo $value['banner_content']; ?></p>
								</div>
							</div>
							<div class="col-sm-12" style="height: 78px;">
								<div class="mt20" align="center">
									<?php if($value['button1_title'] != '' && $value['button1_link'] != '') { ?>
										<a  target="_blank" href="<?php echo $value['button1_link'];?>" class="lobby_slider_button text-center animated-box orange"><?php echo $value['button1_title']; ?></a>
									<?php } 
									if($value['button2_title'] != '' && $value['button2_link'] != '') { ?>
										<a  target="_blank" href="<?php echo $value['button2_link'];?>" class="lobby_slider_button text-center animated-box orange"><?php echo $value['button2_title']; ?></a>
									<?php }  ?>	
								</div>
							</div>
						</div>
						<?php $i++; } 
							foreach ($video_banner as $key => $value1) { ?>
								<div class="item" data-interval="<?php echo $banner_interval;?>">
								<div class="row">   
									<div class="col-sm-12">
										<a>
											<iframe src="https://player.twitch.tv/?channel=<?php echo $value1['twitch_username'];?>" height="500" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
										</a>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div> 
		</div>             
	  <?php } } ?>
<div class="animated-box ">
	<div id="navbar" class="navbar-collapse  collapse menu_div <?php echo $class;?>">		
		<ul class="nav navbar-nav">
			<?php foreach($this->header_menu as $active_menu ) { ?>
			<li <?php if($this->uri->segment(1)==$active_menu['title']) echo 'class="active"';?>><a href="<?php echo base_url().ucfirst($active_menu['link']); ?>"><?php echo $active_menu['title']; ?></a></li>
			<?php } ?>			
		</ul>
	</div>
</div> 	
<style>
.carousel-control {
	left: -12px;
    height: 40px;
	width: 40px;
    background: none repeat scroll 0 0 #222222;
    border: 4px solid #FFFFFF;
    border-radius: 23px 23px 23px 23px;
    margin-top: 90px;
}
.carousel-control.right {
	right: -12px;
}
.margin_top {
	margin-top:100px;
}
.m40 {
	margin: 25px 0px;
}
.banner_lock_btn {
	cursor: pointer;
    margin-right: 20px;
    position: absolute;
    right: 0;
    z-index: 9;
}
.b0 {
	bottom: 0px;
}
.b20 {
	bottom: 20px;
}
</style>
<script>
$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if (scroll >= 100) {
        $(".header-wrapper").addClass("position_change");
    } else {
    	$(".header-wrapper").removeClass("position_change");
    }
});
$(document).ready(function() {	
    $('.select2').select2();
    $('.select21').select2();
});
function CommaFormatted(amount) {
	var delimiter = ","; // replace comma if desired
	var a = amount.split('.',2)
	var d = a[1];
	var i = parseInt(a[0]);
	if(isNaN(i)) { return ''; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	var n = new String(i);
	var a = [];
	while(n.length > 3) {
		var nn = n.substr(n.length-3);
		a.unshift(nn);
		n = n.substr(0,n.length-3);
	}
	if(n.length > 0) { a.unshift(n); }
	n = a.join(delimiter);
	if(d.length < 1) { amount = n; }
	else { amount = n + '.' + d; }
	amount = minus + amount;
	return amount;
}
</script>