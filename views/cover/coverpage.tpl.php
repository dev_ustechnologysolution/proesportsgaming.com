<?php 
    $this->load->view('common/cover-header');
 ?>
<section class="cover-page">

    <div class="container">

        <div class="row">

            <div class="login-signBlock">
                <h2>Enter Password</h2>
                <form role="form" class="signup-form has-validation-callback" action="<?php echo base_url();?>Home/cover_page" method="POST">
                    <!-- signup-form has-validation-callback -->
                    <div class="form-group">
                        <h5 style="color: red; text-align: center;"><?php echo $_SESSION['cover_page_password_error']; unset($_SESSION['cover_page_password_error']);?></h5>
                        <div class="input-group col-lg-12 col-sm-12 col-md-12">

                            <input type="password" class="form-control" name="password">

                        </div>

                    </div>

                    <div class="btn-group">

                        <input type="submit" value="Procced to Developing site" class="btn-submit">

                    </div>
                    <div class="form-group">
                        <p><span><a href="https://proesportsgaming.com/"> <i class="fa fa-link"></i> Go to Original site</a></span></p>
                    </div>
                </form>
            </div>
          
          </div>    
    
    </div>

</section>
