<div class="adm-dashbord">
	<div class="container padd0">
        <?php $this->load->view('dashboard/profile_sidebar.tpl.php'); ?>
        <div class="col-lg-10">
        	<div class="col-lg-11">		
			<div class="profile_top row">
                  <div class="col-sm-6 text-right">
                     <a class="profile_top_btn orange" href="<?php echo base_url('user/myprofile'); ?>">My Profile</a>
                  </div>
                  <div class="col-sm-6">
                     <a class="profile_top_btn black" href="<?php echo base_url('friend'); ?>">Dashboard</a>
                  </div>
               </div>
				<?php $this->load->view('/common/flash_msg_show.tpl.php'); ?>
            	<div>
					<div class="row bidhottest mygamelist">
						<h2>Created Live Lobby Challenges</h2>
						<div class="bidhottest hottestgame playstore_game_list_front lobby_challenges_my_challenges_tab">
							<?php if (count($created_lobbys)>0) { ?>
							<ul>
				            	<?php
					            echo $this->session->flashdata('err_lobby');
					            foreach($created_lobbys as $key => $val) {
					            	$fan_tag = $this->General_model->get_my_fan_tag($val['lobby_id'],$val['user_id']);
					            	$lobby_bet = $this->General_model->lobby_bet_detail($val['lobby_id']);
					            	$everyone_readup = 'everyone_readup = "no"';
					            	if ($lobby_bet[0]['is_full_play'] == 'yes') {
					            		$everyone_readup = 'everyone_readup = "yes"';
					            	} ?>
					                <li id="lobbyliusr_<?php echo $val['lobby_id']; ?>">
							            <div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
							               <img src="<?php echo base_url().'upload/game/'. $val['game_image'];?>" alt="">
							            </div>
							            <div class="col-lg-3 col-sm-6 col-md-3 bid-info">
							               <h3><?php echo $val['game_name']; ?></h3>
							               <p><?php echo $val['sub_game_name']; ?></p>
							               <span class="postby">Tag: <?php echo $val['device_id']; ?></span> 
							               <!--<span class="postby">By <?php echo $val['name']; ?></span>-->
							            </div>
							            <div class="col-lg-2 col-sm-3 col-md-2 uplod-day">
							               <?php if($val['game_timer'] != '' && $val['game_timer'] != "0000-00-00 00:00:00"){ $diff = strtotime($val['game_timer'])- time(); if(time() < strtotime($val['game_timer'])){ ?>
							               <?php } ?>
							               <h3 class="timer custom_timer" data-seconds-left="<?php echo $diff; ?>"></h3>
							               <?php } ?>
							               <p> Gamesize: <?php echo "--".$val['game_type']."V".$val['game_type']."--";?></p>
							               <p class="best_of">
							               	<?php if ($val['best_of']!=null) {
							                    echo 'Best of '.$val['best_of'];
							                  }
							                  ?></p>
							            </div>
							            <div class="col-lg-2 col-sm-4 col-md-2 divPrice">
							               <div class="game-price premium">
							                  <h3><?php $lobby_bet_price = number_format((float) $val['price'], 2 , '.', ''); echo CURRENCY_SYMBOL.$lobby_bet_price; ?></h3>
							               </div>
							            </div>
							            <div class="col-lg-3 col-sm-5 col-md-3 primum-play play-n-rating">
							            	<?php $self="no";
							            	$lobbylink = 'Live Stream';
							            	if ($val['is_event'] == 1) {
							            		$lobbylink = 'Live Event';
							            	} ?>
							               <a href="<?php echo base_url().'livelobby/watchLobby/'.$val['lobby_id'];?>" class="animated-box"><?php echo $lobbylink; ?></a>
											<a class="leave_lobby_btn leave_lobby_btn_from_dash" lby_crtr="<?php echo ($val['creator'] == $_SESSION['user_id'])?'yes':'no'; ?>" self="<?php echo ($val['creator'] == $_SESSION['user_id'])?'yes':'no'; ?>" lby_cntrlr="<?php echo $val['is_controller'];?>" player_tag_name="<?php echo $fan_tag->fan_tag; ?>" title="" usr_id="<?php echo $_SESSION['user_id']; ?>" lobby_id="<?php echo $val['lobby_id']; ?>" group_bet_id="<?php echo $val['group_bet_id']; ?>" data-original-title="Leave Live Lobby" data-placement="bottom" data-toggle="tooltip" <?php echo ($val['from_waitinglist'] == 'yes') ? 'leave_from_waitinlist="yes"' : '' ; ?> <?php echo $everyone_readup; ?>><i class="fa fa-minus"></i></a>
							            </div>
							        </li>
							        <?php
							    } ?>
						    </ul>
						    <?php } ?>
						</div>
					</div>            		
            		<div class="row bidhottest mygamelist">
						<h2>Joined Live Lobby Challenges</h2>
						<span style="color:red"><?php echo $_SESSION['canceled']; unset($_SESSION['canceled']);?></span>
						<div class="bidhottest hottestgame playstore_game_list_front lobby_challenges_my_challenges_tab">
							<?php if(count($get_my_lobby_list)>0) { ?>
							<ul>
					            <?php
					            echo $this->session->flashdata('err_lobby');
					            foreach($get_my_lobby_list as $key => $val) {
					            	$fan_tag = $this->General_model->get_my_fan_tag($val['lobby_id'],$val['user_id']);
					            	$lobby_bet = $this->General_model->lobby_bet_detail($val['lobby_id']);
					            	$everyone_readup = 'everyone_readup = "no"';
					            	if ($lobby_bet[0]['is_full_play'] == 'yes') {
					            		$everyone_readup = 'everyone_readup = "yes"';
					            	} ?>
					                <li id="lobbyliusr_<?php echo $val['lobby_id']; ?>">
							            <div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
							               <img src="<?php echo base_url().'upload/game/'. $val['game_image'];?>" alt="">
							            </div>
							            <div class="col-lg-3 col-sm-6 col-md-3 bid-info">
							               <h3><?php echo $val['game_name']; ?></h3>
							               <p><?php echo $val['sub_game_name']; ?></p>
							               <span class="postby">Tag: <?php echo $val['device_id']; ?></span> 
							               <!--<span class="postby">By <?php echo $val['name']; ?></span>-->
							            </div>
							            <div class="col-lg-2 col-sm-3 col-md-2 uplod-day">
							               <?php if($val['game_timer'] != '' && $val['game_timer'] != "0000-00-00 00:00:00"){ $diff = strtotime($val['game_timer'])- time(); if(time() < strtotime($val['game_timer'])){ ?>
							               <?php } ?>
							               <h3 class="timer custom_timer" data-seconds-left="<?php echo $diff; ?>"></h3>
							               <?php } ?>
							               <p> Gamesize: <?php echo "--".$val['game_type']."V".$val['game_type']."--";?></p>
							               <p class="best_of">
							               	<?php if ($val['best_of']!=null) {
							                    echo 'Best of '.$val['best_of'];
							                  }
							                  ?></p>
							            </div>
							            <div class="col-lg-2 col-sm-4 col-md-2 divPrice">
							               <div class="game-price premium">
							                  <h3><?php $lobby_bet_price = number_format((float) $val['price'], 2 , '.', ''); echo CURRENCY_SYMBOL.$lobby_bet_price; ?></h3>
							               </div>
							            </div>
							            <div class="col-lg-3 col-sm-5 col-md-3 primum-play play-n-rating">
							            	<?php $self="no";
							            	$lobbylink = 'Live Stream';
							            	if ($val['is_event'] == 1) {
							            		$lobbylink = 'Live Event';
							            	} ?>
							               <a href="<?php echo base_url().'livelobby/watchLobby/'.$val['lobby_id'];?>" class="animated-box"><?php echo $lobbylink; ?></a>
											<a class="leave_lobby_btn leave_lobby_btn_from_dash" lby_crtr="<?php echo ($val['creator'] == $_SESSION['user_id'])?'yes':'no'; ?>" self="<?php echo ($val['creator'] == $_SESSION['user_id'])?'yes':'no'; ?>" lby_cntrlr="<?php echo $val['is_controller'];?>" player_tag_name="<?php echo $fan_tag->fan_tag; ?>" title="" usr_id="<?php echo $_SESSION['user_id']; ?>" lobby_id="<?php echo $val['lobby_id']; ?>" group_bet_id="<?php echo $val['group_bet_id']; ?>" data-original-title="Leave Live Lobby" data-placement="bottom" data-toggle="tooltip" <?php echo ($val['from_waitinglist'] == 'yes') ? 'leave_from_waitinlist="yes"' : '' ; ?> <?php echo $everyone_readup; ?>><i class="fa fa-minus"></i></a>
							            </div>
							        </li>
							        <?php
							    } ?>
						    </ul>
						    <?php } ?>
						</div>
					</div>
            		<div class="row bidhottest mygamelist">
						<h2>Friend Challenges</h2>
						<span style="color:red"><?php echo $_SESSION['canceled']; unset($_SESSION['canceled']);?></span>
						<?php
						if(isset($friend_challange_list) && !empty($friend_challange_list)){ 
							if(count($friend_challange_list)>0){ 
						?>
					   	<?php foreach($friend_challange_list as $friend) { ?>
					   	<ul>
							<li>
								<div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
									<img class="game-image" src="<?php echo base_url().'upload/game/'. $friend['game_image'];?>" alt="">
								</div>
								<div class="col-lg-3 col-sm-6 col-md-3 bid-info">
									<h3 class="game_name"><?php echo $friend['game_name']; ?></h3>
									<p class="sub_game_name_list"><?php echo $friend['sub_game_name']; ?></p>
									<h3 class="tag">Tag: <?php echo $friend['device_id']; ?></h3>
								</div>
								<div class="col-lg-2 col-sm-3 col-md-2 uplod-day">
									<h4><?php //echo time_elapsed_string(strtotime($value['created_at']));?></h4>
									<?php if($friend['game_timer'] != '' && $friend['game_timer'] != "0000-00-00 00:00:00"){ $diff = strtotime($friend['game_timer'])- time();  if(time() < strtotime($friend['game_timer'])){ ?>
									<?php } ?>
									<h3 class="timer custom_timer" data-seconds-left="<?php echo $diff; ?>"></h3>
			                        <?php } ?>
									<h5 class="gamesize">Gamesize: --<?php echo $friend['game_type']; ?>V<?php echo $friend['game_type']; ?>-- </h5>
								</div>
								<div class="col-lg-2 col-sm-4 col-md-2 divPrice">
									<div class="game-price premium">
										<h3><?php echo CURRENCY_SYMBOL.$friend['price']; ?></h3>
									</div>
								</div>
								<div class="col-lg-3 col-sm-4 col-md-2">
									<div class="game-status">
									<div class="primum-play play-n-rating">
										<?php if ($friend['game_password']!='' && $friend['game_password']!=null) { ?>
				                        <a class="button" href="<?php echo base_url().'home/gamePwAuth/'.$friend['id'];?>">PLAY</a>
				                      <?php } else { ?> 
				                          <a class="button" href="<?php echo base_url().'home/gameAuth/'.$friend['id'];?>">PLAY</a>
				                      <?php } ?>
                    					<form action="<?php echo base_url().'dashboard/cancelChallenge/'.$friend['id']?>" class="has-validation-callback">
											<button class="button reject_challenge" type="submit">REJECT</button>
										</form>
				                    </div>	
									</div>
								</div>
							</li>
						</ul>
						<?php } ?>
					</div>
					<?php } else { echo "<h4>No data Found</h4>"; } } ?>
					<div class="row bidhottest mygamelist">
						<h2>Open Challenges</h2>
						<span style="color:red"><?php echo $_SESSION['canceled']; unset($_SESSION['canceled']);?></span>
						<?php
							if(isset($list) && !empty($list)) { 
								if(count($list)>0) { 
						?>
					   	<?php foreach($list as $value) { 
					   	if($this->session->userdata('user_id') == $value['user_id']) { ?>
					   	<ul>
							<li>
								<div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
									<img src="<?php echo base_url().'upload/game/'. $value['game_image'];?>" alt="">
								</div>
								<div class="col-lg-3 col-sm-6 col-md-3 bid-info">
									<h3><?php echo $value['game_name']; ?></h3>
									<p><?php //echo substr($value['description'],0,60).'.....'; ?></p>
								</div>
								<div class="col-lg-2 col-sm-3 col-md-2 uplod-day">
									<h4><?php //echo time_elapsed_string(strtotime($value['created_at']));?></h4>
									<?php if($value['game_timer'] != '' && $value['game_timer'] != "0000-00-00 00:00:00"){ $diff = strtotime($value['game_timer'])- time();  if(time() < strtotime($value['game_timer'])){ ?>
									<?php } ?>
									<h3 class="timer custom_timer" data-seconds-left="<?php echo $diff; ?>"></h3>
			                        <?php } ?>
								</div>
								<div class="col-lg-2 col-sm-4 col-md-2 divPrice">
									<div class="game-price">
										<h3><?php echo CURRENCY_SYMBOL.$value['price']; ?></h3>
									</div>
								</div>
								<div class="col-lg-3 col-sm-4 col-md-2 divPrice">
									<div class="game-status">
										<h4><?php if($value['payment_status']==1) echo 'Waiting For Acceptance'; ?></h4>
										<form action="<?php echo base_url().'dashboard/cancelChallenge/'.$value['id']; ?>">
											<button class="btn btn-primary" type="submit">Cancel Challenge</button>
										</form>
									</div>
								</div>
							</li>
						</ul>
						<?php } } ?>
					</div>
					<?php } else { echo "<h4>No data Found</h4>"; } } ?>
					<?php 
						if(isset($list1) && !empty($list1)) { 
							if(count($list1)>0) { ?>
							<div class="row bidhottest mygamelist">
								<h2>Pending Results</h2>
								<ul>
									<?php 
									$i=0;
									foreach($list1 as $value) { ?>
									<li>
                                        <?php //if($this->session->userdata('user_id')==$value['user_id']) {  ?>

                                       <div class="col-lg-2 col-sm-2 col-md-2 bidimg padd0">
                                           <img src="<?php echo base_url().'upload/game/'. $value['game_image'];?>" alt="">
                                        </div>
                                        <div class="col-lg-2 col-sm-2 col-md-2 bid-info">
                                            <h3><?php echo $value['game_name']; ?></h3>
                                            <h3><a style="color:#ff5000" title="<?php echo $value['description']; ?>">Description</a></h3>
                                            <span class="bid-info"><p><?php echo $value['description']; ?></p></span>
                                        </div>                                            
                                        <div class="col-lg-1 col-sm-1 col-md-1 divPrice">
                                            <div class="game-price">
                                                <h3><?php echo CURRENCY_SYMBOL.$value['price']; ?></h3>
                                            </div>
                                        </div>
										<?php if($video_status[$i]) { ?>
                                            <div class="col-lg-7 col-sm-3 col-md-3 divPrice">
                                                <h5 style="margin-top:37px;"><?php echo 'Waiting for admin review'; ?></h5>
											</div>
                                         <?php } else { ?>
                                             <div class="col-lg-4 col-sm-2 col-md-2 divPrice" style="padding:0;">
												<h5>
													<span style="color: #ff5000;font-weight: bold; font-size: 18px;">
													<?php echo $value['device_id'];  ?> </span> 
													<span style="color: #ff0000;font-weight: bold; font-size: 18px;">vs 
													</span>
													<span style="color:#ffffff; font-weight: bold; font-size: 18px;">
														<?php echo $value['accepter_device_id']; ?>
														</span>
														</h5>

                                                <h5 style="margin-top: 10px;"><?php if($value['status']==2) echo 'Challenge Accepted'; ?></h5>
                                                 <div class="open_pvp_chat">

                                                <?php
                                                	$game_id = $value['id'];
                                                	$game_name = $value['game_name'];
                                                	$accepter_id=$value['accepter_id'];
                                                	$challenger_id=$value['challenger_id'];
							                        $CI =& get_instance();
							                        $result = $CI->chatsocket->getOnlineAdminAgentConversation();
							                        if ($result['status'] == true) {
							                            $conversation = $result['users_conversation'];
							                            foreach ($conversation as $key => $value) {
							                                $agent_online = $value['user_to'];
							                            }
							                        }
						                        
							                        $result = $CI->chatsocket->getPvpConversation($accepter_id,$challenger_id);
							                        if ($result['status'] == true) 
							                        {
							                            $conversation = $result['users_conversation'];
							                            foreach ($conversation as $key => $value) {
							                       $grp_result = $CI->chatsocket->getPvpConversationGrouplist($game_id);
					                            	$grp_users = implode("|",$grp_result);
					                                $user_to = $value['user_to'];
					                                $offline = 'offline';

							                                ?>
							                                	<a onclick='OpenPopupCenter("<?php echo base_url(); ?>chat/group_chat_box/?pvp_userto=<?php echo $user_to; ?>&accepterid=<?php echo $accepter_id; ?>&challengerid=<?php echo $challenger_id; ?>&groupdata=<?php echo $accepter_id.'|'.$challenger_id; ?>&agentonline=<?php echo $agent_online; ?>&status=<?php echo $offline; ?>&user=<?php echo $grp_users; ?>&ci=<?php echo $game_id; ?>")'>Open PvP Chat</a>
						                                    <select multiple name="conversation_users[]"  id="xwb-conversation-users" class="xwb-conversation-users" style="display:none;">
						                                    	<option value="<?php echo $accepter_id; ?>" selected><?php echo $accepter_id; ?></option>
						                                    	<option value="<?php echo $challenger_id; ?>" selected><?php echo $challenger_id; ?></option>
						                                    	</select>
							                                <?php
						                                   
							                            }
						                                if ($return) {
							                                return $message_page;
							                            } else {
							                                echo $message_page;
							                            }
							                        } 
							                        else {
							                            $list = $result['error'];
							                        }
							                        ?>
                                                </div>
											</div>


										
											<div class="col-lg-3 col-sm-4 col-md-3 game-action1" style="padding:0;">
												<a href="<?php echo base_url().'game/gameurlEdit/'.$game_id.'/won/'; ?>" class="winning_result">Won</a>   
												<a href="<?php echo base_url().'game/gameurlEdit/'.$game_id.'/lose/'; ?>" class="lose_result">Lost</a>   
												<a href="<?php echo base_url().'game/gameurlEdit/'.$game_id.'/admin/'; ?>" class="admin_result">Admin</a>   
											</div>
										<?php }  ?>
									</li>
                                    <?php $i++; } ?>
                                </ul>
                            </div>
                            <?php } } 
                            if(isset($list2) && !empty($list2)) { { 
                            	if(count($list2)>0) { ?>
	                        <div class="row bidhottest mygamelist">
	                            <h2>Winning Games List</h2>
	                            <ul>
	                               <?php foreach($list2 as $value) { 
	                               	   if($this->session->userdata('user_id')==$value['winner_id']) { ?>
	                                   <li>
	                                        <div class="col-lg-6 col-sm-6 col-md-3 bid-info">
	                                            <h3><?php echo $value['game_name']; ?></h3>
	                                            <p><?php //echo substr($value['game_description'],0,60).'.....'; ?></p>
	                                        </div>
	                                        <div class="col-lg-2 col-sm-3 col-md-2 uplod-day">
	                                            <h4><?php //echo time_elapsed_string(strtotime($value['created_at']));?></h4>
	                                        </div>
	                                        <div class="col-lg-4 col-sm-4 col-md-2 divPrice">
	                                            <div class="game-price">
	                                                <h3>Winning Prize=<?php echo CURRENCY_SYMBOL.$value['price']; ?></h3>
	                                            </div>
	                                        </div>
	                                    </li>
	                                <?php } } ?>
	                            </ul>
	                        </div>
                     <?php } } } ?>
                     <?php 
                        if(isset($list3) && !empty($list3)) { 
                            if(count($list3)>0) { ?>
                            <div class="row bidhottest mygamelist">
	                            <h2>Lost Challenges</h2>
	                            <ul>
		                        	<?php foreach($list3 as $value) { 
											if($this->session->userdata('user_id') == $value['loser_id']) { ?>
		                                   <li>
		                                       <!--<div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">

		                                           <img src="<?php echo base_url().'upload/game/'. $value['game_image'];?>" alt="">

		                                        </div>-->

		                                        <div class="col-lg-8 col-sm-6 col-md-3 bid-info">

		                                            <h3><?php echo $value['game_name']; ?></h3>

		                                            <p><?php //echo substr($value['game_description'],0,60).'.....'; ?></p>

		                                        </div>

		                                        <div class="col-lg-2 col-sm-3 col-md-2">

		                                            <h4><?php //echo $value['created_at']; ?></h4>

		                                        </div>

		                                        <div class="col-lg-2 col-sm-4 col-md-2 divPrice">

		                                            <div class="game-price">

		                                                <h5><?php if($this->session->userdata('user_id') == $value['loser_id']) echo 'Lost Game'; ?></h5>

		                                            </div>

		                                        </div>
		                                    </li>
		                                    <?php
		                                }
		                            } ?>
	                            </ul>
	                        </div>
                     <?php } else { echo 'No data Found'; } } ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div id="leave_lobby_modal_from_dash" class="modal custmodal" style="display: none;">
  <div class="modal-content">
     <div class="modal-header">
        <span class="close">&times;</span>  
        <h1 class="warning_msg"></h1>
        <h4>Are you sure to leave this lobby .?</h4>
        <h3><a class="button yes">OK</a><a class="button no">Cancel</a></h3>
     </div>
  </div>
</div>
