<div class="col-lg-2 adm-panel">
    	<div class="adm-profile">
            <span><img src="<?php echo($this->session->userdata('user_image'))? base_url().'upload/profile_img/'.$this->session->userdata('user_image') : base_url().'upload/profile_img/noimg.png'; ?>" alt="logo"></span>
            <span class="usrname">
                <?php if ($_SESSION['display_name'] != '') {
                    echo $_SESSION['display_name'];
                } else {
                    echo $this->session->userdata('user_name');
                } ?>
            </span>
			<br>
            <?php 
            $userdata = $this->General_model->view_data('user_detail',array('user_id'=>$this->session->userdata('user_id')));
            ?>
            <span class="last_login">Account # <?php echo $userdata[0]['account_no']; ?></span>
        </div>
        <div class="adminList">
        	<ul>

                <!-- <li <?php //if($this->uri->segment('2')=='myprofile') echo 'class="active"'; ?>><a href="<?php //echo base_url().'user/myprofile'; ?>"><i class="fa fa-user" aria-hidden="true"></i> My profile </a></li>

                <li <?php //if($this->uri->segment('2')=='profile') echo 'class="active"'; ?>><a href="<?php //echo base_url().'game/newgame'; ?>"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard </a></li> -->

                <li <?php if($this->uri->segment('2')=='gameadd' || $this->uri->segment('2')=='lobbyadd' || $this->uri->segment('2')=='newgame') echo 'class="active"'; ?>><a href="<?php echo base_url().'game/newgame'; ?>"><i class="fa fa-flag-o" aria-hidden="true"></i> Place Challenge</a></li> 

                <li <?php if($this->uri->segment('1')=='dashboard') echo 'class="active"'; ?>><a href="<?php echo base_url().'dashboard'; ?>"><i class="fa fa-list" aria-hidden="true"></i> My Challenges</a></li> 
                <li <?php if($this->uri->segment('2')=='getUserEvents') echo 'class="active"'; ?>><a href="<?php echo base_url().'points/getUserEvents'; ?>"><i class="fa fa-list" aria-hidden="true"></i> My Events</a></li> 


                <li <?php if($this->uri->segment('2')=='buypoints') echo 'class="active"'; ?>><a href="<?php echo base_url().'points/buypoints'; ?>"><i class="fa fa-cart-plus" aria-hidden="true"></i> Purchase Funds </a></li>
                <li <?php if($this->uri->segment('2')=='history') echo 'class="active"'; ?>><a href="<?php echo base_url().'points/history'; ?>"><i class="fa fa-history" aria-hidden="true"></i>  Purchase History</a></li>


                <li <?php if($this->uri->segment('2')=='withdrawal') echo 'class="active"'; ?>><a href="<?php echo base_url().'points/withdrawal'; ?>"><i class="fa fa-bell" aria-hidden="true"></i>  Withdrawal</a></li>
                
				<li id="sendmoney_li" class="hover_effect <?php echo ($this->uri->segment('1')=='sendmoney') ? 'active' : ''; ?>"><a href="<?php echo base_url().'sendmoney'; ?>" src="<?php echo base_url() ?>assets/frontend/images/send_money.png" hover_src="<?php echo base_url() ?>assets/frontend/images/send_money_white.png"><img src="<?php echo base_url().'assets/frontend/images/send_money.png'; ?>" class="customer_service_icon" height="20" width="20" >&nbsp; Send Money to Friend / Partners</a></li>

                <li id="my_order_li" class="togglemenu hover_effect <?php echo ($this->uri->segment('1') == 'order') ? 'active' : ''; ?>"><a class="show_store_opt" src="<?php echo base_url() ?>assets/frontend/images/my_order_icon_orange.png" hover_src="<?php echo base_url() ?>assets/frontend/images/my_order_icon_white.png" toggle_tg="my_store_exp"><img src="<?php echo base_url().'assets/frontend/images/my_order_icon_orange.png'; ?>" class="my_order_icon" height="20" width="20" ><span>&nbsp; My Store Account</span></a></li>
                <div class="sublist submenu_div" toogle_div="my_store_exp" style="display: none;">
                    <li id="my_order_li" class="hover_effect <?php echo ($this->uri->segment('1') == 'order') ? 'active' : ''; ?>"><a href="<?php echo base_url().'order'; ?>" src="<?php echo base_url() ?>assets/frontend/images/my_order_icon_orange.png" hover_src="<?php echo base_url() ?>assets/frontend/images/my_order_icon_white.png" data-toggle="collapse" data-target="#demo"><img src="<?php echo base_url().'assets/frontend/images/my_order_icon_orange.png'; ?>" class="my_order_icon" height="20" width="20" >&nbsp; My Order</a></li>
                    <li id="my_order_li" class="hover_effect <?php echo ($this->uri->segment('1') == 'order') ? 'active' : ''; ?>"><a href="<?php echo base_url().'order'; ?>" src="<?php echo base_url() ?>assets/frontend/images/my_order_icon_orange.png" hover_src="<?php echo base_url() ?>assets/frontend/images/my_order_icon_white.png"><img src="<?php echo base_url().'assets/frontend/images/my_order_icon_orange.png'; ?>" class="my_order_icon" height="20" width="20" >&nbsp; My Wishlist</a></li>
                </div>

                <li id="trade_li" <?php if($this->uri->segment('2')=='tradeHistory') echo 'class="active"'; ?>><a href="<?php echo base_url().'trade/tradeHistory'; ?>"><i class="fa fa-exchange"></i>&nbsp; Trade History</a></li>

                <li id="trade_li" <?php if($this->uri->segment('2')=='tipHistory') echo 'class="active"'; ?>><a href="<?php echo base_url().'trade/tipHistory'; ?>"><i class="fa fa-money"></i>&nbsp; Tip History</a></li>

            </ul>

        </div>

    </div>
