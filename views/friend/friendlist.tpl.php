<div class="adm-dashbord">
<?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
    <div class="container padd0">
        <?php $this->load->view('dashboard/profile_sidebar_2.tpl.php'); ?>
        <div class="col-lg-10">
            <div class="col-lg-12 padd0">
                <div> 
                    <div class="col-lg-11">
                        <div class="profile_top row">
                            <div class="col-sm-6 text-right">
                                <a class="profile_top_btn black" href="<?php echo base_url('user/myprofile'); ?>">My Profile</a>
                            </div>
                            <div class="col-sm-6">
                                <a class="profile_top_btn orange" href="<?php echo base_url('friend'); ?>">Dashboard</a>
                            </div>
                        </div>
                        <div class="myprofile-edit">

                          <h2>Friend List</h2>

                          <div class="col-lg-12">

                                <div class="friend_list_second_row row">
                                <div class="row pendings_friends_div">
                                <a href="<?php echo base_url();?>friend/pendingrequest_friendlist/" class="">Friend Request <strong>( <?php print_r($pendingrequest_friendlist_count); ?> ) </a></strong>
                                </div>
                                <div class="col-lg-4 col-sm-4 col-md-2"></div>
                                  <div class="col-lg-4 col-sm-4 col-md-2 search_div">
                                    <div class="input-group add-on form-control">
                                      <input placeholder="Name/ID" name="srch-plyr" id="srch-plyr" type="text" onkeyup="search_my_player()">
                                      <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                      </div>
                                  </div>
                                  <div class="col-lg-4 col-sm-4 col-md-2"></div>
                                  <div class="row search_for_friends">
                                      <a href="#" class="" style="margin-right: 30px;">Friends List</a>
                                      <a style="background:#333;" href="<?php echo base_url();?>friend/find_friendlist" class="">Search Friends</a>
                                  </div>
                                  

                                  </div>
                              <div class="row friend_list_main_row">
                                  <ul>
                                    <?php 
                                    if($friendlist) {
                                      foreach($friendlist as $val) { 
                                        $name = $val['name'];
                                        if ($val['display_name_status'] == 1) {
                                          $name = $val['display_name'];
                                        }
                                        ?>
                                      <li class="frind_row_li" id="frind_row<?php echo $val['friend_id']; ?>">
                                          <div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
                                              <img src="<?php echo base_url().'upload/profile_img/'. $val['image'];?>" alt="">
                                            </div>
                                            <div class="col-lg-3 col-sm-6 col-md-3 bid-info">
                                              <h3><?php echo $name; ?></h3>
                                                <p><?php //echo $val['country'].' '.$val['state']; ?></p>
                                                <span class="postby"><a href="<?php echo base_url(); ?>friend/friend_profile/params?user_id=<?php echo $val['friend_id']; ?>" class="view_profile">Profile</a><a href="<?php echo base_url(); ?>mail/?friend_id=<?php echo $val['friend_id']; ?>"><i class="fa fa-envelope-o"></i></a></span> 
                                            </div>
                                            <div class="col-lg-4 col-sm-3 col-md-2 bid-info team_info">
                                                <p class="font_big">Esports Team</p>
                                                <?php echo ($val['team_name']) ? '<h3>'.$val['team_name'].'</h3>' : '' ?>
                                                <p class="friends_chat"><a target="_blank" href="<?php echo base_url() ?>chat/friends_chat_box/?friend_id=<?php echo $val['friend_id']; ?>"><i class="fa fa-comments-o"></i></a></p>
                                            </div>
                                            <div class="col-lg-3 col-sm-5 col-md-3 primum-play play-n-rating">
                                              <a href="<?php echo base_url(); ?>game/gameadd/?personal_challenge_friend_id=<?php echo $val['friend_id']; ?>&user_id=<?php echo $_SESSION['user_id']; ?>">Challenge friend</a>
                                                <span class="glyphicon glyphicon-minus" onclick="remove_friend(<?php echo $_SESSION['user_id']; ?>,<?php echo $val['friend_id']; ?>);"></span>
                                            </div>
                                        </li>

                                             <?php } } else { echo "<h5 style='color: #FF5000;'> Friends Not Available</h5>"; }?>

                                    </ul>
                          </div>

                        </div>

                    </div>

                </div>

            </div>   

        </div>

    </div>

</div>



