<div class="adm-dashbord">
<?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
    <div class="container padd0">
        <?php $this->load->view('dashboard/profile_sidebar_2.tpl.php'); ?>
        <div class="col-lg-10">
            <div class="col-lg-12 padd0">
                <div> 
                    <div class="col-lg-11">
                    <div class="profile_top row">
                            <div class="col-sm-6 text-right">
                                <a class="profile_top_btn black" href="<?php echo base_url('user/myprofile'); ?>">My Profile</a>
                            </div>
                            <div class="col-sm-6">
                                <a class="profile_top_btn orange" href="<?php echo base_url('friend'); ?>">Dashboard</a>
                            </div>
                        </div>
                        <div class="myprofile-edit">

                          <h2>Profile</h2>

                          <?php $this->load->view('common/show_message'); ?>

                          <div class="col-lg-12">
                            <?php $value = $friend_profile; ?>
                            <div class="row">

                                <div class="form-group profile_image_div">
                                  <div class="row">
                                    <div class="col-lg-12">

                                    <div class="friend_profile_img" style="text-align:center;">

                                        <img src="<?php if(isset($value->image)) echo base_url().'upload/profile_img/'.$value->image;?>"  id="k"/>                   

                                    </div>
                                    </div>
                                    <div class="col-lg-12 text-center friend_plyr_name">
                                      <p class="white"><?php echo (isset($value->display_name)) ? $value->display_name : $value->name; ?></p>
                                    </div>
                                    <div class="col-lg-12 friend_plyr_id"   >
                                      <label style="font-size:40px;padding:0;">Account NO #<?php echo $value->account_no; ?></label>
                                    </div>                                    
                                    <div class="col-lg-12">
                                      <p>Esports Team</p>
                                    </div>
                                    <div class="col-lg-12 friend_plyr_id">
                                      <label class="white no-padding"><?php echo $value->team_name; ?></label>
                                    </div>
                                    </div>
                                </div>

<!--                                 <div class="form-group">

                                    <label class="col-lg-2">Email</label>

                                    <div class="input-group col-lg-10">

                                        <label><?php // echo $value->email; ?></label>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-lg-2">Phone</label>

                                    <div class="input-group col-lg-10">

                                    <label><?php // echo $value->number; ?></label>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-lg-2">Location</label>

                                    <div class="input-group col-lg-10">

                                      <label><?php // echo $value->location; ?></label>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-lg-2">Country</label>

                                    <div class="input-group col-lg-10">

                                       <label><?php // echo $value->country; ?></label>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-lg-2">State</label>

                                    <div class="input-group col-lg-10">

                                      <label><?php // echo $value->state; ?></label>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-lg-2">Zip Code</label>

                                    <div class="input-group col-lg-10">

                                      <label><?php // echo $value->zip_code; ?></label>

                                    </div>

                                </div> -->
                              
                             </div>
                             <div class="col-lg-12"  align="center">
                                    <div class="input-group back_button">
                                      <a href="javascript:history.go(-1)">Back</a>
                                    </div>
                                </div>
                                    
                          </div>

                        </div>

                    </div>

                </div>

            </div>   

        </div>

    </div>

</div>


