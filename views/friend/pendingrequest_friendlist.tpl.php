<div class="adm-dashbord">
<?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
    <div class="container padd0">

        <?php $this->load->view('dashboard/profile_sidebar_2.tpl.php'); ?>

        <div class="col-lg-10">

            <div class="col-lg-12 padd0">

                <div> 

                    <div class="col-lg-11">

                        <div class="myprofile-edit">

                          <h2>Friend List</h2>

                          <?php $this->load->view('common/show_message'); ?>

                          <div class="col-lg-12 find_friend_list_main">
                                <div class="friend_list_second_row row">
                                <div class="col-lg-4 col-sm-4 col-md-2"></div>
                                  <div class="col-lg-4 col-sm-4 col-md-2 search_div">
                                    <div class="input-group add-on form-control">
                                      <input placeholder="Find Friends/ID" name="srch_pending_plyr" id="srch_pending_plyr" type="text" onkeyup="serach_pending_player()">
                                      <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                      </div>
                                  </div>
                                  <div class="col-lg-4 col-sm-4 col-md-2"></div>
                                  </div>
                              <div class="row friend_list_main_row">
                                  <ul>
                                    <?php 
                                      if(count($pendingrequest_friendlist)>0) {
                                          foreach($pendingrequest_friendlist as $val) {
                                            $name = $val['name'];
                                            if ($val['display_name_status'] == 1) {
                                              $name = $val['display_name'];
                                            }
                                          ?>
                                      <li>
                                          <div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
                                              <img src="<?php echo base_url().'upload/profile_img/'. $val['image'];?>" alt="">
                                            </div>
                                            <div class="col-lg-3 col-sm-6 col-md-3 bid-info">
                                              <h3><?php echo $name; ?></h3>
                                              
                                                <p><?php //echo $val['country'].' '.$val['location'].' '.$val['state']; ?></p>
                                                <span class="postby"><a href="<?php echo base_url(); ?>friend/friend_profile/params?user_id=<?php echo $val['friend_id']; ?>" class="view_profile">Profile</a></span> 
                                            </div>
                                            <div class="col-lg-4 col-sm-3 col-md-2 bid-info team_info">
                                            <p class="font_big">Esports Team</p>
                                              <?php echo ($val['team_name']) ? '<h3>'.$val['team_name'].'</h3>' : '' ?>
                                            </div>
                                            <div class="col-lg-3 col-sm-5 col-md-3 primum-play play-n-rating">
                                              <a class="add_friend_button" href='<?php echo base_url(); ?>friend/accept_the_request/<?php echo $_SESSION["user_id"]; ?>/<?php echo $val["friend_id"]; ?>' onclick="return confirm('Are you sure want to add ?')"> Accept</a>
                                              <a class="add_friend_button" onclick="return confirm('Are you sure want to Remove ?')" href='<?php echo base_url(); ?>friend/remove_the_request/<?php echo $_SESSION["user_id"]; ?>/<?php echo $val["friend_id"]; ?>' style="margin-left:10px;"> Remove</a>
                                             <!--  <form role="form" action="<?php // echo base_url(); ?>friend/add_friend/" name="addToFriendForm" class="add-to-friend-form" method="POST" enctype="multipart/form-data">

                                              <input type="submit" class="add_friend_button" value="Accept"  onclick="return confirm('Are you sure want to add ?')">
                                              <input type="submit" class="add_friend_button" value="Remove"  onclick="return confirm('Are you sure want to Remove ?')" style="margin-left:10px;"> -->
                                              <!-- href="<?php //echo base_url(); ?>friend/add_friend/<?php //echo $_SESSION['user_id'];?>/<?php //echo $val['user_id']; ?>" -->
                                               <!--  <textarea style="display: none;" name="mail">You have new friend request please accept <a href='<?php // echo base_url(); ?>friend/accept_the_request/<?php // echo $_SESSION["user_id"]; ?>/<?php // echo $val["user_id"]; ?>'>click here to accept</a></textarea>
                                                <input type="hidden" name="user_to" value="<?php // echo $val['user_id']; ?>">
                                                </form> -->
                                            </div>
                                            
                                        </li>

                                             <?php } } else { echo "<h5 style='color: #FF5000;'> No new requests</h5>"; }?>

                                    </ul>
                          </div>
                          <div class="col-lg-12" align="center">
                                    <div class="input-group back_button">
                                      <a href="javascript:history.go(-1)">Back</a>
                                    </div>
                                </div>
                        </div>

                    </div>

                </div>

            </div>   

        </div>

    </div>

</div>


