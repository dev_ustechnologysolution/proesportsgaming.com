<div class="adm-dashbord">  
    <div class="container padd0">
        <?php $this->load->view('dashboard/profile_sidebar.tpl.php');?>
        <div class="col-lg-10">
            <div class="col-lg-11 padd0">
                <div>
                    <div class="col-lg-11">
                    <div class="profile_top row">
                    <div class="col-sm-6 text-right">
                        <a class="profile_top_btn black" href="<?php echo base_url('user/myprofile'); ?>">My Profile</a>
                    </div>
                    <div class="col-sm-6">
                        <a class="profile_top_btn orange" href="<?php echo base_url('friend'); ?>">Dashboard</a>
                    </div>
                </div>
                        <div class="myprofile-edit">
                            <h2>Challenge Information</h2>
                            <div class="col-lg-12">
                                    <?php echo @$this->session->flashdata('msg');
                                          if(isset($_GET['personal_challenge_friend_id']) && isset($_GET['user_id']) && $_GET['personal_challenge_friend_id'] != '' && $_GET['user_id'] != '')
                                            {
                                                 $user_id = $_GET['user_id'];
                                                 $personal_challenge_friend_id = $_GET['personal_challenge_friend_id'];
                                                 echo '<div class="challenge_personal_mail"><div class="row friend_list_main_row"><ul><li>
                                                      <div class="col-lg-4 bidimg padd0">
                                                          <img src="'.base_url().'upload/profile_img/'.$challenge_personal_profile->image.'" alt="">
                                                        </div>
                                                        <div class="col-lg-8 bid-info">
                                                          <h3>'.$challenge_personal_profile->name.'</h3>
                                                            <p>'.$challenge_personal_profile->country.' '.$challenge_personal_profile->state.'</p>
                                                            <span class="postby">ACCOUNT #'.$challenge_personal_profile->user_id.'</span> 
                                                        </div>
                                                        </li></ul>
                                                    </div><div class="row personal_mail_custom_settings"><input type="checkbox" class="" name="" id="" value=""> Custom Settings</div></div><div class="row"><form id="frm_game_create" name="frm_game_create"  action="'.base_url().'game/gameCreate" method="post" enctype="multipart/form-data" ><input type="hidden" name="user_id" value="'.$user_id.'"><input type="hidden" name="personal_challenge_friend_id" value="'.$personal_challenge_friend_id.'"><input type="hidden" name="custom_game" id="custom_game">'; 
                                            }
                                            else
                                            {
                                                  echo '<div class="row"><form id="frm_game_create" name="frm_game_create"  action="'.base_url().'game/gameCreate" method="post" enctype="multipart/form-data" >';
                                            }
                                        ?>
                                        <div class="form-group select_games">
                                            <label class="col-lg-4">Game System</label>
                                            <div class="input-group col-lg-8">
                                                <?php
                                                 if(isset($game_cat)) { ?>
                                                   <select type="text" id="game_category" class="select2 form-control game_category" name="game_category">
                                                    <option value="<?php echo $game_cat[0]['id'];
                                                    ?>"><?php echo $game_cat[0]['category_name'] ?></option>
                                                </select>
                                                <?php
                                            } else {
                                                ?>
                                                <select  type="text" id="game_category" class="select2 form-control game_category" name="game_category">
                                                    <option value="">--Select a system--</option>
                                                    <?php foreach ($game_category as $value) {
                                                       ?>
                                                       <option value="<?php echo $value['id'];
                                                       ?>"><?php echo $value['cat_slug'] ?></option>
                                                       <?php
                                                   }
                                                   ?>
                                               </select>
                                               <?php
                                           }
                                           ?>
                                       </div>
                                   </div>
                                   <div class="form-group select_games">
                                    <label class="col-lg-4">Game Name</label>
                                    <div class="input-group col-lg-8">
                                        <?php if(isset($game_dat)) {
                                           ?>
                                           <select type="text" id="name" class="select2 form-control game_name" name="game_name">
                                            <option value="<?php echo $game_dat[0]['id'];
                                            ?>"><?php echo $game_dat[0]['game_name'] ?></option>
                                        </select>
                                        <?php
                                    } else {
                                        ?>
                                        <select type="text" id="name" class="select2 form-control game_name" name="game_name">
                                            <option value="">--Select a Game--</option>
                                        </select>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="form-group select_games">
                                <label class="col-lg-4">Game Sub Category</label>
                                <div class="input-group col-lg-8">
                                    <?php if(isset($subgame) && !empty($subgame)) {
                                       ?>
                                       <select type="text" id="sub_name" class="select2 form-control subgame_name_id" name="subgame_name_id">
                                        <option value="<?php echo $subgame[0]['id'];
                                        ?>"><?php echo $subgame[0]['sub_game_name'] ?></option>
                                    </select>
                                    <?php
                                } else {
                                    ?>
                                    <select type="text" id="sub_name" class="select2 form-control subgame_name_id" name="subgame_name_id">
                                        <option value="0">--Select a Sub Game--</option>
                                    </select>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div class="form-group custom_games">
                            <label class="col-lg-4">Game System</label>
                            <div class="input-group col-lg-8">
                                <input type="text" placeholder="Enter Game Category" class="form-control select_game_category">
                             </div>
                             </div>
                             <div class="form-group custom_games">
                              <label class="col-lg-4">Game Name</label>
                              <div class="input-group col-lg-8">
                                <input type="text" placeholder="Enter Game Name" class="form-control select_game_name">
                          </div>
                          </div>
                          <div class="form-group custom_games">
                              <label class="col-lg-4">Game Sub Category</label>
                              <div class="input-group col-lg-8">
                                <input type="text" placeholder="Enter Sub Game Name" class="form-control select_subgame_name_id">
                                </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4">Game Size</label>
                            <div class="input-group col-lg-8">
                                <select type="text" class="select2 form-control" name="game_type"  id="game_type">
                                    <option>--Select Game Size--</option>
                                    <?php
                                    $game_type_raw = explode('|',$game_dat[0]['game_type']);

                                    if(isset($game_type_raw[0]))
                                    {

                                       foreach($game_type_raw as $j)
                                       {

                                          if($j!='') {

                                             echo '<option value="' . $j . '">--' . $j . 'V' . $j . '--</option>';

                                         }

                                     }

                                    }
                                 ?>
                                          </select>
                                        </div>
                                    </div>
                  									<div class="form-group">
                  										<label class="col-lg-4">Wait Timer</label>
                  										<div class="input-group col-lg-8">
                  										   <select type="text" id="game_timer" class="select2 form-control" name="game_timer">
                  												<option value="">--Please Select a Wait Timer--</option>
                  												<option value="15">15mins</option>
                  												<option value="30">30mins</option>
                  												<option value="45">45mins</option>
                  												<option value="1">1hour</option>
                  												<option value="no">No Timer</option>
                  											</select>											
                  										</div>
                  									</div>
                                    <div class="form-group">
                                        <label class="col-lg-4">PSN/XBOX/PC ID</label>
                                        <div class="input-group col-lg-8">
                                          <!--<select type="text" class="form-control" name="device_number" id="number">
                                              <option value="0">--Please Select a Game--</option>
                                              <option value="PSN">PSN</option>
                                              <option value="XBOX">XBOX</option>
                                              <option value="PC ID">PC ID</option>
                                          </select>-->
                                             <input type="text" placeholder="PSN/XBOX/PC ID Or Nickname" id="number" class="form-control" name="device_number" required="required">
                                             <span style="color:red"><?php echo @$this->session->flashdata('required_msg');?></span>
                                        </div>
                                    </div>
									
                                    <div class="form-group">
                                        <label class="col-lg-4">Entry Amount(Minimum 1)</label>
                                        <!-- <span class="col-lg-1" style="text-align: right;margin-top:6px;">$</span> -->
                                        <div class="input-group col-lg-8">
                                            <input type="text" placeholder="Entry Amount" id="amount" class="form-control" name="bet_amount" onkeypress="return isNumberKey(event)">
                                            <span id="demo" style="margin-left: -194px;color: red;"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-lg-4">Best Of</label>
                                      <div class="input-group col-lg-8">
                                         <select type="text" id="best_of" class="select2 form-control" name="best_of">
                                          <option value="">--Please Select a Best of--</option>
                                          <option value="1">Best of 1</option>
                                          <option value="3">Best of 3</option>
                                          <option value="5">Best of 5</option>
                                        </select>                     
                                      </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4">Password</label>
                                        <div class="input-group col-lg-8 game_password_div">
                                          <label class="switch game_password_switch_button">
                                              <input type="checkbox" name="friends_mail" value="friends_mail">
                                              <span class="slider round"></span>
                                            </label>
                                          <input type="text" class="form-control game_password" placeholder="Enter Password" style="display: none;">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4">Description</label>
                                        <div class="input-group col-lg-8">
                                            <?php if(isset($game_dat) && !empty($game_dat)) {
                                               ?>
                                               <textarea class="form-control" id="description" placeholder="Game Description" rows="5" cols="5" name="description"><?php echo $game_dat[0]['game_description'] ?></textarea>
                                           </select>
                                           <?php
                                       }
                                       else {
                                        ?>
                                        <textarea class="form-control" id="description" placeholder="Game Description" rows="5" cols="5" name="description"></textarea>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4">Image</label>
                                <div class="input-group col-lg-8">
                                    <div id="game_img">
                                        <?php if (isset($game_img) && !empty($game_img)) {
                                           foreach ($game_img as $value) {
                                              ?>
                                              <img class="img-cls" id="img<?php echo $value['id'] ?>" style="margin-right: 31px;" onclick="a('<?php echo $value['id'] ?>')" src="<?php echo base_url().'upload/game/'.$value['game_image'] ?>" width="100" height="80"/>
                                              <?php
                                          }
                                      }
                                      ?>
                                  </div>
                              </div>
                          </div>
                          <div class="btn-group">
                            <div class="col-lg-8 pull-right padd0">

                                <input type="hidden" id="game_image_id" name="game_image_id" value="" class="new_game-image">

                                <input type="button" id="submitPlaceChallenge" value="Submit" class="btn-update">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
  $('#frm_game_create .custom_games').hide();
  $('#frm_game_create .custom_game_size').hide();
  var game_type_option = '<option>--Select Game Size--</option>';
  for (var i = 1; i <=16; i++) {
    game_type_option += '<option value="'+i+'">--'+i+'V'+i+'--</option>';
  }
  $('.personal_mail_custom_settings input[type=checkbox]').change(
    function(){
        if ($('.personal_mail_custom_settings  input:checkbox').is(':checked')) {
          $('#custom_game').val('yes');
          $('#frm_game_create .custom_games').show();
          $('#frm_game_create .custom_games .select_game_category').attr('name','game_category');
          $('#frm_game_create .custom_games .select_game_name').attr('name','game_name');
          $('#frm_game_create .custom_games .select_subgame_name_id').attr('name','subgame_name_id');
          $('#frm_game_create .custom_games .select_game_category').attr('id','select_game_category');
          $('#frm_game_create .custom_games .select_game_name').attr('id','select_game_name');
          $('#frm_game_create .custom_games .select_subgame_name_id').attr('id','select_subgame_name_id');   
          $('#frm_game_create #game_type').attr('name','game_type');
          $('#frm_game_create .select_games').hide();
          $('#frm_game_create .select_games select').removeAttr('id');
          $('#frm_game_create .select_games select').removeAttr('name');
          $('#frm_game_create select#game_type option').remove();
          $('#frm_game_create select#game_type').append(game_type_option);
          $('#frm_game_create #game_img .file-upload').remove();
          $('#frm_game_create #game_img .img-cls').remove();
          $('#frm_game_create #game_img').append('<div class="file-upload"><div class="file-select"><div class="file-select-button" id="fileName">Choose File</div><div class="file-select-name" id="noFile">No file chosen...</div> <input type="file" name="game_image" id="chooseGameimage"></div></div>');
          $('.new_game-image').removeAttr('id');
          $('.new_game-image').removeAttr('name'); 
          if ($('#chooseGameimage').length) {
          $('#chooseGameimage').bind('change', function(){
            var filename = $("#chooseGameimage").val();
            if (/^\s*$/.test(filename)) {
              $(".file-upload").removeClass('active');
              $("#noFile").html("No file chosen...");
            }
            else {
              $(".file-upload").addClass('active');
              $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
            }
          });
          }
        }
        else
        {
          $('#custom_game').val("");
          $('#frm_game_create .select_games').show();
          $('#frm_game_create .select_games .game_category').attr('name','game_category');
          $('#frm_game_create .select_games .game_name').attr('name','game_name');
          $('#frm_game_create .select_games .subgame_name_id').attr('name','subgame_name_id');
          $('#frm_game_create .select_games .game_category').attr('id','game_category');
          $('#frm_game_create .select_games .game_name').attr('id','name');
          $('#frm_game_create .select_games .subgame_name_id').attr('id','sub_name');
          $('#frm_game_create #game_type').attr('name','game_type');

          $('#frm_game_create .select_games .game_category option:first').prop('selected',true);
          $('#frm_game_create .select_games .game_name option:first').prop('selected',true);
          $('#frm_game_create .select_games .subgame_name_id option:first').prop('selected',true);
          
          $('#frm_game_create select#game_type option').remove();
          $('#frm_game_create select#game_type').append('<option>--Select Game Size--</option>');
          $('#frm_game_create .custom_games').hide();
          $('#frm_game_create .custom_games input[type=text]').removeAttr('id');
          $('#frm_game_create .custom_games input[type=text]').removeAttr('name'); 
          $('#frm_game_create .custom_game_size').hide();
          $('#frm_game_create #game_img .file-upload').remove();
          $('.new_game-image').attr('id','game_image_id');
          $('.new_game-image').attr('name','game_image_id');
        }
      });
    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>
