<div class="adm-dashbord">
<div class="container padd0">
   <?php $this->load->view('dashboard/profile_sidebar.tpl.php');?>
   <div class="col-lg-10">
      <div class="col-lg-11 padd0">
         <div>
            <div class="col-lg-11">
               <div class="profile_top row">
                  <div class="col-sm-6 text-right">
                     <a class="profile_top_btn orange" href="<?php echo base_url('user/myprofile'); ?>">My Profile</a>
                  </div>
                  <div class="col-sm-6">
                     <a class="profile_top_btn black" href="<?php echo base_url('friend'); ?>">Dashboard</a>
                  </div>
               </div>
               <div class="myprofile-edit">
                  <h2>Challenge Information</h2>
                  <div class="col-lg-12">
                    <div class="ch_info_page_main_div">
                        <div class="col-sm-5">
                            <a href="<?php echo base_url().'game/gameadd'?>" class="button btn">Create Match</a>
                        </div>
                        <div class="col-sm-2">
                            <h3 class="ch_info_or">OR</h3>
                        </div>
                        <div class="col-sm-5">
                        <a href="<?php echo base_url().'livelobby/lobbyadd'?>" class="button btn">Create Live Lobby</a>
                        </div>
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
