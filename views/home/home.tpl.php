<section class="howit-work">
   <div class="container">
      <div class="row header1">
         <h2 class="margin_top"><?php echo $heading_dt[0]['heading1'] ?></h2>
      </div>
      <div class="row">
         <?php foreach ($dt as $value) { ?>
         <div class="col-lg-3 col-sm-3 col-md-3">
            <img src="<?php echo base_url().'upload/gameflow/'.$value['image'];?>" alt="">
            <p><?php echo $value['description']; ?></p>
         </div>
         <?php } ?>
      </div>
      <div class="row">
         <div class="create-ch">
            <a href="<?php echo base_url().'game/newgame'; ?>" class="animated-box">CREATE CHALLENGE</a>
         </div>
      </div>
   </div>
</section>
<section class="body-middle">
   <div class="container game_boxes">
   <div class="row header1">
      <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
      <div class="col-lg-4 col-sm-4 col-md-4">
         <h2>
            <?php echo $heading_dt[0]['heading2'] ?> <br>
         </h2>
      </div>
      <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
   </div>
   <div class="row latestvdo">
      <?php foreach ($video_dt as $vd) {
         if ($vd->is_twitch == '0') {
            $w=explode('&',substr($vd->video_url,stripos($vd->video_url,"?")+3)); @$watch=$w[0].'?'.$w[2];
            if($w[0] != 'U') { ?>
            <div class="col-lg-3 col-sm-6 col-md-3">
               <a href="<?php echo $vd->video_url; ?>" target="_blank">
                  <img src="https://img.youtube.com/vi/<?php echo $w[0]; ?>/mqdefault.jpg" alt="">
                  <div class="float-caption">
                     <h5 class="animated-box"><?php echo $vd->tag; ?></h5>
                  </div>
               </a>
            </div>
            <?php }
         } else { ?>
            <div class="col-lg-3 col-sm-6 col-md-3">
               <a href="<?php echo base_url(); ?>livelobby/watchLobby/<?php echo $vd->lobby_id; ?>" target="_blank">
                  <iframe src="https://player.twitch.tv/?channel=<?php echo $vd->twitch_username; ?>" height="auto" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
                  <div class="float-caption">
                     <h5 class="animated-box"><?php echo $vd->tag; ?></h5>
                  </div>
               </a>
            </div>
         <?php }
      } ?>
   </div>
   <div class="row">
      <div class="view-all"><a href="<?php echo base_url(); ?>Videos" class="animated-box">ALL RECENT ACTION</a></div>
   </div>
   <div class="row header2" align="center">
      <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
      <div class="col-lg-4 col-sm-4 col-md-4">
         <h2>
            HOTTEST GAMES 
            <h3 style="color: #ff5000;">Live Lobbies</h3>
         </h2>
      </div>
      <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
   </div>
   <div class="animated-box-2">
   <div class="row filter-panel home_page_search_tabbing ">
    
      <div class="col-lg-12   ">
         <div class="form-group ">
            <?php if(count($lobby_list)>0) { ?>
            <select name="lobby_system" id="lobby_system" class="form-control" onchange="lobby_game_search()">
               <option class="select_title" value="">Please select a System</span></option>
               <?php foreach($systemList as $val) {  ?>
               <option value="<?php echo $val['id'];?>">
                  <?php echo $val['category_name'];?>
               </option>
               <?php }  ?>
               <?php foreach($custsysList as $csl) {  ?>
               <option value="<?php echo $csl['id']; ?>">
                  <?php echo $csl['category_name'];?>
               </option>
               <?php }  ?>
            </select>
            <select class="form-control" name="lobby_gamesize"  id="lobby_gamesize" onchange="lobby_game_search()">
               <option value="">Please select a Gamesize</option>
               <?php for ($i = 1; $i <= 16; $i++) { ?>
               <option value="<?php echo $i;?>" class="playstore_gamesize_option">
                  <?php echo '--' . $i . 'V' . $i . '--';?>
               </option>
               <?php
                  };
                  ?>
            </select>
            <select name="lobby_game" id="lobby_game" class="form-control" onchange="lobby_game_search()">
               <option value="">Please select a Game</option>
            </select>
            <select class="form-control" name="lobby_subgame" id="lobby_subgame" onchange="lobby_game_search()">
               <option value="">Please select a Subgame</option>
            </select>
            <div class="input-group add-on form-control" style="float: right;">
               <input placeholder="Search Tag/Game Name" name="lobby_srch-term" id="lobby_srch-term" type="text"  onkeyup="lobby_game_search()" onkeydown="lobby_game_search()">
               <span class="glyphicon glyphicon-search form-control-feedback"></span>
            </div>
            <?php } ?>
         </div>
      </div>
   </div>
 </div>
   <div class="row">
      <div class="create-ch">
         <a href="<?php echo base_url().'game/newgame'; ?>" class="animated-box">CREATE LIVE LOBBY</a>
      </div>
   </div>
   <div class="row bidhottest hottestgame playstore_game_list_front">
      <ul>
         <?php 
            if(count($lobby_list)>0) {
                echo $this->session->flashdata('err_lobby');
                unset($_SESSION['err_lobby']);
                foreach($lobby_list as $val) {
                  if($val['game'] == 1) {
                     $game_type_image = 'XBOX.png';
                  } else if($val['game'] == 2) {
                     $game_type_image = 'PS4.png';
                  } else if($val['game'] == 3) {
                     $game_type_image = 'PC.png';
                  } else {
                     $game_type_image = '';
                  }
                ?>
         <li>
            <div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
               <img src="<?php echo base_url().'upload/game/'. $val['game_image'];?>" alt="">
            </div>
            <div class="col-lg-3 col-sm-6 col-md-3 bid-info">
               <h3><?php echo $val['game_name']; ?></h3>
               <p><?php echo $val['sub_game_name']; ?></p>
               <span class="postby">Tag: <?php echo $val['device_id']; ?></span> 
               <!--<span class="postby">By <?php echo $val['name']; ?></span>-->
            </div>
            <div class="col-lg-2 col-sm-3 col-md-2 uplod-day">
               <?php if($val['game_timer'] != '' && $val['game_timer'] != "0000-00-00 00:00:00"){ $diff = strtotime($val['game_timer'])- time(); if(time() < strtotime($val['game_timer'])){ ?>
               <?php } ?>
               <h3 class="timer custom_timer" data-seconds-left="<?php echo $diff; ?>"></h3>
               <?php } ?>
               <p> Gamesize: <?php echo "--".$val['game_type']."V".$val['game_type']."--";?></p>
               <p class="best_of"> <?php 
                  if ($val['best_of']!=null) {
                    echo 'Best of '.$val['best_of'];
                  }
                  ?>
            </div>
            <div class="col-lg-2 col-sm-4 col-md-2 divPrice">
               <div class="game-price premium">
                  <h3><?php $lobby_bet_price = number_format((float) $val['price'], 2 , '.', ''); echo CURRENCY_SYMBOL.$lobby_bet_price; ?></h3>
               </div>
            </div>
            <div class="col-lg-1 col-sm-4 col-md-2">
               <?php if ($game_type_image != '') { ?>
                  <img src="<?php echo base_url().'upload/game/'. $game_type_image;?>">
               <?php } ?>
            </div>
            <?php
            $lobbylink = 'Live Stream';
            if ($val['is_event'] == 1) {
               $lobbylink = 'Live Event';
            } ?>
            <div class="col-lg-2 col-sm-5 col-md-3 primum-play play-n-rating">
               <a href="<?php echo base_url().'livelobby/watchLobby/'.$val['id'];?>" class="animated-box"><?php echo $lobbylink; ?></a>
            </div>
         </li>
         <?php } } else { echo "<h5 style='color: #FF5000;'> Lobby Not Available</h5>"; }?>
      </ul>
   </div>
   <div class="row">
      <div class="view-all"><a href="<?php echo base_url().'Livelobby/live_lobby_list' ?>" class="animated-box">VIEW ALL Lobby</a></div>
   </div>
   <div class="row header2" align="center">
      <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
      <div class="col-lg-4 col-sm-4 col-md-4">
         <h2>
            HOTTEST GAMES 
            <h3 style="color: #ff5000;">Matches</h3>
         </h2>
      </div>
      <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
   </div>
   <div class="animated-box-2">
   <div class="row filter-panel home_page_search_tabbing">
      <div class="col-lg-12">
         <div class="form-group">
            <?php if(count($challangetDp)>0) { ?>
            <select name="system" id="system" class="form-control" onchange="game_search()">
               <option class="select_title" value="">Please select a System</span></option>
               <?php foreach($systemList as $val) {  ?>
               <option value="<?php echo $val['id'];?>">
                  <?php echo $val['category_name'];?>
               </option>
               <?php } ?>
            </select>
            <select class="form-control" name="gamesize"  id="gamesize" onchange="game_search()">
               <option value="">Please select a Gamesize</option>
               <?php for ($i = 1; $i <= 16; $i++){ ?>
               <option value="<?php echo $i;?>" class="gamesize_option">
                  <?php echo '--' . $i . 'V' . $i . '--';?>
               </option>
               <?php
                  };
                  ?>
            </select>
            <select name="game" id="game" class="form-control" onchange="game_search()">
               <option value="">Please select a Game</option>
               <!--    <?php //foreach($gamenameList as $k=>$v) { ?>
                  <option value="<?php //echo $v['id'];?>" class="system_list_front_side">
                      <?php //echo $v['game_name'];?>
                  </option>
                  <?php //} ?> -->
            </select>
            <select class="form-control" name="subgame" id="subgame" onchange="game_search()">
               <option value="">Please select a Subgame</option>
               <!--  <?php // foreach($subgamenameList as $k=>$v) { ?>
                  <option value="<?php // echo $v['sub_game_name'];?>">
                      <?php // echo $v['sub_game_name'];?>
                  </option>
                  <?php // } ?> -->
            </select>
            <div class="input-group add-on form-control" style="float: right;">
               <input placeholder="Search Tag/Game Name" name="srch-term" id="srch-term" type="text"  onkeyup="game_search()" onkeydown="game_search()">
               <span class="glyphicon glyphicon-search form-control-feedback"></span>
            </div>
            <?php } ?>
         </div>
      </div>
   </div>
 </div>
   <div class="row bidhottest hottestgame xbox1">
      <ul>
         <?php 
            if(count($challangetDp)>0) { 
                echo $this->session->flashdata('err');
                unset($_SESSION['err']);
                foreach($challangetDp as $val) {
                  if($val['cat_slug'] == 'Xbox') {
                     $game_type_image = 'XBOX.png';
                  } else if($val['cat_slug'] == 'Ps4') {
                     $game_type_image = 'PS4.png';
                  } else if($val['cat_slug'] == 'Pc') {
                     $game_type_image = 'PC.png';
                  } else {
                     $game_type_image = '';
                  }
         ?>
         <li>
            <div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
               <img src="<?php echo base_url().'upload/game/'. $val['game_image'];?>" alt="">
            </div>
            <div class="col-lg-3 col-sm-6 col-md-3 bid-info">
               <h3><?php echo $val['game_name']; ?></h3>
               <p><?php echo $val['sub_game_name']; ?></p>
               <span class="postby">Tag: <?php echo $val['device_id']; ?></span> 
               <!--<span class="postby">By <?php // echo $val['name']; ?></span>-->
            </div>
            <div class="col-lg-2 col-sm-3 col-md-2 uplod-day">
               <?php if($val['game_timer'] != '' && $val['game_timer'] != "0000-00-00 00:00:00"){ $diff = strtotime($val['game_timer'])- time(); if(time() < strtotime($val['game_timer'])){ ?>
               <?php } ?>
               <h3 class="timer custom_timer" data-seconds-left="<?php echo $diff; ?>"></h3>
               <?php } ?>
               <p> Gamesize: <?php echo "--".$val['game_type']."V".$val['game_type']."--";?></p>
               <p class="best_of"> <?php 
                  if ($val['best_of']!=null) {
                    echo 'Best of '.$val['best_of'];
                  }
                  ?></p>
            </div>
            <div class="col-lg-2 col-sm-4 col-md-2 divPrice">
               <div class="game-price premium">
                  <h3><?php echo CURRENCY_SYMBOL.$val['price']; ?></h3>
               </div>
            </div>
             <div class="col-lg-1 col-sm-4 col-md-2">
               <?php if ($game_type_image != '') { ?>
                  <img src="<?php echo base_url().'upload/game/'. $game_type_image;?>">
               <?php } ?>
            </div>
            <div class="col-lg-2 col-sm-5 col-md-3 primum-play play-n-rating">
               <?php if ($val['game_password']!='' && $val['game_password']!=null) { ?>
               <a href="<?php echo base_url().'home/gamePwAuth/'.$val['id'];?>" class="animated-box">play</a>
               <?php } else { ?> 
               <a href="<?php echo base_url().'home/gameAuth/'.$val['id'];?>" class="animated-box">play</a>
               <?php } ?>
            </div>
         </li>
         <?php } } else { echo "<h5 style='color: #FF5000;'>Games Not Available</h5>"; }?>
      </ul>
   </div>
   <div class="row">
      <div class="view-all "><a class="animated-box" href="<?php echo base_url().'Matches' ?>">VIEW ALL GAMES</a></div>
   </div>
   
</section>

