<section class="body-middle innerpage">
	<div class="container">
	<!--Xbox Game Challange -->
	<div class="row header2" align="center">
      <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
      <div class="col-lg-4 col-sm-4 col-md-4">
         <h2>
            HOTTEST GAMES 
            <h3 style="color: #ff5000;">Live Lobbies</h3>
         </h2>
      </div>
      <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
   </div>
    <div class="animated-box">
   <div class="row filter-panel home_page_search_tabbing">
      <div class="col-lg-12">
         <div class="form-group">
            <?php if(count($lobby_list)>0) { ?>
            <select name="lobby_system" id="lobby_system" class="form-control" onchange="lobby_game_search()">
               <option class="select_title" value="">Please select a System</span></option>
               <?php foreach($systemList as $val) {  ?>
               <option value="<?php echo $val['id'];?>">
                  <?php echo $val['category_name'];?>
               </option>
               <?php } ?>
               <?php foreach($custsysList as $csl) {  ?>
               <option value="<?php echo $csl['id']; ?>">
                  <?php echo $csl['category_name'];?>
               </option>
               <?php } ?>               
            </select>
            <select class="form-control" name="lobby_gamesize"  id="lobby_gamesize" onchange="lobby_game_search()">
               <option value="">Please select a Gamesize</option>
               <?php
                  for ($i = 1; $i <= 16; $i++){
                    ?>
               <option value="<?php echo $i;?>" class="playstore_gamesize_option">
                  <?php echo '--' . $i . 'V' . $i . '--';?>
               </option>
               <?php
                  };
                  ?>
            </select>
            <select name="lobby_game" id="lobby_game" class="form-control" onchange="lobby_game_search()">
               <option value="">Please select a Game</option>
            </select>
            <select class="form-control" name="lobby_subgame" id="lobby_subgame" onchange="lobby_game_search()">
               <option value="">Please select a Subgame</option>
            </select>
            <div class="input-group add-on form-control" style="float: right;">
               <input placeholder="Search Tag/Game Name" name="lobby_srch-term" id="lobby_srch-term" type="text"  onkeyup="lobby_game_search()" onkeydown="lobby_game_search()">
               <span class="glyphicon glyphicon-search form-control-feedback"></span>
            </div>
            <?php } ?>
         </div>
      </div>
   </div>
</div>
		<div class="row bidhottest hottestgame playstore_game_list_front">
      <ul>
         <?php // print_r($xbox_game);exit;
            if(count($lobby_list)>0) {
                echo $this->session->flashdata('err_lobby');
                unset($_SESSION['err_lobby']);
                foreach($lobby_list as $val) {
                  if($val['game'] == 1)
                  {
                     $game_type_image = 'XBOX.png';
                  } else if($val['game'] == 2) {
                     $game_type_image = 'PS4.png';
                  } else if($val['game'] == 3) {
                     $game_type_image = 'PC.png';
                  }else {
                     $game_type_image = '';
                  }
                ?>
         <li>
            <div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
               <img src="<?php echo base_url().'upload/game/'. $val['game_image'];?>" alt="">
            </div>
            <div class="col-lg-3 col-sm-6 col-md-3 bid-info">
               <h3><?php echo $val['game_name']; ?></h3>
               <p><?php echo $val['sub_game_name']; ?></p>
               <span class="postby">Tag: <?php echo $val['device_id']; ?></span> 
               <!--<span class="postby">By <?php echo $val['name']; ?></span>-->
            </div>
            <div class="col-lg-2 col-sm-3 col-md-2 uplod-day">
               <?php if($val['game_timer'] != '' && $val['game_timer'] != "0000-00-00 00:00:00"){ $diff = strtotime($val['game_timer'])- time(); if(time() < strtotime($val['game_timer'])){ ?>
               <?php } ?>
               <h3 class="timer custom_timer" data-seconds-left="<?php echo $diff; ?>"></h3>
               <?php } ?>
               <p> Gamesize: <?php echo "--".$val['game_type']."V".$val['game_type']."--";?></p>
               <p class="best_of"> <?php 
                  if ($val['best_of']!=null) {
                    echo 'Best of '.$val['best_of'];
                  }
                  ?>
            </div>
            <div class="col-lg-2 col-sm-4 col-md-2 divPrice">
               <div class="game-price premium">
                  <h3><?php $lobby_bet_price = number_format((float) $val['price'], 2 , '.', ''); echo CURRENCY_SYMBOL.$lobby_bet_price; ?></h3>
               </div>
            </div>
            <div class="col-lg-1 col-sm-4 col-md-2">
               <img src="<?php echo base_url().'upload/game/'. $game_type_image;?>">
            </div>
            <div class="col-lg-2 col-sm-5 col-md-3 primum-play play-n-rating">
               <a href="<?php echo base_url().'livelobby/watchLobby/'.$val['id'];?>" class="animated-box">Live</a>
            </div>
         </li>
         <?php } } else { echo "<h5 style='color: #FF5000;'> Lobby Not Available</h5>"; }?>
      </ul>
   </div>
		<div class="row pager-row">
			<div class="game_paging">
				<?php echo $paging_two; ?>
			</div>
		</div>
		
	</div>
</section>
