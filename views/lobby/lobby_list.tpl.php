<section class="body-middle innerpage">
   <div class="container">
      <!--Xbox Game Challange -->
      <div class="row header1" align="center">
         <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
         <div class="col-lg-4 col-sm-4 col-md-4">
            <h2>HOTTEST GAMES </h2>
            <h3 style="color: #ff5000;">Live Lobbies</h3>
         </div>
         <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
      </div>
      <?php if (count($lobby_list) > 0) { ?>
         <?php $this->load->view('common/live_lobby_filter.tpl.php'); ?>
         <div class="bidhottest hottestgame playstore_game_list_front">
            <ul>
               <?php 
               echo $this->session->flashdata('err_lobby');
               unset($_SESSION['err_lobby']);
               foreach($lobby_list as $val) {
                  $game_type_image = ($val['game'] == 1) ? 'XBOX.png' : (($val['game'] == 2) ? 'PS4.png': (($val['game'] == 3) ? 'PC.png' : '')); ?>
                  <li class="row">
                     <div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
                        <img src="<?php echo base_url().'upload/game/'. $val['game_image'];?>" alt="">
                     </div>
                     <div class="col-lg-3 col-sm-6 col-md-3 bid-info">
                        <h3><?php echo $val['game_name']; ?></h3>
                        <p><?php echo $val['sub_game_name']; ?></p>
                        <span class="postby">Tag: <?php echo $val['device_id']; ?></span> 
                        <!--<span class="postby">By <?php echo $val['name']; ?></span>-->
                     </div>
                     <div class="col-lg-2 col-sm-3 col-md-2 uplod-day">
                        <?php if($val['game_timer'] != '' && $val['game_timer'] != "0000-00-00 00:00:00"){ $diff = strtotime($val['game_timer'])- time(); if(time() < strtotime($val['game_timer'])){ ?>
                        <?php } ?>
                        <h3 class="timer custom_timer" data-seconds-left="<?php echo $diff; ?>"></h3>
                        <?php } ?>
                        <p> Gamesize: <?php echo "--".$val['game_type']."V".$val['game_type']."--";?></p>
                        <p class="best_of"> <?php echo ($val['best_of']!=null) ? 'Best of '.$val['best_of'] : ''; ?></p>
                        <?php echo (!empty($val['description']) && $val['description'] != null) ? '<div class="primum-play text-center margin-tpbtm-20 desc_sec_div"><a class="animated-box">Rules</a><div class="description" style="display:none;">'.$val['description'].'</div></div>' : ''; ?>
                     </div>
                     <div class="col-lg-2 col-sm-4 col-md-2 divPrice">
                        <div class="game-price premium">
                           <h3><?php $lobby_bet_price = number_format((float) $val['price'], 2 , '.', ''); echo CURRENCY_SYMBOL.$lobby_bet_price; ?></h3>
                        </div>
                     </div>
                     <div class="col-lg-1 col-sm-4 col-md-2">
                        <?php if ($game_type_image != '') { ?>
                           <img src="<?php echo base_url().'upload/game/'. $game_type_image;?>">
                        <?php } ?>
                     </div>
                     <?php $lobbylink =  ($val['is_event'] == 1) ? 'Live Event' : 'Live Stream'; ?>
                     <div class="col-lg-2 col-sm-5 col-md-3 primum-play play-n-rating">
                        <a href="<?php echo base_url().'livelobby/watchLobby/'.$val['id'];?>" class="animated-box"><?php echo $lobbylink; ?></a>
                     </div>
                  </li>
               <?php } ?>
            </ul>
         </div>
      <?php } else {
         echo "<h5 style='color: #FF5000;'> Lobby Not Available</h5>";
      } ?>
      <div class="row pager-row">
         <div class="game_paging">
            <?php echo $paging_two; ?>
         </div>
      </div>
      
   </div>
</section>
