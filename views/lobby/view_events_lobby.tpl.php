<section class="body-middle innerpage">
  <div class="container">
    <div class="row" align="center">
      <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
      <div class="col-lg-4 col-sm-4 col-md-4">
        <h2><?php echo $basic_details['lobby_details']['game_name']; ?></h2>
      </div>
      <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
    </div>
    <div class="row">
      <div class="col-lg-2"></div>
      <div class="col-lg-8 event_lobby"> 
        <?php
        if ((($this->session->userdata('total_balance') >= $basic_details['lobby_details']['event_price'] && $basic_details['lobby_details']['is_event'] == 1) && (($this->session->userdata('total_balance') > $basic_details['lobby_details']['spectate_price'] && $basic_details['lobby_details']['is_spectator'] == 1) || ($basic_details['lobby_details']['is_spectator'] == 0))) || ($this->session->userdata('total_balance') >= $basic_details['lobby_details']['spectate_price'] && $basic_details['lobby_details']['is_spectator'] == 1)) {
          // && $basic_details['lobby_details']['is_event'] == 0
          // ($this->session->userdata('total_balance') >= a$basic_details['lobby_details']['spectate_price'] && $basic_details['lobby_details']['is_spectator'] == 1)
          // if ($this->session->userdata('total_balance') >= $basic_details['lobby_details']['event_price'] && $basic_details['lobby_details']['is_event'] == 1) { ?>
          <form method="post" action="<?php echo base_url().'livelobby/addUserToEvent';?>">
            <input type="hidden" name="user_id" value="<?php echo $basic_details['user_id'];?>">
            <input type="hidden" name="lobby_id" value="<?php echo $basic_details['lobby_details']['id'];?>">
            <input type="hidden" name="lobby_creator" value="<?php echo $basic_details['lobby_details']['user_id'];?>">
            <input type="hidden" name="event_price" value="<?php echo $basic_details['lobby_details']['event_price'];?>">
            <input type="hidden" name="total_balance" value="<?php echo $this->session->userdata('total_balance');?>">
            <input type="hidden" id="is_gift_flag" name="is_gift" value="<?php echo $basic_details['is_gift'];?>">
            <input type="hidden" name="spectate_price" value="<?php echo $basic_details['lobby_details']['spectate_price'];?>">
            <input type="hidden" id="is_event"  value="<?php echo $basic_details['lobby_details']['is_event'];?>">
            <input type="hidden" id="is_spectator"  value="<?php echo $basic_details['lobby_details']['is_spectator'];?>">
            <?php if($basic_details['is_gift'] == 0) { ?>
              <h3 class="text-center">You are entering a live event to see a multi stream of different view points with different Celebrities/Models/Pro Streamers etc..</h3>
              <h3 class="text-center">You have Total Balance of $<?php echo number_format((float)$this->session->userdata('total_balance'), 2, '.', ''); ?></h3>
            <?php } else { ?>
              <h3 class="text-center">You have Total Balance of $<?php echo number_format((float)$this->session->userdata('total_balance'), 2, '.', ''); ?></h3>
              <h3 class="text-center">Gift Ticket to Friend</h3>
            <?php } ?>
            <div class="form-group">
              <div class="col-lg-12">
                <div class="col-lg-6" align="center">
                  <?php
                    // if ($this->session->userdata('total_balance') >= $basic_details['lobby_details']['spectate_price']) {
                    //   $is_spectator_check = 'checked';
                    //   $is_event_check = '';
                    // } else {
                    //   $is_event_check = 'checked';
                    //   $is_spectator_check = '';
                    // }
                  // $basic_details['lobby_details']['is_spectator'] == 1
                  if($basic_details['lobby_details']['is_event'] == 1 && $this->session->userdata('total_balance') >= $basic_details['lobby_details']['event_price']) {
                    $is_event_check = 'checked'; 
                    $is_spectator_check = '';
                  } else if ($basic_details['lobby_details']['is_spectator'] == 1) {
                    $is_spectator_check = 'checked';
                    $is_event_check = '';
                  } ?> 
                  <h4 class="text-center"><b><?php echo '$'.$basic_details['lobby_details']['event_price']; ?></b></h4>             
                  <h4><span class="<?php echo ($is_event_check != '') ? 'glow1' : '';?>"><b> Purchase Participate Ticket</b></span></h4>
                  <input type="radio" <?php echo $is_event_check; ?> value="1" name="is_event_flag" class="is_event_flag text-center">
                </div>
                <div class="col-lg-6" align="center"> 
                    <h4 class="text-center"><b><?php echo '$'.$basic_details['lobby_details']['spectate_price']; ?></b></h4>                  
                    <h4><span class="<?php echo ($is_spectator_check != '') ? 'glow1' : '';?>"> <b>Purchase Spectate Ticket</b></span></h4>
                    <input type="radio" <?php echo $is_spectator_check; ?> value="0" name="is_event_flag" class="is_event_flag">
                </div>
              </div>
              <div class="clearfix"></div><p id="flg_msg" class="text-center"></p>
            </div>
            <div class="form-group">
              <div class="col-lg-12 web_display_name" style="margin-bottom: 20px;"> 
                <div class="col-lg-3"></div>
                <div class="input-group col-lg-6 web_display_name text-center"> 
                  <?php if ($basic_details['is_gift'] == 1) { ?>
                    <h4 style="margin-top: 22px;" class="gift_title col-lg-<?php echo ($basic_details['is_gift'] == 0) ? '12':'5'; ?>"><i style="color:#FF5000;" class="fa fa-gift fa-2x" aria-hidden="true"></i>  Gift Ticket<label class="switch game_switch"><input  onclick="ShowHideDiv(this)" id="dvPassport" type="checkbox" name="gift_ticket_status" class="d-none"><span style="display:none;" class="gift_slider slider round"></span></label></h4>
                  <?php } ?>
                  <div class="<?php echo ($basic_details['is_gift'] == 0) ? 'col-lg-12':''; ?>">
                    <p class="<?php echo ($basic_details['is_gift'] == 0) ? 'col-lg-3':''; ?>"> </p>
                    <?php if($basic_details['is_gift'] == 0) { ?>
                      <input style="width:30%;margin-top: 20px;" class="gift_ticket_account col-lg-5 form-control none" id="gift_ticket_account" readonly="readonly" name="gift_ticket_account" type="text" class="col-lg-5 form-control" value="" >
                    <?php } else { ?>
                      <input style="width:30%;margin-top: 20px;" required placeholder="Enter Account No" name="gift_ticket_account" id="gift_ticket_account" type="text" class="col-lg-5 form-control" value="" >
                    <?php } ?>
                    <a onclick="checkAccount(<?php echo $basic_details['lobby_details']['id']; ?>);" class="col-lg-2 gift_button text-center animated-box orange <?php echo ($basic_details['is_gift'] == 0) ? 'none':'block'; ?>">Go</a>
                  </div>
                  <p class="message col-lg-12"></p>
                </div>
                <div class="col-lg-3"></div>
              </div>
              <input id="gift_to" type="hidden" name="gift_to">
            </div>
            <div class="second_phase <?php echo ($basic_details['is_gift'] == 0) ? 'block':'none'; ?>"> 
              <div class="col-lg-2"> </div>
              <div class="col-lg-8">
                <div class="form-group profile_image_div"></div>
                <div class="text-center <?php echo ($basic_details['is_gift'] == 0) ? 'none':'block'; ?>">
                  <p style="font-size: 18px;letter-spacing: 0.5px;">
                    Note: Please tell your friend their ticket is waiting for them in My Profile/Events Tab. 
                  </p>
                </div> 
                <div class="btn-group " style="margin:30px 0px;">
                  <button type="submit" id="event_btn" class="event_btn animated-box text-center" value="Submit">Confirm</button>
                </div>
              </div>
              <div class="col-lg-2"> </div>  
            </div>
          </form>
        <?php } else { ?>
          <h3 class="text-center">You have Insufficient Wallet Balance So Please Add Money in Your Wallet.</h3>
          <h3 class="text-center">You have Total Balance of $<?php echo $this->session->userdata('total_balance'); ?></h3>
          <h3 class="text-center">Live Events Require Amount of $<?php echo $basic_details['lobby_details']['event_price']; ?></h3>
          <div class="btn-group" style="margin:30px 0px;">
            <a href="<?php echo base_url('points/buypoints');?>" class="event_btn animated-box text-center">Add Money</a>
          </div>
        <?php }  ?>
      </div>
      <div class="col-lg-2"></div>
    </div>
  </div>
</section>
<style>
.glow {
  font-size: 21px;
  color: #fff;
  text-align: center;
  -webkit-animation: glow 1s ease-in-out infinite alternate;
  -moz-animation: glow 1s ease-in-out infinite alternate;
  animation: glow 1s ease-in-out infinite alternate;
}

@-webkit-keyframes glow {
  from {
    text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #FFD522, 0 0 40px #FFD522, 0 0 50px #FFD522, 0 0 60px #FFD522, 0 0 70px #FFD522;
  }
  to {
    text-shadow: 0 0 20px #fff, 0 0 30px #ff4da6, 0 0 40px #ff4da6, 0 0 50px #ff4da6, 0 0 60px #ff4da6, 0 0 70px #ff4da6, 0 0 80px #ff4da6;
  }
}
#flg_msg {
   font-size: 17px;
    color: red;
    letter-spacing: 1px;
}
</style>
<script type="text/javascript">

   function ShowHideDiv(chkPassport) 
   {
      var is_gift_flag = $('#is_gift_flag').val();
      var dvPassport = document.getElementById("dvPassport");
      if(chkPassport.checked){
         $('.gift_ticket_account').removeClass('none').addClass('block');
         $('.gift_button').removeClass('none').addClass('block');
      } else {
         $('.gift_ticket_account').removeClass('block').addClass('none');
         $('.gift_button').removeClass('block').addClass('none');
         if(is_gift_flag == 0){
            $('.user_div').remove();
            $('#feedback').remove();
            $('#event_btn').removeAttr('disabled');
            document.getElementById("event_btn").innerHTML = "Confirm";
         }
      }
   }
   $(function(){
    $('.is_event_flag').change(function(){
      var radioValue = $("input[name='is_event_flag']:checked").val();
      var is_spectator = $('#is_spectator').val();
      var is_event = $('#is_event').val();
      var total_balance = $('input[name="total_balance"]').val();
      var event_price = $('input[name="event_price"]').val();
      if(is_spectator == 1 && is_event == 1){
          // console.log(total_balance+'>='+event_price+'&&'+radioValue+'=='+1);
           // && radioValue == 1
        if (total_balance < event_price && radioValue == 1) {
          $('#flg_msg').html('Please select another ticket option.');
          document.getElementById("event_btn").disabled = true;
        } else {
          $('#flg_msg').html('');
          document.getElementById("event_btn").disabled = false;
        }
         // $('#flg_msg').html('');
         // document.getElementById("event_btn").disabled = false;
      } else if(is_spectator ==  0 && radioValue == 1 && is_event == 1){
        $('#flg_msg').html('');
        document.getElementById("event_btn").disabled = false;
      } else if (radioValue == 0 && is_spectator == 1 && is_event == 0){
        $('#flg_msg').html('');
        document.getElementById("event_btn").disabled = false;
      } else {
         $('#flg_msg').html('Please select another ticket option.');        
         document.getElementById("event_btn").disabled = true;          
      }
    });
});
</script>


