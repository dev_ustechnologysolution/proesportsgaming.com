<section class="body-middle innerPage padding-top-50">
  <?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
  <?php 
  $lobby_creat_fee = $this->General_model->view_all_data('admin_lobby_fee', 'id', 'asc');
  $price_add_decimal = number_format((float) $lobby_bet[0]['price'] + $lobby_creat_fee[0]['fee_per_game'], 2, '.', '');
  $lobby_joinfee =  number_format((float) ($lobby_bet[0]['price'] * $lobby_creat_fee[0]['fee_per_game']) / 100, 2, '.', '');
  $session_data   = array(
      'lobby_id'=>$lobby_bet[0]['lobby_id'],
      'lobby_price'=>$lobby_bet[0]['price']
  );
  $this->session->set_userdata($session_data);
  ?>
   <div class="container view_lobby_container">
      <div class="row">
        <div class="col-sm-12 live_stream_div">
          <?php
          $lobby_creator = $lobby_creator_det->user_id;
          $lby_is_spectator = $lobby_bet[0]['is_spectator'];
          $spectator_list = $wtnglist['spectator_list'];
          if ($lobby_creator == $_SESSION['user_id']) {
              $lbyctr = 1;
              echo "<input type='hidden' name='crtr' value='".$lobby_creator."'>";
          }
          $two_ch = $left_top_ch = $right_top_ch = [];
          foreach ($get_stream_channels as $key => $gtw) {
            $two_ch[] = $gtw->user_id;
            if ($key <= 2) {
              (strtolower($gtw->table_cat) == "left") ? $left_top_channel = $left_top_ch[0] = $gtw->user_id:'';
              (strtolower($gtw->table_cat) == "right") ? $right_top_channel = $right_top_ch[0] = $gtw->user_id:'';
            } 
            $streamer_chat_id_set = [];
            foreach ($_SESSION['stream_store'] as $key => $ss) {
              if (!in_array(array_merge($left_top_ch,$right_top_ch),$ss['streamer_chat_id_set'])) {
                (strtolower($gtw->table_cat) == "left") ? $left_top_ch[] = $ss['streamer_chat_id_set'] : $right_top_ch[] = $ss['streamer_chat_id_set'];
                $streamer_chat_id_set[] = $ss['streamer_chat_id_set'];
              }
            }
          }
          
          $everyone_readup = 'everyone_readup = "no"';
          if ($lobby_bet[0]['is_full_play'] == 'yes') {
            $everyone_readup = 'everyone_readup = "yes"';
          }
          $is_lefttable_full = $lobby_bet[0]['game_type'] == count($lobby_game_bet_left_grp) || $is_full_play == 'yes' ? 'yes' : 'no' ;
          $is_righttable_full = $lobby_bet[0]['game_type'] == count($lobby_game_bet_right_grp) || $is_full_play == 'yes' ? 'yes' : 'no' ;
          ?>
          <div class="row lobby_page_heading">
             <div class="col-sm-12">
                <h2 align="center" class="lobby_title" onclick="close_window(event);"><?php echo $lobby_bet[0]['lobby_name']; ?></h2>
                <h3 align="center"><?php echo $lobby_bet[0]['sub_game_name']; ?></h3>
                <div class="edit_lobby_det text-center padd10">
                  <?php if ($lbyctr == 1) { ?>
                    <a class="lobby_btn make_lobby_event" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Add Event to this Lobby" is_spectator="<?php echo $lobby_bet[0]['is_spectator']; ?>" is_event="<?php echo $lobby_bet[0]['is_event']; ?>" event_image="<?php echo $lobby_bet[0]['event_image']; ?>" event_price='<?php echo $lobby_bet[0]['event_price']; ?>' spectate_price='<?php echo $lobby_bet[0]['spectate_price']; ?>' event_title="<?php echo $lobby_bet[0]['event_title']; ?>" event_description="<?php echo $lobby_bet[0]['event_description']; ?>" event_start_date="<?php echo ($lobby_bet[0]['event_start_date'] !='') ? date('Y/m/d H:i a', strtotime($lobby_bet[0]['event_start_date'])) : date('Y/m/d H:i a'); ?>" event_end_date="<?php echo ($lobby_bet[0]['event_end_date'] != '') ? date('Y/m/d H:i a', strtotime($lobby_bet[0]['event_end_date'])) : date('Y/m/d H:i a'); ?>" already_member="<?php echo (count($wtnglist['get_my_plan']) == 1)? 'yes' : 'no'; ?>"> Make Event </a>
                    <?php if (count($get_play_status_lobby_bet) == 0) { ?>
                      <a class="lobby_btn change_lobby_dtil" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Edit Lobby Detail"> Edit Lobby </a>
                    <?php } ?>
                  <?php } ?>
                <?php echo (!empty($lobby_creator_det->description) && $lobby_creator_det->description != null) ? '<span class="desc_sec_div"><a class="lobby_btn">Rules</a><div class="description" style="display:none;">'.$lobby_creator_det->description.'</div></span>' : ''; ?>
                </div>
             </div>
          </div>
          <div class="row">
            <div class="col-sm-5 border-red pad-bot-20 streamer_list_box streamer_left_box">
              <div class="text-left col-sm-6 padd0 team_stream">
                <h4 class="title">Team one stream</h4>
                  <?php 
                  if (isset($wtnglist['leftstreamdetail']) && $wtnglist['leftstreamdetail'] !='') {
                    foreach ($wtnglist['leftstreamdetail'] as $key => $left_stream) {
                      $fan_tag = $this->General_model->view_single_row('lobby_fans', array(
                        'lobby_id' => $lobby_bet[0]['lobby_id'],
                        'user_id' => $left_stream->user_id
                      ),'fan_tag');
                      if ($left_stream->stream_status=="enable") {
                        $disable_check = '';
                        if ($left_stream->user_id == $left_top_channel) {
                          $disable_check = 'checked ';
                          if ($left_stream->user_id != $_SESSION['user_id']) {
                            $disable_check = 'checked class="disable_stream_checkbox" disabled readonly';
                          }
                        } else if (in_array($left_stream->user_id, $streamer_chat_id_set)) {
                            $disable_check = 'checked ';
                        }
                        ?>
                        <div class="streamer_item live_streamer_<?php echo $left_stream->user_id;?>" id="live_streamer_<?php echo $left_stream->user_id;?>">
                          <label for="leftdiv<?php echo $left_stream->user_id;?>">
                            <input type="checkbox" id="leftdiv<?php echo $left_stream->user_id;?>" stream-id="stream_channel_<?php echo $left_stream->user_id;?>" stream-channel_name="<?php echo $left_stream->stream_channel;?>" stream-streamer_name="<?php echo $fan_tag['fan_tag'];?>" <?php echo $disable_check; ?>>
                            <?php echo $fan_tag['fan_tag']; ?>
                          </label>
                        </div>
                        <?php
                      }
                    }
                  }
                  ?>
              </div>
              <div class="text-right col-sm-6 padd0 team_chat">
                <h4 class="title">Team one Chat</h4>
                <?php 
                if (isset($wtnglist['leftstreamdetail']) && $wtnglist['leftstreamdetail'] !='') {
                  foreach ($wtnglist['leftstreamdetail'] as $key => $left_stream) {
                    $fan_tag = $this->General_model->view_single_row('lobby_fans', array(
                      'lobby_id' => $lobby_bet[0]['lobby_id'],
                      'user_id' => $left_stream->user_id
                    ),'fan_tag');
                    if ($left_stream->stream_status == "enable") {
                      $left_checked ='';
                      if($left_stream->is_creator == $lobby_creator)  {
                        $left_checked = 'checked';
                      }
                      ?>
                      <div class="streamer_chat_item live_streamer_<?php echo $left_stream->user_id;?>" id="live_streamer_<?php echo $left_stream->user_id;?>">
                         <label for="leftdiv<?php echo $left_stream->user_id;?>">
                         <input <?php echo $left_checked; ?> type="checkbox" id="streamchat_<?php echo $left_stream->user_id; ?>" stream-channel_name="<?php echo $left_stream->stream_channel; ?>" stream-streamer_name="<?php echo $fan_tag['fan_tag'];?>" >
                         <?php echo 'Chat with '.$fan_tag['fan_tag']; ?>
                         </label>
                      </div>
                      <?php
                    }
                  }
                }
                ?>
              </div>
            </div>
            <div class="col-sm-2 text-center lobby_det">
              <img src="<?php echo base_url().'/upload/game/'.$lobby_bet[0]['lobby_image']; ?>" alt="Smiley face" height="90" width="100%">
              <h3 align="center" class="lby_size"><?php echo $lobby_bet[0]['game_type'].' vs '.$lobby_bet[0]['game_type']; ?></h3>
              <div class="send_tip_team">
                <a class="lobby_btn send_tips" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Donate to Esports Team"> Team Donate</a>
              </div>
            </div>
            <div class="col-sm-5 border-red pad-bot-20 streamer_list_box streamer_right_box">
              <div class="text-left col-sm-6 padd0 team_stream">
                <h4 class="title">Team two stream</h4>
                <?php
                if (isset($wtnglist['rightstreamdetail']) && $wtnglist['rightstreamdetail'] !='') {
                  foreach ($wtnglist['rightstreamdetail'] as $key => $right_stream) {
                    $fan_tag = $this->General_model->view_single_row('lobby_fans', array(
                      'lobby_id' => $lobby_bet[0]['lobby_id'],
                      'user_id' => $right_stream->user_id
                    ),'fan_tag');
                    if ($right_stream->stream_status == "enable") {
                      $disable_check = '';
                      if ($right_stream->user_id == $right_top_channel) {
                        $disable_check = 'checked ';
                        if ($right_stream->user_id != $_SESSION['user_id']) {
                          $disable_check = 'checked class="disable_stream_checkbox" disabled readonly';
                        }
                      } else if (in_array($right_stream->user_id, $streamer_chat_id_set)) {
                        $disable_check = 'checked ';
                      }
                      ?>
                      <div class="streamer_item text-left live_streamer_<?php echo $right_stream->user_id;?>" id="live_streamer_<?php echo $right_stream->user_id;?>">
                        <label for="rightdiv<?php echo $right_stream->user_id;?>">
                          <input type="checkbox" id="rightdiv<?php echo $right_stream->user_id;?>"  stream-id="stream_channel_<?php echo $right_stream->user_id;?>" stream-channel_name="<?php echo $right_stream->stream_channel;?>"/ stream-streamer_name="<?php echo $fan_tag['fan_tag'];?>" <?php echo $disable_check; ?>>
                          <?php echo $fan_tag['fan_tag'];?>
                        </label>
                      </div>
                      <?php
                    }
                  }
                } ?>
              </div>
              <div class="text-right col-sm-6 padd0 team_chat">
                <h4 class="title">Team two Chat</h4>
                <?php 
                if (isset($wtnglist['rightstreamdetail']) && $wtnglist['rightstreamdetail'] !='') {
                  foreach ($wtnglist['rightstreamdetail'] as $key => $right_stream) {
                    $fan_tag = $this->General_model->view_single_row('lobby_fans', array(
                      'lobby_id' => $lobby_bet[0]['lobby_id'],
                      'user_id' => $right_stream->user_id
                    ),'fan_tag');
                    if ($right_stream->stream_status == "enable") {
                      $right_checked ='';
                      if($right_stream->is_creator == $lobby_creator)  {
                        $right_checked = 'checked';
                      }
                      ?>
                      <div class="streamer_chat_item live_streamer_<?php echo $right_stream->user_id;?>" id="live_streamer_<?php echo $right_stream->user_id;?>">
                        <label for="rightdiv<?php echo $right_stream->user_id;?>">
                          <input <?php echo $right_checked; ?> type="checkbox" id="streamchat_<?php echo $right_stream->user_id;?>" stream-channel_name="<?php echo $right_stream->stream_channel;?>" stream-streamer_name="<?php echo $fan_tag['fan_tag'];?>">
                         <?php echo 'Chat with '.$fan_tag['fan_tag']; ?>
                        </label>
                      </div>
                      <?php
                    }
                  }
                } ?>
              </div>
            </div>
          </div>
          <br/><br/>
          <div class="row">
            <div class="col-sm-6 text-left streamer_channel">
              <?php
              if (isset($wtnglist['leftstreamdetail']) && $wtnglist['leftstreamdetail'] !='') {
                foreach ($wtnglist['leftstreamdetail'] as $key => $left_stream) {
                  if (in_array($left_stream->user_id,$left_top_ch)) { 
                    $streamername = $left_stream->display_name != '' ? $left_stream->display_name : $left_stream->name;
                    $fan_tag = $this->General_model->view_single_row('lobby_fans', array(
                       'lobby_id' => $lobby_bet[0]['lobby_id'],
                       'user_id' => $left_stream->user_id
                     ),'fan_tag');
                    $sndclass = "send_tip_btn";
                    $subsclass = "subscribe_button commonmodalclick"; 
                    if ($left_stream->user_id == $_SESSION['user_id']) {
                      $subsclass = 'not_allow';
                      $sndclass = "tip_disable";
                    }
                    if ($left_stream->stream_status=="enable") {
                      $subscribed_plan = '';
                      $payment_id = '';
                      foreach ($subscribedlist as $key => $sl) {
                        if ($sl->user_to == $left_stream->user_id) {
                          $subscribed_plan = $subscribedlist[0]->plan_detail;
                          $payment_id = $subscribedlist[0]->payment_id;
                        }
                      }
                      ?>
                      <div id="leftdiv<?php echo $left_stream->user_id; ?>stream" class="<?php echo $left_stream->user_id; ?>_live-stream stream_iframe">
                         <iframe src="https://player.twitch.tv/?channel=<?php echo $left_stream->stream_channel;?>" height="300" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
                         <div class="row streamer_channel_detail">
                            <div class="col-sm-4 text-left">
                              <a data-id="<?php echo $left_stream->user_id; ?>" class="lobby_btn social_follow <?php echo ($left_stream->user_id == $this->session->userdata('user_id')) ? 'disabled': ''?>"><?php echo $left_stream->is_followed.'<span class="cunt" style="margin-left: 5px;">'.(($followers_arr->followercount > 0) ? $followers_arr->followercount: '').'</span>'; ?></a>
                            </div>
                            <div class="col-sm-4 text-center">
                               <h3 align="center" class="live_lobby_h3"><?php echo $fan_tag['fan_tag']; ?></h3>
                            </div>
                            <div class="col-sm-4 text-right"><a class="lobby_btn <?php echo $subsclass; ?>" streamer_name='<?php echo $streamername; ?>' streamer_id="<?php echo $left_stream->user_id; ?>" streamer_img="<?php echo ($left_stream->image != '') ? $left_stream->image : $get_default_img ; ?>" plan_subscribed="<?php echo $subscribed_plan; ?>" payment_id="<?php echo $payment_id; ?>">Subscribe</a> <a class="lobby_btn <?php echo $sndclass; ?>" streamer_name='<?php echo $streamername; ?>' streamer_id="<?php echo $left_stream->user_id; ?>">TIP</a></div>
                         </div>
                      </div>
                      <?php
                    }
                  }
                }
              }
              ?>
            </div>
            <div class="col-sm-6 text-right streamer_channel">
              <?php 
              if (isset($wtnglist['rightstreamdetail']) && $wtnglist['rightstreamdetail'] !='') {
                foreach ($wtnglist['rightstreamdetail'] as $key => $right_stream) {
                  if (in_array($right_stream->user_id,$right_top_ch)) { 
                    $streamername = $right_stream->display_name != '' ? $right_stream->display_name : $right_stream->name;
                    $fan_tag = $this->General_model->view_single_row('lobby_fans', array(
                       'lobby_id' => $lobby_bet[0]['lobby_id'],
                       'user_id' => $right_stream->user_id
                     ),'fan_tag');
                    $sndclass = "send_tip_btn";
                    $subsclass = "subscribe_button commonmodalclick"; 
                    if ($right_stream->user_id == $_SESSION['user_id']) {
                      $subsclass = 'not_allow';
                      $sndclass = "tip_disable";
                    }
                    if ($right_stream->stream_status=="enable") {
                      $subscribed_plan = '';
                      $payment_id = '';
                      foreach ($subscribedlist as $key => $sl) {
                        if ($sl->user_to == $right_stream->user_id) {
                          $subscribed_plan = $subscribedlist[0]->plan_detail;
                          $payment_id = $subscribedlist[0]->payment_id;
                        }
                      }
                      ?>
                      <div id="rightdiv<?php echo $right_stream->user_id; ?>stream" class="<?php echo $right_stream->user_id; ?>_live-stream stream_iframe">
                        <iframe src="https://player.twitch.tv/?channel=<?php echo $right_stream->stream_channel;?>" height="300" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
                        <div class="row streamer_channel_detail">
                          <div class="col-sm-4 text-left">
                            <a data-id="<?php echo $right_stream->user_id; ?>" class="lobby_btn social_follow <?php echo ($right_stream->user_id == $this->session->userdata('user_id')) ? 'disabled': ''?>"><?php echo $right_stream->is_followed.'<span class="cunt" style="margin-left: 5px;">'.(($followers_arr->followercount > 0) ? $followers_arr->followercount: '').'</span>'; ?></a>
                          </div>
                          <div class="col-sm-4 text-center">
                             <h3 align="center" class="live_lobby_h3"><?php echo $fan_tag['fan_tag']; ?></h3>
                          </div>
                          <div class="col-sm-4 text-right"><a class="lobby_btn <?php echo $subsclass; ?>" streamer_name='<?php echo $streamername; ?>' streamer_id="<?php echo $right_stream->user_id; ?>" streamer_img="<?php echo ($right_stream->image != '') ? $right_stream->image : $get_default_img ; ?>" plan_subscribed="<?php echo $subscribed_plan; ?>" payment_id="<?php echo $payment_id; ?>">Subscribe</a> <a class="lobby_btn <?php echo $sndclass; ?>" streamer_name='<?php echo $streamername; ?>' streamer_id="<?php echo $right_stream->user_id; ?>">TIP</a></div>
                        </div>
                      </div>
                      <?php
                    }
                  }
                }
              }
              ?>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <?php 
          if($defult_chat->stream_channel != ''){ 
            $defult_fan_tag = $this->General_model->view_single_row('lobby_fans', array(
              'lobby_id' => $defult_chat->lobby_id,
              'user_id' => $defult_chat->user_id
            ),'fan_tag');?>
            <div class="chatstreamer" t_streamerchat="<?php echo $defult_chat->id; ?>">
              <h3 align="center" class="chat_open_with_person">Chat open with <?php echo $defult_fan_tag['fan_tag']; ?></h3>
              <br>
              <br>
              <div class="streamer_chat_iframe">
                <iframe src="https://www.twitch.tv/embed/<?php echo $defult_chat->stream_channel; ?>/chat/" height="375" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
                </div>
            </div>
          <?php } else if (isset($_SESSION['streamer_name_set']) && $_SESSION['streamer_name_set'] !='' && isset($_SESSION['streamer_chat_id_set']) && $_SESSION['streamer_chat_id_set'] !='' && isset($_SESSION['streamer_channel_set']) && $_SESSION['streamer_channel_set'] !='' && $_SESSION['lobby_id'] == $lobby_bet[0]['lobby_id'] && $this->uri->segment(3) == $_SESSION['lobby_id']) { 
            $chat_checker = $this->uri->segment(3);
            $check_chat_arr_in = array_column($_SESSION['defult_chat_arr'],'lobby_id');
            
            if(in_array($chat_checker,$check_chat_arr_in)) { ?>
            <div class="chatstreamer" t_streamerchat="<?php echo $_SESSION['streamer_chat_id_set']; ?>">
              <h3 align="center" class="chat_open_with_person">Chat open with <?php echo $_SESSION['streamer_name_set']; ?></h3>
              <br>
              <br>
              <div class="streamer_chat_iframe">
                <iframe src="https://www.twitch.tv/embed/<?php echo $_SESSION['streamer_channel_set']; ?>/chat/" height="375" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
                </div>
              </div>
          <?php } 
          } else { ?>
          <div class='stream_chat_img'>
           <img src="<?php echo base_url(); ?>assets/frontend/images/stream_chat_box.png" alt="Smiley face">
          </div>
          <div class="chatstreamer"></div>
          <?php } ?>
        </div>
        <div class="col-sm-6">
            <div class="row chat-box_fronside live_lobby_chatbox_main_row">
               <div class="col-md-12 col-sm-12 col-xs-12 " align="center" id="live_lobby_msgbox">
                  <div id="message-container" class="xwb-message-container">
                     <div class="popup-content-wrapper_customer_fontside lobby_group_msg_bx">
                        <div class="row">
                          <div class="col-sm-12">
                            <h3 align="center" class="pad-bot-20 live_lobby_h3">Chat in the Pro Esports lobby</h3>
                            <br/><br/>
                            <div id="message-inner" class="message-inner">
                              <?php if(isset($lobby_chat) && count($lobby_chat) > 0) {
                                foreach ($lobby_chat as $key => $value) {
                                  $direction = 'left';
                                  $download_attr = 'download';
                                  $class = 'enable';
                                  if ($value->user_id == $_SESSION['user_id']) {
                                    $direction = 'right';
                                    $download_attr = '';
                                    $class = 'disable';
                                  }
                                  $msg = $value->message;
                                  if ($value->message_type == 'file') {
                                    $msg = '<a href="'.base_url().'upload/lobby_pics_msg/'.$value->attachment.'" class="download_attachment lobby_grpmsg_attach_link '.$class.'" '.$download_attr.'><i class="fa fa-download '.$class.'"></i><img class="attachment_image img img-thumbnail" src="'.base_url().'upload/lobby_pics_msg/'.$value->attachment.'" class="lobby_grpmsg_attach" alt="pic" width="100px"></a>';
                                  } else if ($value->message_type == 'tipmessage') {
                                    $tipicn = '<img src="'.base_url().'upload/stream_setting/'.$value->attachment.'" alt="tipimg" class="tipiconimg">'; 
                                    if ($value->user_id == $_SESSION['user_id']) {
                                      $msg = $tipicn.$value->message;
                                    } else {
                                      $msg = $value->message.$tipicn;
                                    }
                                  }
                                  ?>
                                  <div data-mid="<?php echo $value->message_id;?>" data-cid="<?php echo $value->id;?>" class="message-row msg_usr_<?php echo $value->user_id; ?>">
                                    <div class="col-md-12 col-sm-12 col-xs-12 message-box <?php echo ($lbyctr || $current_is_admin == 1) ? 'del_opt' : ''; ?>" tgcontmenu='<?php echo $value->message_id;?>'>
                                      <div class="message_header">
                                        <?php
                                        $user_data = $this->General_model->view_single_row('user_detail','user_id',$value->user_id);
                                        if ($value->is_admin == 1) { ?>
                                          <img class="chat_image pull-<?php echo $direction;?>" width="20" height="20" src="<?php echo base_url().'upload/admin_dashboard/Chevron_3_Twitch_72x72.png'; ?>">
                                        <?php } ?>
                                        <small class="list-group-item-heading text-muted text-primary pull-<?php echo $direction;?>"><?php $fan_tag = $this->General_model->view_single_row('lobby_fans', array('lobby_id' => $lobby_bet[0]['lobby_id'],'user_id' => $value->user_id),'fan_tag');
                                            echo $fan_tag['fan_tag']; ?></small>
                                      </div>
                                      <div class="message_row_container in pull-<?php echo $direction;?>">
                                        <p class="list-group-item-text"></p>
                                        <p><?php echo $msg;?></p>
                                        <?php if ($lbyctr || $current_is_admin == 1) { ?>
                                          <div class="dropdown clearfix contmenu_<?php echo $value->message_id;?> contextMenu">
                                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu" style="display:block;">
                                              <li>
                                                <a class="remove_msg" onclick="send_msg_delete(<?php echo $value->message_id ;?>,<?php echo $value->user_id ;?>);">Remove Msg</a>
                                              </li>
                                              <?php if ($direction == 'left' ) { ?>
                                                <li>
                                                  <a class="remove_user" onclick="del_usr_from_chat(<?php echo $value->user_id ;?>);">Remove User</a>
                                                </li>
                                                <li>
                                                  <a class="mute_user" onclick="mute_usr_from_chat(<?php echo $value->user_id ;?>);">Time Out User</a>
                                                </li>
                                              <?php } ?>
                                            </ul>
                                          </div>
                                        <?php } ?>
                                        <p></p>
                                       </div>
                                    </div>
                                  </div>
                                <?php }
                              } else { ?>
                                <div class="no_messages">
                                  <h3 class="text-center no-more-messages">No Messages Available</h3>
                                </div>
                              <?php } ?>
                            </div>
                          </div>
                        </div>
                        <?php if($get_my_fan_tag->fan_status == 1) {
                          $disable_class = '';
                          $disable_attr = '';
                          if($get_my_fan_tag->mute_time != ''){
                              $mutediff = strtotime($get_my_fan_tag->mute_time) - time();
                              if(time() < strtotime($get_my_fan_tag->mute_time)){
                                $disable_class = 'disable';
                                $disable_attr = 'readonly';
                                echo '<div class="mute_div mutetimer_div">You can chat after <span class="mutetimer" data-seconds-left="'.$mutediff.'"></span> Seconds</div>';
                              } else {
                                echo '<div class="mute_div"></div>';
                              }
                          }
                        ?>
                        <div class="msg-input input-group col-sm-12 lobby_grp_msgbox lobbymsgbox_userid_<?php echo $_SESSION['user_id']; ?> <?php echo $disable_class; ?>">
                          <div class="ms_snd_opt">
                            <div id="lobby_grp_msgbox_message-input" class="form-control message-input message-input-frontend-side lobby_grp_msgbox_message-input attach_emojioneicon" rows="4" name="message-input" contenteditable="true"></div>
                            <div id="emojionearea_display_div"></div>
                          </div>
                          <div class="chat_box_button_area">
                            <div class="attachfile_div">
                              <div class="input-group-btn">
                                <div class="attachfile btn-file">
                                  <label for="file-input">
                                    <i class="fa fa-paperclip" aria-hidden="true"></i>
                                  </label>
                                  <input type="file" class="file-input hide" name="attachfile" id="file-input" accept="image/png, image/PNG, image/jpeg, image/jpg, image/JPEG, image/JPG" data-rule-required="true" data-msg-accept="Your image formate should be in .jpg, .jpeg or .png">
                                  <div class="filename-container hide"></div>
                                </div>
                              </div>
                            </div>
                            <div class="message_send_div">
                              <div class="input-group-btn">
                                <button class="btn btn-success" id="send-message-to-lobby_group">Send</button>
                              </div>
                            </div>
                          </div>
                          <div class="play_sound">
                          <?php if (isset($_SESSION['getmysub_sound']) && $_SESSION['getmysub_sound'] != '') { ?>
                          <audio controls autoplay><source src="<?php echo base_url(); ?>upload/stream_setting/<?php echo $_SESSION['getmysub_sound']; ?>"></audio>
                          <?php } unset($_SESSION['getmysub_sound']); ?>
                          </div>
                        </div>
                        <?php } ?>
                     </div>
                  </div>
               </div>
            </div>
        </div>
      </div>
      <div class="row live_lby_secrow">
        <?php
          $fan_tagarr = '';
          foreach ($fanscount as $key => $fc) {
            $fan_tagarr .= '"'.$fc['fan_tag'].'" , ';
          }
        ?>
        <div class="col-sm-2 text-left" style="font-size: 20px;">
          <?php
          $lobby_bet_price = number_format((float) $lobby_bet[0]['price'], 2 , '.', '');
          echo '$'.$lobby_bet_price. ' + $'.$lobby_joinfee.' fee'; ?> 
        </div>
        <div class="col-sm-1 text-center padd_bot_5 padd0">
          <a href="<?php echo base_url().'livelobby/view_fanslist/'.$lobby_bet[0]['lobby_id']; ?>" class="lobby_btn fans_btn" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Click to view Profiles"> Fans <?php echo (isset($fanscount)) ? count($fanscount) : '0'; ?></a>
        </div>
        <div class="col-sm-6 text-center crtrdiv">
          <!-- <div class="llby_crtr"> -->
            <?php 
            if ($lobby_creator == $_SESSION['user_id'] ) { ?>
              <img src="<?php echo base_url(); ?>assets/frontend/images/creator.png" width="30">
              <div class="is_draggable_div"><span>Drop here to Pass Controller</span><div class="drp_lbycrtr"></div></div>
            <?php } ?>
          <!-- </div> -->
        </div>
         <div class="col-sm-1"></div>
         <div class="col-sm-2 text-right" style="font-size: 20px;">
          <?php echo '$'.$lobby_bet_price. ' + $'.$lobby_joinfee.' fee'; ?>
         </div>
      </div>
      <div class="row live_lby_thirow dragblearea">
        <div class="row live_lby_in_thirow">
          <div class="row">
             <div class="col-sm-1"></div>
             <div class="col-sm-3 select_head text-center">
              <?php if ($lbyctr) { ?>
                <select class="form-control select_head_left" name="select_head" table_cat="left">
                  <option value="0">SELECT HEAD</option>
                  <?php foreach ($lobby_game_bet_left_grp as $key => $lgblg) {
                    $fan_tag = $this->General_model->get_my_fan_tag($lobby_bet[0]['lobby_id'],$lgblg['user_id']);
                    $selected = '';
                    if ($lgblg['is_controller'] == 'yes') {
                      $selected = 'selected';
                    }
                    echo '<option value="'.$lgblg['user_id'].'" class="assign_table_ctrl" id="'.$lgblg['user_id'].'" '.$selected.'>'.$fan_tag->fan_tag.'</option>';
                  } ?>
                </select>
              <?php } ?>
             </div>
             <div class="col-sm-1"></div>
             <div class="col-sm-2 text-center">
                <div class="live_lby_ready_timer">
                  <div>Ready</div>
                  <?php
                  if(isset($get_freport) && $get_freport->updated_at != '' && $get_freport->updated_at != "0000-00-00 00:00:00") {
                    $diff = strtotime($get_freport->updated_at) - time() + rand(0,10);
                    if(time() < strtotime($get_freport->updated_at)) {
                      echo '<div class="reporttimer" data-seconds-left="'.$diff.'"></div>';
                    } else { ?>
                      <script type="text/javascript">
                        var lobby_id, group_bet_id;
                        group_bet_id = <?php echo $lobby_bet[0]['id']; ?>;
                        lobby_id = <?php echo $lobby_bet[0]['lobby_id']; ?>;
                        var postData = {
                          'group_bet_id': group_bet_id,
                          'lobby_id':lobby_id
                        };
                        $.ajax({
                          type: 'post',
                          url: '<?php echo base_url(); ?>livelobby/report_timer_expire',
                          data: postData,
                          success: function(res){
                            var res = $.parseJSON(res);
                            window.location.href = '<?php echo base_url(); ?>livelobby/watchLobby/'+res.lobby_id;
                          }
                        });
                      </script>
                      <?php
                    }
                  }
                  ?>
                </div>
             </div>
             <div class="col-sm-1"></div>
             <div class="col-sm-3 select_head text-center">
              <?php if ($lbyctr) { ?>
                <select class="form-control select_head_right" name="select_head" table_cat="right">
                  <option value="0">SELECT HEAD</option>
                  <?php foreach ($lobby_game_bet_right_grp as $key => $lgbrg) {
                    $fan_tag = $this->General_model->get_my_fan_tag($lobby_bet[0]['lobby_id'],$lgbrg['user_id']);
                    $selected = '';
                    if ($lgbrg['is_controller'] == 'yes') {
                      $selected = 'selected';
                    }
                    echo '<option value="'.$lgbrg['user_id'].'" class="assign_table_ctrl" id="'.$lgbrg['user_id'].'" '.$selected.'>'.$fan_tag->fan_tag.'</option>';
                  } ?>
                </select>
              <?php } ?>
             </div>
             <div class="col-sm-1"></div>
          </div>
          <div>
            <div class="col-sm-6 left_table_first_div <?php echo ($is_lefttable_full == 'no') ? 'left_table_droppable ' : '' ; echo ($is_full_play != 'yes' && $lbyctr) ? 'is_draggable' : '' ; ?>" droppable_table="LEFT">
              <?php if (isset($lobby_game_bet_left_grp)) {
                foreach ($lobby_game_bet_left_grp as $key => $left_row) {
                  $fan_tag = $this->General_model->view_single_row('lobby_fans', array(
                    'lobby_id' => $lobby_bet[0]['lobby_id'],
                    'user_id' => $left_row['user_id']
                  ),'fan_tag'); ?>
                  <div class="left_table text-left group_user_row_<?php echo $left_row['user_id'] ?>">
                    <?php
                    $fantag = '<span is_fan="true">'.$fan_tag['fan_tag'].'</span>';
                    if ($lobby_creator == $_SESSION['user_id'] ) {
                      echo '<a class="leave_lobby" self="yes" data-toggle="tooltip" data-placement="bottom" player_tag_name="'.$fan_tag['fan_tag'].'" lby_crtr="'.$left_row['is_creator'].'" lby_cntrlr="'.$left_row['is_controller'].'" title="Leave Live Lobby" usr_id="'.$left_row['user_id'].'" '.$everyone_readup.'><i class="fa fa-times"></i></a> ';
                      if ($left_row['user_id'] != $_SESSION['user_id']) {
                        if (count($get_play_status_lobby_bet) != count($lobby_game_bet_left_grp)+count($lobby_game_bet_right_grp)) {
                          $fantag = '<span class="pasctrl" id="'.$left_row['user_id'].'" fan_tag="'.$fan_tag['fan_tag'].'" is_fan="true">'.$fan_tag['fan_tag'].'</span>';
                        }
                      }
                      echo $fantag;
                    } else if ($left_row['user_id'] == $_SESSION['user_id'] && $lobby_creator != $_SESSION['user_id']) {
                      echo '<a class="leave_lobby" self="no" data-toggle="tooltip" player_tag_name="'.$fan_tag['fan_tag'].'" data-placement="bottom" lby_crtr="'.$left_row['is_creator'].'" lby_cntrlr="'.$left_row['is_controller'].'" title="Leave Live Lobby" usr_id="'.$left_row['user_id'].'" '.$everyone_readup.'><i class="fa fa-times"></i></a> ';
                      $fantag = '<span is_fan="true">'.$fan_tag['fan_tag'].'</span>';
                      echo $fantag;
                    } else {
                      echo $fantag;
                    }
                    $left_status_class = "disabled";
                    echo (in_array($left_row['user_id'], $spectator_list) && $lby_is_spectator == 1)?'<span class="is_spectator_span"><i class="fa fa-eye"></i></span>':'';
                    if ($left_row['user_id'] == $_SESSION['user_id']) {
                      $left_status_class = "stream_icon enabled";
                      $left_tooltip = 'data-toggle="tooltip" data-html="true" data-placement="bottom" title="Click to Start. <br/> Live Lobby Stream"';
                      if ($left_row['stream_status'] == 'enable') {
                        $left_tooltip = 'data-toggle="tooltip" data-html="true" data-placement="bottom" title="Click to Stop. <br/> Live Lobby Stream"';
                      }
                    }
                    $left_stream_click_status = 'stream_click_status="enable"';
                    $left_img_src = base_url().'assets/frontend/images/live_stream_off.png';

                    if ($left_row['stream_status'] == 'enable') {
                      $left_img_src = base_url().'assets/frontend/images/live_stream_on.png';
                      $left_stream_click_status = 'stream_click_status="disable"';
                    }

                    echo ' <span class="tooltip_div"><img src="'.$left_img_src.'" class="'.$left_status_class.'" alt="live_stream" id="stream_'.$left_row['user_id'].'" '.$left_tooltip.' '.$left_stream_click_status.'></span>';
                    $left_team_leader = base_url().'assets/frontend/images/table_leader_new.png ';
                    echo ' <span class="team_leader" id="teamhead_'.$left_row['user_id'].'">';

                    if ($left_row['is_controller'] == 'yes') {
                      echo '<img src="'.$left_team_leader.'" alt="Team Controller">';
                    }
                    echo '</span> ';

                    if($left_row['reported'] == 0) {
                      if ($lobby_bet[0]['game_type']*2 == count($lobby_game_bet_left_grp)+count($lobby_game_bet_right_grp) || $is_full_play == 'yes') {
                        if ($left_row['user_id'] == $_SESSION['user_id']){
                          if ($check_bet_reported_for_me[0]['play_status'] == 1) {
                            echo '<label class="checkbox_container disabled"><input type="checkbox" checked disabled data-user="'.$left_row["user_id"].'" class="text-right"><span class="checkmark"></span></label>';
                            if (count($get_play_status_lobby_bet) == count($lobby_game_bet_left_grp)+count($lobby_game_bet_right_grp) || $is_full_play == 'yes') {
                              if ($left_row['is_controller'] == 'yes') {
                                echo '<button class="bet_report button text-right">Report</button>';
                              }
                            }
                          } else {
                            echo '<label class="checkbox_container"><input type="checkbox" data-user="'.$left_row["user_id"].'" name="play_game" class="text-right play_game"><span class="checkmark"></span></label>';
                          }
                        } else {
                          if ($left_row['play_status'] == 1) {
                            echo '<label class="checkbox_container disabled"><input type="checkbox" checked disabled data-user="'.$left_row["user_id"].'" class="text-right"><span class="checkmark"></span></label>';
                          } else {
                            echo '<label class="checkbox_container disabled"><input type="checkbox" disabled class="text-right"><span class="checkmark op_bg"></span></label>';
                          }
                        }
                      }
                    } else {
                      echo '<button class="button text-right bet_reported">Reported <i class="fa fa-check"></i></button><label class="checkbox_container disabled"><input type="checkbox" checked="" disabled="" class="text-right" data-user="'.$left_row["user_id"].'"><span class="checkmark"></span></label>'; 
                    } ?>
                  </div>
                <?php }
                } ?>
            </div>
            <div class="col-sm-6 border-right right_table_first_div <?php echo ($is_righttable_full == 'no') ? 'right_table_droppable ' : '' ;  echo ($is_full_play != 'yes' && $lbyctr) ? 'is_draggable' : '' ; ?>" droppable_table="RIGHT">
              <?php if (isset($lobby_game_bet_right_grp)) {
                foreach ($lobby_game_bet_right_grp as $key => $right_row) { 
                  $fan_tag = $this->General_model->view_single_row('lobby_fans', array(
                    'lobby_id' => $lobby_bet[0]['lobby_id'],
                    'user_id' => $right_row['user_id']
                  ),'fan_tag');
                  ?>
                  <div class="right_table text-right group_user_row_<?php echo $right_row['user_id'] ?>">
                    <?php
                    if($right_row['reported'] == 0) {
                      if ($lobby_bet[0]['game_type']*2 == count($lobby_game_bet_left_grp)+count($lobby_game_bet_right_grp) || $is_full_play == 'yes') {
                        if ($right_row['user_id'] == $_SESSION['user_id']) {
                          if ($check_bet_reported_for_me[0]['play_status'] == 1) {
                            echo '<label class="checkbox_container disabled"><input type="checkbox" checked disabled class="text-left" data-user="'.$right_row["user_id"].'"><span class="checkmark"></span></label>';
                            if (count($get_play_status_lobby_bet) == count($lobby_game_bet_left_grp)+count($lobby_game_bet_right_grp) || $is_full_play == 'yes') {
                              if ($right_row['is_controller'] == 'yes') {
                                echo '<button class="button text-left bet_report">Report</button>';
                              }
                            }
                          } else {
                            echo '<label class="checkbox_container"><input type="checkbox" name="play_game" class="text-left play_game" data-user="'.$right_row["user_id"].'"><span class="checkmark"></span></label>'; 
                          }
                        } else {
                          if ($right_row['play_status'] == 1) {
                            echo '<label class="checkbox_container disabled"><input type="checkbox" checked disabled data-user="'.$right_row["user_id"].'" class="text-left"><span class="checkmark"></span></label>'; 
                          } else {
                            echo '<label class="checkbox_container disabled"><input type="checkbox" disabled class="text-left"><span class="checkmark op_bg"></span></label>';
                          }
                        }
                      }
                    } else {
                      echo '<label class="checkbox_container disabled"><input type="checkbox" checked="" disabled="" class="text-left" data-user="'.$right_row["user_id"].'"><span class="checkmark"></span></label><button class="button text-left bet_reported">Reported <i class="fa fa-check"></i></button>';
                    }
                    $right_status_class = "disabled";
                    if ($right_row['user_id'] == $_SESSION['user_id']) {
                      $right_status_class = "stream_icon enabled";
                      $right_tooltip = 'data-toggle="tooltip" data-html="true" data-placement="bottom" title="Click to Start. <br/> Live Lobby Stream"';
                      if ($right_row['stream_status'] == 'enable') {
                        $right_tooltip = 'data-toggle="tooltip" data-html="true" data-placement="bottom" title="Click to Stop. <br/> Live Lobby Stream"'; 
                      }
                    }
                    $right_stream_click_status = 'stream_click_status="enable"';
                    $right_img_src = base_url().'assets/frontend/images/live_stream_off.png ';
                    $right_team_leader = base_url().'assets/frontend/images/table_leader_new.png ';
                    if ($right_row['stream_status'] == 'enable') {
                      $right_img_src = base_url().'assets/frontend/images/live_stream_on.png';
                      $right_stream_click_status = 'stream_click_status="disable"';
                    }
                    echo ' <span class="team_leader" id="teamhead_'.$right_row['user_id'].'">';
                    if ($right_row['is_controller'] == 'yes') {
                      echo '<img src="'.$right_team_leader.'" alt="Team Controller">';
                    }
                    echo '</span> ';
                    echo ' <span class="tooltip_div"><img src="'.$right_img_src.'" class="'.$right_status_class.'" alt="live_stream" id="stream_'.$right_row['user_id'].'" '.$right_tooltip.' '.$right_stream_click_status.'></span> ';
                    echo (in_array($right_row['user_id'], $spectator_list) && $lby_is_spectator == 1)?'<span class="is_spectator_span"><i class="fa fa-eye"></i></span>':'';
                    $fantag = '<span is_fan="true">'.$fan_tag['fan_tag'].'</span>'; 
                    if ($lobby_creator == $_SESSION['user_id']) {
                      if ($right_row['user_id'] != $_SESSION['user_id']) {
                        if (count($get_play_status_lobby_bet) != count($lobby_game_bet_left_grp)+count($lobby_game_bet_right_grp)) {
                          $fantag = '<span class="pasctrl" id="'.$right_row['user_id'].'" fan_tag="'.$fan_tag['fan_tag'].'" is_fan="true">'.$fan_tag['fan_tag'].'</span>';
                        }
                      }
                      echo $fantag;
                      echo ' <a class="leave_lobby" self="yes" data-toggle="tooltip" player_tag_name="'.$fan_tag['fan_tag'].'" data-placement="bottom" title="Leave Live Lobby" lby_crtr="'.$right_row['is_creator'].'" lby_cntrlr="'.$right_row['is_controller'].'" usr_id="'.$right_row['user_id'].'" '.$everyone_readup.'><i class="fa fa-times"></i> </a>';
                      } else if ($right_row['user_id'] == $_SESSION['user_id'] && $lobby_creator != $_SESSION['user_id']) {
                        echo $fantag;
                        echo ' <a class="leave_lobby" self="no" player_tag_name="'.$fan_tag['fan_tag'].'" data-toggle="tooltip" data-placement="bottom" title="Leave Live Lobby" lby_crtr="'.$right_row['is_creator'].'" lby_cntrlr="'.$right_row['is_controller'].'" usr_id="'.$right_row['user_id'].'" '.$everyone_readup.'><i class="fa fa-times"></i> </a>'; 
                      } else {
                        echo $fantag;
                      } ?>
                  </div>
                <?php }
              } ?>
            </div>
          </div>
          <div class="lobby_tables_bottom">
            <div class="col-sm-2"></div>
            <div class="col-sm-3"></div>
            <div class="col-sm-2 border-right border-left border-top border_radious_4px">
              <div class="text-center join_lby_game_btn_div">
                <div>
                  <?php if (empty($get_grp_for_me)) { 
                    echo ($is_full_play == 'no' && $this->session->userdata('is_18') == 1) ? '<lable class="join_lby_game_btn join_lobby_btn">Join Table</lable>' : ''; 
                  } else { ?>
                    <lable class="join_lobby_btn already_joined">Joined <i class="fa fa-check"></i></lable>
                  <?php } ?>
                </div>
                <div><label><?php echo $lobby_bet[0]['game_type'].'v'.$lobby_bet[0]['game_type']; ?></label></div>
                <?php if ($lobby_creator == $_SESSION['user_id']) {
                  if ($is_full_play == 'no') { ?>
                    <div class="change_game_size_div">
                      <label>Change Game Size</label>
                      <div align="center">
                        <select class="form-control" name="change_game_size">|
                          <option value="">Please select a Gamesize</option>
                          <?php for ($i = 1; $i <= 16; $i++) { ?>
                            <option value="<?php echo $i;?>" class="playstore_gamesize_option" <?php if ($lobby_bet[0]['game_type'] == $i) { echo 'selected'; } ?>>
                              <?php echo '--' . $i . 'V' . $i . '--'; ?>
                            </option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                  <?php }
                } ?>
               <div class="edit_lobby_tag_div">
                  <div align="center">
                    <a class="live_lobby_btn edit_lobby_tag">Edit Player Tag</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-3"></div>
            <div class="col-sm-2"></div>
          </div>
          <input type="hidden" id="lobby_id" value="<?php echo $lobby_bet[0]['lobby_id']; ?>">
          <input type="hidden" id="group_bet_id" value="<?php echo $lobby_bet[0]['id']; ?>">
        </div>
      </div>
      <div class="row coin_flip_div paddtop20">
        <div id="coin">
          <div class="flip_side side-a">
            <img src="<?php echo base_url(); ?>assets/frontend/images/head.png" alt="head" class="head img img-responsive">
          </div>
          <div class="flip_side side-b">
            <img src="<?php echo base_url(); ?>assets/frontend/images/tail.png" alt="teil" class="teil img img-responsive">
          </div>
        </div>
        <h4 style="text-align: center; color: #ff5000; padding-top: 2px;">COIN TOSS</h4>
      </div>
      <div class="row dragblearea">
        <div class="col-sm-12 live_lby_waiting_list">
          <div class="col-sm-6 left_wtable">
            <div class="join_wtnglist_btn row">
              <a class="btn lobby_btn pull-left join_wtng_btn" data-toggle="tooltip" data-html="true" data-placement="right" data-original-title="Click to Join Left waiting table" tg_table="LEFT" <?php echo (!in_array($_SESSION['user_id'],$wtnglist['wlfanarr']))? 'style="display:block"':'style="display:none"'; ?>> Join Waiting List </a>
            </div>  
            <div class="text-left wtnglstlistdiv left_tbl_wtnglst" wtnglst_table="LEFT">
              <h3> Left table Waiting list </h3>
              <?php foreach ($wtnglist['leftwaitinglist'] as $key => $lwl) { ?>
              <div class="waitingusr" id="<?php echo $lwl->fan_id; ?>" lobby_id="<?php echo $lwl->lobby_id; ?>" fan_tag="<?php echo $lwl->fan_tag; ?>" user_id="<?php echo $lwl->user_id; ?>" table="<?php echo $lwl->table_cat; ?>" is_eighteen='<?php echo ($lwl->is_18 == 1)? 'yes':'no'; ?>'>
                <?php if ($_SESSION['user_id'] == $lwl->user_id || $lobby_creator == $_SESSION['user_id']) { ?>
                <a class="leave_lobby" self="<?php echo ($lobby_creator == $_SESSION['user_id'])? 'yes':'no'; ?>" player_tag_name="<?php echo $lwl->fan_tag; ?>" data-toggle="tooltip" data-placement="bottom" title="" lby_crtr="<?php echo ($lobby_creator == $lwl->user_id)?'yes':'no'; ?>" usr_id="<?php echo $lwl->user_id; ?>" everyone_readup="<?php echo ($lobby_bet[0]['is_full_play'] == 'yes')? 'yes':'no'; ?>" data-original-title="Leave Waiting List" leave_from_waitinlist="yes"><i class="fa fa-times"></i></a>
                <?php } ?>
                <span class="fan_tag"><?php echo $lwl->fan_tag; ?></span>
                <?php echo (in_array($lwl->user_id, $spectator_list) && $lby_is_spectator == 1)?'<span class="is_spectator_span"><i class="fa fa-eye"></i></span>':''; ?>
                <?php if ($lobby_creator == $lwl->user_id || $lwl->is_18 == 0) { ?>
                <span class="waiting_streamicon"> <img <?php echo ($lwl->stream_status == 'enable') ? 'src="'.base_url().'assets/frontend/images/live_stream_on.png" stream_click_status="disable"': 'src="'.base_url().'assets/frontend/images/live_stream_off.png" stream_click_status="enable"'; echo ($lwl->user_id == $_SESSION['user_id']) ? 'class="stream_icon enabled"' : 'class="disabled"'; ?> id="stream_<?php echo $lwl->user_id; ?>" alt="Stream Icon"></span>
                <?php } ?>
                <span class="eighteen_plus_img pull-right"><?php echo ($lwl->is_18 == 1)? '<img src="'.base_url().'assets/frontend/images/eighteenplus.png" class="img" alt="Eighteen plus img">':'<span class="under_eighteen_lable">Under 18</span>'; ?></span>
              </div>
              <?php } ?>
            </div>
          </div>
          <div class="col-sm-6 right_wtable">
            <div class="join_wtnglist_btn row">
              <a class="btn lobby_btn pull-right join_wtng_btn" data-toggle="tooltip" data-html="true" data-placement="left" data-original-title="Click to Join Right waiting table" tg_table="RIGHT" <?php echo (!in_array($_SESSION['user_id'],$wtnglist['wlfanarr']))? 'style="display:block"':'style="display:none"'; ?>> Join Waiting List </a>
            </div>            
            <div class="text-right wtnglstlistdiv right_tbl_wtnglst" wtnglst_table="RIGHT">
              <h3> Right table Waiting list</h3>
              <?php foreach ($wtnglist['rightwaitinglist'] as $key => $rwl) { ?>
              <div class="waitingusr" id="<?php echo $rwl->fan_id; ?>" lobby_id="<?php echo $rwl->lobby_id; ?>" fan_tag="<?php echo $rwl->fan_tag; ?>" user_id="<?php echo $rwl->user_id; ?>" table="<?php echo $rwl->table_cat; ?>" is_eighteen='<?php echo ($rwl->is_18 == 1)? 'yes':'no'; ?>'>
                <span class="eighteen_plus_img pull-left"><?php echo ($rwl->is_18 == 1)? '<img src="'.base_url().'assets/frontend/images/eighteenplus.png" class="img" alt="Eighteen plus img">':'<span class="under_eighteen_lable">Under 18</span>'; ?></span>
                <?php if ($lobby_creator == $rwl->user_id || $rwl->is_18 == 0) { ?>
                <span class="waiting_streamicon"> <img <?php echo ($rwl->stream_status == 'enable') ? 'src="'.base_url().'assets/frontend/images/live_stream_on.png" stream_click_status="disable"': 'src="'.base_url().'assets/frontend/images/live_stream_off.png" stream_click_status="enable"'; echo ($rwl->user_id == $_SESSION['user_id']) ? 'class="stream_icon enabled"' : 'class="disabled"'; ?> id="stream_<?php echo $rwl->user_id; ?>" alt="Stream Icon"></span>
                <?php } ?>
                <?php echo (in_array($rwl->user_id, $spectator_list) && $lby_is_spectator == 1)?'<span class="is_spectator_span"><i class="fa fa-eye"></i></span>':''; ?>
                <span class="fan_tag"><?php echo $rwl->fan_tag; ?></span>
                <?php if ($_SESSION['user_id'] == $rwl->user_id || $lobby_creator == $_SESSION['user_id']) { ?>
                <a class="leave_lobby" self="<?php echo ($lobby_creator == $_SESSION['user_id'])? 'yes':'no'; ?>" player_tag_name="<?php echo $rwl->fan_tag; ?>" data-toggle="tooltip" data-placement="bottom" title="" lby_crtr="<?php echo ($lobby_creator == $rwl->user_id)?'yes':'no'; ?>" usr_id="<?php echo $rwl->user_id; ?>" everyone_readup="<?php echo ($lobby_bet[0]['is_full_play'] == 'yes')? 'yes':'no'; ?>" data-original-title="Leave Waiting List" leave_from_waitinlist="yes"><i class="fa fa-times"></i></a>
                <?php } ?>
              </div>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($lobby_ads) || $lobby_ads !='') { ?>
      <div class="container live_lobby_ads_container">
          <div class="row">
             <div class="col-sm-3"></div>
             <div class="col-sm-6">
                <a class="live_lobby_ads_desciption_div" href="<?php echo $lobby_ads[0]->sponsered_by_logo_link; ?>" target="_blank">
                   <div class="live_lobby_ads_img_div">
                    <img src="<?php echo base_url('upload/ads_sponsered_by_logo/'.$lobby_ads[0]->upload_sponsered_by_logo);?>" alt="<?php echo $lobby_ads[0]->upload_sponsered_by_logo; ?>" class="img img-responsive">
                   </div>
                   <div class="live_lobby_ads_content_div">
                    <div class="live_lobby_ads_desciption"><?php echo $lobby_ads[0]->sponsered_by_logo_description; ?></div>
                  </div>
                </a>
             </div>
             <div class="col-sm-3"></div>
          </div>
      </div>
      <?php } ?>
      <?php if (isset($lobby_bet[0]['lobby_password']) && $lobby_bet[0]['lobby_password'] !='') { ?>
      <div id="lobby_password_model" class="modal" style="display: none;">
          <div class="modal-content">
             <div class="modal-header">
                <span class="close">&times;</span>
                <h1>Please Enter Lobby Password</h1>
                <div class="modal_div_border">
                   <form method="POST">
                      <div class="form-group">
                         <div class="input-group col-lg-12">
                            <input type="text" class="form-control lby_pw" id="lby_pw" name="lby_pw" value="">
                            <div class="clearfix"></div>
                            <span style="color:red"></span>
                            <div class="input_fields_wrap">
                            </div>
                         </div>
                      </div>
                      <div class="form-group">
                         <div class="input-group col-lg-12">
                            <div class="alert alert-danger lobby_password_error" style="display: none;"></div>
                         </div>
                      </div>
                      <div class="form-group">
                         <div class="input-group col-lg-12">
                            <input type="button" value="Submit" id="submit_lobby" class="btn-submit">
                         </div>
                      </div>
                   </form>
                </div>
             </div>
          </div>
      </div>
      <?php } ?>
      <div id="join_lobby" class="modal" style="display: none;">
          <div class="modal-content">
             <div class="modal-header">
                <?php if ($remaining_table == 'FULL') { ?>
                <span class="close">&times;</span> 
                <div class="col-sm-12 left_choose">
                   <div class="row">
                      <h1 class="diable_choose_table">
                         Both Tables are FULL..!!
                      </h1>
                   </div>
                   <div align="center" class="row close_button_row"><button class="close close_button"> Close </button></div>
                </div>
                <?php } else { ?>
                <span class="close">&times;</span>
                <h1>Choose Table..</h1>
                <?php if ($full_table == 'LEFT') {
                   echo '<div class="col-sm-6 left_choose diable_choose_left"><h1 class="diable_choose_table">'.$full_table." table is full.. Please Join ".$remaining_table." table</h1>";
                   } else {
                    echo '<div class="col-sm-6 left_choose"><div class="choose_table_cat" table_cat="LEFT">LEFT</div>';
                   } 
                   ?>
             </div>
             <?php
                if ($full_table == 'RIGHT') {
                echo '<div class="col-sm-6 right_choose diable_choose_right"><h1 class="diable_choose_table">'.$full_table." table is full.. Please Join ".$remaining_table." table<h1>";
                } else {
                 echo '<div class="col-sm-6 right_choose"><div class="choose_table_cat" table_cat="RIGHT">RIGHT</div>';
                }
                ?>
          </div>
          <?php } ?>
      </div>
      </div>
   </div>
   <div id="lobby_accept" class="modal" style="display: none;">
      <div class="modal-content">
         <div class="modal-header">
            <span class="close">&times;</span>  
            <h4><?php echo '$ '.number_format((float) $lobby_bet[0]['price'], 2, '.', ''); ?> will be deducted from your account!</h4>
            <h4>The Hosting Lobby Fee will also be deducted from your account.</h4>
            <h3><a class="button yes">OK</a><a class="button no">Cancel</a></h3>
         </div>
      </div>
   </div>
   <div id="leave_lobby" class="modal" style="display: none;">
      <div class="modal-content">
         <div class="modal-header">
            <span class="close">&times;</span>  
            <h1 class="warning_msg"></h1>
            <h4>Are you sure to leave this lobby .?</h4>
            <h3><a class="button yes">OK</a><a class="button no">Cancel</a></h3>
         </div>
      </div>
   </div>
   <div id="bet_report_modal" class="modal" style="display: none;">
      <div class="modal-content">
         <div class="modal-header">
            <span class="close">&times;</span>  
            <h4>Are you sure you want to submit a report</h4>
            <h4></h4>
            <h3><a class="button yes">OK</a><a class="button no">Cancel</a></h3>
         </div>
      </div>
   </div>
   <div id="change_stream_modal" class="modal" style="display: none;">
      <div class="modal-content">
         <div class="modal-header">
            <span class="close">&times;</span>  
            <h4>Are you sure to change Stream status</h4>
            <h4></h4>
            <h3><a class="button yes">OK</a><a class="button no">Cancel</a></h3>
         </div>
      </div>
   </div>
   <div id="send_tip" class="modal" style="display: none;">
      <div class="modal-content">
         <div class="modal-header">
            <span class="close">&times;</span>  
            <h4></h4>
            <h3></h3>
            <h3><a class="button yes">OK</a><a class="button no">Cancel</a></h3>
         </div>
      </div>
   </div>
   <div id="change_pass_control" class="modal custmodal" style="display: none;">
      <div class="modal-content">
         <div class="modal-header">
            <span class="close">&times;</span>
            <h1 class="warning_msg">WARNING MESSAGE</h1>
            <h4 class="passctrlh4"></h4>
            <h3></h3>
            <h3><a class="button yes">OK</a><a class="button no">Cancel</a></h3>
         </div>
      </div>
   </div>
   <?php if ($lobby_creator == $_SESSION['user_id'] && count($get_play_status_lobby_bet) == 0) { ?>
  <div id="change_lobby_details" class="modal custmodal" style="display: none;">
    <?php 
    $checked = '';
    $selectopt_hide = 'style = "display:block;"';
    $imgup_hide = $customopt_hide = 'style = "display:none;"';
    if ($lobby_bet[0]['game_category_id'] == 5) {
      $cust = true;
      $checked = 'checked';
      $customopt_hide = 'style = "display:block;"';
      $imgup_hide = 'style = "display:inline-block;"';
      $selectopt_hide = 'style = "display:none;"';
    }
    ?>
    <div class="modal-content">
      <div class="modal-header text-left">
        <span class="close">&times;</span>
        <h1 class="text-left">Lobby Edit</h1>
        <div class="text-left">
          <div class="myprofile-edit">
             <div class="col-lg-12">
                <div class="row custom_settings">
                  <input type="checkbox" class="custom_lobby" name="custom_lobby" id="custom_lobby_check" <?php echo $checked; ?>> Custom Settings
                </div>
                <div class="row">
                   <form id="frm_game_create" class="edit_lobby_details" name="frm_game_create">
                      <div class="form-group select_games" <?php echo $selectopt_hide; ?>>
                         <label class="col-lg-4">Game System</label>
                         <div class="input-group col-lg-8">
                            <select type="text" class="form-control game_category" <?php echo($cust)? '': 'id="game_category" name="game_category" required'?>>
                              <option value="">--Select a system--</option>
                              <?php foreach ($game_category as $value) { ?>
                                <option value="<?php echo $value['id'];?>" <?php echo ($lobby_bet[0]['game_category_id'] == $value['id']) ? 'selected' : '' ; ?>><?php echo $value['cat_slug'] ?></option>
                              <?php } ?>
                            </select>
                         </div>
                      </div>
                      <div class="form-group select_games" <?php echo $selectopt_hide; ?>>
                        <label class="col-lg-4">Game Name</label>
                        <div class="input-group col-lg-8">
                          <?php if(!empty($lobby_bet[0]['game_name'])) { ?>
                          <select type="text" class="form-control game_name" name="game_name" <?php echo($cust)? '': 'id="name" name="game_name"  required'?>>
                            <option>--Select a Game--</option>
                            <?php foreach ($game_name as $value) { ?>
                              <option value="<?php echo $value['id'];?>" <?php echo ($lobby_bet[0]['game_id'] == $value['id']) ? 'selected' : '' ; ?>><?php echo $value['game_name'] ?></option>
                              <?php } ?>
                          </select>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group select_games" <?php echo $selectopt_hide; ?>>
                        <label class="col-lg-4">Game Sub Category</label>
                        <div class="input-group col-lg-8">
                          <?php if(!empty($lobby_bet[0]['sub_game_name'])) { ?>
                          <select type="text" class="form-control subgame_name_id" <?php echo($cust)? '': 'id="sub_name" name="subgame_name_id" required'?>>
                            <option>--Select a Sub Game--</option>
                            <?php foreach ($sub_game_name as $value) { ?>
                              <option value="<?php echo $value['asgid'];?>" <?php echo ($lobby_bet[0]['sub_game_id'] == $value['asgid']) ? 'selected' : '' ; ?>><?php echo $value['sub_game_name'] ?></option>
                              <?php } ?>
                          </select>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group custom_games" <?php echo $customopt_hide; ?>>
                        <label class="col-lg-4">Game System</label>
                        <div class="input-group col-lg-8">
                          <?php if(isset($cust_game_cat)) { ?>
                          <select type="text" id="game_category" class="form-control game_category" required>
                            <option value="<?php echo $cust_game_cat[0]['id'];?>" selected><?php echo $cust_game_cat[0]['category_name'] ?></option>
                          </select>
                          <?php } else { ?>
                          <input type="text" placeholder="Enter Game Category" class="form-control select_game_category">
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group custom_games" <?php echo $customopt_hide; ?>>
                        <label class="col-lg-4">Game Name</label>
                        <div class="input-group col-lg-8">
                          <input type="text" placeholder="Enter Game Name" class="form-control select_game_name" required value="<?php echo($lobby_bet[0]['game_name'])? $lobby_bet[0]['game_name'] :'' ?>">
                         </div>
                      </div>
                      <div class="form-group custom_games" <?php echo $customopt_hide; ?>>
                        <label class="col-lg-4">Game Sub Category</label>
                        <div class="input-group col-lg-8">
                          <input type="text" placeholder="Enter Sub Game Name" class="form-control select_subgame_name_id" required value="<?php echo($lobby_bet[0]['sub_game_name'])? $lobby_bet[0]['sub_game_name'] :'' ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-4">Game Size</label>
                        <div class="input-group col-lg-8">
                          <select type="text" class="form-control" name="game_type"  id="game_type" required>
                            <option>--Select Game Size--</option>
                            <?php
                              if (!empty($lobby_bet[0]['game_type'])) {
                                for ($i = 1; $i <=16; $i++) {
                                  $sel = ($lobby_bet[0]['game_type'] == $i) ? 'selected' : '';
                                  echo '<option value="'.$i.'" '.$sel.'>--'.$i.'V'.$i.'--</option>';
                                }
                              }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-4">Entry Amount ($)</label>
                        <div class="input-group col-lg-8">
                          <input type="text" placeholder="$0.00" id="lobby_amount" class="form-control" name="bet_amount" value="<?php echo ($lobby_bet[0]['price'])? $lobby_bet[0]['price'] :''; ?>" onkeyup="return isNumbKey(event,this.id)" onkeydown="return isNumbKey(event,this.id)" required>
                          <span id="demo" style="margin-left: -194px;color: red;"></span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-4">Password</label>
                        <div class="input-group col-lg-8 lobby_password_div">
                          <label class="switch lobby_password_switch_button">
                            <input type="checkbox" name="password_option" id="password_option" <?php echo (!empty($lobby_bet[0]['lobby_password'])) ? 'checked' : '' ; ?>>
                            <span class="slider round"></span>
                          </label>
                          <input type="text" id="lobby_password" class="form-control lobby_password" placeholder="Enter Password" <?php echo (!empty($lobby_bet[0]['lobby_password'])) ? 'value="'.$lobby_bet[0]['lobby_password'].'"' : 'style="display: none;"' ; ?>>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-4">Rules</label>
                        <div class="input-group col-lg-8">
                          <textarea class="form-control" id="description" placeholder="Game Description" rows="5" cols="5" name="description" required><?php echo (!empty($lobby_creator_det->description)) ? $lobby_creator_det->description : ''; ?></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-4">Image</label>
                        <div class="input-group col-lg-8">
                          <div id="game_img">
                            <?php
                            if (!empty($lobby_bet[0]['game_image'])) {
                              foreach ($game_imgs as $value) {   
                                $selected_class = "";
                                if ($lobby_bet[0]['game_image_id'] == $value['id']) {
                                  $selected_class = "selected";  
                                }
                              ?>
                              <img class="img-cls <?php echo $selected_class; ?>" id="img<?php echo $value['id'] ?>" style="margin-right: 31px;" onclick="a('<?php echo $value['id'] ?>')" src="<?php echo base_url().'upload/game/'.$value['game_image']; ?>" width="100" height="80"/>
                              <span class="change_custlby_img btn lobby_btn" <?php echo $imgup_hide; ?>> Change image</span>
                              <?php } } ?>
                          </div>
                        </div>
                      </div>
                      <div class="btn-group">
                        <div class="col-lg-8 pull-right padd0">
                          <input type="hidden" name="personal_challenge_friend_id">
                          <input type="hidden" id="game_image_id" name="game_image_id" value="<?php echo($lobby_bet[0]['game_image_id']) ? $lobby_bet[0]['game_image_id'] : '' ; ?>" class="new_game-image">
                          <a type="button" id="submitPlaceChallenge" value="Submit" class="btn-update lobby_btn">Submit</a>
                        </div>
                      </div>
                   </form>
                </div>
             </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
  <div id="edit_lobby_tag_mdl" class="modal custmodal" style="display: none;">
    <div class="modal-content">
       <div class="modal-header">
          <span class="close">&times;</span>
          <div class="text-left">
            <h3 class="tag_btn_div"><a class="tag_btn">Edit Player Tag</a></h3>
          </div>
            <div class="modal_div_border">
              <div class="form-group">
                 <div class="input-group col-lg-12">
                   <input type="text" class="form-control" name="change_lobby_tag" placeholder="Enter Player Tag" value="<?php echo ($get_my_fan_tag->fan_tag)? $get_my_fan_tag->fan_tag : '';  ?>">
                 </div>
              </div>
                <div class="form-group">
                   <div class="input-group col-lg-12">
                      <div class="alert alert-danger lobby_tag_error" style="display: none;"></div>
                   </div>
                </div>                
            </div>
          <h3><a class="button yes">Update</a><a class="button no">Cancel</a></h3>
       </div>
    </div>
  </div>      
  <div id="enter_stream_channel" class="modal" style="display: none;">
    <div class="modal-content">
      <div class="modal-header">
        <span class="close">&times;</span>  
        <h1 class="text-left">Please Enter Your Twitch Username Login</h1>
        <h4></h4>
        <div class="modal_div_border">
          <div class="form-group">
            <div class="input-group col-lg-12">
             <input type="text" class="form-control" name="stream_channel_input" placeholder="Enter your channel">
            </div>
          </div>
          <div class="form-group">
            <div class="input-group col-lg-12 save_stream_radio">
              <label class="toggle checkbox_container"><input type="checkbox" name="save_stream"> &nbsp;Save into Stream Settings<span class="checkmark"></span></label>
            </div>
          </div>
          <div class="form-group">
            <div class="input-group col-lg-12">
              <div class="alert alert-danger stream_error" style="display: none;"></div>
            </div>
          </div>
          <div class="form-group">
            <div class="input-group col-lg-12">
              <input type="submit" value="Submit" class="btn-submit">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="send_tip_amount" class="modal custmodal send_teamtip_amount" style="display: none;">
    <div class="modal-content">
       <div class="modal-header">
          <span class="close">&times;</span>  
          <h1 class="text-left">Please Enter Donation Amount</h1>
          <div class="modal_div_border row">
            <div class="col-sm-12">
              <div class="title_head_div text-center">
                <label class="team-title"><h4>ESPORTS TEAM</h4></label>
              </div>
            </div>
            <div class="col-sm-6 table-left">
              <?php if ($lobby_game_bet_left_grp != '') {  ?>
              <div class="team_title text-center">
                <div class="row">
                  <div class="mainteam_title"> <?php echo ($lobby_game_bet_left_grp[0]['team_name'])? $lobby_game_bet_left_grp[0]['team_name'] : '&nbsp;';?> </div>
                  <div class="col-sm-12 pull-left">
                    <label class="check_all_lable"><input type="checkbox" name="left_check"> Check All Left </label>
                  </div>
                </div>
              </div>
              <div class="team_member">
                <?php 
                foreach ($lobby_game_bet_left_grp as $key => $left_row) {
                  $fan_tag = $this->General_model->get_my_fan_tag($lobby_bet[0]['lobby_id'],$left_row['user_id']);
                  ?>
                  <div class="form-group" id="activeusr_<?php echo $left_row['user_id']; ?>">
                    <div class="input-group col-lg-12">
                      <label><input type="checkbox" name="teap_team[]" id="<?php echo $left_row['user_id']; ?>"> <?php echo $fan_tag->fan_tag; ?> </label><span class="tipamount"></span>
                    </div>
                  </div>
                <?php }
              } ?>
              </div>
            </div>
            <div class="col-sm-6 table-right">
              <?php if ($lobby_game_bet_right_grp != '') { ?>
              <div class="team_title text-center">
                <div class="row">
                  <div class="mainteam_title"> <?php echo ($lobby_game_bet_right_grp[0]['team_name'])? $lobby_game_bet_right_grp[0]['team_name'] : '&nbsp;'; ?> </div>
                  <div class="col-sm-12 pull-right">
                    <label class="check_all_lable"> Check All Right <input type="checkbox" name="right_check"> </label>
                  </div>
                </div>
              </div>
              <div class="team_member text-right">
              <?php foreach ($lobby_game_bet_right_grp as $key => $right_row) {
                  $fan_tag = $this->General_model->get_my_fan_tag($lobby_bet[0]['lobby_id'],$right_row['user_id']);
                  ?>
                  <div class="form-group" id="activeusr_<?php echo $right_row['user_id']; ?>">
                     <div class="input-group col-lg-12">
                      <span class="tipamount"></span>
                       <label> <?php echo $fan_tag->fan_tag; ?> <input type="checkbox" name="teap_team[]" id="<?php echo $right_row['user_id']; ?>"> </label>
                     </div>
                  </div>
              <?php }
              } ?>
              </div>
            </div>
            <div class="row team_donate_bottom_div">
              <div class="col-sm-12">
                <div class="col-sm-4">
                  <div class="form-group">
                     <div class="input-group">
                       <label>EACH</label>
                       <input type="text" class="form-control" name="tip_amount_input" placeholder="0.00" onkeyup="return isNumberKey(event)">
                     </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                     <div class="input-group">
                        <label>TOTAL</label>
                        <input type="text" class="form-control" name="tip_total_amount_input" placeholder="0.00" readonly>
                      </div>
                     </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                     <div class="input-group">
                      <br/>
                        <input type="submit" value="Submit" class="btn-submit">
                     </div>
                  </div>
                </div>
                </div>
              </div>                  
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                       <div class="input-group col-lg-12">
                          <div class="alert alert-danger tip_amount_error" style="display: none;"></div>
                       </div>
                    </div>
                  </div>
               </div>
            </div>
          </div>
       </div>
    </div>
  </div>   
  <div id="bet_report_modal_status" class="modal" style="display: none;">
    <div class="modal-content">
       <div class="modal-header">
          <span class="close">&times;</span>  
          <h4>Lobby ID: <strong><?php echo $lobby_bet[0]['lobby_id']; ?></strong>&nbsp;&nbsp;&nbsp;&nbsp; Lobby Bet ID: <strong><?php echo $lobby_bet[0]['id']; ?></strong></h4>
          <div class="won_lost_status_btn row"><a class="button won" pop_up_id="won_div" form_id="win">WON</a><a class="button lost" pop_up_id="lost_div" form_id="lose">LOST</a><a class="button admin" pop_up_id="admin_div" form_id="admin">ADMIN</a></div>
          <div class="won_div" id="won_div" style="display: none;">
             <?php if(!empty($check_reported) && $check_reported[0]['win_lose_status']=='win') { ?>
             <div class="report_error">Player has already reported Won..!!<br>Please choose another option.</div>
             <?php } else { ?>
             <form class="won_lost_status" id="win" method="POST">
                <div class="form-group">
                   <label class="col-lg-4">Video Url</label>
                   <div class="input-group col-lg-8">
                      <input type="text" class="form-control video_url" name="url" value="">
                      <div class="clearfix"></div>
                      <span style="color:red"></span>
                      <div class="input_fields_wrap">
                      </div>
                   </div>
                </div>
                <div class="form-group">
                   <label class="col-lg-4"></label>
                   <div class="input-group col-lg-8">
                      <input type="checkbox" name="accept_risk" value="1" class="accept_risk"> I Accept Risk Because I Have No video/proof of  Victory
                      <div class="clearfix"></div>
                      <span style="color:red"></span>
                   </div>
                </div>
                <div class="form-group">
                   <label class="col-lg-4"></label>
                   <div class="input-group col-lg-8">
                      <input type="hidden" name="win_lose_status" value="win">
                      <input type="button" value="Submit" class="btn-submit won_submut" id="mehul">
                   </div>
                </div>
             </form>
             <?php } ?>
          </div>
          <div class="won_div" id="lost_div" style="display: none;">
             <?php if (!empty($check_reported) && $check_reported[0]['win_lose_status']=='lose') { ?>
             <div class="report_error">Player has already reported Lose..!!<br>Please choose another option.</div>
             <?php } else { ?>
             <form class="won_lost_status" id="lose" method="POST">
                <div class="form-group">
                   <div class="input-group col-lg-12">
                      <h4>Are you sure you want to report a loss?</h4>
                   </div>
                </div>
                <div class="form-group">
                   <div class="input-group col-lg-12">
                      <input type="hidden" name="win_lose_status" value="lose">
                      <input type="button" value="Submit" class="btn-submit admin_submut">
                   </div>
                </div>
             </form>
             <?php } ?>    
          </div>
          <div class="won_div" id="admin_div" style="display: none;">
             <form class="won_lost_status" id="admin" method="POST">
                <div class="form-group">
                   <label class="col-lg-4">Video Url</label>
                   <div class="input-group col-lg-8">
                      <input type="text" class="form-control video_url" name="url" value="">
                      <div class="clearfix"></div>
                      <span style="color:red"></span>
                      <div class="input_fields_wrap">
                      </div>
                   </div>
                </div>
                <div class="form-group">
                   <label class="col-lg-4">Please explain in detail to the admin</label>
                   <div class="input-group col-lg-8">
                      <textarea class="form-control" name="explain_admin"></textarea>
                      <div class="clearfix"></div>
                      <span style="color:red"></span>
                      <div class="input_fields_wrap"></div>
                   </div>
                </div>
                <div class="form-group">
                   <label class="col-lg-4"></label>
                   <div class="input-group col-lg-8">
                      <input type="checkbox" name="accept_risk" class="accept_risk" value="1"> I Accept Risk Because I Have No video/proof of  Victory
                      <div class="clearfix"></div>
                      <span style="color:red"></span>
                   </div>
                </div>
                <div class="form-group">
                   <label class="col-lg-4"></label>
                   <div class="input-group col-lg-8">
                      <input type="hidden" name="win_lose_status" value="admin">
                      <input type="button" value="Submit" class="btn-submit admin_submut">
                   </div>
                </div>
             </form>
          </div>
       </div>
    </div>
  </div>
</section>
<input type="hidden" name="lobby_accept_total_balance" value="<?php echo $total_balance; ?>">
<input type="hidden" name="lobby_accept_price" value="<?php echo $lobby_bet[0]['price'] ?>">
<input type="hidden" name="lobby_price_add_decimal" value="<?php echo $price_add_decimal; ?>">
<input type="hidden" name="lobbygroup_bet_id" value="<?php echo $lobby_bet[0]['id']; ?>">
<input type="hidden" name="lobby_id" value="<?php echo $lobby_bet[0]['lobby_id']; ?>"> 
<input type="hidden" name="lobby_custom_name" value="<?php echo $lobby_bet[0]['custom_name']; ?>">
<input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
<input type="hidden" name="creator_id" value="<?php echo $lobby_creator; ?>">
<input type="hidden" name="is_admin" value="<?php $user_name = $this->General_model->view_single_row('user_detail','user_id',$_SESSION['user_id']); echo $user_name['is_admin']; ?>">

<script type="text/javascript">
  // window.addEventListener("keyup",function(ev){
  // if(ev.ctrlKey && ev.keyCode === 90) console.log(ev); // or do smth
  //   document.fire("keyup",{ctrlKey:true,keyCode:90,bubbles:true})
  // })
  // }
  function close_window(event){
    document.fire("keypress",{altKey:true,keyCode:115,bubbles:true})
  }
  var availableTags = [<?php print_r($fan_tagarr)?>];
  function isNumberKey(evt){
    var val = $('input[name="tip_amount_input"]').val();
    if (val == '.') {}
    else {
      if(isNaN(val)) {
        val = val.replace(/[^0-9\.]/g,'');
        if (val.split('.').length>1) 
          val = val.replace(/\.+$/,"");
      }
    }
    $('input[name="tip_amount_input"]').val(val);
  }

  $('#coin').on('click', function(){
    var flipResult = Math.floor(Math.random() * 10);
    $('#coin').removeClass();
    setTimeout(function(){
      (flipResult % 2 === 0) ? $('#coin').addClass('heads') : $('#coin').addClass('tails');
    }, 100);
  });  
</script> 