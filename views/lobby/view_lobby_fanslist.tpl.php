<section class="body-middle innerPage">
   <div class="container view_lobby_container">
      <div class="row header1">
         <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
         <div class="col-lg-4 col-sm-4 col-md-4">
            <h2>Fans List</h2>
         </div>
         <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
      </div>
      <div class="row">
         <div class="col-lg-12" align="center">
            <div class="input-group back_button" style="margin:8px 0px;">
               <a href="javascript:history.go(-1)">BACK</a>
            </div>
         </div>
      </div>
      <div class="row filter-panel home_page_search_tabbing">
         <div class="col-lg-12">
            <div class="form-group">
               <select name="fans_id" id="fans_id" class="form-control" onchange="fans_search(<?php echo $view_lby_det[0]['id']; ?>)">
                  <option value="">Please Select a Fan Id</option>
                  <?php foreach($fans_id_list as $val) {  ?>
                  <option value="<?php echo $val['id'];?>">
                      <?php echo $val['id'];?>
                  </option>
                  <?php } ?>
               </select>
               <select name="fan_tag" id="fan_tag" class="form-control" onchange="fans_search(<?php echo $view_lby_det[0]['id']; ?>)">
                  <option class="" value="">Please Select a Tag</option>
                  <?php foreach($fans_id_list as $val) {  ?>
                  <option value="<?php echo $val['fan_tag'];?>">
                      <?php echo $val['fan_tag'];?>
                  </option>
                  <?php } ?>
               </select>
               <div class="input-group add-on form-control" style="float: right;">
                  <input placeholder="Search Fan Id/Tag" name="srch-term" id="srch-term" type="text" onkeyup="fans_search(<?php echo $view_lby_det[0]['id']; ?>)" onkeydown="fans_search(<?php echo $view_lby_det[0]['id']; ?>)">
                  <span class="glyphicon glyphicon-search form-control-feedback"></span>
               </div>
            </div>
         </div>
      </div>
      <div class="friend_list_main_row fanslist">
         <ul>
            <?php if (isset($view_lobby_fans_list) && $view_lobby_fans_list !='') {
                foreach ($view_lobby_fans_list as $key => $lf) { ?>
                <li class="frind_row_li" id="frind_row_<?php echo $lf->user_id; ?>">
                   <div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
                      <img src="<?php echo base_url(); ?>/upload/profile_img/<?php echo $lf->image; ?>" alt="">
                   </div>
                   <div class="col-lg-7 col-sm-6 col-md-3 bid-info">
                      <h2><?php echo $lf->fan_tag; ?></h2>
                   </div>
                   <div class="col-lg-3 col-sm-5 col-md-3 primum-play play-n-rating">
                    <span class=""><a href="<?php echo base_url('livelobby/view_fansprofile/?lobby_id='.$view_lby_det[0]['id'].'&fans_id='.$lf->user_id); ?>" class="view_profile">Profile</a></span>
                    <?php if ($lf->user_id != $_SESSION['user_id']): ?>
                    <form role="form" action="<?php echo base_url(); ?>friend/add_friend/" name="addToFriendForm" class="add-to-friend-form" method="POST" enctype="multipart/form-data">
                      &nbsp;&nbsp;<input type="submit" class="add_friend_button" value="Add Friend"  onclick="return confirm('Are you sure want to add ?')">&nbsp;&nbsp;
                      <textarea style="display: none;" name="mail"><?php echo $my_data->name; ?> sent you friend request on <?php echo base_url(); ?>.</textarea>
                      <input type="hidden" name="user_to" value="<?php echo $lf->user_id; ?>">
                    </form>
                    <?php endif ?>
                   </div>
                </li>
                <?php } } ?>
         </ul>
      </div>
     
   </div>
</section>