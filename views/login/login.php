<section class="login-signup-page">

	<div class="container">

    	<div class="row">

        	<div class="login-signBlock">

            	<h2>ENTER YOUR LOGIN</h2>

                <?php if(isset($err)) echo $err; ?>
                <label style="margin-left: 255px;color: #ff5000;"><?php echo $this->session->flashdata('message_succ');?></label>

                <form role="form" class="signup-form" action="<?php echo base_url().'login'; ?>" method="post">

                	

                    <div class="form-group">

                    	<label class="col-lg-4 col-sm-4 col-md-4">Email Address</label>

                        <div class="input-group col-lg-8 col-sm-8 col-md-8">

                        	<input required autocomplete="off" type="email" class="form-control" name="email">

                        </div>

                    </div>

                    <div class="form-group">

                    	<label class="col-lg-4 col-sm-4 col-md-4">Password</label>

                        <div class="input-group col-lg-8 col-sm-8 col-md-8">

                        	<input required autocomplete="off" type="password" class="form-control" name="password">

                        </div>

                    </div>

                    

                    

                    <div class="btn-group">

                    	<input type="submit" value="Sign in" class="btn-submit">

                    </div>

                </form>

                

                <p><span><a href="<?php  echo base_url().'user/forgot_password'; ?>">Forgot Password?</a></span> |  <span>New User? <a href="<?php echo base_url().'signup' ?>"> Signup</a></span></p>

                

                

            </div>

        </div>

    </div>

</section>



