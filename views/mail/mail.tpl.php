<div class="adm-dashbord">
    <div class="container padd0">

        <?php $this->load->view('dashboard/profile_sidebar_2.tpl.php'); ?>

        <div class="col-lg-10">

            <div class="col-lg-12 padd0">

                <div> 
                <div class="profile_top row">
                            <div class="col-sm-6 text-right">
                                <a class="profile_top_btn black" href="<?php echo base_url('user/myprofile'); ?>">My Profile</a>
                            </div>
                            <div class="col-sm-6">
                                <a class="profile_top_btn orange" href="<?php echo base_url('friend'); ?>">Dashboard</a>
                            </div>
                        </div>
                    <div class="col-lg-11">

                        <div class="myprofile-edit mail_div">

                          <h2>Mail</h2>

                          <?php $this->load->view('common/show_message'); ?>

                          <div class="row mail_title">
                            <div class="col-lg-4" align="left">
                              <div class="headr">
                                <a href="<?php echo base_url(); ?>mail" class="<?php if ($this->uri->segment(1)=='mail') { echo 'category_link'; } ?>">Friends Mail</a>
                              </div>
                            </div>
                            <div class="col-lg-4" align="center">
                              <div class="headr">
                                <a href="">Esports Team Mail</a>
                              </div>
                            </div>
                            <div class="col-lg-4" align="right">
                              <div class="headr">
                                <a href="<?php echo base_url(); ?>challenge_mail" class="<?php if ($this->uri->segment(1)=='challenge_mail') { echo 'category_link'; } ?>">Challenge Mail</a>
                              </div>
                            </div>
                          </div>
                          <div class="row mail_second_div">
                              <div class="mail_second_option_div">
                                <div class="col-lg-3">
                                    <div class="select_all_friend"><input type="checkbox" value="" id="ckbCheckAll"> <a href=""> Select All</a></div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="col-sm-3">
                                      <a class="btn button create_message" style="display: none;">Create message</a>
                                    </div>
                                    <div class="col-sm-6">
                                      <div class="input-group add-on form-control">
                                      <input placeholder="Name/ID/Email" name="srch-plyr" id="search_friends_mail" type="text" onkeyup="search_friends_mail()">
                                      <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                      </div>  
                                    </div>
                                    <div class="col-sm-3" style="text-align: right; padding-right: 0px;">
                                      <a class="btn button delete_all_mail" id="friends_mail-<?php print_r($_SESSION['user_id']); ?>"><i class="fa fa-trash"></i> Delete All Mail</a>
                                    </div>
                                </div>
                            </div>
                             <div class="mail_third_option_div">
                            <div class="col-lg-3">
                              <div class="friends_list">
                                <?php
                                // foreach ($unread_msg as $key => $value) {
                                //   $unread_msg_count[]= $value['message_id'];
                                // }
                                foreach ($friendlist as $key => $value) {

                                    $unread_msg_count=$this->General_model->count_unread_msg($value["friend_id"]);
                                    $friendlist_row = '<div class="single_friend_in_list"><input type="checkbox" class="select_mail" name="friends[]" id="id_'.$value["friend_id"].'" value="'.$value["friend_id"].'"> <a href=""><i class="fa fa-envelope-o"></i> <span class="unread_msg">'.count($unread_msg_count).'</span>'.$value["name"].'</a></div>';
                                    echo $friendlist_row;
                                    
                                } ?>
                              </div>
                            </div>
                            <div class="col-lg-9">
                            <div class="panel-group" id="accordion">
                                <?php
                                   foreach ($all_mail as $key => $value) {
                                     echo $value;
                                   } 
                                ?>
                            </div> 
                            <form role="form" action="<?php echo base_url(); ?>mail/send_mail/" class="send_mail_form" name="myForm" method="POST" enctype="multipart/form-data" style="display: none;">
                              <div class="form-group send_mail_box">
                                <div><input type="hidden" name="user_to" id="user_id"></div>
                                <div><textarea rows="10" class="input-group col-lg-12 mail_txtarea" name="mail"></textarea></div>
                                  <div class="file-upload file-upload_div file_upload_from_in" id="file_upload_firstdiv">
                                    <div class="file-select" id="chooseFile_a">
                                      <div class="file-select-button" id="fileName">Choose File</div>
                                      <div class="file-select-name noFile_from_in" id="noFile">No file chosen...</div>
                                      <input type="file" name="file[]" id="chooseFile_a" data-file="hasfile_0" class="chooseFile send_from_in">
                                    </div>
                                    <div class="image_output row">
                                    </div>
                                  </div>
                                <div class="send_mail_div"><input type="submit" class="send_mail_friend" value="Send Mail"></div>
                              </div>
                            </form>
                            <form role="form" action="<?php echo base_url(); ?>mail/send_mail_to_all/" class="send_mail_to_all" name="msg_send_to_all" id="msg_send_to_all" method="POST" enctype="multipart/form-data" style="display: none;">
                              <div class="form-group send_mail_box" id="chooseFile_a">
                                <div><input type="hidden" name="user_to" id="user_id_array"></div>
                                <div><textarea rows="10" class="input-group col-lg-12 mail_txtarea" name="mail"></textarea></div>
                                  <div class="file-upload file-upload_div file_upload_from_out" id="file_upload_secdiv">
                                    <div class="file-select">
                                      <div class="file-select-button" id="fileName">Choose File</div>
                                      <div class="file-select-name noFile_from_out" id="noFile">No file chosen...</div>
                                      <input type="file" name="file[]" id="chooseFile_a" data-file="hasfile_0" class="chooseFile send_from_out">
                                    </div>
                                    <div class="image_output row">
                                    </div>                                    
                                  </div>
                                <div class="send_mail_div"><input type="submit" class="send_mail_friend" value="Send Mail"></div>
                              </div>
                            </form>
                            </div>
                          </div>
                          </div>

                    </div>

                </div>

            </div>   

        </div>

    </div>

</div>
</div>
<script type="text/javascript">

  var select_id;
  var friend_id = GetParameterValues('friend_id');
    function GetParameterValues(param) {
    var chatbox_url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&'); 
    for (var i = 0; i < chatbox_url.length; i++) {  
        var urlparam = chatbox_url[i].split('=');  
        if (urlparam[0] == param) {  
          if (urlparam[1].length>0) {
            $('.select_mail#id_'+urlparam[1]).each(function(){
                this.checked = true;
                select_id = urlparam[1];
            });
          }
        }  
      }
    }
    if (select_id!=undefined) {
      if ($('.select_mail#id_'+select_id).is(':checked')) {
           var id = $('.select_mail#id_'+select_id).attr('id');
           var idsplit = id.split('_');
           if(idsplit[1]){
            $.ajax({
              url : '<?php echo site_url(); ?>mail/get_mail/'+idsplit[1],
              success: function(result) {
                if(result){
                    result = $.parseJSON(result);
                    if (result.user_id != null || result.user_id != '') {
                      
                      $('.mail_third_option_div .panel-group .panel').remove();
                      $('.mail_third_option_div .unread_msg_div').remove();
                      $("#file_upload_firstdiv>.image_output>img").remove();
                      $("#file_upload_secdiv>.image_output>img").remove();
                      $("#noFile").html("No file chosen...");             
                      $("#noFile.noFile_from_out").html("No file chosen...");             
                      $("#noFile.noFile_from_in").html("No file chosen...");
                      for (var i = result.mails.length - 1; i >= 0; i--) {

                      var row_data = '<div id="main_div_'+result.mails[i].id+'"><div class="unread_msg_div" id="'+result.mails[i].id+'">';
                      if (result.mails[i].mail_status == '1') {
                        row_data += 'Unread';
                      }
                      row_data += '</div><div class="mails_panel panel panel-default" id="'+result.mails[i].id+'"><div class="panel-heading"><h4 class="panel-title view_mail" id="'+result.mails[i].id+'" onclick="fortest()" data-toggle="collapse" data-parent="#accordion" href="#collapse'+result.mails[i].id+'"><a><span class="mail_heading">From '+result.mails[i].name+'</span></a><span class="float_right">'+result.mail_time[i]+' ago <a class="trash_selected_mail" id="'+result.mails[i].id+'"><i class="fa fa-trash-o"></i></a></span></h4></div><div id="collapse'+result.mails[i].id+'" class="panel-collapse collapse"><div class="panel-body"><div class="recieved_row"><div class="mail_body"><div class="">'+result.mails[i].mail+'</div>';
                        row_data += '</div></div></div>';
                        row_data += '<div class="panel-body panel-body_attachment_div mail_attachment">';
                        if (result.mails[i].attachment!=null && result.mails[i].attachment!='') {
                          var my_data=result.mails[i].attachment.split(',');
                          for (var a = my_data.length - 1; a >= 0; a--) {
                           row_data+= '<a href="<?php echo base_url() ?>upload/mail/'+my_data[a]+'" class="download_attachment" download><i class="fa fa-download"></i><img class="attachment_image img img-thumbnail" src="<?php echo base_url() ?>upload/mail/'+my_data[a]+'"></a>';
                         }
                        }
                        row_data+= '</div><div class="panel-body"><div class="sent_row"></div><div class="reply_box"><form role="form" action="<?php echo base_url(); ?>mail/reply_mail/" class="send_mail_form send_mails_form reply-form" name="myForm" method="POST" enctype="multipart/form-data"><textarea class="" rows="10" name="reply_data"></textarea><input type="hidden" name="user_to" value="'+idsplit[1]+'"><input type="hidden" name="mail_id" value="'+result.mails[i].message_id+'"> <div class="file-upload file-upload_div" id="reply_div_home_'+result.mails[i].id+'"><div class="file-select"><div class="file-select-button" id="fileName">Choose File</div><div class="file-select-name" id="noFiles_'+result.mails[i].id+'">No file chosen...</div> <input type="file" name="reply_file[]" data-file="hasfile_0" class="chooseFile" id="'+result.mails[i].id+'></div><div class="image_output row"></div><div class="reply_button"><input type="submit" id="reply" value="Reply" name="reply"><input type="reset" value="Cancel"></div></form></div></div></div></div></div>';
                          $('.mail_third_option_div .panel-group').append(row_data);
                        }

                         $('.send_mail_form').show();
                         $('.send_mail_form .send_mail_box #user_id').val(result.user_id);
                         $('.send_mail_to_all').hide();
                         $('.create_message').hide();
                         $('#accordion').show();  

                         $(document.body).on('change', 'input[name^="reply_file[]"]', function() {
                            var filename = [];
                            var get_id = $(this).attr('id');
                            var data = $(this).attr('data-file');
                            var datasplit = data.split('_');
                            var sum_files = parseInt(datasplit[1]);
                                sum_files += 1;
                            var rpl_images = '';
                            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                                  filename.push($(this).get(0).files[i].name);
                                  rpl_images += '<img width="100" height="100" src="'+URL.createObjectURL(event.target.files[i])+'" class="hasfile_'+sum_files+'" />';
                            }
                            if (/^\s*$/.test(filename)) {
                              $(".reply_choose_file_div_"+get_id).removeClass('active');
                              $(".reply_choose_file_div_"+get_id+" #noFile").html("No file chosen...");
                              $("#noFile_"+get_id).html("No file chosen...");
                            }
                            else {
                              $('.reply-form-after-select #reply_seconddiv_home_'+get_id+' .image_output').append(rpl_images);
                              $("#"+get_id).addClass('active');
                              $('#upload_div_'+get_id+'>input[data-file="hasfile_'+datasplit[1]+'"]').hide();
                              $('#upload_div_'+get_id+'>.file-select').append('<input type="file" name="reply_file[]" data-file="hasfile_'+sum_files+'" id="'+get_id+'" class="chooseFile">');
                              var all_files = $('#upload_div_'+get_id+'>.image_output>img').length;
                              $("#noFile_"+get_id).text(all_files+" files");
                            }
                          });

                  }
                }
              }
            })
           }
          }      
    }
 $('.select_mail').change(
    function(){
        if ($('input:checkbox').is(':checked')) {
            $('.select_mail').not(this).prop('checked', false);    
            var id = $(this).attr('id');
            var idsplit = id.split('_');
            if(idsplit[1]){
              $.ajax({
                url : '<?php echo site_url(); ?>mail/get_mail/'+idsplit[1],
                success: function(result) {
                  if(result){
                      result = $.parseJSON(result);
                      if (result.user_id != null || result.user_id != '') {
                        $('.mail_third_option_div .panel-group .panel').remove();
                        $('.mail_third_option_div .unread_msg_div').remove();
                        $("#file_upload_firstdiv>.image_output>img").remove();
                        $("#file_upload_secdiv>.image_output>img").remove();
                        $("#noFile").html("No file chosen...");
                        $("#noFile.noFile_from_out").html("No file chosen...");             
                        $("#noFile.noFile_from_in").html("No file chosen...");
                        for (var i = result.mails.length - 1; i >= 0; i--) {
                            var row_data = '<div id="main_div_'+result.mails[i].id+'"><div class="unread_msg_div" id="'+result.mails[i].id+'">';
                            if (result.mails[i].mail_status == '1') {
                              row_data += 'Unread';
                            }
                            row_data += '</div><div class="mails_panel selected panel panel-default" id="'+result.mails[i].id+'"><div class="panel-heading"><h4 class="panel-title view_mail" id="'+result.mails[i].id+'" onclick="fortest()" data-toggle="collapse" data-parent="#accordion" href="#collapse'+result.mails[i].id+'"><a><span class="mail_heading">From '+result.mails[i].name+'</span></a><span class="float_right">'+result.mail_time[i]+' ago <a class="trash_mail" id="'+result.mails[i].id+'"><i class="fa fa-trash-o"></i></a></span></h4></div><div id="collapse'+result.mails[i].id+'" class="panel-collapse collapse"><div class="panel-body"><div class="recieved_row"><div class="mail_body"><div class="">'+result.mails[i].mail+'</div>';
                            row_data += '</div></div></div>';
                            row_data += '<div class="panel-body panel-body_attachment_div mail_attachment">';
                            if (result.mails[i].attachment!=null && result.mails[i].attachment!='') {
                              var my_data=result.mails[i].attachment.split(',');
                              for (var a = my_data.length - 1; a >= 0; a--) {
                               row_data+= '<a href="<?php echo base_url() ?>upload/mail/'+my_data[a]+'" class="download_attachment" download><i class="fa fa-download"></i><img class="attachment_image img img-thumbnail" src="<?php echo base_url() ?>upload/mail/'+my_data[a]+'"></a>';
                             }
                            }
                          row_data+= '</div><div class="panel-body"><div class="sent_row"></div><div class="reply_box"><form role="form" action="<?php echo base_url(); ?>mail/reply_mail/" class="reply-form-after-select send_mail_form reply-form" name="myForm" method="POST" enctype="multipart/form-data"><textarea class="" rows="10" name="reply_data"></textarea><input type="hidden" name="user_to" value="'+idsplit[1]+'"><input type="hidden" name="mail_id" value="'+result.mails[i].message_id+'"> <div class="file-upload file-upload_div" id="reply_seconddiv_home_'+result.mails[i].id+'"><div class="file-select"><div class="file-select-button" id="fileName">Choose File</div><div class="file-select-name" id="noFile_'+result.mails[i].id+'">No file chosen...</div> <input type="file" name="reply_file[]" data-file="hasfile_0" class="chooseFile reply_attachment_after_select" id="'+result.mails[i].id+'"></div><div class="image_output row"></div></div><div class="reply_button"><input type="submit" id="reply" value="Reply" name="reply"><input type="reset" value="Cancel"></div></form></div></div></div></div></div>';
                            $('.mail_third_option_div .panel-group').append(row_data);
                          }
                           $('.send_mail_box').show();
                           $('.send_mail_form').show();
                           $('.send_mail_form .send_mail_box #user_id').val(result.user_id);
                           $('.send_mail_to_all').hide();
                           $('.create_message').hide();
                           $('#accordion').show();

                           $(document.body).on('change', 'input[name^="reply_file[]"]', function() {
                            var filename = [];
                            var get_id = $(this).attr('id');
                            var data = $(this).attr('data-file');
                            var datasplit = data.split('_');
                            var sum_files = parseInt(datasplit[1]);
                                sum_files += 1;
                            var rpl_images = '';
                            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                                  filename.push($(this).get(0).files[i].name);
                                  rpl_images += '<img width="100" height="100" src="'+URL.createObjectURL(event.target.files[i])+'" class="hasfile_'+sum_files+'" />';
                            }
                            if (/^\s*$/.test(filename)) {
                              $("#reply_seconddiv_home_"+get_id).removeClass('active');
                              $("#reply_seconddiv_home_"+get_id+">#noFile_"+get_id).html("No file chosen...");
                            }
                            else {
                              $('.reply-form-after-select #reply_seconddiv_home_'+get_id+' .image_output').append(rpl_images);
                              $('#reply_seconddiv_home_'+get_id+'>input[data-file="hasfile_'+datasplit[1]+'"]').hide();
                              $('#reply_seconddiv_home_'+get_id+'>.file-select').append('<input type="file" name="reply_file[]" data-file="hasfile_'+sum_files+'" id="'+get_id+'" class="chooseFile reply_attachment_after_select">');
                              var all_files = $('#reply_seconddiv_home_'+get_id+'>.image_output>img').length;
                              $('#reply_seconddiv_home_'+get_id+' #noFile_'+get_id).text(all_files+' files');
                            }
                          });
                    }
                  }
                }
              });
          }
        }
    });
  $(document).on('click','.view_mail',function(){
          var id = $(this).attr('id');
          if(id){
            $.ajax({
                url : '<?php echo base_url();?>Mail/read_single_mail/'+id,
                success: function(result) {
                    $('.unread_msg_div#'+id).html('');
                    $('.send_mail_box').toggle();
                }
            });
         }
        });
    $(document).on('click','.trash_mail',function(){
          if (confirm('Are you sure to want delete this mail ?')) { 
          var id = $(this).attr('id');
          if(id){
            $.ajax({
                url : '<?php echo base_url();?>Mail/delete_single_mail/'+id,
                success: function(data) {
                  var data = $.parseJSON(data);
                  $('#main_div_'+data.id).remove();
              }
            });
          }
         }
        });
        
        $(document).on('click','.delete_all_mail',function(){
          if (confirm('Are you sure to want delete All this mail ?')) { 
          var id = $(this).attr('id');
          var idsplit = id.split('-');
          var url = $('.headr a.category_link').attr('href');
          if(id){
            $.ajax({
                url : '<?php echo base_url();?>Mail/delete_all_mail/'+idsplit[0]+'/'+idsplit[1],
                success: function(result) {
                  window.location.href = url;
              }
            });
          }
         }
        });      
  
</script>