<section class="login-signup-page">    
	<div class="container">
    	<div class="row">
        	<div class="login-signBlock sucess-msg">
        		<h2>Success Page <span><i class="fa fa-check-circle" aria-hidden="true"></i></span></h2>
        		<h3>You have successfully subscribed <?php echo $getdetails[0]['email']; ?> for <?php echo $plan; ?> Membership Plan </h3>
        		<h4><?php echo $subscribe_amount; ?> amount will be deduct from your paypal account</h4>
          </div>
		</div>
	</div>
</section>

<script type="text/javascript">
    window.setTimeout(function(){
        window.location.href = "<?php echo base_url().'Membership/getMembershipPlan'; ?>";
	}, 4000);
</script>