<div class="adm-dashbord">
<div class="container padd0">
   <?php $this->load->view('dashboard/profile_sidebar_2.tpl.php');?>
   <div class="col-lg-10">
      <div class="col-lg-11 padd0">
         <div>
            <div class="col-lg-11">
               <div class="profile_top row">
                  <div class="col-sm-6 text-right">
                     <a class="profile_top_btn black" href="<?php echo base_url('user/myprofile'); ?>">My Profile</a>
                  </div>
                  <div class="col-sm-6">
                     <a class="profile_top_btn orange" href="<?php echo base_url('friend'); ?>">Dashboard</a>
                  </div>
               </div>
               <div class="myprofile-edit">
                  <h2 class="text-center">Pro Esports Player Membership</h2>
                  <div class="col-lg-12">
                    <div class="row">
                      
                        <?php if ($plan) { 
                          foreach ($plan as $key => $sl) {                            
                            $amount = number_format((float) $sl['amount'], 2, '.', '');
                           ?>
                            <div class="col-lg-4 mb40">
                              <div class="subscrib_btn text-center mb15">
                                <a class="lobby_btn membership_plan" membership_plan_id ="<?php echo $sl['id'];?>">
                                  Info
                                </a>
                              </div>
                              <?php if($active_plan == $sl['id']) { 
                                $action = 'Membership/inactiveMembershipPlan';
                              } else {
                                $action = 'Membership/activeMembershipPlan';
                              }
                              ?>
                              <form class="form_membership" action="<?php echo base_url().$action; ?>" method="POST">
                                   <input type="hidden" name="membership_subscribe_plan" value="<?php echo $sl['id']; ?>">
                                   <input type="hidden" name="title" value="<?php echo $sl['title']; ?>">
                                   <input type="hidden" name="fees" value="<?php echo $sl['fees']; ?>">
                                    <input type="hidden" name="time_duration" value="<?php echo $sl['time_duration']; ?>">
                                   
                                  <div class="subscrib_img">
                                    <img width="180" height="90" src="<?php echo base_url();?>upload/membership/<?php echo $sl['image']; ?>" alt="<?php echo $sl['image']; ?>" class="img img_margin ">
                                  </div>
                                  <div class="animated-box mb20"></div>
                                  <div class="subscrib_plan text-center mb20">
                                    <h4><?php echo ucwords($sl['title']); ?></h4>
                                  </div>
                                  <div class="subscrib_amnt text-center mb20">
                                    <h4>Amount : <?php echo $amount; ?></h4>
                                     <input type="hidden" name="membership_subscribe_amount" value="<?php echo $amount; ?>">
                                  </div>
                                  <!-- <div class="subscrib_btn text-center">
                                    <a class="lobby_btn subscribe_button">Update</a>
                                  </div> -->
                                  <div class="subscrib_btn text-center mb20" style="margin-top: 6px;">
                                    <?php if($active_plan == $sl['id']) { ?>
                                       <input type="submit" name="membership_subscription_opt" value="Unbscribe" class="btn cmn_btn">
                                       <input type="hidden" name="membership_subscribed_plan" value="<?php echo $active_plan; ?>">
                                       <input type="hidden" name="membership_payment_id" value="<?php echo $transaction_id; ?>">
                                    <?php } else { ?>
                                    <input type="submit" name="membership_subscription_opt" value="Subscribe" class="btn cmn_btn">
                                  <?php } ?>
                                  </div>
                              </form>
                            </div>
                          <?php }
                        } ?>
                      
                     
                      <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<style type="text/css">
  .mb20 {
    margin-bottom: 20px;
  }
  .mb15{
    margin-bottom: 15px;
  }
  .img_margin{
    margin:30px 0px;
  }
  .form_membership {
    border: 1px solid red;
    max-width: 170px;
    margin: 0px auto;
  }
  .mb40{
    margin-bottom: 40px;
  }
</style>

