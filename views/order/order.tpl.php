<?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
<div class="adm-dashbord">
	<div class="container padd0">
		<?php $this->load->view('dashboard/profile_sidebar.tpl.php'); ?>
		<div class="col-lg-10 common_product_list">
			<div class="col-lg-12">
				<div class="profile_top row">
					<div class="col-sm-6 text-right">
						<a class="profile_top_btn orange" href="<?php echo base_url('user/myprofile'); ?>">My Profile</a>
					</div>
					<div class="col-sm-6">
						<a class="profile_top_btn black" href="<?php echo base_url('friend'); ?>">Dashboard</a>
					</div>
				</div>
				<div class="myprofile-edit row">
					<div class="col-sm-12">
						<h2>My Orders</h2>
					</div>
					<div class="common_product_list col-sm-12">
						<ul class="c_product_list">
							<li>
								<div class="col-md-3"><label>Order ID</label></div>
								<div class="col-md-2"><label>Payment Method</label></div>
								<div class="col-md-2"><label>Total Items</label></div>
								<div class="col-md-2"><label>Paid Amount ($)</label></div>
								<div class="col-md-1"><label>process</label></div>
								<div class="col-md-2 text-right"><label>Action</label></div>
							</li>
							<?php if (!empty($order_list)) { 
								$i = 1;
								foreach ($order_list as $key => $ol) { ?>
									<li class="c_product_list_li font-18">
										<div class="col-md-3">
											<div><?php echo $ol->transaction_id; ?></div>
										</div>
										<div class="col-md-2">
											<div><?php echo $ol->payment_method; ?></div>
										</div>
										<div class="col-md-2">
											<div><?php echo $ol->count_order; ?></div>
										</div>
										<div class="col-md-2">
											<div><?php echo $ol->grand_total; ?></div>
										</div>
										<div class="col-md-1"><div><?php echo $ol->order_status ;?></div></div>
										<div class="col-md-2 c_prdct_chckt_btn text-right">
											<a class="cmn_btn view_order" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="View <br> Order" id="<?php echo $ol->id; ?>"><i class="fa fa-eye"></i></a>
											<!-- href="<?php echo base_url() ?>store/cancel_order/" -->
											<?php if ($ol->order_status == 'placed') { ?>
												<a class="cmn_btn cancel_order" data-order_id="<?php echo $ol->id; ?>" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Cancel <br> Order">Cancel</a>
											<?php } ?>
										</div>
									</li>
								<?php $i++; }
							} else { ?>

							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
