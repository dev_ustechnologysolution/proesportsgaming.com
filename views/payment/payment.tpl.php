
<div class="adm-dashbord">
	<div class="container padd0">
		<?php $this->load->view('dashboard/profile_sidebar.tpl.php');?>
		<div class="col-lg-10">
			<div class="col-lg-11">
				<div>
					<div class="col-lg-11">
						<div class="myprofile-edit">
							<h2>Game Information</h2>
							<div class="col-lg-12">
								<?php
									$paypal_url = $this->paypal_lib->paypal_url;
									$paypal_id = $this->paypal_lib->business;
								?>
								<form action="<?php echo $paypal_url ?>" name="frmPaypal" method="post" id="frmPaypal">
									<div class="form-group">
										<input type="hidden" name="cmd" value="_xclick">
									</div>
									<div class="form-group">
										<input type="hidden" name="business" value="<?php echo $paypal_id ?>">
										<input type="hidden" name="invoice" value="<?php echo $info ?>">
									</div>
									<div class="form-group">
										<label class="col-lg-3">Game Name:</label>
										<div class="input-group col-lg-9">
											<label><?php echo $this->session->userdata('game_name'); ?></label>
										</div>
										<input type="hidden" name="item_name" value="<?php echo $this->session->userdata('game_name');?>">
									</div>
									<div class="form-group">
										<label class="col-lg-3">Game Price:</label>
										<div class="input-group col-lg-9">
											<label>$ <?php echo $this->session->userdata('game_price');?></label>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3">Game Image:</label>
										<div class="input-group col-lg-9">
											<?php if($this->session->userdata('user_id') != $this->session->userdata('user_id')) { ?>
												<label><img src="<?php echo base_url().'upload/game/'.$this->session->userdata('game_image') ?>" width="100" height="100"> </label> <?php } else {?>
												<label><img src="<?php echo base_url().'upload/game/'.$this->session->userdata('game_image') ?>" width="100" height="100"> </label>
											<?php } ?>
										</div>
									</div>
									<!--<input type="hidden" name="item_number" value="">-->
									<div class="form-group">
										<input type="hidden" class="form-control" name="amount" value="<?php echo $this->session->userdata('game_price');?>" readonly>
									</div>
									  <!--<input type="hidden" name="tax" value="">
                                      <input type="hidden" name="quantity" value="">
                                      <input type="hidden" name="no_note" value="">-->
									<div class="form-group">
										<input type="hidden" name="currency_code" value="USD">
									</div>
									<div class="form-group">
										<input type="hidden" name="cancel_return" value="<?php echo base_url('payment/failure');?>">
									</div>
									<div class="form-group">
										<input type="hidden" name="return" value="<?php echo base_url('payment/success');?>" />
									</div>
									<!-- Where to send the PayPal IPN to. -->
									<input type="hidden" name="notify_url" value="<?php echo base_url('payment/success');?>" />
									<!--  <input type="hidden" name="address_override" value="1">
                                      <input type="hidden" name="first_name" value="John">
                                      <input type="hidden" name="last_name" value="Doe">
                                      <input type="hidden" name="address1" value="345 Lark Ave">
                                      <input type="hidden" name="city" value="San Jose">
                                      <input type="hidden" name="state" value="CA">
                                      <input type="hidden" name="zip" value="95121">
                                      <input type="hidden" name="country" value="US">-->
									<div class="form-group">
										<p>To complete this payment, Please pay with Paypal</p>
									</div>
									<div class="form-group" style="width:115px; margin:0 auto;">
										<input type="image" name="submit"
											   src="https://www.paypalobjects.com/webstatic/en_US/btn/btn_pponly_142x27.png"
											   alt="PayPal - The safer, easier way to pay online">
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

									<!--<input type="hidden" name="item_number" value="">-->
									<div class="form-group">
										<input type="hidden" class="form-control" name="amount" value="//Replace13" readonly>
									</div>
									<!--  <input type="hidden" name="tax" value="">
                                      <input type="hidden" name="quantity" value="">
                                      <input type="hidden" name="no_note" value="">-->
									<div class="form-group">
										<input type="hidden" name="currency_code" value="USD">
									</div>
									<div class="form-group">
										<input type="hidden" name="cancel_return" value="//Replace14">
									</div>
									<div class="form-group">
										<input type="hidden" name="return" value="//Replace15" />
									</div>
									<!-- Where to send the PayPal IPN to. -->
									<input type="hidden" name="notify_url" value="//Replace16" />
									<!--  <input type="hidden" name="address_override" value="1">
                                      <input type="hidden" name="first_name" value="John">
                                      <input type="hidden" name="last_name" value="Doe">
                                      <input type="hidden" name="address1" value="345 Lark Ave">
                                      <input type="hidden" name="city" value="San Jose">
                                      <input type="hidden" name="state" value="CA">
                                      <input type="hidden" name="zip" value="95121">
                                      <input type="hidden" name="country" value="US">-->
									<!-- <div class="form-group">
										<p>To complete this payment, Please pay with Paypal</p>
									</div>
									<div class="form-group" style="width:115px; margin:0 auto;">
										<input type="image" name="submit"
											   src="https://www.paypalobjects.com/webstatic/en_US/btn/btn_pponly_142x27.png"
											   alt="PayPal - The safer, easier way to pay online">
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> -->