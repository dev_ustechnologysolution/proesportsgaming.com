<section class="login-signup-page">    
	<div class="container">
    	<div class="row">
        	<div class="login-signBlock sucess-msg">
        		<h2>Success Page <span><i class="fa fa-check-circle" aria-hidden="true"></i></span></h2>
      			<h3>Welcome, <?php echo $this->session->userdata('user_name'); ?></h3>
          		<h4><?php echo $dt; ?></h4>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
    window.setTimeout(function(){
        window.location.href = "<?php echo base_url().'dashboard' ?>";
	}, 3000);
</script>