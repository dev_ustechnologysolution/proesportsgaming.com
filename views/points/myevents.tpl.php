<div class="adm-dashbord">
   <div class="container padd0">
      <?php $this->load->view('dashboard/profile_sidebar.tpl.php'); ?>
      <div class="col-lg-10">
         <div class="col-lg-11">
            <div class="profile_top row">
               <div class="col-sm-6 text-right">
                  <a class="profile_top_btn orange" href="<?php echo base_url('user/myprofile'); ?>">My Profile</a>
               </div>
               <div class="col-sm-6">
                  <a class="profile_top_btn black" href="<?php echo base_url('friend'); ?>">Dashboard</a>
               </div>
            </div>

            <div class="row bidhottest mygamelist">
               <h2>Tournaments</h2>
               <?php if(isset($tournaments) && !empty($tournaments))
                  { 
                      if(count($tournaments)>0)
                      { ?>
               <ul>
               <?php foreach($tournaments as $friend) { ?>
              
                  <li>
                     <div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
                        <img class="game-image" src="<?php echo base_url().'upload/game/'. $friend['game_image'];?>" alt="">
                     </div>
                     <div class="col-lg-3 col-sm-6 col-md-3 bid-info">
                        <h3 class="game_name"><?php echo $friend['game_name']; ?></h3>
                        <p class="sub_game_name_list"><?php echo $friend['sub_game_name']; ?></p>
                        <h3 class="tag">Tag: <?php echo $friend['device_id']; ?></h3>
                     </div>
                     <div class="col-lg-2 col-sm-3 col-md-2 uplod-day">
                        <h4><?php //echo time_elapsed_string(strtotime($value['created_at']));?></h4>
                        <?php if($friend['game_timer'] != '' && $friend['game_timer'] != "0000-00-00 00:00:00"){ $diff = strtotime($friend['game_timer'])- time();  if(time() < strtotime($friend['game_timer'])){ ?>
                        <?php } ?>
                        <h3 class="timer custom_timer" data-seconds-left="<?php echo $diff; ?>"></h3>
                        <?php } ?>
                        <h5 class="gamesize">Gamesize: --<?php echo $friend['game_type']; ?>V<?php echo $friend['game_type']; ?>-- </h5>
                     </div>
                     <div class="col-lg-2 col-sm-4 col-md-2 divPrice">
                        <div class="game-price premium">
                           <h3><?php echo CURRENCY_SYMBOL.$friend['price']; ?></h3>
                        </div>
                     </div>
                     <div class="col-lg-3 col-sm-4 col-md-2">
                        <div class="game-status">
                           <div class="primum-play play-n-rating">
                              <?php if ($friend['game_password']!='' && $friend['game_password']!=null) { ?>
                              <a class="button" href="<?php echo base_url().'home/gamePwAuth/'.$friend['id'];?>">PLAY</a>
                              <?php } else { ?> 
                              <a class="button" href="<?php echo base_url().'home/gameAuth/'.$friend['id'];?>">PLAY</a>
                              <?php } ?>
                              <form action="<?php echo base_url().'dashboard/cancelChallenge/'.$friend['id']?>" class="has-validation-callback">
                                 <button class="button reject_challenge" type="submit">REJECT</button>
                              </form>
                           </div>
                        </div>
                     </div>
                  </li>              
               <?php } ?>
               </ul>
               <?php } else { echo "<h4>No data Found</h4>"; } } ?>
            </div>

            <div class="row bidhottest mygamelist myevelist">
               <h2>Live Events</h2>
               <?php
                  if(isset($live_events) && !empty($live_events)) { 
                  	if(count($live_events)>0) { 
                  ?>
               <ul>
                  <?php foreach($live_events as $value) { 
                     // echo "<pre>"; print_r($value);
                     if($this->session->userdata('user_id') == $value['user_id']) { 
                     ?>
                  <li>
                     <div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
                        <img src="<?php echo base_url().'upload/game/'. $value['game_image'];?>" alt="">
                     </div>
                     <div class="col-lg-3 col-sm-6 col-md-3 bid-info">
                        <h3 class="text-center"><?php echo $value['game_name']; ?></h3>
                        <p class="sub_game_name_list"><?php echo $value['sub_game_name']; ?></p>
                     </div>                    
                     <div class="col-lg-2 col-sm-4 col-md-2 divPrice" style="margin-top:16px;">
                        <div class="game-price premium">
                           <h3><?php echo ($value['type'] == 1) ? CURRENCY_SYMBOL.$value['event_price'] : CURRENCY_SYMBOL.$value['spectate_price']; ?></h3>
                        </div>
                     </div>
                      <div class="col-lg-2 col-sm-4 col-md-2 divPrice">
                         <?php $img = ($value['type'] == 1) ? 'event_ticket.png' : 'spectotar_ticket.png'; 
                         $alt = ($value['type'] == 1) ? 'Event Ticket' : 'Spectotar Ticket';?>
                        <img title="<?php echo $alt;?>" src="<?php echo base_url().'upload/banner/'.$img; ?>" width="150" height="110">
                     </div>
                     <div class="col-lg-3 col-sm-5 col-md-3 primum-play play-n-rating <?php echo ($value['type'] == 2 && $value['is_event'] == 1) ? 'mt16' : 'mt36';?>" >
                        <div style="height: 48px;">
                           <a href="<?php echo base_url();?>livelobby/watchLobby/<?php echo $value['lobby_id']; ?>" class="animated-box">Live Event</a>
                        </div>
                        <?php if($value['type'] == 2 && $value['is_event'] == 1) { ?>

                        <a  id="<?php echo $value['lobby_id']; ?>" class="animated-box update_ticket">Upgrade Ticket</a>
                        <?php } ?>
                     </div>
                  </li>
                  <?php } } ?>
               </ul>            
               <?php } else { echo "<h4>No data Found</h4>"; } } ?>
            </div>

            <div class="row bidhottest mygamelist myevelist">
               <h2>Gifted Events Received By</h2>
               <?php
                  if(isset($gift_to) && !empty($gift_to)) { 
                  	if(count($gift_to)>0) { 
                  ?>
               <ul>
                  <?php foreach($gift_to as $single_gift_to) {                      
                     	$reciver_arr = $this->General_model->view_single_row('user_detail','user_id',$single_gift_to['user_id']);
                     ?>
                  <li>
                     <div class="col-lg-2 col-sm-2 col-md-2 bidimg">
                        <div class="pull-right">
                           <img src="<?php echo base_url(); ?>upload/profile_img/<?php echo $reciver_arr['image']; ?>" alt="">
                        </div>
                     </div>
                     <div class="col-lg-2 col-sm-2 col-md-2 padd0">
                        <div class="text-left">
                           <h4><?php echo ucfirst($reciver_arr['display_name']) ? ucfirst($reciver_arr['display_name']) : ucfirst($reciver_arr['name']); ?> </h3>
                           <h4>ACC# <?php echo $reciver_arr['account_no']; ?></h4>
                           <span style="bottom: 0px;font-size: 16px;" class="postby"><?php echo $reciver_arr['team_name']; ?></span>
                        </div>
                     </div>
                     <div class="col-lg-2 ">
                        <img style="border-radius: 50%;width: 96%;margin-top:6px;" height="100" src="<?php echo base_url().'upload/game/'. $single_gift_to['game_image'];?>"> 
                     </div>
                     <div class="col-lg-2 primum-play padd0">
                        <h4 class="sub_game_name_list"><?php echo $single_gift_to['game_name']; ?></h4>
                        <p class="sub_game_name_list"><?php echo $single_gift_to['sub_game_name']; ?></p>
                        <h4>Price :<?php echo ($single_gift_to['type'] == 1) ? CURRENCY_SYMBOL.$single_gift_to['event_price'] : CURRENCY_SYMBOL.$single_gift_to['spectate_price']; ?></h4>
                     </div>
                     <div class="col-lg-2 col-sm-4 col-md-2 divPrice padd0">
                         <?php $img = ($single_gift_to['type'] == 1) ? 'event_ticket.png' : 'spectotar_ticket.png'; 
                         $alt = ($single_gift_to['type'] == 1) ? 'Event Ticket' : 'Spectotar Ticket';?>
                        <img title="<?php echo $alt;?>" src="<?php echo base_url().'upload/banner/'.$img; ?>" width="150" height="110">
                     </div>
                     <!-- <div class="col-lg-2 col-sm-4 col-md-2 divPrice" style="width:16%;margin-top:16px;">
                        <div class="game-price premium">
                           <h3><?php echo CURRENCY_SYMBOL.$single_gift_to['event_price']; ?></h3>
                        </div>
                     </div> -->
                     <div class="col-lg-3 col-sm-5 col-md-3 primum-play play-n-rating padd0 <?php echo ($single_gift_to['type'] == 2 && $single_gift_to['is_event'] == 1) ? 'mt16' : 'mt36';?>"style="width:16%;">
                        <div style="height: 48px;">
                        <a href="<?php echo base_url();?>livelobby/watchLobby/<?php echo $single_gift_to['lobby_id']; ?>" class="animated-box">Live Event</a>
                        </div>
                        <?php if($single_gift_to['type'] == 2 && $single_gift_to['is_event'] == 1) { ?>
                           
                        <a id="<?php echo $single_gift_to['lobby_id']; ?>" class="animated-box update_ticket">Upgrade Ticket</a>
                        <?php } ?>
                     </div>
                  </li>
                  <?php } ?>
               </ul>            
               <?php } else { echo "<h4>No data Found</h4>"; } 
            } ?>
            </div>

            <div class="row bidhottest mygamelist myevelist">
               <h2>Gifted Events To</h2>
               <?php
                  if(isset($gift_giver) && !empty($gift_giver)) { 
                  	if(count($gift_giver)>0) { 
                  ?>
               <ul>
                  <?php foreach($gift_giver as $single_gift_from) { 
                     
                     $sender_arr = $this->General_model->view_single_row('user_detail','user_id',$single_gift_from['gift_to']);
                     	 
                     ?>
                  <li>
                     <div class="col-lg-2 col-sm-2 col-md-2 bidimg ">
                        <img src="<?php echo base_url(); ?>upload/profile_img/<?php echo $sender_arr['image']; ?>" alt="">
                     </div>
                     <div class="col-lg-2 col-sm-2 col-md-2 padd0">
                        <h4><?php echo ucfirst($sender_arr['display_name']) ? ucfirst($sender_arr['display_name']) : ucfirst($sender_arr['name']); ?> </h4>
                        <h4>ACC# <?php echo $sender_arr['account_no']; ?></h4>
                        <span style="bottom: 0px;font-size: 16px;" class="postby"><?php echo $sender_arr['team_name']; ?></span>
                     </div>
                     <div class="col-lg-2 ">
                        <img style="border-radius: 50%;width: 96%;margin-top:6px;" height="100" src="<?php echo base_url().'upload/game/'. $single_gift_from['game_image'];?>"> 
                     </div>
                     <div class="col-lg-2 primum-play padd0">
                        <h4 class="sub_game_name_list"><?php echo $single_gift_from['game_name']; ?></h4>
                        <p class="sub_game_name_list"><?php echo $single_gift_from['sub_game_name']; ?></p>
                     </div>
                      <div class="col-lg-2 col-sm-4 col-md-2 divPrice padd0">
                         <?php $img = ($single_gift_from['type'] == 1) ? 'event_ticket.png' : 'spectotar_ticket.png'; ?>
                        <img src="<?php echo base_url().'upload/banner/'.$img; ?>" width="150" height="110">
                     </div>
                     <div class="col-lg-2 col-sm-4 col-md-2 divPrice" style="margin-top:16px;">
                        <div class="game-price premium">
                           <h3><?php echo ($single_gift_from['type'] == 1) ? CURRENCY_SYMBOL.$single_gift_from['event_price'] : CURRENCY_SYMBOL.$single_gift_from['spectate_price'] ?></h3>
                        </div>
                     </div>
                  </li>
                  <?php } ?>
               </ul>
            
               <?php } else { echo "<h4>No data Found</h4>"; } } ?>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- </div>
</div>
</div>
</div> -->
<style type="text/css">
   .mt16 {
      margin-top: 16px !important;
   }
   .mt36 {
      margin-top: 36px !important;
   }
   p.modal_h1_msg.text-left.update_event_ticket {
    font-size: 30px;
    text-align: center;
}
</style>
<script type="text/javascript">
  
</script>