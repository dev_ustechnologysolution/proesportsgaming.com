<div class="adm-dashbord">
	<div class="container padd0">
		<?php $this->load->view('dashboard/profile_sidebar.tpl.php');?>
		<div class="col-lg-10">
			<div class="col-lg-11">
				<div>
				<div class="profile_top row">
                            <div class="col-sm-6 text-right">
                            	<a class="profile_top_btn orange" href="<?php echo base_url('user/myprofile'); ?>">My Profile</a>
                            </div>
                            <div class="col-sm-6">
                            	<a class="profile_top_btn black" href="<?php echo base_url('friend'); ?>">Dashboard</a>
                            </div>
                        </div>
					<div class="col-lg-11">
						<div class="myprofile-edit">
							<h2>Package Information</h2>
							<div class="col-lg-12">
								<?php
									$paypal_url = $this->paypal_lib->paypal_url;
									$paypal_id = $this->paypal_lib->business;
								?>
								<form action="<?php echo site_url() ?>points/payment_paypal" name="frmPaypal" method="post" id="frmPaypal">
									<div class="form-group">
										<input type="hidden" name="cmd" value="_xclick">
										<input type="hidden" name="custom" value='<?php echo $custom ?>'>
									</div>
									<div class="form-group">
										<input type="hidden" name="business" value="<?php echo $paypal_id ?>">
										<input type="hidden" name="invoice" value="<?php echo $info ?>">
										<input type="hidden" name="paypal_url" value="<?php echo $paypal_url ?>">
									</div>
									<div class="form-group">
										<label class="col-lg-3">Package Name:</label>
										<div class="input-group col-lg-9">
                                            <label><?php echo $selectedPackage['point_title'];?></label>
										</div>
										<input type="hidden" name="item_name" value="<?php echo $selectedPackage['point_title'];?>">
									</div>
									<div class="form-group">
										<label class="col-lg-3">Amount:</label>
										<div class="input-group col-lg-9">
											<label> <?php echo '$ '.number_format($selectedPackage['amount'],2,".",",");?></label>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3">Fee:</label>
										<div class="input-group col-lg-9">
											<label> <?php echo '$ '.number_format($collected_fees,2,".",",");?></label>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-lg-3">Description:</label>
										<div class="input-group col-lg-9">
											<label> <?php echo $selectedPackage['point_description'];?></label>
										</div>
									</div>

									<div class="form-group">
										<input type="hidden" class="form-control" name="amount" value="<?php echo $selectedPackage['amount_tobe_paid'];?>" readonly>
									</div>
									  
									<div class="form-group">
										<input type="hidden" name="currency_code" value="USD">
									</div>
									<div class="form-group">
										<input type="hidden" name="cancel_return" value="<?php echo base_url('payment/failure'); ?>">
									</div>
									<div class="form-group">
										<input type="hidden" name="return" value="<?php echo base_url('points/payment_success'); ?>" />
									</div>
									<!-- Where to send the PayPal IPN to. -->
									<input type="hidden" name="notify_url" value="<?php echo base_url('points/payment_ipn'); ?>" />
									
									<div class="form-group">
										<p>To Complete This Payment</p>
									</div>
									<div class="row">
									<div class="form-group col-lg-6">
										<span class="paypal_title text-center" style="width: 436px;">Pay with Paypal</span><input type="image" class="paypal_input1 pull-right" name="submit"
											   src="<?php echo base_url().'upload/paypal_img/paypal-buttons-peg.png';?>"
											   alt="PayPal - The safer, easier way to pay online">
									</div>
											<div class="form-group col-lg-6">
										<span class="paypal_title">Pay with Credit/Debit Card</span>
										<!-- <input class="paypal_input1" type="image" name="submit"
											   src="<?php echo base_url().'upload/paypal_img/paypal-buttons-peg2.png';?>"
											   alt="PayPal - The safer, easier way to pay online"> -->
											  <input style="margin-top: 4px;"class="paypal_input1" type="image" name="submit"
											   src="<?php echo base_url().'upload/paypal_img/paypal-buttons-peg3.png';?>"
											   alt="PayPal - The safer, easier way to pay online">
										
									</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style type="text/css">
	.paypal_title {
		display: block;
		margin-bottom: 15px;
		font-size: 20px;
	}
	.paypal_input {
    display: block;
    width: 160px;
    height: 42px;
	}
</style>