<section class="body-middle innerpage">
    <div class="container">

        <!-- PcTab Game Challange -->
        <div class="row header1">
            <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
            <div class="col-lg-4 col-sm-4 col-md-4"><h2>Ps4 Challenges</h2></div>
            <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
        </div>
         <div class="row filter-panel">
          <div class="col-lg-12">
          <div class="form-group">
          <div class="has-validation-callback home_page_search_tabbing">
                <div class="col-lg-12">
                    <div class="form-group">
                        <form name="frm_pctab_challange" id="frm_pctab_challange" action="<?php echo base_url()?>Ps4/index" method="POST">
                            <?php if(!empty($challangetDp['data'])) { ?>
                                <select name="challange_id" id="challange_id" class="form-control" onchange="this.form.submit()">
                                    <option value="">Please select a Game</option>
                                    <?php foreach($challangetDp['data'] as $k=>$v) { ?>
                                        <option value="<?php echo $v['id'];?>" <?php if($challangeDpPoser==$v['id']){ echo "selected='selected'"; }?> >
                                            <?php echo $v['game_name'];?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <select class="form-control" name="sub_game-name" id="sub_game-name" onchange="this.form.submit()">
                                    <option value="">Please select a Subgame</option>
                                    <?php foreach($challangetDp['data'] as $k=>$v) { ?>
                                        <option value="<?php echo $v['sub_game_name'];?>" <?php if($challangeSubGame==$v['sub_game_name']){ echo "selected='selected'"; }?> >
                                            <?php echo $v['sub_game_name'];?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <select class="form-control" name="game_type"  id="game_type" onchange="this.form.submit()">
                                    <option value="">Please select a Gamesize</option>
                                    <?php
                                    foreach($challangetDp['data'] as $k=>$v) 
                                    { 
                                    $game_type_raw = explode('|',$v['game_type']);
                                    foreach($game_type_raw as $j)
                                    {
                                    ?>
                                    <option value="<?php echo $j;?>" <?php if($challangeGameSize==$j){ echo "selected='selected'"; }?> >
                                            <?php echo '--' . $j . 'V' . $j . '--';?>
                                        </option>
                                        <?php
                                    }
                                    }
                                    ?>
                                    </select>
                               
                             
                        </form>
                        <form name="frm_pctab_challange" id="frm_pctab_challange" action="<?php echo base_url()?>Ps4/index" method="POST">
                          <div class="input-group add-on form-control" style="float: right;">
                            <input placeholder="Search" name="srch-term" id="srch-term" type="text">
                            <div class="input-group-btn">
                              <button class="btn btn-default" onchange="this.form.submit()" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                            </div>
                        </form>
                           
                                    <?php
                                }
                            ?>
                    </div>
                </div>
            </div>
          </form>
        </div>
      </div>
    </div>

         


        <div class="row filterGame-row">

            <div class="bidhottest hottestgame">
                <ul>
                    <?php if(count($playstore_game['data'])>0) {
                        foreach($playstore_game['data'] as $k=>$val) { ?>
                            <li>
                                <div class="col-lg-2 col-sm-3 col-md-2 bidimg padd0">
                                    <img src="<?php echo base_url().'upload/game/'. $val['game_image'];?>" alt="">
                                </div>
                                <div class="col-lg-3 col-sm-6 col-md-3 bid-info">
                                    <h3><?php echo $val['game_name']; ?></h3>
                                    <p><?php echo $val['sub_game_name']; ?></p>
                                    <span class="postby">Tag: <?php echo $val['device_id']; ?></span>
                                </div>
                                <div class="col-lg-2 col-sm-3 col-md-2 uplod-day">
                                    
                                    <?php if($val['game_timer'] != '' && $val['game_timer'] != "0000-00-00 00:00:00"){ $diff = strtotime($val['game_timer'])- time();  if(time() < strtotime($val['game_timer'])){ ?>
                                    <script type="application/javascript">
                                        var myCountdown1 = new Countdown({
                                                time: <?php echo $diff; ?>, // 86400 seconds = 1 day
                                                width:120, 
                                                height:45,  
                                                rangeHi:"minute",
                                                style:"flip"    // <- no comma on last item!
                                            });

                                    </script>
                                    <?php } } ?>
                                    <p style="margin-top:15px;">Gamesize: <?php echo "--".$val['game_type']."V".$val['game_type']."--";?></p>
                                </div>
                                <div class="col-lg-2 col-sm-4 col-md-2 divPrice">
                                    <div class="game-price premium">
                                        <h3><?php echo CURRENCY_SYMBOL.$val['price']; ?></h3>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-5 col-md-3 primum-play play-n-rating">
                                    <a href="<?php echo base_url().'home/gameAuth/'.$val['id'];?>">play</a>
                                    <span class="glyphicon glyphicon-star"></span>
                                </div>
                            </li>
                        <?php } } else { echo "<h5 style='color: #FF5000;'>Hottest Games Not Available</h5>"; }?>


                </ul>
            </div>

        </div>

        <div class="row pager-row">
            <div class="game_paging">

                <?php echo $paging_two; ?>
            </div>
        </div>

       


    </div>

</section>

<div style="font-size: 30px; color: #F29F21; text-align:center;">Architizer A+Awards 2019 Finalist</div>
<div style="font-size: 18px; text-align:center;">
    <div>This award program celebrates the best architecture and design projects around the world. And we are the only Finalist from the Philippines this year! Please support us by sharing and voting</div>
    <div style="font-size: 13px;">Deadline:</div>
    <div><b>July 5, 12AM PST</b> or</div>
    <div><b>July 6, 3PM GMT+8/MNL</b></div>
    <div><a href="" style="color: #F29F21; font-size: 20px;">VOTE FOR US HERE</a></div>
</div>
<br>
<div style="font-size: 18px; text-align:center; color: #2B6D63;">
    <div>Step-by-Step Guide available on our Facebook Page.</div>
    <div><a href="">CLICK HERE</a></div>
</div>
<br>
<div style="font-size: 18px; text-align:center;">
    <div>Follow us on Facebook and Instagram</div>
    <div>
        <ul>
            <li class="" style="widows: 25%; float: left">
                <a href=""><i class="qode_icon_font_awesome fa fa-facebook-square"></i></a>
            </li>
            <li class="" style="widows: 25%; float: left">
                <a href=""><i class=" fa fa-instagram"></i></a>
            </li>
            <li class="" style="widows: 25%; float: left">
                <a href=""><i class=" fa fa-link"></i></a>
            </li>
            <li class="" style="widows: 25%; float: left">
                <a href=""><i class=" fa fa-envelope"></i></a>
            </li>
        </ul>
    </div>
</div>