 <section class="body-middle innerpage">
<div class="container">
        <div class="row header1">
            <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
            <div class="col-lg-4 col-sm-4 col-md-4"><h2><?php echo $page[0]['title'];?></h2></div>
            <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
        </div>
        
        <div class="row">
         
            <div class="col-lg-4"></div>
            <div class="col-lg-12">
            	<p><?php echo $page[0]['content']; ?></p>
            </div>
        </div>
        
    </div>
</section>
