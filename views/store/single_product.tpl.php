<section class="body-middle innerpage">
    <?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
    <div class="container">
        <div class="row header1">
            <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
            <div class="col-lg-4 col-sm-4 col-md-4"><h2>store</h2></div>
            <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
        </div>
        <?php $this->load->view('common/product_filter.tpl.php'); ?>    
        <div class="row product_single_page">
        	<div class="col-sm-2 padd0 small-img">
                <div class="img_arr small-container">
                    <ul class="prdct_img_ul" id="small-img-roll">
        			<?php
                     if (!empty($prdct_img_arr)) { ?>
                        <?php foreach ($prdct_img_arr as $key => $p_img) { ?>
        					<li class="prdctimg <?php echo ($p_img == $default_sel_img) ? 'active' : ''; ?>" data-tg_img="<?php echo $p_img; ?>">
                                <img class="prdct_thmbnl img img-responsive show-small-img" src="<?php echo base_url().'upload/products/'.$p_img; ?>" alt="<?php echo ($p_img == $default_sel_img) ? 'now' : ''; ?>">
        					</li>
        				<?php } ?>
        			<?php } ?>
        			</ul>
                    <?php if (!empty($prdct_img_arr) && count($prdct_img_arr) > 1) { ?>
                        <div id="prev-img" class="cr_arrw icon-left"><span class="sldr_spn top_spn"><i class="fa fa-chevron-circle-up"></i></span></div>
                        <div id="next-img" class="cr_arrw icon-right"><span class="sldr_spn bottom_spn"><i class="fa fa-chevron-circle-down"></i></span></div>
                    <?php } ?>
        		</div>
        	</div>
            <div class="col-sm-4">
                <div class="product_main_img show zoom" href="<?php echo base_url().'upload/products/'.$default_sel_img; ?>">
                    <img class="img img-responsive" src="<?php echo base_url().'upload/products/'.$default_sel_img; ?>" alt="<?php echo $default_sel_img; ?>" id="show-img">
                </div>
            </div>
            <div class="col-sm-6 padd0 prdct_right-side">
                <div class="product_title_div"> <?php echo $product_detail[0]->name; ?> </div>
                <div class="product_shortdesc_div"> <h5><?php echo $product_detail[0]->description; ?></h5></div>
        		<div class="product_price_div margin-tpbtm-10">
        			<div>$ <span class="price"><?php echo number_format((float)$product_detail[0]->price, 2, '.', ''); ?></span></div>
        		</div>
                <form class="add-to-cart_form" method="post" action="<?php echo base_url(); ?>store/add_product_cart">
                    <div class="row addtocart_div">
                		<div class="select_prdct_qnt col-sm-4">
                            <button type="button" id="sub" class="sub cmn_btn"><i class="fa fa-minus"></i></button>
                			<input type="number" name="prdct_qntity" class="form-control prdct_qntity" min="1" pattern="[0-9]+" required value="1">
                            <button type="button" id="add" class="add cmn_btn"><i class="fa fa-plus"></i></button>
                		</div>
                        <div class="add_to_cart_div col-sm-5 pull-right">
                            <button class="cmn_btn" type="submit" name="add_to_cart"> Add to Cart </button>
                        </div>
                        <div class="add_to_wishlist_div col-sm-3 pull-right">
                            <button class="cmn_btn" type="submit" name="add_to_wishlist"> WISH LIST</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 prdct_fee">
                            <?php 
                            if ($product_detail[0]->fee != '' && $product_detail[0]->fee>0) { ?>
                                <div>Note : $ <?php echo number_format((float)$product_detail[0]->fee, 2, '.', ''); ?> fee will be count on each Product.</div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php if(count($attribute_list) > 0) { ?>
                        <div class="row selvariation_div margin-tpbtm-40">
                            <ul class="variation_ul">
                                <?php foreach($attribute_list as $key => $v) {
                                    $arr_attr = explode('_',$key);
                                    ?> 
                                    <li class="select_var <?php echo $arr_attr[0]; ?>_variation">
                                        <label>Choose <?php echo $arr_attr[0]; ?></label>
                                        <select class="form-control <?php echo $arr_attr[0]; ?>">
                                            <option value="" attribute_lable="<?php echo $arr_attr[0]; ?>">Please choose <?php echo $arr_attr[0]; ?></option>
                                            <?php foreach($v as $v1) {
                                                if($arr_attr[0] == $v1->title) { ?>
                                                    <option value="atr_<?php echo $v1->attr_id; ?>&optid_<?php echo $v1->option_id; ?>" attribute_id="<?php echo $v1->attr_id; ?>" attribute_lable="<?php echo $v1->title; ?>" option_id="<?php echo $v1->option_id; ?>" option_lable="<?php echo $v1->option_label; ?>"><?php echo $v1->option_label; ?> </option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    <?php } ?>

                    <input type="hidden" name="select_img" value="<?php echo ($default_sel_img) ? $default_sel_img : '';?>">
                    <input type="hidden" name="product_id" value="<?php echo ($product_id) ? $product_id : '';?>">
                    <input type="hidden" name="price" value="<?php echo number_format((float)$product_detail[0]->price, 2, '.', ''); ?>">
                    <input type="hidden" name="product_original_name" value="<?php echo number_format((float)$product_detail[0]->price, 2, '.', ''); ?>" class="originlprice">
                    <input type="hidden" name="product_fee" value="<?php echo($product_detail[0]->fee>0 && $product_detail[0]->fee !='')? number_format((float)$product_detail[0]->fee, 2, '.', '') :'';?>">
                    <input type="hidden" name="int_fee" value="<?php echo($product_detail[0]->int_fee>0 && $product_detail[0]->int_fee !='')? number_format((float)$product_detail[0]->int_fee, 2, '.', '') :'';?>">
                    <input type="hidden" name="product_name" value="<?php echo ($product_detail[0]->name) ? $product_detail[0]->name : ''; ?>">
                    <div class="selected_var"></div>
                </form>
        	</div>
            <?php if (!empty($product_ads)) { ?>
            <div class="col-sm-12 add_listing_div margin-tpbtm-40">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <a class="ads_container_desciption_div" href="<?php echo $product_ads->sponsered_by_logo_link; ?>" target="_blank">
                       <div class="ads_img_div col-sm-4">
                        <img src="<?php echo base_url() ?>upload/ads_master_upload/<?php echo $product_ads->upload_sponsered_by_logo; ?>" alt="<?php echo $product_ads->upload_sponsered_by_logo; ?>" class="img img-responsive">
                       </div>
                       <div class="ads_content_div col-sm-8">
                        <div class="ads_desciption"><?php echo $product_ads->sponsered_by_logo_description; ?></div>
                      </div>
                    </a>
                </div>
                <div class="col-md-3"></div>
            </div>
            <?php } ?>
            <?php if ($product_detail[0]->description !='') { ?>
                <div class="col-sm-12 pr_description_div">
                    <div class="desc_title margin-tpbtm-10">About the product</div>
                    <div class="desc_cntnt margin-tpbtm-10"> <?php echo $product_detail[0]->description; ?> </div>
                </div>
            <?php } ?>
            <?php if (!empty($product_ads)) {
                if ($product_ads->youtube_link != '' || $product_ads->custom_commercial !='') { ?>
                    <div class="col-sm-12 add_listing_div commercial_add_div margin-tpbtm-40">
                        <div class="col-sm-12 margin-tpbtm-40">
                            <div class="ads_title">
                                <h2><font color="FD5103">PRO </font><font color="FFC22418">ESPORTS </font><font color="FDFDFD">GAMiNG </font> Review</h2>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <?php if ($product_ads->youtube_link != '') { 
                                $iframe = $this->General_model->getYoutubeEmebedUrl($product_ads->youtube_link,1); ?>
                                <?php echo $iframe['embeded_iframe']; ?>
                            <?php } else if ($product_ads->custom_commercial != '') { ?>
                            <video class="commercial_ads" loop autoplay muted controls src="<?php echo base_url(); ?>upload/ads_master_upload/<?php echo $product_ads->custom_commercial; ?>" width="100%"></video>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</section>
