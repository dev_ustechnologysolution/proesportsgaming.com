<section class="body-middle innerpage">
    <?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
    <div class="container">
        <div class="row header1">
            <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
            <div class="col-lg-4 col-sm-4 col-md-4"><h2>Store</h2></div>
            <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
        </div>
        <?php $this->load->view('common/product_filter.tpl.php'); ?>    
        <div class="row">
            <div class="product_listing_first_div">
                <div class="count_product"><?php echo ($count_product !='') ? $count_product : ''; ?></div>
                <ul class="product_list">
                    <?php if (!empty($list)) { ?>
                        <?php 
                        $i = $last_id = 0;
                        foreach ($list as $key => $product) {
                            $last_id = $product['id'];
                            $image_arr = $this->General_model->view_single_row('product_images',array('is_default' => '1','product_id' => $product['id']),'image'); ?>
                            <?php if ($key % 5 == 0 && $key != 0) { ?>
                                <li class="ads row">
                                    <div class="add_listing_div">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">
                                            <a class="ads_container_desciption_div" href="<?php echo $store_ads_list[$i]->sponsered_by_logo_link; ?>" target="_blank">
                                               <div class="ads_img_div col-sm-4">
                                                <img src="<?php echo base_url() ?>upload/ads_master_upload/<?php echo $store_ads_list[$i]->upload_sponsered_by_logo; ?>" alt="<?php echo $store_ads_list[$i]->upload_sponsered_by_logo; ?>" class="img img-responsive">
                                               </div>
                                               <div class="ads_content_div col-sm-8">
                                                <div class="ads_desciption"><?php echo $store_ads_list[$i]->sponsered_by_logo_description; ?></div>
                                              </div>
                                            </a>
                                        </div>
                                        <div class="col-md-3"></div>
                                    </div>
                                </li>
                            <?php $i++; } ?>
                            <li class="product_<?php echo $product['id']; ?>">
                                <div class="main_product">
                                    <div class="product_img_dv"><img src='<?php echo base_url()."upload/products/".$image_arr["image"];?>' alt="<?php echo $image_arr["image"]; ?>" class="img img-responsive"></div>
                                    <div class="product_title_dv">
                                        <label class="title"><?php echo $product['name']; ?></label>
                                    </div>
                                    <div class="product_view_dv">
                                        <a class="view_product" href="<?php echo base_url(); ?>Store/view_product/<?php echo $product['id']; ?>"> View Item</a>
                                    </div>
                                </div>
                            </li>
                        <?php } ?>
                        <?php 
                        if (count($list) >= $itemperpage) { ?>
                            <li class="load_more_div row text-center">
                                <a class="load_more_btn"> Load More</a>
                                <input type="hidden" id="totalcount" value="<?php echo $get_last_id['last_id']; ?>">
                                <input type="hidden" id="all" value="<?php echo $last_id; ?>">
                            </li>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="prdctempty"> Products not availble </div>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</section>
