<?php $this->load->view('common/flash_msg_show.tpl.php'); ?>
<div class="adm-dashbord">
<div class="container padd0">
  <?php $this->load->view('dashboard/profile_sidebar_2.tpl.php');?>
  <div class="col-lg-10">
    <div class="col-lg-11 padd0">
      <div>
        <div class="col-lg-11">
          <div class="profile_top row">
            <div class="col-sm-6 text-right">
              <a class="profile_top_btn black" href="<?php echo base_url('user/myprofile'); ?>">My Profile</a>
            </div>
            <div class="col-sm-6">
              <a class="profile_top_btn orange" href="<?php echo base_url('friend'); ?>">Dashboard</a>
            </div>
          </div>
          <div class="myprofile-edit row stream_settings">
          <div style="color:#FF0000">
            <div class="col-lg-4"></div>
            <div class="col-lg-8">
              <?php $this->load->view('common/show_message') ?>
            </div>
          </div>
          <div class="col-sm-12"><h2>Stream Setting</h2></div>
            <div class="col-sm-12">
              <div class="title">Set Default Twitch Login ID</div>
              <div class="stream_settings_stream_first_div">
                <form action="<?php echo base_url(); ?>Streamsetting/update_default_stream" method="POST">
                  <div class="form-group">
                    <input type="text" name="default_stream" class="form-control input-group" placeholder="Enter Default Twitch Login ID" value="<?php echo ($get_defaultstream[0]->default_stream_channel)? $get_defaultstream[0]->default_stream_channel : '' ; ?>">
                  </div>
                  <div class="form-group">
                    <input type="submit" name="submit" class="lobby_btn" value="Submit">
                  </div>
                </form>
              </div>
            </div>
            <div class="col-sm-12 tip_div_stream_setting">
              <div class="title">
                <nav>
                  <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-add_tip_pic" data-toggle="tab" href="#add_tip_pic" role="tab" aria-controls="nav-add_tip_pic" aria-selected="true">Add Tip Gif or Pictures</a>
                    <a class="nav-item nav-link" id="nav-add_tip_sound" data-toggle="tab" href="#add_tip_sound" role="tab" aria-controls="nav-add_tip_sound" aria-selected="false">Add Tip Sounds</a>
                    <a class="nav-item nav-link" id="nav-add_sub_sound" data-toggle="tab" href="#add_sub_sound" role="tab" aria-controls="nav-add_sub_sound" aria-selected="false">Add Subscription Sounds</a>
                  </div>
                </nav>
              </div>
              <div class="tab-content" id="nav-tabContent">
                <div class="stream_settings_tip_first_div tab-pane fade active in" id="add_tip_pic" role="tabpanel" aria-labelledby="nav-add_tip_pic">
                  <form class="col-sm-12 padd0 add_tipicon_form" action="<?php echo base_url(); ?>Streamsetting/add_data?add_tipimg=true" method="POST" enctype="multipart/form-data">
                    <div class="">
                      <div class="file-upload col-sm-10 padd0" style="padding-right: 0px;">
                        <div class="file-select">
                          <div class="file-select-button" id="fileName">Choose File</div>
                          <div class="file-select-name" id="noFile">No file chosen...</div>
                          <input type="file" name="new_tip_img" id="chooseFile" accept="image/*" required>
                        </div>
                      </div>
                      <div class="col-sm-2 text-center padd0 pull-right" style="padding-right: 0px;">
                        <input type="submit" name="upload" value="Add" class="lobby_btn">
                      </div>
                    </div>
                  </form>
                  <form action="<?php echo base_url(); ?>Streamsetting/update_settings?update_data=tip_icon" method="POST">
                    <ul>
                      <?php
                      $icon_arr = explode(',', $get_tipicon[0]->tip_icon_imgs);
                      if (count($get_tipicon[0]->tip_icon_imgs) != 0) {
                      foreach ($icon_arr as $key => $ic) {
                        $file_name = (strpos($ic,'_-image-ini-file_'))? explode('_-image-ini-file_', $ic)[1] : $ic;
                        ?>
                        <li class="tip_li tipicon_<?php echo $get_tipicon[0]->id; ?>" id="">
                          <div>
                            <div class="select_tipicon">
                              <label><input type="checkbox" checkas="tip_icon" name="seltipicon" id="tipicon_<?php echo $get_tipicon[0]->id; ?>" value="<?php echo $ic; ?>" <?php echo ($get_tipicon[0]->tip_selicon_img == $ic) ? 'checked required' : ''; ?>> Set Default </label>
                            </div>
                            <div class="tipicon_image">
                              <div class="tipimage_div_sec">
                                <div class="file_label">
                                  <div class="lbl_nm"><i class="fa fa-file-image-o"></i>&nbsp; <?php echo $file_name; ?></div>
                                  <span class="edit_file" file_type="tip_icon" file_name='<?php echo $file_name; ?>' original_name="<?php echo $ic; ?>"><i class="fa fa-pencil"></i></span>
                                </div>
                                <div>
                                  <img src="<?php echo base_url() ?>/upload/stream_setting/<?php echo $ic; ?>" alt="tipicon" class="img img-responsive">
                                </div>
                              </div>
                            </div>
                            <a class="delete_data" href="<?php echo base_url(); ?>Streamsetting/delete_data?img=tip_icon&&delete_file=<?php echo $ic; ?>" onclick="return confirm('Are you sure to Delete?');">
                              <span><i class="fa fa-trash"></i></span>
                            </a>
                          </div>
                        </li>
                      <?php }
                      ?>
                    </ul>
                    <div class="save_button text-left">
                      <input type="submit" name="upload" value="Submit" class="lobby_btn">
                    </div>
                    <?php  } else {
                        echo '<span style="color:red;">Gif not availble</span>';
                      } ?>
                  </form>
                </div>
                <div class="stream_settings_tip_third_div tab-pane fade" id="add_tip_sound" role="tabpanel" aria-labelledby="nav-add_tip_sound">
                  <form class="col-sm-12 add_tipsound_form" action="<?php echo base_url(); ?>Streamsetting/add_data?add_sound=tip_sound" method="POST" enctype="multipart/form-data">
                    <div class="">
                      <div class="file-upload add_tip_sound col-sm-10 padd0" style="padding-right: 0px;">
                        <div class="file-select">
                          <div class="file-select-button add_tip_sound" id="fileName">Choose File</div>
                          <div class="file-select-name add_tip_sound no_File" id="no_File">No file chosen...</div>
                          <input type="file" name="new_audio" class="choose_File" id="chooseAudioFile" attachtype="add_tip_sound" accept="audio/*" required>
                        </div>
                      </div>
                      <div class="col-sm-2 text-center padd0 pull-right" style="padding-right: 0px;">
                        <input type="submit" name="upload" value="Add" class="cmn_btn">
                      </div>
                    </div>
                  </form>
                  <form action="<?php echo base_url(); ?>Streamsetting/update_settings?update_data=tip_sound" method="POST" class="has-validation-callback">
                    <ul>
                      <?php
                      $tip_sound_arr = explode(',', $get_tipsound[0]->sound_audios);
                      if (count($get_tipsound[0]->sound_audios) != 0) {
                      foreach ($tip_sound_arr as $key => $tc) {
                        $file_name = (strpos($tc,'_-audio-ini-mp3_'))? explode('_-audio-ini-mp3_', $tc)[1] : $tc;
                        ?>
                        <li class="tip_li tipicon_<?php echo $get_tipsound[0]->id; ?>" id="">
                          <div>
                            <div class="select_tipicon">
                              <label><input type="checkbox" name="selsound" id="tipicon_<?php echo $get_tipsound[0]->id; ?>" checkas="tip_sound" value="<?php echo $tc; ?>" <?php echo ($get_tipsound[0]->selsound_audio == $tc) ? 'checked required' : ''; ?>> Set default</label>
                            </div>
                            <div class="tipicon_image">
                              <div class="audio_div_sec">
                                <div class="file_label">
                                  <div class="lbl_nm">
                                    <i class="fa fa-file-audio-o"></i>&nbsp; <?php echo $file_name; ?>
                                  </div>
                                  <span class="edit_file" file_type="tip_sound" file_name='<?php echo $file_name; ?>' original_name="<?php echo $tc; ?>"><i class="fa fa-pencil"></i></span>
                                </div>
                                <div>
                                  <img src="<?php echo base_url(); ?>upload/stream_setting/audio.png" alt="">
                                </div>
                                <div>
                                  <audio controls>
                                    <source src="<?php echo base_url(); ?>upload/stream_setting/<?php echo $tc; ?>">
                                  </audio>
                                </div>
                              </div>
                            </div>
                            <a class="delete_data" href="<?php echo base_url(); ?>Streamsetting/delete_data?sound=tip_sound&delete_file=<?php echo $tc; ?>"onclick="return confirm('Are you sure to Delete?');">
                              <span><i class="fa fa-trash"></i></span>
                            </a>
                          </div>
                        </li>
                      <?php } ?>
                    </ul>
                    <div class="save_button text-left">
                      <input type="submit" name="upload" value="Submit" class="lobby_btn">
                    </div>
                  <?php  } else {
                    echo '<span style="color:red;">Tip Sounds not availble</span>';
                  } ?>
                  </form>
                </div>
                <div class="stream_settings_tip_third_div tab-pane fade" id="add_sub_sound" role="tabpanel" aria-labelledby="nav-add_sub_sound">
                  <form class="col-sm-12 add_tipsound_form" action="<?php echo base_url(); ?>Streamsetting/add_data?add_sound=sub_sound" method="POST" enctype="multipart/form-data">
                    <div class="">
                      <div class="file-upload add_tip_sound col-sm-10 padd0" style="padding-right: 0px;">
                        <div class="file-select">
                          <div class="file-select-button add_tip_sound" id="fileName">Choose File</div>
                          <div class="file-select-name add_tip_sound no_File" id="no_File">No file chosen...</div>
                          <input type="file" name="new_audio" class="choose_File" id="chooseAudioFile" attachtype="add_tip_sound" accept="audio/*" required>
                        </div>
                      </div>
                      <div class="col-sm-2 text-center padd0 pull-right" style="padding-right: 0px;">
                        <input type="submit" name="upload" value="Add" class="cmn_btn">
                      </div>
                    </div>
                  </form>
                  <form action="<?php echo base_url(); ?>Streamsetting/update_settings?update_data=sub_sound" method="POST" class="has-validation-callback">
                    <ul>
                      <?php
                      $sub_sound_arr = explode(',', $get_subsound[0]->sound_audios);
                      if (count($get_subsound[0]->sound_audios) != 0) {
                      foreach ($sub_sound_arr as $key => $tc) {
                        $file_name = (strpos($tc,'_-audio-ini-mp3_'))? explode('_-audio-ini-mp3_', $tc)[1] : $tc;
                        ?>
                        <li class="tip_li tipicon_<?php echo $get_subsound[0]->id; ?>" id="">
                          <div>
                            <div class="select_tipicon">
                              <label><input type="checkbox" name="selsound" id="tipicon_<?php echo $get_subsound[0]->id; ?>" checkas="sub_sound" value="<?php echo $tc; ?>" <?php echo ($get_subsound[0]->selsound_audio == $tc) ? 'checked required' : ''; ?>> Set default</label>
                            </div>
                            <div class="tipicon_image">
                              <div class="audio_div_sec">
                                <div class="file_label">
                                  <div class="lbl_nm">
                                    <i class="fa fa-file-audio-o"></i>&nbsp; <?php echo $file_name; ?>
                                  </div>
                                  <span class="edit_file" file_type="sub_sound" file_name='<?php echo $file_name; ?>' original_name="<?php echo $tc; ?>"><i class="fa fa-pencil"></i></span>
                                </div>
                                <div><img src="<?php echo base_url(); ?>upload/stream_setting/audio.png" alt=""></div>
                                <div>
                                  <audio controls>
                                    <source src="<?php echo base_url(); ?>upload/stream_setting/<?php echo $tc; ?>">
                                  </audio>
                                </div>
                              </div>
                            </div>
                            <a class="delete_data" href="<?php echo base_url(); ?>Streamsetting/delete_data?sound=sub_sound&delete_file=<?php echo $tc; ?>"onclick="return confirm('Are you sure to Delete?');">
                              <span><i class="fa fa-trash"></i></span>
                            </a>
                          </div>
                        </li>
                      <?php } ?>
                    </ul>
                    <div class="save_button text-left">
                      <input type="submit" name="upload" value="Submit" class="lobby_btn">
                    </div>
                  <?php  } else {
                    echo '<span style="color:red;">Subscription Sounds not availble</span>';
                  } ?>
                  </form>
                </div>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="title">Change your Live Lobby link</div>
              <?php
              if (count($created_lobbys) > 0) {
                foreach ($created_lobbys as $key => $cl) { ?>
                  <div class="stream_settings_stream_first_div created_lobbys_list row">
                    <form action="<?php echo base_url(); ?>Streamsetting/update_lobby_url" method="POST" lobby_id="<?php echo $cl['lobby_id']; ?>" class="update_lobby_custom_url">
                        <div class="col-sm-3 bidimg padd0">
                          <img src="<?php echo base_url().'upload/game/'. $cl['game_image'];?>" alt="">
                        </div>
                        <div class="col-sm-9 bid-info">
                          <h5 class="custom_link_label">Custom link : <span class="pull-right">Default ID : <?php echo $cl['lobby_id']; ?></span></h5>
                          <label class="lobby_lnk">
                            <div class="row">
                              <div class="lnk_loc col-sm-8">
                                <span><?php echo base_url().'livelobby/'; ?></span>
                              </div>
                              <div class="col-sm-4">
                                <div class="pull-right">
                                  <input type="text" name="custom_name" placeholder="Custom" class="form-control" value="<?php echo $cl['custom_name']; ?>" lobby_id="<?php echo $cl['lobby_id']; ?>" required>
                                  <input type="hidden" name="lobby_id" value="<?php echo $cl['lobby_id']; ?>">
                                  <input type="hidden" name="old_custom_name" lobby_id="<?php echo $cl['lobby_id']; ?>" value="<?php echo $cl['custom_name']; ?>">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-3">
                                <?php $self="no";
                                $lobbylink = 'Live Stream';
                                if ($cl['is_event'] == 1) {
                                  $lobbylink = 'Live Event';
                                } ?>
                                <a href="<?php echo base_url().'livelobby/watchLobby/'.$cl['lobby_id'];?>" class="cmn_btn custom_btn"><?php echo $lobbylink; ?></a>
                              </div>
                              <div class="col-sm-6"></div>
                              <div class="col-sm-3">
                                <input type="submit" name="submit" class="cmn_btn custom_btn submit_custom_url" value="Submit" lobby_id="<?php echo $cl['lobby_id']; ?>">
                                <div class="lobby_exist_loader" lobby_id="<?php echo $cl['lobby_id']?>"><span></span></div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-12">
                                <label class="lobby_exist_error pull-right" style="color: #fff;" lobby_id="<?php echo $cl['lobby_id']?>"></label>
                              </div>
                            </div>
                          </label>
                        </div>
                    </form>
                  </div>
                <?php }
              } else { ?>
                  <div class="stream_settings_stream_first_div created_lobbys_list row">
                    <form action="<?php echo base_url(); ?>Streamsetting/update_lobby_url" method="POST" lobby_id="<?php echo $cl['lobby_id']; ?>" class="update_lobby_custom_url">
                        <div class="col-sm-3 bidimg padd0">
                          <img src="<?php echo base_url().'upload/profile_img/'. $get_user_detail->image;?>" alt="">
                        </div>
                        <div class="col-sm-9 bid-info">
                          <h5 class="custom_link_label">Default link :
                            <!-- <span class="pull-right">Default ID : <?php // echo $get_user_detail->id; ?></span> -->
                          </h5>
                          <label class="lobby_lnk">
                            <div class="row">
                              <div class="lnk_loc col-sm-9">
                                <span><?php echo base_url().'livelobby/'.$get_user_detail->custom_name; ?></span>
                              </div>
                              <div class="col-sm-3">
                                <input type="text" name="custom_name" placeholder="Custom" class="form-control default_user_custom_link" value="<?php echo $get_user_detail->custom_name; ?>" required>
                                <input type="hidden" name="old_custom_name" lobby_id="<?php echo $cl['lobby_id']; ?>" value="<?php echo $get_user_detail->custom_name; ?>">

                                <input type="submit" name="submit" class="cmn_btn custom_btn disabled submit_custom_url" value="Submit">
                                <div class="custom_default_link_exist_loader"><span></span></div>
                              </div> 
                            </div>
                            <div class="row">
                              <div class="col-sm-12">
                                <label class="custom_default_link_exist_error pull-right" style="color: #fff;"></label>
                              </div>
                            </div>
                          </label>
                        </div>
                    </form>
                  </div>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
