<section class="login-signup-page">    
	<div class="container">
    	<div class="row">
        	<div class="login-signBlock sucess-msg">
                <?php 
                $_SESSION['getmysub_sound'] = ($getmysub_sound) ? $getmysub_sound : '';
                $this->session->set_flashdata('message_succ', "You have successfully subscribed ".$receiver_name." for ".$plan." Plan ".$planamt." amount deducted from your paypal account");
                ?>
        		<h2>Success Page <span><i class="fa fa-check-circle" aria-hidden="true"></i></span></h2>
        		<h3>You have successfully subscribed <?php echo $getdetails[0]['email']; ?> for <?php echo $plan; ?> Plan </h3>
        		<h4><?php echo $subscribe_amount; ?> amount deducted from your paypal account</h4>
          </div>
		</div>
	</div>
</section>

<!--  This code was done, Due to Real time massage of Subscription.  Don't remove or change it   -->
<!-- code start -->
    <?php
        $CI =& get_instance();
        if ($CI->chatsocket->current_user != null && $CI->chatsocket->current_user != '') {
          $socketUser = $CI->chatsocket->current_user;
        } else {
          $socketUser = 0;
        }
    ?>
     <!-- jquery -->
    <?php $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://"; ?>
    <script src="<?php echo $protocol . addhttp($CI->chatsocket->domain_name . ':' . $CI->chatsocket->socket_port . '/socket.io/socket.io.js'); ?>"></script> 
    <script type="text/javascript">
      if(typeof io != 'undefined'){
        var socket = io.connect( "<?php
          echo addhttp($CI->chatsocket->domain_name . ':' . $CI->chatsocket->socket_port); ?>",{secure: true}); //connect to socket
      }
      var varSCPath = "<?php echo base_url('xwb_assets'); ?>";
      var socketUser = "<?php echo $socketUser; ?>";
      window.formKey = "<?php echo csFormKey(); ?>";
      if(typeof socket != 'undefined'){
        socket.emit( "socket id", { "user": socketUser } ); // pass the user identity to node
      }
    </script>
    <script type="text/javascript" src="<?php echo base_url(); ?>xwb_assets/js/chatsocket.js"></script>
    <script type="text/javascript">
        var msg_data = {
            'id':<?php echo $lobby_get_chat->id; ?>,
            'message_id':<?php echo $lobby_get_chat->message_id; ?>,
            'user_id':<?php echo $lobby_get_chat->user_id; ?>,
            'message':'<?php echo $lobby_get_chat->message; ?>',
            'message_type':'<?php echo ($lobby_get_chat->message_type)?$lobby_get_chat->message_type:""; ?>',
            'attachment':'<?php echo ($lobby_get_chat->attachment)?$lobby_get_chat->attachment:""; ?>',
        }
        var res = {
            'msg_data': msg_data,
            'name': '<?php echo $fan_tag; ?>',
            'session_id': '<?php echo $lobby_get_chat->user_id; ?>',
            'is_admin': '<?php echo ($lobby_get_chat->is_admin)?$lobby_get_chat->is_admin:""; ?>',
            'lobby_id':'<?php echo $lobby_detail[0]['custom_name']; ?>',
            'getmytip_sound': '<?php echo $_SESSION['getmysub_sound']; ?>',
            'tag': 'send_socket_msg_lobby_grp'
        };
        commonshow(res);

        window.setTimeout(function(){
            window.location.href = "<?php echo $redirect_url; ?>";
        }, 4000);
    </script>
<!-- code end -->