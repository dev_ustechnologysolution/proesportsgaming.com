<div class="adm-dashbord">
<div class="container padd0">
   <?php $this->load->view('dashboard/profile_sidebar.tpl.php');?>
   <div class="col-lg-10">
      <div class="col-lg-11 padd0">
         <div>
            <div class="col-lg-11">
               <div class="profile_top row">
                  <div class="col-sm-6 text-right">
                     <a class="profile_top_btn orange" href="<?php echo base_url('user/myprofile'); ?>">My Profile</a>
                  </div>
                  <div class="col-sm-6">
                     <a class="profile_top_btn black" href="<?php echo base_url('friend'); ?>">Dashboard</a>
                  </div>
               </div>
               <div class="myprofile-edit row">
                  <div class="col-sm-12"><h2>Money Sent</h2></div>
                  <?php if (count($get_sent_history) != 0) { ?>
                    <div class="tradelist col-sm-12">
                      <ul class="trade_history_ul">
                        <?php 
                        foreach ($get_sent_history as $key => $gsh) {
                          $name = $gsh->display_name ? $gsh->display_name : $gsh->name;
                          $myname = $my_data['display_name'] ? $my_data['display_name'] : $my_data['name'];
                          $time = date('m-d-Y', strtotime($gsh->created));
                          $amount =  number_format((float)$gsh->transfer_amt, 2, '.', ''); ?>
                            <li>
                              <div class="col-lg-2 col-sm-2 col-md-3 bidimg padd0">
                                <img src="<?php echo base_url(); ?>upload/profile_img/<?php echo $my_data['image']; ?>" alt="">
                              </div>
                              <div class="col-lg-3 col-sm-3 col-md-3">
                                <h3><?php echo $myname; ?> </h3>
                                <h3>ACC# <?php echo $my_data['account_no']; ?></h3>
                                <span class="postby"><?php echo $my_data['team_name']; ?></span>
                              </div>
                              <div class="col-lg-2 col-sm-2 col-md-3">
                                <div><?php echo $time; ?></div>
                                <div><span class="postby">Amount: $<?php echo $amount; ?></span></div>
                              </div>
                              <div class="col-lg-3 col-sm-3 col-md-3">
                                <div class="text-right">
                                  <h3><?php echo $name; ?> </h3> <h4>ACC# <?php echo $gsh->account_no; ?></h4>
                                  <span class="postby"><?php echo $gsh->team_name; ?></span>
                                </div>
                              </div>                              
                              <div class="col-lg-2 col-sm-2 col-md-2 bidimg padd0">
                                 <div class="pull-right">
                                  <img src="<?php echo base_url(); ?>upload/profile_img/<?php echo $gsh->image; ?>" alt="">
                                 </div>
                              </div>
                           </li>
                        <?php } ?>
                      </ul>
                    </div>
                    <div class="col-lg-12">
                      <br>
                    </div>
                  <?php } ?>
                  <div class="col-sm-12"><h2>Money Received</h2></div>
                  <?php if (count($get_receive_history) != 0) { ?>
                    <div class="tradelist col-sm-12">
                      <ul class="trade_history_ul">
                        <?php 
                        foreach ($get_receive_history as $key => $grh) {
                          $name = $grh->name;
                          if ($grh->display_name != '') {
                            $name = $grh->display_name;
                          }
                          $time = date('m-d-Y', strtotime($grh->created));
                          $amount =  number_format((float)$grh->transfer_amt, 2, '.', ''); ?>
                          <li>
                              <div class="col-lg-2 col-sm-6 col-md-3 bidimg padd0">
                                <img src="<?php echo base_url(); ?>upload/profile_img/<?php echo $grh->image; ?>" alt="">
                              </div>
                              <div class="col-lg-3 col-sm-6 col-md-3">
                                <h3><?php echo $name; ?></h3>
                                <span class="postby"><?php echo $grh->team_name; ?></span>
                              </div>
                              <div class="col-lg-3 col-sm-6 col-md-3">
                                <h3>ACC# <?php echo $grh->account_no; ?></h3>
                              </div>
                              <div class="col-lg-4 col-sm-4 col-md-2">
                                 <div class="text-right">
                                    <div><?php echo $time; ?></div>
                                    <div><span class="postby">Amount: $<?php echo $amount; ?></span></div>
                                 </div>
                              </div>
                           </li>
                        <?php } ?>
                      </ul>
                    </div>
                  <?php } ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
