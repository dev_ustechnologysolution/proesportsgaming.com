<section class="body-middle innerPage">
	<div class="container">
    <div class="row">
    
     <!-- ajax message-->
            <div id="ajax_msg"></div>
<div class="tab-pane" id="Change_Password">
                    <form class="form-horizontal">
                      <div class="form-group">
                        <label for="current_password" class="col-sm-2 control-label">Current Password</label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" id="current_password" placeholder="current password" name="current_password">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="new_password" class="col-sm-2 control-label" >New Password </label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" id="new_password" placeholder="new password" name="new_password">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="confirm_password" class="col-sm-2 control-label">Confirm New Password </label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" id="confirm_password" placeholder="confirm new password" name="confirm_password">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                        <input type='hidden' id="base_url" value=<?php echo base_url();?>>
                        <input type="button" id="chang_pass" value="Submit" class="btn btn-danger">
                        </div>
                      </div>
                    </form>
                  </div>
                  
                   </div>
    </div>
</section>