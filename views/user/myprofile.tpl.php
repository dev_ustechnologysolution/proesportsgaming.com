<div class="adm-dashbord">

    <div class="container padd0">
    <?php $value = $friend_profile; ?>
        <?php $this->load->view('dashboard/profile_sidebar.tpl.php'); ?>

        <div class="col-lg-10">

            <div class="col-lg-12 padd0">

                <div> 
                <div class="profile_top row">
                            <div class="col-sm-6 text-right">
                                <a class="profile_top_btn orange" href="<?php echo base_url('user/myprofile'); ?>">My Profile</a>
                            </div>
                            <div class="col-sm-6">
                                <a class="profile_top_btn black" href="<?php echo base_url('friend'); ?>">Dashboard</a>
                            </div>
                        </div>
                    <div class="col-lg-11">

                        <div class="myprofile-edit">

                          <!-- <h2>My Profile</h2> -->

                          <?php $this->load->view('common/show_message'); ?>

                          <div class="col-lg-12">
                           
                            <div class="row">

                                <div class="form-group profile_image_div">
                                  <div class="row">
                                    <div class="col-lg-12">

                                    <div class="friend_profile_img" style="text-align:center;">

                                        <img src="<?php if(isset($value->image)) echo base_url().'upload/profile_img/'.$value->image;?>"  id="k"/>

                                    </div>
                                    
                                    </div>
                                    <div class="col-lg-12">
                                    <p class="mt12"><?php echo (isset($value->display_name)) ? $value->display_name : $value->name ?></p>
                                    </div>
                                    <div class="col-lg-12 friend_plyr_id">
                                      <label class="white no-padding">Account NO #<?php echo $value->account_no; ?></label>
                                    </div>
                                    <div class="col-lg-12">
                                      <p>Esports Team</p>
                                    </div>
                                    <div class="col-lg-12 friend_plyr_id">
                                      <label class="white no-padding"><?php echo $value->team_name; ?></label>
                                    </div>
                                    </div>
                                </div>
                             </div>
                             <div class="col-lg-12"  align="center" style="display:none;">
                                    <div class="input-group back_button">
                                      <a href="javascript:history.go(-1)">Back</a>
                                    </div>
                                </div>
                                    
                          </div>

                        </div>

                    </div>

                </div>

            </div>   

        </div>

    </div>

</div>



