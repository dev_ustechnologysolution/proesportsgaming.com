<section class="login-signup-page">
	<div class="container">
    	<div class="row">
        	<div class="login-signBlock">
            	<h2>PROFILE UPDATE</h2>
				<label style="margin-left: 204px;"><?php if(isset($msg)) echo $msg;?></label>
                <form role="form" action="<?php echo base_url().'user/profile_edit';?>" class="signup-form" method="POST" enctype="multipart/form-data">
                	<div class="form-group">
                    	<label class="col-lg-4 col-sm-4 col-md-4">Full Name</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                        	<input type="text" class="form-control" name="name" required="required" value="<?php if(isset($user_detail[0]['name'])) echo $user_detail[0]['name']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                    	<label class="col-lg-4 col-sm-4 col-md-4">Email Address</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                        	<input type="email" class="form-control" name="email" readonly="readonly" value="<?php if(isset($user_detail[0]['email'])) echo $user_detail[0]['email']; ?>">
                        </div>
                    </div>
                    <!-- <div class="form-group">
                    	<label class="col-lg-4 col-sm-4 col-md-4">Password</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                        	<input type="password" class="form-control" name="password" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                    	<label class="col-lg-4 col-sm-4 col-md-4">Re-type Password</label>
                        <div class="input-group col-lg-8 col-sm-8 col-md-8">
                        	<input type="text" class="form-control" name="password1" required="required">
                        </div>
                    </div> -->
                    <div class="form-group">
                      <label class="col-lg-4 col-sm-4 col-md-4">Image</label>
                        <div class="col-lg-8 col-sm-8 col-md-8 form-upload">
                        <div class="fileUpload">
                            <span>Upload</span>
                            <label style="display:none"></label>
                            <input id="uploadBtn" type="file" name="image" class="upload" />
                        </div>
                        <input id="uploadFile" class="form-control" placeholder="Choose File" disabled="disabled" />
                        
                        </div>
                    </div>
                    <div class="form-group">
                    <div class="game-img col-lg-12 col-sm-12 col-md-12">
                      <img src="<?php echo base_url().'upload/profile_img/'.$user_detail[0]['image']?>" height="100" width="100"/>
                    </div>
                    </div>

                    <div class="btn-group">
                        <input type="hidden" name="old_image" value="<?php if(isset($user_detail[0]['image'])) echo $user_detail[0]['image']; ?>">
                    	<input type="submit" value="Update" class="btn-submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>