<div class="adm-dashbord">
    <div class="container">
        <?php $this->load->view('dashboard/profile_sidebar_2.tpl.php'); ?>
        <div class="col-lg-10">
            <div class="col-lg-11 padd0">
                <div>
                <div class="profile_top row">
                    <div class="col-sm-6 text-right">
                        <a class="profile_top_btn black" href="<?php echo base_url('user/myprofile'); ?>">My Profile</a>
                    </div>
                    <div class="col-sm-6">
                        <a class="profile_top_btn orange" href="<?php echo base_url('friend'); ?>">Dashboard</a>
                    </div>
                </div>
                    <div class="col-lg-11">
                        <div class="myprofile-edit">
                          <h2>Edit Profile</h2>
                          <?php $this->load->view('common/show_message'); ?>
                          <div class="col-lg-12">
                          	<div class="row">
                             <form role="form" action="<?php echo base_url().'user/profile_edit';?>" class="signup-form prof_ed_form" method="POST" enctype="multipart/form-data">
                                <input type="hidden" id="is_admin" value="<?php echo $user_detail[0]['is_admin']?>">
                                <div class="form-group">
                                    <label class="col-lg-4">Name</label>
                                    <div class="input-group col-lg-8">                                 

                                        <input readonly type="text" class="form-control" name="name" value="<?php if(isset($user_detail[0]['name'])) echo $user_detail[0]['name']; ?>">

                                    </div>

                                </div>

                                <div class="form-group">

                                    <div class="col-lg-4 web_display_name"> <label>Display Name </label>
                                        <label class="switch">
                                          <input  type="checkbox" <?php if(isset($user_detail[0]['display_name_status']) && $user_detail[0]['display_name_status'] == 1) echo 'checked'; ?> name="display_name_status">
                                          <span class="slider round"></span>
                                        </label>
                                        </div>

                                    <div class="input-group col-lg-8 web_display_name input_display">

                                        <input onkeyup="check_valid_name(this.value,'display_name');" type="text" class="form-control" name="display_name" value="<?php if(isset($user_detail[0]['display_name'])) echo $user_detail[0]['display_name']; ?>" <?php if(isset($user_detail[0]['display_name_status']) && $user_detail[0]['display_name_status'] == 0) echo 'readonly'; ?>>

                                    </div>

                                </div>
                                <div class="form-group">

                                    <label class="col-lg-4">Esports Team Name</label>

                                    <div class="input-group col-lg-8 input_team">

                                        <input type="text" onkeyup="check_valid_name(this.value,'team_name');" class="form-control" name="team_name" value="<?php if(isset($user_detail[0]['team_name'])) echo $user_detail[0]['team_name']; ?>">

                                    </div>

                                </div>


                                <div class="form-group">

                                    <label class="col-lg-4">Email</label>

                                    <div class="input-group col-lg-8">

                                        <input required type="text" class="form-control" name="email" value="<?php if(isset($user_detail[0]['email'])) echo $user_detail[0]['email']; ?>" required>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-lg-4">Mail Notifications</label>
                                    <div class="input-group col-lg-8 notifications_div">
                                       <div class="col-lg-4">
                                        <div class="title">Friends Mail</div>
                                        <div><label class="switch">
                                              <input type="checkbox" <?php if(isset($mail_categories_fun[0]['friends_mail']) && $mail_categories_fun[0]['friends_mail'] == 'yes') echo 'checked'; ?> name="friends_mail" value="friends_mail">
                                              <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="title">Esports Team Mail</div>
                                        <div><label class="switch">
                                              <input type="checkbox" <?php if(isset($mail_categories_fun[0]['team_mail']) && $mail_categories_fun[0]['team_mail'] == 'yes') echo 'checked'; ?> name="team_mail" value="team_mail">
                                              <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="title">Challenge Mail</div>
                                        <div><label class="switch">
                                              <input type="checkbox" <?php if(isset($mail_categories_fun[0]['challenge_mail']) && $mail_categories_fun[0]['challenge_mail'] == 'yes') echo 'checked'; ?> name="challenge_mail" value="challenge_mail">
                                              <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                  </div>
                                </div>                                
                                <div class="form-group">

                                    <label class="col-lg-4">New Password</label>

                                    <div class="input-group col-lg-8">

                                        <input type="password" class="form-control" name="password">

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-lg-4">Phone</label>

                                    <div class="input-group col-lg-8">

                                        <input  type="text" class="form-control" name="number" value="<?php if(isset($user_detail[0]['number'])) echo $user_detail[0]['number']; ?>" required>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-lg-4">Location</label>

                                    <div class="input-group col-lg-8">

                                        <input type="text" class="form-control" name="location" value="<?php if(isset($user_detail[0]['location'])) echo $user_detail[0]['location']; ?>" required>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-lg-4">Country</label>

                                    <div class="input-group col-lg-8">
                                         <select id="countries_states1" class="form-control" name="country" required>
                                            <?php 
                                                    echo '<option value="">Select Country</option>';
                                                    foreach ($country_list as $key => $value) {
                                                        echo '<option value="'.$value['country_id'].'"';
                                                        if ($value['country_id'] == $user_detail[0]['country']) { echo ' selected="selected"'; }
                                                        echo '>'.$value['countryname'].'</option>';   
                                                    }
                                            ?>
                                         </select>

                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-lg-4">State</label>
                                    <div class="input-group col-lg-8">
                                         <select class="form-control" name="state" id="state" required>
                                             <?php if (isset($user_detail[0]['state']) && $user_detail[0]['state'] != '') {
                                                echo '<option value="'.$user_detail[0]['stateid'].'" selected="selected" >'.$user_detail[0]['statename'].'</option>';
                                             } ?>
                                         </select>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-lg-4">Zip Code</label>

                                    <div class="input-group col-lg-8">

                                        <input type="text" class="form-control" name="zip_code" id="zip_code" value="<?php if(isset($user_detail[0]['zip_code'])) { echo $user_detail[0]['zip_code']; } ?>" required>

                                    </div>

                                </div>
							
								<div class="form-group">

                                    <label class="col-lg-4">TAX ID</label>

                                    <div class="input-group col-lg-8">

                                        <input type="password" class="form-control" name="taxid" id="taxid" value="<?php if(isset($user_detail[0]['taxid'])) { echo $user_detail[0]['taxid']; } ?>" >

                                    </div>

                                </div>
							
							
                                <div class="form-group">

                                  <label class="col-lg-4 col-sm-4 col-md-4">Profile Picture</label>

                                    <div class="col-lg-8 col-sm-8 col-md-8 form-upload">

                                    <div class="profileImg">
                                        <img src="<?php if(isset($user_detail[0]['image'])) echo base_url().'upload/profile_img/'.$user_detail[0]['image']?>" height="50" width="50" id="k"/>                   

                                    </div>

                                    <div class="fileUpload">
                                        <span>Change Picture</span>
                                        <label style="display:none"></label>
                                        <input type="file" name="image" class="upload" onchange="previewFile()" data-validation=" mime size" data-validation-allowing="jpg, png, gif" data-validation-max-size="1M" data-validation-error-msg-size="You can not upload images larger than 1MB"/>

                                    </div>
									<div>
										<h4>Max Size 1MB </h4>
									</div>

                                   <div style="color:red;margin-left:192px;"><?php echo $this->session->flashdata('msg'); ?></div>

                                    </div>
                                </div>

                                <div class="btn-group">
                                    <div class="col-lg-8 pull-right padd0">
                                    <input type="hidden" name="old_image" value="<?php if(isset($user_detail[0]['image'])) echo $user_detail[0]['image']; ?>" >

                                    <input type="submit" value="Update" class="btn-update">

                                    </div>

                                </div>

                            </form>

                             </div>

                           </div>

                        </div>

                    </div>

                </div>

            </div>   

        </div>

    </div>

</div>
<script>
function check_valid_name(name,type)
{
    var is_admin = $('#is_admin').val();
    if(is_admin == 0)
    {
        var res_name = name.toLowerCase();
        var final_name = res_name.replace(/\s+/g, '-');
        if(final_name == 'pro-esports-gaming' || final_name == 'pro-esports' || final_name == 'proesports' || final_name == 'proesportsgaming' || final_name == 'esportsteam' || final_name == 'esports-team'){
            if(type == 'display_name' || type == 'first_name'){
                $('.input_display').find('input:text').val('');  
                $('.input_display').append('<label class="display_label">You dont use '+type.replace('_', ' ')+' as Pro Esports Gaming word.</label>');
            } else if(type == 'team_name' || type == 'last_name') {
                $('.input_team').find('input:text').val(''); 
                $('.input_team').append('<label class="team_label">You dont use '+type.replace('_', ' ')+' as Pro esports gaming word.</label>');
            }        
        } else {
            $('.display_label').remove();
            $('.team_label').remove();
        }
    }
    
}
</script>


