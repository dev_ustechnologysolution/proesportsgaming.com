<section class="content">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Games List</h3>
      <a href="<?php echo base_url().'admin/game'; ?>"><button class="btn pull-right btn-primary btn-xl">Add</button></a>
    </div><!-- /.box-header -->
    <?php $this->load->view('admin/common/show_message') ?>
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th style="display:none"></th>
            <th>Game Name</th>
            <th>Video</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($list as $value){?>
          <tr>
            <td style="display:none"></td>
            <td><?php echo $value['name']?></td>
            <td><?php echo $value['video_url']?> <?php $w=explode('&',substr($value['video_url'],stripos($value['video_url'],"?")+3)); @$watch=$w[0].'?'.$w[2]?>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $watch; ?>" frameborder="0" allowfullscreen></iframe>
            </td>
            <td><a href="<?php echo base_url().'Video/editVideo/'.$value['id'];?>">Edit</a></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div><!-- /.box-body -->
  </div>
</section>