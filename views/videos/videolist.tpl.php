<section class="body-middle innerpage">
    <div class="container">
        <div class="row header1">
            <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
            <div class="col-lg-4 col-sm-4 col-md-4"><h2>LATEST ACTION</h2></div>
            <div class="col-lg-4 col-sm-4 col-md-4 onlyborder"></div>
        </div>        
        <div class="row videos_list">
        <?php foreach ($video_dt as $vd) {
         if ($vd->is_twitch == '0') {
            $w=explode('&',substr($vd->video_url,stripos($vd->video_url,"?")+3)); @$watch=$w[0].'?'.$w[2];
            if($w[0] != 'U') { ?>
            <div class="col-lg-3 col-sm-6 col-md-3 videoimgs">
               <a href="<?php echo $vd->video_url; ?>" target="_blank">
                  <img src="https://img.youtube.com/vi/<?php echo $w[0]; ?>/mqdefault.jpg" alt="" class="img img-responsive">
                  <div class="float-caption">
                    <h5 class="animated-box"><?php echo $vd->tag; ?></h5>
                 </div>
               </a>
            </div>
            <?php }
         } else { ?>
            <div class="col-lg-3 col-sm-6 col-md-3 videoimgs">
               <a href="<?php echo base_url(); ?>livelobby/watchLobby/<?php echo $vd->lobby_id; ?>" target="_blank">
                  <iframe src="https://player.twitch.tv/?channel=<?php echo $vd->twitch_username; ?>" height="auto" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>
                 <div class="float-caption">
                    <h5 class="animated-box"><?php echo $vd->tag; ?></h5>
                 </div>
               </a>
            </div>
         <?php }
      } ?>            
        </div>        
    </div>
</section>
