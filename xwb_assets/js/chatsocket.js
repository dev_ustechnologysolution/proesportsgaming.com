/*
* Library Name: CI Chatsocket
* Description: “CI Chatsocket” a CodeIgniter Chat application that uses Node JS and socket.io
* to communicate real-time to each user.
* This library can be used in any existing CodeIgniter website or web application.
* Author: Jay-r Simpron
* 
*/


/* global variable declaration */
var csDropzone = null;
var varSCPath;
var is_recieved = 0;
var socketUser;
var socket;
var bootbox;
var Dropzone;
var jQuery;
(function ($) {
'use strict';
/**
 * This jQuery script support for IE 9 and above
 */
/* =============================================================================== */


/* =============================================================================== */
/* Start jQuery functions */

$(document).ready(function(){

	var audioHTML;
	var csrf_name, csrf_key;

	// if($('.video_box').length){
	// 	$('#vid1').get(0).pause();
	// 	$('#vid2').get(0).pause();
	// }
	var requested_user_to = GetParameteruser_to_from_customerValues('user_to_from_customer');
	function GetParameteruser_to_from_customerValues(param) {
	var chatbox_url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&'); 
	for (var i = 0; i < chatbox_url.length; i++) {  
	    var urlparam = chatbox_url[i].split('=');  
	    if (urlparam[0] == param) {  
	    	if (urlparam[1].length>0) {
				function getUrlParameter(name) {
				    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
				    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
				    var results = regex.exec(location.search);
				    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
				};		    		
	    	var user_to, ci, args;
				user_to = urlparam[1];
				ci = getUrlParameter('cn_id');
				$('.ci',$("#message-container")).val(ci);
				args = {'ci':ci, 'user_to':user_to,'start_from':'',elContainer: $("#message-container")};
				get_admin_messages(args);
				if( $('.video_box video').is(':visible') ){
					$("#message-container").addClass('contentnot');
				} else {
					$("#message-container").removeClass('contentnot');
				}	
	    	}
	    }  
	  }
	}
	

	var requested_user_to = GetfriendParameterValues('friend_id');
	function GetfriendParameterValues(param) {
	var chatbox_url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&'); 
	for (var i = 0; i < chatbox_url.length; i++) {  
	    var urlparam = chatbox_url[i].split('=');  
	    if (urlparam[0] == param) {  
	    	if (urlparam[1].length>0) {
				function getUrlParameter(name) {
				    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
				    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
				    var results = regex.exec(location.search);
				    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
				};		    		
	    	var user_to, ci, args;
				user_to = urlparam[1];
				ci = getUrlParameter('cn_id');
				$('.ci',$("#message-container")).val(ci);
				args = {'ci':ci, 'user_to':user_to,'start_from':'',elContainer: $("#message-container")};
				get_friends_messages(args);
				if( $('.video_box video').is(':visible') ){
					$("#message-container").addClass('contentnot');
				} else {
					$("#message-container").removeClass('contentnot');
				}	
	    	}
	    }  
	  }
	}
	


	var requested_user_to = GetUserToFromAdminParameterValues('user_to_from_admin');
	function GetUserToFromAdminParameterValues(param) {
	var chatbox_url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&'); 
	for (var i = 0; i < chatbox_url.length; i++) {  
	    var urlparam = chatbox_url[i].split('=');  
	    if (urlparam[0] == param) {  
	    	if (urlparam[1].length>0) {
				function getUrlParameter(name) {
				    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
				    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
				    var results = regex.exec(location.search);
				    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
				};		    		
	    		$('.main-header').hide();
	    		$('.main-sidebar').hide();
	    		$('.main-footer').hide();
	    		$('.content-wrapper').css("margin-left","0px");
	    		var user_to, ci, args;
					user_to = urlparam[1];
					ci = getUrlParameter('cn_id');
					$('.ci',$("#message-container")).val(ci);
					args = {'ci':ci, 'user_to':user_to,'start_from':'',elContainer: $("#message-container")};
					get_messages_from_adminside(args);
	    	}
	    }  
	  }
	}

	var requested_user_to = GetGroupUserToFromAdminParameterValues('user_to_from_admin_group');
	function GetGroupUserToFromAdminParameterValues(param) {
	var chatbox_url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&'); 
	for (var i = 0; i < chatbox_url.length; i++) {  
	    var urlparam = chatbox_url[i].split('=');  
	    if (urlparam[0] == param) {  
	    	if (urlparam[1].length>0) {
	    		$('.main-header').hide();
	    		$('.main-sidebar').hide();
	    		$('.main-footer').hide();
	    		$('.content-wrapper').css("margin-left","0px");
	    		function getUrlParameter(name) {
				    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
				    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
				    var results = regex.exec(location.search);
				    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
				};
	    		var user_to, ci, args, groupdata, agentonline, accepterid, challengerid;
				accepterid = getUrlParameter('accepterid');
				challengerid = getUrlParameter('challengerid');
				agentonline = getUrlParameter('agentonline');
				user_to = getUrlParameter('user_to_from_admin_group');
				ci = getUrlParameter('ci');
				$('.ci',$("#message-container_pvp")).val(ci);
				groupdata = $("#xwb-conversation-users").val();
	            args = {'ci':ci,'user_to':user_to,'accepterid':accepterid,'challengerid':challengerid,'agentonline':agentonline,'groupdata':groupdata,'start_from':'',elContainer: $("#message-container_pvp")};
	            get_agent_in_group(args);
	    	}
	    }  
	  }
	}

	var requested_pvp_userto = GetPvpParameterValues('pvp_userto');
	function GetPvpParameterValues(param) {
	var chatbox_url = window.location.href.slice(window.location.href.indexOf('?') + 1).split(','); 
	for (var i = 0; i < chatbox_url.length; i++) {  
	    var urlparam = chatbox_url[i].split('=');  
	    if (urlparam[0] == param) {  
	    	if (urlparam[1].length>0) {
	    		function getUrlParameter(name) {
				    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
				    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
				    var results = regex.exec(location.search);
				    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
				};
	    		var user_to, ci, args, agentonline, challengerid, accepterid, userto;
				user_to = getUrlParameter('user'); 
				userto = getUrlParameter('pvp_userto'); 
				agentonline = getUrlParameter('agentonline'); 
				challengerid = getUrlParameter('challengerid'); 
				accepterid = getUrlParameter('accepterid'); 
				ci = getUrlParameter('ci'); 
				$('.ci',$("#message-container_pvp")).val(ci);
				var groupdata = [challengerid,accepterid];
				args = {'ci':ci,'user_to':user_to,'userto':userto,'accepterid':accepterid,'challengerid':challengerid,'agentonline':agentonline,'groupdata':groupdata,'start_from':'',elContainer: $("#message-container_pvp")};
				get_pvp_chat_messages_frontside(args);
				if( $('.pvp_chatbox_main_row .video_box video').is(':visible') ){
					$("#message-container_pvp").addClass('contentnot');
				} else {
					$("#message-container_pvp").removeClass('contentnot');
				}	
	    	}
	    }  
	  }
	}

	if($('.pvp_message_box-frontside').length){
		$(".popup-content-wrapper").hide();
		$('#vid1').get(0).pause();
		$('#vid2').get(0).pause();
	}

	if($('#customer_services_msgbox').length){
		$(".popup-content-wrapper_customer_fontside").hide();
		$('#vid1').get(0).pause();
		$('#vid2').get(0).pause();
	}
	if($('#live_lobby_msgbox').length){
		// $(".popup-content-wrapper_customer_fontside").hide();
		// $('#vid1').get(0).pause();
		// $('#vid2').get(0).pause();
	}
	$('.membership_plan').click(function() {
		var membership_plan_id = $(this).attr('membership_plan_id');
		var postData = { 'id': membership_plan_id };
		$.ajax({
            url: base_url+'membership/getMembershipPlanById',
            type: "post",
			data: postData,
            success: function(data) {
            	var data = $.parseJSON(data);
            	membership_btn_append(data);
            },	
        })
		
	});

	$(document).on("click",".open_pop_up_for_pvp_chat_admin_side",function(){
		var user_to, ci, args, groupdata, gamename, agentonline, accepterid, challengerid;
		gamename = $(this).data('gamename');
		accepterid = $(this).data('accepterid');
		challengerid = $(this).data('challengerid');
		agentonline = $(this).data('agentonline');
		user_to = $(this).data('user');
		ci = $(this).data('ci'); //conversation id
		$('.ci',$("#message-container_pvp")).val(ci);
		groupdata = $("#xwb-conversation-users").val();
          args = {'ci':ci,'user_to':user_to,'accepterid':accepterid,'challengerid':challengerid,'agentonline':agentonline,'groupdata':groupdata,'start_from':'','gamename':gamename,elContainer: $("#message-container_pvp")};
          get_agent_in_group(args);
          // $('#pvp_msgbox_for_admin').show();
      });

	$(document).on("click",".open_pop",function(){
		var user_to, ci, args;
		user_to = $(this).data('user');
		ci = $(this).data('ci'); //conversation id
		$('.ci',$("#message-container")).val(ci);
		args = {'ci':ci,'user_to':user_to,'start_from':'',elContainer: $("#message-container")};
		get_messages(args);
	});
	// $('#customer_services_msgbox').hide();
	$(document).on("click",".open_pop_adminside",function(){
		$('#customer_services_msgbox').show();
		var user_to, ci, args;
		user_to = $(this).data('user');
		ci = $(this).data('ci'); //conversation id
		$('.ci',$("#message-container")).val(ci);
		args = {'ci':ci, 'user_to':user_to,'start_from':'',elContainer: $("#message-container")};
		get_messages_from_adminside(args);
	});		

	$(document).on("click",".open_pop_for_admin",function(){
		$('#customer_services_msgbox').show();
		var user_to, ci, args;
		user_to = $(this).data('user');
		ci = $(this).data('ci'); //conversation id
		$('.ci',$("#message-container")).val(ci);
		args = {'ci':ci, 'user_to':user_to,'start_from':'',elContainer: $("#message-container")};
		get_admin_messages(args);
		if( $('.video_box video').is(':visible') ){
			$("#message-container").addClass('contentnot');
		} else {
			$("#message-container").removeClass('contentnot');
		}
	});

	
	$('.close_model').click(function(e){
		// $("#pvp_msgbox_for_admin").fadeOut('normal');
		$('.modal.fade').find('video').remove();
		location.reload();
	});
	$('.subscriblist_main_div .subscribed_img').click(function () {
	    var image = $(this).find('img').attr('src');
	    var image_name = $(this).find('img').attr('alt');
	    var senddata = '<div class="col-sm-12 imageshow_area"> <img src="'+image+'" alt="'+image_name+'" class="img img-responsive"> </div>';
	    var data = {
			'add_class_in_modal_data': 'image_modal_data',
			'add_class_in_modal' : 'image_modal',
			'senddata':senddata,
			'tag':'view_subs_image'
		}
		commonshow(data);
	});
	$( "#message-inner" ).scroll(function() {
		var $message_row, message_inner_position, message_row, ci, user_to, args;
		$message_row = $("div.message-row",this); // message row container
		message_inner_position = $(this).offset().top + 2; // plus 2 for the margin of message_row

		if($message_row.first().offset() != undefined){
			message_row = $message_row.first().offset().top; //get the first message row top position
			ci = $(this).parent('.xwb-message-container').attr('data-cnid');

			if(message_inner_position == message_row){ // trigger load previous messages when top position reached
				console.log('loading next message...');
				// $(this).prepend('<div class="row loading_container"><h3 class="text-center">Loading more messages...</h3></div>');
				
				user_to = $(".user_to",'#message-container_pvp').val();

				args = {'ci':ci,'user_to':user_to, start_from: ($message_row.length), elContainer: $("#message-container_pvp")};
				
				get_pvp_chat_messages_frontside(args, true);
			}
		}
	});

		$( "#message-inner" ).scroll(function() {
		var $message_row, message_inner_position, message_row, ci, user_to, args;
		$message_row = $("div.message-row",this); // message row container
	
		message_inner_position = $(this).offset().top + 2; // plus 2 for the margin of message_row

		if($message_row.first().offset() != undefined){
			message_row = $message_row.first().offset().top; //get the first message row top position
			ci = $(this).parent('.xwb-message-container').attr('data-cnid');

			if(message_inner_position == message_row){ // trigger load previous messages when top position reached
				console.log('loading next message...');
				// $(this).prepend('<div class="row loading_container"><h3 class="text-center">Loading more messages...</h3></div>');
				
				user_to = $(".user_to",'#message-container').val();

				args = {'ci':ci,'user_to':user_to, start_from: ($message_row.length), elContainer: $("#message-container")};
				
				get_admin_messages(args, true);
			}
		}
	});


		$( "#message-inner-div-admin-side" ).scroll(function() {
		var $message_row, message_inner_position, message_row, ci, user_to, args;
		$message_row = $("div.message-row",this); // message row container
	
		message_inner_position = $(this).offset().top + 2; // plus 2 for the margin of message_row

		if($message_row.first().offset() != undefined){
			message_row = $message_row.first().offset().top; //get the first message row top position
			ci = $(this).parent('.xwb-message-container').attr('data-cnid');

			if(message_inner_position == message_row){ // trigger load previous messages when top position reached
				console.log('loading next message...');
				// $(this).prepend('<div class="row loading_container"><h3 class="text-center">Loading more messages...</h3></div>');
				
				user_to = $(".user_to",'#message-container').val();

				args = {'ci':ci,'user_to':user_to, start_from: ($message_row.length), elContainer: $("#message-container")};
				
				get_messages_from_adminside(args, true);
			}
		}
	});

		$(".pvp_message_box-frontside .pvp_msgbox-message-container").bind("mouseover", function()
      {
      	var ci = $('.conversation-name a').data('groupid');
      	var args = {'ci':ci,elContainer: $("#message-container_pvp")};
          get_user_to_for_group(args);
      });
      $(".pvp_message_box-frontside .pvp_msgbox-message-container").bind("mouseout", function()
      {
          var ci = $('.conversation-name a').data('groupid');
      	var args = {'ci':ci,elContainer: $("#message-container_pvp")};
          get_user_to_for_group(args);
      });

	
      function get_user_to_for_group(args)
      {
		var ci = args.ci;
		var postData = {
				'ci': ci,
				'cs_key' : 'xwb_get_user_to_for_group'
			};
		
		$.ajax({
			url: window.location.pathname,
			type: "post",
			data: postData,
			success: function(data){
				data = $.parseJSON(data);
				var users_to = data.updated_user;
				$(args.elContainer).attr('data-users', data.updated_user.join('|'));
				$('.user_to',args.elContainer).remove();
				$.each(users_to,function(i,v){
				$('.msg-input',args.elContainer).append('<input type="hidden" name="user_to[]" class="user_to" id="user_to" value="'+v+'" />');
				$('.chatbox',args.elContainer).append('<input type="hidden" name="user_to[]" class="user_to" id="user_to" value="'+v+'" />');
				});

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {}
		});
	}

	$('#bet_report_modal .yes').click(function(){
		$('#bet_report_modal_status').show();
	});
	$('.won_lost_status .btn-submit').click(function(){
		if ($('.won_lost_status.active .required_element').val() !='') {
		var editorID, data, users, socket_data, accept_risk, explain_admin, win_lose_status, video;
		var group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
		var lobby_id = $('input[name="lobby_id"]').val();
		var user_id = $('input[name="user_id"]').val();
		video = $('form.active input[name="url"]').val();
		accept_risk = $('form.active input.accept_risk').val();
		win_lose_status = $('form.active input[name="win_lose_status"]').val();
		explain_admin = $('form.active textarea[name="explain_admin"]').val();
		var postData = {
				'lobby_id': lobby_id,
      	'group_bet_id':group_bet_id,
      	'user_id':user_id,
      	'video':video,
				'accept_risk':accept_risk,
				'win_lose_status':win_lose_status,
				'explain_admin':explain_admin	
			};
			$.ajax({
        url: base_url+'livelobby/report_lobbybet',
        type: "post",
        data: postData,
        success: function(data) {
        	var data = $.parseJSON(data);
        	$('#bet_report_modal').hide();
					$('#bet_report_modal_status').hide();
					var lobby_custom_name = $('input[name="lobby_custom_name"]').val();
					socket_data = {
						'group_bet_id': data.get_updated_detail['group_bet_id'],
						'lobby_id': lobby_custom_name,
						'table_cat': data.get_updated_detail['table_cat'],
						'win_lose_status': data.get_updated_detail['win_lose_status'],
						'user_id' : data.get_updated_detail['user_id'],
						'ref_id' : data.ref_id
					};
					socket.emit( 'send_report_group_data', socket_data );
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {}
      })
		} else {
			$('.won_lost_status.active .required_element').css({'border': '1px solid #ee4f05','box-shadow': 'inset 0px 0px 5px 1px #ee4f05'});
			$('.won_lost_status.active .required_element').focus();
			return false;
		}
	});
			
		
		// play game status
		$('.play_game').change(function(){
			if($('.play_game').prop('checked')){
				$('#lobby_accept').show();
			}
  		});
		$('#lobby_accept .no, #lobby_accept .close').click(function(){
		    $('#lobby_accept').hide();
		    $('.play_game').removeAttr('checked');
		});
		$('#lobby_accept .yes').click(function(){
		    var total = $('input[name="lobby_accept_total_balance"]').val();
		    var price = $('input[name="lobby_accept_price"]').val();
		    var tax_price = $('input[name="lobby_accept_price"]').val();
		    var socket_data;
		    // if (parseInt(total) > parseInt(tax_price)) {
	        var group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
	        var lobby_id = $('input[name="lobby_id"]').val();
	        $.ajax({
	        	url : base_url +'livelobby/challenge_payment',
	            data: {lobby_id:lobby_id,group_bet_id:group_bet_id,tax_price:tax_price},
	            type: 'POST',
	            success: function(srcData) {
	            	srcData = $.parseJSON(srcData);
	            	if (srcData.balance == 'less') {
	            		window.location.href = base_url+'points/buypoints';
		            	return false;
	            	}
	            	$('#lobby_accept').hide();
            		var lobby_custom_name = $('input[name="lobby_custom_name"]').val();
              		socket_data = {
						'lobby_id': lobby_custom_name,
						'group_bet_id': group_bet_id,
						'user_id': srcData.lobby_payment_update_data[0].user_id,
						'table_cat': srcData.lobby_payment_update_data[0].table_cat,
						'on':'get_played_group_data'
					};
					socket.emit('sendrealtimedata', socket_data );
					window.location.href = base_url+'Payment/lobby_payment_success/'+lobby_id+'/'+srcData.user_id;
				}
			});
	     	// } else {
     		// alert("You do not have enough credits to place a challenge. Please Proceed to Purchase tab for more.");
     		// window.location.href = base_url+'points/buypoints';
		    // }
		});
		if ($('.live_lby_ready_timer .reporttimer').length) {
			$('.live_lby_ready_timer .reporttimer').startTimer({
			  onComplete: function(element){
			    element.addClass('is-complete');
			    var lobby_id, group_bet_id;
			    group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
				lobby_id = $('input[name="lobby_id"]').val();
			    
			    var postData = {
			        'group_bet_id': group_bet_id,
			        'lobby_id':lobby_id
			      };
			    $.ajax({
			          type: 'post',
			          url: base_url+'livelobby/report_timer_expire',
			          data: postData,
			          success: function(res){
			            var res = $.parseJSON(res);
			            window.location.href = base_url+'livelobby/watchLobby/'+res.lobby_id;
			          }
			        });
			    }
			});
		}

		$('.send_tip_team a.send_tips').click(function () {
			$('#send_tip_amount').show();
		});
		$('.commonmodal .close').click(function(){
			$('.commonmodal').hide().removeClass('image_modal'); $('.commonmodal .modal_data').removeClass('image_modal_data').html('');
		});

		$('.send_tip_btn').click(function () {
			var streamer_id = $(this).attr('streamer_id');
			$('#send_tip_amount .team_member input[type="checkbox"]#'+streamer_id).attr('checked','');
			$('#send_tip_amount .team_member input[type="checkbox"]').not($('#send_tip_amount .team_member input[type="checkbox"]#'+streamer_id)).removeAttr('checked');
			$('#send_tip_amount .team_member .form-group').removeClass('active_usr');
			$('#send_tip_amount .team_member .form-group#activeusr_'+streamer_id).addClass('active_usr');
			$('#send_tip_amount').show();
		});
		$('#send_tip_to_team .no, #send_tip_to_team .close').click(function(){
			$('#send_tip_to_team').hide();
		});	
	    $('#send_tip_to_team .yes').click(function(){
			$('#send_tip_to_team').hide();
			$('#send_teamtip_amount').show();
		});
		$('#send_tip_amount input[type="button"]').click(function (e) {
			e.preventDefault();
			var amount = $('#send_tip_amount input[name="tip_amount_input"]').val();

			var checkbox_val = $('#default_sound').val();
			if(checkbox_val==0){
				var file_val = $('#fireworks_audio').val();
				
				if(file_val!='')
				{
					var file_size=$("#fireworks_audio")[0].files[0].size/1024/ 1024;
					var file_size_mb=file_size.toFixed(0);
				}
			}
			
			if (amount!='') {
				var to_usr_arr = [];
				$('#send_tip_amount .team_member .active_usr input[type="checkbox"]:checked').each(function(){
					to_usr_arr.push($(this).attr('id'));
				});
				var to_usr_arr = to_usr_arr;
				if (to_usr_arr.length>0) {
					if(checkbox_val==0 && file_val=="")
					{
						$('#send_tip_amount .alert.tip_amount_error').html('Please Select file')
						$('#send_tip_amount .alert.tip_amount_error').show();
					}
					else{
							if(file_size_mb>2)
						   {
								$('#send_tip_amount .alert.tip_amount_error').html('Please Select file up to 2MB')
								$('#send_tip_amount .alert.tip_amount_error').show();
							}
							else
							{

							var group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
					        var lobby_id = $('input[name="lobby_id"]').val();
					        var user_id = $('input[name="user_id"]').val();
					        var from = $('input[name="user_id"]').val();
					        
					        var postData = {
					        	'from':from,
					        	'lobby_id':lobby_id,
					        	'group_bet_id':group_bet_id,
					        	'amount':amount,
					        	'to_usr_arr':to_usr_arr,
					        };
					        if ($('#fireworks_audio').prop('files').length) {
						        var formdata = new FormData();
						        var audio_file = $('#fireworks_audio').prop('files')[0];
						        // console.log(audio_file);
						         
						        // formData.append('audio', $('input[type=file]')[0].files[0]);
						        // console.log($('#fireworks_audio')[0].files[0]);
										formdata.append('file', audio_file);
										// formdata.append('amount', 1111);
										// var contents = formdata.entries(audio_file);
										// formdata.append('fireworks_audio', $('#fireworks_audio')[0].files[0]);
								  
						        
						        postData.formdata_file = {'file' : formdata.get('file')};
					        }
					         // send_tip_to_team(postData);
						}
					}
				} 
				 else {
					$('#send_tip_amount .alert.tip_amount_error').html('Please Select User')
					$('#send_tip_amount .alert.tip_amount_error').show();
				}

			} else {
				$('#send_tip_amount .alert.tip_amount_error').html('Please Enter Amount');
				$('#send_tip_amount .alert.tip_amount_error').show();
				return false;	
			}
		});
		$('#send_teamtip_amount .no, #send_teamtip_amount .close').click(function(){
			$('#send_teamtip_amount').hide();
		});

		$('#send_tip .no, #send_tip .close').click(function(){
			$('#send_tip').hide();
		});	
		$('#send_tip .yes').click(function(){
			$('#send_tip').hide();
			var streamer_id = $(this).attr('streamer_id');
			if (streamer_id.length>0) {
				$('#send_tip_amount input.btn-submit').attr('to',streamer_id);
				$('#send_tip_amount').show();
			}
		});	
		$('#send_tip_amount .no, #send_tip_amount .close').click(function(){
			$('#send_tip_amount').hide();
		});

		//Page Loading event
		var data = {
			'event': 'single_page_load'
		};
		commonappend(data);

		
		$('#send_tip_amount .team_donate_bottom_div input[name="tip_amount_input"]').on('keyup keypress', function(){
			var amount = $(this).val();
			tip_change_am(amount);
			$('#send_tip_amount .alert.tip_amount_error').html('');
		});
		$('.join_lby_game_btn_div select[name="change_game_size"]').on('change', function(){
			var group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
			var lobby_id = $('input[name="lobby_id"]').val();
		  	var user_id = $('input[name="user_id"]').val();
		  	var game_size = $(this).val();
			var socket_data;
			if (game_size.length > 0) {
				var postData = { 
		        	'lobby_id':lobby_id,
		        	'game_size': game_size,
		        	'group_bet_id':group_bet_id
		        };
        		$.ajax({
				    url : base_url +'livelobby/change_game_size',
				    data: postData,
				    type: 'POST',
			    	success: function(res) {
		    			var res = $.parseJSON(res);
	        			var lobby_custom_name = $('input[name="lobby_custom_name"]').val();
	        			socket_data = {
							'lobby_id': lobby_custom_name,
							'group_bet_id': res.group_bet_details[0].id,
							'refresh': res.refresh
						};
						socket.emit( 'change_lobby_size', socket_data );
					}
				});
      		}
    	});

		// $('span.pasctrl').dblclick(function () {
		// 	var user_id = $(this).attr('id');
		// 	if (user_id != undefined) {
		// 		var user_to_fan_tag = $(this).attr('fan_tag');
		// 		var passctrlh4 = 'You Are Passing Control to '+user_to_fan_tag;
		// 		$('#change_pass_control h4.passctrlh4').html(passctrlh4);
		// 		$('#change_pass_control a.yes').attr('pass_to',user_id);
		// 		$('#change_pass_control').show();
		// 	}
		// });

		$('#change_pass_control .yes').click(function(){
			var group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
	      	var lobby_id = $('input[name="lobby_id"]').val();
	      	var user_id = $('input[name="user_id"]').val();
	      	var pass_to = $(this).attr('pass_to');
	      	var amount = $('input[name="lobby_accept_price"]').val();
	      	var postData = { 
	      		'user_id':user_id,
	      		'lobby_id':lobby_id,
	      		'group_bet_id':group_bet_id,
	      		'amount':amount,
	      		'pass_to': pass_to
	      	};
			pass_ctrl(postData);
		});
		
		$(function() {
		  	$("body").on("contextmenu", ".lobby_group_msg_bx .message-box.del_opt, .commonchatbox_msg_bx .message-box.del_opt", function(e) {
		    	var tgcontmenu = $(this).attr('tgcontmenu');
		    	var contextMenu = $(".contextMenu.contmenu_"+tgcontmenu);
		       	$(".contextMenu").hide();
		       	contextMenu.css({
		            display: "block",
		            // background: '#000'
		            width: 'auto',
		            position: 'absolute'
		            //left: e.pageX,
		            //top: e.pageY
		       	});
		       	return false;
		  	});
			$('html').click(function() {
			  	$(".contextMenu").hide();
			});
		});
		
		$('#change_stream_modal .no, #change_stream_modal .close').click(function(){
			$('#change_stream_modal').hide();
		});	
		$('#change_stream_modal .yes').click(function(){
			$('#change_stream_modal').hide();
			var change_status = $(this).attr('change_status');
			var stream_type = $(this).attr('stream_type');
			var lobby_is_event = $('input[name="lobby_event"]').val();
			
			// if (change_status == 'enable') {
				// if (lobby_is_event == 1) {
				//   	var group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
				//     var lobby_id = $('input[name="lobby_id"]').val();
				//     var user_id = $('input[name="user_id"]').val();
				//     var ajaxurl = (stream_type == 1) ? base_url +'Streamsetting/get_default_stream/'+user_id : base_url +'Streamsetting/get_default_obs_stream/'+user_id;
				//     var post_data = { 'data' : 'json' };
				//     $.ajax({
				//     	url : ajaxurl,
				//     	data : post_data,
				//     	type : 'POST',
				//     	success: function(res) {
				// 	      	var res = $.parseJSON(res);
				// 	      	if (res.count_get_defaultstream != 0 && res.get_defaultstream.default_stream_channel != null && res.get_defaultstream.default_stream_channel != '') {
				// 	      		var postData = { 
				// 		        	'user_id':user_id,
				// 		        	'lobby_id':lobby_id,
				// 		        	'group_bet_id':group_bet_id,
				// 		        	'change_status':change_status,
				// 		        	'stream_type':stream_type,
				// 		        	'channel_name':res.get_defaultstream.default_stream_channel
				// 		        };
				// 		        send_stream_status(postData);
				// 		    } else {
				// 		      	$('#enter_stream_channel').show();
				// 		    }
				// 	    }
				// 	});
				// } else {
					var stream_type = $('#enter_stream_channel #stream_type_select').is(":checked") ? 'twitch' : 'obs';
					$('.stream_eldiv[stream_type="'+stream_type+'"]').show();
					$('#enter_stream_channel .alert').html('').hide();
					$('#enter_stream_channel').show();
				// }
			// } else {
			// 	var group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
		 //        var lobby_id = $('input[name="lobby_id"]').val();
		 //        var user_id = $('input[name="user_id"]').val();
		 //        var postData = { 
		 //        	'user_id':user_id,
		 //        	'lobby_id':lobby_id,
		 //        	'group_bet_id':group_bet_id,
		 //        	'change_status':change_status,
		 //        	'stream_type':stream_type
		 //        };
			// 	send_stream_status(postData);
			// }
		});

		// function endLiveStream(user_id){
		// 	var streamer_id = $('input[name="user_id"]').val();
		// 	if(user_id == streamer_id){
		// 		var group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
		//         var lobby_id = $('input[name="lobby_id"]').val();
		//         var postData = { 
		//         	'user_id':user_id,
		//         	'lobby_id':lobby_id,
		//         	'group_bet_id':group_bet_id
		//         };
		// 		send_stream_status(postData);
		// 	}
		// }

		$('.select_stream_chat select.select_chat').change(function () {
			var stream_channel = ($(this).find('option:selected').attr('stream-channel_name') != undefined) ? $(this).find('option:selected').attr('stream-channel_name') : 0;
			var streamer_name = ($(this).find('option:selected').attr('stream-streamer_name') != undefined) ? $(this).find('option:selected').attr('stream-streamer_name') : 0;
			var split = ($(this).find('option:selected').attr('id') != undefined) ? $(this).find('option:selected').attr('id').split('_') : 0;
			var id = (split != 0) ? split[1]: 0;
			var group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
			var lobby_id = $('input[name="lobby_id"]').val();
			var user_id = $('input[name="user_id"]').val();
		  
	      	var postData = { 
		      	'user_id': id,
		      	'lobby_id':lobby_id,
		      	'group_bet_id':group_bet_id,
		      	't_streamerchat':id,
		      	'streamer_name' : streamer_name,
		      	'stream_channel' : stream_channel,
	      	};
	      	postData.check = (id != 0) ? 'check' : 'uncheck';
	      	send_streamchat_status(postData);
		});
		
		$('#enter_stream_channel #stream_type_select').on('change',function (){
			var stream_type = $(this).is(":checked") ? 'twitch' : 'obs';
			var lobby_is_event = $('input[name="lobby_event"]').val();
			var group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
		    var lobby_id = $('input[name="lobby_id"]').val();
		    var user_id = $('input[name="user_id"]').val();
		    var ajaxurl = (stream_type == 'twitch') ? base_url +'Streamsetting/get_default_stream/'+user_id : base_url +'Streamsetting/get_default_obs_stream/'+user_id;
		    var post_data = { 'data' : 'json' };
		    $.ajax({
		    	url : ajaxurl,
		    	data : post_data,
		    	type : 'POST',
		    	beforeSend: function(){ loader_show(); },
		    	success: function(res) {
		      		loader_hide();
			      	var res = $.parseJSON(res);
			      	var default_key = (res.get_defaultstream != '' && res.get_defaultstream != null && res.get_defaultstream.default_stream_channel != undefined && res.get_defaultstream.default_stream_channel != null && res.get_defaultstream.default_stream_channel != '') ? res.get_defaultstream.default_stream_channel : '';
			      	$('#enter_stream_channel .alert').html('').hide();
			      	$('.stream_eldiv, .stream_option_div').not($('.stream_eldiv[stream_type="'+stream_type+'"]')).hide();
			      	$('.stream_eldiv[stream_type="'+stream_type+'"]').find('input[name="stream_channel_input"]').val(default_key);
					$('.stream_eldiv[stream_type="'+stream_type+'"]').show();
			    }
			});
		});
		$('#enter_stream_channel input[name="stream_channel_input"]').keypress(function(event) {
		    (event.which == 13) ? $('#enter_stream_channel .stream_sbmit').click() : '';
		});
		$('#enter_stream_channel .stream_sbmit').click(function () {
			var action = $(this).attr('action');
			var stream_type = $('#enter_stream_channel #stream_type_select').is(":checked") ? 'twitch' : 'obs';
			// var stream_type = $(this).attr('stream_type');
			var channel_name = $('#enter_stream_channel .stream_eldiv[stream_type="'+stream_type+'"] input[name="stream_channel_input"]').val();
			if (channel_name != '') {
		  		var group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
		        var lobby_id = $('input[name="lobby_id"]').val();
		        var user_id = $('input[name="user_id"]').val();

		        var save_stream_check = ($('#enter_stream_channel .stream_eldiv[stream_type="'+stream_type+'"] .save_stream_radio input[name="save_stream"]').prop('checked') == true) ? 1 : 0;
		        (stream_type == 'obs') ? save_stream_check = 1 : '';
		        if((stream_type == 'obs' && $('#force_start_stream_checkbox').prop("checked") == true) || (stream_type == 'twitch' && $('#start_twitch_stream_checkbox').prop("checked") == true)){
		        	var postData = { 
			        	'user_id':user_id,
			        	'lobby_id':lobby_id,
			        	'group_bet_id':group_bet_id,
			        	'save_stream_check':save_stream_check,
			        	'action':action,
			        	'stream_type' : stream_type,
			        	'channel_name':channel_name
			        };
			        send_stream_status(postData);
			        // connection.openOrJoin(channel_name);
		        }else{
		      	    var postData = { 
			        	'user_id':user_id,
			        	'lobby_id':lobby_id,
			        	'group_bet_id':group_bet_id,
			        	'change_status':'disable',
			        	'stream_type':stream_type
			        };
					send_stream_status(postData);
		        }
			} else {
				$('.stream_error').html('Please Enter Channel').show();
				return false;	
			}
		});
		$('#enter_stream_channel .no, #enter_stream_channel .close').click(function(){
			$('.stream_option_div').show();
			$('#enter_stream_channel, .stream_eldiv').hide();
		});
		
		$('.leave_lobby_btn_from_dash').click(function(){
			var leave_id = $(this).attr('usr_id');
			var lobby_id = $(this).attr('lobby_id');
			var group_bet_id = $(this).attr('group_bet_id');
			var is_creator = $(this).attr('lby_crtr');
			var is_controller = $(this).attr('lby_cntrlr');
			var player_tag_name = $(this).attr('player_tag_name');
			var self_user = $(this).attr('self');
			var everyone_readup = $(this).attr('everyone_readup');
			var leave_from_waitinlist = 'no';
			if ($(this).attr('leave_from_waitinlist') != undefined && $(this).attr('leave_from_waitinlist').length) {
				leave_from_waitinlist = 'yes';
			}
			leave_lobby(leave_id,lobby_id,group_bet_id,is_creator,is_controller,player_tag_name,self_user,everyone_readup,leave_from_waitinlist);
		});
		$('#leave_lobby_modal_from_dash .no, #leave_lobby_modal_from_dash .close').click(function(){
			$('#leave_lobby_modal_from_dash').hide();
			$('#leave_lobby_modal_from_dash .yes').removeAttr('leave_id');
			$('#leave_lobby_modal_from_dash .yes').removeAttr('creator');
		});
		$('#leave_lobby_modal_from_dash .yes').click(function(){
		    var group_bet_id = $(this).attr('group_bet_id');
		    var lobby_id = $(this).attr('lobby_id');
		    var leave_id = $(this).attr('leave_id');
		    var creator = $(this).attr('creator');
	  		var leave_from_waitinlist = $(this).attr('leave_from_waitinlist');
	  		var controller = $(this).attr('controller');
			var autoloss = $(this).attr('autoloss');
			var postData = { 
				'creator': creator,
				'lobby_id': lobby_id,
				'leave_id': leave_id,
				'group_bet_id': group_bet_id,
				'leave_from_waitinlist': leave_from_waitinlist,
			};
		    send_leave_report(postData);
		});
		$('input[name="lby_pw"]').keyup(function() {
			$('.lobby_password_error').hide();
		});
		$('#lobby_password_model .close').click(function(){
		    $('#lobby_password_model').hide();
		});
		$('.change_lobby_dtil').click(function(){
		    $('#change_lobby_details').show();
		});
		$('#change_lobby_details .close').click(function(){
		    $('#change_lobby_details').hide();
		});
		$('.edit_lobby_tag').click(function(){
		    $('#edit_lobby_tag_mdl').show();
		});
		$('#edit_lobby_tag_mdl .close').click(function(){
		    $('#edit_lobby_tag_mdl').hide();
		});
		$('#edit_lobby_tag_mdl input').keypress(function(event) {
		    if (event.keyCode == 13) {
		        event.preventDefault();
		        var lobby_id = $('input[name="lobby_id"]').val();
			    var lobby_tag = $('#edit_lobby_tag_mdl input[name="change_lobby_tag"]').val();
			    var group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
		      	if (lobby_tag != '' && lobby_tag != undefined) {
			        var postData = {
			        	'lobby_id':lobby_id,
			        	'group_bet_id':group_bet_id,
			        	'lobby_tag':lobby_tag
			        };
			        lobby_tag_change(postData);
		        } else {
		          $('.lobby_tag_error').html('Please Enter Your TAGID').show();
		        }
		    }
		});
		$('#edit_lobby_tag_mdl .yes').click(function(){
		    var lobby_id = $('input[name="lobby_id"]').val();
		    var lobby_tag = $('#edit_lobby_tag_mdl input[name="change_lobby_tag"]').val();
		    var group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
	      	if (lobby_tag != '' && lobby_tag != undefined) {
		        var postData = {
		        	'lobby_id':lobby_id,
		        	'group_bet_id':group_bet_id,
		        	'lobby_tag':lobby_tag
		        };
		        lobby_tag_change(postData);
	        } else {
	          $('.lobby_tag_error').html('Please Enter Your TAGID').show();
	        }
		});


		$('.edit_myteam_tag').click(function(){
	    	$('#edit_team_name_mdl').show();
		});
		$('#edit_team_name_mdl .close').click(function(){
		    $('#edit_team_name_mdl').hide();
		});
		$('#edit_team_name_mdl input').keypress(function(event) {
		    if (event.keyCode == 13) {
		        event.preventDefault();
		        var lobby_id = $('input[name="lobby_id"]').val();
		    	var group_bet_id = $('input[name="lobbygroup_bet_id"]').val(), team_name = $('input[name="change_team_name"]').val();
		    	var save_stream_opt = ($('.team_save_settings input[name="save_stream"]').prop('checked')) ? 'on' : 'off';
		      	if (team_name != '' && team_name != undefined) {
			        var postData = {
			        	'lobby_id' : lobby_id,
			        	'group_bet_id' : group_bet_id,
			        	'save_stream_opt' : save_stream_opt,
			        	'team_name' : team_name
			        };
			        lobby_tag_change(postData);
		        } else {
		          $('.lobby_team_name_error').html('Please Enter Your Team name').show();
		        }
		    }
		});
		$('#edit_team_name_mdl .yes').click(function(){
		    var lobby_id = $('input[name="lobby_id"]').val();
		    var group_bet_id = $('input[name="lobbygroup_bet_id"]').val(), team_name = $('input[name="change_team_name"]').val();
		    var save_stream_opt = ($('.team_save_settings input[name="save_stream"]').prop('checked')) ? 'on' : 'off';
		    if (team_name != '' && team_name != undefined) {
		        var postData = {
		        	'lobby_id' : lobby_id,
		        	'group_bet_id' : group_bet_id,
		        	'save_stream_opt' : save_stream_opt,
		        	'team_name' : team_name
		        };
		        lobby_tag_change(postData);
	        } else {
	          $('.lobby_team_name_error').html('Please Enter Your Team name').show();
	        }
		});

		if ($('#live_lobby_msgbox .mutetimer, .commonchatbox_msg_bx .mutetimer').length) {
			$('#live_lobby_msgbox .mutetimer, .commonchatbox_msg_bx .mutetimer').startTimer({
			  onComplete: function(element){
			    element.addClass('is-complete');
			    	if ($('.lobby_group_msg_bx .mutetimer_div, .commonchatbox_msg_bx .mutetimer_div').length) {
			    		$('.lobby_group_msg_bx .mute_div.mutetimer_div, .commonchatbox_msg_bx .mute_div.mutetimer_div').removeClass('mutetimer_div').html('');
			    		$('#live_lobby_msgbox .lobby_group_msg_bx .lobby_grp_msgbox, .commonchatbox_msg_bx .grp_msgbox').removeClass('disable').find('textarea').removeAttr('readonly');
			    	}
			    }
			});
		}
		if (typeof socket != 'undefined'){
			socket.on('get_changed_lobby_size',function(data){
				var winlocation = window.location.href.split('#')[0].toLowerCase();
				var lobby_id, group_bet_id, page_refresh;
				lobby_id = data.lobby_id.toLowerCase();
				if (base_url+'livelobby/'+lobby_id == winlocation) {
					window.location.reload();
				}
			});
			socket.on('getrealtimedata',function(getdata){
				var winlocation = window.location.href.split('#')[0].toLowerCase();
				if (getdata.data.on == 'pass_ctrl') {
					var lobby_id, group_bet_id, page_refresh;
					lobby_id = getdata.data.lobby_id.toLowerCase();
					if (base_url+'livelobby/'+lobby_id == winlocation) {
					 	window.location.href = base_url+'livelobby/'+getdata.data.new_lobby_id;
					}
				}
				if (getdata.data.on == 'change_lby_detail') {
					var lobby_id;
					lobby_id = getdata.data.lobby_id.toLowerCase();
					if (base_url+'livelobby/'+lobby_id == winlocation) {
						window.location.href = base_url+'livelobby/'+lobby_id;
					}
					if ((base_url+'livelobby/addFan/'+getdata.data.id).toLowerCase() == winlocation) {
						window.location.href = base_url+'livelobby/addFan/'+getdata.data.id;
					}
				}
				if (getdata.data.on == 'change_lby_tag') {
					var lobby_id;
					lobby_id = getdata.data.lobby_id.toLowerCase();
					if (base_url+'livelobby/'+lobby_id == winlocation) {
						window.location.href = base_url+'livelobby/'+lobby_id;
					}
				}
				if (getdata.data.on == 'delete_msg_from_lobby') {
					var lobby_id = getdata.data.lobby_id.toLowerCase();
					if (base_url+'livelobby/'+lobby_id == winlocation) {
						$('.message-row[data-mid="'+getdata.data.msg_id+'"]').remove();
					}
				}
				if (getdata.data.on == 'del_usr_from_looby_chat') {
					var lobby_id = getdata.data.lobby_id.toLowerCase();
					if (base_url+'livelobby/'+lobby_id == winlocation) {
						if ($('.lobbymsgbox_userid_'+getdata.data.user_id).length) {
							$('.lobbymsgbox_userid_'+getdata.data.user_id).remove();
						}
						if ($('.msg_usr_'+getdata.data.user_id).length) {
							$('.msg_usr_'+getdata.data.user_id).remove();
						}
					}
				}
				if (getdata.data.on == 'mute_usr_from_looby_chat') {
					var lobby_id = getdata.data.lobby_id.toLowerCase();
					if (base_url+'livelobby/'+lobby_id == winlocation) {
				      	var user_id = $('input[name="user_id"]').val();
				      	if (user_id == getdata.data.user_id) {
				      		$('.live_lobby_chatbox_main_row #message-container .lobby_grp_msgbox').addClass('disable');
				      		$('.live_lobby_chatbox_main_row #message-container .lobby_grp_msgbox textarea').attr('readonly','');
				      		var append_mute_timer = 'You can chat after <span class="mutetimer" data-seconds-left="'+getdata.data.mutediff+'"></span> Seconds';
				      		$('.mute_div').addClass('mutetimer_div').html(append_mute_timer);
				      		$('#live_lobby_msgbox .mutetimer').startTimer({
							  onComplete: function(element){
							    element.addClass('is-complete');
							    	if ($('.lobby_group_msg_bx .mutetimer_div').length) {
							    		$('.lobby_group_msg_bx .mute_div.mutetimer_div').removeClass('mutetimer_div');
							    		$('.lobby_group_msg_bx .mute_div').html('');
							    		$('#live_lobby_msgbox .lobby_group_msg_bx .lobby_grp_msgbox').removeClass('disable');
							    		$('#live_lobby_msgbox .lobby_group_msg_bx .lobby_grp_msgbox textarea').removeAttr('readonly');
							    	}
							    }
							});
				      	}
					}
				}
				if (getdata.data.on == 'send_messagelobby_grp') {
					var lobby_id = getdata.data.lobby_id.toLowerCase();
					if (base_url+'livelobby/'+lobby_id == winlocation) {
						var my_user_id = $('input[name="user_id"]').val();
						var crtr = $('input[name="crtr"]').val();
						if (my_user_id == crtr) {
							var lbyctr = 1;
						}
						var direction = 'left';
	                      var download_attr = 'download';
						var clss = 'enable';												
						if (my_user_id == getdata.data.session_id) {							
								direction = 'right';
								download_attr = '';
								clss = 'disable';	
						}
						var get_msg_bx_height = $('.lobby_group_msg_bx').height();
						var context_menu = '';
						var cn_clss = '';
						
						if (lbyctr || getdata.data.is_admin == 1) {
							cn_clss = "del_opt";
						} else {
							cn_clss = '';
						}

						if(lbyctr || getdata.data.is_admin == 1){
							context_menu = '<div class="dropdown clearfix contmenu_'+getdata.data.msg_data.message_id+' contextMenu"><ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu" style="display:block;">';
							context_menu += '<li><a class="remove_msg" onclick="send_msg_delete('+getdata.data.msg_data.message_id+','+getdata.data.msg_data.user_id+')">Remove Msg</a></li>';
							if (direction == 'left') {
								context_menu += '<li><a class="remove_userid" onclick="del_usr_from_chat('+getdata.data.msg_data.user_id+');" >Remove User</a></li><li><a class="mute_user" onclick="mute_usr_from_chat('+getdata.data.msg_data.user_id+');">Time Out User</a></li>';
							}
							context_menu += '</ul></div>';
						}
						var msg = getdata.data.msg_data.message;
						if (getdata.data.msg_data.message_type == 'file') {
							msg = '<a href="'+base_url+'upload/lobby_pics_msg/'+getdata.data.msg_data.attachment+'" class="download_attachment lobby_grpmsg_attach_link '+clss+'" '+download_attr+'><i class="fa fa-download '+clss+'"></i><img class="attachment_image img img-thumbnail" src="'+base_url+'upload/lobby_pics_msg/'+getdata.data.msg_data.attachment+'" class="lobby_grpmsg_attach" alt="pic" width="100px"></a>';
						} else if (getdata.data.msg_data.message_type == 'tipmessage') {
							var tipicon = '<img src="'+base_url+'upload/stream_setting/'+getdata.data.msg_data.attachment+'" alt="tipimg" class="tipiconimg">';
							if (my_user_id == getdata.data.session_id) {
								msg = tipicon+getdata.data.msg_data.message;
							} else {
								msg = getdata.data.msg_data.message+tipicon;
							}
						}
						var img = '';
						if(getdata.data.is_admin == 1){
						   var img = '<img class="chat_image pull-'+direction+'" width="20" height="20" src="'+base_url+'upload/admin_dashboard/Chevron_3_Twitch_72x72.png">';
						}
						var append_data = '<div data-mid="'+getdata.data.msg_data.message_id+'" data-cid="'+getdata.data.msg_data.id+'" class="message-row"><div class="col-md-12 col-sm-12 col-xs-12 message-box '+cn_clss+'" tgcontmenu="'+getdata.data.msg_data.message_id+'"><div class="message_header">'+img+'<small class="list-group-item-heading text-muted text-primary pull-'+direction+'">'+getdata.data.name+'</small></div><div class="message_row_container in pull-'+direction+'"><p class="list-group-item-text"></p><p>'+msg+'</p>'+context_menu+'<p></p></div></div></div>';
						$('.live_lobby_chatbox_main_row #message-inner').append(append_data);
						$(".live_lobby_chatbox_main_row #message-inner .no_messages").remove();

						$("body").on("contextmenu", ".lobby_group_msg_bx .message-box.del_opt", function(e) {
						  var tgcontmenu = $(this).attr('tgcontmenu');
						  var contextMenu = $(".contextMenu.contmenu_"+tgcontmenu);
						     $(".contextMenu").hide();
						     contextMenu.css({
						          display: "block",
						          width: 'auto',
						          position: 'absolute'
						     });
						     return false;
						});
						$('html').click(function() {
						     $(".contextMenu").hide();
						});

						var data = {
							'lobby_id':lobby_id,
							'audio_file':getdata.data.getmytip_sound,
				        	'event': 'msg_sound_play'
						};
						commonappend(data);
					}
				}
				if (getdata.data.on == 'change_head_of_table') {
					var lobby_id = getdata.data.lobby_id.toLowerCase();
					if (base_url+'livelobby/'+lobby_id == winlocation) {
						var table = getdata.data.table_cat.toLowerCase();
						if ($('.'+table+'_table a.leave_lobby').length) {
							$('.'+table+'_table a.leave_lobby').attr('lby_cntrlr','no');
							$('.'+table+'_table a[usr_id="'+getdata.data.head_id+'"]').attr('lby_cntrlr','yes');
						}
						if (getdata.data.ready_up == 1) {
							if (getdata.data.head_id != 0) {
								var user_id = $('input[name="user_id"]').val();
								if ($('.'+table+'_table .team_leader img').length) {
									$('.'+table+'_table .team_leader img').remove();
								}
								$('#teamhead_'+getdata.data.head_id).html('<img src="'+base_url+'assets/frontend/images/table_leader_new.png " alt="Team Controller">');
								if ($('.'+table+'_table button.bet_report').length) {
									$('.'+table+'_table button.bet_report').remove();
								}
								if (user_id == getdata.data.head_id) {
									$('.group_user_row_'+getdata.data.head_id).append('<button class="bet_report button text-'+table+'">Report</button>');
								}
							} else {
								$('.'+table+'_table span.team_leader img').remove();
								$('.'+table+'_table button.bet_report').remove();
							}
						} else {
							if ($('.'+table+'_table .team_leader img').length) {
								$('.'+table+'_table .team_leader img').remove();
							}
							$('.'+table+'_table span.team_leader img').remove();
							$('#teamhead_'+getdata.data.head_id).html('<img src="'+base_url+'assets/frontend/images/table_leader_new.png " alt="Team Controller">');
						}
					}
					var data = {
						'event': 'single_page_load'
					};
					commonappend(data);
				}
				if (getdata.data.on == 'balance_update') {
					var user_id = $('input[name="user_id"]').val();
					console.log('balance_arr',getdata.data.balance_arr);
					$.map( getdata.data.balance_arr, function( val, i ) {
						if (user_id == i) {
							$('.login-sec .header_available_balance.user_id_'+i+' span.tot-balance').html('$'+getdata.data.balance_arr[user_id]);
						}
					});
				}
				if (getdata.data.on == 'get_team_tip_data') {
					var get_grp_with_balance, sender_id, current_user_id, append_data ,final_append_data;
					current_user_id = $('input[name="user_id"]').val();
					for (var i = getdata.data.get_grp_with_balance.length - 1; i >= 0; i--) {
						if (getdata.data.get_grp_with_balance[i].user_id == current_user_id) {
							append_data = '$'+getdata.data.get_grp_with_balance[i].total_balance;	
						}
					}
					if(current_user_id == getdata.data.sender_id){
						final_append_data = '$'+getdata.data.sender_balance;
					} else {
						final_append_data = append_data;
					}
					$('.login-sec span.tot-balance').html(final_append_data);
					if (getdata.data.tip_fireworks_sound != '') {
						
						$('body').append('<div class="clearfix btn_disable_overlay"><div class="fireworks_blast"></div></div>');
						$('.fireworks_blast').fireworks();
						if($('.fireworks_audio_file').length && $('.fireworks_audio_file') != undefined)
						{
						  $('.fireworks_audio_file').attr('src',base_url + 'upload/stream_setting/' + getdata.data.tip_fireworks_sound );
						  var session_volume=$('#session_volume').val();
						  
						 if(session_volume=="")
						 {
						 	volume_val=1.0;
						 }
						 else
						 {
						 	
						  var volume_val=session_volume/100;
						  var volume_val=volume_val.toFixed(1);
						  
						 }
						  
						  // $('audio,video')[0].volume =volume_val;
						  $("audio,video").prop("volume", volume_val);
						
						  $('.fireworks_audio_file')[0].play();
						}
						
						setTimeout(function () {
							$('.fireworks_blast').fireworks('destroy');
							($('.fireworks_audio_file').length && $('.fireworks_audio_file') != undefined) ? $('.fireworks_audio_file').trigger("pause") : '';
							$('.fireworks_blast, .btn_disable_overlay').remove();
						}, 7000);
					}
				}
				if (getdata.data.on == 'change_stream_status_data') {
					// if (base_url+'livelobby/'+getdata.data.lobby_id.toLowerCase() == winlocation) {
						var data = {
							'follower_count':getdata.data.follower_count,
							'is_followd':getdata.data.is_followd,
							'is_subscribed':getdata.data.is_subscribed,
							'lobby_id':getdata.data.lobby_id,
							'group_bet_id':getdata.data.group_bet_id,
							'streamer_id':getdata.data.streamer_id,
							'fan_tag':getdata.data.fan_tag,
							'streamer_name':getdata.data.streamer_name,
							'streamer_channel':getdata.data.streamer_channel,
							'streamer_img':getdata.data.streamer_img,
							'streamer_type':getdata.data.stream_type,
							'updated_data':getdata.data.updated_data,
							'table_cat':getdata.data.table_cat,
							'is_creator':getdata.data.is_creator,
							'subscribedlist':getdata.data.subscribedlist,
							'event': 'change_stream_status_data'
						};
						commonappend(data);	
					// }
				}
				if (getdata.data.on == 'endStreamRemoveBanner') {
					if (base_url == winlocation || base_url+'/home' == winlocation) {
						$(document).find('.stream_carousel_container .stream_carousel .item').each(function(){
							if($(this).find('video').attr('data-key') == getdata.data.key && $(this).find('video').attr('data-user_id') == getdata.data.is_creator){
								$(this).remove();
							}
						});
						carouselcal($(".stream_carousel_container"));
					}
				}
				if (getdata.data.on == 'send_user_to_waitinglist') {
					if (base_url+'livelobby/'+getdata.data.lobby_id.toLowerCase() == winlocation) {
						var data = {
							'lobby_id':getdata.data.lobby_id,
							'get_lobby_detail':getdata.data.get_lobby_detail,
							'get_fan_detail':getdata.data.get_fan_detail,
							'table_cat':getdata.data.table_cat,
							'remaining_table':getdata.data.remaining_table,
							'full_table':getdata.data.full_table,
							'is_full_play':getdata.data.is_full_play,
							'post_userid':getdata.data.user_id,
							'user_id_balance':getdata.data.user_id_balance,
							'creator_in_waitinglist':getdata.data.creator_in_waitinglist,
							'is_spectator':getdata.data.is_spectator,
							'action': 'send_user_to_waitinglist',
				        	'event': 'bottom_choose_tables_update'
						};
						commonappend(data);	
						if ($('.group_user_row_'+getdata.data.user_id).length) {
							$('.group_user_row_'+getdata.data.user_id).remove();
						}
						toastr.success(getdata.data.get_fan_detail.fan_tag+" moved to "+getdata.data.table_cat.toLowerCase()+" table waiting list");
					}
				}
				if (getdata.data.on == 'get_played_group_data') {
					if (base_url+'livelobby/'+getdata.data.lobby_id.toLowerCase() == winlocation) {
						var lobby_id = $('input[name="lobby_id"]').val();
						var group_bet_id;
						var postData = {
							'lobby_id': lobby_id,
							'group_bet_id': getdata.data.group_bet_id,
							'userid': getdata.data.user_id,
							'table_cat': getdata.data.table_cat
						};
						$.ajax({
							url : base_url +'livelobby/get_playedgrp_data',
							type: "post",
							data: postData,
							success: function(srcData){
								srcData = $.parseJSON(srcData);	
								
								//Get played data in commonapend function
								var senddata = {
									'get_playedgrp_data_count':srcData.get_playedgrp_data_count,
									'get_playedgrp_data':srcData.get_playedgrp_data,
									'get_lobby_size':srcData.get_lobby_size,
									'is_full_play':srcData.is_full_play,
									'userid':getdata.data.user_id,
									'sessuser_id':srcData.user_id,
									'lobby_creator':srcData.lobby_creator,
									'event':'get_played_data'
								};
								commonappend(senddata);
							},
							error: function(XMLHttpRequest, textStatus, errorThrown) {}
						});

					}
				}
				if (getdata.data.on == 'leave_lobby_data') {
					if (base_url+'livelobby/'+getdata.data.lobby_id.toLowerCase() == winlocation) {
						var senddata = {
							'lobby_id': getdata.data.lobby_id,
							'group_bet_id': getdata.data.group_bet_id,
							'user_id': getdata.data.user_id,
							'refresh': getdata.data.refresh,
							'leaved_total_balance': getdata.data.leaved_total_balance,
							'table_cat': getdata.data.table_cat,
							'event' : 'get_leave_lobby_data'
						};
						commonappend(senddata);
					}
				}
				if (getdata.data.on == 'join_wtnglst_data') {
					if (base_url+'livelobby/'+getdata.data.lobby_id.toLowerCase() == winlocation) {
						var senddata = {
							'lobby_id':getdata.data.lobby_id,
							'user_id':getdata.data.user_id,
							'join_wtnglst_data':getdata.data.get_my_fan_tag,
							'waiting_table':getdata.data.waiting_table,
							'is_spectator':getdata.data.is_spectator,
							'event':'get_witinglst_data'
						};
						commonappend(senddata);
					}
				}
				if (getdata.data.on == 'send_lobby_group_data') {
					if (base_url+'livelobby/'+getdata.data.lobby_id.toLowerCase() == winlocation) {
						var senddata = {
							'lobby_id': getdata.data.lobby_id,
							'group_bet_id': getdata.data.group_bet_id,
							'table_cat': getdata.data.table_cat,
							'user_name': getdata.data.user_name,
							'user_id' : getdata.data.user_id,
							'get_defaultstream' : getdata.data.get_defaultstream,
							'change_user_vice_versa' : getdata.data.change_user_vice_versa,
							'event' : 'get_lobby_group_data'
						};
						commonappend(senddata);
					}
				}
				if (getdata.data.on == 'cmn_event') {
					if (base_url+'livelobby/'+getdata.data.lobby_id.toLowerCase() == winlocation) {
						var senddata = {
					  	'addClass': getdata.data.addClass,
							'event' : 'toss_coin'
						};
						commonappend(senddata);
					}
				}
				if (getdata.data.on == 'reset_giveaway') {
					if (base_url+'livelobby/'+getdata.data.lobby_id.toLowerCase() == winlocation || base_url+'giveaway/'+getdata.data.id.toLowerCase() == winlocation) {
						window.location.reload();
					}
				}
				if (getdata.data.on == 'send_realtime_stream') {
				}

				// if (getdata.data.on == 'get_livestream') {
				// 	if (base_url+'livelobby/'+getdata.data.lobby_id.toLowerCase() == winlocation) {
				// 		var event = getdata.data.eventdata;
				// 		if (event.type === 'remote') {
				// 			var mediaElement = getMediaElement(event.mediaElement, {
				// 				title: event.userid,
				// 				buttons: [],
				// 				width: width,
				// 				showOnMouseEnter: false
				// 			});
				// 			mediaElement.style.zIndex = "1";
				// 			mediaElement.style.position = 'absolute';
				// 			mediaElement.style.top = 10 + 'px';
				// 			mediaElement.style.left = 20 + 'px';

				// 			document.getElementById("videos-container").appendChild(mediaElement);
				// 		}
				// 		if (event.type === 'local') {
				// 			var mediaElement = getMediaElement(event.mediaElement, {
				// 				title: event.userid,
				// 				buttons: [],
				// 				width: width / 3,
				// 				showOnMouseEnter: false
				// 			});
				// 			mediaElement.style.zIndex = "9999";
				// 			mediaElement.style.position = 'absolute';
				// 			mediaElement.style.top = 10 + 'px';
				// 			mediaElement.style.left = 20 + 'px';

				// 			document.getElementById("videos-container").appendChild(mediaElement)
				// 		}
				// 	}
				// }
			});
		}

	/**
	 * Send message button
	 * 
	 * @return {[Void]}
	 */
	$("#send-message").click(function(){
		var el = {};
		el.container = $("#message-container");
		sendMessage(el);
	});
	
	/**
	 * Send message to admin button
	 * 
	 * @return {[Void]}
	 */
	$("#send-message-to-admin").click(function(){
		var el = {};
		el.container = $("#message-container");
		sendMessageToAdmin(el);
	});

	/**
	 * Send message to Friend
	 * 
	 * @return {[Void]}
	 */
	$("#send-message-to-friend").click(function(){
		var el = {};
		el.container = $("#message-container");
		sendMessageToFriend(el);
	});

	
	/**
	 * Send message to customer button
	 * 
	 * @return {[Void]}
	 */
	$("#send-message-to-customer").click(function(){
		var el = {};
		el.container = $("#message-container");
		sendMessageToCustomer(el);

		// socket.on("recieved-message", function(data){
		// 	get_single_message(data);
		// 	$(".cs-audio").get(0).play();
		// });
	});

	
	/**
	 * Send message to customer button
	 * 
	 * @return {[Void]}
	 */
	$("#send-message-to-pvp-player").click(function(){
		var el = {};
		el.container = $("#message-container_pvp");
		sendMessageToPvpPlayer(el);
		// socket.on("recieved-message", function(data){
		// get_single_message(data);
		// $(".cs-audio").get(0).play();
		// });

	});




	/**
	 * Send message to customer button
	 * 
	 * @return {[Void]}
	 */
	$("#send-message-to-pvp-player-from-admin").click(function(){
		var el = {};
		el.container = $("#message-container_pvp");
		sendMessageToPvpPlayerFromAdmin(el);
		// socket.on("recieved-message", function(data){
		// get_single_message(data);
		// $(".cs-audio").get(0).play();
		// });

	});

	/**
	 * Call admin for help button
	 * 
	 * @return {[Void]}
	 */
	$("#call-admin-for-help").click(function(){
		var calladminvalue, args;
		calladminvalue = $("#calladminvalue").val();
		var groupid = $("#groupid").html(); 
		args = {'groupid':groupid,'calladminvalue':calladminvalue};
		callAdminForHelp(args);
	});




	/*if users list is empty, disable input for message*/
	var initUsersList = function(){
		var cle, elem;
		if($(".agent_table table tbody tr").length==0){
			$("#message-input").prop(true);
			return false;
		}

		/* trigger click event for the first user */
		cle = document.createEvent("MouseEvent");
	    cle.initEvent("click", true, true);
	    elem = $("#cs-ul-users li")[0];
	    // elem.dispatchEvent(cle);
	};


	initUsersList();




/*start chatbox functions*/
	/**
	 * It will open and close the tray of the chatbox
	 */
	$(document).on('click','.chatbox-title-tray, .xwb-chat .title h5', function(){
		var $sideuserTitle, state, $csrf;
		$sideuserTitle = $(this).parents('.xwb-contact-sidebar');

		if($sideuserTitle.find('h5.xwb-sideuser-title').length == 1){
			state = 'close';
			if($sideuserTitle.hasClass('chatbox-tray'))
				state = 'open';

			var postData = {
					'cs_key' : 'xwb_open_close_sideuser',
					'state'	: state
				};

			$csrf = $('.csrf_key');
			csrf_name = $csrf.attr('name');
			csrf_key = $csrf.val();
			postData[csrf_name] = csrf_key;

			$.ajax({
				url: window.location.pathname,
				type: "get",
				data: postData,
				dataType: 'JSON',
				success: function(data){
					$('input[name="'+csrf_name+'"]').val(data.csrf_key);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {}
			});
		}
        $(this).parents('.xwb-chat').toggleClass('chatbox-tray');
    });
	
    /* It removes the chatbox */
    $(document).on('click','.chatbox-title-close', function(){
        $(this).parents('.xwb-chat').remove();
    });

    /* open a chatbox tray when clicked in the list of users/conversation */
    $(document).on("click","#cs-ul-sideusers li",function(){
    	var user_to, ci, user_name, online, textarea_profix, args, chatbox;
    	var $xwbChat, $conversationArea, $conversationContainer, textArea;
    	user_to = $(this).attr('data-user');
    	ci = $(this).data('ci');
    	user_name = $('a span.xwb-display-name',this).text();
    	online = $(this).hasClass('online');
    	online = online?'online':'offline';

    	if(ci!="") // if group conversation no status display
    		online = '';

    	user_name = $.trim(user_name);

    	textarea_profix = user_to;
    	if(ci!="")
    		textarea_profix = 'g_'+ci; // profix `g_` for the group conversation to avoid conflicts

		args = {'user_to':user_to,'start_from':''};


		if($("div.xwb-chat[data-user='" + user_to + "']").length > 0) // return false if the chatbox already open
			return false;

		/*Chatbox HTML*/
    	chatbox  = '<div class="xwb-chat xwb-cb-chat '+online+'" data-user="'+user_to+'">'+
	        '<div class="title">'+
	        	'<h5 class="conversation-title"><a href="javascript:;">'+user_name+'</a></h5>'+
		        '<button class="chatbox-title-tray">'+
		            '<span></span>'+
		        '</button>'+
		        '<button class="chatbox-title-close">'+
		            '<span>'+
		                '<svg viewBox="0 0 12 12" width="12px" height="12px">'+
		                    '<line stroke="#FFFFFF" x1="11.75" y1="0.25" x2="0.25" y2="11.75"></line>'+
		                    '<line stroke="#FFFFFF" x1="11.75" y1="11.75" x2="0.25" y2="0.25"></line>'+
		                '</svg>'+
		            '</span>'+
		        '</button>'+
	        '</div>'+
	        '<div class="cb-conversation-container">'+
		        '<div class="conversation-container message-inner">'+
		        '</div>'+
		        '<div class="chatbox">'+
		            '<textarea id="xwb_cb_input_'+textarea_profix+'" class="form-control message-input" style="resize:none;" rows="4" name="message-input" placeholder="Enter your message here ..."></textarea>'+
		            '<input type="hidden" name="user_id" class="user_id" id="user_id" value="">'+
		            '<input name="ci" class="ci" id="ci" value="'+ci+'" type="hidden" />'+
		        '</div>'+
	        '</div>'+
	    '</div>';

	    $("#xwb-bottom-chat-container").append(chatbox);

	    $xwbChat = $('#xwb-bottom-chat-container').find('div.xwb-chat[data-user="'+user_to+'"]');
	    $conversationArea = $xwbChat.find('.cb-conversation-container');
	    $conversationContainer = $xwbChat.find('.conversation-container');
	    textArea = $xwbChat.find('textarea');
		args = {'ci':ci, 'user_to':user_to,'start_from':'', elContainer: $conversationArea};

	    get_messages(args); // get messages of the newly opened chatbox tray


	    /**
	     * Scroll function on every chatbox tray to view previous messages
	     */
		$conversationContainer.scroll(function() {
			var $message_row, message_inner_position, message_row, ci, user_to, args;
			$message_row = $("div.message-row",this);

			message_inner_position = $(this).offset().top + 2; // plus 2 for the margin of message_row

			if($message_row.first().offset() != undefined){
				message_row = $message_row.first().offset().top; //get the first message row top position
				ci = $(this).parent('.cb-conversation-container').attr('data-cnid');

				// trigger load previous messages when top position reached
				if(message_inner_position == message_row){ 
					console.log('loading next message...');
					$(this).prepend('<div class="row loading_container"><h5 class="text-center">Loading more messages...</h5></div>');
					
					user_to = $(this).next(".chatbox").find('.user_to').val();
					args = {'ci':ci, 'user_to':user_to, start_from: ($message_row.length), elContainer: $conversationArea};
					get_messages(args, true);
				}
			}
		});


		/**
		 * EmojioneArea plugin on each chatbox tray input to enable the emoticons/icons
		 */
		$('#xwb_cb_input_'+textarea_profix).emojioneArea({
			pickerPosition: "top",
    	tonesStyle: "radio",
    	// saveEmojisAs: "shortname",
    	events: {
		    keypress: function (editor, event) {
		    	var editorID, el;
					if (event.which == 13 && !event.shiftKey){ // send message if enter is pressed
						event.preventDefault();
						$(textArea).val($(textArea).data("emojioneArea").getText());
						editorID = $(editor).parents('.chatbox').find('textarea').prop('id');
						el = {};
						el.container = $(editor).parents('.xwb-cb-chat');
						el.editorID = editorID;
						sendMessage(el);
					}
				}
			}
		});


		/*Append the attach button beside the emojionearea icon*/
		var $chatboxDivEmoji = $('#xwb_cb_input_'+textarea_profix);
		$chatboxDivEmoji.appendAttachment();
			
    });

	/**
	 * Append attachment button to chatbox tray emojione area
	 * 
	 * @return null
	 */
	$.fn.appendAttachment = function(){
		if($(this).next('div.emojionearea').length === 0){
			var el = this;
			setTimeout(function(){
				$(el).appendAttachment();
			},50);
		}else{
			$(this).next('div.emojionearea').append('<a href="javascript:;" class="emojionearea-button xwb-attach-btn" onclick="uploadAttachment(this)"><i class="fa fa-paperclip"></i></a>');
		}
	};


	/**
	 * Append userTo hidden field to chatbox when open
	 * 
	 * @param  {Number} v User ID
	 * @return Null
	 */
	$.fn.appendUsertoCB = function(v){
		
		if($(this).find('div.message-input').length === 0){
			var el = this;
			setTimeout(function(){
				$(el).appendUsertoCB(v);
			},50);
		}else{
			$(this).find('div.message-input').append('<input type="hidden" name="user_to[]" class="user_to" id="user_to" value="'+v+'" />');
		}
	};

	/**
	 * Append userTo hidden field to main message box
	 * 
	 * @param  {Number} v [User ID]
	 * @return {Null}
	 */
	$.fn.appendUsertoMain = function(v){
		if($(this).find('div.msg-input').length === 0){
			var el = this;
			setTimeout(function(){
				$(el).appendUsertoMain(v);
			},50);
		}else{
			$(this).find('div.msg-input').append('<input type="hidden" name="user_to[]" class="user_to" id="user_to" value="'+v+'" />');
		}
	};



	/**
	 * Mark message as read when clicked on the message container
	 * 
	 * @return null
	 */
	$(document).on('click',".xwb-message-container, .xwb-cb-chat .cb-conversation-container",function(){
		var cn_id, users;
		cn_id = $(this).attr('data-cnid');
		users = $(this).attr('data-users');
		markConversationRead(cn_id,users);

	});


/*end chatbox functions*/

});



/**
 * Ask to leave the page when there are unsend attachment that need to send 
 * 
 * @return string
 */	
$(window).on('beforeunload', function() {
	if(csDropzone != null){
		if(csDropzone.files.length>0){
			return "You have pending attachment that you need to send, Are you sure you want to exit?";
		}
	}
}); 


/**
 * Process remove all uploaded files
 * 
 * @return void
 */
$(window).on('unload', function() {
  if(csDropzone != null){
		if(csDropzone.files.length>0){
			csDropzone.removeAllFiles();
		}
	}
});

}( jQuery ));
/* End jQuery functions */
/* =============================================================================== */



/* =============================================================================== */
/* Start JavaScript functions */
var $ = jQuery.noConflict(); // no conflict
var $csrf, csrf_name, csrf_key;
/**
* Get the messages and append to message container
*
* @param array args 
* @param string prepend_message 
* @return mixed
*/
function get_messages(args, prepend_message){
var users_to, ci, conversation_name, mid, scroll_to;
prepend_message = prepend_message || false; // default value for IE compatibility
ci = "";
if(args.ci!=undefined)
	ci = args.ci;

var postData = {
		'user_to': args.user_to,
		'ci': ci,
		'start_from': args.start_from,
		'is_recieved': is_recieved,
		'cs_key' : 'xwb_show_message'
	};

$csrf = $('.csrf_key');
csrf_name = $csrf.attr('name');
csrf_key = $csrf.val();
postData[csrf_name] = csrf_key;
$.ajax({
	url: window.location.pathname,
	type: "post",
	data: postData,
	success: function(data){
		data = $.parseJSON(data);
		users_to = data.user_to;

		$('input[name="'+csrf_name+'"]').val(data.csrf_key);

		$(args.elContainer).attr('data-users', users_to.join('|'));
		$(args.elContainer).attr('data-cnid', ci);

		$('.user_to',args.elContainer).remove();

		/* add hidden inputs for the recipient */
		$.each(users_to,function(i,v){
			$('.msg-input',args.elContainer).append('<input type="hidden" name="user_to[]" class="user_to" id="user_to" value="'+v+'" />');
			$('.chatbox',args.elContainer).append('<input type="hidden" name="user_to[]" class="user_to" id="user_to" value="'+v+'" />');
		});

		$('.user_id',args.elContainer).val(data.user_id); // set the sender
		
		conversation_name = data.conversation_name;
		if(ci) // if group conversation, add onclick event in the conversation title to be able the user can manage it
			conversation_name ='<a href="javascript:;" onClick="conversationOption('+ci+')"><i class="fa fa-cog"></i> '+data.conversation_name+'</a>';

		$(".conversation-name",args.elContainer).html(conversation_name);

		if(prepend_message != true){ //this will prepend single message
			$(".message-inner",args.elContainer).html(data.row);
			$(".message-inner",args.elContainer).scrollTop($(".message-inner",args.elContainer)[0].scrollHeight);

			if(is_recieved==0){
				if(data.ci!=''){
					$("li[data-ci='"+data.ci+"']").find('span.unread').remove();
				}else{
					$("li[data-user='"+users_to[0]+"']").find('span.unread').remove();
				}

			}
		} else { // This will load all the messages
			$(".loading_container",args.elContainer).remove();
			mid = $(".message-inner div.message-row",args.elContainer).data('mid');

			$(".message-inner",args.elContainer).prepend(data.row);

			scroll_to = $(".message-inner",args.elContainer).find("[data-mid='" + mid + "']").offset().top;

			$(".message-inner",args.elContainer).scrollTop(scroll_to);
		}

		updateGroupConversation(data);

		$("a[rel^='prettyPhoto']").prettyPhoto({
			deeplinking:false,
			social_tools:false
		});
		is_recieved = 0;
	},
	error: function(XMLHttpRequest, textStatus, errorThrown) {}
});
}

/**
* Get the messages for adminside and append to message container
*
* @param array args 
* @param string prepend_message 
* @return mixed
*/
function get_messages_from_adminside(args, prepend_message){
var users_to, ci, conversation_name, mid, scroll_to, banner, sponsered_by_logo, banner_link;
prepend_message = prepend_message || false; // default value for IE compatibility
ci = "";
if(args.ci!=undefined)
	ci = args.ci;

var postData = {
		'user_to': args.user_to,
		'ci': ci,
		'start_from': args.start_from,
		'is_recieved': is_recieved,
		'cs_key' : 'xwb_show_message_from_adminside'
	};

$csrf = $('.csrf_key');
csrf_name = $csrf.attr('name');
csrf_key = $csrf.val();
postData[csrf_name] = csrf_key;
$.ajax({
	url: window.location.pathname,
	type: "post",
	data: postData,
	success: function(data){
		data = $.parseJSON(data);
		users_to = data.user_to;

		$('input[name="'+csrf_name+'"]').val(data.csrf_key);

		$(args.elContainer).attr('data-users', users_to.join('|'));
		$(args.elContainer).attr('data-cnid', ci);

		$('.user_to',args.elContainer).remove();
		$('.banner_on_box').html('<a target="_blank" href="'+data.banner_link+'"><img src="'+data.banner+'" style="max-width:100%; width:100%; height:auto;"></a>');
		$('.sponsered_by_logo').html('<a href="'+data.sponsered_by_logo_link+'" target="_blank"><img src="'+data.sponsered_by_logo+'" style=""></a>');

		/* add hidden inputs for the recipient */
		$.each(users_to,function(i,v){
			$('.msg-input',args.elContainer).append('<input type="hidden" name="user_to[]" class="user_to" id="user_to" value="'+v+'" />');
			$('.chatbox',args.elContainer).append('<input type="hidden" name="user_to[]" class="user_to" id="user_to" value="'+v+'" />');
		});

		$('.user_id',args.elContainer).val(data.user_id); // set the sender
		
		conversation_name = data.conversation_name;
		if(ci) // if group conversation, add onclick event in the conversation title to be able the user can manage it
			conversation_name ='<a href="javascript:;" onClick="conversationOption('+ci+')"><i class="fa fa-cog"></i> '+data.conversation_name+'</a>';

		$(".conversation-name",args.elContainer).html(conversation_name);

		if(prepend_message != true){ //this will prepend single message
			$(".message-inner",args.elContainer).html(data.row);
			$(".message-inner",args.elContainer).scrollTop($(".message-inner",args.elContainer)[0].scrollHeight);

			if(is_recieved==0){
				if(data.ci!=''){
					$("li[data-ci='"+data.ci+"']").find('span.unread').remove();
				}else{
					$("li[data-user='"+users_to[0]+"']").find('span.unread').remove();
				}

			}
		}else{ // This will load all the messages
			$(".loading_container",args.elContainer).remove();
			mid = $(".message-inner div.message-row",args.elContainer).data('mid');

			$(".message-inner",args.elContainer).prepend(data.row);

			scroll_to = $(".message-inner",args.elContainer).find("[data-mid='" + mid + "']").offset().top;

			$(".message-inner",args.elContainer).scrollTop(scroll_to);
		}

		updateGroupConversation(data);

		$("a[rel^='prettyPhoto']").prettyPhoto({
			deeplinking:false,
			social_tools:false
		});
		is_recieved = 0;
	},
	error: function(XMLHttpRequest, textStatus, errorThrown) {}
});
}

/**
* Get the messages and append to message container
*
* @param array args 
* @param string prepend_message 
* @return mixed
*/
function get_friends_messages(args, prepend_message){
var users_to, ci, conversation_name, mid, scroll_to, banner, sponsered_by_logo, youtube_link, banner_link;
prepend_message = prepend_message || false; // default value for IE compatibility
ci = "";
if(args.ci!=undefined)
	ci = args.ci;
var postData = {
		'user_to': args.user_to,
		'ci': ci,
		'start_from': args.start_from,
		'is_recieved': is_recieved,
		'cs_key' : 'xwb_friends_message'
	};
$csrf = $('.csrf_key');
csrf_name = $csrf.attr('name');
csrf_key = $csrf.val();
postData[csrf_name] = csrf_key;
$.ajax({
	url: window.location.pathname,
	type: "post",
	data: postData,
	success: function(data){
		data = $.parseJSON(data);
		users_to = data.user_to;
		$('input[name="'+csrf_name+'"]').val(data.csrf_key);
		$(args.elContainer).attr('data-users', users_to.join('|'));
		$(args.elContainer).attr('data-cnid', ci);
		$('.user_to',args.elContainer).remove();

		var arr = {
			'banner' : data.banner,
			'banner_link' : data.banner_link,
			'sponsered_by_logo_link' : data.sponsered_by_logo_link,
			'sponsered_by_logo' : data.sponsered_by_logo,
			'upload_ten_sec_intro':data.upload_ten_sec_intro,
			'youtube_link':data.youtube_link,
			'autoskipp_time':data.autoskipp_time,
			'upload_commercial':data.upload_commercial,
		}
		adsmgt(arr);

		// $('#customer_services_msgbox .banner_on_box').html('<a target="_blank" href="'+data.banner_link+'"><img src="'+data.banner+'" style="max-width:100%; width:100%; height:auto;"></a>');
		// $('.sponsered_by_logo').html('<a target="_blank" href="'+data.sponsered_by_logo_link+'"><img src="'+data.sponsered_by_logo+'" style=""></a>');

		// $('#customer_services_msgbox #vid1').attr('src',data.upload_ten_sec_intro);
		// if(data.youtube_link != '') {
		// 	$('#customer_services_msgbox video#vid2').remove();
		// 	$('#customer_services_msgbox .video_box').append('<iframe id="vid2" src="" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>');
		// 	$('#customer_services_msgbox #vid1').get(0).play();
		// 	$('#customer_services_msgbox #vid2').hide();
			
		// 	if(data.autoskipp_time != '') {
		// 		$('#customer_services_msgbox #vid1').on('ended',function() { 
		// 			$('#customer_services_msgbox #vid1').hide('normal').load().get(0).pause();
		// 			$('#customer_services_msgbox iframe#vid2').attr("src",data.youtube_link+"?rel=0&mute=1&autoplay=1&enablejsapi=1&start=0&showinfo=0&loop=0");
		// 			$('#customer_services_msgbox #vid2').show();
		// 			setTimeout(function () {
		// 				$('#customer_services_msgbox .skipvidtrigger').show();
		// 			}, 15000);
		// 			$("#customer_services_msgbox .popup-content-wrapper_customer_fontside").hide();
		// 			setTimeout(function () {
		// 				$('#customer_services_msgbox .skipvidtrigger, #customer_services_msgbox #vid1, #customer_services_msgbox #vid2, #customer_services_msgbox .video_box').remove();
		// 				$("#customer_services_msgbox .popup-content-wrapper_customer_fontside").show("normal");
		// 			}, data.autoskipp_time+'000');				
		// 		});
		// 	}
		// } else {
		// 	$('#customer_services_msgbox #vid1').get(0).play();
		// 	$('#customer_services_msgbox #vid2').hide();
		// 	if(data.autoskipp_time != '') {
		// 		$('#customer_services_msgbox #vid1').on('ended',function() { 
		// 			$('#customer_services_msgbox #vid1').load().hide('normal').get(0).pause();
		// 			$('#customer_services_msgbox video.commercial_video').attr('src',data.upload_commercial).get(0).play();
		// 			$('#customer_services_msgbox #vid2').show();
		// 			 setTimeout(function () {
		// 			 	$('#customer_services_msgbox .skipvidtrigger').show();
		// 			 }, 15000);
		// 			$("#customer_services_msgbox .popup-content-wrapper_customer_fontside").hide();
		// 			setTimeout(function () {
		// 				$('#customer_services_msgbox .skipvidtrigger, #customer_services_msgbox #vid1, #customer_services_msgbox #vid2, #customer_services_msgbox .video_box').remove();
		// 				$("#customer_services_msgbox .popup-content-wrapper_customer_fontside").show("normal");
		// 			}, data.autoskipp_time+'000');
		// 		});
		// 	}
		// }


		// var $this = $(this);
		// if (($('#customer_services_msgbox.modal.fade.in').find('video').attr('autoplay') === 'autoplay')) {
 //      		$('#customer_services_msgbox.modal.fade.in').find('video').get(0).play();
 //    		}
		
		// $('#customer_services_msgbox .skipvidtrigger').hide();

		// $('#customer_services_msgbox #vid2').on('ended',function(){
		//  	$(this).remove();
		//  	$("#customer_services_msgbox .popup-content-wrapper_customer_fontside").show("normal");
		//  	$('#customer_services_msgbox .skipvidtrigger').remove();
		// });

		// $('#customer_services_msgbox .skipvidtrigger').click(function(e){
		// 	$(this).remove();
		// 	$('#customer_services_msgbox #vid2, #customer_services_msgbox #vid1, #customer_services_msgbox .video_box').remove();
		// 	$("#customer_services_msgbox .popup-content-wrapper_customer_fontside").show("normal");
		// });
		

		/* add hidden inputs for the recipient */
		$.each(users_to,function(i,v){
			$('.msg-input',args.elContainer).append('<input type="hidden" name="user_to[]" class="user_to" id="user_to" value="'+v+'" />');
			$('.chatbox',args.elContainer).append('<input type="hidden" name="user_to[]" class="user_to" id="user_to" value="'+v+'" />');
		});
		$('.user_id',args.elContainer).val(data.user_id); // set the sender
		conversation_name = data.conversation_name;

		if(ci) // if group conversation, add onclick event in the conversation title to be able the user can manage it
		conversation_name ='<a href="javascript:;" onClick="conversationOption('+ci+')"><i class="fa fa-cog"></i> '+data.conversation_name+'</a>';
		
		$(".conversation-name",args.elContainer).html(conversation_name);
		if(prepend_message != true){ //this will prepend single message
			$(".message-inner",args.elContainer).html(data.row);
			$(".message-inner",args.elContainer).scrollTop($(".message-inner",args.elContainer)[0].scrollHeight);
			if(is_recieved==0){
				if(data.ci!=''){
					$("li[data-ci='"+data.ci+"']").find('span.unread').remove();
				}else{
					$("li[data-user='"+users_to[0]+"']").find('span.unread').remove();
				}

			}
		}else{ // This will load all the messages
			$(".loading_container",args.elContainer).remove();
			mid = $(".message-inner div.message-row",args.elContainer).data('mid');

			$(".message-inner",args.elContainer).prepend(data.row);

			scroll_to = $(".message-inner",args.elContainer).find("[data-mid='" + mid + "']").offset().top;

			$(".message-inner",args.elContainer).scrollTop(scroll_to);
		}

		updateGroupConversation(data);

		$("a[rel^='prettyPhoto']").prettyPhoto({
			deeplinking:false,
			social_tools:false
		});
		is_recieved = 0;
	},
	error: function(XMLHttpRequest, textStatus, errorThrown) {}
});
}




/**
* Get the messages and append to message container
*
* @param array args 
* @param string prepend_message 
* @return mixed
*/
function get_admin_messages(args, prepend_message){
var users_to, ci, conversation_name, mid, scroll_to, banner, sponsered_by_logo, youtube_link, banner_link;
prepend_message = prepend_message || false; // default value for IE compatibility
ci = "";
if(args.ci!=undefined)
	ci = args.ci;
var postData = {
		'user_to': args.user_to,
		'ci': ci,
		'start_from': args.start_from,
		'is_recieved': is_recieved,
		'cs_key' : 'xwb_admin_show_message'
	};


$csrf = $('.csrf_key');
csrf_name = $csrf.attr('name');
csrf_key = $csrf.val();
postData[csrf_name] = csrf_key;
$.ajax({
	url: window.location.pathname,
	type: "post",
	data: postData,
	success: function(data){
		data = $.parseJSON(data);
		users_to = data.user_to;
		$('input[name="'+csrf_name+'"]').val(data.csrf_key);
		$(args.elContainer).attr('data-users', users_to.join('|'));
		$(args.elContainer).attr('data-cnid', ci);
		$('.user_to',args.elContainer).remove();

		var arr = {
			'banner' : data.banner,
			'banner_link' : data.banner_link,
			'sponsered_by_logo_link' : data.sponsered_by_logo_link,
			'sponsered_by_logo' : data.sponsered_by_logo,
			'upload_ten_sec_intro':data.upload_ten_sec_intro,
			'youtube_link':data.youtube_link,
			'autoskipp_time':data.autoskipp_time,
			'upload_commercial':data.upload_commercial,
		}
		adsmgt(arr);
		

		/* add hidden inputs for the recipient */
		$.each(users_to,function(i,v){
			$('.msg-input',args.elContainer).append('<input type="hidden" name="user_to[]" class="user_to" id="user_to" value="'+v+'" />');
			$('.chatbox',args.elContainer).append('<input type="hidden" name="user_to[]" class="user_to" id="user_to" value="'+v+'" />');
		});
		$('.user_id',args.elContainer).val(data.user_id); // set the sender
		conversation_name = data.conversation_name;

		if(ci) // if group conversation, add onclick event in the conversation title to be able the user can manage it
		conversation_name ='<a href="javascript:;" onClick="conversationOption('+ci+')"><i class="fa fa-cog"></i> '+data.conversation_name+'</a>';
		
		$(".conversation-name",args.elContainer).html(conversation_name);
		if(prepend_message != true){ //this will prepend single message
			$(".message-inner",args.elContainer).html(data.row);
			$(".message-inner",args.elContainer).scrollTop($(".message-inner",args.elContainer)[0].scrollHeight);
			if(is_recieved==0){
				if(data.ci!=''){
					$("li[data-ci='"+data.ci+"']").find('span.unread').remove();
				}else{
					$("li[data-user='"+users_to[0]+"']").find('span.unread').remove();
				}

			}
		}else{ // This will load all the messages
			$(".loading_container",args.elContainer).remove();
			mid = $(".message-inner div.message-row",args.elContainer).data('mid');

			$(".message-inner",args.elContainer).prepend(data.row);

			scroll_to = $(".message-inner",args.elContainer).find("[data-mid='" + mid + "']").offset().top;

			$(".message-inner",args.elContainer).scrollTop(scroll_to);
		}

		updateGroupConversation(data);

		$("a[rel^='prettyPhoto']").prettyPhoto({
			deeplinking:false,
			social_tools:false
		});
		is_recieved = 0;
	},
	error: function(XMLHttpRequest, textStatus, errorThrown) {}
});
}



/**
* Get the PVP chat messages and append to message container
*
* @param array args 
* @param string prepend_message 
* @return mixed
*/
function get_pvp_chat_messages_frontside(args, prepend_message){

var users_to, ci, conversation_name, mid, scroll_to, banner, sponsered_by_logo, upload_ten_sec_intro, upload_commercial, youtube_link, banner_link;
prepend_message = prepend_message || false; // default value for IE compatibility
ci = "";
if(args.ci!=undefined)
	ci = args.ci;
var postData = {
	'userto': args.userto,
	'challengerid': args.challengerid,
	'accepterid': args.accepterid,
	'agentonline': args.agentonline,
	'groupdata': args.groupdata,
	'user_to': args.user_to,
	'ci': ci,
	'start_from': args.start_from,
	'is_recieved': is_recieved,
	'cs_key' : 'xwb_pvp_show_message'
};

$csrf = $('.csrf_key');
csrf_name = $csrf.attr('name');
csrf_key = $csrf.val();
postData[csrf_name] = csrf_key;
$.ajax({
	url: window.location.pathname,
	type: "post",
	data: postData,
	success: function(data){
		data = $.parseJSON(data);
		users_to = data.user_to;
		$('input[name="'+csrf_name+'"]').val(data.csrf_key);
		$(args.elContainer).attr('data-users', users_to.join('|'));
		$(args.elContainer).attr('data-cnid', ci);
		$('.user_to',args.elContainer).remove();

		// $('.banner_on_box').html('<a target="_blank" href="'+data.banner_link+'"><img src="'+data.banner+'" style="max-width:100%; width:100%; height:auto;"></a>');
		// $('.sponsered_by_logo').html('<a href="'+data.sponsered_by_logo_link+'" target="_blank"><img src="'+data.sponsered_by_logo+'" style=""></a>');
	
		$.each(users_to,function(i,v){
			$('.pvp_player_chat_msg-input',args.elContainer).append('<input type="hidden" name="user_to[]" class="user_to" id="user_to" value="'+v+'" />');
			$('.chatbox',args.elContainer).append('<input type="hidden" name="user_to[]" class="user_to" id="user_to" value="'+v+'" />');
		});

		if(data.admin_request == '') {
			$('#call-admin-for-help').remove();
		} else {
			$('#call-admin-for-help').html(data.admin_request);
		}
		var arr = {
			'banner' : data.banner,
			'banner_link' : data.banner_link,
			'sponsered_by_logo_link' : data.sponsered_by_logo_link,
			'sponsered_by_logo' : data.sponsered_by_logo,
			'upload_ten_sec_intro':data.upload_ten_sec_intro,
			'youtube_link':data.youtube_link,
			'autoskipp_time':data.autoskipp_time,
			'upload_commercial':data.upload_commercial,
		}
		adsmgt(arr);

		// $('.pvp_chatbox_main_row #vid1').attr('src',data.upload_ten_sec_intro);
		// if(data.youtube_link != '') {
		// 	$('.pvp_chatbox_main_row video#vid2').remove();
		// 	$('.pvp_chatbox_main_row .video_box').append('<iframe class="youtubeiframe" id="vid2" src="" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>');
		// 	$('.pvp_chatbox_main_row #vid1').get(0).play();
		// 	$('.pvp_chatbox_main_row iframe.youtubeiframe').hide();
		// 	if(data.autoskipp_time != '') {
		// 		$('.pvp_chatbox_main_row #vid1').on('ended',function(){ 
		// 			$('.pvp_chatbox_main_row iframe.youtubeiframe').attr("src",data.youtube_link+"?rel=0&mute=1&autoplay=1&enablejsapi=1&start=0&showinfo=0&loop=0");
		// 			$('.pvp_chatbox_main_row iframe.youtubeiframe').show();
		// 			 setTimeout(function () {
		// 			 	$('.pvp_chatbox_main_row .skipvidtrigger').show();
		// 			 }, 15000);
		// 			// $('.skipvidtrigger').show(15000);
		// 			$(".pvp_chatbox_main_row .popup-content-wrapper").hide();
		// 			$('.pvp_chatbox_main_row #vid1').hide('normal');
		// 			setTimeout(function () {
		// 				$('.pvp_chatbox_main_row .skipvidtrigger').remove();
		// 				$('#vid1').remove();
		// 				$('.pvp_chatbox_main_row iframe.youtubeiframe').remove();
		// 				$('.pvp_chatbox_main_row .pvp_msgbox-message-container .video_box').remove();
		// 				$(".pvp_chatbox_main_row .popup-content-wrapper").show("normal");
		// 			}, data.autoskipp_time+'000');
		// 		});
		// 	}
		// } else  {
		// 	$('.pvp_chatbox_main_row #vid1').get(0).play();
		// 	$('.pvp_chatbox_main_row #vid2').hide();
		// 	if(data.autoskipp_time != '') {
		// 		$('.pvp_chatbox_main_row #vid1').on('ended',function(){ 
		// 			$('.pvp_chatbox_main_row video.pvp_commercial_video').attr('src',data.upload_commercial).get(0).play();
		// 			$('.pvp_chatbox_main_row .pvp_commercial_video#vid2').show();
		// 			setTimeout(function () {
 //    						$('.pvp_chatbox_main_row .skipvidtrigger').show();
	// 					}, 15000);
		// 			// $('.skipvidtrigger').show(15000);
		// 			$(".pvp_chatbox_main_row .popup-content-wrapper").hide();
		// 			$('.pvp_chatbox_main_row #vid1').hide('normal');
		// 			setTimeout(function () {
		// 				$('.pvp_chatbox_main_row .skipvidtrigger').remove();
		// 				$('.pvp_chatbox_main_row #vid1').remove();
		// 				$('.pvp_chatbox_main_row #vid2').remove();
		// 				$('.pvp_chatbox_main_row .pvp_msgbox-message-container .video_box').remove();
		// 				$(".pvp_chatbox_main_row .popup-content-wrapper").show("normal");
		// 			}, data.autoskipp_time+'000');
		// 		});
		// 	}
		// }

		// var $this = $(this);
		// if ($('.pvp_chatbox_main_row.modal.fade.in').find('video').attr('autoplay') === 'autoplay') {
 //      		$('.pvp_chatbox_main_row.modal.fade.in').find('video').get(0).play();
 //    		}
	
		// $('.pvp_chatbox_main_row .skipvidtrigger').hide();

		// $('.pvp_chatbox_main_row #vid2').on('ended',function(){
		//  	$(this).hide();
		//  	$(".pvp_chatbox_main_row .popup-content-wrapper").show("normal");
		//  	$('.pvp_chatbox_main_row .skipvidtrigger').hide();
		// });

		// $('.pvp_chatbox_main_row .skipvidtrigger').click(function(e){
		// 	$(this).remove();
		// 	$('.pvp_chatbox_main_row #vid1, .pvp_chatbox_main_row #vid2').remove();
		// 	$('.pvp_chatbox_main_row .pvp_msgbox-message-container .video_box').remove();
		// 	$(".pvp_chatbox_main_row .popup-content-wrapper").show("normal");
		// });
		

		/* add hidden inputs for the recipient */
	
		$('.user_id',args.elContainer).val(data.user_id); // set the sender
		conversation_name = data.conversation_name;
		
		if(ci) // if group conversation, add onclick event in the conversation title to be able the user can manage it
			$('#groupid').html(ci);
			conversation_name ='Group : <a data-groupid='+ci+'><i class="fa fa-cog"></i> '+data.conversation_name+'</a>';
		$(".conversation-name",args.elContainer).html(conversation_name);
		if(prepend_message != true){ //this will prepend single message
		$('.user_id',args.elContainer).val(data.user_id); // set the sender
		conversation_name = data.conversation_name;
			$(".message-inner",args.elContainer).html(data.row);
			$(".message-inner",args.elContainer).scrollTop($(".message-inner",args.elContainer)[0].scrollHeight);
			if(is_recieved==0){
				if(data.ci!=''){
					$("li[data-ci='"+data.ci+"']").find('span.unread').remove();
				}else{
					$("li[data-user='"+users_to[0]+"']").find('span.unread').remove();
				}

			}
		}else{ // This will load all the messages
			$(".loading_container",args.elContainer).remove();
			mid = $(".message-inner div.message-row",args.elContainer).data('mid');

			$(".message-inner",args.elContainer).prepend(data.row);

			scroll_to = $(".message-inner",args.elContainer).find("[data-mid='" + mid + "']").offset().top;

			$(".message-inner",args.elContainer).scrollTop(scroll_to);
		}
		updateGroupConversation(data);

		// $("a[rel^='prettyPhoto']").prettyPhoto({
		// 	deeplinking:false,
		// 	social_tools:false
		// });
		// is_recieved = 0;
	}
});
}



/**
* Get the PVP chat messages and append to message container
*
* @param array args 
* @param string prepend_message 
* @return mixed
*/
function get_pvp_chat_messages_adminside(args, prepend_message){
var ci, conversation_name, mid, scroll_to, toggle, banner, sponsered_by_logo, banner_link;
prepend_message = prepend_message || false; // default value for IE compatibility
ci = "";
if(args.ci!=undefined)
	ci = args.ci;
var postData = {
		'accepterid': args.accepterid,
		'challengerid': args.challengerid,
		'agentonline': args.agentonline,
		'gamename': args.gamename,
		'groupdata': args.groupdata,
		'user_to': args.user_to,
		'ci': ci,
		'start_from': args.start_from,
		'is_recieved': is_recieved,
		'cs_key' : 'xwb_pvp_show_pvp_message_adminside'
	};
$csrf = $('.csrf_key');
csrf_name = $csrf.attr('name');
csrf_key = $csrf.val();
postData[csrf_name] = csrf_key;
$.ajax({
	url: window.location.pathname,
	type: "post",
	data: postData,
	success: function(data){

		data = $.parseJSON(data);
		users_to = data.user_to;
		$('input[name="'+csrf_name+'"]').val(data.csrf_key);
		$(args.elContainer).attr('data-users', users_to.join('|'));
		$(args.elContainer).attr('data-cnid', ci);

		$('.user_to',args.elContainer).remove();
		$('.banner_on_box').html('<a target="_blank" href="'+data.banner_link+'"><img src="'+data.banner+'" style="max-width:100%; width:100%; height:auto;"></a>');
		$('.sponsered_by_logo').html('<a href="'+data.sponsered_by_logo_link+'" target="_blank"><img src="'+data.sponsered_by_logo+'" style=""></a>');
		/* add hidden inputs for the recipient */
		$.each(users_to,function(i,v){
			$('.msg-input',args.elContainer).append('<input type="hidden" name="user_to[]" class="user_to" id="user_to" value="'+v+'" />');
			$('.chatbox',args.elContainer).append('<input type="hidden" name="user_to[]" class="user_to" id="user_to" value="'+v+'" />');
		});

		$('.user_id',args.elContainer).val(data.user_id); // set the sender
		conversation_name = data.conversation_name;
		if(ci) // if group conversation, add onclick event in the conversation title to be able the user can manage it
			conversation_name ='Group : <a href="javascript:;" onClick="conversationOption('+ci+')"><i class="fa fa-cog"></i> '+data.conversation_name+'</a>';
		$(".conversation-name",args.elContainer).html(conversation_name);
		if(prepend_message != true){ //this will prepend single message
			$(".message-inner",args.elContainer).html(data.row);
			$(".message-inner",args.elContainer).scrollTop($(".message-inner",args.elContainer)[0].scrollHeight);
			if(is_recieved==0){
				if(data.ci!=''){
					$("li[data-ci='"+data.ci+"']").find('span.unread').remove();
				}else{
					$("li[data-user='"+users_to[0]+"']").find('span.unread').remove();
				}

			}
		}else{ // This will load all the messages
			$(".loading_container",args.elContainer).remove();
			mid = $(".message-inner div.message-row",args.elContainer).data('mid');

			$(".message-inner",args.elContainer).prepend(data.row);

			scroll_to = $(".message-inner",args.elContainer).find("[data-mid='" + mid + "']").offset().top;

			$(".message-inner",args.elContainer).scrollTop(scroll_to);
		}
		updateGroupConversation(data);

		// $("a[rel^='prettyPhoto']").prettyPhoto({
		// 	deeplinking:false,
		// 	social_tools:false
		// });
		// is_recieved = 0;
	}
});
}


/**
* Get the PVP chat messages and append to message container
*
* @param array args 
* @param string prepend_message 
* @return mixed
*/
function get_agent_in_group(args, prepend_message){

var ci, conversation_name, mid, scroll_to;
prepend_message = prepend_message || false; // default value for IE compatibility
ci = "";
if(args.ci!=undefined)
	ci = args.ci;
var postData = {
		'accepterid': args.accepterid,
		'challengerid': args.challengerid,
		'agentonline': args.agentonline,
		'gamename': args.gamename,
		'groupdata': args.groupdata,
		'user_to': args.user_to,
		'ci': ci,
		'start_from': args.start_from,
		'is_recieved': is_recieved,
		'cs_key' : 'xwb_get_agent_in_group'
	};
$csrf = $('.csrf_key');
csrf_name = $csrf.attr('name');
csrf_key = $csrf.val();
postData[csrf_name] = csrf_key;
$.ajax({
	url: window.location.pathname,
	type: "post",
	data: postData,
	success: function(data){
		data = $.parseJSON(data);

		// if(data.currentagentex_rows == '0')
		// {
		// 	// $('#pvp_msgbox_for_admin').hide();
		// 	// alert("Agent is already Helping There");
		// 	// return false;
		// }
		// if(data.currentagentex_rows == '1')
		// {				
			$('#pvp_msgbox_for_admin').fadeIn('normal');
			$('.close_model').click(function(e){
				$("#pvp_msgbox_for_admin").fadeOut('normal');
			});
			get_pvp_chat_messages_adminside(args);

		// }
	}
});
}
function get_lobby_join_group_data(args){
	var editorID, data, users, socket_data;
	var lobby_id = args.lobby_id;
	var group_bet_id = args.group_bet_id;
	var table_cat = args.table_cat;
	var price_add_decimal = args.price_add_decimal;
	var dragged_userid = '';
	var dragged_waitingid = '';
	if (args.dragged_userid != undefined && args.dragged_waitingid != undefined) {
		dragged_userid = args.dragged_userid;
		dragged_waitingid = args.dragged_waitingid;
	}
	var postData = {
		'lobby_id': lobby_id,
		'group_bet_id': group_bet_id,
		'table_cat': table_cat,
		'price_add_decimal': price_add_decimal,
		'dragged_userid': dragged_userid,
		'dragged_waitingid': dragged_waitingid
	};

	$.ajax({
		url: base_url +'livelobby/join_lobby_group',
		type: "post",
		data: postData,
		success: function(data){
		    var data = $.parseJSON(data);
		    $('#join_lobby').hide();
		    $('.join_wtng_btn').hide();
			if (data.balance == 'less' && data.session_user_id == data.join_user_id) {
		    	window.location.href = base_url+'points/buypoints';
		    	return false;
		    } else if (data.balance == 'less' && data.session_user_id != data.join_user_id) {
		    	alert("This Player do not have enough credits to place a challenge. Please Proceed to Sendmoney tab for more.");
		    	window.location.href = base_url+'sendmoney';
		    	return false;
		    } else if ((data.reached_limit == 'no' && data.membership == 'yes') || data.lobby_details.is_event == 1) {
		    	if (table_cat.length > 0) {
		    		var lobby_custom_name = $('input[name="lobby_custom_name"]').val();
		    		socket_data = {
						'lobby_id': lobby_custom_name,
						'group_bet_id': data.get_join_detail[0]['group_bet_id'],
						'table_cat': data.get_join_detail[0]['table_cat'],
						'user_name': data.get_join_detail[0]['name'],
						'user_id' : data.get_join_detail[0]['user_id'],
						'change_user_vice_versa' : (data.change_user_vice_versa != undefined && data.change_user_vice_versa == 'yes') ? 'yes' : 'no',
						'get_defaultstream' : (data.get_defaultstream != undefined && data.get_defaultstream == 1) ? 1 : 0,
						'on':'send_lobby_group_data'
					};
					socket.emit( 'sendrealtimedata', socket_data );
					return false;
		    	}
		    } else if (data.reached_limit == 'yes' && data.session_user_id == data.join_user_id && data.lobby_details.is_event == 0) {
		    	window.location.href = base_url+'dashboard';
		    } else if (data.membership == 'no' && data.session_user_id == data.join_user_id && data.lobby_details.is_event == 0) {
		    	window.location.href = base_url+'membership/getMembershipPlan';
		    } else if (data.reached_limit == 'yes' && data.session_user_id != data.join_user_id && data.lobby_details.is_event == 0) {
		    	alert("This Player Already Reached Limit of joining of A Lobby list.");
		    	location.reload();
		    	return false;
		    } else if (data.membership == 'no' && data.session_user_id != data.join_user_id && data.lobby_details.is_event == 0) {
		    	alert("This Player does't have Membership of joining of A Lobby list.");
		    	location.reload();
		    	return false;
		    }
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {}
	});
}
function senduser_to_waitinglist(args) {
	var socket_data;
	var lobby_id = args.lobby_id;
	var group_bet_id = args.group_bet_id;
	var table_cat = args.table_cat;
	var price_add_decimal = args.price_add_decimal;
	var user_id = args.user_id;
	var fan_tag = args.fan_tag;
	var postData = {
		'lobby_id': lobby_id,
		'group_bet_id': group_bet_id,
		'table_cat': table_cat,
		'price_add_decimal': price_add_decimal,
		'user_id': user_id,
		'fan_tag': fan_tag
	};
	$.ajax({
		url: base_url +'livelobby/senduser_to_waitinglist',
		type: "post",
		data: postData,
		success: function(srcData){
			var srcData = $.parseJSON(srcData);
		    var lobby_custom_name = $('input[name="lobby_custom_name"]').val();
		    var is_spectator = (srcData.get_ticket_detail.length > 0 && srcData.get_ticket_detail[0].lobby_is_spectator == 1)?'yes':'no';
		    var creator_in_waitinglist = (srcData.creator_in_waitinglist != undefined) ? srcData.creator_in_waitinglist : '';
			socket_data = {
		    	'lobby_id': lobby_custom_name,
		    	'user_id': user_id,
		    	'user_id_balance':srcData.user_id_balance,
		    	'table_cat':table_cat,
				'remaining_table':srcData.remaining_table,
				'full_table':srcData.full_table,
				'get_fan_detail':srcData.get_fan_detail,
				'is_full_play':srcData.is_full_play,
				'is_spectator':is_spectator,
				'creator_in_waitinglist': creator_in_waitinglist,
				'get_lobby_detail':srcData.get_lobby_detail,
				'on':'send_user_to_waitinglist',
			};
			socket.emit( 'sendrealtimedata', socket_data );
		}
	});
}
function send_leave_report(data){
	var socket_data;
	$.ajax({
		url : base_url +'livelobby/leave_lobby',
		data: data,
		type: 'POST',
    	success: function(res) {
      		var res = $.parseJSON(res);
	    	$('#leave_lobby').hide();
	    	if ($('#leave_lobby_modal_from_dash').length) {
	    		$('#leave_lobby_modal_from_dash').hide();
	    	}
	    	if ($('#lobbyliusr_'+data.lobby_id).length) {
	    		$('#lobbyliusr_'+data.lobby_id).remove();
	    	}
	    	var table_cat = '';
	    	if (res.lobby_leaved_users != null && res.lobby_leaved_users.table_cat != null) {
	    		table_cat = res.lobby_leaved_users.table_cat;
	    	}
	    	var lobby_custom_name = $('input[name="lobby_custom_name"]').val();
	      	socket_data = {
				'lobby_id': lobby_custom_name,
				'group_bet_id': data.group_bet_id,
				'user_id': res.user_id,
				'refresh': res.refresh,
				'leaved_total_balance': res.leaved_total_balance,
				'table_cat': table_cat,
				'on':'leave_lobby_data'
			};
			socket.emit( 'sendrealtimedata', socket_data );
    	}
  	});
}
function send_msg_delete($msg_id,$plyr_id){
	var msg_id = $msg_id;
	var plyr_id = $plyr_id;
	var lobby_id = $('input[name="lobby_id"]').val();
	if (msg_id != '') {
		var data = { 
	  	'msg_id':msg_id,
	  	'lobby_id':lobby_id,
	  	'plyr_id':plyr_id
	  };
		var socket_data;
		$.ajax({
			url : base_url +'livelobby/send_msg_delete',
			data: data,
			type: 'POST',
			success: function(res) {
				var table_cat;
				var res = $.parseJSON(res);
				var lobby_custom_name = $('input[name="lobby_custom_name"]').val();
				socket_data = {
		    	'lobby_id': lobby_custom_name,
		    	'id' : lobby_id,
		    	'msg_id': data.msg_id,
					'on':'delete_msg_from_lobby',
				};
				socket.emit( 'sendrealtimedata', socket_data );
			}
		}); 
	}
}
function del_usr_from_chat($plyr_id){
	var plyr_id = $plyr_id;
	var lobby_id = $('input[name="lobby_id"]').val();
	if (plyr_id != '') {
		var data = {
			'lobby_id':lobby_id,
			'plyr_id':plyr_id
		};
		var socket_data;
		$.ajax({
			url : base_url +'livelobby/del_usr_from_chat',
			data: data,
			type: 'POST',
			success: function(res) {
				var res = $.parseJSON(res);
				var lobby_custom_name = $('input[name="lobby_custom_name"]').val();
				socket_data = {
		    	'lobby_id': lobby_custom_name,
		    	'id' : lobby_id,
		    	'user_id': res.user_id,
		    	'on':'del_usr_from_looby_chat',
		    };
		    socket.emit( 'sendrealtimedata', socket_data );
		  }
		}); 
	}
}
function mute_usr_from_chat($plyr_id){
	var plyr_id = $plyr_id;
	var lobby_id = $('input[name="lobby_id"]').val();
	if (plyr_id != '') {
		var data = {
	  	'lobby_id':lobby_id,
	  	'plyr_id':plyr_id
	  };
	  var socket_data;
		$.ajax({
			url : base_url +'livelobby/mute_usr_from_chat',
			data: data,
			type: 'POST',
			success: function(res) {
		    var res = $.parseJSON(res);
		    var lobby_custom_name = $('input[name="lobby_custom_name"]').val();
		    socket_data = {
		    	'lobby_id': lobby_custom_name,
		    	'id' : lobby_id,
		    	'user_id': res.user_id,
					'mutediff': res.mutediff,
					'on':'mute_usr_from_looby_chat',
				};
				socket.emit( 'sendrealtimedata', socket_data );
			}
		}); 
	}
}
function leave_lobby(leave_id,lobby_id,group_bet_id,is_creator,is_controller,player_tag_name,self_user='',everyone_readup='',leave_from_waitinlist=''){
	var warning_msg, live_lobby_h4msg, autoloss, creator;
	live_lobby_h4msg = 'Are you sure you want to leave this Live Lobby?'
	warning_msg = '';
	autoloss = 'no';
	creator = 'no';
	controller = 'no';
	if (is_creator == 'yes' && self_user == 'yes') {
		warning_msg = 'WARNING MESSAGE';
		creator = 'yes';
		live_lobby_h4msg = 'Are you sure you want to close out this Live Lobby?';
		if (everyone_readup == 'yes' && leave_from_waitinlist == 'no') {
			controller = is_controller;
			live_lobby_h4msg = 'This is a Automatic Loss for your Entire Team &<br> will reward funds to Winning Team';
			autoloss = 'yes';
		} else if (everyone_readup == 'yes' && leave_from_waitinlist == 'yes') {
			live_lobby_h4msg = 'Are you sure you want to close out this Live Lobby? Penalty funds will be Deduct from your Account';
			autoloss = 'yes';
		}
	} else if (is_creator == 'no' && self_user == 'yes'){
		live_lobby_h4msg = 'Are you sure you want to Kick <span class="leave_lobbyplayer_tag">'+player_tag_name+'</span> ?';
		if (everyone_readup == 'yes' && leave_from_waitinlist == 'no') {
			autoloss = 'yes';
			if (is_controller=='yes') {
				warning_msg = 'WARNING MESSAGE';
				live_lobby_h4msg = 'This is a Automatic Loss for <span class="leave_lobbyplayer_tag">'+player_tag_name+'</span> Entire Team &<br> will reward funds to Winning Team';
				controller = is_controller;
			}
		} else if (leave_from_waitinlist == 'yes') {
			live_lobby_h4msg = 'Are you sure you want to Kick <span class="leave_lobbyplayer_tag">'+player_tag_name+'</span> from Waiting list?';
		}
	} else {
		if (everyone_readup == 'yes' && leave_from_waitinlist == 'no') {
			autoloss = 'yes';
			live_lobby_h4msg = 'This is a Automatic Loss for you & will reward funds to Winning Team?';
			if (is_controller=='yes') {
				warning_msg = 'WARNING MESSAGE';
				live_lobby_h4msg = 'This is a Automatic Loss for your Entire Team &<br> will reward funds to Winning Team';
				controller = is_controller;
			}
		} else if (leave_from_waitinlist == 'yes') {
			live_lobby_h4msg = 'Are you sure you want to leave from Waiting list?';
		}
	}
	$('#leave_lobby .modal-header h4, #leave_lobby_modal_from_dash .modal-header h4').html(live_lobby_h4msg);
	$('#leave_lobby .modal-header h1.warning_msg, #leave_lobby_modal_from_dash .modal-header h1.warning_msg').html(warning_msg);
	$('#leave_lobby .yes, #leave_lobby_modal_from_dash .yes').removeAttr('autoloss creator controller leave_id lobby_id group_bet_id leave_from_waitinlist');
	$('#leave_lobby .yes, #leave_lobby_modal_from_dash .yes').attr({'autoloss': autoloss, 'creator': creator, 'controller': controller, 'leave_id':leave_id, 'lobby_id':lobby_id, 'group_bet_id':group_bet_id, 'leave_from_waitinlist':leave_from_waitinlist});
	$('#leave_lobby, #leave_lobby_modal_from_dash').show();
}
function lobby_details_change(custom_lby,lobby_id,group_bet_id,system,gamename,subgamename,game_type,game_amount,lobby_password,password_option,game_image_id,for_18_plus){
	game_image_id.append('custom_lby',custom_lby);
	game_image_id.append('lobby_id',lobby_id);
	game_image_id.append('system',system);
	game_image_id.append('gamename',gamename);
	game_image_id.append('subgamename',subgamename);
	game_image_id.append('game_type',game_type);
	game_image_id.append('game_amount',game_amount);
	game_image_id.append('lobby_password',lobby_password);
	game_image_id.append('for_18_plus',for_18_plus);
	game_image_id.append('password_option',password_option);
	// game_image_id.append('description',description);
	game_image_id.append('group_bet_id',group_bet_id);
	var socket_data;
	xhr = new XMLHttpRequest();
  	xhr.open( 'POST', base_url +'livelobby/change_lobby_details', true );
  	xhr.responseType = 'json';
  	xhr.onreadystatechange = function (srcData) {
	  	var lobby_custom_name = $('input[name="lobby_custom_name"]').val();
	  	srcData = this.response;
	  	if (srcData !='' && srcData !=null) {
			socket_data = {
				'lobby_id': lobby_custom_name,
				'on':'change_lby_detail',
			};
			socket.emit('sendrealtimedata', socket_data );
			$('#change_lobby_details').hide();
			game_image_id.delete(lobby_id);
	  	}
  	};
  	xhr.send(game_image_id);
}
function subscribe_btn_append(streamer_name,img,user_to,payment_id){
	var user_id = $('input[name="user_id"]').val();
	var postdata = {
		'user_to':user_to,
		'user_from':user_id,
		'subscribe_status':1,
	}
	$.ajax({
    url : base_url+'subscribeuser/get_subscription_plans',
    data: postdata,
    type: 'POST',
    success: function(res) {
    	res = $.parseJSON(res);
    	$('.subscribe_error').html('');
    	var add_class_in_modal_data = "subscribe_main_div";
			var current_subscribption_plan_id = (res.subscription_plan != null) ? res.subscription_plan.subscribption_plan_id : '';
			var current_subscribption_payment_id = (res.subscription_plan != null) ? res.subscription_plan.transaction_id : '' ;
			var senddata = '<div class="col-sm-5"><div class="image" user_to="'+postdata.user_to+'"><img src="'+base_url+'upload/profile_img/'+img+'" class="img img-responsive"></div></div><div class="col-sm-7"><div class="subscribe-info text-left"><h3 class="substitle">Subscribe to '+streamer_name+'</h3>';
			for (var i = 0; i <= res.all_plan.length - 1; i++) {
				var title = res.all_plan[i].title;
				var amount = res.all_plan[i].amount;
				var plan_id = res.all_plan[i].id;
				senddata +='<div class="col-sm-4"><div class="subamount_div text-center '+title+'"><h5>'+title+'</h5><div class="subs_player-plan_div"><a class="plan_info plan_info_btn" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="View Plan info" plan_title="'+title+'" id="'+plan_id+'" plan_image="'+res.all_plan[i].image+'" plan_desc="'+res.all_plan[i].description+'" plan_termscond="'+res.all_plan[i].terms_condition+'">info</a></div><div class="amount"><span class="currency_sym">$</span>'+amount+'<span class="dura">/mon</span></div><div class="button">';
				senddata += plan_id == current_subscribption_plan_id ? '<div class="alrdy_subscribe subid_'+res.subscription_plan.user_to+'" payment_id="'+current_subscribption_payment_id+'" subsplan="'+current_subscribption_plan_id+'"> <i class="fa fa-check"></i> Subscribed</div>' : '<button type="submit" name="subscribe" class="btn" plan_id="'+plan_id+'" plan_name="'+title+'" value="'+plan_id+'">Subscribe Now</button>';
				senddata += '</div><div class="subscribeloader '+title+'"></div></div></div>';
    	}
			senddata += '</div>';
			senddata += '<div class="col-sm-12">* Amount will be deducted Every month</div>';
			senddata += '<div class="subscribe_error"></div>';
			var data = {
				'add_class_in_modal_data':add_class_in_modal_data,
				'senddata':senddata,
				'tag':'subscribe_button_click'
			}
			commonshow(data);
    }
  }); 
}
function subscribe_user_plan_view(data) {
	var plan_image = data.plan_image, plan_title = data.plan_title, plan_desc = data.plan_desc, plan_termscond = data.plan_termscond;
	var subscribe_plan_info = '<div class="subscbe_plan_detail">';
	subscribe_plan_info += (plan_image != undefined && plan_image != '') ? '<div class="col-sm-5"><div class="image" class="subscribe_plan_image"><img src="'+base_url+'upload/subscription/'+plan_image+'" class="img img-responsive"></div></div><div class="col-sm-7">' : '<div class="col-sm-12">';
	subscribe_plan_info += '<div class="subscribe_plan-info text-left"><h3 class="subs_plan_title">'+plan_title+'</h3><div class="subs_plan_desc">'+plan_desc+'</div><h3 class="subs_plan_title">Terms & Conditions</h3><div class="subs_plans_terms_cond">'+plan_termscond+'</div></div></div>';
	subscribe_plan_info += '<div class="col-sm-12"> <div class="clsbtn_area"> <a class="close_plan_detail"> <i class="fa fa-close"></i> </a> </div> </div>';
	var viewdata = {
		'title' : 'Plan Information',
		'tag':'view_subscription_plan_info',
		'subscribe_plan_info' : subscribe_plan_info
	}
	commonshow(viewdata);
}
function adsmgt(arr) {
$('.banner_on_box').html('<a target="_blank" href="'+arr.banner_link+'"><img src="'+arr.banner+'" style="max-width:100%; width:100%; height:auto;"></a>');
$('.sponsered_by_logo').html('<a target="_blank" href="'+arr.sponsered_by_logo_link+'"><img src="'+arr.sponsered_by_logo+'" style=""></a>');

$('.msg_bx_sec #vid1').attr('src',arr.upload_ten_sec_intro);
if(arr.youtube_link != '') {
	$('.msg_bx_sec video#vid2').remove();
	// $('.msg_bx_sec .video_box').append('<iframe id="vid2" src="'+arr.youtube_link+'?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>');
	$('.msg_bx_sec .video_box').append('<iframe id="vid2" src="" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>');
	$('.msg_bx_sec #vid1').get(0).play();
	$('.msg_bx_sec #vid2').hide();
	var vid2_inf = {
		'type' : 'youtube_link',
		'link' : arr.youtube_link
	}
} else {
	var vid2_inf = {
		'type' : 'video_link',
		'link' : arr.upload_commercial
	}
}
if(arr.autoskipp_time != '') {
	setTimeout(function () {
		if (vid2_inf.type == 'youtube_link') {
			$('.msg_bx_sec iframe#vid2').attr("src",vid2_inf.link+"?rel=0&mute=1&autoplay=1&enablejsapi=1&start=0&showinfo=0&loop=0");
		} else if (vid2_inf.type == 'video_link') {
			$('.msg_bx_sec video.commercial_video').attr('src',vid2_inf.link).get(0).play();
		}
		$('.msg_bx_sec #vid2').show();
		setTimeout(function () {
			$('.msg_bx_sec .skipvidtrigger').show();
		}, 15000);
		$(".msg_bx_sec .popup-content-wrapper_customer_fontside").hide();
		$('.msg_bx_sec #vid1').hide('normal').remove();
		setTimeout(function () {
			$('.msg_bx_sec .skipvidtrigger, .msg_bx_sec #vid1, .msg_bx_sec #vid2, .msg_bx_sec .video_box').remove();
			$(".msg_bx_sec .popup-content-wrapper_customer_fontside").show("normal");
		}, arr.autoskipp_time+'000');
	}, 10000);
}
var $this = $(this);
if ($('.msg_bx_sec.modal.fade.in').find('video').attr('autoplay') === 'autoplay') {
	$('.msg_bx_sec.modal.fade.in').find('video').get(0).play();
}
$('.msg_bx_sec .skipvidtrigger').hide();
$('.msg_bx_sec #vid2').on('ended',function(){
 	$(this).remove();
 	$(".msg_bx_sec .popup-content-wrapper_customer_fontside").show("normal");
 	$('.msg_bx_sec .skipvidtrigger').remove();
});
$('.msg_bx_sec .skipvidtrigger').click(function(e){
	$(this).remove();
	$('.msg_bx_sec #vid1, .msg_bx_sec #vid2, .msg_bx_sec .video_box').remove();
	$(".msg_bx_sec .popup-content-wrapper_customer_fontside").show("normal");
});
}

function make_lobby_event(data) {
	var lobby_id = data.lobby_id, group_bet_id = data.group_bet_id, check_event = data.check_event, event_old_img = data.event_old_img, check_spectator = data.check_spectator, event_price = data.event_price, spectate_price = data.spectate_price, lobby_custom_name = data.lobby_custom_name, event_title = data.event_title, event_description = data.event_description, event_key = data.event_key;
	var show_eve_div = (check_event == 1)?'style="display:block"':'style="display:none"';
	var show_spectator = (check_spectator == 1)?'style="display:block"':'style="display:none"';
	var event_checkbox = (check_event == 1)?'checked':'';
	var spectator_checkbox = (check_spectator == 1)?'checked':'';
	var event_required = (check_event == 1)?'required':'';
	var spectator_required = (check_spectator == 1)?'required':'';
	var event_old_img = data.event_old_img;
	var img_show = 'style="display:none;"';
	var choose_img_show = 'style="display:block;"';
	var event_img_req = 'required';
	var img_class = ' choose_active';

	var event_start_date = (data.event_start_date != undefined && data.event_start_date !='') ? data.event_start_date : '';
	var event_end_date = (data.event_end_date != undefined && data.event_end_date !='') ? data.event_end_date : '';
	if(event_old_img != '') {
		img_show = 'style="display:block;"';
		img_class = '';
		choose_img_show = 'style="display:none;"';
		event_img_req = '';
	}
	// event_default_date

	var senddata = '<div class="lobby_event_detail myprofile-edit"><form><div class="col-sm-12">';
		senddata += '<div class="form-group"><label class="col-lg-2 text-left"> Event </label><div class="input-group col-lg-10 text-left"><label class="switch change_lobby_event_label"><input type="hidden" name="lobby_custom_name" value="'+lobby_custom_name+'"><input type="checkbox" name="change_lobby_event" id="change_lobby_event" target_div="event_edit_detail" '+event_checkbox+'><span class="slider round"></span></label></div></div>';
		senddata += '<div class="form-group event_edit_detail" '+show_eve_div+'><label class="col-lg-2 text-left"> Price </label><div class="input-group col-lg-10"><input type="number" name="lobby_event_amount" class="form-control " '+event_required+' value="'+event_price+'" placeholder="Enter event price"></div></div>';
		senddata += '<div class="form-group event_edit_detail" '+show_eve_div+'><label class="col-lg-2 text-left"> Image </label><div class="input-group col-lg-10 text-left">';
		senddata += '<div id="game_img" '+img_show+'><img class="img-cls selected" id="img" style="margin-right: 31px;" src="'+base_url+'upload/banner/'+event_old_img+'" width="100" height="80"><span class="change_event_img btn cmn_btn" style="display:inline-block;"> Change image</span></div><div class="file-upload choose_eventdiv '+img_class+'" '+choose_img_show+'><div class="file-select"><div class="file-select-button" id="fileName">Choose File</div><div class="file-select-name" id="noFile">No file chosen...</div> <input type="file" name="event_image" class="event_image_upload" id="chooseGameimage" accept="image/*"></div><div class="image_error"> (Recommended Website Banner Size 1920x * 678x) </div></div></div></div>';
		senddata += '<div class="form-group event_edit_detail" '+show_eve_div+'><label class="col-lg-2 text-left"> Title </label><div class="input-group col-lg-10"><input type="text" name="event_title" class="form-control " '+event_required+' placeholder="Enter event title" value="'+event_title+'"></div></div>';
		senddata += '<div class="form-group event_edit_detail" '+show_eve_div+'><label class="col-lg-2 text-left"> Description </label><div class="input-group col-lg-10"><textarea name="event_description" class="form-control " '+event_required+' placeholder="Enter event description">'+event_description+'</textarea></div></div>';
		senddata += '<div class="form-group event_edit_detail col-sm-6" '+show_eve_div+'><label class="col-lg-4 text-left"> Start Date </label><div class="input-group col-lg-8"><input type="text" name="startdate" class="form-control custom_datetimepicker" '+event_required+' placeholder="Event start date" value="'+event_start_date+'"></div></div>';
		senddata += '<div class="form-group event_edit_detail col-sm-6" '+show_eve_div+'><label class="col-lg-4 text-left"> End Date </label><div class="input-group col-lg-8"><input type="text" name="enddate" class="form-control custom_datetimepicker" '+event_required+' placeholder="Event end date" value="'+event_end_date+'"></div></div>';
		senddata += '<div class="form-group"><div class="input-group col-lg-12"><hr class="devide_hr"></div></div>';
		senddata += '<div class="form-group"><label class="col-lg-2 text-left"> Spectator </label><div class="input-group col-lg-10 text-left"><label class="switch change_lobby_spectator_label"><input type="checkbox" name="change_lobby_spector" id="change_lobby_spector" target_div="spectator_edit_detail" '+spectator_checkbox+'><span class="slider round"></span></label></div></div>';
		senddata += '<div class="form-group spectator_edit_detail" '+show_spectator+'><label class="col-lg-2 text-left"> Price </label><div class="input-group col-lg-10"><input type="number" name="lobby_spector_amount" class="form-control " '+spectator_required+' value="'+spectate_price+'"></div></div>';
		senddata += '<div class="form-group"><div class="col-lg-12"><input type="hidden" name="event_key" value="'+event_key+'"><input type="hidden" name="event_old_img" value="'+event_old_img+'"><input type="hidden" name="lobby_id" value="'+lobby_id+'"><input type="button" class="cmn_btn" name="submit" value="Submit"></div></div>';
		senddata += '</div></form></div>';

	var viewdata = {
		'modal_title' : 'Make Lobby Event',
		'senddata' : senddata,
		'add_class_in_modal':'lobby_event_modal',
		'tag':'view_make_event'
	}
	commonshow(viewdata);
}
function show_membeership_purchase(data) {
	var senddata = '<h3 class="text-justify">To Make an Event Click to Proceed to buy Membership Plan</h3><h3><a href="'+base_url+'membership/getMembershipPlan" class="button yes">Proceed</a><a class="button no">Cancel</a></h3>';
	var viewdata = {
		'senddata' : senddata,
		'add_class_in_modal':'proceed_to_purchase_membership',
		'tag':'proceed_to_purchase_membership'
	}
	commonshow(viewdata);
}
function membership_btn_append(data){
	var tournament_ticket_discount = 0;
	if (data.tournament_ticket_discount_type == 1 && data.tournament_ticket_discount_amount != 0) {
		tournament_ticket_discount = data.tournament_ticket_discount_amount+' %';
	} else if (data.tournament_ticket_discount_type == 2 && data.tournament_ticket_discount_amount != 0) {
		tournament_ticket_discount = '$ '+data.tournament_ticket_discount_amount.toFixed(2);
	} else if (data.tournament_ticket_discount_type == 3) {
		tournament_ticket_discount = 'FREE';
	}
	var senddata = '<div class="col-sm-5">';
	senddata += '<div class="image"><img src="'+base_url+'upload/membership/'+data.image+'" class="img img-responsive"></div>';
	senddata += '<div class="lobby_feature text-left"><h3>Features</h3><h4 class="text-left feature"> Lobby Creation limit : '+data.lobby_creation_limit+'</h4>';
	senddata += '<h4 class="text-left feature"> Lobby Joining limit : '+data.lobby_joining_limit+'</h4>';
	(tournament_ticket_discount != 0) ? senddata += '<h4 class="text-left feature"> Tournament Ticket discount : <b>'+tournament_ticket_discount+'</b></h4>' : '';
	senddata += '<h4 class="text-left feature"> Stream Record limit : '+data.stream_record_limit+'</h4></div>';
	senddata += '</div><div class="col-sm-7">';
	senddata += '<div class="subscribe-info text-left">';
	senddata += '<h3 class="membership_title">'+data.title+'</h3>';
	senddata += '<p class="text-left membership_normal_text activeBids">'+data.description+'</p>';
	senddata += '<h4 class="membership_title">Terms & Conditions </h4><p class="text-left membership_normal_text">'+data.terms_condition+'</p></div></div>';

	var add_class_in_modal_data = "membership_main_div";
	var data = {
		'add_class_in_modal_data':add_class_in_modal_data,
		'senddata': senddata,
		'tag':'membership_button_click'
	}
	commonshow(data);
}
function commonshow(data){
	var commonmodal_calss = 'modal custmodal commonmodal ';
	if(data.tag == 'update_event_ticket'){	
		$('.modal.commonmodal').removeClass().addClass('modal custmodal commonmodal update_event_ticket_mdl');
		$('.modal_h1_msg').addClass('update_event_ticket').html(data.modal_title);
		$('.modal.commonmodal .update_event_ticket').next('hr').remove();
		$('.modal.commonmodal .modal_data').addClass(data.add_class_in_modal_data).html(data.senddata);
		$('.modal.commonmodal').show();	
	}
	if (data.tag == 'view_order_item') {
		$('.modal_h1_msg').html(data.modal_title);
		$('.modal.commonmodal .modal_data').html(data.senddata);
		$('.modal.commonmodal').removeClass().addClass('modal custmodal commonmodal '+data.add_class_in_modal).show();
		return false;
	}
	if (data.tag == 'cancel_order') {
		$('.modal.commonmodal hr').hide();
		$('.modal_h1_msg').html(data.modal_title);
		$('.modal.commonmodal .modal_data').html(data.senddata);
		$('.modal.commonmodal').removeAttr('class').addClass(commonmodal_calss+data.add_class_in_modal_data).show();
	}
	if (data.tag == 'subscribe_button_click') {
		$('.modal_h1_msg').html('Subscription').show();
		$('.modal_h1_msg').next('hr').show();
		$('.modal.commonmodal .modal_data').addClass(data.add_class_in_modal_data).html(data.senddata)
		$('.modal.commonmodal').show();
	}
	if (data.tag == 'membership_button_click') {
		$('.modal_h1_msg').html('Pro Esports Membership');
		$('.modal.commonmodal .modal_data').addClass(data.add_class_in_modal_data).html(data.senddata)
		$('.modal.commonmodal').show();
	}
	if (data.tag == 'join_bottom_wtnglst') {
		$('.modal_h1_msg').addClass('jointwaitinglst_modal_head').html(data.modal_title);
		$('.modal.commonmodal .jointwaitinglst_modal_head').next('hr').remove();
		$('.modal.commonmodal .modal_data').addClass(data.add_class_in_modal_data).html(data.senddata);
		$('.modal.commonmodal').show();	
	}
	if (data.tag == 'view_subscription_plan_info') {
		$('<div class="row subscription_plan-info_div" style="display:none;"></div>').insertAfter('.subscribe_main_div');
		$('.subscribe_main_div').hide();
		$('.custmodal.commonmodal .close, .modal_h1_msg').hide();
		$('<p class="modal_h2_subscription_plan-info text-left">'+data.title+'</p>').insertAfter('.modal_h1_msg');
		$('.subscription_plan-info_div').html(data.subscribe_plan_info).slideDown('slow');
	}
	if (data.tag == 'view_subs_image') {
		$('.modal.commonmodal .modal_h1_msg, .modal.commonmodal hr').hide();
		$('.modal.commonmodal').removeClass().addClass('modal custmodal commonmodal '+data.add_class_in_modal);
		$('.modal.commonmodal .modal_data').addClass(data.add_class_in_modal_data).html(data.senddata)
		$('.modal.commonmodal').show();
	}
	if (data.tag == 'confirm_modal') {
		$('.modal.commonmodal hr').hide();
		$('.modal_h1_msg').html(data.modal_title);
		$('.modal.commonmodal .modal_data').html(data.senddata);
		$('.modal.commonmodal').removeClass().addClass('modal custmodal commonmodal '+commonmodal_calss+data.add_class_in_modal_data).show();
	}
	if (data.tag == 'commonmodalshow') {
		var tg_id = (data.tgid != undefined && data.tgid != '') ? data.tgid : '';
		$('.modal_h1_msg').addClass(data.add_class_in_modal_data+'_div').html(data.modal_title);
		$('.modal.commonmodal').addClass(data.add_class_in_modal_data);
		$('.modal.commonmodal .modal_data').html(data.senddata);
		$('.modal.commonmodal').show();
		var cls = data.add_class_in_modal_data;
		$('.commonmodal.'+cls+' .yes[tg_id="'+data.tgid+'"]').click(function () {
			var delete_id = $(this).attr('tg_id');
			if (cls == 'delete_prdct_modal') {
				var url = base_url + 'products/delete_product/'+delete_id;
				window.location.href = url;
			}
			if (cls == 'delete_plrcat') {
				var url = base_url + 'category/delete/'+delete_id;
				window.location.href = url;
			}
		});
	}
	if (data.tag == 'send_socket_msg_lobby_grp') {
		var is_admin = (data.is_admin != '') ? data.is_admin : $('input[name="is_admin"]').val();
		var user_id = (data.session_id !='') ? data.session_id : $('input[name="user_id"]').val();
		var lobby_custom_name = (data.lobby_id != undefined && data.lobby_id != '') ? data.lobby_id : $('input[name="lobby_custom_name"]').val();
		var getmytip_sound = (data.getmytip_sound != undefined && data.getmytip_sound != '') ? data.getmytip_sound : '';
		var socket_data = {
			'lobby_id': lobby_custom_name,
			'msg_data': data.msg_data,
			'name': data.name,
			'session_id': user_id,
			'getmytip_sound': getmytip_sound,
			'is_admin': is_admin,
			'on': 'send_messagelobby_grp',
		};
		socket.emit( 'sendrealtimedata', socket_data );
	}
	if (data.tag == 'change_file_name_stream_setting') {
		$('.modal_h1_msg').html(data.modal_title);
		$('.modal.commonmodal .modal_data').html(data.senddata);
		$('.modal.commonmodal').removeClass().addClass('modal custmodal commonmodal '+data.add_class_in_modal).show();
	}
	if (data.tag == 'view_make_event') {
		$('.modal_h1_msg').html(data.modal_title);
		$('.modal.commonmodal .modal_data').html(data.senddata);
		$('.modal.commonmodal').removeClass().addClass('modal custmodal commonmodal '+data.add_class_in_modal).show();

		var dateNow = new Date();
		$('.custom_datetimepicker').datetimepicker({
			format: 'YYYY-MM-DD HH:mm',
			defaultDate: dateNow,
		});
	}
	if(data.tag == 'proceed_to_purchase_membership') {
		$('.modal.commonmodal .modal_h1_msg, .modal.commonmodal hr').hide();
		$('.modal.commonmodal .modal_data').html(data.senddata);
		$('.modal.commonmodal').removeClass().addClass('modal custmodal commonmodal '+data.add_class_in_modal).show();
	}
	if (data.tag == 'cmninfo_shw') {
		$('.modal_h1_msg').html(data.modal_title);
		$('.modal.commonmodal .modal_data').html(data.senddata);
		$('.modal.commonmodal').removeClass().addClass('modal custmodal commonmodal '+data.add_class_in_modal_data).show();

		$('[data-toggle="tooltip"]').tooltip({
		    trigger : 'hover'
		})	
	}
	if (data.tag == 'shipping_type') {
		$('.modal_h1_msg').html(data.modal_title);
		$('.modal.commonmodal .modal_data').html(data.senddata);
		$('.modal.commonmodal').addClass(data.add_class_in_modal_data).show();	
		$('.inside_us_choose').on('click', function(){
			window.location.href = base_url+'checkout/inside';
		})
		$('.outside_us_choose').on('click', function(){
			window.location.href = base_url+'checkout/outside';
		})
	}
	if (data.tag == 'share_clip') {
		$('.modal_h1_msg').html(data.modal_title);
		$('.modal.commonmodal .modal_data').html(data.senddata);
		$('.modal.commonmodal').addClass(data.add_class_in_modal_data).show();
		$('.copy_clip').on('click', function(){
			var copyText = $(this).attr('data-link');
			var textarea = document.createElement("textarea");
		  	textarea.textContent = copyText;
		  	textarea.style.position = "fixed"; // Prevent scrolling to bottom of page in MS Edge.
		  	document.body.appendChild(textarea);
		  	textarea.select();
		  	document.execCommand("copy");
		  	document.body.removeChild(textarea);
			document.execCommand("copy");
			alert('copied link')
		})
	}
	if (data.tag == 'add_rules_modal_show') {
		$('.modal_h1_msg').addClass(data.add_class_in_modal_data+'_div').html(data.modal_title);
		$('.modal.commonmodal').addClass(data.add_class_in_modal_data);
		$('.modal.commonmodal .modal_data').html(data.senddata)
		$('.modal.commonmodal').show();
		$('#add_rules_yes').on('click', function(){
			$('#add_rules_hidden').val(1);
			$('#frm_game_create').submit();
		})
		$('#add_rules_no').on('click', function(){
			$('#add_rules_hidden').val(0);
			$('#frm_game_create').submit();
		})
	}

	if (data.tag == 'edit_tracking') {
		$('.modal_h1_msg').html(data.modal_title);
		$('.modal.commonmodal .modal_data').html(data.senddata);
		$('.modal.commonmodal').removeClass().addClass('modal custmodal commonmodal '+data.add_class_in_modal).show();
	}

	$('.commonmodal .join_wtnglst .yes').on('click', function() {
		var table = $(this).attr('target_table');
		var lobby_id = $('input[name="lobby_id"]').val();
		var user_id = $('input[name="user_id"]').val();
		var postData = {
			'table':table,
			'lobby_id': lobby_id,
			'user_id':user_id
		};
		$.ajax({
			url : base_url +'livelobby/joinwaitinglist',
		    data: postData,
		    type: 'POST',
		    success: function(res) {
		    	var srcData = $.parseJSON(res);
		    	var lobby_custom_name = $('input[name="lobby_custom_name"]').val();
		    	if (srcData.reached_limit != undefined && srcData.reached_limit == 'yes') {
		    		window.location.href = base_url+'dashboard';
		    		return false;
		    	}
		    	if (srcData.balance == 'less') {
		            window.location.href = base_url+'points/buypoints';
		            return false;
		    	}
		    	var is_spectator = (srcData.get_ticket_detail.length>0 && srcData.get_ticket_detail[0].lobby_is_spectator == 1)?'yes':'no';
		    	$('.join_wtng_btn').hide();
		    	$('.commonmodal').hide();
					var socket_data = {
						'lobby_id':lobby_custom_name,
						'get_my_fan_tag':srcData.get_my_fan_tag,
						'waiting_table':srcData.waiting_table,
						'user_id':srcData.user_id,
						'is_spectator':is_spectator,
						'on':'join_wtnglst_data',
					};
					socket.emit('sendrealtimedata', socket_data );
		    }
		});
	});
	$('.reset_giveaway_entries').click(function() {
		var lobby_id = $('input[name="lobby_id"]').val();
		var senddata = '<div class="col-sm-12">';
	    senddata += '</div><div class="col-sm-12"><h3><a class="button yes resetgiveaway_btn">OK</a><a class="button no">Cancel</a></h3></div>';
	    var modaltitle = 'Are you sure to Reset Giveaway ?';
	    var add_class_in_modal_data = 'giveaway_reset_entries';
	    var data = {
	      'add_class_in_modal_data':add_class_in_modal_data,
	      'modal_title': modaltitle,
	      'senddata':senddata,
	      'tag':'confirm_modal'
	    }
	    commonshow(data);
	});

	$('.resetgiveaway_btn').click(function() {
		var lobby_id = $('input[name="lobby_id"]').val();
		var postData = {
			'lobby_id': lobby_id,
		};
		$.ajax({
			url : base_url +'giveaway/resetgiveaway',
		    data: postData,
		    type: 'POST',
		    beforeSend: function(){ loader_show(); },
		    success: function(result) {
		      var result = $.parseJSON(result);
		      loader_hide();
		      $('.commonmodal').hide();
		      if (result.updated != '') {
		      	var lobby_custom_name = $('input[name="lobby_custom_name"]').val();
		      	var socket_data = {
							'lobby_id':lobby_custom_name,
							'id' : result.updated_where.lobby_id,
							'on':'reset_giveaway',
						};
						socket.emit('sendrealtimedata', socket_data );
		      }
		    }
		});
	});
	$('.giveaway_setting_form input[name="amount"]').on('keyup keypress', function (e) {
		((e.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) ? e.preventDefault() : '';
		var amount = $(this).val();
		(amount != '') ? ((parseFloat(amount).toFixed(2) == 0) ? $('input[name="is_multiple_entries"]').prop('checked', false).closest('.switch').addClass('disabled') : $('input[name="is_multiple_entries"]').closest('.switch').removeClass('disabled')) : '';
	});
	$('.change_giveaway_settings input[name="use_custom_fireworks_audio"]').change(function() {
		if ($(this).is(':checked')) {
			$('.select_custom_sounds').show().siblings('.default_sounds').hide();
			($('.giveaway_settings').attr('fireworks_audio') == '') ? $('.select_custom_sounds input[type="file"]').attr('required','') : '';
		} else {
			$('.default_sounds').show().siblings('.select_custom_sounds').hide();
			$('.select_custom_sounds input[type="file"]').removeAttr('required');
		}
	});
	$('#confirm_start_tournament').click(function () {
	    var lobby_id = $('input[name="lobby_id"]').val();
	    var user_id = $('input[name="user_id"]').val();
	    var postData = {
	      'lobby_id': lobby_id,
	      'user_id':user_id,
	    };
	    $.ajax({
	    	url: base_url+'livelobby/start_tournament',
	    	type: "post",
	    	data: postData,
	    	beforeSend: function(){ loader_show(); },
	    	success: function (res) {
	      		loader_hide();
	      		var res = $.parseJSON(res);
	      		$('.commonmodal').hide();
	      		var lobby_custom_name = $('input[name="lobby_custom_name"]').val();
	      		$('#start_trnmnt_btn').hide();
	      		$('#end_trnmnt_btn').show();
	      		// $('.total_tournament_price_coll').find('#start_trnmnt_btn').html('END Tournament');
	      		// $('.total_tournament_price_coll').find('#start_trnmnt_btn').attr('id','end_trnmnt_btn');
	      		var socket_data = {
					'lobby_id': lobby_custom_name,
					'balance_arr' : res.balance_arr,
					'on': 'balance_update'
				};
				socket.emit('sendrealtimedata', socket_data );
	      	}
	    });
	});

	$('#confirm_end_tournament').click(function () {
	    var lobby_id = $('input[name="lobby_id"]').val();
	    var user_id = $('input[name="user_id"]').val();
	    var postData = {
	      'lobby_id': lobby_id,
	      'user_id':user_id,
	    };
	    $.ajax({
	    	url: base_url+'livelobby/end_tournament_data',
	    	type: "post",
	    	data: postData,
	    	beforeSend: function(){ loader_show(); },
	    	success: function (res) {
	      		var data = $.parseJSON(res);
	    		console.log(data)
	    		var total_dist_amt = data.total_dist_amt;
	    		var res = data.res;
	      		loader_hide();
	      		var append_dist_data = '';
	      		$(res).each(function(i,val){
	      			var dist_profile_img = (val.image != null && val.image != '') ? val.image : 'placeholder-image.png';
	      			var common_settings = $.parseJSON(val.common_settings);
	      			var admin_lable = (val.is_admin == 1) ? '<div class="anim_lable">Admin</div>' : ((common_settings != '' && common_settings != null && common_settings.free_membership != undefined && common_settings.free_membership == 'on') ? '<div class="anim_lable">Free membership</div>' : '');
	      			append_dist_data += '<div class="text-left amt_users_div" user_id="'+val.user_id+'">'+admin_lable+'<div class="ga_users inputWithIconRight inputIconBg" ><a href="'+base_url+'user/user_profile/?fans_id='+val.user_id+'" target="_blank" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="View Profile" style=""><div class="profile_img_div"><img src="'+base_url+'upload/profile_img/'+val.image+'" class="img img-responsive" alt="smile"></div><div class="acc_anim_lable"># '+val.account_no+'</div><span>'+val.fan_tag+'</span></a> <input type="number" min="0" max="100" name="'+val.user_id+'" class="form-control" value="0"><i class="fa fa-percent fa-lg fa-fw" aria-hidden="true"></i> </div></div>'
	      		});
	      		$('.amt_tables').html(append_dist_data);
	      		$('.commonmodal').hide();
	      		$('#amount_distribution_modal .total_dist_amt').html('$ '+total_dist_amt.toFixed(2));
	      		$('#amount_distribution_modal #dist_amount_hidden').val(total_dist_amt.toFixed(2));
	      		$('#amount_distribution_modal .total_dist_per').html('100 %');
	      		$('#amount_distribution_modal').show();
	      	}
	    });
	});

	$('#amount_distribution_modal #distribution_type_select').on('change',function (){
		var distribution_type = $(this).is(":checked") ? 'amount' : 'percentage';
		if(distribution_type == 'amount'){
			$('#amount_distribution_modal #dist_type').val('amount');
			$('#amount_distribution_modal .total_dist_per').html('$ '+$('#amount_distribution_modal #dist_amount_hidden').val());
			$('#amount_distribution_modal #greater_amount_error').html("Total of all user's amount shouldn't be greater than "+$('#amount_distribution_modal #dist_amount_hidden').val());
			$('#amount_distribution_modal #dist_note').html("*Note: Distribution amount should be in USD");
			$('#amount_distribution_modal .ga_users').removeClass('inputWithIconRight');
			$('#amount_distribution_modal .ga_users').addClass('inputWithIcon');
			$('#amount_distribution_modal .ga_users i').removeClass('fa-percent');
			$('#amount_distribution_modal .ga_users i').addClass('fa-usd');
			$(document).find('#amount_distribution_modal input[type="number"]').val('0.00');
		}else{
			$('#amount_distribution_modal #dist_type').val('percentage');
			$('#amount_distribution_modal .total_dist_per').html('100 %');
			$('#amount_distribution_modal #greater_amount_error').html("Total of all user's percentage shouldn't be greater than 100");
			$('#amount_distribution_modal #dist_note').html("*Note: Distribution amount should be in percentage");
			$('#amount_distribution_modal .ga_users').removeClass('inputWithIcon');
			$('#amount_distribution_modal .ga_users').addClass('inputWithIconRight');
			$('#amount_distribution_modal .ga_users i').removeClass('fa-usd');
			$('#amount_distribution_modal .ga_users i').addClass('fa-percent');
			$(document).find('#amount_distribution_modal input[type="number"]').val('0');
		}
		$('#amount_distribution_modal .total_dist_per').removeClass('lable_red');	
		$('#amount_distribution_modal .total_dist_per').addClass('lable_orange');
	});

	$('#amount_distribution_modal .distribution_submit').off("click").on('click',function(){
		var distribution_type = ($('#amount_distribution_modal #dist_type').val() == 'amount') ? 'amount' : 'percentage';
		var total_per = 0;
		var dist_arr = {};
		if(distribution_type == 'amount'){
			$('#amount_distribution_modal input[type="number"]').each(function(i){
				var val = ($(this).val() != '') ? parseFloat($(this).val()).toFixed(2) : 0;
				total_per += parseFloat(val);
				// total_per = total_per.toFixed(2);
				dist_arr[$(this).attr('name')] = val;
			});
		}else{
			$('#amount_distribution_modal input[type="number"]').each(function(i){
				var val = ($(this).val() != '') ? $(this).val() : 0;
				total_per += parseFloat(val);
				dist_arr[$(this).attr('name')] = val;
			});
		}

		if((distribution_type == 'amount' && total_per > parseFloat($('#amount_distribution_modal #dist_amount_hidden').val())) || (distribution_type == 'percentage' && total_per > 100)){
			$('.span_error').show();
			setTimeout(function(){
				$('.span_error').hide();
			},10000);
		}else{
			var lobby_id = $('input[name="lobby_id"]').val();
			$.ajax({
		    	url: base_url+'livelobby/end_tournament',
		    	type: "post",
		    	data: {postData:dist_arr, lobby_id:lobby_id, distribution_type:distribution_type, total:total_per},
		    	beforeSend: function(){ loader_show(); },
		    	success: function (res) {
		    		loader_hide();
		    		var res = $.parseJSON(res);
		      		if(res.success == 0){
		      			$('#amount_distribution_modal #enough_balance_error').html(res.message);
		      			$('#amount_distribution_modal #enough_balance_error').show();
						setTimeout(function(){
							$('#amount_distribution_modal #enough_balance_error').hide();
						},10000);
		      		}else{
		      			$('.total_tournament_price_coll').hide();
			      		$('#amount_distribution_modal').hide();
			      		var lobby_custom_name = $('input[name="lobby_custom_name"]').val();
			      		var socket_data = {
							'lobby_id': lobby_custom_name,
							'balance_arr' : res,
							'on': 'balance_update'
						};
						socket.emit('sendrealtimedata', socket_data );
		      		}
		      	}
		    });
		}
	});

	$(document).on('blur', '#amount_distribution_modal input[type="number"]', function(){
		if($('#amount_distribution_modal #dist_type').val() == 'amount'){
			var total_per = parseFloat($('#amount_distribution_modal #dist_amount_hidden').val()).toFixed(2);
			$('#amount_distribution_modal input[type="number"]').each(function(i){
				var val = ($(this).val() != '') ? parseFloat($(this).val()).toFixed(2) : 0;
				total_per -= parseFloat(val);
				total_per = total_per.toFixed(2);
			});
		}else{
			var total_per = 100;
			$('#amount_distribution_modal input[type="number"]').each(function(i){
				var val = ($(this).val() != '') ? $(this).val() : 0;
				total_per -= parseFloat(val);
			});
		}
		if(total_per < 0){
			$('#amount_distribution_modal .total_dist_per').removeClass('lable_orange');	
			$('#amount_distribution_modal .total_dist_per').addClass('lable_red');	
		}else{
			$('#amount_distribution_modal .total_dist_per').removeClass('lable_red');	
			$('#amount_distribution_modal .total_dist_per').addClass('lable_orange');
		}
		var total_val = ($('#amount_distribution_modal #dist_type').val() == 'amount') ? '$ '+total_per : total_per+ ' %';
		$('#amount_distribution_modal .total_dist_per').html(total_val);
	})

	var data = {
		'event': 'single_page_load'
	};
	commonappend(data);
}
function PreviewAudio(inputFile, previewElement) {
	if (inputFile.files && inputFile.files[0] && $(previewElement).length > 0) {
		$(previewElement).stop();
		var reader = new FileReader();
		reader.onload = function (e) {
			$(previewElement).attr('src', e.target.result);
			// var playResult = $(previewElement).get(0).play();
			// if (playResult !== undefined) { 
			// 	playResult.then(_ => { $(previewElement).show(); }).catch(error => {
   //          $(previewElement).hide();
   //          alert("File Is Not Valid Media File");
   //        });
			// }
		};
		reader.readAsDataURL(inputFile.files[0]);
	} else {
    // $(previewElement).attr('src', '');
    // $(previewElement).hide();
    // alert("File Not Selected");
  }
}

function lobby_tag_change(data){
	var socket_data;
	$.ajax({
	    url : base_url +'livelobby/change_lobby_tag',
	    data: data,
	    type: 'POST',
	    success: function(res) {
			var lobby_custom_name = $('input[name="lobby_custom_name"]').val();
	    	$('#edit_lobby_tag_mdl').hide();
	    	socket_data = {
				'lobby_id':lobby_custom_name,
				'lobby_tag':data.lobby_tag,
				'on':'change_lby_tag',
			};
			socket.emit('sendrealtimedata', socket_data );
	    }
	}); 
}

function endLiveStream(user_id, lobby_id, group_bet_id){
	if(user_id != '' && lobby_id != '' && group_bet_id != '' && user_id == $('input[name="user_id"]').val()){
		var postData = { 
	    	'user_id':user_id,
	    	'lobby_id':lobby_id,
	    	'group_bet_id':group_bet_id,
	    	'stream_type':'obs',
	    	'channel_name':''
	    };
		send_stream_status(postData);
	}
}

function send_stream_status(data){
	// console.log(data);
	var postdata = data;
	var socket_data;
	$.ajax({
		url : base_url +'livelobby/change_stream_status',
		data : postdata,
		type : 'POST',
		beforeSend: function(){ loader_show(); },
	    success: function (res) {
	      	loader_hide();
			var res = $.parseJSON(res);
			if (res.sync == '1' && res.sync_msg != undefined && res.sync_msg != null && res.sync_msg) {
				$('.obs_sync_msg').addClass('alert alert-success').html(res.sync_msg).show();
				return false;
			}
			var streamer_id, table_cat, streamer_img, streamer_img, group_bet_id, streamer_channel, streamer_name;
			var lobby_id = res.lobby_id;
			streamer_img = res.fan_detail.image;
			// if (res.stream_update_as_fan != undefined && res.stream_update_as_fan == 'yes') {
				table_cat = res.streamer_details.table_cat;
				streamer_id = res.streamer_details.user_id;
				streamer_channel = (res.streamer_details.stream_type == '1') ? res.streamer_details.stream_channel : res.streamer_details.obs_stream_channel;
				stream_type = res.streamer_details.stream_type;
			// } else {
				streamer_name = res.streamer_details.display_name != '' ? res.streamer_details.display_name : res.streamer_details.name;
				// streamer_id = res.streamer_details.user_id;
		  //     	table_cat = res.streamer_details.table_cat;
		      	group_bet_id = res.streamer_details.group_bet_id;
		      	// streamer_channel = res.streamer_details.stream_channel;
		      	// stream_type = res.streamer_details.stream_type;
			// }
			var updated_data = res.status_update_data;
			var fan_tag = res.fan_detail.fan_tag;
			var is_creator = (res.creator_id != '' && res.creator_id != undefined) ? res.creator_id : $('input[name="creator_id"]').val();
			var subscribedlist = res.subscribedlist;
			var lobby_custom_name = (res.custom_name != '' && res.custom_name != undefined) ? res.custom_name : $('input[name="lobby_custom_name"]').val();
			$('#enter_stream_channel').hide();
			var socket_data = {
				'lobby_id': lobby_custom_name,
				'follower_count': res.follow_count,
				'is_followd' : res.is_followed,
				'is_subscribed' : res.is_subscribed,
				'group_bet_id': group_bet_id,
				'streamer_id': streamer_id,
				'fan_tag': fan_tag,
				'streamer_name': streamer_name,
				'streamer_channel': streamer_channel,
				'streamer_img':streamer_img,
				'stream_type':stream_type,
				'updated_data':updated_data,
				'table_cat': table_cat,
				'is_creator':is_creator,
				'subscribedlist':subscribedlist,
				'on': 'change_stream_status_data'
			};
			socket.emit('sendrealtimedata', socket_data );
		}
	}); 
}
function send_streamchat_status(data){
	var socket_data;
	$.ajax({
   		url : base_url +'livelobby/change_streamchat_status',
   		data: data,
   		type: 'POST',
   		success: function(res) {
   			var res = $.parseJSON(res);
   			if (res.steam_chat_set == 'on') {
   				if (res.stream_type == 1) {
   					$('.chat_open_with_person').removeClass('hide');
	   				$('.stream_chat_img').remove();
	   				var append_html = '<div class="streamer_chat_iframe"><iframe src="https://www.twitch.tv/embed/'+res.streamer_channel_set+'/chat?parent='+ $(location).attr('hostname') +'" height="375" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe></div>';
	   				// <h3 align="center" class="chat_open_with_person">Chat open with '+res.streamer_name_set+'</h3><br><br><div class="streamer_chat_iframe"><iframe src="https://www.twitch.tv/embed/'+res.streamer_channel_set+'/chat?parent='+ $(location).attr('hostname') +'" height="375" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe></div>
	   				$('.chatstreamer').removeAttr('t_streamerchat').attr('t_streamerchat',res.streamer_chat_id_set).html(append_html);
					// document.getElementById("streamchat_"+res.streamer_chat_id_set).checked = true;
   				}
			} else {
				$('.chatstreamer').removeAttr('t_streamerchat').html('').html('<div class="stream_chat_img"><img src="'+base_url+'/assets/frontend/images/stream_chat_box_transperent.png" alt="Smiley face"></div>');
			}
		}
	}); 
}
function send_live_stream_show_status(data){
	 // debugger;
	var socket_data;
	$.ajax({
	    url : base_url +'livelobby/send_live_stream_show_status',
	    data: data,
	    type: 'POST',
	    success: function(res) {
	    	var res = $.parseJSON(res);	
	    	if (res.steam_chat_set == 'on') {
	    		var subscribed_plan = ''; 
	    		var payment_id = '';
	    		// var disclass = '';
	    		var disable_class = "send_tip_btn ";
	    		var subsclass = "subscribe_button commonmodalclick"; 
	    		var user_id = $('input[name="user_id"]').val();
	    		if (res.streamer_chat_id_set == user_id) {
	    			subsclass = 'not_allow';
	    			// disclass = 'disabled';
	    			disable_class = "tip_disable";
	    		}
	    		for (var i = 0; i < res.subscribedlist.length; i++) {
	    			if (res.subscribedlist[i].user_to == res.streamer_chat_id_set) {
	    				subscribed_plan = res.subscribedlist[i].plan_detail;
	    				payment_id = res.subscribedlist[i].payment_id;
	    			}
	    		}

	    		var res = {
					'follower_count': res.follow_count,
					'is_followd' : res.is_followed,
					'is_subscribed' : res.is_subscribed,
					'streamer_id':res.streamer_chat_id_set,
					'table_direction':res.table_cat,
					'fan_tag':res.streamer_name_set,
					'streamer_channel':res.streamer_channel_set,
					'subsclass':subsclass,
					'streamer_name':res.streamer_name_set,
					'streamer_img':res.getstreamer_detail.image,
					'stream_type' :res.stream_type,
					'plan_subscribed':subscribed_plan,
					'disable_class':disable_class,
					'action':'add_new_stream',
			    	'event':'reset_stream'
			  	}
			  	commonappend(res);

				// var append_data = '<div id="'+res.table_cat+'div'+res.streamer_chat_id_set+'stream" class="stream_iframe"><iframe src="'+base_url+'live_stream?channel='+res.streamer_channel_set+'" height="300" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe><div class="row streamer_channel_detail">';
				// append_data += '<div class="col-sm-4 text-left"><a data-id="'+res.streamer_chat_id_set+'" class="lobby_btn social_follow '+disclass+'" data-html="true" data-toggle="tooltip" data-placement="bottom" data-original-title="Follow">';
				// append_data += '<img src="'+base_url+'assets/frontend/images/follow.png"></a>';
				// append_data += '<sup><span class="cunt badge b_color" style="margin-left: 5px;">0</span></sup>';
				// append_data += '<a class="lobby_btn" href="'+base_url+'Store/'+res.streamer_chat_id_set+'" target="_blank" style="margin-left: -1.3em" data-html="true" data-toggle="tooltip" data-placement="bottom" data-original-title="Store"><img src="'+base_url+'assets/frontend/images/store.png"></a>';
				// append_data += '<a href="'+base_url+'user/user_profile/?fans_id='+res.streamer_chat_id_set+'" target="_blank" style="margin-left: 0.3em" data-html="true" data-toggle="tooltip" data-placement="bottom" data-original-title="Profile"><img src="'+base_url+'upload/profile_img/'+res.getstreamer_detail.image+'" class="profile_img"></a>';
				// append_data += '</div><div class="col-sm-4 text-right"><h3 align="center" class="live_lobby_h3">'+res.streamer_name_set+'</h3></div><div class="col-sm-4 text-right">';
				// append_data += '<a class="lobby_btn '+res.disable_class+'" streamer_name="'+res.streamer_name_set+'" streamer_id="'+res.streamer_chat_id_set+'" streamer_img="" plan_subscribed="'+subscribed_plan+'"  data-html="true" data-toggle="tooltip" data-placement="bottom" data-original-title="Subscribe">';
				// append_data += '<img src="'+base_url+'assets/frontend/images/subscription1p.png">';
				// append_data += '</a>';
				// append_data += '<a class="lobby_btn tip_disable" streamer_name="'+res.streamer_name_set+'" streamer_id="'+res.streamer_chat_id_set+'" style="margin-left: 0.3em" data-html="true" data-toggle="tooltip" data-placement="bottom" data-original-title="Tip">';
				// append_data += '<img src="'+base_url+'assets/frontend/images/tip2.png">';
				// append_data += '</a>';
	  			
		  		// 	append_data += '<div class="col-sm-4 text-left"> <a class="lobby_btn">Follow</a></div><div class="col-sm-4 text-center"><h3 align="center" class="live_lobby_h3">'+res.streamer_name_set+'</h3></div><div class="col-sm-4 text-right">';
		  		// 	append_data += '<a class="lobby_btn '+subsclass+'" streamer_name="'+res.streamer_name_set+'" streamer_id="'+res.streamer_chat_id_set+'" streamer_img="'+res.getstreamer_detail.image+'" plan_subscribed="'+subscribed_plan+'" payment_id="'+payment_id+'">Subscribe</a>';
				// append_data += ' <a class="lobby_btn '+res.disable_class+'" streamer_name="'+res.streamer_name_set+'" streamer_id="'+res.streamer_chat_id_set+'">TIP</a></div></div></div>';
				// $('.streamer_channel.text-'+res.table_cat).append(append_data);
	 		} else {
	 			(res.table_cat == 'center') ? $('.default_stream_img').removeClass('hide').addClass('show') : '';
	 			$('.streamer_channel.text-'+res.table_cat+' #'+res.table_cat+'div'+res.streamer_chat_id_set+'stream').remove();
	 		}
			$('.send_tip_btn').click(function () {
				var streamer_id = $(this).attr('streamer_id');
				$('#send_tip_amount .team_member input[type="checkbox"]#'+streamer_id).attr('checked','');
				$('#send_tip_amount .team_member input[type="checkbox"]').not($('#send_tip_amount .team_member input[type="checkbox"]#'+streamer_id)).removeAttr('checked');
				$('#send_tip_amount .team_member .form-group').removeClass('active_usr');
				$('#send_tip_amount .team_member .form-group#activeusr_'+streamer_id).addClass('active_usr');
				$('#send_tip_amount').show();
			});
			$('#send_tip .no, #send_tip .close').click(function(){
				$('#send_tip').hide();
			});	
			var data = { 'event': 'single_page_load' };
			commonappend(data);
		}
	}); 
}
function send_tip_to_streamer(data){
	var socket_data;
	$.ajax({
	  url : base_url +'livelobby/send_tip_to_streamer',
	  data: data,
	  type: 'POST',
	  success: function(res) {
	  	var res = $.parseJSON(res);
	  	if (res.balance_status=='less') {
	  		alert("You do not have enough credits to place a challenge. Please Proceed to Purchase tab for more.");
	      window.location.href = base_url+'points/buypoints';
	  	} else {
	  		var from_total_balance = res.from_total_balance;
	      var to_total_balance = res.to_total_balance;
	      var user_from = res.user_from;
	      var user_to = res.user_to;
	      $('#send_tip_amount').hide();
	      socket_data = {
					'from_total_balance': from_total_balance,
					'to_total_balance': to_total_balance,
					'user_from': user_from,
					'user_to': user_to
				};
				socket.emit( 'get_tip_data', socket_data );
	  	}
	  }
	});
}
$('.upgrade_spectator_to_event').click(function() {
	var lobby_id = $('input[name="lobby_id"]').val();
	var tkt_prc = $('input[name="event_tkt_amnt"]').val();
	var senddata = '<div class="col-sm-12">';
    senddata += '</div><div class="col-sm-12"><h3><a class="button yes"">OK</a><a class="button no">Cancel</a></h3></div>';
    var modaltitle = '<h2 class="text-center">$'+tkt_prc+' Will be deducted from your wallet</h2></hr><h3 class="text-center">Are you sure to Upgrade Spectate to Event ?</h3>';
    var add_class_in_modal_data = 'upgrade_spectator_to_event';
    var data = {
      'add_class_in_modal_data':add_class_in_modal_data,
      'modal_title': modaltitle,
      'senddata':senddata,
      'tag':'confirm_modal'
    }
    commonshow(data);
});

function commonappend(data) {
	var crtr = $('input[name="crtr"]').val();
	var user_id = $('input[name="user_id"]').val();
	var is_cretr = (crtr == user_id) ? 'yes' : 'no';
	var socket_data;
	var cudttime = new Date();
	if (data.event == 'get_lobby_group_data') {
		var result = data;
		var lobby_id = $('input[name="lobby_id"]').val();
		var postData = {
			'lobby_id': lobby_id,
			'group_bet_id': data.group_bet_id,
			'table_cat': data.table_cat,
			'user_id': data.user_id,
			'user_name': data.user_name
		};
		$.ajax({
			url: base_url+'livelobby/get_lobby_singleuser',
			type: "post",
			data: postData,
			success: function(srcData){
				var tooltip;
				srcData = $.parseJSON(srcData);
				if (result.get_defaultstream != undefined && result.get_defaultstream == 1) {
					var streamer_id, streamer_img, streamer_img, group_bet_id, streamer_channel, streamer_name, updated_data, is_creator, subscribedlist, is_spectator, stream_type, streamer_channel, stream_channel_name;
					streamer_id = srcData.get_detail[0].user_id;
					stream_type = srcData.get_detail[0].stream_type;
					streamer_channel = (stream_type == '1') ? srcData.get_detail[0].stream_channel : srcData.get_detail[0].obs_stream_channel;
					streamer_name = (srcData.get_detail[0].display_name != '' && srcData.get_detail[0].display_name != null) ? srcData.get_detail[0].display_name : srcData.get_detail[0].name;
					streamer_img = (srcData.get_detail[0].image != '') ? srcData.get_detail[0].image : srcData.get_default_img;
					is_creator = $('input[name="creator_id"]').val();
					subscribedlist = srcData.subscribedlist;
					is_spectator = (srcData.get_ticket_detail.length > 0 && srcData.get_ticket_detail[0].lobby_is_spectator == 1) ? 'yes' : 'no';

					updated_data = 'enable';
					var data = {
						'follower_count': srcData.follow_count,
						'is_followd' : srcData.is_followed,
						'is_subscribed' : srcData.is_subscribed,
						'lobby_id': srcData.lobby_custom_name,
						'group_bet_id': postData.group_bet_id,
						'streamer_id': streamer_id,
						'fan_tag': srcData.fan_tag,
						'streamer_type' : stream_type,
						'streamer_name': streamer_name,
						'streamer_channel': streamer_channel,
						'streamer_img':streamer_img,
						'updated_data': updated_data,
						'table_cat': postData.table_cat,
						'is_creator':is_creator,
						'is_spectator' : is_spectator,
						'subscribedlist':subscribedlist,
						'change_user_vice_versa' : result.change_user_vice_versa,
						'event': 'change_stream_status_data'
					};
					commonappend(data);
				}
	
				var table_cat = srcData.get_detail[0]['table_cat'].toLowerCase();
				var fantag = '<span is_fan="true" '+((srcData.get_detail[0].is_controller == 'yes') ? 'style="max-width: 40%;"' : '')+'>'+srcData.fan_tag+'</span>';
				var table_diraction = (table_cat == 'left') ? 'right' : 'left';

				var channel_status_class = (srcData.usertable_uid == srcData.post_userid) ? "stream_icon enabled" : "disabled";
				// var channel_status_class = "disabled";
				($('.group_user_row_'+srcData.post_userid).length) ? $('.group_user_row_'+srcData.post_userid).remove() : '';
				($('.select_head select[name="select_head"] .assign_table_ctrl#'+srcData.post_userid).length) ? $('.select_head select[name="select_head"] .assign_table_ctrl#'+srcData.post_userid).appendTo('.select_head select.select_head_'+table_cat) : $('.select_head select.select_head_'+table_cat).append('<option value="'+srcData.post_userid+'" class="assign_table_ctrl" id="'+srcData.post_userid+'">'+fantag+'</option>');
		   		var is_spectator_span = is_spectator == 'yes' ? '<span class="is_spectator_span"><i class="fa fa-eye"></i></span>':'';
		   		var checkbx_append_lable = (table_cat == 'right') ? '<label> '+srcData.fan_tag+' <input type="checkbox" name="teap_team[]" id="'+srcData.post_userid+'"> </label>' : '<label> <input type="checkbox" name="teap_team[]" id="'+srcData.post_userid+'"> '+srcData.fan_tag+' </label>';
		   		var donateusr = '<div class="form-group" id="activeusr_'+srcData.post_userid+'"><div class="input-group col-lg-12"><span class="tipamount"></span>';
			    donateusr += checkbx_append_lable;
			    donateusr += '</div></div>';
		      	($('#send_tip_amount .modal_div_border .team_member #activeusr_'+srcData.post_userid).length) ? $('#send_tip_amount .modal_div_border .team_member #activeusr_'+srcData.post_userid).appendTo('#send_tip_amount .modal_div_border .table-'+table_cat+' .team_member') : $('#send_tip_amount .modal_div_border .table-'+table_cat+' .team_member').append(donateusr);
		      	if (table_cat.length > 0) {
			        var get_data = '<div class="'+table_cat+'_table row margin0 text-'+table_cat+' group_user_row_'+srcData.post_userid+' ui-draggable">';
			        var stream_icon = (srcData.get_detail[0].stream_status == 'enable') ? 'live_stream_on.png' : 'live_stream_off.png';
			        // var stream_icon = 'live_stream_off.png';
			        if (table_diraction == 'right') {
			        	get_data += '<div class="padd0 col-lg-4">';
	        			if (srcData.lobby_creator == srcData.usertable_uid) {
	        	   			get_data += '<a self="yes" player_tag_name="'+srcData.fan_tag+'" class="leave_lobby" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Leave Live Lobby" lby_crtr="'+srcData.get_detail[0]['is_creator']+'" self="yes" usr_id="'+srcData.post_userid+'"><i class="fa fa-times"></i></a> ';
	        			} else if (srcData.usertable_uid == srcData.post_userid && srcData.lobby_creator != srcData.usertable_uid) {
	        	   			tooltip = 'data-toggle="tooltip" data-html="true" data-placement="bottom" title="Click to Stop. <br/> Live Lobby Stream"';
	        	   			get_data += '<a self="no" player_tag_name="'+srcData.fan_tag+'" class="leave_lobby" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Leave Live Lobby" lby_crtr="'+srcData.get_detail[0]['is_creator']+'" self="no" usr_id="'+srcData.post_userid+'"><i class="fa fa-times"></i></a> ';
	        			}
			        	if (srcData.lobby_creator == srcData.usertable_uid) {
			        		if (srcData.post_userid != srcData.usertable_uid) {
			        		   fantag = '<span class="pasctrl" id="'+srcData.post_userid+'" fan_tag="'+srcData.fan_tag+'" is_fan="true" '+((srcData.get_detail[0].is_controller == 'yes') ? 'style="max-width: 40%;"' : '')+'>'+srcData.fan_tag+'</span>';
			        		}
			        	}
			        	get_data += fantag;
			        	get_data += is_spectator_span;
			        	
			        	get_data +=' <span class="tooltip_div"><img src="'+base_url+'/assets/frontend/images/'+stream_icon+'" class="'+channel_status_class+'" alt="live_stream" id="stream_'+srcData.post_userid+'" '+tooltip+' stream_click_status="disable" stream_type='+srcData.get_detail[0].streamer_type+'>';
			        	get_data +='</span>';

			        	get_data += ' <span class="team_leader" id="teamhead_'+srcData.post_userid+'">';
			        	get_data += (srcData.get_detail[0].is_controller == 'yes') ? '<img src="'+base_url+'assets/frontend/images/table_leader_new.png" alt="Team Controller">' : '';
			        	get_data += (srcData.get_detail[0].is_creator == 'yes') ? '<img src="'+base_url+'assets/frontend/images/creator.png" alt="Lobby Creator">' : '';
			        	get_data +='</span>';
			        	get_data += '</div>';
			        	get_data += '<div class="padd0 col-lg-6 text-center">';
			        	(srcData.team_name != null && srcData.team_name !='') ? get_data += ' <span class="team_name">'+srcData.team_name+'</span>' : '';
			        	get_data += '</div>';
			        	get_data += '<div class="padd0 col-lg-2"> </div>';
			        } else {
						get_data += '<div class="padd0 col-lg-2"> </div>';
			        	get_data += '<div class="padd0 col-lg-6 text-center">';
			        	(srcData.team_name != null && srcData.team_name !='') ? get_data += '<span class="team_name">'+srcData.team_name+'</span> ' : '';
			        	get_data += '</div>';
			        	get_data += '<div class="padd0 col-lg-4 text-center">';
			        	if (srcData.usertable_uid == srcData.post_userid) {
			        		tooltip = 'data-toggle="tooltip" data-html="true" data-placement="bottom" title="Click to Stop. <br/> Live Lobby Stream"';
			        	}
			        	get_data += ' <span class="team_leader" id="teamhead_'+srcData.post_userid+'">';
			        	get_data += (srcData.get_detail[0].is_creator == 'yes') ? '<img src="'+base_url+'assets/frontend/images/creator.png" alt="Lobby Creator">' : '';
			        	get_data += (srcData.get_detail[0].is_controller == 'yes') ? '<img src="'+base_url+'assets/frontend/images/table_leader_new.png" alt="Team Controller">' : '';
			        	get_data +='</span>';
			        	get_data += '<span class="tooltip_div"><img src="'+base_url+'/assets/frontend/images/'+stream_icon+'" class="'+channel_status_class+'" alt="live_stream" id="stream_'+srcData.post_userid+'" '+tooltip+' stream_click_status="disable" stream_type='+srcData.get_detail[0].streamer_type+'>';
			        	get_data += '</span> ';
			        	// echo ($left_row['is_creator'] == 'yes') ? '<img src="'.$lobby_creator_img.'" alt="Lobby Creator">' : '';
			        	if (srcData.lobby_creator == srcData.usertable_uid) {
			        		if (srcData.post_userid != srcData.usertable_uid) {
			        		   fantag = '<span class="pasctrl" id="'+srcData.post_userid+'" fan_tag="'+srcData.fan_tag+'" is_fan="true" '+((srcData.get_detail[0].is_controller == 'yes') ? 'style="max-width: 40%;"' : '')+'>'+srcData.fan_tag+'</span>';
			        		}
			        	}
			        	get_data += is_spectator_span;
			        	get_data += fantag;
						if (srcData.lobby_creator == srcData.usertable_uid) {
							get_data +=' <a self="yes" player_tag_name="'+srcData.fan_tag+'" class="leave_lobby" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Leave Live Lobby" lby_crtr="'+srcData.get_detail[0]['is_creator']+'" usr_id="'+srcData.post_userid+'"><i class="fa fa-times"></i></a>';
						} else if (srcData.usertable_uid == srcData.post_userid && srcData.lobby_creator != srcData.usertable_uid) {
							get_data +=' <a self="no" player_tag_name="'+srcData.fan_tag+'" class="leave_lobby" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Leave Live Lobby" lby_crtr="'+srcData.get_detail[0]['is_creator']+'" usr_id="'+srcData.post_userid+'"><i class="fa fa-times"></i></a>';
						}
						get_data +=' </div>';
					}
					get_data +='</div>';
					$('.'+table_cat+'_table_first_div').append(get_data);
					for (var i = srcData.get_all_detail.length - 1; i >= 0; i--) {
						append_tables_direction = 'RIGHT';
						if (srcData.get_all_detail[i].table_cat=='RIGHT') {
							append_tables_direction = 'LEFT'
						}
						$('.group_user_row_'+srcData.get_all_detail[i].user_id+' .leave_lobby').removeAttr('everyone_readup');
						$('.group_user_row_'+srcData.get_all_detail[i].user_id+' .leave_lobby').attr('everyone_readup','no');
						if (srcData.get_all_detail[i].user_id == srcData.usertable_uid) {
							if (srcData.get_all_detail[i].play_status == 1) {
								var append_play_button_opp = '<label class="checkbox_container disabled"><input type="checkbox" checked="" disabled="" data-user="'+srcData.post_userid+'" class="text-'+append_tables_direction+'"><span class="checkmark"></span></label>';
							} else {
								if (srcData.get_lobby_group_play == srcData.lobby_bet_detail[0].game_type*2) {
									var append_play_button_opp = '<label class="checkbox_container"><input type="checkbox" data-user="'+srcData.post_userid+'" name="play_game" class="text-'+append_tables_direction+' play_game"><span class="checkmark"></span></label>';
								}
							}
							$('.group_user_row_'+srcData.usertable_uid).append(append_play_button_opp); 
						} else {
							if (srcData.get_all_detail[i].play_status == 1) {
								var append_play_button_opp = '<label class="checkbox_container disabled"><input type="checkbox" checked="" disabled="" data-user="'+srcData.post_userid+'" class="text-'+append_tables_direction+'"><span class="checkmark"></span></label>';
							} else {
								if (srcData.get_lobby_group_play == srcData.lobby_bet_detail[0].game_type*2) {
									var append_play_button_opp = '<label class="checkbox_container disabled"><input type="checkbox" disabled class="text-'+append_tables_direction+'"><span class="checkmark op_bg"></span></label>';
								}
							}
							$('.group_user_row_'+srcData.get_all_detail[i].user_id).append(append_play_button_opp); 
						}
					}
					var data = {
						'remaining_table':srcData.remaining_table,
						'full_table':srcData.full_table,
						'is_full_play':srcData.is_full_play,
						'post_userid':srcData.post_userid,
						'table_cat':table_cat,
						'event':'bottom_choose_tables_update'
					}
	      			commonappend(data);
			      	creator_dragndrop(crtr,user_id);
			      	(user_id == srcData.post_userid) ? $('.join_wtng_btn').hide() : '';
	      		}
	      		is_recieved = 0;
			}
		});
	}
	if (data.event == 'msg_sound_play') {
		if (data.audio_file != '') {
			var session_volume=$('#session_volume').val();

			 if(session_volume=="")
			 {
			 	volume_val=1.0;
			 }
			 else
			 {
			 	
			  var volume_val=session_volume/100;
			  var volume_val=volume_val.toFixed(1);
			 }
			var play_sound_html = '<audio controls volume="'+volume_val+'" autoplay><source src="'+base_url+'upload/stream_setting/'+data.audio_file+'"></audio>';
			$('.play_sound').html(play_sound_html);
			$("audio,video").prop("volume", volume_val);
		}
	}
	if (data.event == 'bottom_choose_tables_update') {
		var append_disable_tables = '<span class="close">&times;</span>';
		if (data.remaining_table != 'FULL') {
			append_disable_tables += '<h1>Choose Table..</h1>';
			if (data.full_table == 'LEFT') {
				$(".left_table_first_div.is_draggable").droppable({disabled: true});
				append_disable_tables += '<div class="col-sm-6 left_choose diable_choose_left"><h1 class="diable_choose_table">'+data.full_table+' table is full.. Please Join '+data.remaining_table+' table</h1></div>';
			} else {
				$(".left_table_first_div.is_draggable").addClass('left_table_droppable').droppable({disabled: false});
				append_disable_tables += '<div class="col-sm-6 left_choose"><div class="choose_table_cat" table_cat="LEFT">LEFT</div></div>';
			}
			if (data.full_table == 'RIGHT') {
				$(".right_table_first_div.is_draggable").droppable({disabled: true});
				append_disable_tables +='<div class="col-sm-6 right_choose diable_choose_right"><h1 class="diable_choose_table">'+data.full_table+' table is full.. Please Join '+data.remaining_table+' table</h1></div>';
			} else {
				$(".right_table_first_div.is_draggable").addClass('right_table_droppable').droppable({disabled: false});
				append_disable_tables +='<div class="col-sm-6 right_choose"><div class="choose_table_cat" table_cat="RIGHT">RIGHT</div></div>';	
			}
			if (data.is_full_play == 'no') {
				$('label.checkbox_container').remove();
			}
		} else {
			$(".left_table_first_div.left_table_droppable, .right_table_first_div.right_table_droppable").droppable({disabled: true});
			append_disable_tables += '<div class="col-sm-12 left_choose"><div class="row"><h1 class="diable_choose_table">Both Tables are FULL..!!</h1></div></div><div align="center" class="col-sm-12 close_button_row"><button class="close close_button"> Close </button></div>';
		}
		$('#join_lobby .modal-header').html(append_disable_tables);

		if (data.action == 'send_user_to_waitinglist') {
			if (user_id == data.post_userid) {
				$('.join_lby_game_btn_div>div>lable.join_lobby_btn').css('pointer-events','all').removeClass('already_joined').html('Join Table').addClass('join_lby_game_btn').css('cursor','pointer');
				if (data.user_id_balance != undefined) {
					$('span.tot-balance').html('$'+data.user_id_balance);
				}
				$('.join_wtng_btn').hide();
			}
			var stream_icon = 'live_stream_off.png'
			var stream_enableclass = 'stream_icon enabled';
			var stream_click_status_attr = 'stream_click_status="enable"';
			var is_eighteenimgtable = data.get_fan_detail.waiting_table.toLowerCase() == 'left' ? 'right' : 'left';
			var is_eighteen = data.get_fan_detail.is_18 == 1 ? 'yes' : 'no';
			var is_spectator_span = data.is_spectator == 'yes' ? '<span class="is_spectator_span"><i class="fa fa-eye"></i></span>' : '';
			var is_eighteenimg = data.get_fan_detail.is_18 == 1 ? '<span class="eighteen_plus_img pull-'+is_eighteenimgtable+'"><img src="'+base_url+'assets/frontend/images/eighteenplus.png" class="img" alt="Eighteen plus img"></span>' : '<span class="eighteen_plus_img pull-'+is_eighteenimgtable+'"><span class="under_eighteen_lable">Under 18</span></span>';
			if (data.get_fan_detail.stream_status == 'enable') {
				if (data.get_lobby_detail[0].creator != user_id) {
					stream_enableclass = 'disabled';
				}
				stream_icon = 'live_stream_on.png';
				stream_click_status_attr = 'stream_click_status="disable"';
			} else if (data.get_fan_detail.stream_status == 'disable') {
	   	   		if ($('.'+data.post_userid+'_live-stream').length) {
	   	   			$('.'+data.post_userid+'_live-stream').remove();
	   	   		}
			}
			var stream_waitingicon = (data.get_fan_detail.team_name != '' && data.get_fan_detail.team_name != null) ? '<span class="team_name">'+data.get_fan_detail.team_name+'</span> ' : ''; 
			stream_waitingicon += is_eighteenimg;
			if (data.get_lobby_detail[0].creator != data.post_userid && $(".streamer_list_box .team_stream .live_streamer_"+data.post_userid).length) {
				$(".streamer_list_box .team_stream .live_streamer_"+data.post_userid).remove();
			} else {
				stream_waitingicon += '<span class="waiting_streamicon"> <img src="'+base_url+'assets/frontend/images/'+stream_icon+'" alt="Stream Icon" class="'+stream_enableclass+'" '+stream_click_status_attr+'></span> ';
			}
			

			stream_waitingicon += is_spectator_span;
			stream_waitingicon += '<span>'+data.get_fan_detail.fan_tag+'</span>';
			var Self = data.get_lobby_detail[0].creator == user_id ? "yes" : "no"; 
			var iscrtor = data.get_lobby_detail[0].creator == data.post_userid ? "yes" : "no";
			if (user_id == data.post_userid || data.get_lobby_detail[0].creator == user_id) {
				stream_waitingicon += ' <a class="leave_lobby" self="'+Self+'" player_tag_name="'+data.get_fan_detail.fan_tag+'" data-toggle="tooltip" data-placement="bottom" title="" lby_crtr="'+iscrtor+'" usr_id="'+data.post_userid+'" everyone_readup="'+data.is_full_play+'" data-original-title="Leave Waiting List" leave_from_waitinlist="yes"><i class="fa fa-times"></i></a>';
			}
			if (data.get_fan_detail.waiting_table.toLowerCase() == 'left') {
				if (user_id == data.post_userid || data.get_lobby_detail[0].creator == user_id) {
					stream_waitingicon = '<a class="leave_lobby" self="'+Self+'" player_tag_name="'+data.get_fan_detail.fan_tag+'" data-toggle="tooltip" data-placement="bottom" title="" lby_crtr="'+iscrtor+'" usr_id="'+data.post_userid+'" everyone_readup="'+data.is_full_play+'" data-original-title="Leave Waiting List" leave_from_waitinlist="yes"><i class="fa fa-times"></i></a> ';
					stream_waitingicon += '<span>'+data.get_fan_detail.fan_tag+'</span> ';
				} else {
					stream_waitingicon = '<span>'+data.get_fan_detail.fan_tag+'</span> ';
				}
				if (data.get_lobby_detail[0].creator == data.post_userid) {
					stream_waitingicon += '<span class="waiting_streamicon"> <img src="'+base_url+'assets/frontend/images/'+stream_icon+'" alt="Stream Icon" class="'+stream_enableclass+'" '+stream_click_status_attr+'></span>';				
				}
				stream_waitingicon += is_spectator_span;
				stream_waitingicon += is_eighteenimg;
				(data.get_fan_detail.team_name != '' && data.get_fan_detail.team_name != null) ? stream_waitingicon += '<span class="team_name">'+data.get_fan_detail.team_name+'</span> ' : ''; 
			}

			var append = '<div class="waitingusr ui-draggable" id="'+data.get_fan_detail.id+'" lobby_id="'+data.lobby_id+'" fan_tag="'+data.get_fan_detail.fan_tag+'" user_id="'+data.post_userid+'" table="'+data.table_cat.toUpperCase()+'" is_eighteen="'+is_eighteen+'"> ';
			append += stream_waitingicon;
			append +='</div>';

			// if ($('.live_lby_waiting_list .waitingusr[user_id="'+data.post_userid+'"]').length) {
			// 	$('.live_lby_waiting_list .waitingusr[user_id="'+data.post_userid+'"]').appendTo('.live_lby_waiting_list .'+data.table_cat.toLowerCase()+'_tbl_wtnglst');
			// } else {
				$('.live_lby_waiting_list .waitingusr[user_id="'+data.post_userid+'"], .select_head select[name="select_head"] option#'+data.post_userid).remove();
				$('.live_lby_waiting_list .'+data.table_cat.toLowerCase()+'_tbl_wtnglst').append(append);
			// }
			$('.'+data.table_cat.toLowerCase()+'_table_first_div').addClass(data.table_cat.toLowerCase()+'_table_droppable');
		} else if (data.action == 'lobby_join_button_allow') {
			if (user_id == data.post_userid) {
				$('.join_lby_game_btn_div>div>lable.join_lobby_btn').css('pointer-events','all').removeClass('already_joined').html('Join Table').addClass('join_lby_game_btn').css('cursor','pointer');
			}
			$('.wtnglstlistdiv div[user_id="'+data.post_userid+'"]').remove();
		} else {
			if (user_id == data.post_userid) {
	    		$('.join_lby_game_btn_div>div>lable.join_lobby_btn').addClass('already_joined').html('Joined <i class="fa fa-check"></i>').css('pointer-events','none');
			}
			$('.join_lby_game_btn_div>lable.join_lobby_btn').removeClass('join_lby_game_btn').css('cursor','pointer');

			$('.wtnglstlistdiv div[user_id="'+data.post_userid+'"]').remove();
		}
		// var oppsite_table = (data.table_cat.toLowerCase() == 'left') ? 'right' : 'left'; 
		// if ($(".streamer_list_box .team_stream .live_streamer_"+data.post_userid).length > 0) {
		// 	$(".streamer_list_box .team_stream .live_streamer_"+data.post_userid).appendTo($(".streamer_list_box.streamer_"+data.table_cat+"_box .team_stream"));
		// }
		if ($('.streamer_channel.players_streamers .'+data.post_userid+'_live-stream.stream_iframe').length > 0) {
			$('.streamer_channel.players_streamers .'+data.post_userid+'_live-stream.stream_iframe').appendTo($('.streamer_channel.text-'+data.table_cat));
		}
	 		// if ($(".streamer_list_box .team_chat .live_streamer_"+data.post_userid).length) {
	 		// 	$(".streamer_list_box .team_chat .live_streamer_"+data.post_userid).appendTo($(".streamer_list_box.streamer_"+data.table_cat+"_box .team_chat"));
	 		// }
	}
	if (data.event == 'single_page_load') {
	}
	if (data.event == 'reset_stream') {
		if (data.action == 'add_new_stream') {
			var is_creator = $('input[name="creator_id"]').val();
			if (is_creator == data.streamer_id) {
			 	data.table_direction = 'center';
			}
			// connection.openOrJoin(data.streamer_channel, function(isRoomExist, roomid) {
			// 	if(isRoomExist) {
			//          connection.sdpConstraints.mandatory = {
			//              OfferToReceiveAudio: true,
			//              OfferToReceiveVideo: true
			//          };
			//        }
			//    });
			// debugger;
			// alert(data.disable_class);
			var rid = generateRandomId();
			var lobby_id = $('input[name="lobby_id"]').val();
			var group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
			// console.log(data);
		    // var append_data = '<div id="'+data.table_direction+'div'+data.streamer_id+'stream" class="'+data.streamer_id+'_live-stream stream_iframe"><iframe src="'+base_url+'live_stream?channel='+data.streamer_channel+'&user_id='+data.streamer_id+'" height="300" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe><div class="row streamer_channel_detail"><div class="col-sm-4 text-left">';
		    // var append_data = '<div id="'+data.table_direction+'div'+data.streamer_id+'stream" class="'+data.streamer_id+'_live-stream stream_iframe">';
		    // append_data += '<iframe src="https://player.twitch.tv/?channel='+data.streamer_channel+'&parent='+$(location).attr('hostname')+'" height="300" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe><div class="row streamer_channel_detail"><div class="col-sm-4 text-left">';
		    var stream_url = (data.stream_type == 1) ? 'https://player.twitch.tv/?channel='+data.streamer_channel+'&parent='+$(location).attr('hostname') : base_url+'live_stream?channel='+data.streamer_channel+'&user_id='+data.streamer_id+'&lobby_id='+lobby_id+'&group_bet_id='+group_bet_id;
		    
		    var append_data = '<div id="'+data.table_direction+'div'+data.streamer_id+'stream" class="'+data.streamer_id+'_live-stream stream_iframe">';
		    append_data += (data.stream_type == 1) ? '<iframe src="'+stream_url+'" height="300" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>' : '<div style="height: 300px;width: 100%;box-shadow: 0px 0px 3px 0px #ff5000;" id="'+rid+'"><div class="stream_error" class="alert alert-danger" style="display: none;"></div><video id="videojs-flvjs-player" autoplay class="obs_streaming video-js vjs-default-skin" controls style="width: 100%; height: 100%;" poster="'+base_url+'assets/frontend/images/stream_offline_img.jpeg" preload="auto" data-key="'+data.streamer_channel+'" data-random_id="'+rid+'" data-base_url="'+base_url+'" data-user_id="'+data.streamer_id+'" data-lobby_id="'+lobby_id+'" data-group_bet_id="'+group_bet_id+'"></video></div>'
		    append_data += '<div class="row streamer_channel_detail"><div class="col-sm-4 text-left">';
			append_data += '<a data-id="'+data.streamer_id+'" class="lobby_btn social_follow '+data.dis_class+'" data-html="true" data-toggle="tooltip" data-placement="bottom" data-original-title="Follow">';
			append_data += (data.is_followd == 'following') ? '<img src="'+base_url+'assets/frontend/images/follower.png"></a>' : '<img src="'+base_url+'assets/frontend/images/follow.png"></a>';
			append_data += '<sup><span class="cunt badge b_color" style="margin-left: 5px;">'+data.follower_count+'</span></sup>';
			append_data += '<a class="lobby_btn" href="'+base_url+'Store/'+data.streamer_id+'" target="_blank" style="margin-left: -1.3em" data-html="true" data-toggle="tooltip" data-placement="bottom" data-original-title="Store"><img src="'+base_url+'assets/frontend/images/store.png"></a>';
			append_data += '<a href="'+base_url+'user/user_profile/?fans_id='+data.streamer_id+'" target="_blank" style="margin-left: 0.3em" data-html="true" data-toggle="tooltip" data-placement="bottom" data-original-title="Profile"><img src="'+base_url+'upload/profile_img/'+data.streamer_img+'" class="profile_img"></a>';
			append_data += '</div><div class="col-sm-4 text-center"><h3 align="center" class="live_lobby_h3">'+data.fan_tag+'</h3></div><div class="col-sm-4 text-right">';
			append_data += (data.stream_type == 2) ? '<a class="lobby_btn" style="margin-right: 0.3em;display: none;" href="'+base_url+'Streamsetting/clip?streamer='+data.streamer_id+'&lobby_id='+lobby_id+'"  data-toggle = "tooltip" data-placement="bottom" data-original-title = "Clip" target="_blank"><img src = "'+base_url+'assets/frontend/images/clip_ico.png"></a>' : '';
			append_data += '<a class="lobby_btn '+data.subsclass+'" streamer_name="'+data.streamer_name+'" streamer_id="'+data.streamer_id+'" streamer_img="'+data.streamer_img+'" plan_subscribed="'+data.subscribed_plan+'" data-html="true" data-toggle="tooltip" data-placement="bottom" data-original-title="Subscribe">';
			append_data += (data.is_subscribed == 'yes') ? '<img src="'+base_url+'assets/frontend/images/subscription11.png">' : '<img src="'+base_url+'assets/frontend/images/subscription1p.png">';
			append_data += '</a>';
			append_data += '<a class="lobby_btn '+data.disable_class+'" streamer_name="'+data.fan_tag+'" streamer_id="'+data.streamer_id+'" style="margin-left: 0.3em" data-html="true" data-toggle="tooltip" data-placement="bottom" data-original-title="Tip"><img src="'+base_url+'assets/frontend/images/tip2.png"></a>';
			
			(data.table_direction.toLowerCase() == 'center') ? $('.default_stream_img').removeClass('show').addClass('hide') : '';
			if($(document).find('.'+data.streamer_id+'_live-stream').length > 0 && data.stream_type == 2){
				var attr_arr = {
					'data-key':data.streamer_channel,
					'data-lobby_id':lobby_id,
					'data-group_bet_id':group_bet_id,	
				} 
				$('.'+data.streamer_id+'_live-stream').find('video').attr(attr_arr);
			} else {
				$('.'+data.streamer_id+'_live-stream').remove();		
				$('.streamer_channel.text-'+data.table_direction).append(append_data);
				var skey = data.streamer_channel;
		        var burl = base_url;
		        var rid = rid;
		        var stream_arr = {
		           'stream_key' : skey,
		           'base_url' : burl,
		           'random_id' : rid,
		        };
		        stream_launch(stream_arr);
		        if($('#'+rid+' .obs_streaming').attr('data-is_record') != '0'){
		        	recordstream($('#'+rid+' .obs_streaming')[0]);
			    }				
			}
			if ($('.text-'+data.table_direction+'.streamer_channel .stream_iframe').length != 0) {
				$(".team_stream .live_streamer_"+data.streamer_id+" input[type='checkbox']").prop("checked",true).attr('readonly disabled');
			} else {
				$(".team_stream .live_streamer_"+data.streamer_id+' input[type="checkbox"]').prop("checked",false).removeAttr('readonly disabled');
			}
		}
	}	
	if (data.event == 'get_played_data') {
		if (data.userid != user_id) {
			var append_data = '<label class="checkbox_container disabled"><input type="checkbox" checked="" disabled="" data-user="'+data.userid+'" class="text-right"><span class="checkmark"></span></label>';
			$('.group_user_row_'+data.userid).append(append_data);
		}
		if (data.is_full_play == "yes") {
			if ($('.join_lby_game_btn.join_lobby_btn').length) {
				$('.join_lby_game_btn.join_lobby_btn').remove();
			}
		}
		if ($('#change_lobby_details.modal').length) {
			$('#change_lobby_details.modal').remove();
			$('.lobby_btn.change_lobby_dtil').remove();
		}
		if (data.get_lobby_size == data.get_playedgrp_data_count || data.is_full_play == "yes") {
			for (var i = data.get_playedgrp_data.length - 1; i >= 0; i--) {
				if (data.lobby_creator.user_id != data.get_playedgrp_data[i].user_id) {
					$('span.pasctrl#'+data.get_playedgrp_data[i].user_id).removeAttr('fan_tag');
					$('span.pasctrl#'+data.get_playedgrp_data[i].user_id).removeAttr('class');
					$('.group_user_row_'+data.get_playedgrp_data[i].user_id+' span#'+data.get_playedgrp_data[i].user_id).removeAttr('id');
				}
				$('.live_lby_in_thirow .leave_lobby').removeAttr('everyone_readup');
				$('.live_lby_in_thirow .leave_lobby').attr('everyone_readup', 'yes');
				if ($('div.change_game_size_div').length) {
					$('div.change_game_size_div').remove();
				}
				if (data.get_playedgrp_data[i].is_controller == 'yes') {
					if (data.get_playedgrp_data[i].user_id == data.userid) {
						$('.group_user_row_'+data.sessuser_id).append('<button class="bet_report button text-'+data.get_playedgrp_data[i].table_cat.toLowerCase()+'">Report</button>');
					}
				}
			}
			$('.right_table_first_div, .left_table_first_div').removeClass('is_draggable');
			$('.wtnglstlistdiv.left_tbl_wtnglst').removeClass('left_tbl_wtnglst');
			$('.wtnglstlistdiv.right_tbl_wtnglst').removeClass('right_tbl_wtnglst');
		}
	}
	if (data.event == 'get_witinglst_data') {

		var is_eighteen = data.join_wtnglst_data.is_18 == 1?'yes':'no';
		var wtngusr = '<div class="waitingusr" id="'+data.join_wtnglst_data.id+'" lobby_id="'+data.lobby_id+'" fan_tag="'+data.join_wtnglst_data.fan_tag+'" user_id="'+data.user_id+'" table="'+data.waiting_table+'" is_eighteen="'+is_eighteen+'">';
		var Self = crtr == user_id ? "yes" : "no"; 
		var iscrtor = crtr == data.user_id ? "yes" : "no";
		var isuser = user_id == data.user_id ? "yes" : "no";
		var is_eighteenimgtable = data.waiting_table.toLowerCase() == 'left' ? 'right' : 'left';
		var is_eighteenimg = data.join_wtnglst_data.is_18 == 1 ? '<span class="eighteen_plus_img pull-'+is_eighteenimgtable+'"><img src="'+base_url+'assets/frontend/images/eighteenplus.png" class="img" alt="Eighteen plus img"></span>' : '<span class="eighteen_plus_img pull-'+is_eighteenimgtable+'"><span class="under_eighteen_lable">Under 18</span></span>';
		
		var is_spectator = data.is_spectator == 'yes'? '<span class="is_spectator_span"><i class="fa fa-eye"></i></span>' : '';
		wtngusr += is_eighteenimg;
		(data.join_wtnglst_data.team_name != '' && data.join_wtnglst_data.team_name != null) ? wtngusr += '<span class="team_name">'+data.join_wtnglst_data.team_name+'</span> ' : ''; 
		if (iscrtor == 'yes' || data.join_wtnglst_data.is_18 == 0) {
			wtngusr +='<span class="waiting_streamicon"> <img src="'+base_url+'assets/frontend/images/live_stream_off.png" stream_click_status="enable" class="enable stream_icon" id="stream_'+data.user_id+'" alt="Stream Icon"></span>';
		}
		wtngusr += is_spectator;
		wtngusr += '<span>'+data.join_wtnglst_data.fan_tag+'</span>';
		if (Self == 'yes' || isuser == 'yes') {
			wtngusr +=' <a class="leave_lobby" self="'+Self+'" player_tag_name="'+data.join_wtnglst_data.fan_tag+'" data-toggle="tooltip" data-placement="bottom" title="" lby_crtr="'+iscrtor+'" usr_id="'+data.user_id+'" everyone_readup="no" data-original-title="Leave Waiting List" leave_from_waitinlist="yes"><i class="fa fa-times"></i></a>';
		}
		wtngusr += '</div>';
		if (data.waiting_table.toLowerCase() == 'left') {
			wtngusr = '<div class="waitingusr" id="'+data.join_wtnglst_data.id+'" lobby_id="'+data.lobby_id+'" fan_tag="'+data.join_wtnglst_data.fan_tag+'" user_id="'+data.user_id+'" table="'+data.waiting_table+'">';

			(Self == 'yes' || isuser == 'yes') ? wtngusr +='<a class="leave_lobby" self="'+Self+'" player_tag_name="'+data.join_wtnglst_data.fan_tag+'" data-toggle="tooltip" data-placement="bottom" title="" lby_crtr="'+iscrtor+'" usr_id="'+data.user_id+'" everyone_readup="no" data-original-title="Leave Waiting List" leave_from_waitinlist="yes"><i class="fa fa-times"></i></a> ' : '';

			wtngusr +='<span>'+data.join_wtnglst_data.fan_tag+'</span> ';
			wtngusr += is_spectator;
			if (iscrtor == 'yes' || data.join_wtnglst_data.is_18 == 0) {
				wtngusr +='<span class="waiting_streamicon"> <img src="'+base_url+'assets/frontend/images/live_stream_off.png" stream_click_status="enable" class="enable stream_icon" id="stream_'+data.user_id+'" alt="Stream Icon"></span>';
			}
			wtngusr += is_eighteenimg;
			(data.join_wtnglst_data.team_name != '' && data.join_wtnglst_data.team_name != null) ? wtngusr += '<span class="team_name">'+data.join_wtnglst_data.team_name+'</span> ' : ''; 
			wtngusr += '</div>';
		}
		$('.live_lby_waiting_list .'+data.waiting_table.toLowerCase()+'_tbl_wtnglst #'+data.join_wtnglst_data.id).remove();
		$('.live_lby_waiting_list .'+data.waiting_table.toLowerCase()+'_tbl_wtnglst').append(wtngusr);
	}
	if (data.event == 'get_leave_lobby_data') {
		if (data.refresh == 'yes') {
			window.location.reload();
		} else {
			var lobby_id = $('input[name="lobby_id"]').val();
			$('.group_user_row_'+data.user_id).remove();
			$('.live_lby_waiting_list .waitingusr[user_id="'+data.user_id+'"]').remove();
			$('#send_tip_amount .team_member #activeusr_'+data.user_id).remove();
			($('.select_head select[name="select_head"]').length) ? $('.select_head select[name="select_head"] option#'+data.user_id).remove() : '';
			
			$('#'+data.table_cat+'div'+data.user_id+'stream, .streamer_item#live_streamer_'+data.user_id+', .select_stream_chat select.select_chat option#streamchat_'+data.user_id).remove();
			
			var tbl_streamer_item = $('.streamer_'+data.table_cat.toLowerCase()+'_box .streamer_item');
			if (tbl_streamer_item != undefined && tbl_streamer_item.length >= 6) {
			 	$('.streamer_'+data.table_cat.toLowerCase()+'_box .show_more_navigation').show(); 
			} else {
			  $('.streamer_'+data.table_cat.toLowerCase()+'_box .show_more_navigation').hide();
			}

			($('.chatstreamer').attr('t_streamerchat') == data.user_id) ? $('.chatstreamer').removeAttr('t_streamerchat').find('.streamer_chat_iframe').html('<div class="stream_chat_img"><img src="'+base_url+'/assets/frontend/images/stream_chat_box_transperent.png" alt="Smiley face"></div>').closest('.chatstreamer').find(".select_stream_chat select.select_chat option:first").prop('selected',true) : '';
			$('.'+data.user_id+'_live-stream').remove();
			
			var t_streamerchat_id = $('.chatstreamer').attr('t_streamerchat');
			if (t_streamerchat_id != undefined) {
				if (t_streamerchat_id == data.user_id) {
					var stream_chat_img = '<div class="stream_chat_img"><img src="'+base_url+'/assets/frontend/images/stream_chat_box_transperent.png" alt="Smiley face"></div>';
					$('.chatstreamer').html(stream_chat_img);
				}
			}
			if (data.user_id.length>0) {
				var postData = {
					'lobby_id': lobby_id,
					'group_bet_id': data.group_bet_id,
					'table_cat': data.table_cat,
					'leaved_total_balance':data.leaved_total_balance,
					'user_id': data.user_id
				};
				$.ajax({
					url: base_url+'livelobby/get_lbydt_on_leave',
					type: "post",
					data: postData,
					success: function(srcData){
						srcData = $.parseJSON(srcData);
						if (srcData.lobby_updated == 1) {
			            	window.location.href = base_url;
						} else {
							for (var i = srcData.get_detail.length - 1; i >= 0; i--) {
								if (srcData.is_full_play == 'no') {
				            	$('.group_user_row_'+srcData.get_detail[i].user_id+' .leave_lobby').removeAttr('everyone_readup');
								$('.group_user_row_'+srcData.get_detail[i].user_id+' .leave_lobby').attr('everyone_readup','no');
									if (srcData.lobby_creator.user_id != srcData.get_detail[i].user_id) {
										$('.group_user_row_'+srcData.get_detail[i].user_id+' span[is_fan="true"]').attr('id',srcData.get_detail[i].user_id);
										var fan_tag = $('.group_user_row_'+srcData.get_detail[i].user_id+' span#'+srcData.get_detail[i].user_id).html();
										$('.group_user_row_'+srcData.get_detail[i].user_id+' span[is_fan="true"]').attr('fan_tag',fan_tag);
										$('.group_user_row_'+srcData.get_detail[i].user_id+' span[is_fan="true"]').attr('class','pasctrl');
									}
								}
								if (user_id == srcData.get_detail[i].user_id) {
									$('span.tot-balance').html('$'+srcData.get_detail[i].total_balance);
								}
								if (user_id == srcData.post_userid && srcData.post_userid != srcData.get_detail[i].user_id) {
									$('span.tot-balance').html('$'+postData.leaved_total_balance);
								}
							}
							var count = 0;
							if (srcData.get_updated_detail.length>0) {
								if (srcData.is_full_play == 'no') {
									$('.live_lby_ready_timer .timer').remove();
									$('.bet_reported').remove();
									$('.bet_report').remove();
								}
								for (var i = srcData.get_updated_detail.length - 1; i >= 0; i--) {
									if (user_id == srcData.get_updated_detail[i].user_id) {
										$('.tot-balance').html('$'+srcData.get_updated_detail[i].total_balance);
										count = 1;
									}
								}
								if (count == 0) {
									$('.join_lby_game_btn_div>div>lable.join_lobby_btn').css('pointer-events','all').html('Join Table').removeClass('already_joined').addClass('join_lby_game_btn').css('cursor','pointer');
								}
							} else {
								$('.join_lby_game_btn_div>div>lable.join_lobby_btn').css('pointer-events','all').html('Join Table').removeClass('already_joined').addClass('join_lby_game_btn').css('cursor','pointer');
							}

							var data = {
								'remaining_table':srcData.remaining_table,
								'full_table':srcData.full_table,
								'is_full_play':srcData.is_full_play,
								'post_userid':srcData.post_userid,
								'action': 'lobby_join_button_allow',
					        	'event': 'bottom_choose_tables_update'
							};
							commonappend(data);
							if (user_id == srcData.post_userid) {
								$('.join_wtng_btn').show();
							}
							if (srcData.is_full_play == 'yes') {
								$('.join_lby_game_btn.join_lobby_btn').remove();
							}				

							$('.play_game').change(function(){
								if($('.play_game').prop('checked')){
									$('#lobby_accept').show();
								}
				    		});
							
							is_recieved = 0;
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {}
				});
			}
		}
	}
	if (data.event == 'change_stream_status_data') {
		var winlocation = window.location.href.split('#')[0].toLowerCase();
		if (base_url+'livelobby/'+data.lobby_id.toLowerCase() == winlocation) {

			var lobby_id = $('input[name="lobby_id"]').val();
			if (data.table_cat != null && data.table_cat != undefined && data.table_cat != '') {
				var table_direction = (data.table_cat.toLowerCase() == 'right') ? 'right' : 'left';
			}

			// var table_direction = data.table_cat.toLowerCase();
			if (data.streamer_id == data.is_creator) {
				var cls = 'streamer_center_box';
				table_direction = 'center';
			}
			// var table_direction = 'center';
			var tbl_streamer_item = $('.streamer_'+table_direction+'_box .streamer_item');
			(tbl_streamer_item != undefined && tbl_streamer_item.length >= 5) ? $('.streamer_'+table_direction+'_box .show_more_navigation').show() : $('.streamer_'+table_direction+'_box .show_more_navigation').hide();
			if (data.updated_data == 'enable') {
		  		$('img#stream_'+data.streamer_id).attr({"src" : base_url+'assets/frontend/images/live_stream_on.png', "stream_click_status" : 'disable', "data-original-title" : 'Click to Stop. <br/> Live Lobby Stream'});

			  	if ($('.team_stream .live_streamer_'+data.streamer_id).length) {
			  		if (data.table_cat != null && data.table_cat != undefined && data.table_cat != '') {
			  			$(".team_stream .live_streamer_"+data.streamer_id).appendTo($(".streamer_"+data.table_cat.toLowerCase()+"_box .team_stream"));
			  		}
			  	} else {
			  		var streamer_append = '<div class="streamer_item text-left live_streamer_'+data.streamer_id+'" id="live_streamer_'+data.streamer_id+'" '+((tbl_streamer_item != undefined && tbl_streamer_item.length >= 5) ? 'style="display : none;"' : 'style="display : block;"')+'><label for="'+table_direction+'div'+data.streamer_id+'"><input type="checkbox" id="'+table_direction+'div'+data.streamer_id+'" stream-id="stream_channel_'+data.streamer_id+'" stream_type="'+data.streamer_type+'" stream-channel_name="'+data.streamer_channel+'" stream-streamer_name="'+data.fan_tag+'"> '+data.fan_tag+'</label></div>';
					$('.streamer_item.live_streamer_'+data.streamer_id).remove();
			    	$('.streamer_'+data.table_cat.toLowerCase()+'_box .team_stream').append(streamer_append);
			    	(data.streamer_type == 1) ? $('.select_stream_chat .select_chat').append('<option value="'+data.streamer_id+'" id="streamchat_'+data.streamer_id+'" stream-channel_name="'+data.streamer_channel+'" stream-streamer_name="'+data.fan_tag+'">'+data.fan_tag+'</option>') : '';
			  	}

			  	var live_streamchannel_name = $('.text-'+table_direction+'.streamer_channel .stream_iframe');
			  	var sess_user_id = $('input[name="user_id"]').val();

				var disable_class = "send_tip_btn";
				var subsclass = "subscribe_button commonmodalclick"; 
				var dis_class = "";
				if (sess_user_id != data.streamer_id) {
					// $('.streamer_item #'+table_direction+'div'+data.streamer_id).attr({'disabled':'','readonly':''});
				} else {
					disable_class = "tip_disable";
					subsclass = 'not_allow';
					dis_class = "disabled";
				}

				if (data.streamer_id == data.is_creator) {
					var subscribed_plan = '';
					var payment_id = '';
					for (var i = 0; i < data.subscribedlist.length; i++) {
						if (data.subscribedlist[i].user_from == data.streamer_id) {
							subscribed_plan = data.subscribedlist[i].plan_detail;
							payment_id = data.subscribedlist[i].payment_id;
						}
					}
					var postData_chat = { 
						'user_id':sess_user_id,
						'lobby_id':lobby_id,
						'group_bet_id':data.group_bet_id,
						't_streamerchat':data.streamer_id,
						'streamer_name' : data.fan_tag,
						'stream_channel' : data.streamer_channel,
						'stream_type' : data.streamer_type,
						'check': 'check',
					};
					send_streamchat_status(postData_chat);
					
					// Default stream append
					var res = {
						'follower_count': data.follower_count,
						'is_followd' : data.is_followd,
						'is_subscribed' : data.is_subscribed,
						'streamer_id':data.streamer_id,
						'table_direction':table_direction,
						'fan_tag':data.fan_tag,
						'streamer_channel':data.streamer_channel,
						'stream_type' : data.streamer_type,
						'subsclass':subsclass,
						'streamer_name':data.streamer_name,
						'streamer_img':data.streamer_img,
						'plan_subscribed':subscribed_plan,
						'payment_id':payment_id,
						'disable_class':disable_class,
						'dis_class':dis_class,
						'action':'add_new_stream',
				 		'event':'reset_stream'
				 	}
				 	commonappend(res);
				}
			} else {
			  	$('img#stream_'+data.streamer_id).attr({'src':base_url+'assets/frontend/images/live_stream_off.png','stream_click_status':'enable','data-original-title':'Click to Start. <br/> Live Lobby Stream'});
			  	$('.streamer_list_box #live_streamer_'+data.streamer_id+', .'+data.streamer_id+'_live-stream').remove();
			  	$('.select_stream_chat .select_chat option#streamchat_'+data.streamer_id).remove();

			  	(table_direction == 'center') ? $('.default_stream_img').removeClass('hide').addClass('show') : '';

			  	var defaultselected_chat = $('.select_stream_chat .select_chat option:selected').val();
			  	if (defaultselected_chat != undefined && defaultselected_chat != '' && defaultselected_chat == data.streamer_id) {
			  		$('.select_stream_chat .select_chat option:first').attr("selected","selected");
			  		$('.chatstreamer').html('<div class="stream_chat_img"><img src="'+base_url+'assets/frontend/images/stream_chat_box_transperent.png" alt="Smiley face"></div>');
			  	}
			  	if (data.streamer_id.length>0) {
			  		var postData = {
			  			'user_id': data.streamer_id
			  		};
					$.ajax({
						url: base_url+'livelobby/get_change_stream_status',
						type: "post",
						data: postData,
						success: function(srcData){
							srcData = $.parseJSON(srcData);
							if ($('.chatstreamer').attr('t_streamerchat') == srcData.user_id) {
				        		$('.chatstreamer').removeAttr('t_streamerchat');
				        		$('.stream_chat_img').remove();
				        		$('.chatstreamer .stream_chat_img').remove();
				        		$('.chatstreamer').html('');
				        		$('.chatstreamer').html('<div class="stream_chat_img"><img src="'+base_url+'/assets/frontend/images/stream_chat_box_transperent.png" alt="Smiley face"></div>');
				        	}
				        }
				    });
				}
			}
		} else {
			if (data.updated_data == 'enable' && data.is_creator == data.streamer_id) {
				var attr_arr = {
					'data-key' : data.streamer_channel,
				};
				$('video[data-user_id="'+data.streamer_id+'"]').each(function(e){
					$(this).attr('data-is_record','0');
					$(this).prop('muted', true);
					var skey = data.streamer_channel;
			        var burl = $(this).attr('data-base_url');
    				var rid = $(this).attr('data-random_id');
			        $(this).attr(attr_arr);
					var stream_arr = {
				       'stream_key' : skey,
				       'base_url' : burl,
				       'random_id' : rid,
				    };
				    stream_launch(stream_arr);

				    if($(this).attr('data-is_record') != '0'){
				    	recordstream($(this)[0]);
				    }
				});
			}
		}
		// $('.streamer_list_box .streamer_item input[type="checkbox"]').click(function () {
		//   var id = $(this).attr('id');
		//   var stream_channel = $(this).attr('stream-channel_name');
		//   var streamer_name = $(this).attr('stream-streamer_name');
		//   var stream_type = $(this).attr('stream_type');
		//   var stream_id = $(this).attr('stream_id');
		//   var split = id.split('div');
		//   var table_cat = split[0];
		//   var user_id = split[1];
		//   var sess_user_id = $('input[name="user_id"]').val();
		//   var disable_class = "send_tip_btn";
		//   var subsclass = "subscribe_button commonmodalclick"; 
		//   var disclass = '';
		//   if ($(this).is(":checked")) {
		//   	if (sess_user_id == user_id) {
		// 	  	disable_class = "tip_disable";
		// 			subsclass = 'not_allow';
		// 			disclass = 'disabled';
		// 		}
		// 		if ($('#'+table_cat+'div'+user_id+'stream').length == 0) {
		// 			var rid = generateRandomId();
		// 			var lobby_id = $('input[name="lobby_id"]').val();
		// 			var group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
		// 			var stream_url = (stream_type == 1) ? 'https://player.twitch.tv/?channel='+stream_channel+'&parent='+$(location).attr('hostname') : base_url+'live_stream?channel='+stream_channel+'&user_id='+user_id+'&lobby_id='+lobby_id+'&group_bet_id='+group_bet_id;
		// 			// connection.openOrJoin(stream_channel, function(isRoomExist, roomid) {
		// 			// 	if(isRoomExist) {
		// 		 //          connection.sdpConstraints.mandatory = {
		// 		 //              OfferToReceiveAudio: true,
		// 		 //              OfferToReceiveVideo: true
		// 		 //          };
		// 		 //        }
		// 		 //    });

		// 			var append_data = '<div id="'+table_cat+'div'+user_id+'stream" class="stream_iframe">';
		// 			append_data += (stream_type == 1) ? '<iframe src="'+stream_url+'" height="300" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>' : '<div style="height: 300px;width: 100%;box-shadow: 0px 0px 3px 0px #ff5000;" id="'+rid+'"><div class="stream_error" class="alert alert-danger" style="display: none;"></div><video id="videojs-flvjs-player" autoplay class="obs_streaming video-js vjs-default-skin" muted="muted" controls style="width: 100%; height: 100%;" poster="'+base_url+'assets/frontend/images/stream_offline_img.jpeg" preload="auto" data-key="'+stream_channel+'" data-random_id="'+rid+'" data-base_url="'+base_url+'" data-user_id="'+user_id+'" data-lobby_id="'+lobby_id+'" data-group_bet_id="'+group_bet_id+'"></video></div>';
		// 			// append_data += '<iframe src="https://player.twitch.tv/?channel='+stream_channel+'&parent='+$(location).attr('hostname')+'" height="300" width="100%" frameborder="<frameborder>" scrolling="<scrolling>" allowfullscreen="<allow full screen>" muted="<true>"></iframe>';

		// 			// <div class="row streamer_channel_detail"><div class="col-sm-4 text-left">
		// 			// <a class="lobby_btn">Follow</a></div><div class="col-sm-4 text-center"><h3 align="center" class="live_lobby_h3">'+streamer_name+'</h3></div><div class="col-sm-4 text-right">
		// 			// append_data += '<a class="lobby_btn '+subsclass+'" streamer_name="'+streamer_name+'" streamer_id="'+user_id+'" streamer_img="">Subscribe</a> <a class="lobby_btn '+disable_class+'" streamer_name="'+streamer_name+'" streamer_id="'+user_id+'">TIP</a></div></div></div>';
		// 			append_data += '<div class="row streamer_channel_detail"><div class="col-sm-4 text-left">';
		// 			append_data += '<a data-id="'+user_id+'" class="lobby_btn social_follow '+disclass+'" data-html="true" data-toggle="tooltip" data-placement="bottom" data-original-title="Follow">';
		// 			append_data += '<img src="'+base_url+'assets/frontend/images/follow.png"></a>';
		// 			append_data += '<sup><span class="cunt badge b_color" style="margin-left: 5px;">0</span></sup>';
		// 			append_data += '<a class="lobby_btn" href="'+base_url+'Store/'+user_id+'" target="_blank" style="margin-left: -1.3em" data-html="true" data-toggle="tooltip" data-placement="bottom" data-original-title="Store"><img src="'+base_url+'assets/frontend/images/store.png"></a>';
		// 			append_data += '<a href="'+base_url+'user/user_profile/?fans_id='+user_id+'" target="_blank" style="margin-left: 0.3em" data-html="true" data-toggle="tooltip" data-placement="bottom" data-original-title="Profile"><img src="'+base_url+'upload/profile_img/'+data.streamer_img+'" class="profile_img"></a>';
		// 			append_data += '</div><div class="col-sm-4 text-right"><h3 align="center" class="live_lobby_h3">'+data.fan_tag+'</h3></div><div class="col-sm-4 text-right">';
		// 			append_data += '<a class="lobby_btn '+data.disable_class+'" streamer_name="'+data.fan_tag+'" streamer_id="'+user_id+'" streamer_img="" plan_subscribed="" data-html="true" data-toggle="tooltip" data-placement="bottom" data-original-title="Subscribe">';
		// 			append_data += '<img src="'+base_url+'assets/frontend/images/subscription11.png">';
		// 			append_data += '</a>';
		// 			append_data += '<a class="lobby_btn '+data.disable_class+'" streamer_name="'+data.fan_tag+'" streamer_id="'+user_id+'" style="margin-left: 0.3em" data-html="true" data-toggle="tooltip" data-placement="bottom" data-original-title="Tip"><img src="'+base_url+'assets/frontend/images/tip2.png"></a>';

		// 			$('.streamer_channel.text-'+table_cat).append(append_data);
		// 		}
		//   } else {
		//   	console.log(table_cat.toLowerCase()+'== center');
		//   	(table_cat.toLowerCase() == 'center') ? $('.default_stream_img').removeClass('hide').addClass('show') : '';
		//     $('.streamer_channel.text-'+table_cat+' #'+table_cat+'div'+user_id+'stream').remove();
		//   }
		// });
	}
	if (data.event == 'toss_coin') {
	  $('#coin').removeClass().addClass('disabled');
	  setTimeout(function(){
	    $('#coin').addClass(data.addClass);
	  }, 100);
	  setTimeout(function(){
	    $('#coin').removeClass('disabled');
	  }, 3000);	
	}
	($('#chooseGameimage').length) ? chooseGameimage() : '';
	$('.social_follow, .bet_report, #coin, .streamer_list_box .streamer_item input[type="checkbox"], .commonmodal button, .lobby_grp_msgbox .attachfile input[name="attachfile"], #send-message-to-lobby_group, .join_lby_game_btn, .stream_icon, .play_game, #leave_lobby .yes, .leave_lobby, .select_head select[name="select_head"], .lobby_grp_msgbox_message-input, .subscribe-info .plan_info.plan_info_btn, .subscription_plan-info_div .subscbe_plan_detail .close_plan_detail').unbind('click dblclick change keyup keypress');
	
	$('.bet_report').on('click', function(){
		$('#bet_report_modal').show();
		return false;
	});
	$('#coin').on('click', function(){
		var lobby_custom_name = $('input[name="lobby_custom_name"]').val();
	  	var flipResult = Math.floor(Math.random() * 10);
	  	var addClass = (flipResult % 2 === 0) ? 'heads' : 'tails';
	  	if (is_cretr == 'yes') {
			socket_data = {
				'lobby_id': lobby_custom_name,
				'addClass': addClass,
				'on':'cmn_event'
			};
			socket.emit( 'sendrealtimedata', socket_data );
		} else {
			$('#coin').removeClass().addClass('disabled');
			setTimeout(function(){ $('#coin').addClass(addClass); }, 100);
			setTimeout(function(){ $('#coin').removeClass('disabled'); }, 3000);
	  	}
	});
	$('.send_tip_btn').click(function () {
		var streamer_id = $(this).attr('streamer_id');
		$('#send_tip_amount .team_member #activeusr_'+streamer_id+' input[type="checkbox"]').prop('checked',true);
		$('#send_tip_amount .team_member input[type="checkbox"]').not($('#send_tip_amount .team_member #activeusr_'+streamer_id+' input[type="checkbox"]')).removeAttr('checked');
		$('#send_tip_amount .team_member .form-group').removeClass('active_usr');
		$('#send_tip_amount .team_member .form-group#activeusr_'+streamer_id).addClass('active_usr');
		$('#send_tip_amount').show();
	});
	$('#send_tip .no, #send_tip .close').click(function(){
		$('#send_tip').hide();
	});
	$('input:radio[name="select_membership"]').change(function() {
		if($(this).is(":checked")){
			var checked_value = $(this).data('membership_amount');
			var current_mplan = $("#current_plan").data('current_mplan');
			if(current_mplan){
				if(checked_value < current_mplan){
				    $(".membership_discount").text("You can not downgrade your membership");
				}else{
					$(".membership_discount").text("");
				}
			}else{
				$(".membership_discount").text("");
			}
			return true;
		}
	});
	$(".monthly_membership_plan").submit(function(e){
		var submitBtn = $(document.activeElement).val();
		if(submitBtn == 1){
			if($('input[type=radio][name=select_membership]:checked').length == 0){
	    		$(".membership_discount").text("Please select membership");
	    		return false;
    		}else{
    			var checked_value = $('input[type=radio][name=select_membership]:checked').data('membership_amount');
				var current_mplan = $("#current_plan").data('current_mplan'); 
				if(current_mplan){
					if(checked_value < current_mplan){
					    return false;
					}
				}
    		}
		}
    	return true;
  	});
  	$(".yearly_membership_plan").submit(function(e){
  		var submitBtn = $(document.activeElement).val();
  		if(submitBtn == 1){
			if($('input[type=radio][name=year_select_membership]:checked').length == 0){
		    	$(".year_membership_discount").text("Please select membership");
		    	return false;
	    	}else{
	    		var checked_value = $('input[type=radio][name=year_select_membership]:checked').data('membership_amount');
				var current_mplan = $("#current_plan").data('current_mplan'); 
				if(current_mplan){
					if(checked_value < current_mplan){
						return false;
					}
				}
	    	}
    	}
    	return true;
  	});
  	$('input:radio[name="year_select_membership"]').change(function() {
		if($(this).is(":checked")){
			var checked_value = $(this).data('membership_amount');
			var current_mplan = $("#current_plan").data('current_mplan');
			if(current_mplan){
				if(checked_value < current_mplan){
				    $(".year_membership_discount").text("You can not downgrade your membership");
				}else{
					$(".year_membership_discount").text("");
				}
			}else{
				$(".year_membership_discount").text("");
			}
			return true;
		}
	});
	$('#event_ticket_update_btn').on('click' ,function(){
		var lobby_id = $(this).attr('target_table');
		$.ajax({
		   	url : base_url +'points/updateTicket',
		    data: {id:lobby_id},
		    type: 'POST',
		    success: function(res) {
		    	var res = $.parseJSON(res);
		    	var page_refresh = res.refresh;  
				var lobby_custom_name = res.lobby_custom_name;        
		      	if (page_refresh == 'yes') {
		        	window.location.href = base_url+'livelobby/'+lobby_custom_name;
		      	} else {
		         	window.location.href = base_url+'points/getUserEvents';
		      	}
	    	}
	  	});
	});
	$('.social_follow').click(function(e){
	  var follow_id = $(this).attr('data-id');
	  var curr = $(this);
	  $.ajax({
	    type: "POST",  
	    url: base_url + "streamsetting/getFollowUserDetails",
	    data: { follow_id: follow_id },
	    beforeSend: function(){ loader_show(); },
	    success: function (result) {
	      var result = $.parseJSON(result);
	      loader_hide();
	      var current_follower = $(curr).closest('.streamer_channel_detail').find('.cunt').html();
	      if (result.status == 1) {
	        var followercunt = 1;
	        followercunt += parseInt(current_follower);
	        $(curr).attr('data-original-title', 'Following').find('img').attr('src',base_url+'assets/frontend/images/follower.png');
	        $(curr).closest('.streamer_channel_detail').find('.cunt').html(followercunt);
	        ($(curr).html().includes("Follow") == true) ? $(curr).html($(curr).html().replace('Follow', 'Following')) : '';
	        // $(curr).html('following<span class="cunt" style="margin-left: 5px;">'+followercunt+'</span>');
	      }
	      if (result.data != undefined && result.data.length > 0) {    
	        var senddata = result.social_link_div;
	        var modaltitle = 'Social Media';
	        var add_class_in_modal_data = 'social_link_modal';
	        var data = {
	          'add_class_in_modal_data':add_class_in_modal_data,
	          'modal_title': modaltitle,
	          'senddata':senddata,
	          'tag':'cmninfo_shw'
	        }
	        commonshow(data);
	      }
	    },
	  });
	});
	$('.join_lby_game_btn').on('click', function() {
    var lobby_password_model, lobby_id, lby_pw;
    lobby_password_model = $('#lobby_password_model').html();
    lobby_id = $('input[name="lobby_id"]').val();
    if (lobby_password_model != undefined && lobby_id != '' && lobby_id != undefined) {
    	$('#lobby_password_model').show();
    	$('#lobby_password_model form .lby_pw').on('keyup keypress', function(event) {
				if (event.which == 13) {
					event.preventDefault();
					$('#lobby_password_model form #submit_lobby').click();
				}
			});
			$('#lobby_password_model form .btn-submit').click(function() {
	        lby_pw = $('input[name="lby_pw"]').val();
	        if (lby_pw != '' && lby_pw != undefined) {
	            var postData = {
	              'lobby_password':lby_pw,
	              'lobby_id':lobby_id
	            }
	            lobby_password_submit(postData);
	        } else {
	          $('.lobby_password_error').html('Please Enter Password');
	          $('.lobby_password_error').show();
	        }
	    });
		} else {
			$('#join_lobby').show();
		}
	});
	$('#join_lobby .close').click(function() {
	    $('#join_lobby').hide();
	    $('.play_game').removeAttr('checked');
	});
	$('.modal .close, .custmodal .no, .custmodal .close, #lobby_password_model .close, .right_table_first_div.is_draggable .right_table, .left_table_first_div.is_draggable .left_table, .wtnglstlistdiv.left_tbl_wtnglst .waitingusr, .wtnglstlistdiv.right_tbl_wtnglst .waitingusr').click(function() {
		$('.modal, .custmodal, #lobby_password_model').hide();
	});

	$('.select_head select[name="select_head"]').on('change', function(){
		var lobby_id, group_bet_id, user_id, table_cat, socket_data;
		group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
		lobby_id = $('input[name="lobby_id"]').val();
		user_id = $(this).val();
		table_cat = $(this).attr('table_cat');
		var postData = {
	        'lobby_id': lobby_id,
	        'group_bet_id': group_bet_id,
	        'user_id': user_id,
	        'table_cat': table_cat
	    };
	    if (user_id !='' && user_id != undefined) {
	    	$.ajax({
	    		type: 'post',
	    		url: base_url+'livelobby/select_head_table',
	    		data: postData,
	    		success: function(res){
	    			var res = $.parseJSON(res);
	    			var lobby_custom_name = $('input[name="lobby_custom_name"]').val();
	    			socket_data = {
				    	'lobby_id': lobby_custom_name,
				    	'group_bet_id': res.group_bet_id,
				    	'head_id': res.head_id,
							'ready_up': res.ready_up,
							'table_cat': res.table_cat,
							'on':'change_head_of_table'
						};
						socket.emit( 'sendrealtimedata', socket_data );
	    		}
	    	});
	    }
	});

	$('.leave_lobby').on('click dblclick', function(){
		var leave_id = $(this).attr('usr_id');
		var lobby_id = $('input[name="lobby_id"]').val();
		var group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
		var is_creator = $(this).attr('lby_crtr');
		var is_controller = $(this).attr('lby_cntrlr');
		var player_tag_name = $(this).attr('player_tag_name');
		var self_user = $(this).attr('self');
		var everyone_readup = $(this).attr('everyone_readup');
		var leave_from_waitinlist = 'no';
		if ($(this).attr('leave_from_waitinlist') != undefined && $(this).attr('leave_from_waitinlist').length) {
			leave_from_waitinlist = 'yes';
		}
		leave_lobby(leave_id,lobby_id,group_bet_id,is_creator,is_controller,player_tag_name,self_user,everyone_readup,leave_from_waitinlist);
		return false;
	});
	$('.giveaway_setting_form input[name="datetime_giveaway"]').focus(function() {
		$('.giveaway_setting_form .datetime_giveaway_error_div').hide();
	});
	$('.change_giveaway_settings form').submit(function(e) {
	  e.preventDefault();
	  var registration_close_time = $(this).find('input[name="registration_close_time"]').val(), datetime_giveaway = $('input[name="datetime_giveaway"]').val();
	  if (new Date(registration_close_time) >= new Date(datetime_giveaway)) {
	  	$('.giveaway_setting_form .datetime_giveaway_error_div').show();
	  	return false;
	  }
		var lobby_id = $('input[name="lobby_id"]').val(), lobby_custom_name = $('input[name="lobby_custom_name"]').val();
		$.ajax({
	    url: base_url+'giveaway/giveaway_setting/'+lobby_id,
	    dataType: "JSON",
      data: new FormData(this),
      processData: false,
      contentType: false,
	    type: 'POST',
	    beforeSend: function(){ loader_show(); },
	    success: function(res) {
	    	loader_hide();
	    	if (res.updated != 0) {
	      	var socket_data = {
						'lobby_id':lobby_custom_name,
						'id' : lobby_id,
						'on':'reset_giveaway',
					};
					socket.emit('sendrealtimedata', socket_data);
	    	} else {
	    		window.location.reload();
	    	}
	    }
		});
	});
	$('.set_banner_home_stream .modal_data .yes').click(function() {
		var is_stream_banner = ($('#is_stream_banner').is(":checked")) ? 'on' : 'off';
		var lobby_id = $('input[name="lobby_id"]').val();
		var group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
		var user_id = $('input[name="user_id"]').val();

		var postData = {
			'lobby_id': lobby_id,
			'group_bet_id': group_bet_id,
			'is_stream_banner': is_stream_banner,
			'user_id': user_id
		};
		$.ajax({
			url: base_url + 'livelobby/update_stream_banner',
			type: "post",
			data: postData,
			beforeSend: function(){ loader_show(); },
			success: function() { location.reload(); }
		});
	});
	$('#leave_lobby .no, #leave_lobby .close').on('click dblclick', function(){
		$('#leave_lobby').hide();
		$('#leave_lobby .yes').removeAttr('leave_id');
		$('#leave_lobby .yes').removeAttr('creator');
	});

	if($('.only_datepicker').length > 0){
		$('.only_datepicker').datetimepicker({viewMode: 'days',format: 'MM/DD/YYYY',maxDate: moment(cudttime).format("MM/DD/YYYY")}).keydown(false).val("");
	}

	if($('.only_timepicker').length > 0){
		$('.only_timepicker').datetimepicker({format: 'hh:mm'}).keydown(false).val("");
	}
	
	// var settimezone_ga = new Date();
	// var timeZoneOffset = -4*60;

	// settimezone_ga.setMinutes(settimezone_ga.getMinutes() + settimezone_ga.getTimezoneOffset() + timeZoneOffset);
	if($('.only_datetimepicker_giveaway').length > 0){
		$('.only_datetimepicker_giveaway').datetimepicker({viewMode: 'days',format: 'MM/DD/YYYY HH:mm'}).keydown(false);
	}
	if($('.only_timepicker_giveaway').length > 0){
		$('.only_timepicker_giveaway').datetimepicker({format: 'HH:mm'}).keydown(false);
	}
	if($('.datetimepicker').length > 0){
		$('.datetimepicker').datetimepicker({viewMode: 'days', format: 'MM/DD/YYYY HH:mm'}).keydown(false);
	}
	
	// show stream 
	$('.streamer_list_box .streamer_item input[type="checkbox"]').click(function () {
	  var id = $(this).attr('id');
	  var stream_channel = $(this).attr('stream-channel_name');
	  var streamer_name = $(this).attr('stream-streamer_name');
	  var stream_type = $(this).attr('stream_type');
	  var sess_user_id = $('input[name="user_id"]').val();
	  var group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
	  var lobby_id = $('input[name="lobby_id"]').val();
	  var stream_id = $(this).attr('stream_id');
	  var split = id.split('div');
	  var table_cat = split[0];																			
	  var user_id = split[1];
	  var disable_class = (sess_user_id == user_id) ? "tip_disable" : "send_tip_btn";
	  if ($(this).is(":checked")) {
	  	var postData = { 
			// 'user_id':sess_user_id,
			'user_id':user_id,
			'lobby_id':lobby_id,
			'group_bet_id':group_bet_id,
			't_streamerchat':user_id,
			'streamer_name':streamer_name,
			'stream_channel':stream_channel,
			'table_cat':table_cat,
			'stream_type':stream_type,
			'disable_class':disable_class,
			'check': 'check'
		};
		send_live_stream_show_status(postData);
	  } else {
	  	var postData = { 
	  		// 'user_id':sess_user_id,
  			'user_id':user_id,
			'lobby_id':lobby_id,
			'group_bet_id':group_bet_id,
			't_streamerchat':user_id,
			'streamer_name':streamer_name,
			'stream_channel':stream_channel,
			'table_cat':table_cat,
			'stream_type':stream_type,
			'disable_class':disable_class,
			'check': 'uncheck'
	 	};
	 	send_live_stream_show_status(postData);
	  }

	});
	$('#leave_lobby .yes').on('click dblclick', function(){
		var group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
	    var lobby_id = $('input[name="lobby_id"]').val();
	    var leave_from_waitinlist = $(this).attr('leave_from_waitinlist');
		var controller = $(this).attr('controller');
		var leave_id = $(this).attr('leave_id');
		var creator = $(this).attr('creator');
		var autoloss = $(this).attr('autoloss');
	    var postData = { 
			'creator':creator,
			'controller':controller,
	    	'leave_id':leave_id,
	    	'lobby_id':lobby_id,
	    	'group_bet_id':group_bet_id,
	    	'autoloss':autoloss,
	    	'leave_from_waitinlist':leave_from_waitinlist
	    };
	    send_leave_report(postData)
	});

	$('.choose_table_cat').click(function(){
		var table_cat = $(this).attr('table_cat');
	    var lobby_id = $('#lobby_id').val();
	    var group_bet_id = $('#group_bet_id').val();
	    var price_add_decimal = $('input[name="lobby_accept_price"]').val();
	    var postData = {
	    	'lobby_id': lobby_id,
	    	'price_add_decimal': price_add_decimal,
			'group_bet_id':group_bet_id,
			'table_cat':table_cat,
	    }
    	get_lobby_join_group_data(postData);
	});

	$('.pasctrl').on('click dblclick', function () {
		var user_id = $(this).attr('id');
		var user_to_fan_tag = $(this).attr('fan_tag');
		var passctrlh4 = 'You Are Passing Control to '+user_to_fan_tag;
		$('#change_pass_control h4.passctrlh4').html(passctrlh4);
		$('#change_pass_control a.yes').attr('pass_to',user_id);
		$('#change_pass_control').show();
	});

	$('.stream_icon').on('click dblclick', function () {
		var stream_status = $(this).attr('stream_click_status');
		var stream_type = $(this).attr('stream_type');
		$('#change_stream_modal a.yes').attr({'change_status':stream_status, 'stream_type':stream_type});
		$('#change_stream_modal').show();
		return false;
	});
	$('.stream_settings .file_label .edit_file').on('click', function () {
		var file_name = $(this).attr('file_name');
		var original_name = $(this).attr('original_name');
		var file_type = $(this).attr('file_type');
		var ext = file_name.split('.').pop();
		file_name = file_name.replace("."+ext, "");
		
		var senddata = '<form action="'+base_url+'Streamsetting/change_file_name?type='+file_type+'" method="post"><div class="col-sm-12"><div class="form-group"><div class="col-lg-2">File name </div><div class="col-lg-9"><input type="text" placeholder="file_name" class="form-control" name="edit_file_name" value="'+file_name+'" required="required" pattern="[A-Za-z0-9-_]+" title="Allow letters, numbers, hyphens, underscores Only"><span id="demo" style="margin-left: -194px;color: red;"></span></div><div class="col-lg-1">.'+ext+'</div></div><div class="form-group"><div class="input-group col-lg-12"><input type="hidden" name="old_file_name" value="'+original_name+'"></div></div><div class="form-group"><div class="col-lg-12"><input type="submit" class="cmn_btn" value="Submit"></div></form>';
		var modaltitle = 'change file name';
		var add_class_in_modal_data = 'edit_file_name';
		var data = {
			'add_class_in_modal_data':add_class_in_modal_data,
			'modal_title': modaltitle,
			'senddata':senddata,
			'tag':'change_file_name_stream_setting'
		}
		commonshow(data);
		return false;
	});
	$('.update_ticket').on('click',function(){
		var lobby_id = $(this).attr('id');
		var senddata = '<div class="col-sm-12"><h3><a class="button yes" id="event_ticket_update_btn" target_table="'+lobby_id+'">OK</a><a class="button no">Cancel</a></h3></div>';
		var modaltitle = 'Are you sure you want to Upgrade to the Full Event?';
		var add_class_in_modal_data = 'update_event_ticket';
		var data = {
			'add_class_in_modal_data':add_class_in_modal_data,
			'modal_title': modaltitle,
			'senddata':senddata,
			'tag':'update_event_ticket'
		}
		commonshow(data);
	});
	$('.join_wtng_btn').on('click', function () {
		var stream_status = $(this).attr('tg_table');
		var senddata = '<div class="col-sm-12"><h3><a class="button yes" target_table="'+stream_status+'">OK</a><a class="button no">Cancel</a></h3></div>';
		var modaltitle = 'Are you sure you want to Join <span class="waiting_list-table_sides">'+stream_status.toLowerCase()+'</span> Side waiting List?';
		var add_class_in_modal_data = 'join_wtnglst';
		var data = {
			'add_class_in_modal_data':add_class_in_modal_data,
			'modal_title': modaltitle,
			'senddata':senddata,
			'tag':'join_bottom_wtnglst'
		}
		commonshow(data);
	});

	$('#cancel_ordr').click(function () {
		var order_id = $(this).attr('order_id');
		if (order_id.length) {
			var postdata = { 'order_id': order_id };
			$.ajax({
			    url : base_url +'order/cancel_order',
			    data: postdata,
			    type: 'POST',
			    beforeSend: function(){
			    	loader_show();
			    },
			    success: function(res) {
			    	var res = $.parseJSON(res);
		    		window.location.href = res.redirect;
			    }
			});
		}
	});

	$('.commonmodal .subscribe_main_div button[name="subscribe"]').click(function(){
		var user_to, user_from, subscribe_plan_amount, subscribe_plan_id, subscribe_plan_name, postdata;
		user_to = $('.commonmodal .subscribe_main_div .image').attr('user_to');
		user_from = $('input[name="user_id"]').val();
		subscribe_plan_amount = $(this).val();
		subscribe_plan_id = $(this).attr('plan_id');
		subscribe_plan_name = $(this).attr('plan_name');
		var subscribed_plan = '';
		var payment_id = '';
		var lobby_id = $('input[name="lobby_id"]').val();
		if ($('.subid_'+user_to+'.alrdy_subscribe').length) {
			subscribed_plan = $('.subid_'+user_to+'.alrdy_subscribe').attr('subsplan');
			payment_id = $('.subid_'+user_to+'.alrdy_subscribe').attr('payment_id');
		}
		postdata = {
			'user_to':user_to,
			'user_from':user_from,
			'subscribe_amount':subscribe_plan_amount,
			'subscribe_plan':subscribe_plan_id,
			'subscribed_plan':subscribed_plan,
			'redirect_lobby_id': lobby_id,
			'payment_id':payment_id
		};
		$.ajax({
		    url : base_url +'Subscribeuser/Set_player_subscription_plan',
		    data: postdata,
		    type: 'POST',
		    beforeSend: function(){
		    	loader_show();
		    	// $('.subscribeloader.'+subscribe_plan_name).html('<i class="fa fa-circle-o-notch fa-spin"></i>');
		    },
		    success: function(res) {
		    	loader_hide();
		    	var data = $.parseJSON(res);		    	
		    	$('.subscribe_error').html('');
		    	if (data.errors) {
		    		for (var i = 0; i < data.errors.Errors.length; i++) {
		    			$('.subscribe_error').append(data.errors.Errors[i].L_LONGMESSAGE);
		    		}
		    	}
		    	if (data.redirect.length) {
		    		window.location = data.redirect;
		    	}
		    }
		}); 	
	});
	$('.lobby_event_modal .lobby_event_detail form input[name="submit"]').click(function() {
		var lobby_id = $('input[name="lobby_id"]').val(), group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
		var change_lobby_event = ($('input[name="change_lobby_event"]').is(":checked")) ? 1 : 0;
		var lobby_event_amount = $('input[name="lobby_event_amount"]').val();
		var change_lobby_spector = ($('input[name="change_lobby_spector"]').is(":checked")) ? 1 : 0;
		var lobby_spector_amount = $('input[name="lobby_spector_amount"]').val();
		var event_old_img = $('input[name="event_old_img"]').val();
		var event_image = $('.choose_eventdiv.choose_active input[name="event_image"]').prop('files');
		var lobby_custom_name = $('input[name="lobby_custom_name"]').val();
		var eve_title = $('.event_edit_detail input[name="event_title"]').val();
		var eve_description = $('.event_edit_detail textarea[name="event_description"]').val();
		var eve_start_date = $('.event_edit_detail input[name="startdate"]').val();
		var eve_end_date = $('.event_edit_detail input[name="enddate"]').val();
		var event_key = $('input[name="event_key"]').val();
		if (change_lobby_event == '1') {
			if (lobby_event_amount == '') {
				alert('Please enter Event Price');
				return false;
			} else if (event_image != undefined && event_image.length == 0) {
				alert('Please choose image');
				return false;
			} else if (eve_title == '') {
				alert('Please enter event title');
				return false;
			} else if (eve_description == '') {
				alert('Please enter event description');
				return false;
			} else if (eve_start_date == '') {
				alert('Please enter event start date');
				return false;
			} else if (eve_end_date == '') {
				alert('Please enter event end date');
				return false;
			}
		} 
		if (change_lobby_spector == '1') {
			if (lobby_spector_amount == '') {
				alert('Please enter Spectator Price');
				return false;
			} 
			if (change_lobby_event == 1 && lobby_event_amount < lobby_spector_amount) {
				alert('Event Ticket price is '+lobby_event_amount+' you have to enter Spectator price less than Event price');
				return false;
			}
		}
		event_image = (event_image != undefined) ? event_image[0] : '';
		var formData = new FormData();
			formData.append('event_image', event_image);
			formData.append('lobby_id',lobby_id);
			formData.append('change_lobby_event',change_lobby_event);
			formData.append('lobby_event_amount',lobby_event_amount);
			formData.append('change_lobby_spector',change_lobby_spector);
			formData.append('lobby_spector_amount',lobby_spector_amount);
			formData.append('event_old_img',event_old_img);
			formData.append('lobby_custom_name',lobby_custom_name);

			formData.append('event_title',eve_title);
			formData.append('event_key',event_key);
			formData.append('event_description',eve_description);
			formData.append('event_start_date',eve_start_date);
			formData.append('event_end_date',eve_end_date);

			xhr = new XMLHttpRequest();
	    xhr.open( 'POST', base_url +'livelobby/change_event', true );
	    xhr.responseType = 'json';
	    xhr.onreadystatechange = function (srcData) {
	    	srcData = this.response;
	    	if (srcData.error != undefined && srcData.error != '') {
	    		location.reload();
	    		return false;
	    	}
	    	if (srcData !='' && srcData !=null && srcData.error == '') {
	    		var socket_data = {
			    	'lobby_id': srcData.lobby_custom_name,
			    	'id' : srcData.lobbyid,
			    	'on':'change_lby_detail',
				};
				socket.emit('sendrealtimedata', socket_data );
	    	}
	    };
	    xhr.send( formData );

		
		// action="'+base_url+'livelobby/change_event"
	});
	$('.play_game').change(function(){
		if($('.play_game').prop('checked')){
			$('#lobby_accept').show();
		}
	});

	if($('[data-toggle="tooltip"]').length > 0){
		$('[data-toggle="tooltip"]').tooltip();
	}

	$('.team_title input[type="checkbox"]').on('click', function(){
		var name  = $(this).attr('name');
		var table = name.split('_');
		var is_checked = $(this).is(":checked");
		var prop_checked = $(this).prop('checked');
		table = table[0];
		var data = {
    	'name':name,
    	'table': table,
    	'is_checked':is_checked,
    	'prop_checked':prop_checked,
    	'event': 'whole_team_tip_check'
		};
		tip_change_event(data);
	});

	$('.team_member input[type="checkbox"]').on('click', function(){
		var check_userid = $(this).attr('id');
		var is_checked = $(this).is(":checked");
		var prop_checked = $(this).prop('checked');
		var data = {
    	'is_checked':is_checked,
    	'prop_checked':prop_checked,
    	'check_userid':check_userid,
    	'event': 'team_member_tip_check'
		};
		tip_change_member(data);
	});

	$('#bet_report_modal .close, #bet_report_modal .no').click(function(){
		$('#bet_report_modal').hide();
	});

	$('#bet_report_modal_status .close').click(function(){
		$('#bet_report_modal').hide();
		$('#bet_report_modal_status').hide();
	});

	if ($("#message-inner").length) {
		$("#message-inner").scrollTop($("#message-inner")[0].scrollHeight);
	}

	$('.subscribe_button').click(function(){
		var streamer_name = $(this).attr('streamer_name');
		var img = $(this).attr('streamer_img');
		var user_to = $(this).attr('streamer_id');
		subscribe_btn_append(streamer_name,img,user_to);
	});

	if ($("#cmn_grp_msgbox_message-input.attach_emojioneicon, #lobby_grp_msgbox_message-input.attach_emojioneicon").length) {
		$("#cmn_grp_msgbox_message-input.attach_emojioneicon, #lobby_grp_msgbox_message-input.attach_emojioneicon").emojioneArea({
			container: "#emojionearea_display_div",
			search: false,
			saveEmojisAs: "image",	
			buttonTitle: "Insert Emoji",
			recentEmojis: false,
			searchPosition: "bottom",
			autocomplete: true,
			events: {
				keypress: function (editor, event) {
					var message = $('.cmn_grp_msgbox_message-input .emojionearea-editor, .lobby_grp_msgbox_message-input .emojionearea-editor').html();
					message = message.replace(/<div><br><\/div>/g,'');
					message = message.replace(/<br>/g,'')
					message = message.replace(/<div><\/div>/g,'');
					(message.length == 0) ? $('.cmn_grp_msgbox_message-input .emojionearea-editor, .lobby_grp_msgbox_message-input .emojionearea-editor').html('') : '';
					if (event.keyCode == 13 && !event.shiftKey) {
						$("#send-message-to-lobby_group, .send-message-to-cmn_group").click();
					} else {
						(event.keyCode === $.ui.keyCode.TAB) ? event.preventDefault() : '';
						$(".attach_emojioneicon .emojionearea-editor").autocomplete({
							minLength: minWordLength,
							source: function(request, response) {
								var term = extractLast($(".cmn_grp_msgbox_message-input .emojionearea-editor, .lobby_grp_msgbox_message-input .emojionearea-editor").html());
								(term != undefined) ? response($.ui.autocomplete.filter(availableTags, term)) :  $('.ui-autocomplete').hide();
							},
							focus: function() {
								return false;
							},
			        select: function(event, ui) {
			        	var html = $(".cmn_grp_msgbox_message-input .emojionearea-editor, .lobby_grp_msgbox_message-input .emojionearea-editor").html();
			        	var lastword = html.split(" ").pop(), lastword_nbsp = html.split("&nbsp;").pop();
			        	if (/^[^@^]*$/.test(lastword_nbsp) == false) {
			        		lastword_nbsp = '@'+lastword_nbsp.split('@').pop();
			        		html = html.replace(lastword_nbsp,'');
			        	} else if (/^[^@^]*$/.test(lastword) == false) {
			        		lastword = '@'+lastword.split('@').pop();
			        		html = html.replace(lastword,'');
			        	}
			        	var terms = split(ui.item.value);
			        	$(".cmn_grp_msgbox_message-input .emojionearea-editor, .lobby_grp_msgbox_message-input .emojionearea-editor").html(html+'<b>'+terms+'</b> ').focusEnd();
			        	return false;
			        }
				    });
					}
				}
			}
		});
	}
	$('.lobby_grp_msgbox .attachfile input[name="attachfile"]').change(function(event) {
		var filename = [];
		var images = '';
		var lobby_id, message, user_id, imgs, is_admin;
		imgs = $('.lobby_grp_msgbox .attachfile_div .attachfile input[type="file"]').prop('files');
		message = $('.lobby_grp_msgbox_message-input .emojionearea-editor').html();
		lobby_id = $('input[name="lobby_id"]').val();
		user_id = $('input[name="user_id"]').val();
		sendMessageOptLobbygrp(imgs,message,lobby_id,is_admin,user_id);
	});

	$("#send-message-to-lobby_group").click(function(){
		var lobby_id, message, user_id, imgs;
		imgs = $('.attachfile_div .attachfile input[type="file"]').prop('files');
		message = $('.lobby_grp_msgbox_message-input .emojionearea-editor').html();
		lobby_id = $('input[name="lobby_id"]').val();
		user_id = $('input[name="user_id"]').val();
		is_admin = $('input[name="is_admin"]').val();
	    message = message.replace(/<div><br><\/div>/g,'');
	    message = message.replace(/<br>/g,'')
	    message = message.replace(/<div><\/div>/g,'');
	    if (message.length != 0) {
			sendMessageToLobbygrp(lobby_id,message,user_id,is_admin);
		} else if (imgs!= undefined && imgs.length>0) {
			var formData = new FormData();
			for (var i = imgs.length - 1; i >= 0; i--) {
				formData.append( 'file[]', imgs[i]);
			}
			formData.append('lobby_id',lobby_id);
			formData.append('user_id',user_id);
			sendMessageToLobbygrp(lobby_id,message,user_id,is_admin,formData);
		} else {
			$('.lobby_grp_msgbox_message-input .emojionearea-editor').html('');
		}
	});	

	$('.subscribe-info .plan_info.plan_info_btn').click(function() {
		var plan_image = $(this).attr('plan_image');
		var plan_desc = $(this).attr('plan_desc');
		var plan_title = $(this).attr('plan_title');
		var plan_termscond = $(this).attr('plan_termscond');
		var data = {
			'plan_title': plan_title,
			'plan_image': plan_image,
			'plan_desc': plan_desc,
			'plan_termscond': plan_termscond
		};
		subscribe_user_plan_view(data)
	})

	$('#edit_tracking_id_submit').on('click', function(){
		$('#edit_tracking_id_form').submit();
	})

	$('.live_stream_div .lobby_page_heading .lobby_btn.make_lobby_event').click(function () {
		var already_member = $(this).attr('already_member');
		var lobby_id = $('input[name="lobby_id"]').val(), group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
		var check_event = $(this).attr('is_event');
		var event_old_img = $(this).attr('event_image');
		var event_price = $(this).attr('event_price');
		var check_spectator = $(this).attr('is_spectator');
		var spectate_price = $(this).attr('spectate_price');
		var event_title = $(this).attr('event_title');
		var event_description = $(this).attr('event_description');
		var event_start_date = $(this).attr('event_start_date');
		var event_end_date = $(this).attr('event_end_date');
		var lobby_custom_name = $('input[name="lobby_custom_name"]').val();
		var event_key = $(this).attr('event_key');
		if (already_member == 'yes') {
			var arr = {
				'lobby_id':lobby_id,
				'lobby_custom_name':lobby_custom_name,
				'group_bet_id':group_bet_id,
				'check_event':check_event,
				'check_spectator':check_spectator,
				'event_old_img':event_old_img,
				'event_price':event_price,
				'spectate_price':spectate_price,
				'event_title':event_title,
				'event_description':event_description,
				'event_start_date':event_start_date,
				'event_key' : event_key,
				'event_end_date':event_end_date,
			}
			make_lobby_event(arr);
		} else {
			var arr = {
				'lobby_id':lobby_id,
				'lobby_custom_name':lobby_custom_name,
			}
			show_membeership_purchase(arr);
		}
	});
	$('.lobby_event_modal .lobby_event_detaila form').submit(function () {
		var data = $(this).serializeArray();
		//page refresh on add event 
		var socket_data = {
	    	'lobby_id': data[0].value,
			'on':'change_lby_detail'
		};
		socket.emit( 'sendrealtimedata', socket_data );
	})
	$('.lobby_event_detail #game_img .change_event_img').click(function(){
		$('.lobby_event_detail #game_img').hide().removeClass('choose_active');
    	$('.lobby_event_detail .choose_eventdiv').show().addClass('choose_active');
    	chooseGameimage();
	});
	$('.lobby_event_detail form input[name="change_lobby_event"], .lobby_event_detail form input[name="change_lobby_spector"]').click(function() {
		var tg_div = $(this).attr('target_div');
		if ($(this).is(":checked")) {
			$('.'+tg_div).show();
			$('.'+tg_div+' input[name="lobby_event_amount"], .'+tg_div+' input[name="lobby_spector_amount"]').attr('required','');
		} else {
			$('.'+tg_div).hide();
			$('.'+tg_div+' input[name="lobby_event_amount"], .'+tg_div+' input[name="lobby_spector_amount"] ').removeAttr('required');
		}
		chooseGameimage();
	});
	$('.subscription_plan-info_div .subscbe_plan_detail .close_plan_detail').click(function() {
		$('.subscribe_main_div, .custmodal.commonmodal .close, .modal_h1_msg').show();
		$('.subscription_plan-info_div, .modal_h2_subscription_plan-info').remove();
	});
	$('.membership_switch_box #membership_duration').click(function () {
		$('.membership_list_div').not($('.membership_list_div.current_membership_plan')).hide();
		($(this).is(":checked")) ? $('.membership_list_div#year_membership_list').show() : $('.membership_list_div#month_membership_list').show();
	});
	creator_dragndrop(crtr,user_id);
	show_more_strmr();

	// onVideoEnd() used on stream end with obs
	$('.obs_streaming').on('ended',function(){
		var uid = $(this).attr('data-user_id');
		var lid = $(this).attr('data-lobby_id');
		var gbid = $(this).attr('data-group_bet_id');
		var skey = $(this).attr('data-key');
		var frm = new FormData();
	    frm.append('key', skey);
	    frm.append('streamer', uid);
	    frm.append('lobby', lid);
	    setTimeout(event=>{
	    	$.ajax({
	        	type: 'post',
	          	url: base_url+'Streamsetting/removeStreams',
	          	data: frm,
	          	processData: false,
	          	contentType: false,
	          	success: function(response) { 
	            	endLiveStream(uid,lid,gbid);
	            	console.log('removeStream Data',response)
	          	},error: function(jqXHR, textStatus, errorMessage) {
	            	console.log('Error:' + JSON.stringify(errorMessage));
	          	}
	        });
	    },5000);
	});
	
	  // function onVideoEnd(stream_key,user_id,lobby_id='',group_bet_id=''){
	  // 	alert('hi');
	  //     var frm = new FormData();
	  //     frm.append('key', stream_key);
	  //     frm.append('streamer', user_id);
	  //     frm.append('lobby', lobby_id);
	  //     setTimeout(event=>{
	  //       $.ajax({
	  //         type: 'post',
	  //         url: base_url+'Streamsetting/removeStreams',
	  //         data: frm,
	  //         processData: false,
	  //         contentType: false,
	  //         success: function(response) { 
	  //           endLiveStream(user_id,lobby_id,group_bet_id);
	  //           console.log('removeStream Data',response)
	  //         },error: function(jqXHR, textStatus, errorMessage) {
	  //           console.log('Error:' + JSON.stringify(errorMessage));
	  //         }
	  //       });
	  //     },5000);
	  // }

	// function stream_initializing() {
	// 	alert('test');
	// }
	$('#end_trnmnt_btn').unbind().click(function () {
	    var lobby_id = $('input[name="lobby_id"]').val();
	    var user_id = $('input[name="user_id"]').val();
	    var senddata = '<div class="col-sm-12">';
	    senddata += '</div><div class="col-sm-12"><h3><a class="button yes confirm_end_tournament" id="confirm_end_tournament">OK</a><a class="button no">Cancel</a></h3></div>';
	    var modaltitle = 'Are you sure to end tournament';
	    var add_class_in_modal_data = 'end_tournament_action_cls';
	    var data = {
	      'add_class_in_modal_data':add_class_in_modal_data,
	      'modal_title': modaltitle,
	      'senddata':senddata,
	      'tag':'confirm_modal'
	    }
	    commonshow(data);
	});
	
	if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1 && $('input[name="lobby_custom_name"]').val() != undefined && base_url+'livelobby/'+$('input[name="lobby_custom_name"]').val().toLowerCase() == window.location.href.toLowerCase()){
      var video = $(document).find('video');
      if(video.length > 0){
        var promise = video[0].play();
        if (promise !== undefined) {
          promise.then(_ => {
          }).catch(error => {
            var cookie_status = $.cookie('autoplay_turned_off_ck');
            if(cookie_status != 1){
              var senddata = '<div class="col-sm-12">You need to click the unblock button on the right of browser bar to unblock Autoplay Video and Audio';
              senddata += '</div><div class="col-sm-12"><h3><a class="button yes autoplay_turned_off" id="autoplay_turned_off">Keep Turned Off</a><a class="button no">Cancel</a></h3></div>';
              var modaltitle = 'Allow Autoplay Video';
              var add_class_in_modal_data = 'autoplay_turned_off_modal';
              var data = {
                'add_class_in_modal_data':add_class_in_modal_data,
                'modal_title': modaltitle,
                'senddata':senddata,
                'tag':'confirm_modal'
              }
              commonshow(data);
            }
          });
        }
      }
    }

	$('#autoplay_turned_off').on('click', function(){
      $.cookie('autoplay_turned_off_ck',1);
      $('.autoplay_turned_off_modal').hide();
    })
}

$('.obs_streaming').each(function(){
	var skey = $(this).attr('data-key');
    var burl = $(this).attr('data-base_url');
    var rid = $(this).attr('data-random_id');
    var stream_arr = {
       'stream_key' : skey,
       'base_url' : burl,
       'random_id' : rid,
    };
    stream_launch(stream_arr);

    if($(this).attr('data-is_record') != '0'){
    	recordstream($(this)[0]);
    }
 })

function generateRandomId() {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < 8; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;                        // Return shuffled string
}

function stream_launch(stream_arr) {
	if (flvjs.isSupported() && stream_arr.stream_key != undefined) {
		var videoElement = $('#'+stream_arr.random_id+' .obs_streaming').get(0);
		if (videoElement) {
			var url2 = protocol+server_name;
			var streaming_port = '8100';
			(server_name == 'localhost') ? streaming_port = '8000': '';
			var stream_url = url2+':'+streaming_port+'/live/'+stream_arr.stream_key+'.flv'
			var flvPlayer = flvjs.createPlayer({
				"type": 'flv',
				"url": stream_url,
				"isLive": true,
			});
			flvPlayer.attachMediaElement(videoElement);
			flvPlayer.load();
			$(videoElement).show();
			// videojs('videojs-flvjs-player', {
			//     techOrder: ['html5', 'flvjs'],
			//     flvjs: {
			//     	mediaDataSource: {
			//     		isLive: true,
			//         	cors: true,
			//         	withCredentials: false,
			//         	controls: false
			//       	},
			//       	// config: {},
			//     },
			// });
			// ($(videoElement).attr('autoplay')) ? flvPlayer.play() : '';
			var b = setInterval(() => {
				// console.log('readyState', 'random_id =>>>> '+stream_arr.random_id+'     readyState=>>>> '+videoElement.readyState);
				 if(videoElement.readyState >= 3){
		            //This block of code is triggered when the video is loaded
		            //your code goes here
		        	console.log('video play...');
		        	$(videoElement).attr('controls','');
		        	flvPlayer.play();
		            //stop checking every half second
		            clearInterval(b);
		        } else {
		        	$(videoElement).removeAttr('controls');
		        }
		    },3000);
		}
	} else {
	  	$('#'+stream_arr.random_id+' .stream_error').html('Your browser is not supported FLV player').show();
	}
}

function show_more_strmr() {
	//left data 
	var leftarticleLimit = 5, leftcurrentPage = 1, rightarticleLimit = 5, rightcurrentPage = 1;
	// var $currentPage = $('.current-page');
	var $left_articles = $('.streamer_left_box .team_stream .streamer_item'),
	$right_articles = $('.streamer_right_box .team_stream .streamer_item');
	var lefttotalArticles = $left_articles.length, righttotalArticles = $right_articles.length;
	var lefttotalPages = (lefttotalArticles / leftarticleLimit), righttotalPages = (righttotalArticles / rightarticleLimit);
	$left_articles.hide();
	// $currentPage.html(currentPage);
	changeleftPage();
	$('.show_more_navigation .left_next').on('click', function(){
		if (leftcurrentPage >= lefttotalPages) {
	    	// leftcurrentPage = 1;
	  	} else {
	    	leftcurrentPage++;
	  	}
	  	changeleftPage();
	});
	$('.show_more_navigation .left_prev').on('click', function(){
	  	leftcurrentPage--;
	  	if (leftcurrentPage < 1) {
	    	leftcurrentPage = 1;
	  	}
	  	changeleftPage();
	});

	function changeleftPage() {
	  	$left_articles.hide();
	  	for(var i = leftarticleLimit; i >=  1; i--){
	      $($left_articles[leftcurrentPage * leftarticleLimit - i]).show();  
	  	}
	}

	$right_articles.hide();
	// $currentPage.html(currentPage);
	changerightPage();
	$('.show_more_navigation .right_next').on('click', function(){
		if (rightcurrentPage >= righttotalPages) {
			// rightcurrentPage = 1;
		} else {
			rightcurrentPage++;
		}
		changerightPage();
	});
	$('.show_more_navigation .right_prev').on('click', function(){
		rightcurrentPage--;
		if (rightcurrentPage < 1) {
			rightcurrentPage = 1;
		}
		changerightPage();
	});
	function changerightPage() {
	  	$right_articles.hide();
		for(var i = rightarticleLimit; i >=  1; i--){
			$($right_articles[rightcurrentPage * rightarticleLimit - i]).show();  
		}
	}
}

function creator_dragndrop(crtr,user_id) {
	if (crtr == user_id) {
		$(".wtnglstlistdiv.left_tbl_wtnglst .waitingusr, .wtnglstlistdiv.right_tbl_wtnglst .waitingusr").not('.wtnglstlistdiv.left_tbl_wtnglst .waitingusr[is_eighteen="no"], .wtnglstlistdiv.right_tbl_wtnglst .waitingusr[is_eighteen="no"]').draggable({
			cursor: "pointer",
			revert: true,
			stop: function( event, ui ) {}
		});
		$(".right_table_first_div.is_draggable .right_table, .left_table_first_div.is_draggable .left_table").draggable({
			cursor: "pointer",
			revert: true,
			start: function( event, ui ) {
				$(this).addClass('tables_user');
				(crtr != $(this).find('a').attr('usr_id')) ? $('.crtrdiv').addClass('drpcttr') : '';
			},
			stop: function( event, ui ) {
				$('.crtrdiv').removeClass('drpcttr');
			}
		});
		$(".left_table_first_div.left_table_droppable, .right_table_first_div.right_table_droppable").droppable({
			activeClass: "drpactive",
			drop: function( event, ui ) {
				var droppable_table = $(this).attr('droppable_table');
				var tables_users_class = ui.draggable.attr('class');
				var check_class = droppable_table.toLowerCase()+'_table';
				if (!ui.draggable.hasClass(check_class)) {
					adduser_in_lobby_bydroped(ui,droppable_table);
					ui.draggable.remove();
			    	$(this).removeClass( "ui-dropped_moving" );
				}
			},
			over: function ( event, ui ) {
			  $(this).addClass( "ui-dropped_moving" );
			},
			activate: function( event, ui ) {
				$(this).addClass( "ui-dropped_moving" );
			},
			deactivate: function( event, ui ) {
				$(this).removeClass( "ui-dropped_moving" );
	        	$('.crtrdiv').removeClass('drpcttr');
			},
			out: function ( event, ui ) {
			  $(this).removeClass( "ui-state-highlight" );
			}
		});
	    $(".live_lby_waiting_list .right_tbl_wtnglst, .live_lby_waiting_list .left_tbl_wtnglst").droppable({
			activeClass: "drpactive",
			drop: function( event, ui ) {
				var table_cat = $(this).attr('wtnglst_table');;
				var user_id = ui.draggable.find('a.leave_lobby').attr('usr_id');
				var fan_tag = ui.draggable.find('span[is_fan="true"]').html();
				var waiting_class = ui.draggable.attr('class');
				if (waiting_class.indexOf('waitingusr') != -1) {
					user_id = ui.draggable.attr('user_id');
					fan_tag = ui.draggable.attr('fan_tag');
					table_cat = $(this).attr('wtnglst_table').toLowerCase();
				}
				var data = {
					'fan_tag':fan_tag,
					'table_cat':table_cat,
					'user_id':user_id,
				};
				moveuser_to_lobby_waitinglist(data);
				ui.draggable.remove();
		    	$(this).removeClass( "ui-dropped_moving" );
			},
			over: function ( event, ui ) {
			  $(this).addClass( "ui-dropped_moving" );
			},
			out: function ( event, ui ) {
			  $(this).addClass( "ui-dropped_moving" );
			}
	    });
	    $('.crtrdiv .drp_lbycrtr').droppable({
	    	activeClass: "drpactive",
			drop: function( event, ui ) {
				var user_id = ui.draggable.find('.pasctrl').attr('id');
				if (user_id != undefined) {
					var user_to_fan_tag = ui.draggable.find('.pasctrl').html();
					var passctrlh4 = 'You Are Passing Control to '+user_to_fan_tag;
					$('#change_pass_control h4.passctrlh4').html(passctrlh4);
					$('#change_pass_control a.yes').attr('pass_to',user_id);
					$('#change_pass_control').show();
				}
			},
			over: function ( event, ui ) {
				$(this).addClass( "ui-dropped_moving" );
			},
			out: function ( event, ui ) {
				$(this).addClass( "ui-dropped_moving" );
			}
	    });
	}
}
function adduser_in_lobby_bydroped(ui,droppable_table) {
	var lobby_id = $('input[name="lobby_id"]').val();
	var group_bet_id = $('input[name="lobbygroup_bet_id"]').val();

	var tables_users_class = ui.draggable.attr('class');
	var dragged_waitingid = ui.draggable.attr("id");
	var dragged_userid = ui.draggable.attr("user_id");
	var dragged_fantag = ui.draggable.attr("fan_tag");

	if (tables_users_class.indexOf('tables_user') != -1) {
		dragged_waitingid = '';
		dragged_userid = ui.draggable.find('a.leave_lobby').attr('usr_id');
		dragged_fantag = ui.draggable.find('span[is_fan="true"]').html();
	}

	var price_add_decimal = $('input[name="lobby_accept_price"]').val();
	var joinlby_data = {
		'lobby_id' : lobby_id,
		'group_bet_id' : group_bet_id,
		'price_add_decimal' : price_add_decimal,
		'table_cat' : droppable_table,
		'dragged_waitingid' : dragged_waitingid,
		'dragged_userid' : dragged_userid,
		'dragged_fantag' : dragged_fantag
	}
	get_lobby_join_group_data(joinlby_data);
}
function moveuser_to_lobby_waitinglist(args) {
	var lobby_id = $('input[name="lobby_id"]').val();
	var group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
	var user_id = args.user_id;
	var table_cat = args.table_cat;
	var fan_tag = args.fan_tag;
	var price_add_decimal = $('input[name="lobby_accept_price"]').val();
	var sendusrlby_data = {
		'lobby_id' : lobby_id,
		'group_bet_id' : group_bet_id,
		'price_add_decimal' : price_add_decimal,
		'table_cat' : table_cat,
		'user_id' : user_id,
		'fan_tag' : fan_tag
	}
	senduser_to_waitinglist(sendusrlby_data);
}
function tip_change_event(data) {
	if (data.is_checked == true) {
		$('.table-'+data.table+' .team_member .form-group').addClass('active_usr');
		$('.table-'+data.table+' .team_member .form-group input[type="checkbox"]').prop('checked', data.prop_checked);
		var amount = $('.team_donate_bottom_div input[name="tip_amount_input"]').val();
		tip_change_am(amount);
		$('#send_tip_amount .alert.tip_amount_error').html('');					
	} else {
		$('.table-'+data.table+' .team_member .form-group input[type="checkbox"]').removeAttr('checked');
		$('.table-'+data.table+' .team_member .form-group').removeClass('active_usr');
		$('.team_member .form-group .tipamount').html('');
		var amount = $('.team_donate_bottom_div input[name="tip_amount_input"]').val();
		tip_change_am(amount);			
	}
}
function tip_change_member(data) {
	if (data.is_checked == true) {
		$('.team_member #activeusr_'+data.check_userid).addClass('active_usr');
		$('#send_tip_amount .alert.tip_amount_error').html('');
	} else {
		$('.team_member #activeusr_'+data.check_userid).removeClass('active_usr').find('.tipamount').html('');
	}
	var amount = $('.team_donate_bottom_div input[name="tip_amount_input"]').val();
	tip_change_am(amount);
}
function tip_change_am(amount){
	if (amount.length != 0) {
		$('.team_member input[type="checkbox"]:checked').val(amount);
		$('.team_member .active_usr .tipamount').html('$'+amount);
		var get_total = $('.team_member .active_usr .tipamount').html();
		var sum = 0;
	    $('.team_member input[type="checkbox"]:checked').each(function() {
	    	sum += +this.value;
	    });
    	$('.team_donate_bottom_div input[name="tip_total_amount_input"]').val(parseFloat(sum).toFixed(2));
  	} else {
		$('.team_member .active_usr .tipamount').html('');
		$('.team_member input[type="checkbox"]:checked').val(0);
		$('.team_donate_bottom_div input[name="tip_total_amount_input"]').val('');
	}
}
function send_tip_to_team(postdata){
	var socket_data;
	$.ajax({
	    url: base_url +'livelobby/send_tip_to_team',
	   	type:"post",
	    contentType:false,
	    data:postdata,
	    processData:false,
	    cache:false,
	    dataType:"json",
	    success: function(res) {
	    	var res = $.parseJSON(res);
	    	var is_admin = $('input[name="is_admin"]').val();
	    	if (res.balance_status == 'less') {
	    		alert("You do not have enough credits to place a challenge. Please Proceed to Purchase tab for more.");
	        	window.location.href = base_url+'points/buypoints';
	    	} else {
		        var sender_id = res.sender_id;
		        var get_grp_with_balance = res.get_grp_with_balance;
		        var tip_fireworks_sound = $('#tip_fireworks_sound').val(); 
		        $('#send_tip_amount').hide();
				socket_data = {
					'get_grp_with_balance': get_grp_with_balance,
					'sender_id': sender_id,
					'sender_balance' : res.update_balance,
					'on':'get_team_tip_data'
				};

				if (tip_fireworks_sound.length && tip_fireworks_sound != undefined && tip_fireworks_sound != '') {
					socket_data.tip_fireworks_sound = tip_fireworks_sound;
				}
				socket.emit('sendrealtimedata', socket_data );
				var data = {
					'msg_data': res.lobby_get_chat,
					'name': res.fan_tag,
					'session_id': res.session_id,
					'is_admin': is_admin,
					'getmytip_sound':res.getmytip_sound,
					'tag': 'send_socket_msg_lobby_grp'
				};
				commonshow(data);
			}
		}
	}); 
}

function pass_ctrl(data){
var socket_data;
   $.ajax({
          url : base_url +'livelobby/change_pass_control',
          data: data,
          type: 'POST',
          success: function(res) {
          	var res = $.parseJSON(res);
          	var page_refresh = res.refresh;
			var lobby_custom_name = $('input[name="lobby_custom_name"]').val();
			if (page_refresh == 'yes') {
				window.location.href = base_url+'livelobby/'+lobby_custom_name;
			} else {
				socket_data = {
					'lobby_id':lobby_custom_name,
					'new_lobby_id':res.new_lobby_id,
					'group_bet_id':res.group_bet_id,
					'pass_from':res.pass_from,
					'pass_to':res.pass_to,
					'on':'pass_ctrl',
				};
				socket.emit('sendrealtimedata', socket_data );
				$('#change_pass_control').hide();
			}
          }
      }); 
}
function lobby_password_submit(data){
var socket_data;
   $.ajax({
          url : base_url +'livelobby/auth_lobby_password',
          data: data,
          type: 'POST',
          success: function(res) {
          	var table_cat;
            var res = $.parseJSON(res);
            if (res.password_auth=='auth_success') {
            	$('.lobby_password_error').hide();
            	$('#lobby_password_model').hide();
            	$('#join_lobby').show();	
            } else {
            	$('.lobby_password_error').html('Please Enter Correct Password');
				$('.lobby_password_error').show();
				return false;
            }
          }
      }); 
}

function get_updated_lobby_report(data){
var lobby_id, group_bet_id, user_id, table_cat;
if (base_url+'livelobby/'+data.lobby_id.toLowerCase() == window.location.href.toLowerCase()) {
	var postData = {
		'ref_id':data.ref_id,
		'lobby_id': data.lobby_id,
		'group_bet_id': data.group_bet_id,
		'table_cat': data.table_cat,
		'user_id': data.user_id
	};
	$.ajax({
		url: base_url+'livelobby/getGetReportTimer',
		type: "post",
		data: postData,
		success: function(srcData) {
			srcData = $.parseJSON(srcData);
			if(srcData.ref_id==0) {
				$('.group_user_row_'+data.user_id+' button.bet_report').remove();
				$('.group_user_row_'+data.user_id).append('<button class="button text-'+data.table_cat+' bet_reported">Reported <i class="fa fa-check"></i></button>');
				var get_timer = '<div class="timer reporttimer" data-seconds-left="'+srcData.get_remaining_timer+'"></div>';
				$('.live_lby_ready_timer').html('Ready '+get_timer);
				if(data.win_lose_status == 'win'){
					$('#won_div').html('<div class="report_error">Player has already reported Won..!!<br>Please choose another option.</div>');
				} else if(data.win_lose_status == 'lose') {
					$('#lost_div').html('<div class="report_error">Player has already reported Lose..!!<br>Please choose another option.</div>');
				}
				// else if(data.win_lose_status == 'admin'){
				// 	$('#admin_div').html('<div class="report_error">Player has already reported Admin..!!<br>Please choose another option.</div>');
				// }
				$('.live_lby_ready_timer .reporttimer').startTimer({
				  onComplete: function(element){
				    element.addClass('is-complete');
				    var lobby_id, group_bet_id;
				    group_bet_id = $('input[name="lobbygroup_bet_id"]').val();
						lobby_id = $('input[name="lobby_id"]').val();
				    
				    var postData = {
				    	'group_bet_id': group_bet_id,
				    	'lobby_id':lobby_id
				    };
				    $.ajax({
		          type: 'post',
		          url: base_url+'livelobby/report_timer_expire',
		          data: postData,
		          success: function(res){
		            var res = $.parseJSON(res);
		            window.location.href = base_url+'livelobby/'+res.lobby_id;
		          }
		        });
				  }
				});
			} else {
				window.location.href = base_url + 'livelobby/'+srcData.lobby_id;
			}
		}
	});	
}
}

if($('#message-inner').length>0){
$("#message-inner").scrollTop($("#message-inner")[0].scrollHeight);
}
/**
* Send message to lobby group
* 
* @return {[Void]}
*/
var minWordLength = 1;
function split(term) {
	if (term != undefined) {
		return term.split(' ');
	}
}
function extractLast(term) {
	if (term.indexOf('@') != -1) {
		return term.replace('<br>','').split('@')[1];
	}
}

$.fn.focusEnd = function() {
  $(this).focus();
  var tmp = $('<span />').appendTo($(this)),
      node = tmp.get(0),
      range = null,
      sel = null;

  if (document.selection) {
      range = document.body.createTextRange();
      range.moveToElementText(node);
      range.select();
  } else if (window.getSelection) {
      range = document.createRange();
      range.selectNode(node);
      sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(range);
  }
  tmp.remove();
  return this;
}

function sendMessageOptLobbygrp(imgs,message,lobby_id,is_admin,user_id) {
if (message!=undefined && message.length != 0) {
	if (message.indexOf('<div><br></div>') != -1) {
		message = message.replace(/<div><br><\/div>/g,'');
	}
	sendMessageToLobbygrp(lobby_id,message,user_id,is_admin);
} else if (imgs != undefined && imgs.length > 0) {
	var formData = new FormData();
	for (var i = imgs.length - 1; i >= 0; i--) {
		formData.append( 'file[]', imgs[i]);
	}
	formData.append('lobby_id',lobby_id);
	formData.append('user_id',user_id);
	sendMessageToLobbygrp(lobby_id,message,user_id,is_admin,formData);
}
}
function sendMessageToLobbygrp(lobby_id,message,user_id,is_admin,formData=''){
var socket_data, postData;
if (formData != undefined && formData !='') {
	xhr = new XMLHttpRequest();
    xhr.open( 'POST', base_url +'livelobby/send_message_to_lobby_grp', true );
    xhr.responseType = 'json';
    xhr.onreadystatechange = function (srcData) {
    	srcData = this.response;
    	if (srcData !='' && srcData !=null) {
			var data = {
				'msg_data': srcData.lobby_get_chat,
				'name': srcData.fan_tag,
				'session_id': srcData.session_id,
				'is_admin': is_admin,
				'tag': 'send_socket_msg_lobby_grp'
			};
			commonshow(data);
			$('.attachfile_div input[type="file"]').val('');
			formData.delete(lobby_id);
    	}
    };
    xhr.send( formData );
} else {
	postData = {
		'lobby_id': lobby_id,
		'message': message,
		'user_id': user_id
	};
	$.ajax({
		url : base_url +'livelobby/send_message_to_lobby_grp',
		type: "post",
		data: postData,
		success: function(srcData){
			srcData = $.parseJSON(srcData);
			var data = {
				'msg_data': srcData.lobby_get_chat,
				'name': srcData.fan_tag,
				'session_id': srcData.session_id,
				'is_admin': is_admin,
				'tag': 'send_socket_msg_lobby_grp'
			};
			commonshow(data);
			$('.live_lobby_chatbox_main_row .lobby_grp_msgbox_message-input .emojionearea-editor').html('');
			$('.attachfile_div input[type="file"]').val('');
		}
	});
}
}

/**
* Get single message
* @param array data 
* @return mixed
*/
function get_single_message(data){
var $message_container, $unread_l_contacts, $unread_r_contacts, 
	focused, $chatbox, $mainInbox, cn_id, user_to;
is_recieved = 1;

if(data.emit_user_name == 'admin')
{
	var postData = {
		'message_id': data.message_id,
		'user_id': data.emit_to,
		'user_to': data.emit_from,
		'ci': data.ci,
		'cs_key': 'xwb_get_single_message_at_admin_side'
	};
}
else if(data.emit_user_name == 'pvp')
{
	var postData = {
		'message_id': data.message_id,
		'user_id': data.emit_to,
		'user_to': data.emit_from,
		'ci': data.ci,
		'cs_key': 'xwb_get_pvp_single_message_at_frontide'
	};

}
else if(data.emit_user_name == 'pvpadmin')
{
	var postData = {
		'message_id': data.message_id,
		'user_id': data.emit_to,
		'user_to': data.emit_from,
		'ci': data.ci,
		'cs_key': 'xwb_get_single_message_at_admin_side'
	};
}

else if(data.emit_user_name == 'friend')
{
	var postData = {
		'message_id': data.message_id,
		'user_id': data.emit_to,
		'user_to': data.emit_from,
		'ci': data.ci,
		'cs_key': 'xwb_get_single_message_at_friend'
	};
}

else 
{
	var postData = {
		'message_id': data.message_id,
		'user_id': data.emit_to,
		'user_to': data.emit_from,
		'ci': data.ci,
		'cs_key': 'xwb_get_single_message_at_frontside'
	};
}


$csrf = $('.csrf_key');
csrf_name = $csrf.attr('name');
csrf_key = $csrf.val();
postData[csrf_name] = csrf_key;

$.ajax({
	url: window.location.pathname,
	type: "post",
	data: postData,
	success: function(srcData){
		srcData = $.parseJSON(srcData);
		$('input[name="'+csrf_name+'"]').val(srcData.csrf_key);

		if($(".message-inner").find('.no_messages')){
			$(".message-inner").find('.no_messages').remove();
		}
		if($(".message-inner").find('.no-more-messages')){
			$(".message-inner").find('.no-more-messages').remove();
		}
		

		if(srcData.cn_id == null){
			// if private conversation
			$message_container = $('#message-container[data-users="'+data.emit_from+'"], .cb-conversation-container[data-users="'+data.emit_from+'"]').find('.message-inner');
			$unread_l_contacts = $('#cs-ul-users li[data-user="'+data.emit_from+'"]'); // select left contacts
			$unread_r_contacts = $('#cs-ul-sideusers li[data-user="'+data.emit_from+'"]'); // select right contacts
		}else{
			// if group conversation
			$message_container = $('#message-container_pvp[data-cnid="'+srcData.cn_id+'"], .cb-conversation-container[data-cnid="'+srcData.cn_id+'"]').find('.message-inner');
			$unread_l_contacts = $('#cs-ul-users li[data-ci="'+srcData.cn_id+'"]'); // select left contacts
			$unread_r_contacts = $('#cs-ul-sideusers li[data-ci="'+srcData.cn_id+'"]'); // select right contacts
		}
		// console.log($message_container);
		// return false;
		$.each($message_container,function(i,v){
			$(v).append(srcData.row);
			$(v).scrollTop($(v)[0].scrollHeight);
		});


		if (($unread_l_contacts.length == 0 && $unread_r_contacts.length == 0) || ($unread_l_contacts.length == 0 && $unread_r_contacts.length == 1)){
			if($unread_r_contacts.length == 0)
				$("#cs-ul-sideusers").append(srcData.li_sideuser_html);
			if($unread_l_contacts.length == 0)
				$("#cs-ul-users").append(srcData.li_main_contact_html);
			if(srcData.cn_id != null)
				$('#cs-ul-sideusers li[data-ci="'+srcData.cn_id+'"]').trigger('click');
			else
				$('#cs-ul-sideusers li[data-user="'+srcData.user_to+'"]').trigger('click');

		}

		if(srcData.cn_id == null){
			$unread_l_contacts = $('#cs-ul-users li[data-user="'+data.emit_from+'"]'); // select left contacts
			$unread_r_contacts = $('#cs-ul-sideusers li[data-user="'+data.emit_from+'"]'); // select right contacts
		}else{
			$unread_l_contacts = $('#cs-ul-users li[data-ci="'+srcData.cn_id+'"]'); // select left contacts
			$unread_r_contacts = $('#cs-ul-sideusers li[data-ci="'+srcData.cn_id+'"]'); // select right contacts
		}

		$.each([$unread_l_contacts,$unread_r_contacts], function(i,v){
			if($('span.unread',$(v)).length==1){
				$('span.unread',$(v)).text(srcData.unreadCount);
			}else{
				$('a',$(v)).append('<span class="pull-right label label-danger unread">'+srcData.unreadCount+'</span>');
			}
		});

		focused = document.activeElement;
		$chatbox = $(focused).parents('.cb-conversation-container');
		$mainInbox = $(focused).parents('.xwb-message-container');

		if($chatbox.length==1){
			cn_id = $chatbox.attr('data-cnid');
			user_to = $chatbox.attr('data-users');
			markConversationRead(cn_id,user_to);
		}

		if($mainInbox.length==1){
			cn_id = $mainInbox.attr('data-cnid');
			user_to = $mainInbox.attr('data-users');
			markConversationRead(cn_id,user_to);	
		}

		// $("a[rel^='prettyPhoto']",$message_container).prettyPhoto({
		// 		deeplinking:false, 
		// 		social_tools:false
		// });

		is_recieved = 0;
	},
	error: function(XMLHttpRequest, textStatus, errorThrown) {}
});
}


/**
* Send message
*
* @param  {object} el
* @param  {String} message_type
* @param  {Array}  attachments
* @return {void}
*/
function sendMessage(el, message_type,attachments){
var editorID, data, users, socket_data;
if (typeof(attachments)==='undefined') attachments = [];
if (typeof(message_type)==='undefined') message_type = 'message';

editorID = 'message-input';

if(el.editorID!=undefined)
	editorID = el.editorID;

if($('#'+editorID).val() == '' && attachments.length == 0)
	return false;

data = $(".user_to",el.container).serializeArray();
data.push({'name':'user_id','value':$(".user_id",el.container).val()});
data.push({'name':'ci','value':$(".ci",el.container).val()});
data.push({'name':'message','value':$('#'+editorID).val()});
data.push({'name':'message_type','value':message_type});
data.push({'name':'attachments','value':attachments});
data.push({'name':'cs_key','value':'xwb_sendmessage'});

$csrf = $('.csrf_key');
csrf_name = $csrf.attr('name');
csrf_key = $csrf.val();
data.push({'name':csrf_name, 'value': csrf_key});

$.ajax({
	url: window.location.pathname,
	type: "post",
	data: data,
	success: function(data){
		data = $.parseJSON(data);
		$('input[name="'+csrf_name+'"]').val(data.csrf_key);

		if($(".message-inner",el.container).find('.no_messages')){
			$(".message-inner",el.container).find('.no_messages').remove();
		}
		$(".message-inner",el.container).append(data.row);

		$(".message-inner",el.container).scrollTop($(".message-inner",el.container)[0].scrollHeight);
		$(".message-input",el.container).prop('rows',4);

		$("a[rel^='prettyPhoto']").prettyPhoto({
			deeplinking:false, 
			social_tools:false
		});
		csDropzone = null;

		users = $($(".user_to",el.container)).map(function(){return $(this).val();}).get();

		socket_data = {
			 emit_from: socketUser,
			 emit_to: users,
			 message_id: data.message_id,
			 ci: $(".ci",el.container).val() 
		};
		// socket.on("recieved-message", function(data){
		// 	get_single_message(data);
		// 	$(".cs-audio").get(0).play();
		// });
		// socket.emit( 'send-message', socket_data );
		$('.message-input',el.container).data("emojioneArea").setText("");
	},
	error: function(XMLHttpRequest, textStatus, errorThrown) {}
});
}

/**
* Send message to admin
*
* @param  {object} el
* @param  {String} message_type
* @param  {Array}  attachments
* @return {void}
*/
function sendMessageToAdmin(el, message_type,attachments){

var editorID, data, users, socket_data, editorID_a;
if (typeof(attachments)==='undefined') attachments = [];
if (typeof(message_type)==='undefined') message_type = 'message';



editorID = 'message-input.message-input-frontend-side-footer';


if(el.editorID!=undefined)
	editorID = el.editorID;

if($('#'+editorID).val() == '' && attachments.length == 0)
	return false;

data = $(".user_to",el.container).serializeArray();
data.push({'name':'user_id','value':$(".user_id",el.container).val()});
data.push({'name':'ci','value':$(".ci",el.container).val()});
data.push({'name':'message','value':$('#'+editorID).val()});
data.push({'name':'message_type','value':message_type});
data.push({'name':'attachments','value':attachments});
data.push({'name':'cs_key','value':'xwb_send_message_to_admin'});



$csrf = $('.csrf_key');
csrf_name = $csrf.attr('name');
csrf_key = $csrf.val();
data.push({'name':csrf_name, 'value': csrf_key});
$.ajax({
	url: window.location.pathname,
	type: "post",
	data: data,
	success: function(data){
		data = $.parseJSON(data);

		$('input[name="'+csrf_name+'"]').val(data.csrf_key);

		if($(".message-inner",el.container).find('.no_messages')){
			$(".message-inner",el.container).find('.no_messages').remove();
		}
		$(".message-inner",el.container).append(data.row);

		$(".message-inner",el.container).scrollTop($(".message-inner",el.container)[0].scrollHeight);
		$(".message-input",el.container).prop('rows',4);
		$("#message-input").val('');
		// $("a[rel^='prettyPhoto']").prettyPhoto({
		// 	deeplinking:false, 
		// 	social_tools:false
		// });
		csDropzone = null;

		users = $($(".user_to",el.container)).map(function(){return $(this).val();}).get();

		socket_data = {
			 emit_user_name: 'admin',
			 emit_from: socketUser,
			 emit_to: users,
			 message_id: data.message_id,
			 ci: $(".ci",el.container).val(),
		};
		socket.emit('send-message-to-admin', socket_data );
		// $('.message-input',el.container).data("emojioneArea").setText("");

	},
	error: function(XMLHttpRequest, textStatus, errorThrown) {}

});
}




/**
* Send message to admin
*
* @param  {object} el
* @param  {String} message_type
* @param  {Array}  attachments
* @return {void}
*/
function sendMessageToFriend(el, message_type,attachments){

var editorID, data, users, socket_data, editorID_a;
if (typeof(attachments)==='undefined') attachments = [];
if (typeof(message_type)==='undefined') message_type = 'message';



editorID = 'message-input.message-input-frontend-side-footer';


if(el.editorID!=undefined)
	editorID = el.editorID;

if($('#'+editorID).val() == '' && attachments.length == 0)
	return false;

data = $(".user_to",el.container).serializeArray();
data.push({'name':'user_id','value':$(".user_id",el.container).val()});
data.push({'name':'ci','value':$(".ci",el.container).val()});
data.push({'name':'message','value':$('#'+editorID).val()});
data.push({'name':'message_type','value':message_type});
data.push({'name':'attachments','value':attachments});
data.push({'name':'cs_key','value':'xwb_send_message_to_friend'});



$csrf = $('.csrf_key');
csrf_name = $csrf.attr('name');
csrf_key = $csrf.val();
data.push({'name':csrf_name, 'value': csrf_key});
$.ajax({
	url: window.location.pathname,
	type: "post",
	data: data,
	success: function(data){
		data = $.parseJSON(data);

		$('input[name="'+csrf_name+'"]').val(data.csrf_key);

		if($(".message-inner",el.container).find('.no_messages')){
			$(".message-inner",el.container).find('.no_messages').remove();
		}
		$(".message-inner",el.container).append(data.row);

		$(".message-inner",el.container).scrollTop($(".message-inner",el.container)[0].scrollHeight);
		$(".message-input",el.container).prop('rows',4);
		$("#message-input").val('');
		// $("a[rel^='prettyPhoto']").prettyPhoto({
		// 	deeplinking:false, 
		// 	social_tools:false
		// });
		csDropzone = null;

		users = $($(".user_to",el.container)).map(function(){return $(this).val();}).get();

		socket_data = {
			 emit_user_name: 'friend',
			 emit_from: socketUser,
			 emit_to: users,
			 message_id: data.message_id,
			 ci: $(".ci",el.container).val(),
		};
		socket.emit('send-message-to-friend', socket_data );
		// $('.message-input',el.container).data("emojioneArea").setText("");

	},
	error: function(XMLHttpRequest, textStatus, errorThrown) {}

});
}

/**
* Send message to customer
*
* @param  {object} el
* @param  {String} message_type
* @param  {Array}  attachments
* @return {void}
*/
function sendMessageToCustomer(el, message_type,attachments){

var editorID, data, users, socket_data;
if (typeof(attachments)==='undefined') attachments = [];
if (typeof(message_type)==='undefined') message_type = 'message';

editorID = 'message-input';

if(el.editorID!=undefined)
	editorID = el.editorID;

if($('#'+editorID).val() == '' && attachments.length == 0)
	return false;

data = $(".user_to",el.container).serializeArray();
data.push({'name':'user_id','value':$(".user_id",el.container).val()});
data.push({'name':'ci','value':$(".ci",el.container).val()});
data.push({'name':'message','value':$('#'+editorID).val()});
data.push({'name':'message_type','value':message_type});
data.push({'name':'attachments','value':attachments});
data.push({'name':'cs_key','value':'xwb_send_message_to_customer'});

$csrf = $('.csrf_key');
csrf_name = $csrf.attr('name');
csrf_key = $csrf.val();
data.push({'name':csrf_name, 'value': csrf_key});
$.ajax({
	url: window.location.pathname,
	type: "post",
	data: data,
	success: function(data){
		data = $.parseJSON(data);

		$('input[name="'+csrf_name+'"]').val(data.csrf_key);

		if($(".message-inner",el.container).find('.no_messages')){
			$(".message-inner",el.container).find('.no_messages').remove();
		}
		$(".message-inner",el.container).append(data.row);

		$(".message-inner",el.container).scrollTop($(".message-inner",el.container)[0].scrollHeight);
		$(".message-input",el.container).prop('rows',4);
		$("#message-input").val('');
		
		// $("a[rel^='prettyPhoto']").prettyPhoto({
		// 	deeplinking:false, 
		// 	social_tools:false
		// });

		csDropzone = null;

		users = $($(".user_to",el.container)).map(function(){return $(this).val();}).get();
		
		socket_data = {
			 emit_user_name: 'customer',
			 emit_from: socketUser,
			 emit_to: users,
			 message_id: data.message_id,
			 ci: $(".ci",el.container).val(),
		};
		socket.emit( 'send-message-to-customer', socket_data );
		// $('.message-input',el.container).data("emojioneArea").setText("");
		
	},
	error: function(XMLHttpRequest, textStatus, errorThrown) {}
});

}

/**
* Send message to Pvp Player
*
* @param  {object} el
* @param  {String} message_type
* @param  {Array}  attachments
* @return {void}
*/
function sendMessageToPvpPlayer(el, message_type,attachments){

var editorID, data, users, socket_data;
if (typeof(attachments)==='undefined') attachments = [];
if (typeof(message_type)==='undefined') message_type = 'message';

editorID = 'message-input';
if(el.editorID!=undefined)
	editorID = el.editorID;

if($('#'+editorID).val() == '' && attachments.length == 0)
	return false;

data = $(".user_to",el.container).serializeArray();
data.push({'name':'user_id','value':$(".user_id",el.container).val()});
data.push({'name':'ci','value':$(".ci",el.container).val()});
data.push({'name':'message','value':$('#'+editorID).val()});
data.push({'name':'message_type','value':message_type});
data.push({'name':'cs_key','value':'xwb_send_message_pvp_player'});

$csrf = $('.csrf_key');
csrf_name = $csrf.attr('name');
csrf_key = $csrf.val();
data.push({'name':csrf_name, 'value': csrf_key});

$.ajax({
	url: window.location.pathname,
	type: "post",
	data: data,
	success: function(data){
		data = $.parseJSON(data);

		$('input[name="'+csrf_name+'"]').val(data.csrf_key);

		if($(".message-inner",el.container).find('.no_messages')){
			$(".message-inner",el.container).find('.no_messages').remove();
		}
		$(".message-inner",el.container).append(data.row);

		$(".message-inner",el.container).scrollTop($(".message-inner",el.container)[0].scrollHeight);
		$(".message-input",el.container).prop('rows',4);
		$("#message-input").val('');
		
		// $("a[rel^='prettyPhoto']").prettyPhoto({
		// 	deeplinking:false, 
		// 	social_tools:false
		// });
		csDropzone = null;

		users = $($(".user_to",el.container)).map(function(){return $(this).val();}).get();
	
		socket_data = {
			 emit_user_name: 'pvp',
			 emit_from: socketUser,
			 emit_to: users,
			 message_id: data.message_id,
			 ci: $(".ci",el.container).val() 
		};

		socket.emit( 'send-message-to-pvp-player', socket_data );
		// $('.message-input',el.container).data("emojioneArea").setText("");
	},
	error: function(XMLHttpRequest, textStatus, errorThrown) {}
});
}

/**
* Send message to Pvp Player From Admin
*
* @param  {object} el
* @param  {String} message_type
* @param  {Array}  attachments
* @return {void}
*/
function sendMessageToPvpPlayerFromAdmin(el, message_type,attachments){

var editorID, data, users, socket_data;
if (typeof(attachments)==='undefined') attachments = [];
if (typeof(message_type)==='undefined') message_type = 'message';

editorID = 'message-input';
if(el.editorID!=undefined)
	editorID = el.editorID;

if($('#'+editorID).val() == '' && attachments.length == 0)
	return false;

data = $(".user_to",el.container).serializeArray();
data.push({'name':'user_id','value':$(".user_id",el.container).val()});
data.push({'name':'ci','value':$(".ci",el.container).val()});
data.push({'name':'message','value':$('#'+editorID).val()});
data.push({'name':'message_type','value':message_type});
data.push({'name':'cs_key','value':'xwb_send_message_pvp_player_from_admin'});

$csrf = $('.csrf_key');
csrf_name = $csrf.attr('name');
csrf_key = $csrf.val();
data.push({'name':csrf_name, 'value': csrf_key});

$.ajax({
	url: window.location.pathname,
	type: "post",
	data: data,
	success: function(data){
		data = $.parseJSON(data);

		$('input[name="'+csrf_name+'"]').val(data.csrf_key);

		if($(".message-inner",el.container).find('.no-more-messages')){
			$(".message-inner",el.container).find('.no-more-messages').remove();
		}
		$(".message-inner",el.container).append(data.row);

		$(".message-inner",el.container).scrollTop($(".message-inner",el.container)[0].scrollHeight);
		$(".message-input",el.container).prop('rows',4);
		$("#message-input").val('');
		
		// $("a[rel^='prettyPhoto']").prettyPhoto({
		// 	deeplinking:false, 
		// 	social_tools:false
		// });
		csDropzone = null;

		users = $($(".user_to",el.container)).map(function(){return $(this).val();}).get();
		socketUser = data.socketUser;

		socket_data = {
			 emit_user_name: 'pvpadmin',
			 emit_from: socketUser,
			 emit_to: users,
			 message_id: data.message_id,
			 ci: $(".ci",el.container).val() 
		};
		socket.emit( 'send-message-to-pvp-player-from-admin',socket_data);
		// $('.message-input',el.container).data("emojioneArea").setText("");
	},
	error: function(XMLHttpRequest, textStatus, errorThrown) {}
});
}




/**
* Send message to Pvp Player
*
* @param  {object} el
* @param  {String} message_type
* @param  {Array}  attachments
* @return {void}
*/
function callAdminForHelp(args){
var calladminvalue, groupid;
var data = {
		'calladminvalue': args.calladminvalue,
		'groupid': args.groupid,
		'cs_key': 'xwb_calladminforhelp',
	};

$.ajax({
	url: window.location.pathname,
	type: "post",
	data: data,
	success: function(data){

		$('.call_admin_row #call-admin-for-help').remove();
		// $('.popup-content-wrapper #call-admin-for-help').css({'pointer-events':'none','background':'#fff','border': 'none','outline': '0'});
		// $('.popup-content-wrapper #call-admin-for-help:focus').css({'outline': '0'});
		$('.call_admin_row').html('<i class="fa fa-check"></i> Request Sent');
	},
	error: function(XMLHttpRequest, textStatus, errorThrown) {}
});
}

/**
* Upload attachment dialog box script
* 
* @return void
*/
function uploadAttachment(element) {
var files, attachments, el;
bootbox.dialog({
      title: 'Upload Attachment',
      message: '<div class="row">'+
                  '<div class="col-md-12">'+
                      '<div id="attachment-container" class="dropzone">'+
                          
                      '</div>'+
                  '</div>'+
              '</div>',
      size: 'large',
      closeButton: false,
      buttons:{
      	send: {
              label: "Send",
              className: "btn-success",
              callback: function () {
              	files = csDropzone.files;
              	attachments = [];
              	$.each( files, function( key, value ) {
              		attachments.push(value.previewElement.id);
				});

              	if(attachments.length>0){
              		el = {};
              		if($(element).parents('#message-container').length == 0)
						el.container = $(element).parents('.xwb-cb-chat');
					else
						el.container = $(element).parents('#message-container');	
					
              		sendMessage(el,'attachment',attachments);
              	}

              }
          },
          cancel: 
          {
              label: "Close",
              className: "btn-warning",
              callback: function () {
              	csDropzone.removeAllFiles();
              }
          }
      }

  });

  /* initialize dropzone */
csDropzone = new Dropzone("div#attachment-container", { 
	url: window.location.pathname,
	method: 'post',
	addRemoveLinks: true,
	acceptedFiles: '.gif,.jpg,.png,.zip,.zipx,.rar,.7z,.pdf,.doc,.docx,.txt,.odt,.mp3,.mp4',
});

/* append xwb_upload to formData to recognize the upload function */
csDropzone.on('sending', function(file, xhr, formData){
	$csrf = $('.csrf_key');

	csrf_name = $csrf.attr('name');
	csrf_key = $csrf.val();

	formData.append(csrf_name,csrf_key);
  	formData.append('cs_key', 'xwb_upload');

});

/* Assign Attachment id from server to file.previewElement.id */
csDropzone.on("success", function(file, response) {
	var obj = JSON.parse(response);
	$('input[name="'+csrf_name+'"]').val(obj.csrf_key);
  	file.previewElement.id = obj.attachmentID;
});

/* on upload error */
csDropzone.on("error", function(file, response) {
	var message, _ref, _results, node, _i, _len;
	var obj = JSON.parse(response);
	$('input[name="'+csrf_name+'"]').val(obj.csrf_key);

    	message = obj.response;

      file.previewElement.classList.add("dz-error");
      _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          node = _ref[_i];
          _results.push(node.textContent = message);
      }
      return _results;
});

/* ajax function to remove file from server */
csDropzone.on("removedfile", function(file) {
	var postData = {
			'attachmentID': file.previewElement.id,
			'cs_key' : 'xwb_deleteFile'
		};
	$csrf = $('.csrf_key');
	csrf_name = $csrf.attr('name');
	csrf_key = $csrf.val();
	postData[csrf_name] = csrf_key;


	$.ajax({
		url: window.location.pathname,
		type: "post",
		data: postData,
		success: function(data){
			data = JSON.parse(data);
			$('input[name="'+csrf_name+'"]').val(data.csrf_key);
			return true;
		}
	});

	});

}


/**
* Delete message
* 
* @param  {int} conversationID
* @return {void}
*/
function deleteMessage(conversationID){
bootbox.confirm("Are you sure you want to delete this message?", function(result){ 
      if(result){
      	var postData = {
                  'conversationID':conversationID,
                  'cs_key' : 'xwb_deleteMessage'
              };

          $csrf = $('.csrf_key');
		csrf_name = $csrf.attr('name');
		csrf_key = $csrf.val();
		postData[csrf_name] = csrf_key;


          $.ajax({
              url: window.location.pathname,
              type: "post",
              data: postData,
              success: function(data){
              	var obj = $.parseJSON(data);
              	$('input[name="'+csrf_name+'"]').val(obj.csrf_key);

                 	if(obj.status){
              		$("div.message-row[data-cid='"+conversationID+"']").remove();
                 	}
              },
              error: function(XMLHttpRequest, textStatus, errorThrown) {}
          });
      }
  });
}


/**
* Delete all message conversation
* 
* @return {void}
*/
function deleteConversation(el){
var user_to, ci, obj;
user_to = $(el).parents('#message-container').find('.user_to').val();
ci = $(el).parents('#message-container').find('.ci').val();


bootbox.confirm("Are you sure you want to delete all messages?", function(result){ 
      if(result){
      	var postData = {
                  'user_to': user_to,
                  'ci': ci,
                  'cs_key' : 'xwb_deleteAllMessages'
              };
          $csrf = $('.csrf_key');
		csrf_name = $csrf.attr('name');
		csrf_key = $csrf.val();
		postData[csrf_name] = csrf_key;

          $.ajax({
              url: window.location.pathname,
              type: "post",
              data: postData,
              success: function(data){
              	obj = $.parseJSON(data);
              	$('input[name="'+csrf_name+'"]').val(obj.csrf_key);

                 	if(obj.status){
              		if(ci!='')
              			$("#cs-ul-users li.cs-list-users[data-ci='"+ci+"']").click();
              		else
              			$("#cs-ul-users li.cs-list-users[data-user='"+user_to+"']").click();

              		$('.cb-conversation-container[data-cnid="'+ci+'"]').parent('.xwb-cb-chat').find('.chatbox-title-close').trigger('click');
                 	}

              },
              error: function(XMLHttpRequest, textStatus, errorThrown) {}
          });
      }
  });
}


/**
* Search contact list
* 
* @return none
*/
function searchContact(){
var input, filter, ul, li, a, i;
  input = document.getElementById("xwb-search-contact");
  filter = input.value.toUpperCase();
  ul = document.getElementById("cs-ul-users");
  li = ul.getElementsByTagName("li");
  for (i = 0; i < li.length; i++) {
      a = li[i].getElementsByTagName("a")[0];
      if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
          li[i].style.display = "";
      } else {
          li[i].style.display = "none";

      }
  }
}

/**
* Search contact list in sidebar
* 
* @return none
*/
function searchSideContact(){
var input, filter, ul, li, a, i;
  input = document.getElementById("xwb-search-sidecontact");
  filter = input.value.toUpperCase();
  ul = document.getElementById("cs-ul-sideusers");
  li = ul.getElementsByTagName("li");
  for (i = 0; i < li.length; i++) {
      a = li[i].getElementsByTagName("a")[0];
      if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
          li[i].style.display = "";
      } else {
          li[i].style.display = "none";

      }
  }
}





/**
* Create New Group Conversation
* 
* @param  object el [Element clicked]
* @return null
*/
function createGroupConversation(el){

bootbox.dialog({
      title: 'Create Group Conversation',
      message: '<div class="row">'+
      			'<div class="col-md-12 xwb-message-container">'+
      			'</div>'+
      			'<form class="form-horizontal" name="form_create_conversation" id="xwb-form-create-conversation">'+
                    '<div class="col-md-12">'+
	                    '<div class="form-group">'+
	                        '<label class="control-label col-md-4 col-sm-4 col-xs-12" for="conversation_name">Conversation Name <span class="required">*</span>'+
	                        '</label>'+
	                        '<div class="col-md-8 col-sm-8 col-xs-12">'+
	                        	'<input id="xwb-conversation-name" name="conversation_name" required="required" class="form-control col-md-7 col-xs-12" type="text">'+
	                        '</div>'+
                      	'</div>'+
                    	'<div class="form-group">'+
	                        '<label class="control-label col-md-4 col-sm-4 col-xs-12" for="conversation_users">Select Users <span class="required">*</span>'+
	                        '</label>'+
	                        '<div class="col-md-8 col-sm-8 col-xs-12">'+
	                          	'<select multiple="multiple" style="width:100%;" id="xwb-conversation-users" class="xwb-conversation-users" name="conversation_users[]">'+
	                          		'<option value="">Select Users</option>'+
	                          	'</select>'+
	                         '</div>'+
	                    '</div>'+
                    '</div>'+
                '</form>'+
              '</div>',
      closeButton: false,
      buttons:{
      	create: {
              label: "Create",
              className: "btn-info",
              callback: function () {
              	var data = $("#xwb-form-create-conversation").serializeArray();
              	data.push({'name':'cs_key', 'value':'xwb_create_conversation'});

              	$csrf = $('.csrf_key');
				csrf_name = $csrf.attr('name');
				csrf_key = $csrf.val();
              	data.push({'name':csrf_name, 'value':csrf_key});

				$.ajax({
					url: window.location.pathname,
					type: "post",
					data: data,
					dataType: 'JSON',
					success: function(data){
						$('input[name="'+csrf_name+'"]').val(data.csrf_key);
						if(data.status == false){
							$('.xwb-message-container').html('<div class="alert alert-danger alert-dismissible fade in" role="alert">'+
                  				'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>'+
                  				'</button>'+
                  				data.message+
              					'</div>');
						}else{
							$("#cs-ul-sideusers").append(data.li_sideuser_html);
							$("#cs-ul-users").append(data.li_main_contact_html);


							if($(el).parent('.xwb-main-contact-conversation').length==1){
								$('#cs-ul-users li[data-ci="'+data.cn_id+'"]').trigger('click');
							}

							if($(el).parent('.xwb-tray-contact-conversation').length==1){
								$('#cs-ul-sideusers li[data-ci="'+data.cn_id+'"]').trigger('click');
							}

							bootbox.hideAll();
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {}
				});
				return false;
              }
          },
          cancel: {
              label: "Close",
              className: "btn-warning",
              callback: function () {
              	
              }
          }
      }

  });

  $("#xwb-conversation-users").select({
	ajax: {
		url: window.location.pathname,
		type: "get",
		dataType: 'JSON',
		data: function (params){
			var query = {
				'term': params.term,
				'cs_key' : 'xwb_get_users'
			};
			return query;
		},
		success: function(data){
		}
	}
  });
}



/**
* Group Conversation Option
* 
* @param  Int cn_id [Conversation Name ID]
* @return null
*/
function conversationOption(cn_id) {
var users, options, socket_data, query;
var postData = {
		'cn_id': cn_id,
		'cs_key' : 'xwb_get_conversation_option'
	};

$csrf = $('.csrf_key');
csrf_name = $csrf.attr('name');
csrf_key = $csrf.val();
postData[csrf_name] = csrf_key;

$.ajax({
	url: window.location.pathname,
	type: "get",
	dataType: "JSON",
	data: postData,
	success: function(data){
		$('input[name="'+csrf_name+'"]').val(data.csrf_key);

		users = data.users;
		options = '';
		$.each(users,function(i,v){
			options +='<option value="'+i+'" selected>'+v.display_name+'</option>';
		});

		/*Dialog box*/
		bootbox.dialog({
			title: 'Group Conversation Option',
		    message: '<div class="row">'+
		    			'<div class="col-md-12">'+
	    					'<div class="xwb-message-notification"></div>'+
	    					'<form class="form-horizontal" name="form_group_conversation" id="xwb-conversation-option">'+
	    						'<input value="'+cn_id+'" id="cn_id" name="cn_id" type="hidden" />'+
		    					'<div class="form-group">'+
			                        '<label class="control-label col-md-4 col-sm-4 col-xs-12" for="conversation_name">Conversation Name <span class="required">*</span>'+
			                        '</label>'+
			                        '<div class="col-md-8 col-sm-8 col-xs-12">'+
			                        	'<input value="'+data.conversation_name+'" id="xwb-conversation-name" name="conversation_name" required="required" class="form-control col-md-7 col-xs-12" type="text">'+
			                        '</div>'+
		                      	'</div>'+
			    				'<div class="form-group">'+
			                        '<label class="control-label col-md-4 col-sm-4 col-xs-12" for="conversation_users">Select Users <span class="required">*</span>'+
			                        '</label>'+
			                        '<div class="col-md-8 col-sm-8 col-xs-12">'+
			                          	'<select multiple="multiple" style="width:100%;" id="xwb-conversation-users" class="xwb-conversation-users" name="conversation_users[]">'+
			                          		'<option value="">Select Users</option>'+
			                          		options+
			                          	'</select>'+
			                        '</div>'+
			                    '</div>'+
			    			'</form>'+
		    			'</div>'+
		    		'</div>',
		    closeButton: false,
		    buttons:{
		    	save: {
		            label: "Save",
		            className: "btn-success",
		            callback: function () {
		            	data = $("#xwb-conversation-option").serializeArray();
		            	data.push({'name': 'cs_key', 'value': 'xwb_update_conversation'});

		            	$csrf = $('.csrf_key');
						csrf_name = $csrf.attr('name');
						csrf_key = $csrf.val();
						data.push({'name': csrf_name, 'value': csrf_key});

						$.ajax({
							url: window.location.pathname,
							type: "post",
							data: data,
							dataType: 'JSON',
							success: function(data){
								$('input[name="'+csrf_name+'"]').val(data.csrf_key);

								if(data.status == false){
									$('.xwb-message-notification').html('<div class="alert alert-danger alert-dismissible fade in" role="alert">'+
	                    				'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>'+
	                    				'</button>'+
	                    				data.message+
	                					'</div>');
								}else{
									updateGroupConversation(data);

									bootbox.hideAll();

									// emit changes to the other affected users
									socket_data = {
										'emit_to': data.con_all_users,
										'cn_id': data.cn_id,
										'emit_from': data.user_id
									};
									socket.emit( 'update-group-conversation', socket_data );
								}
							},
							error: function(XMLHttpRequest, textStatus, errorThrown) {}
						});

						return false;
		            }
		        },
		        cancel: {
		            label: "Close",
		            className: "btn-warning",
		            callback: function () {
		            }
		        }
		    }
		});


		$("#xwb-conversation-users").select2({

			ajax: {
				url: window.location.pathname,
				type: "get",
				dataType: 'JSON',
				data: function (params){
					query = {
						'term': params.term,
						'cs_key' : 'xwb_get_users'
					};
					return query;
				},
				success: function(data){
					
				}
			}
	    });

	},
	error: function(XMLHttpRequest, textStatus, errorThrown) {}
});




}


/**
* Update users affected by the changed group conversation
* 
* @param  {object} data [Object from nodejs data]
* @return {null}
*/
function updateGroupConversation(data) {
var $chatboxParent;
$('.xwb-message-container[data-cnid="'+data.cn_id+'"]').attr('data-users',data.conversation_users);
$('.cb-conversation-container[data-cnid="'+data.cn_id+'"]').attr('data-users',data.conversation_users);

$('.cs-list-users[data-ci="'+data.cn_id+'"]').attr('data-user',data.conversation_users);
$('.cs-list-users[data-ci="'+data.cn_id+'"]').find('.xwb-display-name').text(data.conversation_name);

$('.xwb-message-container[data-cnid="'+data.cn_id+'"]').find('.conversation-name a').html('<i class="fa fa-cog"></i> '+data.conversation_name);
$('.xwb-message-container[data-cnid="'+data.cn_id+'"]').find('.user_to').remove();

$chatboxParent = $('.cb-conversation-container[data-cnid="'+data.cn_id+'"]').parents('.xwb-cb-chat');
$chatboxParent.attr('data-user',data.conversation_users);
$chatboxParent.find('.conversation-title a').text(data.conversation_name);
$chatboxParent.find('.user_to').remove();

$.each(data.con_usersArr,function(i,v){
	$chatboxParent.appendUsertoCB(v);
	$('.xwb-message-container[data-cnid="'+data.cn_id+'"]').appendUsertoMain(v);
});

if(data.user_disable){
	$('.xwb-message-container[data-cnid="'+data.cn_id+'"]').find('.emojionearea-editor').attr('placeholder','You cannot reply this conversation.').attr('contenteditable',false);
	$('.xwb-message-container[data-cnid="'+data.cn_id+'"]').find('.xwb-attachment, .emojionearea-button').hide();

	$chatboxParent.find('.emojionearea-editor').attr('placeholder','You cannot reply this conversation.').attr('contenteditable',false);
	$chatboxParent.find('.emojionearea-button').hide();
	
}else{
	$('.xwb-message-container').find('.emojionearea-editor').attr('placeholder','Enter your message here').attr('contenteditable',true);
	$('.xwb-message-container').find('.xwb-attachment, .emojionearea-button').show();


	$chatboxParent.find('.emojionearea-editor').attr('placeholder','Enter your message here').attr('contenteditable',true);
	$chatboxParent.find('.emojionearea-button').show();

}
}



/**
* Mark conversation as read
* 
* @param  {Number} cn_id   [Conversation name ID]
* @param  {Number} user_to [User to]
* @return {Null}
*/
function markConversationRead(cn_id,user_to){
var postData = {
		'users' : user_to,
		'cn_id' : cn_id,
		'cs_key' : 'xwb_mark_read',
	};
$csrf = $('.csrf_key');
csrf_name = $csrf.attr('name');
csrf_key = $csrf.val();
postData[csrf_name] = csrf_key;
$.ajax({
	url: window.location.pathname,
	type: "post",
	dataType: 'JSON',
	data: postData,
	success: function(data){

		$('input[name="'+csrf_name+'"]').val(data.csrf_key);
		if(data.cn_id!=''){
			$("li[data-ci='"+data.cn_id+"']").find('span.unread').remove();
		}else{
			$("li[data-user='"+data.user+"']").find('span.unread').remove();
		}
		
	},
	error: function(XMLHttpRequest, textStatus, errorThrown) {}

});
}


/* =============================================================================== */
/* start socket script */
/**
* update online users
* @param array data 
* @return void
*/
if(typeof socket != 'undefined'){
socket.on("update ol users", function(data){
	var users, $el;
	users = data.users;

		$.each(users, function( index, value ) {
			$el = $("ul#cs-ul-users, ul#cs-ul-sideusers, #xwb-bottom-chat-container").find("[data-user=\'" + value + "\']");
			$el.removeClass('online');
			$el.removeClass('offline');
			if($el.length>0){
				$el.data('status','online');
				$el.addClass('online');
			}else{
				$el.data('status','offline');
				$el.addClass('offline');
			}

		});

});


/**
 * On get group data
 * 
 * @param array data 
 * @return mixed
 */
socket.on("get-join-grp_data", function(data){
	get_join_grp_data(data);
});


/**
 * On get submitted report
 * 
 * @param array data 
 * @return mixed
 */
socket.on("get_report_updated-data", function(data){
	get_updated_lobby_report(data);
});

/**
 * On get group played data
 * 
 * @param array data 
 * @return mixed
 */
socket.on("get_played_group-data", function(data){
		get_played_group_data(data);
});	

/**
 * On user disconnected
 * 
 * @param array data 
 * @return mixed
 */
socket.on("recieved-message", function(data){
		get_single_message(data);
});

socket.on("disconnect user", function(data){
	var disconnected_user, $el;
	disconnected_user = data.user;
	$el = $("ul#cs-ul-users, ul#cs-ul-sideusers, #xwb-bottom-chat-container").find("[data-user=\'" + disconnected_user + "\']");
	$el.data('status','offline');
	$el.removeClass('online');
	$el.addClass('offline');
});

// return false;
// Recieved message

// Socket update group conversation
socket.on("update-group-conversation", function(data){
	var postData = {
			'cn_id': data.cn_id,
			'user_id' : data.emit_to,
			'cs_key': 'xwb_get_group_conversation_data'
		};

	$csrf = $('.csrf_key');
	csrf_name = $csrf.attr('name');
	csrf_key = $csrf.val();
	postData[csrf_name] = csrf_key;

	$.ajax({
		url: window.location.pathname,
		type: "get",
		data: postData,
		dataType: 'JSON',
		success: function(data){
			$('input[name="'+csrf_name+'"]').val(data.csrf_key);

			updateGroupConversation(data);
		}
	});
});

}
/* end socket script */
/* =============================================================================== */

(function ( $ ) {
'use strict';
$(document).ready(function(){
	var $csrf;
	var csrf_name, csrf_key;
	$('#xwb-users-table').change(function(){
		var postData = {
				'table': $(this).val(),
				'cs_key' : 'xwb_get_usertable_fields'
			};
		$csrf = $('.csrf_key');
		csrf_name = $csrf.attr('name');
		csrf_key = $csrf.val();
		postData[csrf_name] = csrf_key;


		$.ajax({
			url: window.location.pathname,
			type: "post",
			data: postData,
			success: function(data){
				data = $.parseJSON(data);
				$('input[name="'+csrf_name+'"]').val(data.csrf_key);

				var userFields = '';
				$.each(data.fields, function(i,v){
					userFields += '<option value='+v+'>'+v+'</option>';
				});
				$("#xwb-users-id").html(userFields);
				if(data.user_id_field)
					$("#xwb-users-id").val(data.user_id_field);

				$("#xwb-display-name").html(userFields);
				if(data.user_name_field)
					$("#xwb-display-name").val(data.user_name_field);
				
				$("#xwb-pic-filename").html(userFields);
				if(data.picture_filename)
					$("#xwb-pic-filename").val(data.picture_filename);
				
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {}
		});
	});



	$('#xwb-user-table-other').change(function(){
		var postData = {
				'table': $(this).val(),
				'cs_key' : 'xwb_get_otherusertable_fields'
			};
		$csrf = $('.csrf_key');
		csrf_name = $csrf.attr('name');
		csrf_key = $csrf.val();
		postData[csrf_name] = csrf_key;


		$.ajax({
			url: window.location.pathname,
			type: "post",
			data: postData,
			success: function(data){
				data = $.parseJSON(data);
				$('input[name="'+csrf_name+'"]').val(data.csrf_key);

				var userFields = '';
				$.each(data.fields, function(i,v){
					userFields += '<option value='+v+'>'+v+'</option>';
				});

				$("#xwb-user-table-fkey").html(userFields);
				if(data.user_table_fkey)
					$("#xwb-user-table-fkey").val(data.user_table_fkey);

				$("#xwb-user-table-fdisplayname").html(userFields);
				if(data.user_table_fdisplayname)
					$("#xwb-user-table-fdisplayname").val(data.user_table_fdisplayname);

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {}
		});
	});


	$('#xwb-picture-table').change(function(){
		var postData = {
				'table': $(this).val(),
				'cs_key' : 'xwb_get_picturetable_fields'
			};
		$csrf = $('.csrf_key');
		csrf_name = $csrf.attr('name');
		csrf_key = $csrf.val();
		postData[csrf_name] = csrf_key;

		$.ajax({
			url: window.location.pathname,
			type: "post",
			data: postData,
			success: function(data){
				data = $.parseJSON(data);
				$('input[name="'+csrf_name+'"]').val(data.csrf_key);

				var userFields = '';
				$.each(data.fields, function(i,v){
					userFields += '<option value='+v+'>'+v+'</option>';
				});
				
				$("#xwb-pic-table-key").html(userFields);
				$("#xwb-picture-field").html(userFields);

				if(data.picture_table_key)
					$("#xwb-pic-table-key").val(data.picture_table_key);

				if(data.picture_field)
					$("#xwb-picture-field").html(data.picture_field);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {}
		});
	});
});

}( jQuery ));


var $ = jQuery.noConflict();
function browsePath(el){
		bootbox.dialog({
			title: 'Select profile picture path',
		    message: '<div class="row">'+
		    			'<div class="col-md-12">'+
		    				'<a href="javascript:;" onClick="goToPath(\'..\')" class="btn btn-primary" title="Up one level">'+
	                            '<span class="fa fa-arrow-up"></span> Up one level'+
	                        '</a>'+
	                        '<input type="hidden" name="current_dir" id="xwb-current-dir" />'+
	                        '<hr />'+
		    				'<div id="xwb-folder-path-container">'+
		    				'</div>'+
		    			'</div>'+
		    		'</div>',
		    closeButton: false,
		    buttons:{
	        	insert: {
	                label: "Insert",
	                className: "btn-success",
	                callback: function () {
	                	$("#profile_pic_path").val($(".xwb-folder-selected:checked").val());
	                }
	            },
	            cancel: {
	                label: "Close",
	                className: "btn-warning",
	                callback: function () {
	                	
	                }
	            }
	        }
		});

		goToPath('');
	}


function goToPath(path){
	var postData = {
			'cs_key': 'xwb_go_to_path',
			'path': path,
			'current_dir': $("#xwb-current-dir").val()
		};
	$csrf = $('.csrf_key');
	csrf_name = $csrf.attr('name');
	csrf_key = $csrf.val();
	postData[csrf_name] = csrf_key;

  	$.ajax({
		url: window.location.pathname,
		type: "post",
		data: postData,
		success: function(data){
			data = $.parseJSON(data);
			$('input[name="'+csrf_name+'"]').val(data.csrf_key);

          	$("#xwb-folder-path-container").html(data.dir_list);
          	$("#xwb-current-dir").val(data.current_dir);
		}
	});
}

function selectUsers(){
	bootbox.dialog({
			title: 'Select users',
		    message: '<div class="row">'+
		    			'<div class="col-md-12">'+
		    				'<div class="table-responsive xwb-table-users-container">'+
		    					'<div class="xwb-message-notification"></div>'+
		    					'<form class="form-horizontal" name="form_users" id="xwb-form-users">'+
				    				'<table id="xwb-table-users" class="table table-bordered table-hover">'+
										'<thead>'+
											'<tr>'+
												'<th width="60">'+
													'<label><input id="xwb-checkall" type="checkbox" value=""> All </label>'+
												'</th>'+
												'<th><input type="text" placeholder="Search User.." onkeyup="searchUser()" name="search_user" id="xwb-search-user" class="form-control" /></th>'+
											'</tr>'+
										'</thead>'+
										'<tbody class="xwb-users">'+
										'</tbody>'+
				    				'</table>'+
				    			'</form>'+
		    				'</div>'+
		    			'</div>'+
		    		'</div>',
		    closeButton: false,
		    buttons:{
	        	save: {
	                label: "Save",
	                className: "btn-success",
	                callback: function () {
	                	var data = $("#xwb-form-users").serializeArray();
	                	data.push({'name': 'cs_key', 'value': 'xwb_setUsers'});

	                	$csrf = $('.csrf_key');
						csrf_name = $csrf.attr('name');
						csrf_key = $csrf.val();
						data.push({'name': csrf_name, 'value': csrf_key});

				    	$.ajax({
							url: window.location.pathname,
							type: "post",
							data: data,
							success: function(data){
								data = $.parseJSON(data);
								$('input[name="'+csrf_name+'"]').val(data.csrf_key);

								$(".xwb-message-notification").html(data.message);
								setTimeout(function(){
									bootbox.hideAll();
								},1000);
							},
							error: function(XMLHttpRequest, textStatus, errorThrown) {}
						});

						return false;
	                }
	            },
	            cancel: {
	                label: "Close",
	                className: "btn-warning",
	                callback: function () {
	                }
	            }
	        }
		});

	var postData = {
			'cs_key': 'xwb_getUsers',
			'users_table': $('#xwb-users-table').val(),
			'users_id': $('#xwb-users-id').val(),
			'display_name': $('#xwb-display-name').val(),
			'users_table_other': $('#xwb-user-table-other').val(),
			'users_table_other_id': $('#xwb-user-table-fkey').val(),
			'users_table_other_displayname': $('#xwb-user-table-fdisplayname').val()
		};

	$csrf = $('.csrf_key');
	csrf_name = $csrf.attr('name');
	csrf_key = $csrf.val();
	postData[csrf_name] = csrf_key;

  	$.ajax({
		url: window.location.pathname,
		type: "post",
		data: postData,
		success: function(data){
			data = $.parseJSON(data);
			$('input[name="'+csrf_name+'"]').val(data.csrf_key);
			$("tbody.xwb-users").html(data.html);
		}
	});


    $('#xwb-checkall').click(function() {
        var checked = $(this).prop('checked');
        $('input:checkbox.users-check').prop('checked', checked);
    });

}

/**
 * Search User
 * 
 * @return none
 */
function searchUser(){
	var input, filter, i, table, tr, td;
    input = document.getElementById("xwb-search-user");
    filter = input.value.toUpperCase();
    table = document.getElementById("xwb-table-users");
    tr = $(table).find("tbody tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
        } else {
            tr[i].style.display = "none";

        }
    }
}

function updateConsole(){
	$("#form-console").submit(); // comment this line if you want to use ajax
}

//tip sound js

$( "#default_sound" ).click(function() {
	if(this.checked){
	    $('#default_sound').val(1);
	    $('.select_audio').hide();
	    $('#fireworks_audio').val("");
	}
	if(!this.checked){
	    $('#default_sound').val(0);
	    $('.select_audio').show();
	}
});
     $(document).ready(function(e){
    // Submit form data via Ajax
    $('#send_tip_amount input[name="fireworks_audio_file"]').on('change', function(){
      if($(this).val() != null || $(this).val() != undefined || $(this).val() !=''){
        $('#send_tip_amount .alert.tip_amount_error').html('');
      }
    })

    $("#upload_form").on('submit', function(e){
       // debugger;
       var amount = $('#send_tip_amount input[name="tip_amount_input"]').val();
       var checkbox_val = $('#send_tip_amount input[name="default_sound"]').val();
       var to_usr_arr = [];
        $('#send_tip_amount .team_member input[type="checkbox"]:checked').each(function(){
          to_usr_arr.push($(this).val());
        });
        var to_usr_arr = to_usr_arr;
       // var checkbox_val = $('#default_sound').val();
      if(checkbox_val==0){
        // var file_val = $('#fireworks_audio').val();
        var file_val = $('#send_tip_amount input[name="fireworks_audio_file"]').val();
        
        if(file_val!='')
        {
          var file_size=$("#fireworks_audio")[0].files[0].size/1024/ 1024;
          var file_size_mb=file_size.toFixed(0);
        }
      }
      
      if(to_usr_arr.length <= 0){
        $('#send_tip_amount .alert.tip_amount_error').html('Please Select User')
        $('#send_tip_amount .alert.tip_amount_error').show();
          return false;
      }else{

    
       if (amount=='') {
          $('#send_tip_amount .alert.tip_amount_error').html('Please Enter Amount');
          $('#send_tip_amount .alert.tip_amount_error').show();
          return false;
       } else {
          if(checkbox_val==0 && file_val=="")
          {
            $('#send_tip_amount .alert.tip_amount_error').html('Please Select file')
            $('#send_tip_amount .alert.tip_amount_error').show();
            return false;
          } else {
              if(file_size_mb>2)
               {
                $('#send_tip_amount .alert.tip_amount_error').html('Please Select file up to 2MB')
                $('#send_tip_amount .alert.tip_amount_error').show();
                return false;
              } else {
              e.preventDefault();
              $.ajax({
                  type: 'POST',
                  url: base_url +'livelobby/send_tip_to_team',
                  data: new FormData(this),
                  dataType: 'json',
                  contentType: false,
                  cache: false,
                  processData:false,
                  beforeSend: function(){
                      $('.submitBtn').attr("disabled","disabled");
                      $('#upload_form').css("opacity",".5");
                  },
                  success: function(res){ //console.log(response);
                      // var res = $.parseJSON(res);
                      // var res = JSON.parse(res);
                      // console.log(res);
                      // console.log(res.balance_status);

                        var is_admin = $('input[name="is_admin"]').val();
                        if (res.balance_status == 'less') {
                          alert("You do not have enough credits to place a challenge. Please Proceed to Purchase tab for more.");
                            window.location.href = base_url+'points/buypoints';
                        } else {
                            var sender_id = res.sender_id;
                            var get_grp_with_balance = res.get_grp_with_balance;
                            var tip_fireworks_sound = (res.tip_sound_type=="custom") ? res.tip_sound_file_name : $('#tip_fireworks_sound').val(); 
                            $('#send_tip_amount').hide();
                          
                          var socket_data = {
                            'get_grp_with_balance': get_grp_with_balance,
                            'sender_id': sender_id,
                            'sender_balance' : res.update_balance,
                            'on':'get_team_tip_data'
                          };
                          // console.log(res.tip_sound_type);
                          // console.log(res.tip_sound_file_name);
                        if (tip_fireworks_sound.length && tip_fireworks_sound != undefined && tip_fireworks_sound != '') {
                          socket_data.tip_fireworks_sound = tip_fireworks_sound;
                        }
                        socket.emit('sendrealtimedata', socket_data );
                        var data = {
                          'msg_data': res.lobby_get_chat,
                          'name': res.fan_tag,
                          'session_id': res.session_id,
                          'is_admin': is_admin,
                          'getmytip_sound':res.getmytip_sound,
                          'tag': 'send_socket_msg_lobby_grp'
                        };
                        commonshow(data);
                      }
                      $('.statusMsg').html('');
                      // if(response.status == 1){
                      //     $('#upload_form')[0].reset();
                      //     $('.statusMsg').html('<p class="alert alert-success">'+response.message+'</p>');
                      // }else{
                      //     $('.statusMsg').html('<p class="alert alert-danger">'+response.message+'</p>');
                      // }
                      $('#upload_form').css("opacity","");
                      $(".submitBtn").removeAttr("disabled");
                  }
                });
              }
            }
          }
        }
    });
});

